#!/bin/bash
REMOTE_IMAGE_TAG="${GAZELOOK_DOCKER_REPOSITORY}/${GAZELOOK_WEB_NAME}:${GAZELOOK_WEB_IMAGE_VERSION}"
sh clean-docker.sh
echo "==> Pushing image ${REMOTE_IMAGE_TAG}... "
docker push "${REMOTE_IMAGE_TAG}"