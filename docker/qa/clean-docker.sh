#!/bin/bash
REMOTE_IMAGE_TAG="${GAZELOOK_DOCKER_REPOSITORY}/${GAZELOOK_WEB_NAME}:${GAZELOOK_WEB_IMAGE_VERSION}"

echo "==> Stopping microservice ${GAZELOOK_WEB_NAME}... "
docker stop "${GAZELOOK_WEB_NAME}"
echo "==> Deleting microservice ${GAZELOOK_WEB_NAME}... "
docker rm "${GAZELOOK_WEB_NAME}"
echo "==> Deleting image tag ${REMOTE_IMAGE_TAG}... "
docker rmi "${REMOTE_IMAGE_TAG}"
echo "==> Building docker image ${REMOTE_IMAGE_TAG}... "

#docker build -f Dockerfile -t "${REMOTE_IMAGE_TAG}" .
docker build "${GAZELOOK_WEB_PATH_PROJECT}" -f Dockerfile -t "${REMOTE_IMAGE_TAG}"