import { ComponentFactoryResolver, ComponentRef, Directive, Input, ViewContainerRef } from '@angular/core';
import { DatosItem } from '../diseno/modelos/datos-item-lista.interface';
@Directive({
  selector: '[crearComponente]',
})
export class CrearComponenteDirective {
  private componentInstance: ComponentRef<any> = null;
  @Input() dataConfiguracion: number

  @Input() componente: number
  @Input() unClick: Function
  @Input() dobleClick: Function
  @Input() clickSostenido: Function

  @Input() itemInformacion: any
  constructor(
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
  }

  ngOnInit(): void {
    this.cargarComponente(
      {
        dataConfiguracion: this.dataConfiguracion,
        componente: this.componente,
        dataComponente: this.itemInformacion,
        unClick: this.unClick,
        dobleClick: this.dobleClick,
        clickSostenido: this.clickSostenido
      })
  }

  cargarComponente(datosItem: DatosItem) {

    if (!this.componentInstance) {
      this.createLoaderComponent(datosItem);
      this.makeComponentAChild();
    }
    this.componentInstance.instance.datosItem = datosItem;
  }

  //Crea componente
  private async createLoaderComponent(datosItem: DatosItem) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(datosItem.componente);
    this.componentInstance = this.viewContainerRef.createComponent(componentFactory);
  }

  //Agrega el componente hijo a la vista
  private async makeComponentAChild() {
    const loaderComponentElement = await this.componentInstance.location.nativeElement;
    const sibling: HTMLElement = await loaderComponentElement.previousSibling;
    sibling.insertBefore(loaderComponentElement, sibling.firstChild);
  }

  ngOnDestroy(): void {
    if (this.componentInstance) {
      this.componentInstance.destroy();
    }
  }
}

