import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { AccionesBuscadorLocalidadModal } from '@shared/diseno/enums';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos';
import { GeneradorId } from '@core/servicios/generales';
@Component({
  selector: 'app-buscador-modal',
  templateUrl: './buscador-modal.component.html',
  styleUrls: ['./buscador-modal.component.scss']
})
export class BuscadorModalComponent implements OnInit {
  @Input() configuracion: ConfiguracionBuscadorModal

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId,
  ) {}

  ngOnInit() {
  }

  // Abrir selector
  clickInputPreview() {
    if (this.configuracion.evento) {
      this.configuracion.evento({
        accion: AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR
      })
    }
  }
}
