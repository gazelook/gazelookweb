import { Component, Input, OnInit } from '@angular/core';
import { IdiomaNegocio } from 'dominio/logica-negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio';
import { ComentarioModel } from 'dominio/modelo/entidades';
import { ConfiguracionEstiloModel } from 'dominio/modelo/entidades';
import { PerfilModel } from 'dominio/modelo/entidades';
import { CodigosCatalogoIdiomasPrincipales } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { DireccionDelReproductor } from '../../diseno/enums/audio-reproductor.enum';
import { ColorDeFondo } from '@shared/diseno/enums';
import { AlbumNegocio } from 'dominio/logica-negocio';
import { MediaModel } from 'dominio/modelo/entidades';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId } from '@core/servicios/generales';
import { CodigosCatalogoEstilo } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-estilos.enum';
import { ConfiguracionAudioReproductor } from './../../diseno/modelos/audio-reproductor.interface';
import { ComentarioRespuesta, ConfiguracionComentario } from '@shared/diseno/modelos';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.scss']
})
export class ComentarioComponent implements OnInit {

  @Input() configuracion: ConfiguracionComentario

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private idiomaNegocio: IdiomaNegocio
  ) {}

  ngOnInit(): void {

    this.configurarAdjuntos()
  }

  configurarAdjuntos() {
    if (!this.configuracion || !this.configuracion.comentarios) {
      return
    }
    this.configuracion.configuracionAdjuntos = []
    this.configuracion.comentarios.forEach(item => {
      if (item.comentario.adjuntos && item.comentario.adjuntos.length > 0) {
        this.configuracion.configuracionAdjuntos.push({
          id: item.comentario.id,
          confAdjunto: this.obtenerConfiguracionAudio(
            item.comentario.adjuntos,
            item.comentario.coautor.configuraciones,
            item.idPerfilRespuesta
          )
        })
      }
    })
  }

  obtenerConfiguracionPorId(id: string) {
    let conf: ConfiguracionAudioReproductor = {}
    this.configuracion.configuracionAdjuntos.forEach(configuracion => {
      if (configuracion.id === id) {
        conf = configuracion.confAdjunto
      }
    })
    return conf
  }

  obtenerConfiguracionAudio(
    adjuntos: MediaModel[],
    configuraciones: ConfiguracionEstiloModel[],
    idPerfilRespuesta: PerfilModel,
  ): ConfiguracionAudioReproductor {
    const media: MediaModel = adjuntos[0]
    return {
      idInterno: 'audio_' + this.generadorId.generarIdConSemilla(),
      media: media,
      direccion: this.configuracion.esPropietario ? DireccionDelReproductor.HACIA_LA_IZQUIERDA : DireccionDelReproductor.HACIA_LA_DERECHA,
      colorDeFondo: ColorDeFondo.FONDO_AMARILLO_CLARO,
      mostrarLoader: true,
      mostrarTitulo: false,
      colorDeFondoCustom: this.obtenerEstilosParaContendorDelAdjunto(
        configuraciones,
        idPerfilRespuesta
      ),
      esComentarioRespuesta: (idPerfilRespuesta && idPerfilRespuesta !== null),
      anchoCompleto: true,
      preload: true,
      tiempoReproduccion: {
        actual: 0,
        total: parseInt(media.principal.duracion),
        factor: 100 / parseInt(media.principal.duracion)
      }
    }
  }

  obtenerEstilosParaContendorDelAdjunto(
    configuraciones: ConfiguracionEstiloModel[],
    idPerfilRespuesta: PerfilModel
  ) {
    if (idPerfilRespuesta && idPerfilRespuesta !== null) {
      return '#03486b'
    }

    let codigo = ''
    configuraciones.forEach(configuracion => {
      configuracion.estilos.forEach(estilo => {
        if (estilo.tipo.codigo === CodigosCatalogoEstilo.COLOR_DE_FONDO) {
          codigo = estilo.color.codigo
        }
      })
    })

    return codigo
  }

  obtenerEstilosParaContendorDelTexto(
    configuraciones: ConfiguracionEstiloModel[],
    idPerfilRespuesta: PerfilModel
  ) {
    const data = {}

    configuraciones.forEach(configuracion => {
      configuracion.estilos.forEach(estilo => {
        if (estilo.tipo.codigo === CodigosCatalogoEstilo.COLOR_DE_FONDO) {
          data['background-color'] = estilo.color.codigo
        }
      })
    })
    return data
  }

  obtenerFechaEnFormatoDate(fechaActualizacion: string) {
    return new Date(fechaActualizacion)
  }

  eventoEliminarComentario(comentario: ComentarioModel) {
    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(comentario)
    }
  }

  eventoTapEnPerfil() {
    if (this.configuracion.eventoTapEnPerfil) {
      this.configuracion.eventoTapEnPerfil(this.configuracion.coautor._id, this.configuracion.idInterno)
    }
  }

  eventoPressEnPerfil() {
    if (this.configuracion.eventoPress) {
      this.configuracion.eventoPress(this.configuracion.coautor._id)
    }
  }

  configurarTextoDelComentario(comentario: ComentarioModel) {
    try {
      if (!(comentario.traducciones && comentario.traducciones.length > 0)) {
        return ''
      }

      const traducciones = comentario.traducciones

      if (this.configuracion.inglesActivo) {
        const index = traducciones.findIndex(e => e.idioma.codigo === CodigosCatalogoIdiomasPrincipales.INGLES)
        if (index >= 0) {
          return traducciones[index].texto
        }

        return ''
      }

      const idiomaActivo = this.idiomaNegocio.obtenerIdiomaSeleccionado()

      if (!idiomaActivo) {
        return ''
      }

      const indexDos = traducciones.findIndex(e => e.idioma.codigo === idiomaActivo.codigo)
      if (indexDos >= 0) {
        return traducciones[indexDos].texto
      }

      return ''
    } catch (error) {
      return ''
    }
  }

  trackByFn(index: number, item: ComentarioRespuesta) {
    return item.comentario.id
  }

}
