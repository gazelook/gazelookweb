import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { OrigenFoto } from '../../diseno/enums/origen-foto.enum';
import { ConfiguracionModalOrigenFoto } from '../../diseno/modelos/modal-opciones-foto.interface';
@Component({
  selector: 'app-modal-origen-foto',
  templateUrl: './modal-origen-foto.component.html',
  styleUrls: ['./modal-origen-foto.component.scss']
})
export class ModalOrigenFotoComponent implements OnInit {

  @Input() configuracion: ConfiguracionModalOrigenFoto

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void { }

  cerrarModalOrigenFoto(event: any) {
    if (event.target.className.indexOf('modalOrigenFoto') >= 0) {
      this.configuracion.mostrar = false
      this.configuracion.origenFoto = OrigenFoto.SIN_DEFINIR
    }
  }

  eventoTapItemOrigenCamara() {
    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(OrigenFoto.TOMAR_FOTO)
    }
  }

  eventoTapItemOrigenGaleria() {
    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(OrigenFoto.IMAGEN_GALERIA)
    }
  }

  eventoTapVideo(){
    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(OrigenFoto.GRABAR_VIDEO)
    }
  }
}
