import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { ConfiguracionDialogoInline } from '@shared/diseno/modelos';
@Component({
  selector: 'app-dialogo-inline',
  templateUrl: './dialogo-inline.component.html',
  styleUrls: ['./dialogo-inline.component.scss']
})
export class DialogoInlineComponent implements OnInit {

  @Input() configuracion: ConfiguracionDialogoInline

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {}

  ngOnInit(): void {}
}
