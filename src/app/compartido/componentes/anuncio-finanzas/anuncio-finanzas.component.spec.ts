import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnuncioFinanzasComponent } from './anuncio-finanzas.component';

describe('AnuncioFinanzasComponent', () => {
  let component: AnuncioFinanzasComponent;
  let fixture: ComponentFixture<AnuncioFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuncioFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnuncioFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
