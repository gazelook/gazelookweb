import { ConvertidorArchivos } from '@core/util/caster-archivo.service';
import { ConfiguracionCamaraFoto } from '@shared/diseno/modelos/camara-foto.interface';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy, HostListener } from '@angular/core';

@Component({
    selector: 'app-camara-foto',
    templateUrl: './camara-foto.component.html',
    styleUrls: ['./camara-foto.component.scss']
})
export class CamaraFotoComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() configuracion: ConfiguracionCamaraFoto
    
    WIDTH = window.innerWidth;
    HEIGHT = Math.round(window.innerWidth / 1.40);
    // WIDTH1 = window.screen.width;

    @ViewChild("videoCap", { static: false }) videoCap: ElementRef;
    @ViewChild("canvas", { static: false }) canvas: ElementRef;
    camaraPrincipal: boolean

    // @ViewChild("videoCap1", { static: false }) videoCap1: ElementRef;
    // @ViewChild("canvas1", { static: false }) canvas1: ElementRef;
    // stream1: any

    stream: any
    captures: string[] = [];
    error: any;
    isCaptured: boolean;

    camConf = { video: { facingMode: "enviroment", width: window.innerWidth, height: Math.round(window.innerWidth / 1.40) }, audio: false }
    constructor(
        private convertidorArchivos: ConvertidorArchivos,
    ) { 
        this.camaraPrincipal = true
    }

    @HostListener('unloaded')
    ngOnDestroy(): void {

    }
    async ngOnInit() {
      

    }

    ngAfterContentInit() {

    }
    async ngAfterViewInit() {
        await this.setupDevices(this.camaraPrincipal);
    }

    async setupDevices(camaraPrincipal: boolean) {
        let facing: string = ''
        if (camaraPrincipal) {
            facing = 'environment'
        } else {
            facing = 'user'
        }

        this.camConf.video.facingMode = facing
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia || navigator.mediaDevices.enumerateDevices) {
            try {
                this.stream = await navigator.mediaDevices.getUserMedia(this.camConf);
                if (this.stream) {
                    this.videoCap.nativeElement.srcObject = this.stream;
                    this.videoCap.nativeElement.play();
                    this.error = null;
                } else {
                    this.error = "You have no output video device";
                }

            } catch (e) {

                this.error = e;
            }
        }
    }

    capture() {

        this.drawImageToCanvas(this.videoCap.nativeElement);
        this.captures.push(this.canvas.nativeElement.toDataURL("image/jpeg"));
        // this.isCaptured = true;

       
        // let a = document.createElement('a');
        // a.href = URL.createObjectURL( this.convertidorArchivos.dataURItoBlob( this.captures[0]));
        // a.download = 'screenshot.jpg';
        // document.body.appendChild(a);
        // a.click();
        // this.videoCap.nativeElement.stop()
        this.stream.stop()
        this.configuracion.fotoRealizada(this.captures)
        // this.ngOnDestroy()

    }

    async cambiarCamara() {
        this.stream.stop()
        this.camaraPrincipal = !this.camaraPrincipal
        await this.setupDevices(this.camaraPrincipal)
    }

    cerrarCamara() {
        this.stream.stop()
        this.configuracion.cerrarCamara()
    }

    removeCurrent() {
        this.isCaptured = false;
    }

    setPhoto(idx: number) {
        this.isCaptured = true;
        var image = new Image();
        image.src = this.captures[idx];
        this.drawImageToCanvas(image);
    }

    drawImageToCanvas(image: any) {

        this.canvas.nativeElement
            .getContext("2d")
            .drawImage(image, 0, 0, this.HEIGHT, this.WIDTH);
    }

}
