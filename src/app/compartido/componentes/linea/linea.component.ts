import { Component, Input, OnInit } from '@angular/core'
import { AnchoLineaItem } from '@shared/diseno/enums'
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums'
import { LineaCompartida } from '@shared/diseno/modelos'
@Component({
  selector: 'app-linea',
  templateUrl: './linea.component.html',
  styleUrls: ['./linea.component.scss']
})
export class LineaComponent implements OnInit {

  @Input() configuracionLinea: LineaCompartida

  constructor() { }

  ngOnInit(): void {
    if (!this.configuracionLinea) {
      this.inicializarConfiguracionPorDefecto()
    }
  }

  // En caso de que la linea no reciba configuracion alguna, se inicializa por defecto
  inicializarConfiguracionPorDefecto() {
    this.configuracionLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: true
    }
  }

  // Devuelve las clases de la linea segun la configuracion
  obtenerClasesLinea() {
    const clases = {}
    clases[this.configuracionLinea.ancho.toString()] = true
    clases[this.configuracionLinea.espesor.toString()] = true
    clases[this.configuracionLinea.colorFondo.toString()] = true
    clases['forzarAlFinal'] = (this.configuracionLinea.forzarAlFinal) ? this.configuracionLinea.forzarAlFinal : false
    clases['cajaGaze'] = (this.configuracionLinea.cajaGaze) ? this.configuracionLinea.cajaGaze : false
    clases['zindex'] = true
    // clases['position'] = true
    return clases
  }
}

