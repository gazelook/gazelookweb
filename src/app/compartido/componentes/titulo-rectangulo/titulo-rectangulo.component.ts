import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { Component, OnInit, Input } from '@angular/core'
@Component({
  selector: 'app-titulo-rectangulo',
  templateUrl: './titulo-rectangulo.component.html',
  styleUrls: ['./titulo-rectangulo.component.scss']
})
export class TituloRectanguloComponent implements OnInit {

  /*
    Especificaciones generales,

    - Antes de diubjar el item, se debe configurar segun el uso que se le vaya a dar: enum UsoItemRectangular
    - La configuracion debe ser almacenada en una variable de tipo modelo ItemRectangularCompartido y asignada al paremetro @Input() configuracion
    - Las acciones que se disparan en el item segun el tipo de evento, estan catalogadas por el enum AccionesItemCirRec
  */

  @Input() configuracion: CongifuracionTituloRectangulo // Configuracion del item, para dibujar el item los valores de dataItemCRCompartido deben ser establecidos

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
  ) { }

  ngOnInit(): void {
    if (this.configuracion.textoBoton1) {
      this.configuracion.textoBoton1 = (this.configuracion.textoBoton1.length > 30) ? this.configuracion.textoBoton1.substring(0, 30) + '...' : this.configuracion.textoBoton1
    }

    if (this.configuracion.textoBoton2) {
      this.configuracion.textoBoton2 = (this.configuracion.textoBoton2.length > 30) ? this.configuracion.textoBoton2.substring(0, 30) + '...' : this.configuracion.textoBoton2
    }
   }

  obtenerClasesTitulo() {
    const clases = {}
    if (this.configuracion.colorDeFondo) {
      clases[this.configuracion.colorDeFondo.toString()] = true
    }
    clases['titulo'] = true
    return clases
  }
}
