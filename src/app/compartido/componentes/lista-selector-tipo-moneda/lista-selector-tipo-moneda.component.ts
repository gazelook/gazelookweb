import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ItemSelector } from '@shared/diseno/modelos';
import { InfoAccionSelector } from '@shared/diseno/modelos';
import { ConfiguracionMonedaPicker } from '@shared/diseno/modelos';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { MonedaPickerService } from 'src/app/nucleo/servicios/generales/moneda-picker.service';
import { AccionesSelector } from '@shared/diseno/enums';
@Component({
  selector: 'app-lista-selector-tipo-moneda',
  templateUrl: './lista-selector-tipo-moneda.component.html',
  styleUrls: ['./lista-selector-tipo-moneda.component.scss']
})
export class ListaSelectorTipoMonedaComponent implements OnInit, AfterViewInit {

  @Input() configuracion: ConfiguracionMonedaPicker
  @ViewChild('inputBuscador', { static: false }) inputBuscador: ElementRef

  public barraBusqueda$: Observable<string>
  public idInput: string

  public elegibles: ItemSelector[]
  public listaBase: ItemSelector[]
  public todasMonedas: ItemSelector[]
  public buscar: string

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public monedaPickerService: MonedaPickerService
  ) {
    this.idInput = 'inputBusquedaMoneda'
    this.buscar = ''
    this.elegibles = []
    this.listaBase = []
    this.todasMonedas = []
  }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.listaBase = this.configuracion.selectorTipoMoneda.elegibles
    this.configurarObservable()
  }

  configurarObservable() {
    this.barraBusqueda$ = fromEvent<any>(this.inputBuscador.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        debounceTime(300),
      )

    this.barraBusqueda$.subscribe(query => {
      if (!query || query.length === 0) {
        this.configuracion.selectorTipoMoneda.evento({
          accion: AccionesSelector.ABRIR_SELECTOR
        })

      }

      this.elegibles = this.configuracion.selectorTipoMoneda.elegibles
      if (this.todasMonedas.length === 0) {
        this.todasMonedas = this.configuracion.selectorTipoMoneda.elegibles
      }
      const coincidencias = this.encontrarCoincidencias(query)
      this.configuracion.selectorTipoMoneda.elegibles = coincidencias
    })
  }

  buscador() {

  }

  encontrarCoincidencias(buscar: string): ItemSelector[] {
    let query = buscar.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    try {
      return this.todasMonedas.filter(place => {
        if (place.nombre.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(query)) {
          return place && place.nombre
        }
      })
    } catch (error) {
      return []
    }
  }

  obtenerClasesInputCantidadMoneda(): any {
    const clases = {}
    clases['input-cantidad'] = true
    clases[this.configuracion.inputCantidadMoneda.colorFondo.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.colorTexto.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.estiloDelTexto.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.tamanoDelTexto.toString()] = true

    if (this.configuracion.inputCantidadMoneda.estiloBorde) {
      clases[this.configuracion.inputCantidadMoneda.estiloBorde.toString()] = true
    }

    return clases
  }

  obtenerClasesInputPreviewSelector() {
    const clases = {}
    clases['input-preview-selector'] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.colorFondo.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.colorTexto.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloDelTexto.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.tamanoDelTexto.toString()] = true

    if (this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde) {
      clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde.color.toString()] = true
      clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde.espesor.toString()] = true
    }

    return clases
  }

  tapReintentar() {
    if (this.configuracion.selectorTipoMoneda.evento) {
      const accion: InfoAccionSelector = {
        accion: AccionesSelector.REINTERTAR_CONTENIDO
      }
      this.configuracion.selectorTipoMoneda.evento(accion)
    }
  }

  eventoModal(target: any) {
    target.classList.forEach((clase: any) => {
      if (clase === 'selector-moneda') {
        this.configuracion.selectorTipoMoneda.error.mostrar = false
        this.configuracion.selectorTipoMoneda.error.llaveTexto = ''
        this.configuracion.selectorTipoMoneda.mostrarLoader = false
        this.configuracion.selectorTipoMoneda.mostrarSelector = false
        return
      }
    })
  }

  tapEnElegible(item: ItemSelector) {
    if (this.configuracion.selectorTipoMoneda.evento) {
      const accion: InfoAccionSelector = {
        accion: AccionesSelector.SELECCIONAR_ITEM,
        informacion: item
      }
      this.configuracion.selectorTipoMoneda.evento(accion)
    }
  }
}
