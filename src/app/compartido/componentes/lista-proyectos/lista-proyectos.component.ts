import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { TamanoLista } from '@shared/diseno/enums';
import { DatosLista } from '@shared/diseno/modelos';
import { ProyectoEntity } from 'src/app/dominio/entidades/proyecto.entity';
import { MediaNegocio } from 'dominio/logica-negocio';
import { ArchivoModel } from 'dominio/modelo/entidades';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos';
import { AnchoLineaItem } from '@shared/diseno/enums';
import {
  ColorDeBorde,
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums';
import { LineaCompartida } from '@shared/diseno/modelos';
import { ConfiguracionListaProyectos } from '../../diseno/modelos/lista-proyectos.interface';
import { ConfiguracionPortadaExandida } from '@shared/diseno/modelos';
import { NoticiaNegocio } from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import { FuncionesCompartidas } from '@core/util';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos';
@Component({
  selector: 'app-lista-proyectos',
  templateUrl: './lista-proyectos.component.html',
  styleUrls: ['./lista-proyectos.component.scss']
})
export class ListaProyectosComponent implements OnInit, OnChanges {

  @Input() configuracion: ConfiguracionListaProyectos;
  public listaProyectos: DatosLista;
  public archivosPorDefecto: Array<ArchivoModel>;
  public archivoPorDefectoNoticia: ArchivoModel;
  public placeholderNoticia: CongifuracionItemProyectosNoticias;
  public util = FuncionesCompartidas;
  public mostrarlistaProyectos = false;
  public paginacionProyectos: PaginacionModel<ProyectoEntity>;
  public error: string;
  public ultimaPagina = false;
  public confPortadaExpandida: ConfiguracionPortadaExandida;
  public confLinea: LineaCompartida;
  public puedeCargarMas = true;

  constructor(public estiloTexto: EstiloDelTextoServicio,
    public mediaNegocio: MediaNegocio,
    private noticiaNegocio: NoticiaNegocio) {
    this.archivosPorDefecto = [];
    this.paginacionProyectos = {
      paginaActual: 0,
      proximaPagina: true,
    };
  }

  ngOnInit(): void {
    this.obtenerArchivosPorDefecto();
    this.configurarPlaceholders();
    this.configurarLinea();


  }
  ngOnChanges() {}

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    }
  }

  cargarNoticias() {
    this.listaProyectos = {
      tamanoLista: TamanoLista.LISTA_CONTACTOS,
      lista: [],
      cargarMas: () => this.configuracionNoticias(),
      cargando: false,
    };
  }

  cargarMas() {

    let element = document.getElementById('scroll');

    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
      this.puedeCargarMas = false;
      if (this.configuracion.eventoScroll) {
        this.configuracion.eventoScroll();
      }
    }
  }

  configuracionNoticias(proyectos?) {
    this.listaProyectos.lista = [];
    if (proyectos) {
      this.configuracion.proyectos = proyectos;
    }
    for (const noticia of this.configuracion.proyectos) {
      this.listaProyectos.lista.push(this.configNoticia(noticia));
    }
  }

  configNoticia(noticia): CongifuracionItemProyectosNoticias {
    return {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: noticia.traducciones[0].tituloCorto,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: noticia.fechaActualizacion,
          formato: 'dd/MM/yyyy',
        },
      },
      loader: true,
      urlMedia: noticia.adjuntos[0]?.portada.principal.url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {

        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: noticia._id,
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
    };
  }

  obtenerArchivosPorDefecto() {
    this.mediaNegocio.obtenerListaArchivosDefault().subscribe((archivos) => {
      for (const archivo of archivos) {
        if (
          archivo.catalogoArchivoDefault ===
          CodigosCatalogoArchivosPorDefecto.PROYECTOS
        ) {
          this.archivosPorDefecto.push(archivo);
        }
      }

      this.archivoPorDefectoNoticia = this.util.randomItem(
        this.archivosPorDefecto
      );

      if (this.placeholderNoticia) {
        this.placeholderNoticia.loader = true;
        this.placeholderNoticia.urlMedia = this.archivoPorDefectoNoticia.url;
      }
      (error) => {
        if (this.placeholderNoticia) {
          this.placeholderNoticia.loader = false;
        }
      };
    });
  }

  configurarPlaceholders() {
    this.placeholderNoticia = {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: 'm3v1texto15',
          textoBoton2: 'm3v1texto16',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: { fecha: new Date(), formato: 'dd/MM/yyyy' },
      },
      loader: true,
      urlMedia: '',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
    };
  }
}
