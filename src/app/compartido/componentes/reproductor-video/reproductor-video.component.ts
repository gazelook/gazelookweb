import { Component, Input, OnInit } from '@angular/core';
import { ReproductorVideo } from '../../diseno/modelos/reproductor-video.interface';

@Component({
  selector: 'app-reproductor-video',
  templateUrl: './reproductor-video.component.html',
  styleUrls: ['./reproductor-video.component.scss']
})
export class ReproductorVideoComponent implements OnInit {

  @Input() configuracion: ReproductorVideo;

  urlVideo: string

  constructor() { }

  ngOnInit(): void {
    this.urlVideo = this.configuracion.url
    this.pausarVideo();
  }

  pausarVideo() {
    if(this.configuracion.mostrar) {
      document.querySelector('video').pause();
    }
  }

}
