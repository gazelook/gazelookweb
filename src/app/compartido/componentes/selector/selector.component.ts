import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { AccionesSelector } from '@shared/diseno/enums';
import { ConfiguracionSelector } from '@shared/diseno/modelos';
@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {
  @Input() configuracion: ConfiguracionSelector

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
  ) { }

  ngOnInit() { }

  ngAfterViewInit(): void { }

  clickInputSelector() {
    if (this.configuracion.evento) {
      this.configuracion.evento({
        accion: AccionesSelector.ABRIR_SELECTOR
      })
    }
  }
}
