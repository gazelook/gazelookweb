import { Location } from '@angular/common';
import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { MetodosSessionStorageService } from '@env/src/app/nucleo/util/metodos-session-storage.service';
import { BuscadorComponent } from '@shared/componentes/buscador/buscador.component';
import {
  ButtonComponent,
  ColorTextoBoton,
  TipoBoton
} from '@shared/componentes/button/button.component';
import { ToastComponent } from '@shared/componentes';

import {
  AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoDeTextoConInterlineado, TipoDialogo, UsoAppBar
} from '@shared/diseno/enums';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos';
import { BotonCompartido } from '@shared/diseno/modelos';
import { DialogoCompartido } from '@shared/diseno/modelos';
import { LineaCompartida } from '@shared/diseno/modelos';
import { ConfiguracionToast } from '@shared/diseno/modelos';
import { CuentaNegocio } from 'dominio/logica-negocio';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { RutasLocales } from 'src/app/rutas-locales.enum';

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.scss'],
})
export class AppbarComponent implements OnInit, OnChanges {
  @ViewChild('buscador', { static: false }) public buscador: BuscadorComponent;
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @Input() configuracion: ConfiguracionAppbarCompartida;

  public usoAppBar = UsoAppBar;
  public textoNombrePerfil: string;
  public textoTituloPrincipal: string;
  public textoSubtitulo: string;
  public textoSubtituloDemo: string;
  public textoSubtituloNormal: string;
  public textoBack: string;
  public textoHome: string;
  public textoElegirPerfil: string;

  public confBoton: BotonCompartido;
  public confLinea: LineaCompartida;

  public datosDialogoCerrarSession: DialogoCompartido;
  public confToast: ConfiguracionToast;

  constructor(
    private servicioIdiomas: InternacionalizacionNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private router: Router,
    private auth: AngularFireAuth,
    private metodosSessionStorageService: MetodosSessionStorageService
  ) {
    this.textoNombrePerfil = '';
    this.textoTituloPrincipal = '';
    this.textoSubtitulo = '';
    this.textoSubtituloDemo = '';
    this.textoBack = '';
    this.textoHome = '';
    this.textoElegirPerfil = '';
  }

  ngOnInit(): void {
    if (this.configuracion) {
      this.configurarToast();
      this.inicializarTextos();
      if (this.configuracion.usoAppBar === UsoAppBar.USO_DEMO_APPBAR) {
        this.configurarBotonPrincipal();
      }
      this.configurarLinea();
    }
    this.prepararDialogoCerrarSession();
  }

  ngOnChanges() {
    if (this.configuracion) {
      this.inicializarTextos();
      if (this.configuracion.usoAppBar === UsoAppBar.USO_DEMO_APPBAR) {
        this.configurarBotonPrincipal();
      }
      this.configurarLinea();
    }
  }

  // Inicializar textos
  async inicializarTextos() {
    if (this.configuracion.demoAppbar) {
      const conf = this.configuracion.demoAppbar;
      this.textoNombrePerfil = await this.servicioIdiomas.obtenerTextoLlave(
        conf.nombrePerfil.llaveTexto
          ? conf.nombrePerfil.llaveTexto
          : 'undefined'
      );
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(

        
        conf.subtitulo?.llaveTexto ? conf.subtitulo.llaveTexto : 'undefined'
      );
 
    }

    if (this.configuracion.searchBarAppBar) {
      const conf = this.configuracion.searchBarAppBar;
      this.textoNombrePerfil = await this.servicioIdiomas.obtenerTextoLlave(
        conf.nombrePerfil.llaveTexto
          ? conf.nombrePerfil.llaveTexto
          : 'undefined'
      );
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(
        conf.subtitulo.llaveTexto ? conf.subtitulo.llaveTexto : 'undefined'
      );
      this.textoBack = await this.servicioIdiomas.obtenerTextoLlave(
        'm2v13texto1'
      );
      this.textoHome = await this.servicioIdiomas.obtenerTextoLlave(
        'm2v13texto2'
      );
    }

    if (this.configuracion.gazeAppBar) {
      const conf = this.configuracion.gazeAppBar;
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(
        conf.subtituloNormal?.llaveTexto
          ? conf.subtituloNormal.llaveTexto
          : 'undefined'
      );
      this.textoSubtituloDemo = await this.servicioIdiomas.obtenerTextoLlave(
        conf.subtituloDemo?.llaveTexto
          ? conf.subtituloDemo.llaveTexto
          : 'undefined'
      );
      this.textoTituloPrincipal = await this.servicioIdiomas.obtenerTextoLlave(
        conf.tituloPrincipal.llaveTexto
          ? conf.tituloPrincipal.llaveTexto
          : 'undefined'
      );
      this.textoElegirPerfil = await this.servicioIdiomas.obtenerTextoLlave(
        conf.textoElegirPerfil.llaveTexto
          ? conf.textoElegirPerfil.llaveTexto
          : 'undefined'
      );
    }

    if (this.configuracion.tituloAppbar) {
      this.textoTituloPrincipal = await this.servicioIdiomas.obtenerTextoLlave(
        this.configuracion.tituloAppbar.tituloPrincipal.llaveTexto
      );
    }
  }

  // Configurar boton principal
  async configurarBotonPrincipal() {
    if (this.configuracion.demoAppbar.boton) {
      //Se puede recibir un boton custom desde fuera
    } else {
      this.confBoton = {
        text: 'm1v4texto1',
        tipoBoton: TipoBoton.TEXTO,
        tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
        colorTexto: ColorTextoBoton.AMARRILLO,
        enProgreso: false,
        ejecutar: () => {
          this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO);
        },
      };
    }
    //this.confBoton.text = await this.servicioIdiomas.obtenerTextoLlave(this.configuracion.demoAppbar.boton.llaveTexto ? this.configuracion.demoAppbar.boton.llaveTexto : 'undefined')
  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false,
    };
  }

  // Devuelve las clases que definen los estilos del appbar
  obtenerClasesAppBar() {
    const clases = {};
    clases['appbar'] = true;
    clases[this.configuracion.usoAppBar.toString()] = true;
    clases['color-fondo-principal'] = true;
    return clases;
  }

  // Devuelve las clases para el color de fondo
  obtenerClasesCapaColorFondo(configuracion: any) {
    const clases = {};
    clases['colorFondo'] = true;
    clases[configuracion.toString()] = true;
    clases['con-color'] = this.configuracion.conColorSecundario;
    return clases;
  }

  mostrarOriginal() {
    if (
      this.configuracion.searchBarAppBar.idiomaOriginal.clickMostrarOriginal
    ) {
      this.configuracion.searchBarAppBar.idiomaOriginal.clickMostrarOriginal();
    }
  }
  // Eventos de click
  clickBotonAtras() {
    if (this.configuracion.accionAtras) {
      this.configuracion.accionAtras();
    } else {
      this._location.back();
    }
  }
  clickBotonXRoja() {
    this.datosDialogoCerrarSession.mostrarDialogo = true;
  }
  clickBotonPrincipal() {}

  clickBotonCasaHome() {
    if (this.configuracion.demoAppbar.accionCasaHome) {
      this.configuracion.demoAppbar.accionCasaHome();
    }
  }

  clickHome() {
    if (this.configuracion.eventoHome) {
      this.configuracion.eventoHome();
      return;
    }

    this.metodosSessionStorageService.eliminarSessionStorage();
    this.router.navigateByUrl(RutasLocales.MENU_PRINCIPAL);
  }

  clickRefresh() {
    if (this.configuracion.eventoRefresh) {
      this.configuracion.eventoRefresh();
      return;
    }
  }

  prepararDialogoCerrarSession() {
    let textoDescripcion = '';

    if (this.cuentaNegocio.sesionIniciada()) {
      textoDescripcion = 'm2v11texto27';
    } else {
      textoDescripcion = 'm1v3texto4';
    }

    this.datosDialogoCerrarSession = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: textoDescripcion,
      listaAcciones: [
        ButtonComponent.crearBotonAfirmativo(() => this.cerrarSession()),
        ButtonComponent.crearBotonNegativo(() => {
          this.datosDialogoCerrarSession.mostrarDialogo = false;
        }),
      ],
    };
  }

  async cerrarSession() {
    try {
      this.datosDialogoCerrarSession.mostrarDialogo = false;
      this.toast.abrirToast('', true);

      const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
      const respuesta = await this.cuentaNegocio
        .cerrarSessionEnElApi(usuario)
        .toPromise();

      if (respuesta !== 200 && respuesta !== 201) {
        throw new Error('');
      }

      await this.auth.signOut();

      this.cuentaNegocio.cerrarSession();
      this._location.replaceState('/');
      this.router.navigateByUrl(RutasLocales.BASE, { replaceUrl: true });
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  clickTituloPrincipal() {
    switch (this.configuracion.usoAppBar) {
      case UsoAppBar.USO_DEMO_APPBAR:
        break;
      case UsoAppBar.USO_SEARCHBAR_APPBAR:
        break;
      case UsoAppBar.USO_GAZE_BAR:
        if (this.configuracion.gazeAppBar.clickTituloPrincipal) {
          this.configuracion.gazeAppBar.clickTituloPrincipal();
        }
        break;
      case UsoAppBar.SOLO_TITULO:
        break;
      case UsoAppBar.USO_ELEGIR_PERFIL:
        if (this.configuracion.gazeAppBar.clickElegirOtroPerfil) {
          this.configuracion.gazeAppBar.clickTituloPrincipal();
        }
        break;
      default:
        break;
    }
  }

  clickTituloElegPer() {
    if (this.configuracion.gazeAppBar.clickElegirOtroPerfil) {
      this.configuracion.gazeAppBar.clickElegirOtroPerfil();
    }
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    };
  }
}
