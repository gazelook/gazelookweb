import { Component, HostListener, Input, OnInit } from '@angular/core'
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam'
import { Observable, Subject } from 'rxjs'
import { ConfiguracionCamara } from '../../diseno/modelos/foto-editor.interface'
import { EstiloDelTextoServicio } from '@core/servicios/diseno'

@Component({
  selector: 'app-camara',
  templateUrl: './camara.component.html',
  styleUrls: ['./camara.component.scss']
})
export class CamaraComponent implements OnInit {

  @Input() configuracion: ConfiguracionCamara

  private trigger: Subject<void>
  private switchCamera: Subject<boolean | string>
  private nextWebcam: Subject<boolean | string>
  public showWebcam = true
  public allowCameraSwitch = true
  public multipleWebcamsAvailable = false
  public deviceId: string
  public errors: WebcamInitError[] = []
  public webcamImage: WebcamImage

  public anchoCamara: number
  public altoCamara: number

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {
    this.onResize()
    this.trigger = new Subject<void>()
    this.switchCamera = new Subject<boolean | string>()
    this.nextWebcam = new Subject<boolean | string>()
    this.webcamImage = null
    this.allowCameraSwitch = false
    this.multipleWebcamsAvailable = false
  }

  ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1
      })
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?: Event) {
    const win = !!event ? (event.target as Window) : window
    this.anchoCamara = win.innerWidth
    this.altoCamara = win.innerHeight
  }

  public triggerSnapshot(): void {
    this.trigger.next()
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error)
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    this.nextWebcam.next(directionOrDeviceId)
  }

  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage

    if (this.configuracion.fotoCapturada) {
      this.configuracion.fotoCapturada(webcamImage)
      return
    }

    this.configuracion.mostrarLoader = false
  }

  public cameraWasSwitched(deviceId: string): void {
    this.deviceId = deviceId
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable()
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable()
  }

  // Cerrar Camara
  cerrarCamara() {
    this.configuracion.mostrarModal = false
  }

  // Error en camara
  errorEnCamara(error: WebcamInitError): void {
    if (error) {
      if (error.mediaStreamError && error.mediaStreamError.name === "NotAllowedError") {
        this.configuracion.mensajeError.contenido = "text75"
        this.configuracion.mensajeError.mostrarError = true
        return
      }
      this.configuracion.mensajeError.contenido = error.message
      this.configuracion.mensajeError.mostrarError = true
      return
    }
  }

}
