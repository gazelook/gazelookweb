import { Component, OnInit } from '@angular/core';
import { VariablesGlobales } from '@core/servicios/generales';
@Component({
	selector: 'app-imagen-mundo',
	templateUrl: './imagen-mundo.component.html',
	styleUrls: ['./imagen-mundo.component.scss']
})
export class ImagenMundoComponent implements OnInit {

	constructor(
		public variablesGlobales: VariablesGlobales
	) { }

	ngOnInit(): void {}
}
