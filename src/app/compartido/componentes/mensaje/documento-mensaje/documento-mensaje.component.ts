import { TipoMensaje } from '../../../diseno/enums/mensaje.enum';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentoMensajeCompartido, MensajeAccion } from 'src/app/compartido/diseno/modelos/mensaje.interface';

@Component({
  selector: 'app-documento-mensaje',
  templateUrl: './documento-mensaje.component.html',
  styleUrls: ['./documento-mensaje.component.scss']
})
export class DocumentoMensajeComponent implements OnInit {
  @Input() configuracion: DocumentoMensajeCompartido

  constructor(
    private sanitizer: DomSanitizer,
    public estiloTexto: EstiloDelTextoServicio,
  ) { }

  ngOnInit(): void { }

  obtenerUrlSaniti(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url)
  }

  eventoTap(id:string, url: string) {
    let  mensajeAccion: MensajeAccion = {
      id:id,
      url: url,
      tipoMensaje: TipoMensaje.DOCUMENTO
    }

    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(mensajeAccion)
    }
  }
}
