import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagenMensajeComponent } from './imagen-mensaje.component';

describe('ImagenMensajeComponent', () => {
  let component: ImagenMensajeComponent;
  let fixture: ComponentFixture<ImagenMensajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagenMensajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagenMensajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
