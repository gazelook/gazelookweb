import { Component, Input, OnInit } from '@angular/core';
import { ImagenMensajeCompartido } from 'src/app/compartido/diseno/modelos/mensaje.interface';
@Component({
  selector: 'app-imagen-mensaje',
  templateUrl: './imagen-mensaje.component.html',
  styleUrls: ['./imagen-mensaje.component.scss']
})
export class ImagenMensajeComponent implements OnInit {
  @Input() configuracion: ImagenMensajeCompartido
  constructor() { }

  ngOnInit(): void { }

  siImagenEstaCargada(evt: any) {

    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarLoader = false
    }
  }
}
