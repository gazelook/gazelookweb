import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioMensajeComponent } from './audio-mensaje.component';

describe('AudioMensajeComponent', () => {
  let component: AudioMensajeComponent;
  let fixture: ComponentFixture<AudioMensajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioMensajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioMensajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
