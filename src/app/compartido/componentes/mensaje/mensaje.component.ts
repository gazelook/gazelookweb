import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos';
import { PerfilNegocio } from 'dominio/logica-negocio';
import { PerfilModel } from 'dominio/modelo/entidades';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorDeBorde,
  ColorDeFondo
} from '@shared/diseno/enums';
import { TipoMensaje } from '../../diseno/enums/mensaje.enum';
import {
  DocumentoMensajeCompartido, ImagenMensajeCompartido,
  TextoMensajeCompartido
} from '../../diseno/modelos/mensaje.interface';
import { MediaEntity } from './../../../dominio/entidades/media.entity';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  CatalogoEstatusMensaje, CatalogoTipoMensaje
} from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogoTipoArchivo } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-archivo.enum';
import { RutasNoticias } from './../../../presentacion/noticias/rutas-noticias.enum';
import { RutasProyectos } from './../../../presentacion/proyectos/rutas-proyectos.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums';
import { BotonCompartido } from '@shared/diseno/modelos';
import {
  AudioMensajeCompartido, ConfiguracionMensaje, MensajeAccion, MensajeEntidadCompartido
} from './../../diseno/modelos/mensaje.interface';
import {
  ColorTextoBoton, TipoBoton
} from '@shared/componentes';

@Component({
  selector: 'app-mensaje',
  templateUrl: './mensaje.component.html',
  styleUrls: ['./mensaje.component.scss'],
})
export class MensajeComponent implements OnInit {
  @Input() configuracion: ConfiguracionMensaje;
  public configuracionMensajeTexto: TextoMensajeCompartido;
  public mensajeProyectoCompartido: MensajeEntidadCompartido;
  public configuracionMensajeImagen: ImagenMensajeCompartido;
  public configuracionMensajeDocumento: DocumentoMensajeCompartido;
  public configuracionMensajeAudio: AudioMensajeCompartido;
  public CatalogoTipoMensajeEnum = CatalogoTipoMensaje;
  public CatalogoEstatusMensajeEnum = CatalogoEstatusMensaje;
  public perfilSeleccionado: PerfilModel;

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
    private perfilNegocio: PerfilNegocio,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.obtenerPerfilSeleccionado();
    this.configuracion.listaConfiguracionAudios = [];
    this.configuracion.listaConfiguracionDocumentos = [];
    this.configuracion.listaConfiguracionImagenes = [];
    this.configuracion.listaConfiguracionMensajeCompartidoProyecto = [];
    this.configuracion.listaConfiguracionMensajeCompartidoNoticia = [];

    if (
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.TEXTO ||
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.VIDEOLLAMADA ||
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.LLAMADA
    ) {
      this.configuracionMensajeTexto = this.configuracion;
    }

    if (
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.ARCHIVO ||
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.AUDIO ||
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.IMAGEN
    ) {
      if (
        this.configuracion.adjuntos &&
        this.configuracion.adjuntos.length > 0
      ) {
        for (const adjunto of this.configuracion.adjuntos) {
          switch (adjunto.principal.tipo.codigo as CodigosCatalogoTipoArchivo) {
            case CodigosCatalogoTipoArchivo.IMAGEN:
              if (this.configuracion.mostrar) {
                this.configuracion.listaConfiguracionImagenes.push(
                  this.configurarMensajeImagen(adjunto)
                );
              }
              break;
            case CodigosCatalogoTipoArchivo.AUDIO:
              if (this.configuracion.mostrar) {
                this.configuracion.listaConfiguracionAudios.push(
                  this.configurarMensajeAudio(adjunto)
                );
              }
              break;
            case CodigosCatalogoTipoArchivo.FILES:
              if (this.configuracion.mostrar) {
                this.configuracion.listaConfiguracionDocumentos.push(
                  this.configurarMensajeDocumento(adjunto)
                );
              }
              break;
            default:
              break;
          }
        }
      }
    }

    if (
      this.configuracion.tipoMensaje ===
        CatalogoTipoMensaje.TRANSFERIR_PROYECTO ||
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.COMPARTIR_PROYECTO
    ) {
      this.configuracion.listaConfiguracionMensajeCompartidoProyecto.push(
        this.configurarMensajeProyecto(
          this.configuracion.referenciaEntidadCompartida
        )
      );
    }

    // if (
    //   this.configuracion.tipoMensaje === CatalogoTipoMensaje.TRANSFERIR_PROYECTO
    // ) {
    //   this.configuracion.listaConfiguracionMensajeCompartidoNoticia.push(this.configurarMensajeProyecto(this.configuracion.referenciaEntidadCompartida))
    // }

    if (
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.COMPARTIR_NOTICIA
    ) {
      this.configuracion.listaConfiguracionMensajeCompartidoNoticia.push(
        this.configurarMensajeNoticia(
          this.configuracion.referenciaEntidadCompartida
        )
      );
    }
  }

  obtenerPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarMensajeImagen(adjunto: MediaEntity): ImagenMensajeCompartido {
    let copiaConfiguracion = this.configuracion;
    delete copiaConfiguracion['adjuntos'];
    return {
      ...copiaConfiguracion,
      mostrarLoader: true,
      adjunto: adjunto,
    };
  }
  configurarMensajeDocumento(adjunto: MediaEntity): DocumentoMensajeCompartido {
    let copiaConfiguracion = this.configuracion;
    delete copiaConfiguracion['adjuntos'];
    return {
      ...copiaConfiguracion,
      adjunto: adjunto,
    };
  }
  configurarMensajeAudio(adjunto: MediaEntity): AudioMensajeCompartido {
    let copiaConfiguracion = this.configuracion;
    delete copiaConfiguracion['adjuntos'];
    return {
      ...copiaConfiguracion,
      adjunto: adjunto,
    };
  }

  configurarMensajeProyecto(infoProyecto: any): MensajeEntidadCompartido {
    let copiaConfiguracion = this.configuracion;
    let info = '';
    let acciones: Array<BotonCompartido> = [];
    if (
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.TRANSFERIR_PROYECTO
    ) {
      if (
        this.configuracion.estatusMensaje === CatalogoEstatusMensaje.ENVIADO &&
        this.configuracion.propietario.perfil._id !==
          this.perfilSeleccionado._id
      ) {
        acciones = [
          {
            ejecutar: () => {
              let mensajeAccion: MensajeAccion = {
                id: this.configuracion.id,
                aceptarTransferPro: true,
                tipoMensaje: TipoMensaje.PROYECTO,
              };
              if (this.configuracion.eventoTap) {
                this.configuracion.eventoTap(mensajeAccion);
              }
            },
            text: 'ACCEPT TRANSFER',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.AMARRILLO,
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
          {
            ejecutar: () => {
              let mensajeAccion: MensajeAccion = {
                id: this.configuracion.id,
                aceptarTransferPro: false,
                tipoMensaje: TipoMensaje.PROYECTO,
              };
              if (this.configuracion.eventoTap) {
                this.configuracion.eventoTap(mensajeAccion);
              }
            },
            text: 'DECLINE TRANSFER',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.ROJO,
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
        ];
      }
    }

    if (
      this.configuracion.tipoMensaje === CatalogoTipoMensaje.TRANSFERIR_PROYECTO
    ) {
      if (
        this.configuracion.estatusMensaje === CatalogoEstatusMensaje.ACEPTADO
      ) {
        info = 'ACCEPT TRANSFER';
      } else if (
        this.configuracion.estatusMensaje === CatalogoEstatusMensaje.RECHAZADO
      ) {
        info = 'DECLINE TRANSFER';
      }
    }
    return {
      ...copiaConfiguracion,
      configuracionEntidad: {
        titulo: {
          mostrar: true,
          configuracion: {
            textoBoton1: infoProyecto.traducciones[0].tituloCorto,
            colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
          },
        },
        colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
        etiqueta: { mostrar: false },
        fecha: {
          mostrar: true,
          configuracion: {
            fecha: infoProyecto.fechaActualizacion,
            formato: 'dd/MM/yyyy',
          },
        },
        loader: false,
        urlMedia: infoProyecto.adjuntos[0].portada.principal.url,
        // urlMedia: 'https://images.ctfassets.net/hrltx12pl8hq/2StXTIF5oeiGpSJj8GMRc0/aa3192be223019fff3541692a99b920b/music.jpg?fit=fill&w=480&h=270',
        colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
        eventoTap: {
          activo: true,
          evento: (proyect: CongifuracionItemProyectosNoticias) => {
            let ruta = RutasLocales.MODULO_PROYECTOS.toString();
            let componente = RutasProyectos.VISITAR.toString();
            componente = componente.replace(':id', proyect.id);

            this.router.navigateByUrl(ruta + '/' + componente);
          },
        },
        eventoPress: { activo: false },
        eventoDobleTap: { activo: false },
        id: infoProyecto._id,
        usoItem: UsoItemProyectoNoticia.RECPROYECTO,
        actualizado: infoProyecto.actualizado,
      },
      listaAcciones: acciones,
      infoAccion: info,
    };
  }

  configurarMensajeNoticia(infoNoticia: any): MensajeEntidadCompartido {
    let copiaConfiguracion = this.configuracion;
    let info = '';
    let acciones: Array<BotonCompartido> = [];

    return {
      ...copiaConfiguracion,
      configuracionEntidad: {
        titulo: {
          mostrar: true,
          configuracion: {
            textoBoton1: infoNoticia.traducciones[0].tituloCorto,
            colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
          },
        },
        colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
        etiqueta: { mostrar: false },
        fecha: {
          mostrar: true,
          configuracion: {
            fecha: infoNoticia.fechaActualizacion,
            formato: 'dd/MM/yyyy',
          },
        },
        loader: false,
        urlMedia: infoNoticia.adjuntos[0].portada.principal.url,
        // urlMedia: 'https://images.ctfassets.net/hrltx12pl8hq/2StXTIF5oeiGpSJj8GMRc0/aa3192be223019fff3541692a99b920b/music.jpg?fit=fill&w=480&h=270',
        colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
        eventoTap: {
          activo: true,
          evento: (proyect: CongifuracionItemProyectosNoticias) => {
            let ruta = RutasLocales.MODULO_NOTICIAS.toString();
            let componente = RutasNoticias.VISITAR.toString();
            componente = componente.replace(':id', proyect.id);
            this.router.navigateByUrl(ruta + '/' + componente);
          },
        },
        eventoPress: { activo: false },
        eventoDobleTap: { activo: false },
        id: infoNoticia._id,
        usoItem: UsoItemProyectoNoticia.RECPROYECTO,
        actualizado: infoNoticia.actualizado,
      },
      listaAcciones: acciones,
      infoAccion: info,
    };
  }

  eventoPress(idMensaje: string) {
    let mensajeAccion: MensajeAccion = {
      id: idMensaje,
      tipoMensaje: TipoMensaje.GENERAL,
    };
    if (this.configuracion.eventoPress) {
      this.configuracion.eventoPress(mensajeAccion);
    }
  }

  eventoTap(idMensaje: string) {
    let mensajeAccion: MensajeAccion = {
      id: idMensaje,
      tipoMensaje: TipoMensaje.GENERAL,
    };
    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(mensajeAccion);
    }
  }
}
