import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { InputCompartido } from '@shared/diseno/modelos';
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @Input() inputCompartido: InputCompartido
  public copiar:boolean = false
  public currentDay: string
  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private translateService: TranslateService
  ) {
    this.currentDay = new Date().toISOString().slice(0, 10);
    this.copiar = false

  }

  ngOnInit(): void {
    this.copiar = false
    
  }

  obtenerError(controlName: any): string {

    let error = '';
    if (!this.inputCompartido.ocultarMensajeError) {

      if (controlName.touched && controlName.errors != null && !this.inputCompartido.soloLectura) {
        if (controlName.errors.required) {
          error = this.internacionalizacionNegocio.obtenerTextoSincrono('text1')
        } else {
          if (controlName.errors.email) {
            error = this.internacionalizacionNegocio.obtenerTextoSincrono('text10')
          } else {
            if (controlName.errors.minlength) {
              error = this.internacionalizacionNegocio.obtenerTextoSincrono('text2', { numero: controlName.errors.minlength.requiredLength })
            } else {
              if (controlName.errors.maxlength) {
                error = this.internacionalizacionNegocio.obtenerTextoSincrono('text3', { numero: controlName.errors.maxlength.requiredLength })
              } else {}
            }
          }
        }
      } else {
        if (controlName.untouched) {
          if (this.inputCompartido.error) {
            error = this.internacionalizacionNegocio.obtenerTextoSincrono('text4')
          }
        }
      }
    }
    return error
  }

  // Churon
  obtenerTextoContador(): string {
    let contador = ''

    if (this.inputCompartido.data.value.length > 0) {
      this.inputCompartido.contadorCaracteres.contador = this.inputCompartido.data.value.length
    }

    if (this.inputCompartido.contadorCaracteres.contador > 9) {
      contador = '' + this.inputCompartido.contadorCaracteres.contador
    } else {
      contador = '0' + this.inputCompartido.contadorCaracteres.contador
    }

    return contador + '/' + this.inputCompartido.contadorCaracteres.numeroMaximo
  }

  // Churon
  determinarCadenaMaxima(): string {
    let numeroCaracteres = '800'
    if (this.inputCompartido.contadorCaracteres) {
      numeroCaracteres = this.inputCompartido.contadorCaracteres.numeroMaximo.toString()
    }
    return numeroCaracteres
  }

  // Churon
  empezarEscribir() {
    if (!this.inputCompartido.soloLectura) {
      if (this.inputCompartido.contadorCaracteres) {
        this.inputCompartido.contadorCaracteres.mostrar = true
      }

      if (this.inputCompartido.errorPersonalizado) {
        this.inputCompartido.errorPersonalizado = ''
      }

      if (
        this.inputCompartido.id &&
        this.inputCompartido.esInputParaFecha
      ) {
        const input: HTMLInputElement = document.getElementById(this.inputCompartido.id) as HTMLInputElement
        input.setAttribute('type', 'date')
      }
    }
  }

  isString(inputText){
		if(typeof inputText === 'string' || inputText instanceof String){
			//it is string
			return true;    
		}else{
			//it is not string
			return false;
		}
	}

  // Churon
  escribiendoEnInput() {
    if (this.inputCompartido.contadorCaracteres) {
      if (this.inputCompartido.contadorCaracteres.contador <= this.inputCompartido.contadorCaracteres.numeroMaximo) {
        this.inputCompartido.contadorCaracteres.contador = this.inputCompartido.data.value.length
      }
    }
  }

  // Churon
  dejarDeEscribir() {
    if (!this.inputCompartido.soloLectura) {
      if (this.inputCompartido.contadorCaracteres) {
        this.inputCompartido.contadorCaracteres.mostrar = false
      }

			if (this.inputCompartido.id && this.inputCompartido.validarCampo && this.inputCompartido.validarCampo.validar) {
				let texto: string = ''
				if (this.isString(this.inputCompartido.data.value) ) {
					texto = this.inputCompartido.data.value.trim()
				} else {
          texto = this.inputCompartido.data.value

        }
			
				this.inputCompartido.validarCampo.validador({
					id: this.inputCompartido.id,
					texto: texto
				})
			}

      if (
        this.inputCompartido.id &&
        this.inputCompartido.esInputParaFecha
      ) {
        const input: HTMLInputElement = document.getElementById(this.inputCompartido.id) as HTMLInputElement
        input.setAttribute('type', 'text')
      }
    }
  }

  verContrasena() {
    if (this.inputCompartido.tipo === 'text') {
      this.inputCompartido.tipo = 'password'
    } else if (this.inputCompartido.tipo === 'password') {
      this.inputCompartido.tipo = 'text'
    }
  }

  validar()  {
    
   if( this.inputCompartido.tipo  === 'email' ){
     return false
   }
   return true
  }

}
