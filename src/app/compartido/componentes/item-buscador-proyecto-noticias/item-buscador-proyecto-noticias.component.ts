import { Component, Input, OnInit } from '@angular/core';
import { AnchoLineaItem } from '@shared/diseno/enums';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { ConfiguracionItemBuscadorProyectoNoticia } from './../../diseno/modelos/item-buscador-proyectos-noticias.interface';
import { LineaCompartida } from '@shared/diseno/modelos';

@Component({
  selector: 'app-item-buscador-proyecto-noticias',
  templateUrl: './item-buscador-proyecto-noticias.component.html',
  styleUrls: ['./item-buscador-proyecto-noticias.component.scss']
})
export class ItemBuscadorProyectoNoticiasComponent implements OnInit {

  @Input() configuracion: ConfiguracionItemBuscadorProyectoNoticia

  public confLinea: LineaCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {}

  ngOnInit(): void {
    this.configurarLinea()
  }

  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
    }
  }

  eventoTap() {
    if (this.configuracion.eventoTap) {

      if (this.configuracion.proyecto && !this.configuracion.noticia) {
        this.configuracion.eventoTap(this.configuracion.proyecto.id)
        return
      }

      if (!this.configuracion.proyecto && this.configuracion.noticia) {
        this.configuracion.eventoTap(this.configuracion.noticia.id)
        return
      }
    }
  }
}
