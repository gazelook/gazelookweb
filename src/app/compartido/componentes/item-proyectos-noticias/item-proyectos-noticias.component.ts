import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos';

@Component({
  selector: 'app-item-proyectos-noticias',
  templateUrl: './item-proyectos-noticias.component.html',
  styleUrls: ['./item-proyectos-noticias.component.scss'],
})
export class ItemProyectosNoticiasComponent implements OnInit {
  @Input() configuracion: CongifuracionItemProyectosNoticias;

  public bordesEsquinas: Array<number>;
  public click = 'click';
  public tipoLink = CodigosCatalogoTipoAlbum.LINK;

  constructor(
    public estiloTexto: EstiloDelTextoServicio
  ) {
    this.bordesEsquinas = [0, 1, 2, 3];
  }

  ngOnInit(): void {
  }

  eventoTap(item: CongifuracionItemProyectosNoticias) {

    if (
      this.configuracion.eventoTap.activo &&
      this.configuracion.eventoTap.evento
    ) {
      this.configuracion.eventoTap.evento(item);
    }
  }

  // Devuelve las clases que definen el estilo del item rectangular
  obtenerClasesParaItemRectangular() {
    const clases = {
      'item-proyecto-noticia': this.configuracion.usoItem !== UsoItemProyectoNoticia.SOLO_TEXTO, // Clase por defecto
      'proyecto-detalle':
        this.configuracion.usoItem === UsoItemProyectoNoticia.RECPROYECTO
        && this.configuracion.resumen,
      'noticia': this.configuracion.usoItem === UsoItemProyectoNoticia.RECNOTICIA,
      'item-solo-texto': this.configuracion.usoItem === UsoItemProyectoNoticia.SOLO_TEXTO,
      'mini-rectangulo': this.configuracion.usoVersionMini,
    };

    clases[this.configuracion.colorDeFondo.toString()] = true;

    return clases;
  }

  // Define las clases para los bordes (esquinas) sobre la foto - Uso UsoItemCirRec.RECPERFIL
  obtenerClasesBordesEnLasEsquinas(index: number) {
    return {
      esquina: true,
      blanca: (!this.configuracion.usarMarcosDeConfiguracion),
      a: index === 0,
      b: index === 1,
      c: index === 2,
      d: index === 3,
      mostrar: true,
    };
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.loader = false
    }
  }
}
