import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-modal-inferior',
  templateUrl: './modal-inferior.component.html',
  styleUrls: ['./modal-inferior.component.scss']
})
export class ModalInferiorComponent implements OnInit {
  @Input() data: ModalInferior

  constructor() { }

  ngOnInit(): void { }

  eventoModal(target: any) {
    target.classList.forEach((clase: any) => {
      if (clase === 'fondo-modal-inferior') {
        if (!this.data.bloqueado) {
          this.data.abierto = false;
        }
      }
    })
  }
}
export interface ModalInferior {
  id: string,
  bloqueado: boolean,
  abierto: boolean,
}
