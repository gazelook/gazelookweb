import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import { IdiomaNegocio } from 'dominio/logica-negocio';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { ColorDelTexto, EstilosDelTexto } from '@shared/diseno/enums';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums';
import { LineaDeTexto } from '../../diseno/modelos/linea-de-texto.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos';
import { ToastComponent } from '@shared/componentes';
import { VariablesGlobales } from '@core/servicios/generales';
import { CodigosCatalogoIdioma } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
@Component({
  selector: 'app-cabecera-idiomas',
  templateUrl: './cabecera-idiomas.component.html',
  styleUrls: ['./cabecera-idiomas.component.scss']
})
export class CabeceraIdiomasComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent
  idiomas: Array<CatalogoIdiomaEntity>
  idiomaEstilo: LineaDeTexto
  idiomaSeleccionado: string
  @Output() cambiarIdioma = new EventEmitter()
  configuracionToast: ConfiguracionToast

  constructor(
    public variablesGlobales: VariablesGlobales,
    private idiomaNegocio: IdiomaNegocio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
  ) {
  }

  ngOnInit(): void {
    this.caragarDatos()
    this.validarIdiomaInicial()
  }

  async validarIdiomaInicial() {
    let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(CodigosCatalogoIdioma.INGLES)
    const idiomaSeleccionadoLocal = this.idiomaNegocio.obtenerIdiomaSeleccionado()

    if (idiomaSeleccionadoLocal && idiomaSeleccionadoLocal !== null) {
      idioma = idiomaSeleccionadoLocal
    }

    this.idiomaSeleccionado = idioma.codNombre.toLowerCase()
    this.internacionalizacionNegocio.usarIidoma(idioma.codNombre.toLocaleLowerCase())
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma)
    this.idiomaNegocio.eliminarVarablesStorage()
  }

  caragarDatos() {
    this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: false, mostrarToast: false, texto: "" }
    //VERIFICAR SI ESTA GUARDADO UN IDIOMA EN LOCALSTORAGE
    this.idiomaSeleccionado = this.internacionalizacionNegocio.obtenerIdiomaInternacionalizacion()
    this.idiomaEstilo = {
      texto: 'idioma',
      enMayusculas: true,
      estiloTexto: EstilosDelTexto.BOLD,
      tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_IGUAL,
      color: ColorDelTexto.TEXTOBLANCO
    }
    this.obtenerCatalogoIdiomas()
  }

  //OBTENER EL CATALOGO DE IDIOMAS
  obtenerCatalogoIdiomas() {
    this.idiomaNegocio.obtenerCatalogoIdiomas()
      .subscribe(res => {
        if (res) {

          this.idiomas = []
          let c = 0
          res.forEach(idioma => {
            if (idioma.idiomaSistema) {
              this.idiomas.push(idioma)
              this.idiomas[c]['orden'] = c
              c++
            }
          }
          )
          let nuevoOrden = []
          let index = this.idiomas.findIndex(e => e.codigo === 'IDI_2')
          nuevoOrden.push(this.idiomas[index])

          let index1 = this.idiomas.findIndex(e => e.codigo === 'IDI_5')
          nuevoOrden.push(this.idiomas[index1])

          let index2 = this.idiomas.findIndex(e => e.codigo === 'IDI_1')
          nuevoOrden.push(this.idiomas[index2])

          let index3 = this.idiomas.findIndex(e => e.codigo === 'IDI_3')
          nuevoOrden.push(this.idiomas[index3])

          let index4 = this.idiomas.findIndex(e => e.codigo === 'IDI_6')
          nuevoOrden.push(this.idiomas[index4])

          let index5 = this.idiomas.findIndex(e => e.codigo === 'IDI_4')
          nuevoOrden.push(this.idiomas[index5])

          this.idiomas = nuevoOrden
        } else {
          this.idiomas = []
        }
      }, error => { })
  }
  // Obtener clases lineas texto
  obtenerClasesLineasTexto(linea: LineaDeTexto, idioma?: string) {
    const clases = {}
    clases[linea.color.toString()] = true
    clases[linea.estiloTexto.toString()] = true
    clases[linea.tamanoConInterlineado.toString()] = true
    clases['enMayusculas'] = linea.enMayusculas
    if ((linea.texto === 'idioma') && (this.idiomaSeleccionado === idioma)) {
      clases['amarilloIdioma'] = true
    }
    return clases
  }

  seleccionarIdioma(idioma: CatalogoIdiomaEntity) {
    this.idiomaSeleccionado = idioma.codNombre
    this.internacionalizacionNegocio.usarIidoma(idioma.codNombre.toLocaleLowerCase())
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma)
    this.idiomaNegocio.eliminarVarablesStorage()
    this.cambiarIdioma.emit('')
    this.variablesGlobales.animacion.$cambioDeIdioma.next(idioma.codNombre.toLowerCase())
  }
}
