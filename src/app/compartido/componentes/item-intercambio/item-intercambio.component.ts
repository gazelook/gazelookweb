import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos';
import { UsoItemIntercambio } from './../../diseno/enums/uso-item-intercambio.enum';
import { ConfiguracionItemIntercambio } from './../../diseno/modelos/item-intercambio.interface';
@Component({
  selector: 'app-item-intercambio',
  templateUrl: './item-intercambio.component.html',
  styleUrls: ['./item-intercambio.component.scss']
})
export class ItemIntercambioComponent implements OnInit {
  @Input() configuracion: ConfiguracionItemIntercambio;

  public bordesEsquinas: Array<number>;
  public click = 'click';
  public tipoLink = CodigosCatalogoTipoAlbum.LINK;

  constructor(
    public estiloTexto: EstiloDelTextoServicio
  ) {
    this.bordesEsquinas = [0, 1, 2, 3];
  }

  ngOnInit(): void { }

  eventoTap(item: ConfiguracionItemIntercambio) {

    if (
      this.configuracion.eventoTap.activo &&
      this.configuracion.eventoTap.evento
    ) {
      this.configuracion.eventoTap.evento(item);
    }
  }
  obtenerClasesParaItemRectangular() {
    const clases = {
      'item-proyecto-noticia': this.configuracion.usoItem !== UsoItemIntercambio.SOLO_TEXTO, // Clase por defecto
      'proyecto-detalle':
        this.configuracion.usoItem === UsoItemIntercambio.REC_INTERC
        && this.configuracion.resumen,
      'item-solo-texto': this.configuracion.usoItem === UsoItemIntercambio.SOLO_TEXTO,
      'mini-rectangulo': this.configuracion.usoVersionMini,
    };

    return clases;
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.loader = false
    }
  }
}
