
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PerfilNegocio } from 'dominio/logica-negocio';
import {
  ColorDeBorde,
  ColorDeFondo
} from '@shared/diseno/enums';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums';
import { InternacionalizacionNegocio } from './../../../dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilModel } from 'dominio/modelo/entidades';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId } from '@core/servicios/generales';
import { FuncionesCompartidas } from '@core/util';
import { AlturaResumenPerfil } from '@shared/diseno/enums';
import { TamanoLista } from '@shared/diseno/enums';
import {
  EstiloItemPensamiento, TipoPensamiento
} from '@shared/diseno/enums';
import { UsoItemCircular } from '@shared/diseno/enums';
import { DatosLista } from '@shared/diseno/modelos';
import { ItemCircularCompartido } from '@shared/diseno/modelos';
import { ConfiguracionListaContactosCircular } from '@shared/diseno/modelos';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos';
import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes';
@Component({
  selector: 'app-lista-contactos-circular',
  templateUrl: './lista-contactos-circular.component.html',
  styleUrls: ['./lista-contactos-circular.component.scss'],
})
export class ListaContactosCircularComponent implements OnInit, OnChanges {
  @Input() configuracion: ConfiguracionListaContactosCircular;
  public util = FuncionesCompartidas;
  public configCirculo: ItemCircularCompartido;
  public configContactos: any[] = [];
  public totalContactosInvitacion: number;
  public numeroMaxContactosResumen = 4;
  public mostrarResumen: boolean = false;
  public tituloNoContactos: CongifuracionTituloRectangulo;
  public dataListaContactos: DatosLista;
  public dataListaPensamientos: DatosLista;
  public configResumenPerfil: ConfiguracionResumenPerfil;
  public cargando: boolean;
  public idPerfil: string;
  public esMiPerfil = false;
  public miPerfil: PerfilModel;
  public paginacion: boolean;
  public puedeCargarMas = true;

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
    private generadorId: GeneradorId,
    public internacionalizacionNegocio: InternacionalizacionNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.cargando = true;
    this.paginacion = false;
  }

  ngOnInit(): void {

    this.obtenerPerfil();
    this.configurarResumenPerfil();
    this.cargarContactos();
    this.configuracionContactos();
    this.configurarTituloNocontactos();
  }
  ngOnChanges() {}

  obtenerPerfil(idPerfil?) {
    let otroPerfil;
    this.miPerfil = this.perfilNegocio.obtenerPerfilSeleccionado(); // perfil propietario
    if (idPerfil) {
      otroPerfil = idPerfil;
    } else {
      otroPerfil = this.route.snapshot.paramMap.get('id');
    }
    if (otroPerfil === this.miPerfil._id) {
      this.esMiPerfil = true;
    } else {
      this.esMiPerfil = false;
    }
  }

  validarDivisorGrilla(pos: number) {
    if (pos === 0 || pos % 2 === 0) {
      return false;
    }
    return true;
  }

  configurarTituloNocontactos() {
    this.tituloNoContactos = {
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
      textoBoton1: 'm3v1texto9',
      textoBoton2: 'm3v1texto10',
    };
  }

  cargarContactos() {
    this.dataListaContactos = {
      tamanoLista: TamanoLista.LISTA_CONTACTOS,
      lista: [],
      cargarMas: () => this.configuracionContactos(),
      cargando: false,
    };
  }

  cargarMas() {
    let element = document.getElementById('scroll');
    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
      if (this.configuracion.eventoScroll) {
        this.configuracion.eventoScroll();
      }
    }
  }

  async configuracionContactos(esInvitacion?: boolean, contactos?) {
    // this.configContactos = [];
    this.dataListaContactos.lista = [];
    if (contactos) {
      this.configuracion.contactos = contactos;
    }
    for (const contacto of this.configuracion.contactos) {
      // this.configContactos.push(this.configContacto(contacto,esInvitacion));
      this.dataListaContactos.lista.push(
        this.configContacto(contacto, esInvitacion)
      );
    }
    this.paginacion = this.configuracion.contactos.length > 12 ? true : false;
  }

  configContacto(contacto, esInvitacion: boolean): ItemCircularCompartido {
    let usoCirculo;
    let urlMedia: string;
    let idPerfil: string;
    const colorFondo = this.util.obtenerColorFondoAleatorio();
    const semilla = this.generadorId.generarIdConSemilla();
    if (contacto.invitadoPor) {
      urlMedia = contacto.invitadoPor.perfil.album[0].portada.principal.url;
      idPerfil = contacto.invitadoPor.perfil._id;
      if (contacto.invitadoPor?.perfil.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    } else if (contacto.perfil) {
      idPerfil = contacto.perfil._id;
      urlMedia = contacto.perfil.album[0].portada.principal.url;
      if (contacto.perfil.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    } else if (contacto.contactoDe) {
      idPerfil = contacto.contactoDe._id;
      urlMedia = contacto.contactoDe.album[0].portada.principal.url;
      if (contacto.contactoDe.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    }

    return {
      id: idPerfil,
      idInterno: semilla,
      usoDelItem: usoCirculo,
      esVisitante: this.esMiPerfil ? false : true,
      urlMedia: urlMedia,
      activarClick: true,
      activarDobleClick: this.esMiPerfil ? true : false,
      activarLongPress: false,
      mostrarBoton: false,
      mostrarLoader: urlMedia ? false : true,
      textoBoton: '',
      eventoEnItem: this.configuracion.evento,
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: colorFondo,
      mostrarCorazon: false,
      abrirResumenPerfil:
        !this.configuracion.resumen ||
        this.configuracion.contactos.length <= this.numeroMaxContactosResumen,
    };
  }



  // mostrarLoaderCancelar

  // ----------------- Config modal resumen perfil  ----------------

  async configurarResumenPerfil() {
    this.dataListaPensamientos = {
      tamanoLista: TamanoLista.LISTA_PENSAMIENTO_RESUMEN_MODAL_PERFIL,
      lista: [],
    };
    this.configResumenPerfil = {
      titulo: '',
      alturaModal: AlturaResumenPerfil.VISTA_BUSCAR_CONTACTOS_100,
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        '',
        ColorDeBorde.BORDER_AMARILLO,
        ColorDeFondo.FONDO_BLANCO,
        false,
        UsoItemCircular.CIRCONTACTO,
        false
      ),
      configuracionPensamiento: {
        tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
        esLista: true,
        subtitulo: true,
        configuracionItem: {
          estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
        },
      },
      confDataListaPensamientos: this.dataListaPensamientos,
      botones: [
        {
          text: await this.internacionalizacionNegocio.obtenerTextoLlave('m3v5texto2'),
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.AMARRILLO,
          ejecutar: (id) => {

            this.cerrarResumenPerfil();
          },

          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,

        },
        {
          text: await this.internacionalizacionNegocio.obtenerTextoLlave('m3v5texto4'),
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.ROJO,
          ejecutar: () => {
            this.cerrarResumenPerfil();
          },
          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,
        },
      ],
      reintentar: (idPerfil) => {

        this.obtenerPerfilBasico(idPerfil);
      },
      mostrar: false,
      error: false,
      loader: false,
    };
  }

  configurarGeneralFotoPerfilContactos(
    urlMedia: string,
    colorBorde: any,
    colorFondo: any,
    mostrarCorazon: boolean,
    usoItemCirculo: UsoItemCircular,
    activarClick: boolean
  ): ItemCircularCompartido {
    return {
      id: '',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: usoItemCirculo,
      esVisitante: false,
      urlMedia: urlMedia,
      activarClick: activarClick,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: true,
      textoBoton: '',
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: colorBorde,
      colorDeFondo: colorFondo,
      mostrarCorazon: mostrarCorazon,
    };
  }

  cerrarResumenPerfil() {
    this.configResumenPerfil.mostrar = false;
    this.configResumenPerfil.loader = true;
  }

  obtenerPerfilBasico(idPerfil: string) {
    this.idPerfil = idPerfil;
    this.cargando = false;
    this.configResumenPerfil.mostrar = true;
    this.configResumenPerfil.loader = true;
    this.configResumenPerfil.confDataListaPensamientos.lista = [];
    this.perfilNegocio.obtenerPerfilBasico(idPerfil).subscribe(
      (resumenPerfil: PerfilModel) => {

        if (resumenPerfil === null) {
        } else {
          this.configResumenPerfil.configCirculoFoto.urlMedia =
            resumenPerfil.album[0].portada.principal.url;
          this.configResumenPerfil.configuracionPensamiento.subtitulo = true;
          // let titulo =
          //   resumenPerfil.nombre + '(' + resumenPerfil.nombreContacto + ')';
          this.configResumenPerfil.titulo = resumenPerfil.nombreContacto;

          if (resumenPerfil.album[0].portada.principal.fileDefault) {
            this.configResumenPerfil.configCirculoFoto.usoDelItem =
              UsoItemCircular.CIRCARITACONTACTODEFECTO;
          } else {
            this.configResumenPerfil.configCirculoFoto.usoDelItem =
              UsoItemCircular.CIRCONTACTO;
          }
          if (resumenPerfil.pensamientos) {
            for (const iterator of resumenPerfil.pensamientos) {
              this.configResumenPerfil.confDataListaPensamientos.lista.push(
                iterator
              );
            }
          }
          this.configResumenPerfil.loader = false;
        }
      },
      (error) => {
        this.cargando = false;
      }
    );
  }
}
