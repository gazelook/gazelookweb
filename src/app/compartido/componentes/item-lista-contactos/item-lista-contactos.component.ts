import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos';
import { UsoItemListaContacto } from './../../diseno/enums/item-lista-contacto.enum';

@Component({
  selector: 'app-item-lista-contactos',
  templateUrl: './item-lista-contactos.component.html',
  styleUrls: ['./item-lista-contactos.component.scss']
})
export class ItemListaContactosComponent implements OnInit {
  @Input() configuracion: ConfiguracionItemListaContactosCompartido

  public usoItemListaContacto = UsoItemListaContacto
  constructor(
    public estiloTexto: EstiloDelTextoServicio,
  ) { }

  ngOnInit(): void {
  }

  eventoIconoXContacto() {
    if (this.configuracion.eventoIconoX) {
      // this.configuracion.eventoCirculoNombre(this.configuracion.id)
      this.configuracion.eventoIconoX(this.configuracion.id)
    }
  }

  configuracionLineaVerde() {
    const clases = {}
    clases['linea-verde-contacto '] = true
    clases[this.configuracion.configuracionLineaVerde.paddingLineaVerde.toString()] = true
    return clases
  }

  tap() {
    if (this.configuracion.eventoCirculoNombre) {
      this.configuracion.eventoCirculoNombre(this.configuracion.id, this.configuracion.contacto, this.configuracion.configCirculoFoto.urlMedia)
    }
  }

  dobleTap() {
    if (this.configuracion.eventoDobleTap) {
      this.configuracion.eventoDobleTap(
        this.configuracion.id,
        this.configuracion.contacto
      )
    }
  }
}
