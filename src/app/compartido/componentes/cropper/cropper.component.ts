import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Dimensions, ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { ConfiguracionCropper } from '../../diseno/modelos/foto-editor.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';

@Component({
  selector: 'app-cropper',
  templateUrl: './cropper.component.html',
  styleUrls: ['./cropper.component.scss']
})
export class CropperComponent implements OnInit {
  @ViewChild('crop', { static: false }) crop: ImageCropperComponent

  @Input() configuracion: ConfiguracionCropper

  public croppedImage: any

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {

  }

  ngOnInit(): void {

  }
  // Cuando la imagen fue cortada
  imageCropped(event: ImageCroppedEvent) {
    if (this.configuracion.imagenCortada) {
      this.configuracion.imagenCortada(event)
      this.cerrarCropper()
      return
    }
    this.cerrarCropper()
    return
  }

  // Cuando la imagen es cargada en el cropper
  imageLoaded() {
    this.cambiarStatusCropper(true, false, false)
  }

  // Cuando el componente empieza a cortar la imagen
  startCropImage() {
    this.cambiarStatusCropper(false, true, false)
  }

  // Cuando el cropper esta listo para interactuar
  cropperReady(size: Dimensions) {

  }

  // Cuando ocurre un error al cargar la imagen en el cropper
  loadImageFailed() {
    this.cambiarStatusCropper(false, false, true, 'text37')
  }

  // Obtener imagen cortada
  getCropImage() {
    if (this.crop) {
      this.crop.crop()
      return
    }

    this.cambiarStatusCropper(false, false, true, 'text37')
    return
  }

  cerrarCropper() {
    this.cambiarStatusCropper(false, true, false)
    this.configuracion.imageBase64 = null
    this.configuracion.imageURL = null
    this.configuracion.imageFile = null
    this.configuracion.imageChangedEvent = null
    this.configuracion.mostrarModal = false
  }

  // Cambiar valores
  cambiarStatusCropper(mostrarCaja: boolean, mostrarLoader: boolean, mostrarError: boolean, errorContenido?: string) {
    this.configuracion.mostrarCaja = mostrarCaja
    this.configuracion.mostrarLoader = mostrarLoader
    this.configuracion.mensajeError.mostrarError = mostrarError
    if (errorContenido) {
      this.configuracion.mensajeError.contenido = errorContenido
    }
  }

}
