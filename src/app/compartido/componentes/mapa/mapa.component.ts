import { Component, Input, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';
import { LocationService } from './../../../nucleo/servicios/locales/location.service';
import { ConfiguracionMapa } from './../../diseno/modelos/mapa.interface';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss']
})
export class MapaComponent implements OnInit {
  @Input() configuracion: ConfiguracionMapa
  mapbox = (mapboxgl as typeof mapboxgl);
  styleMap: string;
  zoomMap: number;
  kmCirculo: number

  constructor(
    private geoLocation: LocationService
  ) {
    this.mapbox.accessToken = environment.mapBoxToken;
    this.styleMap = 'mapbox://styles/mapbox/streets-v11'
    this.zoomMap = 0
    this.kmCirculo = 0.2
  }

  ngOnInit() {
    if (this.configuracion && !this.configuracion.visitar) {
      this.zoomMap = -1
    } else {
      this.zoomMap = 13
    }
    this.crearMapa(this.configuracion.latitudInicial, this.configuracion.longitudInicial, this.configuracion.visitar)
  }

  crearMapa(latitud: number, longitud: number, visitar: boolean = false) {
    let nuevoCirculo = (latitud: number, longitud: number) => {
      let circulo2 = createGeoJSONCircle([longitud, latitud], this.kmCirculo)
      let source: mapboxgl.GeoJSONSource = map.getSource('polygon') as mapboxgl.GeoJSONSource
      source.setData(circulo2.data)
    }

    let createGeoJSONCircle = (center?, radiusInKm?, points?): any => {
      if (!points) points = 128;

      let coords = {
        latitude: center[1],
        longitude: center[0]
      };

      let km = radiusInKm;

      let ret = [];
      let distanceX = km / (111.320 * Math.cos(coords.latitude * Math.PI / 180));
      let distanceY = km / 110.574;

      let theta, x, y;
      for (let i = 0; i < points; i++) {
        theta = (i / points) * (2 * Math.PI);
        x = distanceX * Math.cos(theta);
        y = distanceY * Math.sin(theta);

        ret.push([coords.longitude + x, coords.latitude + y]);
      }
      ret.push(ret[0]);


      return {
        "type": "geojson",
        "data": {
          "type": "FeatureCollection",
          "features": [{
            "type": "Feature",
            "properties": {},
            "geometry": {
              "type": "Polygon",
              "coordinates": [ret]
            }
          }]
        }
      };
    }

    let circulo = createGeoJSONCircle([longitud, latitud], this.kmCirculo)

    let map = new mapboxgl.Map({
      container: 'map',
      style: this.styleMap,
      zoom: this.zoomMap,
      center: [longitud, latitud]
    });
    map.on('load', function () {
      map.addSource("polygon", circulo)
      map.addLayer({
        "id": "polygon",
        "type": "fill",
        "source": "polygon",
        "layout": {},
        "paint": {
          "fill-color": "rgb(125, 135, 148)",
          "fill-opacity": 0.7
        }
      });
    });



    map.addControl(new mapboxgl.NavigationControl());
    let onDragEnd3 = () => {
      let lngLat = marker.getLngLat()
      this.configuracion.nuevasPosiciones(lngLat.lng, lngLat.lat)
      nuevoCirculo(lngLat.lat, lngLat.lng)
    }
    let marker = new mapboxgl.Marker({
      draggable: true,
      // element: el,
      scale: 1

    })
      .setLngLat([longitud, latitud])
      .addTo(map);

    marker.on('dragend', onDragEnd3);

    if (this.configuracion.visitar && this.zoomMap >= 13) {
      marker.remove()
    }

    map.on('zoom', function () {
      if (map.getZoom() > 12) {
        if (visitar) {
          marker.remove()
        }

      } else {
        marker.addTo(map)
      }
    });
  }
}


