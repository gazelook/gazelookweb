import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-cargando',
  templateUrl: './cargando.component.html',
  styleUrls: ['./cargando.component.scss']
})
export class CargandoComponent implements OnInit {
  @Input() diametro: DiametroDelLoader

  public DiametroDelLoaderEnum = DiametroDelLoader

  constructor() { }

  ngOnInit(): void {
    if (!this.diametro) {
      this.diametro = DiametroDelLoader.LOADER_DEFAULT
    }
  }

  obtenerClasesLoader() {
    const clases = {}
    clases['contenedor-loader'] = true
    clases[this.diametro.toString()] = true
    return clases
  }

}

export enum DiametroDelLoader {
  LOADER_DEFAULT = 'diametro-default',
  LOADER_BOTON_ICONO = 'diametro-boton-icono',
}
