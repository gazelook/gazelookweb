import { Component, Input, OnInit } from '@angular/core';
import { InternacionalizacionNegocio } from './../../../dominio/logica-negocio/internacionalizacion.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos';
@Component({
  selector: 'app-lista-contacto',
  templateUrl: './lista-contacto.component.html',
  styleUrls: ['./lista-contacto.component.scss']
})
export class ListaContactoComponent implements OnInit {
  @Input() configuracion: ConfiguracionListaContactoCompartido

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
    public internacionalizacionNegocio: InternacionalizacionNegocio,
  ) { }

  ngOnInit(): void {
    if (
      this.configuracion &&
      !this.configuracion.llaveSubtitulo
    ) {
      this.configuracion.llaveSubtitulo = 'm3v8texto4'
    }
  }

  eventoTap(contacto: ConfiguracionItemListaContactosCompartido) {
    if (this.configuracion.contactoSeleccionado) {
      this.configuracion.contactoSeleccionado(contacto.id)
    }
  }
}
