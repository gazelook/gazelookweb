import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MediaNegocio } from 'dominio/logica-negocio';
import { ArchivoModel } from 'dominio/modelo/entidades';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos';
import { Enrutador, InformacionEnrutador } from 'src/app/presentacion/enrutador/enrutador.component';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { VariablesGlobales } from '@core/servicios/generales';
@Component({
  selector: 'app-portada-gif',
  templateUrl: './portada-gif.component.html',
  styleUrls: ['./portada-gif.component.scss']
})
export class PortadaGifComponent implements OnInit, OnDestroy, Enrutador {

  @Input() configuracion: ConfiguracionPortadaGif

  public informacionEnrutador: InformacionEnrutador
  public archivosRamdon: ArchivoModel[]
  public intervalo: any
  public primeraImagen: boolean
  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private mediaNegocio: MediaNegocio
  ) {
    this.archivosRamdon = []
    this.primeraImagen = true
  }

  ngOnInit(): void {
    this.configurarImagenRamdon()
  }

  ngOnDestroy(): void {
    this.limpiar()
  }

  limpiar() {
    if (this.intervalo) {
      clearInterval(this.intervalo)
    }
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }

      if (this.configuracion.esIzquierdo) {
        setTimeout(() => {

          this.variablesGlobales.configurarDataAnimacionParaGifs(true)
        }, 5000)
      } else {
        setTimeout(() => {
          this.variablesGlobales.configurarDataAnimacionParaGifs(true, true)
          setTimeout(() => {
            this.variablesGlobales.configurarDataAnimacionParaGifs(true, true, true)
            this.variablesGlobales.animacion.$subjectCapaGif.next(true)
          }, 2000)
        }, 14000)
      }
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(info: InformacionEnrutador) {
    this.informacionEnrutador = info

    // Se usa para validar params relacionados a configuraciones de componentes hijos
    if (!this.informacionEnrutador.params) {
      return
    }

    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'configuracion') {
        this.configuracion = item.valor
      }
    })
  }

  configurarImagenRamdon() {

    let tiempo: number
    if (this.configuracion.esIzquierdo && this.primeraImagen) {

      tiempo = 40000
    }
    if (!this.configuracion.esIzquierdo) {

      tiempo = 30000
    }
    this.intervalo = setInterval(() => {

      if (
        this.variablesGlobales.animacion.mostrarGifUno ||
        this.variablesGlobales.animacion.mostrarGifDos
      ) {
        this.limpiar()
        return
      }
      this.archivosRamdon = this.mediaNegocio.obtenerArchivosDefaultPorTipo(CodigosCatalogoArchivosPorDefecto.TERCIOS)
      const pos = Math.floor(Math.random() * this.archivosRamdon.length)

      if (pos >= 0) {
        this.configuracion.urlImagen = this.archivosRamdon[pos]?.url
      }
      this.primeraImagen = false
      tiempo = 20000
    }, tiempo)
  }
}
export interface ConfiguracionPortadaGif {
  esIzquierdo: boolean,
  urlImagen: string,
  urlGif?: string,
  llaveTexto: string,
  textoEspecial?: boolean,
  imagenFija?: boolean,
  vaCentrado?: boolean
}
