import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId } from '@core/servicios/generales';
import { DiametroDelLoader } from '@shared/componentes';
import { ConfiguracionMensajeFirebase } from './../mensaje-gaze.component';

@Component({
  selector: 'app-mensaje-archivo',
  templateUrl: './mensaje-archivo.component.html',
  styleUrls: ['./mensaje-archivo.component.scss']
})
export class MensajeArchivoComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  public idArchivo: string
  public DiametroDelLoaderEnum = DiametroDelLoader

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId
  ) {
    this.idArchivo = 'archivo_' + this.generadorId.generarIdConSemilla()
  }

  ngOnInit(): void {
    if (
      this.configuracion &&
      !this.configuracion.mensaje.esLocal &&
      (
        !this.configuracion.contenidoError ||
        this.configuracion.contenidoError.length === 0
      )
    ) {
      this.configuracion.mostrarCargando = false
    }
  }

  obtenerDateDeTimeStamp(fecha: any) {
    return new Date(fecha)
  }

  obtenerExtensionDelArchivo(url: string) {
    const a = url.split('/')
    const b = a[a.length - 1]
    const c = b.split('.')
    return 'archivo-adjunto.' + c[c.length - 1]
  }

}
