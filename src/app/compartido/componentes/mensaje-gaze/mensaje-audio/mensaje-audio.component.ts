import { Component, Input, OnInit } from '@angular/core';
import { DireccionDelReproductor } from 'src/app/compartido/diseno/enums/audio-reproductor.enum';
import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from 'src/app/nucleo/servicios/generales/generador-id.service';
import { ColorDeFondo } from './../../../diseno/enums/estilo-colores.enum';
import { ConfiguracionAudioReproductor } from './../../../diseno/modelos/audio-reproductor.interface';
import { DiametroDelLoader } from './../../cargando/cargando.component';
import { ConfiguracionMensajeFirebase } from './../mensaje-gaze.component';
@Component({
  selector: 'app-mensaje-audio',
  templateUrl: './mensaje-audio.component.html',
  styleUrls: ['./mensaje-audio.component.scss']
})
export class MensajeAudioComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  public confAudio: ConfiguracionAudioReproductor
  public DiametroDelLoaderEnum = DiametroDelLoader

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    private generadorId: GeneradorId
  ) { }

  ngOnInit(): void {
    if (
      this.configuracion &&
      this.configuracion.mensaje.adjuntos &&
      this.configuracion.mensaje.adjuntos.length > 0 &&
      !this.confAudio
    ) {
      this.configurarAudioReproductor()
    }
  }

  configurarAudioReproductor() {
    this.confAudio = {
      colorDeFondo:
        (!this.configuracion.esPropietario)
          ? ColorDeFondo.FONDO_MENSAJE_ENVIADO
          : ColorDeFondo.FONDO_MENSAJE_RECIBIDO,
      direccion:
        (this.configuracion.esPropietario)
          ? DireccionDelReproductor.HACIA_LA_IZQUIERDA
          : DireccionDelReproductor.HACIA_LA_DERECHA,
      idInterno: this.generadorId.generarIdConSemilla(),
      media: this.configuracion.mensaje.adjuntos[0],
    }
  }

  obtenerDateDeTimeStamp(fecha: any) {
    return new Date(fecha)
  }

  obtenerConfiguracionAudio() {
    if (!this.confAudio) {
      this.configurarAudioReproductor()
    }

    return this.confAudio
  }
}
