import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from './../../../../nucleo/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from './../../../../nucleo/servicios/generales/generador-id.service';
import { DiametroDelLoader } from './../../cargando/cargando.component';
import { ConfiguracionMensajeFirebase } from './../mensaje-gaze.component';

@Component({
  selector: 'app-mensaje-imagen',
  templateUrl: './mensaje-imagen.component.html',
  styleUrls: ['./mensaje-imagen.component.scss']
})
export class MensajeImagenComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  public idImagen: string
  public DiametroDelLoaderEnum = DiametroDelLoader

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId
  ) {
    this.idImagen = 'imagen_' + this.generadorId.generarIdConSemilla()
  }

  ngOnInit(): void {
  }

  obtenerDateDeTimeStamp(fecha: any) {
    if (typeof fecha !== 'number') {
      return new Date()
    }

    return new Date(fecha)
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarCargando = false
    }
  }
}

