import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from './../../../../nucleo/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionMensajeFirebase } from './../mensaje-gaze.component';
@Component({
  selector: 'app-mensaje-texto',
  templateUrl: './mensaje-texto.component.html',
  styleUrls: ['./mensaje-texto.component.scss']
})
export class MensajeTextoComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void { }

  obtenerDateDeTimeStamp(fecha: any) {
    return new Date(fecha)
  }
}
