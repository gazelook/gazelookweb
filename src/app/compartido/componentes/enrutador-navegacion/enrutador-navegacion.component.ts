import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { UbicacionDelComponente } from 'src/app/presentacion/enrutador/enrutador.component';
@Component({
	selector: 'app-enrutador-navegacion',
	templateUrl: './enrutador-navegacion.component.html',
	styleUrls: ['./enrutador-navegacion.component.scss']
})
export class EnrutadorNavegacionComponent implements OnInit {

	@Input() configuracion: ConfiguracionEnrutadorNavegacion
	public coloresCirculoEnrutador: Array<string>
	public aleatorio: number

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private enrutadorService: EnrutadorService
	) {

		this.coloresCirculoEnrutador = [
			"#2c5870",
			"#05637c",
			"#66888a",
			"#467161",
			"#bd7440",
			"#a24949",
			"#474063",
			"#b58b55",
			"#a28067",
			"#5c7eab"

		]
		this.aleatorio = Math.floor(Math.random()*this.coloresCirculoEnrutador.length)
	}

	ngOnInit(): void {}

	clickIzquierdo() {
		this.aleatorio = Math.floor(Math.random()*this.coloresCirculoEnrutador.length)
		this.enrutadorService.clicEnManoDelEnrutador$.next(UbicacionDelComponente.IZQUIERDA)
	}

	clickAbajo() {
		this.aleatorio = Math.floor(Math.random()*this.coloresCirculoEnrutador.length)
		this.enrutadorService.clicEnManoDelEnrutador$.next(UbicacionDelComponente.CENTRAL)
	}

	clickDerecha() {

		this.aleatorio = Math.floor(Math.random()*this.coloresCirculoEnrutador.length)
		this.enrutadorService.clicEnManoDelEnrutador$.next(UbicacionDelComponente.DERECHA)
	}

	clickEnContenedorCirculo(target: any){
		target.classList.forEach((clase: any) => {
			if (
				(
					clase === 'cuerpo-circulo' ||
					clase === 'tercio-enrutador' ||
					clase === 'tercio'
				) &&
				clase !== 'contenedor-circulo'
			) {
				this.aleatorio = Math.floor(Math.random()*this.coloresCirculoEnrutador.length)
				this.configuracion.mostrar = false
				this.enrutadorService.informacionAEnrutar = undefined
			}
		})
	}

	obtenerClasesCirculo() {
		const data = {}
		data['background-color'] = this.coloresCirculoEnrutador[this.aleatorio];
		return data
	}





}

export interface ConfiguracionEnrutadorNavegacion {
	mostrar?: boolean,
	posIzquierda?: boolean,
    posCentral?: boolean,
    posDerecha?: boolean,
    clickMano?: Function
}
