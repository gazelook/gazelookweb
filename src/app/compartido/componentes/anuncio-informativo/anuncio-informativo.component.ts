import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-anuncio-informativo',
  templateUrl: './anuncio-informativo.component.html',
  styleUrls: ['./anuncio-informativo.component.scss']
})
export class AnuncioInformativoComponent implements OnInit {

  @Input() configuracion: AnuncioPublicitarioCompartido

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void {
    
  }

}
