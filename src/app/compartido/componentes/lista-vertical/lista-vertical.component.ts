import { Component, Input, OnInit } from '@angular/core';
import { DatosLista } from '@shared/diseno/modelos';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
@Component({
  selector: 'app-lista-vertical',
  templateUrl: './lista-vertical.component.html',
  styleUrls: ['./lista-vertical.component.scss']
})
export class ListaVerticalComponent implements OnInit {

  @Input() dataLista: DatosLista

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void {}

  //Verifica si presento error al traer los datos de la lista, en caso de llegar a final ejecuta el metodo que envio el padre de reintentar
  reintentar() {
    this.dataLista.reintentar()
  }

  //Verifica si ya llego al final de la lista, en caso de llegar a final ejecuta el metodo que envio el padre de cargas mas
  cargarMas() {
    let element = document.getElementById("scroll");
    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
      if (this.dataLista.cargarMas) {
        this.dataLista.cargarMas()
      }
    }
  }
}
