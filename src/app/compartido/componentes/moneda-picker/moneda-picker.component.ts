import { Component, Input, OnInit } from '@angular/core';
import { InfoAccionSelector } from '@shared/diseno/modelos';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { MonedaPickerService } from './../../../nucleo/servicios/generales/moneda-picker.service';
import { AccionesSelector } from '@shared/diseno/enums';
import { ConfiguracionMonedaPicker } from '@shared/diseno/modelos';
@Component({
  selector: 'app-moneda-picker',
  templateUrl: './moneda-picker.component.html',
  styleUrls: ['./moneda-picker.component.scss']
})
export class MonedaPickerComponent implements OnInit {

  @Input() configuracion: ConfiguracionMonedaPicker
  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public monedaPickerService: MonedaPickerService
  ) { }

  ngOnInit(): void { }

  obtenerClasesInputCantidadMoneda(): any {
    const clases = {}
    clases['input-cantidad'] = true
    clases[this.configuracion.inputCantidadMoneda.colorFondo.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.colorTexto.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.estiloDelTexto.toString()] = true
    clases[this.configuracion.inputCantidadMoneda.tamanoDelTexto.toString()] = true

    if (this.configuracion.inputCantidadMoneda.estiloBorde) {
      clases[this.configuracion.inputCantidadMoneda.estiloBorde.toString()] = true
    }
    return clases
  }

  obtenerClasesInputPreviewSelector() {
    const clases = {}
    clases['input-preview-selector'] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.colorFondo.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.colorTexto.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloDelTexto.toString()] = true
    clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.tamanoDelTexto.toString()] = true

    if (this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde) {
      clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde.color.toString()] = true
      clases[this.configuracion.selectorTipoMoneda.inputTipoMoneda.estiloBorde.espesor.toString()] = true
    }

    return clases
  }

  tapSelector() {
    if (this.configuracion.selectorTipoMoneda.evento) {
      const accion: InfoAccionSelector = {
        accion: AccionesSelector.ABRIR_SELECTOR
      }
      this.configuracion.selectorTipoMoneda.evento(accion)
    }
  }
}

