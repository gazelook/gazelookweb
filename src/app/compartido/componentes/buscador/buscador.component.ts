import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { BuscarComponent } from 'src/app/presentacion/buscador-general/buscar/buscar.component';
import { ConfiguracionBuscador, ModoBusqueda } from '@shared/diseno/modelos';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
@Component({
    selector: 'app-buscador',
    templateUrl: './buscador.component.html',
    styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit, AfterViewInit {
    @Input() configuracion: ConfiguracionBuscador

    // Utils
    public ModoBusquedaEnum = ModoBusqueda

    constructor(
        public internacionalizacionNegocio: InternacionalizacionNegocio,
        public estiloTexto: EstiloDelTextoServicio,
        private router: Router,
        private enrutadorService: EnrutadorService
    ) { }

    ngOnInit(): void {
        this.inicializarPlaceholder()
        this.inicializarModoBusqueda()
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.focusEnInput()
        })
    }

    focusEnInput() {
        if (!this.configuracion.focusEnInput) {
            return
        }

        const elemento: HTMLInputElement = document.getElementById('inputBarraBusqueda') as HTMLInputElement
        if (elemento) {
            elemento.focus()
        }
    }

    inicializarPlaceholder() {
        this.configuracion.placeholderActivo = true
    }

    inicializarModoBusqueda() {
        if (!this.configuracion.modoBusqueda) {
            this.configuracion.modoBusqueda = ModoBusqueda.BUSQUEDA_COMPONENTE
        }
    }

    validarSiEstaBuscando() {
        if (this.configuracion.modoBusqueda !== ModoBusqueda.BUSQUEDA_COMPONENTE) {
            this.configuracion.placeholderActivo = (
                this.configuracion.valorBusqueda &&
                this.configuracion.valorBusqueda.length === 0
            )

            if (
                (
                    typeof this.configuracion.valorBusqueda === 'string' &&
                    this.configuracion.valorBusqueda.length === 0
                )
                || this.configuracion.valorBusqueda === null
            ) {
                this.configuracion.buscar(true)
            }
        }
    }

    eventoTap() {
        if (
            this.configuracion &&
            !this.configuracion.disable &&
            this.configuracion.modoBusqueda === ModoBusqueda.BUSQUEDA_COMPONENTE
        ) {
            this.irAComponenteDeBusquedaSegunEntidad()
        }
    }

    irAComponenteDeBusquedaSegunEntidad() {

        if (!this.configuracion.entidad) {
            return
        }
        this.enrutadorService.navegarConPosicionFija(
            {
                componente: BuscarComponent,
                params: [
                    {
                        nombre: 'codigoEntidad',
                        valor: this.configuracion.entidad
                    }
                ]
            },
            this.configuracion.posicion
        )
    }

    focus() {
        if (
            this.configuracion &&
            !this.configuracion.disable
        ) {

        }
    }

    buscar() {
        if (this.configuracion.buscar) {
            this.configuracion.buscar()
        }
    }
}

