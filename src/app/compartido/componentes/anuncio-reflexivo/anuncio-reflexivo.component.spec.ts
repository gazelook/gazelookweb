import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnuncioReflexivoComponent } from './anuncio-reflexivo.component';

describe('AnuncioReflexivoComponent', () => {
  let component: AnuncioReflexivoComponent;
  let fixture: ComponentFixture<AnuncioReflexivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuncioReflexivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnuncioReflexivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
