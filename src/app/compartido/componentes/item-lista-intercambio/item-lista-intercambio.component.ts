import { Component, Input, OnInit } from '@angular/core';
import { AnchoLineaItem } from '@shared/diseno/enums';
import { LineaCompartida } from '@shared/diseno/modelos';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums';
import { ConfiguracionItemListaIntercambio } from './../../diseno/modelos/item-lista-intercambio.interface';
@Component({
  selector: 'app-item-lista-intercambio',
  templateUrl: './item-lista-intercambio.component.html',
  styleUrls: ['./item-lista-intercambio.component.scss']
})
export class ItemListaIntercambioComponent implements OnInit {

  @Input() configuracion: ConfiguracionItemListaIntercambio

  public confLinea: LineaCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {}

  ngOnInit(): void {
    this.configurarLinea()

  }

  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
    }
  }

  eventoTap() {
    if (this.configuracion.eventoTap) {
      if (this.configuracion.intercambio) {
        this.configuracion.eventoTap(this.configuracion.intercambio.id)
        return
      }
    }
  }
}
