import { ConfiguracionToast } from '@shared/diseno/modelos';
import { Component, OnInit, Input } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
@Component({
	selector: 'app-toast',
	templateUrl: './toast.component.html',
	styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {

	@Input() configuracion: ConfiguracionToast

	public timeOut: any
	public urlActiva: string
	public urlLoaders: Array<string>

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio
	) {
		this.urlActiva = ''
		this.urlLoaders = [
			"https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/botones/boton-cargando.gif",
			"https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/botones/boton-cargando-mono.gif",
			"https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/botones/boton-cargando-gato.gif"
		]
	}

	ngOnInit(): void {
		this.obtenerUrlRamdon()
	}

	obtenerUrlRamdon() {
		this.urlActiva = this.urlLoaders[Math.floor(Math.random() * this.urlLoaders.length)]
	}

	cambiarStatusToast(texto: string, mostrarLoader: boolean, mostrarToast: boolean, cerrarClickOutside: boolean) {
		this.configuracion.texto = texto
		this.configuracion.mostrarLoader = mostrarLoader
		this.configuracion.mostrarToast = mostrarToast
		this.configuracion.cerrarClickOutside = cerrarClickOutside
	}

	abrirToast(mensaje: string, progress: boolean = false) {
		this.configuracion.mostrarLoader = progress
		this.configuracion.bloquearPantalla = progress

		if (progress) {
			this.configuracion.cerrarClickOutside = false
			this.obtenerUrlRamdon()
		}

		this.configuracion.mostrarToast = true;
		this.configuracion.texto = mensaje;
		this.visibleHasta()
	}

	cerrarToastDesdePadre(target: any) {
		let encontrado = false
		target.classList.forEach((clase: any) => {
			if (clase === 'wrapper-toast-contenido') {
				encontrado = true
			}
		})

		if (encontrado) {
			this.configuracion.mostrarToast = false
			this.configuracion.mostrarLoader = false
			this.configuracion.texto = ''

			if (this.timeOut) {
				clearTimeout(this.timeOut)
			}
		}
	}

	cerrarToast() {
		this.configuracion.mostrarToast = false
		this.configuracion.texto = ''
		this.configuracion.mostrarLoader = false
	}

	visibleHasta() {
		if (!this.configuracion.mostrarLoader) {
			if (!this.configuracion.intervalo) {
				this.configuracion.intervalo = 5;
			}

			if (this.timeOut) {
				clearTimeout(this.timeOut)
			}
			this.timeOut = setTimeout(() => {
				this.cerrarToast();
			}, this.configuracion.intervalo * 1000)
		}
	}
}
