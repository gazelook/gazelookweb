import { error } from '@angular/compiler/src/util';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfiguracionCamaraVideo } from '@shared/diseno/modelos/camara-video.interface';
import { VideoGrabacionService } from '@core/servicios/generales/video-grabacion/video-grabacion.service';

@Component({
    selector: 'app-camara-video',
    templateUrl: './camara-video.component.html',
    styleUrls: ['./camara-video.component.scss']
})
export class CamaraVideoComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() configuracion: ConfiguracionCamaraVideo


    WIDTH = window.innerWidth;
    HEIGHT = Math.round(window.innerWidth / 1.40);

    @ViewChild('videoElement', { static: false }) videoElement: ElementRef
    error: any
    video: any;
    isPlaying = false;
    displayControls = true;
    isAudioRecording = false;
    isVideoRecording = false;
    audioRecordedTime;
    videoRecordedTime;
    audioBlobUrl;
    videoBlobUrl;
    audioBlob;
    videoBlob;
    audioName;
    videoName;
    audioStream;
    videoStream: MediaStream;
    audioConf = { audio: true }
    videoConf = { video: { facingMode: "enviroment", width: window.innerWidth, height: Math.round(window.innerWidth / 1.40) }, audio: true }
    stream: any
    camaraPrincipal: boolean


    constructor(
        private ref: ChangeDetectorRef,
        private videoRecordingService: VideoGrabacionService,
        private sanitizer: DomSanitizer
    ) {
        this.camaraPrincipal = true
        this.videoRecordingService.recordingFailed().subscribe(() => {
            this.isVideoRecording = false;
            this.ref.detectChanges();
        });

        this.videoRecordingService.getRecordedTime().subscribe((time) => {
            this.videoRecordedTime = time;
            this.ref.detectChanges();
        });

        this.videoRecordingService.getStream().subscribe((stream) => {
            this.videoStream = stream;
            this.ref.detectChanges();
        });

        this.videoRecordingService.getRecordedBlob().subscribe((data) => {
            this.videoBlob = data.blob;
            this.videoName = data.title;
            this.videoBlobUrl = this.sanitizer.bypassSecurityTrustUrl(data.url);
            this.ref.detectChanges();
            this.configuracion.videoRealizado(data)

        });


    }


    @HostListener('unloaded')
    ngOnDestroy(): void {
        // this.videoRecordingService.desconectarEscuchas()
    }

    ngOnInit() {
    }
    async ngAfterViewInit() {
        await this.setupDevicesVideo(this.camaraPrincipal);
    }


    async setupDevicesVideo(camaraPrincipal: boolean) {
        let facing: string = ''
        if (camaraPrincipal) {
            facing = 'environment'
        } else {
            facing = 'user'
        }
        this.videoConf.video.facingMode = facing

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

            try {
                this.stream = await navigator.mediaDevices.getUserMedia(this.videoConf);
                if (this.stream) {
                    this.videoElement.nativeElement.srcObject = this.stream;
                    this.videoElement.nativeElement.play();
                    this.error = null;
                } else {
                    this.error = "You have no output video device";
                }
            } catch (e) {
                this.error = e;
            }
        }
    }

    async cambiarCamara() {
        this.stream.stop()
        this.camaraPrincipal = !this.camaraPrincipal
        await this.setupDevicesVideo(this.camaraPrincipal)
    }

    cerrarCamara() {
        this.abortVideoRecording()
        this.stream.stop()
        this.configuracion.cerrarCamara()
    }

    startVideoRecording() {
        this.stream.stop()
        if (!this.isVideoRecording) {
            this.videoElement.nativeElement.controls = false;
            this.isVideoRecording = true;
            this.videoRecordingService.startRecording(this.videoConf)
                .then(stream => {
                    // this.video.src = window.URL.createObjectURL(stream);
                    this.videoElement.nativeElement.srcObject = stream;
                    this.videoElement.nativeElement.play();
                })
                .catch(function (err) {
                });
        }
    }

    abortVideoRecording() {
        if (this.isVideoRecording) {
            this.isVideoRecording = false;
            this.videoRecordingService.abortRecording();
            this.videoElement.nativeElement.controls = false;
        }
    }

    stopVideoRecording() {
        if (this.isVideoRecording) {
            this.videoRecordingService.stopRecording();
            // this.videoElement.nativeElement.srcObject = this.videoBlobUrl;
            // this.isVideoRecording = false;
            // this.videoElement.nativeElement.controls = true;

        }
    }

    clearVideoRecordedData() {
        this.videoBlobUrl = null;
        this.videoElement.nativeElement.srcObject = null;
        this.videoElement.nativeElement.controls = false;
        this.ref.detectChanges();
    }

    downloadVideoRecordedData() {

        this._downloadFile(this.videoBlob, 'video/mp4', this.videoName);
    }



    clearAudioRecordedData() {
        this.audioBlobUrl = null;
    }

    downloadAudioRecordedData() {
        this._downloadFile(this.audioBlob, 'audio/mp3', this.audioName);
    }



    _downloadFile(data: any, type: string, filename: string): any {
        const blob = new Blob([data], { type: 'video/mp4' });
        const url = window.URL.createObjectURL(blob);
        //this.video.srcObject = stream;
        //const url = data;
        const anchor = document.createElement('a');
        anchor.download = filename;
        anchor.href = url;
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    }

}
