import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { AccionesBuscadorLocalidadModal } from '@shared/diseno/enums';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos';
import { ItemSelector } from '@shared/diseno/modelos';
import { GeneradorId } from '@core/servicios/generales';
@Component({
  selector: 'app-lista-buscador-modal',
  templateUrl: './lista-buscador-modal.component.html',
  styleUrls: ['./lista-buscador-modal.component.scss']
})
export class ListaBuscadorModalComponent implements OnInit, AfterViewInit {
  @ViewChild('inputBuscador', { static: false }) inputBuscador: ElementRef

  @Input() configuracion: ConfiguracionBuscadorModal

  public barraBusqueda$: Observable<string>
  public stringBasico: string
  public idInterno: string
  public idListaBuscador: string

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId,
  ) {
    this.stringBasico = 'inputBuscador_'
    this.idListaBuscador = 'idListaBuscador_'
  }

  ngOnInit() {
    this.idInterno = this.generadorId.generarIdConSemilla()
    if (this.configuracion) {
      this.configuracion.resultado.puedeCargarMas = true
      this.configuracion.resultado.mostrarCargandoPequeno = false
    }
  }

  ngAfterViewInit(): void {
    this.configurarObservable()
  }

  configurarObservable() {
    this.barraBusqueda$ = fromEvent<any>(this.inputBuscador.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        debounceTime(500),
      )

    this.barraBusqueda$.subscribe(query => {
      this.configuracion.inputBuscador.valor = query
      this.buscar()
    })
  }

  buscar() {
    if (this.configuracion.evento && this.configuracion.inputBuscador.valor.length >= 3) {
      const data = {
        accion: AccionesBuscadorLocalidadModal.REALIZAR_BUSQUEDA,
        informacion: {
          pais: this.configuracion.pais.codigo,
          query: this.configuracion.inputBuscador.valor
        }
      }
      this.configuracion.evento(data)
    }
  }

  validarElegibleActivo(codigo: string) {
    if (this.configuracion.seleccionado) {
      return this.configuracion.seleccionado.codigo === codigo
    } else {
      return false
    }
  }

  seleccionarItem(item: ItemSelector) {
    if (this.configuracion.evento) {
      const data = {
        accion: AccionesBuscadorLocalidadModal.SELECCIONAR_ITEM,
        informacion: {
          item: item
        }
      }
      this.configuracion.evento(data)
    }
  }

  eventoModal(target: any) {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal') {
        if (this.configuracion.evento) {
          const data = {
            accion: AccionesBuscadorLocalidadModal.CERRAR_BUSCADOR,
          }
          this.configuracion.evento(data)
        }
        return
      }
    })
  }

  scrollEnListaElegibles() {
    const elemento: HTMLElement = document.getElementById(this.idListaBuscador + this.idInterno) as HTMLElement
    if (!elemento) {
      return
    }

    if (
      (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 3) &&
      this.configuracion.resultado.puedeCargarMas
    ) {
      this.configuracion.evento({
        accion: AccionesBuscadorLocalidadModal.CARGAR_MAS_RESULTADOS,
        informacion: {
          pais: this.configuracion.pais.codigo,
          query: this.configuracion.inputBuscador.valor
        }
      })
    }
  }
}

