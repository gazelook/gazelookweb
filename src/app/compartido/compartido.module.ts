import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {NgxCurrencyModule} from 'ngx-currency';
import {ImageCropperModule} from 'ngx-image-cropper';
import {WebcamModule} from 'ngx-webcam';
import {AnuncioFinanzasComponent} from './componentes/anuncio-finanzas/anuncio-finanzas.component';
import {AnuncioInformativoComponent} from './componentes/anuncio-informativo/anuncio-informativo.component';
import {AnuncioPublicitarioComponent} from './componentes/anuncio-publicitario/anuncio-publicitario.component';
import {AnuncioReflexivoComponent} from './componentes/anuncio-reflexivo/anuncio-reflexivo.component';
import {AppbarComponent} from '@shared/componentes';
import {AudioReproductorComponent} from './componentes/audio-reproductor/audio-reproductor.component';
import {BarraInferiorInlineComponent} from './componentes/barra-inferior-inline/barra-inferior-inline.component';
import {BarraInferiorComponent} from './componentes/barra-inferior/barra-inferior.component';
import {BuscadorModalComponent} from '@shared/componentes';
import {BuscadorComponent} from './componentes/buscador/buscador.component';
import {ButtonComponent} from '@shared/componentes';
import {CabeceraIdiomasComponent} from './componentes/cabecera-idiomas/cabecera-idiomas.component';
import {CamaraFotoComponent} from './componentes/camara-foto/camara-foto.component';
import {CamaraVideoComponent} from './componentes/camara-video/camara-video.component';
import {CamaraComponent} from './componentes/camara/camara.component';
import {CargandoComponent} from '@shared/componentes';
import {ComentarioComponent} from './componentes/comentario/comentario.component';
import {ContadorDiasComponent} from '@shared/componentes';
import {CropperComponent} from '@shared/componentes';
import {DialogoContenidoComponent} from '@shared/componentes';
import {DialogoInlineComponent} from '@shared/componentes';
import {DialogoComponent} from './componentes/dialogo/dialogo.component';
import {DoneComponent} from './componentes/done/done.component';
import {EnrutadorNavegacionComponent} from '@shared/componentes';
import {ImagenMundoComponent} from './componentes/imagen-mundo/imagen-mundo.component';
import {ImagenPatallaCompletaComponent} from './componentes/imagen-patanlla-completa/imagen-patanlla-completa.component';
import {InputComponent} from './componentes/input/input.component';
import {
  ItemBuscadorProyectoNoticiasComponent
} from './componentes/item-buscador-proyecto-noticias/item-buscador-proyecto-noticias.component';
import {ItemCirculoComponent} from '@shared/componentes';
import {ItemFechaComponent} from './componentes/item-fecha/item-fecha.component';
import {ItemIntercambioComponent} from './componentes/item-intercambio/item-intercambio.component';
import {ItemLinkComponent} from './componentes/item-link/item-link.component';
import {ItemListaContactosComponent} from './componentes/item-lista-contactos/item-lista-contactos.component';
import {ItemListaIntercambioComponent} from './componentes/item-lista-intercambio/item-lista-intercambio.component';
import {ItemLlamadaComponent} from './componentes/item-llamada/item-llamada.component';
import {ItemMenuPerfilesComponent} from './componentes/item-menu-perfiles/item-menu-perfiles.component';
import {ItemMenuComponent} from '@shared/componentes';
import {ItemPensamientoComponent} from './componentes/item-pensamiento/item-pensamiento.component';
import {ItemProyectosNoticiasComponent} from './componentes/item-proyectos-noticias/item-proyectos-noticias.component';
import {ItemRectanguloComponent} from '@shared/componentes';
import {LineaComponent} from './componentes/linea/linea.component';
import {ListaBuscadorModalComponent} from '@shared/componentes';
import {ListaContactoComponent} from './componentes/lista-contacto/lista-contacto.component';
import {ListaContactosCircularComponent} from './componentes/lista-contactos-circular/lista-contactos-circular.component';
import {ListaNoticiasComponent} from './componentes/lista-noticias/lista-noticias.component';
import {ListaProyectosComponent} from './componentes/lista-proyectos/lista-proyectos.component';
import {ListaSelectorTipoMonedaComponent} from './componentes/lista-selector-tipo-moneda/lista-selector-tipo-moneda.component';
import {ListaSelectorComponent} from '@shared/componentes';
import {ListaVerticalComponent} from './componentes/lista-vertical/lista-vertical.component';
import {ListoComponent} from './componentes/listo/listo.component';
import {MapaComponent} from './componentes/mapa/mapa.component';
import {MensajeArchivoComponent} from './componentes/mensaje-gaze/mensaje-archivo/mensaje-archivo.component';
import {MensajeAudioComponent} from './componentes/mensaje-gaze/mensaje-audio/mensaje-audio.component';
import {MensajeGazeComponent} from '@shared/componentes';
import {MensajeImagenComponent} from './componentes/mensaje-gaze/mensaje-imagen/mensaje-imagen.component';
import {MensajeNoticiaComponent} from './componentes/mensaje-gaze/mensaje-noticia/mensaje-noticia.component';
import {MensajeProyectoComponent} from './componentes/mensaje-gaze/mensaje-proyecto/mensaje-proyecto.component';
import {MensajeTextoComponent} from './componentes/mensaje-gaze/mensaje-texto/mensaje-texto.component';
import {AudioMensajeComponent} from './componentes/mensaje/audio-mensaje/audio-mensaje.component';
import {DocumentoMensajeComponent} from './componentes/mensaje/documento-mensaje/documento-mensaje.component';
import {ImagenMensajeComponent} from './componentes/mensaje/imagen-mensaje/imagen-mensaje.component';
import {MensajeComponent} from './componentes/mensaje/mensaje.component';
import {TextoMensajeComponent} from './componentes/mensaje/texto-mensaje/texto-mensaje.component';
import {ModalInferiorComponent} from '@shared/componentes';
import {ModalOrigenFotoComponent} from './componentes/modal-origen-foto/modal-origen-foto.component';
import {ModalTituloAudioComponent} from './componentes/modal-titulo-audio/modal-titulo-audio.component';
import {MonedaPickerComponent} from './componentes/moneda-picker/moneda-picker.component';
import {PensamientoCompartidoComponent} from '@shared/componentes';
import {PortadaExpandidaComponent} from '@shared/componentes';
import {PortadaGazeComponent} from '@shared/componentes';
import {PortadaGifComponent} from './componentes/portada-gif/portada-gif.component';
import {ReproductorVideoComponent} from './componentes/reproductor-video/reproductor-video.component';
import {ResumenPerfilComponent} from './componentes/resumen-perfil/resumen-perfil.component';
import {SelectorComponent} from '@shared/componentes';
import {TituloRectanguloComponent} from './componentes/titulo-rectangulo/titulo-rectangulo.component';
import {ToastComponent} from '@shared/componentes';
import {VotarEntidadComponent} from './componentes/votar-entidad/votar-entidad.component';
import {CrearComponenteDirective} from './directivas/crear-componente.directive';
import {EstiloListaDirective} from './directivas/estilo-lista.directive';
import {GestorEventosDirective} from './directivas/gestor-eventos.directive';
import {ConfirmacionPagoCriptoComponent} from './componentes/confirmacion-pago-cripto/confirmacion-pago-cripto.component';

import {CountdownModule} from '@env/node_modules/ngx-countdown';
import {ClipboardModule} from '@env/node_modules/ngx-clipboard';
import {ProyectoItemForoComponent} from './componentes/proyecto-item-foro/proyecto-item-foro.component';
import {ProyectoItemForoRecomendadoComponent} from './componentes/proyecto-item-foro-recomendado/proyecto-item-foro-recomendado.component';

@NgModule({
  imports: [
    NgxCurrencyModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WebcamModule,
    ImageCropperModule,
    TranslateModule,
    ClipboardModule,
    CountdownModule

  ],
  declarations: [
    CrearComponenteDirective,
    EstiloListaDirective,
    ListaVerticalComponent,
    CargandoComponent,
    ButtonComponent,
    InputComponent,
    ItemCirculoComponent,
    ItemRectanguloComponent,
    ItemMenuComponent,
    LineaComponent,
    AppbarComponent,
    DialogoComponent,
    PortadaGazeComponent,
    PensamientoCompartidoComponent,
    ItemPensamientoComponent,
    SelectorComponent,
    BuscadorModalComponent,
    CabeceraIdiomasComponent,
    CamaraComponent,
    CropperComponent,
    ToastComponent,
    ItemFechaComponent,
    DialogoContenidoComponent,
    ModalInferiorComponent,
    DialogoInlineComponent,
    ListoComponent,
    BarraInferiorComponent,
    GestorEventosDirective,
    BuscadorComponent,
    TituloRectanguloComponent,
    ItemListaContactosComponent,
    PortadaExpandidaComponent,
    MonedaPickerComponent,
    BarraInferiorInlineComponent,
    ModalTituloAudioComponent,
    AudioReproductorComponent,
    ItemProyectosNoticiasComponent,
    ResumenPerfilComponent,
    ListaContactosCircularComponent,
    MensajeComponent,
    TextoMensajeComponent,
    AudioMensajeComponent,
    ImagenMensajeComponent,
    DocumentoMensajeComponent,
    ListaNoticiasComponent,
    ItemLinkComponent,
    ModalOrigenFotoComponent,
    ComentarioComponent,
    ItemLlamadaComponent,
    ListaProyectosComponent,
    ListaContactoComponent,
    ImagenPatallaCompletaComponent,
    VotarEntidadComponent,
    ItemMenuPerfilesComponent,
    ContadorDiasComponent,
    ItemBuscadorProyectoNoticiasComponent,
    DoneComponent,
    ListaSelectorComponent,
    ListaBuscadorModalComponent,
    ListaSelectorTipoMonedaComponent,
    MensajeGazeComponent,
    MensajeTextoComponent,
    MensajeArchivoComponent,
    MensajeAudioComponent,
    MensajeImagenComponent,
    MensajeTextoComponent,
    MensajeProyectoComponent,
    MensajeNoticiaComponent,
    PortadaGifComponent,
    ImagenMundoComponent,
    EnrutadorNavegacionComponent,
    MapaComponent,
    ItemListaIntercambioComponent,
    ItemIntercambioComponent,
    AnuncioPublicitarioComponent,
    AnuncioInformativoComponent,
    AnuncioReflexivoComponent,
    AnuncioFinanzasComponent,
    ReproductorVideoComponent,
    CamaraFotoComponent,
    CamaraVideoComponent,
    ConfirmacionPagoCriptoComponent,
    ProyectoItemForoComponent,
    ProyectoItemForoRecomendadoComponent

  ],
  exports: [
    CrearComponenteDirective,
    EstiloListaDirective,
    ListaVerticalComponent,
    CargandoComponent,
    ButtonComponent,
    InputComponent,
    ItemCirculoComponent,
    ItemRectanguloComponent,
    ItemMenuComponent,
    LineaComponent,
    AppbarComponent,
    DialogoComponent,
    PortadaGazeComponent,
    PensamientoCompartidoComponent,
    ItemPensamientoComponent,
    SelectorComponent,
    BuscadorModalComponent,
    CabeceraIdiomasComponent,
    CamaraComponent,
    CropperComponent,
    ToastComponent,
    ItemFechaComponent,
    DialogoContenidoComponent,
    ModalInferiorComponent,
    DialogoInlineComponent,
    BarraInferiorComponent,
    GestorEventosDirective,
    BuscadorComponent,
    TituloRectanguloComponent,
    ItemListaContactosComponent,
    TranslateModule,
    PortadaExpandidaComponent,
    MonedaPickerComponent,
    BarraInferiorInlineComponent,
    ModalTituloAudioComponent,
    AudioReproductorComponent,
    ItemProyectosNoticiasComponent,
    ResumenPerfilComponent,
    ListaContactosCircularComponent,
    MensajeComponent,
    ListaNoticiasComponent,
    ItemLinkComponent,
    ModalOrigenFotoComponent,
    ComentarioComponent,
    ItemLlamadaComponent,
    ListaProyectosComponent,
    ListaContactoComponent,
    ImagenPatallaCompletaComponent,
    VotarEntidadComponent,
    ItemMenuPerfilesComponent,
    ContadorDiasComponent,
    ItemBuscadorProyectoNoticiasComponent,
    NgxCurrencyModule,
    DoneComponent,
    ListaSelectorComponent,
    ListaBuscadorModalComponent,
    ListaSelectorTipoMonedaComponent,
    MensajeGazeComponent,
    MensajeTextoComponent,
    MensajeArchivoComponent,
    MensajeAudioComponent,
    MensajeImagenComponent,
    MensajeTextoComponent,
    MensajeProyectoComponent,
    MensajeNoticiaComponent,
    PortadaGifComponent,
    ImagenMundoComponent,
    EnrutadorNavegacionComponent,
    MapaComponent,
    ItemListaIntercambioComponent,
    ItemIntercambioComponent,
    AnuncioPublicitarioComponent,
    AnuncioInformativoComponent,
    AnuncioReflexivoComponent,
    AnuncioFinanzasComponent,
    ReproductorVideoComponent,
    ConfirmacionPagoCriptoComponent,
    ProyectoItemForoComponent,
    ProyectoItemForoRecomendadoComponent
  ]
})

export class CompartidoModule {
}
