// Para los colores de fondo del item en el menu
export enum ColorFondoItemMenu {
    PREDETERMINADO = 'tranparencia-base',
    PERFILCREADO = 'perfil-creado',
    PERFILHIBERNADO = 'perfil-hibernado',
    TRANSPARENTE = ""
}

export enum ColorFondoLinea {
    FONDOLINEAVERDE = 'fondo-verde-base',
    FONDOLINEABLANCO = 'fondo-blanco',
    FONDOLINEACELESTE = 'fondo-celeste'
}

export enum EspesorLineaItem {
    ESPESOR012 = 'espesor-012',
    ESPESOR041 = 'espesor-041',
    ESPESOR071 = 'espesor-071',
    ESPESOR089 = 'espesor-089',
}

export enum ColorDelTexto {
    TEXTOBLANCO = 'txt-blanco',
    TEXTONEGRO = 'txt-negro',
    TEXTOAMARILLOBASE = 'txt-amarillo-base',
    TEXTOAMARILLOTITULOPENSA = 'txt-amarillo-titulu-pensa',
    TEXTOROJOBASE = 'txt-rojo-base',
    TEXTOVERDEBASE = 'txt-verde-base',
    TEXTOAZULBASE = 'txt-azul-base',
    TEXTOAZULCLARO = 'txt-azul-claro',
    TEXTOBOTONHISTORY = 'txt-btn-history',
    TEXTOBOTONBLANCO = 'txt-btn-blanco',
    TEXTOCELESTE = 'txt-celeste',
    TEXTOLACRE = 'txt-lacre',
    TEXTOTITULOFECHA = 'titulo-fecha',
    TEXTOERRORFORMULARIO = 'error-formulario',
    TEXTO_COLOR_PLATEADO = 'txt-color-plateado',
    AZUL_HIBERNATE = 'txt-color-azul-hibernate',
    GRIS = "txt-color-gris",
    TEXTOGRISDEMO = 'txt-color-gris-demo',
    TEXTOMARRON = 'txt-color-marron-demo',
    TEXTOAZULMEDIO = 'txt-azul-medio',
    TEXTOAMARILLOMEDIO = 'txt-amarillo-medio',
    TEXTOCELESTEMAIN = 'txt-celeste-texto-main',
    TEXTOPERFILCREADO = 'txt-perfil-creado',
    TEXTO_ROJO_INTERCAMBIO_POR = 'texto-rojo-intercambio-por',
    TEXTO_AVISO_GUAR_AUTO = 'txt-amarillo-aviso-guardado-auto',
    TEXTO_LINK_VISITADO = 'txt-link-visitado'
    
}


export enum EspesorDelBorde {
    ESPESOR_018 = 'espesor-del-borde-018'
}

export enum ColorDeBorde {
    BORDER_TRANSPARENTE = 'borde-transparente',
    BORDER_BLANCO = 'borde-blanco',
    BORDER_ROJO = 'borde-rojo',
    BORDER_AZUL = 'borde-azul',
    BORDER_AMARILLO = 'borde-amarillo',
    BORDER_NEGRO = 'borde-negro',
    BORDER_SEMI_AMARILLO = 'borde-semi-amarillo',
}

export enum ColorDeFondo {
    FONDO_TRANSPARENTE = 'fondo-transparente',
    FONDO_BLANCO = 'fondo-blanco',
    FONDO_AZUL_CLARO = 'fondo-azul-claro',
    FONDO_TRANSPARENCIA_BASE = 'fondo-transparencia-base',
    FONDO_AMARILLO_CLARO = 'fondo-amarillo-claro',
    FONDO_AZUL_CON_OPACIDAD = 'fondo-azul-base-con-opacidad',
    FONDO_CELESTE_CON_OPACIDAD = 'fondo-celeste-con-opacidad',
    FONDO_AMARILLO_BASE = 'fondo-amarillo-base',
    FONDO_AZUL_BASE = 'fondo-azul-base',
}

export enum ColorFondoAleatorio{
    FONDO_ALEATORIO_1 = 'fondo_aleatorio_1',
    FONDO_ALEATORIO_2 = 'fondo_aleatorio_2',
    FONDO_ALEATORIO_3 = 'fondo_aleatorio_3',
    FONDO_ALEATORIO_4 = 'fondo_aleatorio_4',
    FONDO_ALEATORIO_5 = 'fondo_aleatorio_5',
    FONDO_ALEATORIO_6 = 'fondo_aleatorio_6',
    FONDO_ALEATORIO_7 = 'fondo_aleatorio_7',
}


export enum EstilosDelTexto {
    REGULAR = 'regular',
    SEMI_BOLD = 'semi-bold',
    BOLD = 'bold',
    ITALIC = 'italic',
    REGULARCONSOMBRA = 'regularSombra',
    BOLDCONSOMBRA = 'boldSombra' ,
    REGULARENITALIC = 'regularItalic',
    BOLDENITALIC = 'boldItalic',
    REGULARENITALICCONSOMBRA = 'regularItalicSombra',
    BOLDENITALICCONSOMBRA = 'boldItalicSombra',
    BOLDCONSOMBRAMENU = 'boldSombraMenuPri'
}

export enum EstiloErrorInput {
    ROJO='error txt-rojo-base',
    BLANCO='error txt-blanco',
    AMARILLO='error txt-amarillo-medio',
}