export enum EstiloInput {
    REGISTRO = 'registro',
    REGISTRO_NOMBRE_CONTACTO = 'registro-nombre-contacto',
    LOGIN = 'login',
    DEFECTO = "defecto",
    PROYECTO = 'proyecto',
    PROYECTO_CENTRADO = 'proyecto-centrado',
    PROYECTO_MINUSCULA = 'proyecto-minuculas',
    PROYECTO_AUTOR = 'proyecto-minuculas-autor',
    TITULO_AUDIO = 'titulo-audio',
    CONTACTO = 'contacto',
    MI_CUENTA = 'mi-cuenta'
}
