
export enum EspesorDelBorde {
    ESPESOR_018 = 'espesor-del-borde-018'
}

export enum ColorDeBorde {
    BORDER_TRANSPARENTE = 'borde-transparente',
    BORDER_BLANCO = 'borde-blanco',
    BORDER_ROJO = 'borde-rojo',
    BORDER_AZUL = 'borde-azul', 
    BORDER_AMARILLO = 'borde-amarillo',
    BORDER_NEGRO = 'borde-negro',
    BORDER_SEMI_AMARILLO = 'borde-semi-amarillo',
}

// Cuando el album de perfil no tiene foto de portada, se va a asignar un color de fondo debajo del icono del rostro,
// el color de fondo es de origen aleatorio.
// Para anadir un nuevo color de fondo, anadir el valor al enum, enviar en la configuracion y anadir la clase al archivo .scss del componente item circular o rectangular
export enum ColorDeFondo {
    FONDO_TRANSPARENTE = 'fondo-transparente',
    FONDO_BLANCO = 'fondo-blanco',
    FONDO_AZUL_CLARO = 'fondo-azul-claro',
    FONDO_TRANSPARENCIA_BASE = 'fondo-transparencia-base',
    FONDO_AMARILLO_CLARO = 'fondo-amarillo-claro',
    FONDO_AZUL_CON_OPACIDAD = 'fondo-azul-base-con-opacidad',
    FONDO_CELESTE_CON_OPACIDAD = 'fondo-celeste-con-opacidad',
    FONDO_AMARILLO_BASE = 'fondo-amarillo-base',
    FONDO_MENSAJE_ENVIADO = 'fondo-mensaje-recibido',
    FONDO_MENSAJE_RECIBIDO = 'fondo-mensaje-enviado',
    FONDO_AZUL_BASE = 'fondo-azul-base',
}

export enum ColorFondoAleatorio{
    FONDO_ALEATORIO_1 = 'fondo_aleatorio_1',
    FONDO_ALEATORIO_2 = 'fondo_aleatorio_2',
    FONDO_ALEATORIO_3 = 'fondo_aleatorio_3',
    FONDO_ALEATORIO_4 = 'fondo_aleatorio_4',
    FONDO_ALEATORIO_5 = 'fondo_aleatorio_5',
    FONDO_ALEATORIO_6 = 'fondo_aleatorio_6',
    FONDO_ALEATORIO_7 = 'fondo_aleatorio_7',
}