export enum TipoIconoBarraInferior {
    ICONO_TEXTO = 'icono-texto',
    ICONO_ARCHIVO = 'icono-archivo',
    ICONO_CAMARA = 'icono-camara',
    ICONO_LLAMADA = 'icono-llamada',
    ICONO_VIDEO_LLAMADA = 'icono-video-llamada',
    ICONO_AUDIO = 'icono-audio',
}