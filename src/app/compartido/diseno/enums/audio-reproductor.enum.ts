export enum DireccionDelReproductor {
    HACIA_LA_IZQUIERDA = 'hacia-la-izquierda',
    HACIA_LA_DERECHA = 'hacia-la-derecha'
}