export interface ConfiguracionImagenPantallaCompleta {
  mostrar?: boolean,
  mostrarLoader?: boolean,
  urlMedia?: string,
  eventoBotonCerrar?: Function
  eventoBotonDescargar?: Function
}
