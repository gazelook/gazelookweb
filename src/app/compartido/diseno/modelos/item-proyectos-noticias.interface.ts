import { ColorDeBorde } from '@shared/diseno/enums';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums';
import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos';
type eventoType = (arg: any) => any;


export interface EventoTap {
  activo: boolean;
  evento?: eventoType;
}

export interface CongifuracionItemProyectosNoticias {
  usoVersionMini?: boolean;
  id: string;
  titulo?: {
    mostrar: boolean;
    configuracion?: CongifuracionTituloRectangulo;
  };
  colorDeBorde: ColorDeBorde;
  urlMedia?: string;
  loader?: boolean;
  colorDeFondo?: string;
  fecha?: {
    mostrar: boolean;
    configuracion?: {
      fecha: Date;
      formato: string;
    };
  };
  etiqueta?: {
    mostrar: boolean;
    titulo?: string;
  };
  eventoTap: EventoTap;
  eventoDobleTap: EventoTap;
  eventoPress: EventoTap;
  usoItem: UsoItemProyectoNoticia;
  tipo?: string;
  actualizado?: boolean;
  resumen?: boolean;
  mostrarCorazon?: boolean;
  textoGeneral?: string;
  capaDemo?: boolean;
  noDisponible?: boolean;
  usarMarcosDeConfiguracion?: boolean;
  totalVotos?: number;
}
