import { ColorDelTexto, EstilosDelTexto } from '@shared/diseno/enums';
import { PaddingIzqDerDelTexto } from '@shared/diseno/enums';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums';

// Configuracion de cada linea de texto que aparece en el item
export interface ConfiguracionTexto {
  color: ColorDelTexto, // Color del texto
  estiloTexto: EstilosDelTexto, // Indica el estilo del texto
  enMayusculas: boolean, // Indica si el texto se debe mostrar todo en mayusculas
  tamano?: TamanoDeTextoConInterlineado, // Tamano de letra con interlineado por defecto que pone el navegador
  tamanoConInterlineado?: TamanoDeTextoConInterlineado, // Tamano del texto con interlineado personalizado
  paddingIzqDerDelTexto?: PaddingIzqDerDelTexto, // Indica el valor del padding a la izquierda y derecha del texto
  enCapitalize?: boolean, // Texto en Capitalize
  textoOverflow?: boolean, // Texto que sobresale del div, añadir puntitos al final
}
