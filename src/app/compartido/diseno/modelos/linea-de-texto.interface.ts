import { ColorDelTexto, EstilosDelTexto } from '@shared/diseno/enums';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums';

// Configuracion de cada linea de texto que aparece en el item
export interface LineaDeTexto {
  texto?: string, // Texto a ser mostrado
  tamanoConInterlineado: TamanoDeTextoConInterlineado, // Tamano del texto con interlineado
  color: ColorDelTexto, // Color del texto
  enMayusculas: boolean, // Indica si el texto se debe mostrar todo en mayusculas
  estiloTexto: EstilosDelTexto, // Indica el estilo del texto
}
