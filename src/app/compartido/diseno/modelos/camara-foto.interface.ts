import { MensajeError } from "@shared/diseno/modelos/error.interface";
export interface ConfiguracionCamaraFoto {
    mostrarModal?: boolean,
    mostrarLoader?: boolean,
    mensajeError?: MensajeError,
    usarCroopper?: boolean,
    fotoRealizada?: Function,
    cerrarCamara?:  Function
}
