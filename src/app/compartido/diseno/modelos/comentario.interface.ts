import { ComentarioFirebaseModel } from 'dominio/modelo/entidades';
import { PerfilModel } from 'dominio/modelo/entidades';
import { ConfiguracionAudioReproductor } from './audio-reproductor.interface';
import { ItemCircularCompartido } from './item-cir-rec.interface';
export interface ComentarioRespuesta {
  idPerfilRespuesta: PerfilModel,
  comentario: ComentarioFirebaseModel,
  itemCirculo: ItemCircularCompartido,
}
export interface ConfiguracionComentario {
  fecha?: Date,
  coautor?: PerfilModel,
  idInterno?: string,
  esPropietario?: boolean,
  puedeBorrar?: boolean,
  comentarios?: Array<ComentarioRespuesta>,
  itemCirculo?: ItemCircularCompartido,
  configuracionAdjuntos?: Array<{
    id: string,
    confAdjunto: ConfiguracionAudioReproductor
  }>,
  couatorSeleccionado?: boolean,
  inglesActivo?: boolean,
  textoComentario?: string,
  eventoTap?: Function,
  eventoTapEnPerfil?: Function,
  eventoDobleTap?: Function,
  eventoPress?: Function,
}
