import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { ConfiguracionItemIntercambio } from '@shared/diseno/modelos/item-intercambio.interface';

export interface ConfiguracionItemListaIntercambio {
  codigoEntidad?: CodigosCatalogoEntidad,
  intercambio?: IntercambioModel
  configuracionRectangulo?: ConfiguracionItemIntercambio,
  eventoTap?: Function
}
