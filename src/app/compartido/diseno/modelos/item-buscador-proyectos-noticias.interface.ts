import { NoticiaModel } from 'dominio/modelo/entidades';
import { ProyectoModel } from 'dominio/modelo/entidades';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos';
export interface ConfiguracionItemBuscadorProyectoNoticia {
  codigoEntidad?: CodigosCatalogoEntidad,
  noticia?: NoticiaModel,
  proyecto?: ProyectoModel,
  configuracionRectangulo?: CongifuracionItemProyectosNoticias,
  eventoTap?: Function
}
