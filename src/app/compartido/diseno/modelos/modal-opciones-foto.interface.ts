import { OrigenFoto } from '@shared/diseno/enums/origen-foto.enum';
export interface ConfiguracionModalOrigenFoto {
  mostrar: boolean,
  origenFoto: OrigenFoto,
  eventoTap?: Function,
  eventoDobleTap?: Function
}
