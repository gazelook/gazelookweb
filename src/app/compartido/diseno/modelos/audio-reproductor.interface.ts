import { MediaModel } from 'src/app/dominio/modelo/entidades/media.model';
import { DireccionDelReproductor } from '@shared/diseno/enums';
import { ColorDeFondo } from '@shared/diseno/enums/estilo-colores.enum';
export interface TimpoReproduccion {
  actual: number,
  total: number,
  factor: number,
}
export interface PropiedadCustom {
  usar: boolean,
  valor?: string,
}
export interface ConfiguracionAudioReproductor {
  idInterno?: string,
  mostrarTitulo?: boolean,
  mostrarLoader?: boolean,
  colorDeFondo?: ColorDeFondo,
  direccion?: DireccionDelReproductor,
  reproduciendo?: boolean,
  media?: MediaModel,
  tiempoReproduccion?: TimpoReproduccion,
  colorDeFondoCustom?: string,
  anchoCompleto?: boolean,
  preload?: boolean,
  esComentarioRespuesta?: boolean,
  eventoTap?: Function,
  eventoDobleTap?: Function,
  eventoPress?: Function
}
