import { DatosLista } from '@shared/diseno/modelos';
import { DialogoCompartido } from '@shared/diseno/modelos';
export interface ConfiguracionListaContactoCompartido {
  listaContactos: DatosLista,
  botonAccion?: DialogoCompartido,
  contactoSeleccionado?: Function,
  cargarMas?: Function,
  mostrar?: boolean
  llaveSubtitulo?: string,
}
