import { TamanoColorDeFondo } from '@shared/diseno/enums';
import { ColorDeFondo } from '@shared/diseno/enums';
import { TipoIconoBarraInferior } from '@shared/diseno/enums';
import { ConfiguracionTexto } from '@shared/diseno/modelos';
export interface IconoBarraInferior {
  tipo: TipoIconoBarraInferior,
  mostrar: boolean,
  disabled?: boolean,
  eventoTap?: Function,
  eventoDobleTap?: Function,
  eventoPress?: Function,
  idConversacion?: string
}
export interface CapaColorDeFondo {
  mostrar: boolean,
  anchoCapa?: TamanoColorDeFondo,
  colorDeFondo?: ColorDeFondo
}
export interface ContadorCaracteres {
  mostrar: boolean,
  maximo: number,
}
export interface CapaContenido {
  mostrar?: boolean,
  siempreActiva?: boolean,
  informacion?: any,
  grabadora?: ConfiguracionGrabadora,
  contador?: ContadorCaracteres,
  clickParar?: boolean
}
export interface CapaIconoContenido {
  icono: IconoBarraInferior,
  capa?: CapaContenido
}
export interface Boton {
  mostrar: boolean,
  disabled?: boolean,
  evento?: Function
}
export interface ConfiguracionGrabadora {
  usarLoader: boolean, // Indica si usar el loader o no al momento de capturar la grabacion
  mostrarLoader?: boolean,
  grabando: boolean, // Indica si esta grabando o no
  duracionActual: number, // Tiempo en segundos
  tiempoMaximo: number, // Tiempo en segundos
  factorAumentoLinea: number, // Es el factor para aumentar el recorrido de la bolita del tiempo
  noPedirTituloGrabacion?: boolean // Si es true, no pide el titulo al finalizar la grabacion
}
export interface PlaceholderCentral {
  mostrar: boolean,
  texto?: string,
  configuracion?: ConfiguracionTexto,
}
export interface ConfiguracionBarraInferiorInline {
  desactivarBarra?: boolean,
  capaColorFondo: CapaColorDeFondo,
  estatusError: Function,
  placeholder?: PlaceholderCentral,
  iconoTexto?: CapaIconoContenido,
  iconoArchivo?: CapaIconoContenido,
  iconoCamara?: CapaIconoContenido,
  iconoLlamada?: CapaIconoContenido,
  iconoVideoLlamada?: CapaIconoContenido,
  iconoAudio?: CapaIconoContenido,
  botonSend?: Boton,
}
