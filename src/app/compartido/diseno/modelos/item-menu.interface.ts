import { ItemAccion, ItemMenuModel } from 'dominio/modelo/entidades';
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { ColorDelTexto, ColorFondoItemMenu } from '@shared/diseno/enums';
import { TamanoItemMenu } from '@shared/diseno/enums';
import { LineaDeTexto } from '@shared/diseno/modelos';
import { LineaCompartida } from '@shared/diseno/modelos';

// Configuracion del item menu
export interface ItemMenuCompartido {
  id: string; // Id del item
  tamano: TamanoItemMenu; // Indica el tamano del item (altura)
  colorFondo: ColorFondoItemMenu; // El color de fondo que tendra el item
  //lineasTexto: Array<LineaDeTexto>, // Todas las lineas de texto a ser mostradas en el item
  colorTexto?: ColorDelTexto;
  linea: {
    mostrar: boolean; // Indica si se debe mostrar o no la linea
    configuracion?: LineaCompartida; // Configuracion de la linea
  };
  gazeAnuncios?: boolean; // Indica si el item es para Gazelook Announcemente, lo hice como variable porque es un caso en particular
  idInterno?: string;
  mostrarDescripcion?: boolean;

  onclick?: Function;
  dobleClick?: Function;
  clickSostenido?: Function;

  tipoMenu: TipoMenu;
  texto1?: string;
  texto2?: string;
  texto3?: string;
  texto4?: string;
  texto5?: string;
  texto6?: string;
  descripcion?: Array<LineaDeTexto>;
  submenus?: ItemMenuModel[];
  acciones?: ItemAccion[];
  mostrarConrazon?: boolean;
}
