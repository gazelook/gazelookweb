import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos';
import { MediaModel } from 'dominio/modelo/entidades';

export interface ConfiguracionItemLink {
  entidad: CodigosCatalogoEntidad,
  fueVisitado?: boolean,
  esPortada?: boolean,
  media?: MediaModel,
  eventoTap?: Function,
  eventoDobleTap?: Function,
  eventoPress?: Function,
  albumPredeterminado?: boolean
}
