import { TipoMensaje } from '@shared/diseno/enums/mensaje.enum';
import { MediaEntity } from 'dominio/entidades';
import { CatalogoEntidadModel } from 'dominio/modelo/catalogos';
import { MediaModel } from 'dominio/modelo/entidades';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades';
import { TipoLlamada } from '@shared/diseno/enums/llamada-agora.enum';
import { BotonCompartido } from '@shared/diseno/modelos';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos';
export interface ConfiguracionMensajeCompartido {
  id?: string
  identificadorTemporal?: number
  contenido?: string
  propietario?: ParticipanteAsociacionModel
  fechaCreacion?: Date
  enviado?: boolean,
  reenvio?: boolean,
  eventoTap?: Function
  eventoDobleTap?: Function
  eventoPress?: Function
  loader?: boolean
  mostrar?: boolean
  error?: boolean
  tipoMensaje?: string
  estatusMensaje?: string
  referenciaEntidadCompartida?: any
  entidadCompartida?: CatalogoEntidadModel
}
export interface ConfiguracionMensaje extends ConfiguracionMensajeCompartido {
  adjuntos?: Array<MediaEntity>,
  listaConfiguracionImagenes?: Array<ImagenMensajeCompartido>
  listaConfiguracionAudios?: Array<AudioMensajeCompartido>
  listaConfiguracionDocumentos?: Array<DocumentoMensajeCompartido>
  listaConfiguracionMensajeCompartidoProyecto?: Array<MensajeEntidadCompartido>
  listaConfiguracionMensajeCompartidoNoticia?: Array<MensajeEntidadCompartido>
}
export interface AudioMensajeCompartido extends ConfiguracionMensajeCompartido {
  adjunto: MediaModel
}
export interface DocumentoMensajeCompartido extends ConfiguracionMensajeCompartido {
  adjunto: MediaModel
}
export interface ImagenMensajeCompartido extends ConfiguracionMensajeCompartido {
  adjunto: MediaModel
  mostrarLoader?: boolean
}
export interface TextoMensajeCompartido extends ConfiguracionMensajeCompartido {

}
export interface MensajeEntidadCompartido extends ConfiguracionMensajeCompartido {
  configuracionEntidad: CongifuracionItemProyectosNoticias,
  listaAcciones?: Array<BotonCompartido>,
  infoAccion?: string,
}

export interface LlamadaMensajeCompartido extends ConfiguracionMensajeCompartido {
  tipoLlamada: TipoLlamada
}
export interface MensajeAccion {
  aceptarTransferPro?: boolean
  id?: string,
  url?: string,
  tipoMensaje?: TipoMensaje,
}
