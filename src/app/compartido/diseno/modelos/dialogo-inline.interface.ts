import { ConfiguracionTexto } from '@shared/diseno/modelos';
import { BotonCompartido } from '@shared/diseno/modelos';
export interface ConfiguracionDialogoInline {
  noMostrar?: boolean,
  sinMargenEnDescripcion?: boolean,
  anchoParaProyectos?: boolean,
  descripcion: Array<{
    texto: string,
    estilo: ConfiguracionTexto
  }>,
  listaBotones: Array<BotonCompartido>,
}
