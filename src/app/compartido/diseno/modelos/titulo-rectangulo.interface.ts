import { ColorDeBorde, ColorDeFondo } from '@shared/diseno/enums';
export interface CongifuracionTituloRectangulo {
  textoBoton1?: string; // Texto del boton que se muestra en el item
  textoBoton2?: string;
  textoBoton3?: string;
  colorBorde?: ColorDeBorde; // Indica el color de borde a usar en el item
  colorDeFondo?: ColorDeFondo; // Indica el color de fondo a usar en el item
}
