import { DatosLista } from '@shared/diseno/modelos';
import { AlturaResumenPerfil } from '@shared/diseno/enums';
import { BotonCompartido } from '@shared/diseno/modelos';
import { ItemCircularCompartido } from '@shared/diseno/modelos';
import { PensamientoCompartido } from '@shared/diseno/modelos';
export interface ConfiguracionResumenPerfil {
  configCirculoFoto?: ItemCircularCompartido,
  alturaModal: AlturaResumenPerfil,
  configuracionPensamiento?: PensamientoCompartido,
  titulo?: string,
  botones?: Array<BotonCompartido>,
  confDataListaPensamientos?: DatosLista,
  mostrar?: boolean,
  loader?: boolean
  error?: boolean,
  reintentar?: Function,
  sonContactos?: boolean
}
