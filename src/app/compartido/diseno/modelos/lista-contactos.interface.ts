import { PaddingLineaVerdeContactos } from '@shared/diseno/enums';
import { UsoItemListaContacto } from '@shared/diseno/enums';
import { ItemCircularCompartido } from '@shared/diseno/modelos';
import { LineaCompartida } from '@shared/diseno/modelos';
import { ConfiguracionTexto } from '@shared/diseno/modelos';
export interface DataContacto {
  nombreContacto: string
  nombre?: string
  nombreContactoTraducido?: string,
  contacto?: boolean
  idPerfil?: string
  estilosTextoSuperior?: ConfiguracionTexto
  estilosTextoInferior?: ConfiguracionTexto
}
export interface DataMensaje {
  contacto?: DataContacto
  contadorMensajes?: number
  mensaje?: string,
  estilosTextoSuperior?: ConfiguracionTexto
  estilosTextoInferior?: ConfiguracionTexto
}
export interface ConfiguracionLineaVerde extends LineaCompartida {
  paddingLineaVerde?: PaddingLineaVerdeContactos,
}

// TRUE PARA NEGRO FALSE PARA ROJO EN COLOR
export interface ConfigCruzContacto {
  mostrar?: boolean,
  color?: boolean
}
export interface ConfiguracionItemListaContactosCompartido {
  id: string,
  usoItem: UsoItemListaContacto
  contacto?: DataContacto
  mensaje?: DataMensaje
  configCirculoFoto: ItemCircularCompartido
  mostrarX: ConfigCruzContacto
  mostrarCorazon?: boolean
  configuracionLineaVerde?: ConfiguracionLineaVerde
  overflowTexto?: boolean
  mostrarTextoInfo?: boolean
  eventoCirculoNombre?: Function
  eventoDobleTap?: Function,
  eventoIconoX?: Function
}
export interface ConfiguracionListaContactosCircular {
  cargando?: boolean,
  id?: string,
  titulo?: string;
  subtitulo?: string,
  info?: string,
  esMiPerfil?: boolean,
  resumen?: boolean,
  modal?: boolean,
  evento?: Function,
  eventoScroll?: Function,
  contactos?: Array<any>
  ultimaPagina?: boolean
}

