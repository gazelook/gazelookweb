import { AnchoLineaItem } from "@shared/diseno/enums";
import { ColorFondoLinea, EspesorLineaItem } from '../enums/estilos-colores-general';

// Configuracion de la linea
export interface LineaCompartida {
  ancho: AnchoLineaItem,
  espesor: EspesorLineaItem,
  colorFondo: ColorFondoLinea,
  forzarAlFinal?: boolean, // Ubica la linea con posicion absoluta al final del elemento padre, tomar en cuenta que el padre debe tener position relative
  cajaGaze?: boolean,
}
