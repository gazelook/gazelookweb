import { EstiloInput } from "@shared/diseno/enums";
import { EstiloErrorInput } from '@shared/diseno/enums';
export interface InputCompartido {
  placeholder?: string, //Texto del placeholder del input
  estilo?: TipoEstilo, //Estilo que tiene se le va a dar un input
  tipo?: string, //Typo text, password, email etc
  error?: boolean, //Enviar por defecto false y cuando el usuario no presione el input y al presionar un boton enviar true
  //error:string,
  bloquearCopy: boolean,
  ocultarMensajeError?: boolean,
  data?: any,
  contadorCaracteres?: { // Data para el contador de caracteres
    mostrar: boolean,
    numeroMaximo: number,
    contador: number,
  },
  id?: string, // identificador unico del input, necesario para validar
  validarCampo?: { // Para validador de campo unico, el input emite al componente padre cuando pierde el focus (blur), con el valor del input y su id
    validar: boolean, // Indica si se debe o no validar el valor del campo al perder el focus
    validador?: Function // Validar campo
  },
  errorPersonalizado?: string, // Cuando se requiere mostrar un error personalizado
  soloLectura?: boolean, // true indica que el input es de solo lectura, false indica que el input es editable
  soloLecturaRegistro?: boolean,
  contrasena?: boolean,
  esInputParaFecha?: boolean
}
export interface TipoEstilo {
  estiloInput: EstiloInput,
  estiloError: EstiloErrorInput
}
