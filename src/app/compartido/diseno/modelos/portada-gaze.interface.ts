import { TamanoPortadaGaze } from "@shared/diseno/enums";
export interface PortadaGazeCompartido {
  tamano: TamanoPortadaGaze,
  espacioDerecha?: boolean,
  mostrarLoader?: boolean,
  imagenAleatoriaRamdon?: boolean
}
