import { ColorDeBorde } from '@shared/diseno/enums';
import { UsoItemIntercambio } from '@shared/diseno/enums/uso-item-intercambio.enum';
import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos';
export interface EventoTap {
  activo: boolean;
  evento?: Function;
}
export interface ConfiguracionItemIntercambio {
  usoVersionMini?: boolean,
  id: string;
  titulo?: {
    mostrar: boolean;
    configuracion?: CongifuracionTituloRectangulo;
  };
  colorDeBorde: ColorDeBorde;
  urlMedia?: string;
  loader?: boolean;
  colorDeFondo?: string;
  fecha?: {
    mostrar: boolean;
    configuracion?: {
      fecha: Date;
      formato: string;
    };
  };
  etiqueta?: {
    mostrar: boolean;
    titulo?: string;
  };
  eventoTap: EventoTap;
  eventoDobleTap: EventoTap;
  eventoPress: EventoTap;
  usoItem: UsoItemIntercambio;
  tipo?: string;
  actualizado?: boolean;
  resumen?: boolean;
  mostrarCorazon?: boolean,
  textoGeneral?: string,
  capaDemo?: boolean,
  noDisponible?: boolean,
  usarMarcosDeConfiguracion?: boolean
}
