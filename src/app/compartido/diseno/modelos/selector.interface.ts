import {ItemSelector} from '@shared/diseno/modelos';
import {MensajeError} from '@shared/diseno/modelos';

export interface SelectorCargando {
  mostrar: boolean; // Indica si mostrar el cargando o no
  // Lo dejo como interfaz en caso se necesite mas a futuro
}

export interface SelectorModalInput {
  placeholder: string;
  valor: string;
  textoBold?: boolean;
  textoEnMayusculas?: boolean;
  desactivar?: boolean;
}

export interface SelectorModalInputPreview {
  mostrar: boolean;
  input: SelectorModalInput;
  error?: boolean;
  mensajeError?: string;
}

export interface ConfiguracionSelector {
  tituloSelector: string; // Titulo a mostrar en el selector
  mostrarModal: boolean; // Indica si mostrar o no el selector
  inputPreview: SelectorModalInputPreview;
  seleccionado: ItemSelector; // Item Seleccionado
  elegibles: Array<ItemSelector>; // Valores posibles a elegir,
  cargando: SelectorCargando; // Mostrar loader o no
  error: MensajeError; // Contenido del error
  quitarMarginAbajo?: boolean; // Al ser true, anade por defecto un margin bottom de 2.49
  evento?: Function;
  tipoIntercambio?: boolean;
  tipoFinanzas?: boolean;
}
