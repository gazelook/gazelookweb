import { MensajeError } from "@shared/diseno/modelos";
export interface ConfiguracionCamara {
  mostrarModal: boolean,
  mostrarLoader: boolean,
  mensajeError: MensajeError,
  usarCroopper?: boolean,
  fotoCapturada?: Function,
}
export interface ConfiguracionCropper {
  mostrarModal: boolean,
  mostrarLoader: boolean,
  mostrarCaja: boolean,
  mensajeError: MensajeError,
  imageURL?: any,
  imageChangedEvent?: any,
  imageFile?: any,
  imageBase64?: string,
  imagenCortada?: Function,
}
