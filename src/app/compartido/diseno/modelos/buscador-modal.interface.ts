import { ItemSelector } from '@shared/diseno/modelos'
import { MensajeError } from './error.interface'
export interface BuscadorModalInput {
  placeholder: string, // Placeholder del input donde se ingresa el texto que se va a mandar a buscar
  valor: string, // Texto que se va a mandar a buscar
  auxiliar?: string, // Para valores adicionales
  textoBold?: boolean,
  textoEnMayusculas?: boolean,
  desactivar?: boolean,
}
export interface BuscadorModalInputPreview {
  mostrar: boolean, // Mostrar input con el valor de busqueda seleccionado en el componente padre
  input: BuscadorModalInput, //
  quitarMarginAbajo?: boolean,
}
export interface BuscadorModalResultado {
  mostrarElegibles: boolean, // Indica si mostrar el div que contiene el resultado
  mostrarCargando: boolean, // Indica si mostrar el cargando
  error: MensajeError, // Indica el contenido del error
  items: Array<ItemSelector>, // Lista de los items que se pueden elegir del resultado
  puedeCargarMas?: boolean,
  mostrarCargandoPequeno?: boolean
}
export interface ConfiguracionBuscadorModal {
  mostrarModal: boolean, // Indica si mostrar el modal de buscar
  seleccionado: ItemSelector, // Item seleccionado del resultado de busqueda
  inputPreview: BuscadorModalInputPreview, // Para mostrar la preview del buscador o resultado seleccionado (revisar buscador de codigos postales para referencia de uso)
  inputBuscador: BuscadorModalInput, // Donde el usuario ingresa el texto a buscar (Dentro del modal)
  resultado: BuscadorModalResultado, // Contenedor para mostrar el resultado de la busqueda,
  pais?: ItemSelector,
  evento?: Function,
  tipoIntercambio?: boolean
}
