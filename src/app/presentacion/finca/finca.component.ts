import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { VariablesGlobales } from '@core/servicios/generales';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves';

import { RutasLocales } from 'src/app/rutas-locales.enum';

@Component({
  selector: 'app-finca',
  templateUrl: './finca.component.html',
  styleUrls: ['./finca.component.scss']
})
export class FincaComponent implements OnInit, OnDestroy {

  public paginas: number
  public menu: number

  public sliderUno: DataSlider
  public imgSliderUno: Array<DataImagenes>

  public sliderDos: DataSlider
  public imgSliderDos: Array<DataImagenes>

  public sliderTres: DataSlider
  public imgSliderTres: Array<DataImagenes>

  public sliderCuatro: DataSlider
  public imgSliderCuatro: Array<DataImagenes>

  public intervaloUno: any
  public intervaloDos: any
  public intervaloTres: any
  public intervaloCuatro: any

  public botonBack: number

  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private router: Router,
  ) {
    this.paginas = 1
    this.imgSliderUno = []
    this.imgSliderDos = []
    this.imgSliderTres = []
    this.imgSliderCuatro = []
    this.botonBack = 0
  }

  ngOnInit(): void {
    this.configurarVariablesGlobales()
    this.detectarDispositivo()
    this.configurarBotonBack()
    this.configurarDataSliders()
    this.imgSliderUno = this.inicializarImagenesSlider(this.imgSliderUno, 7)
    this.imgSliderDos = this.inicializarImagenesSlider(this.imgSliderDos, 7)

    // Pagina dos
    this.configurarDataSlidersPaginaDos()
    this.imgSliderTres = this.inicializarImagenesSlider(this.imgSliderTres, 6)
    this.imgSliderCuatro = this.inicializarImagenesSlider(this.imgSliderCuatro, 7)
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.configurarSliders()
      this.configurarSlidersPaginaDos()
    })
  }

  ngOnDestroy(): void {
    this.variablesGlobales.configurarDataAnimacion()

    if (this.intervaloUno) {
      clearInterval(this.intervaloUno)
    }

    if (this.intervaloDos) {
      clearInterval(this.intervaloDos)
    }

    if (this.intervaloTres) {
      clearInterval(this.intervaloTres)
    }

    if (this.intervaloCuatro) {
      clearInterval(this.intervaloCuatro)
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.detectarDispositivo()
  }

  configurarVariablesGlobales() {
    this.variablesGlobales.configurarDataAnimacion(true)
  }


  detectarDispositivo() {
    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      document.location.href = 'https://m.gazelook.com/';
    }
  }

  configurarBotonBack() {
    const pagina = sessionStorage.getItem(LlavesSessionStorage.PAGINAS)
    if (pagina && pagina !== null) {
      this.botonBack = parseInt(pagina)
    }
  }

  notranslate() {
    const elementos = document.getElementsByTagName('p') as HTMLCollectionOf<HTMLElement>

    if (elementos) {

      for (let i = 0; i < elementos.length; i++) {
        const elemento = elementos[i]
        let clases = elemento.getAttribute('class')

        if (clases && clases !== null) {
          elemento.setAttribute('class', clases + ' notranslate')
        } else {
          elemento.setAttribute('class', 'notranslate')
        }
      }
    }
  }

  irALaPaginaPrincipal() {
    this.router.navigateByUrl(RutasLocales.LANDING)
  }

  configurarDataSliders() {
    this.sliderUno = {
      idSlider: 'slider-uno',
      totalItems: 0,
      posiciones: [],
      elementos: null,
      elementoActivo: 1,
      clicsIzquierda: 0,
      elementosParaMostrar: 5
    }

    this.sliderDos = {
      idSlider: 'slider-dos',
      totalItems: 0,
      posiciones: [],
      elementos: null,
      elementoActivo: 1,
      clicsIzquierda: 0,
      elementosParaMostrar: 5
    }
  }

  configurarDataSlidersPaginaDos() {
    this.sliderTres = {
      idSlider: 'slider-tres',
      totalItems: 0,
      posiciones: [],
      elementos: null,
      elementoActivo: 1,
      clicsIzquierda: 0,
      elementosParaMostrar: 3
    }

    this.sliderCuatro = {
      idSlider: 'slider-cuatro',
      totalItems: 0,
      posiciones: [],
      elementos: null,
      elementoActivo: 1,
      clicsIzquierda: 0,
      elementosParaMostrar: 3
    }
  }

  configurarSliders() {
    this.sliderUno = this.incializarSliders(this.sliderUno)

    this.moverSliderUno()

    this.sliderDos = this.incializarSliders(this.sliderDos)
    this.moverSliderDos()
  }

  configurarSlidersPaginaDos() {
    this.sliderTres = this.incializarSlidersVertical(this.sliderTres)
    this.moverSliderTres()

    this.sliderCuatro = this.incializarSlidersVertical(this.sliderCuatro)
    this.moverSliderCuatro()

  }

  eventoclick(pagina: number) {
    
    if (pagina === 1) {
      this.paginas = pagina
    }
    if (pagina === 2) {
      this.paginas = pagina
    }
    if(pagina === 3) {
      // this.router.navigateByUrl('')
      window.open('https://www.youtube.com/embed/EfeJB--2KrA', '_blank');
   
      
    }
    
  }

  inicializarImagenesSlider(
    data: Array<DataImagenes>,
    totalFotos: number
  ): Array<DataImagenes> {
    for (let i = 0; i < totalFotos; i++) {
      data.push({
        url: i + 1
      })
    }

    return data
  }

  incializarSliders(
    data: DataSlider
  ): DataSlider {
    const elemento = document.getElementById(data.idSlider) as HTMLElement
    data.elementos = elemento.getElementsByClassName('mini') as HTMLCollectionOf<HTMLElement>
    let porcentajex = 0
    for (let i = 0; i < data.elementos.length; i++) {
      const elemento: HTMLElement = data.elementos[i]
      elemento.style.transform = 'translateX(' + porcentajex + '%)'
      data.posiciones[i] = porcentajex
      porcentajex = porcentajex + 100
    }

    data.elementoActivo = 1
    data.clicsIzquierda = 0
    data.totalItems = data.elementos.length

    return data
  }

  moverDerecha(
    data: DataSlider
  ): DataSlider {
    if (
      data.elementoActivo >= (data.elementosParaMostrar - 1) &&
      data.elementoActivo < data.elementos.length
    ) {
      for (let i = 0; i < data.elementos.length; i++) {
        const elemento: HTMLElement = data.elementos[i]
        let porcentajex = data.posiciones[i] - 100
        elemento.style.transform = 'translateX(' + porcentajex + '%)'
        data.posiciones[i] = porcentajex
      }
    }

    if (data.elementoActivo < data.elementos.length) {
      data.elementoActivo += 1
    } else {
      data = this.incializarSliders(data)
    }

    return data
  }

  moverSliderUno() {
    this.intervaloUno = setInterval(() => {
      this.sliderUno = this.moverDerecha(this.sliderUno)
    }, 3000)
  }

  moverSliderDos() {
    this.intervaloDos = setInterval(() => {
      this.sliderDos = this.moverDerecha(this.sliderDos)
    }, 3000)
  }

  moverSliderTres() {
    this.intervaloTres = setInterval(() => {
      this.sliderTres = this.moverVertical(this.sliderTres)
    }, 3000)
  }

  moverSliderCuatro() {
    this.intervaloCuatro = setInterval(() => {
      this.sliderCuatro = this.moverVertical(this.sliderCuatro)
    }, 3000)
  }

  incializarSlidersVertical(
    data: DataSlider
  ): DataSlider {
    const elemento = document.getElementById(data.idSlider) as HTMLElement
    data.elementos = elemento.getElementsByClassName('mini') as HTMLCollectionOf<HTMLElement>
    let porcentajex = 0
    for (let i = 0; i < data.elementos.length; i++) {
      const elemento: HTMLElement = data.elementos[i]
      elemento.style.transform = 'translateY(' + porcentajex + '%)'
      data.posiciones[i] = porcentajex
      porcentajex = porcentajex + 100
    }

    data.elementoActivo = 1
    data.clicsIzquierda = 0
    data.totalItems = data.elementos.length

    return data
  }

  moverVertical(
    data: DataSlider
  ): DataSlider {
    if (
      data.elementoActivo >= (data.elementosParaMostrar - 1) &&
      data.elementoActivo < data.elementos.length
    ) {
      for (let i = 0; i < data.elementos.length; i++) {
        const elemento: HTMLElement = data.elementos[i]
        let porcentajex = data.posiciones[i] - 100
        elemento.style.transform = 'translateY(' + porcentajex + '%)'
        data.posiciones[i] = porcentajex
      }
    }

    if (data.elementoActivo < data.elementos.length) {
      data.elementoActivo += 1
    } else {
      data = this.incializarSlidersVertical(data)
    }

    return data
  }

}
export interface DataSlider {
  idSlider: string,
  totalItems: number,
  elementoActivo: number,
  clicsIzquierda: number,
  elementos: HTMLCollectionOf<HTMLElement>,
  posiciones: Array<number>,
  elementosParaMostrar: number
}

export interface DataImagenes {
  url: number,
  id?: string,
}
