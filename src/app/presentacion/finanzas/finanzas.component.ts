import { ConfiguracionToast } from './../../compartido/diseno/modelos/toast.interface';
import { ToastComponent } from './../../compartido/componentes/toast/toast.component';
import { MenuPrincipalComponent } from './../menu-principal/menu-principal.component';
import { VariablesGlobales } from './../../nucleo/servicios/generales/variables-globales.service';
import {
  EspesorLineaItem,
  ColorFondoLinea,
  ColorDelTexto,
  EstilosDelTexto,
} from '../../compartido/diseno/enums/estilos-colores-general';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BotonCompartido } from 'src/app/compartido/diseno/modelos/boton.interface';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
import { Location } from '@angular/common';
import { ConfiguracionAppbarCompartida } from 'src/app/compartido/diseno/modelos/appbar.interface';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { LineaCompartida } from 'src/app/compartido/diseno/modelos/linea.interface';
import { UsoAppBar } from 'src/app/compartido/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { AnchoLineaItem } from 'src/app/compartido/diseno/enums/ancho-linea-item.enum';

import { TamanoDeTextoConInterlineado } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import {
  ColorTextoBoton,
  TipoBoton,
} from 'src/app/compartido/componentes/button/button.component';
import { NotificacionesDeUsuario } from 'src/app/nucleo/servicios/generales/notificaciones/notificaciones-usuario.service';
import {
  Enrutador,
  InformacionEnrutador,
  TipoDeNavegacion,
} from '../enrutador/enrutador.component';
import { EnrutadorService } from 'src/app/nucleo/servicios/generales/enrutador/enrutador.service';
import { ConfiguracionDialogoInline } from '../../compartido/diseno/modelos';
import { PaddingIzqDerDelTexto } from '../../compartido/diseno/enums';

@Component({
  selector: 'app-finanzas',
  templateUrl: './finanzas.component.html',
  styleUrls: ['./finanzas.component.scss'],
})
export class FinanzasComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  configuracionAppBar: ConfiguracionAppbarCompartida;
  perfilSeleccionado: PerfilModel;
  confLinea: LineaCompartida;

  //botones
  botonCompra: BotonCompartido;
  botonIntercambio: BotonCompartido;

  botonBuscarCultural: BotonCompartido;
  botonPublicarCultural: BotonCompartido;

  // lista
  mostrarCuotaAnual: boolean;
  mostrarCuotaUsuario: boolean;
  mostrarReportes: boolean;

  // Dialogos
  public confDialogoSinIngresos: ConfiguracionDialogoInline;
  public confDialogoAlianzas: ConfiguracionDialogoInline;
  public confDialogoProyectoSeleccionado: ConfiguracionDialogoInline;
  public confDialogoAdicionalProSelec: ConfiguracionDialogoInline;
  public confToast: ConfiguracionToast;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private perfilNegocio: PerfilNegocio,
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    private _location: Location,
    private variablesGlobales: VariablesGlobales,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private enrutadorService: EnrutadorService
  ) {
    this.variablesGlobales.mostrarMundo = false;
    this.mostrarCuotaAnual = false;
    this.mostrarCuotaUsuario = false;
    this.mostrarReportes = false;
  }

  ngOnInit(): void {
    this.obtenerPerfil();
    this.configurarToast();
    this.configurarDialogos();
    if (this.perfilSeleccionado) {
      this.prepararAppBar();
      this.configuracionBotones();
      this.notificacionesUsuario.validarEstadoDeLaSesion(false);
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    };
  }

  obtenerPerfil() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  prepararAppBar() {
    this.configuracionAppBar = {
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [],
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            },
          }
        ),
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilSeleccionado?.tipoPerfil.nombre,
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          },
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm7v1texto1',
        },
      },
    };
  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false,
    };
  }

  configuracionBotones() {
    this.botonBuscarCultural = {
      text: 'm6v5texto5',
      tamanoTexto: TamanoDeTextoConInterlineado.L3_I2,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => {
        // this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.INTERCAMBIO)
      },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };

    this.botonPublicarCultural = {
      text: 'm6v5texto6',
      tamanoTexto: TamanoDeTextoConInterlineado.L3_I2,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => {
        // this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.COMPRA)
      },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };
  }

  abrirCuotaAnual() {
    this.reiniciarDialogos();
    if (this.mostrarCuotaAnual) {
      this.mostrarCuotaAnual = !this.mostrarCuotaAnual;
      return;
    }

    this.reiniciarFee();
    this.mostrarCuotaAnual = !this.mostrarCuotaAnual;
  }

  abrirCuotaUsuario() {
    this.reiniciarDialogos();
    if (this.mostrarCuotaUsuario) {
      this.mostrarCuotaUsuario = !this.mostrarCuotaUsuario;
      return;
    }
    this.reiniciarFee();
    this.mostrarCuotaUsuario = !this.mostrarCuotaUsuario;
  }

  abrirReportes() {
    this.toast.abrirToast('text81');
    // this.reiniciarDialogos()
    // if (this.mostrarReportes) {
    // 	this.mostrarReportes = !this.mostrarReportes
    // 	return
    // }
    // this.reiniciarFee()
    // this.mostrarReportes = !this.mostrarReportes;
  }

  abrirAnunciosFinanzas() {
    this.reiniciarFee();
    if (!this.confDialogoSinIngresos.noMostrar) {
      this.confDialogoSinIngresos.noMostrar = true;

      return;
    }
    if (this.confDialogoSinIngresos.noMostrar) {
      this.confDialogoSinIngresos.noMostrar = false;

      this.confDialogoProyectoSeleccionado.noMostrar = true;
      this.confDialogoAdicionalProSelec.noMostrar = true;
      this.confDialogoAlianzas.noMostrar = true;

      return;
    }
  }
  abrirGazeAlianzas() {
    this.reiniciarFee();

    if (!this.confDialogoAlianzas.noMostrar) {
      this.confDialogoAlianzas.noMostrar = true;
      return;
    }
    if (this.confDialogoAlianzas.noMostrar) {
      this.confDialogoSinIngresos.noMostrar = true;
      this.confDialogoProyectoSeleccionado.noMostrar = true;
      this.confDialogoAdicionalProSelec.noMostrar = true;
      this.confDialogoAlianzas.noMostrar = false;
      return;
    }
  }

  abrirFundsProject() {
    this.reiniciarFee();
    if (!this.confDialogoProyectoSeleccionado.noMostrar) {
      this.confDialogoProyectoSeleccionado.noMostrar = true;

      return;
    }
    if (this.confDialogoProyectoSeleccionado.noMostrar) {
      this.confDialogoProyectoSeleccionado.noMostrar = true;

      this.confDialogoProyectoSeleccionado.noMostrar = false;
      this.confDialogoAdicionalProSelec.noMostrar = true;
      this.confDialogoAlianzas.noMostrar = true;

      return;
    }
  }

  abrirAdditionalExpenses() {
    this.reiniciarFee();
    if (!this.confDialogoAdicionalProSelec.noMostrar) {
      this.confDialogoAdicionalProSelec.noMostrar = true;

      return;
    }
    if (this.confDialogoAdicionalProSelec.noMostrar) {
      this.confDialogoAdicionalProSelec.noMostrar = true;

      this.confDialogoProyectoSeleccionado.noMostrar = true;
      this.confDialogoAdicionalProSelec.noMostrar = false;
      this.confDialogoAlianzas.noMostrar = true;

      return;
    }
  }

  configurarDialogos() {
    // Proyectos seleccionados
    this.confDialogoSinIngresos = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoSinIngresos.descripcion.push(
      {
        texto: 'm7v4texto3',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm7v1texto20',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );
    this.confDialogoAlianzas = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoAlianzas.descripcion.push({
      texto: 'm7v1texto18',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });

    this.confDialogoProyectoSeleccionado = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectoSeleccionado.descripcion.push({
      texto: 'm7v1texto19',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });
    this.confDialogoAdicionalProSelec = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoAdicionalProSelec.descripcion.push({
      texto: 'm7v1texto19',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });
  }

  reiniciarDialogos() {
    this.confDialogoSinIngresos.noMostrar = true;
    this.confDialogoAlianzas.noMostrar = true;
  }
  reiniciarFee() {
    this.mostrarCuotaAnual = false;
    this.mostrarCuotaUsuario = false;
    this.mostrarReportes = false;
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}
