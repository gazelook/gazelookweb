import { Component, OnInit, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  ReporteFinanzasUsuarioModel,
  valores,
} from '@env/src/app/dominio/modelo/entidades/reporte-finanzas-usuario.model';
import { CodigosCatalogoTipoMoneda } from '@env/src/app/nucleo/servicios/remotos/codigos-catalogos';
import { ToastComponent } from '@shared/componentes';
import {
  ConfiguracionMonedaPicker,
  ConfiguracionToast,
} from '@shared/diseno/modelos';
import {
  CuentaNegocio,
  FinanzasNegocio,
  InternacionalizacionNegocio,
  PerfilNegocio,
} from 'dominio/logica-negocio';
import { PerfilModel, UsuarioModel } from 'dominio/modelo/entidades';
@Component({
  selector: 'app-cuota-anual',
  templateUrl: './cuota-anual.component.html',
  styleUrls: ['./cuota-anual.component.scss'],
})
export class CuotaAnualComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  // Parametros internos
  public perfilSeleccionado: PerfilModel;
  public idCapaCuerpo: string;
  public fechaInicial: number;
  public anio: number;
  public mes: number;
  public email: string;
  public usuario: UsuarioModel;
  public bandera: boolean;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;
  public puedeCargarMas: boolean;

  public dataInformeFinal: Array<ReporteFinanzasUsuarioModel>;

  public aportacionSuscripcionEuro: number;
  public aportacionSuscripcionDolar: number;
  public aportacionValorExtraEuro: number;
  public aportacionValorExtraDolar: number;
  public aportacionDonacionEuro: number;
  public aportacionDonacionDolar: number;
  public valorFinalEuro: number;
  public valorFinalDolar: number;

  public confMonedaPicker: ConfiguracionMonedaPicker;
  public confToast: ConfiguracionToast;

  public cuotasExtrasEuro: Array<valores>;

  public cuotasExtrasDolar: Array<valores>;

  public cuotasExtrasFechas: Array<valores>;
  public cuotasDonaciones: Array<valores>;
  public aniosSelect: Array<number>;
  constructor(
    private perfilNegocio: PerfilNegocio,
    private finanzasNegocio: FinanzasNegocio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio
  ) {
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.fechaInicial = new Date().getFullYear();
    this.bandera = false;
    this.mes = 0;
    this.anio = 0;
    this.dataInformeFinal = [];
    this.aportacionSuscripcionEuro = 0;
    this.aportacionSuscripcionDolar = 0;
    this.aportacionValorExtraEuro = 0;
    this.aportacionValorExtraDolar = 0;
    this.valorFinalEuro = 0;
    this.valorFinalDolar = 0;
    this.cuotasExtrasFechas = [];
    this.cuotasDonaciones = [];
    this.aniosSelect = [];
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado();
    this.configurarToast();
    this.obtenerDatosInformeFinanzas(this.fechaInicial.toString());
    this.selectAnios();

    // Inicializar funcion de redondeo
    if (!Math['round10']) {
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp);
      };
    }
  }
  selectAnios() {
    let currentYear = new Date().getFullYear();
    for (let index = 2020; index <= currentYear; index++) {
      this.aniosSelect.push(index);
    }
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    };
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
  }

  async obtenerAportaciones() {
    this.toast.abrirToast(
      await this.internacionalizacionNegocio.obtenerTextoLlave('text11'),
      true
    );
  }

  ajustarDecimales(type: any, value: any, exp: any) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? +value[1] + exp : exp));
  }

  cambioDeFecha() {
    this.obtenerDatosInformeFinanzas(this.fechaInicial.toString());
  }

  async obtenerDatosInformeFinanzas(anio?: string) {
    let idUsuario = this.usuario.id;
    const data = this.finanzasNegocio
      .obtenerDatosInformeActualFinanzas(idUsuario, anio)
      .subscribe((res) => {
        console.log('res', res);

        if (res.length === 0) {
          this.aportacionSuscripcionDolar = 0;
          this.aportacionValorExtraDolar = 0;
          this.valorFinalDolar =
            this.aportacionSuscripcionDolar + this.aportacionValorExtraDolar;
          this.valorFinalDolar = parseFloat(this.valorFinalDolar.toFixed(2));
          this.aportacionSuscripcionEuro = 0;
          this.aportacionValorExtraEuro = 0;
          this.cuotasDonaciones = [];
          this.cuotasExtrasFechas = [];

          this.valorFinalEuro =
            this.aportacionSuscripcionEuro + this.aportacionValorExtraEuro;
          this.aportacionSuscripcionEuro = parseFloat(
            this.aportacionSuscripcionEuro.toFixed(2)
          );
        }
        this.dataInformeFinal = res;
        for (let elemento of this.dataInformeFinal) {
          if (elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.USD) {
            this.aportacionSuscripcionDolar = elemento.aportacionesSuscripcion;
            for (const iterator of elemento.aportacionesValorExtra) {
              this.aportacionValorExtraDolar =
                this.aportacionValorExtraDolar + iterator.monto;
            }
            for (const iterator of elemento.aportacionesDonacionProyectos) {
              this.aportacionValorExtraDolar =
                this.aportacionValorExtraDolar + iterator.monto;
            }

            let valorFinalDolar =
              this.aportacionSuscripcionDolar + this.aportacionValorExtraDolar;
            this.valorFinalDolar = parseFloat(valorFinalDolar.toFixed(2));

            if (elemento.aportacionesValorExtra.length > 0) {
              this.cuotasExtrasFechas = elemento.aportacionesValorExtra;
              this.cuotasExtrasDolar = elemento.aportacionesValorExtra;

              for (
                let index = 0;
                index < this.cuotasExtrasDolar.length;
                index++
              ) {
                this.cuotasExtrasFechas[index].fechaCreacion =
                  this.cuotasExtrasDolar[index].fechaCreacion;

                this.cuotasExtrasFechas[index].montoUSD =
                  this.cuotasExtrasDolar[index].monto;
              }
            }

            if (elemento.aportacionesDonacionProyectos.length > 0) {
              this.cuotasDonaciones = elemento.aportacionesDonacionProyectos;
              let aportacionDona = elemento.aportacionesDonacionProyectos;

              for (let index = 0; index < aportacionDona.length; index++) {
                this.cuotasDonaciones[index].fechaCreacion =
                  this.cuotasDonaciones[index].fechaCreacion;

                this.cuotasDonaciones[index].montoUSD =
                  aportacionDona[index].monto;
              }
            }
          }

          if (elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.EUR) {
            this.aportacionSuscripcionEuro = elemento.aportacionesSuscripcion;

            for (const iterator of elemento.aportacionesValorExtra) {
              this.aportacionValorExtraEuro =
                this.aportacionValorExtraEuro + iterator.monto;
            }
            for (const iterator of elemento.aportacionesDonacionProyectos) {
              this.aportacionValorExtraEuro =
                this.aportacionValorExtraEuro + iterator.monto;
            }

            let valorFinalEuro =
              this.aportacionSuscripcionEuro + this.aportacionValorExtraEuro;
            this.valorFinalEuro = parseFloat(valorFinalEuro.toFixed(2));

            if (elemento.aportacionesValorExtra) {
              this.cuotasExtrasEuro = elemento.aportacionesValorExtra;

              for (
                let index = 0;
                index < this.cuotasExtrasEuro.length;
                index++
              ) {
                this.cuotasExtrasFechas[index].montoEUR =
                  this.cuotasExtrasEuro[index].monto;
              }
            }

            if (elemento.aportacionesDonacionProyectos.length > 0) {
              let aportacionDona = elemento.aportacionesDonacionProyectos;

              for (let index = 0; index < aportacionDona.length; index++) {
                this.cuotasDonaciones[index].montoEUR =
                  aportacionDona[index].monto;
              }
            }
          }
        }

        this.cuotasExtrasFechas = this.cuotasExtrasFechas.concat(
          this.cuotasDonaciones
        );

        for (const iterator of this.cuotasExtrasFechas) {
          delete iterator['monto'];
        }

        this.cuotasExtrasFechas.sort(function (x, y) {
          if (x.fechaCreacion > y.fechaCreacion) {
            return 1;
          }
          if (x.fechaCreacion < y.fechaCreacion) {
            return -1;
          }
          return 0;
        });
      });
  }
}
