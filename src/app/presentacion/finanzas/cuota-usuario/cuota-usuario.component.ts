import { Component, OnInit, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { MonedaPickerService } from '@core/servicios/generales';
import { TipoAportaciones } from '@core/servicios/remotos/rutas';
import { ReporteFinanzasUsuarioModel } from '@env/src/app/dominio/modelo/entidades/reporte-finanzas-usuario.model';
import { FinanzasServiceRemoto } from '@env/src/app/nucleo/servicios/remotos';
import { CodigosCatalogoTipoMoneda } from '@env/src/app/nucleo/servicios/remotos/codigos-catalogos';
import {
  BuscadorModalComponent, SelectorComponent, ToastComponent
} from '@shared/componentes';
import { AccionesSelector } from '@shared/diseno/enums';
import {
  ConfiguracionSelector,
  ConfiguracionToast, InfoAccionSelector, ItemSelector, ResumenDataMonedaPicker, ValorBase
} from '@shared/diseno/modelos';
import { FinanzasNegocio, InternacionalizacionNegocio, PerfilNegocio, TipoMonedaNegocio, UbicacionNegocio } from 'dominio/logica-negocio';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos';
import {
  PerfilModel
} from 'dominio/modelo/entidades';
@Component({
  selector: 'app-cuota-usuario',
  templateUrl: './cuota-usuario.component.html',
  styleUrls: ['./cuota-usuario.component.scss']
})
export class CuotaUsuarioComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent
  @ViewChild('selectorPaises', { static: false }) selectorPaises: SelectorComponent;
  @ViewChild('buscadorLocalidades', { static: false }) buscadorLocalidades: BuscadorModalComponent;

  // Parametros internos
  public fechaInicial: Date;
  public fechaFinal: Date;
  public filtroActivo: string;
  public filtroPorTitulo: boolean;
  public metodoBusqueda: number;
  public perfilSeleccionado: PerfilModel;
  public idCapaCuerpo: string;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;
  public puedeCargarMas: boolean;
  public email: string;
  public miPerfil: PerfilModel;
  public valorBaseAPagar: ValorBase

  public confToast: ConfiguracionToast

  public confSelectorPais: ConfiguracionSelector // Configuracion del selector solo pais
  public confSelectorPaisLocalidad: ConfiguracionSelector // Configuracion del selector pais y localidad

  //Aportaciones
  public aportaciones: any;
  public aportacionSuscripcionEuro: number;
  public aportacionSuscripcionDolar: number;
  public aportacionValorExtraEuro: number;
  public aportacionValorExtraDolar: number;

  public dataTarifaTotalFecha: Array<ReporteFinanzasUsuarioModel>;
  public dataTarifaTotalFechaCodigoPais: Array<ReporteFinanzasUsuarioModel>;

  public data: InfoAccionSelector

  public paisSeleccionado: Object

  public pais: string

  public codigoPais: string

  public cargando: boolean

  public fechaModificada: string

  public estadoPaisSeleccionado: boolean

  public estadoFechaModificada: boolean

  constructor(private perfilNegocio: PerfilNegocio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private monedaPickerService: MonedaPickerService,
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    private ubicacionNegocio: UbicacionNegocio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private finanzasServiceRemoto: FinanzasServiceRemoto,
    private finanzasNegocio: FinanzasNegocio,
  ) {
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.fechaInicial = new Date();
    this.fechaFinal = new Date();
    this.aportacionSuscripcionEuro = 0;
    this.aportacionSuscripcionDolar =0;
    this.aportacionValorExtraEuro = 0;
    this.aportacionValorExtraDolar = 0;
    this.fechaModificada = ''
    this.estadoPaisSeleccionado = false
    this.dataTarifaTotalFecha = []
    this.dataTarifaTotalFechaCodigoPais = []
    this.estadoFechaModificada = false
  }

  ngOnInit(): void {
    this.configurarSelectorPais()
    this.configurarToast()
    this.obtenerTarifasTotalesUsuario();

    // Inicializar funcion de redondeo
    if (!Math['round10']) {
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp)
      };
    }
  }

  ajustarDecimales(type: any, value: any, exp: any) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false, //True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    }
  }

  obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    const valorEstimado = (this.valorBaseAPagar && this.valorBaseAPagar.valorNeto) ? parseFloat(this.valorBaseAPagar.valorNeto) : 0
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    }

    const data = {
      valorEstimado: this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(valorEstimado),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda)
    }
    return data
  }

  validarFechasIngresadas() {
    if (new Date(this.fechaInicial).getTime() > new Date(this.fechaFinal).getTime()) {
      return false;
    }

    return true;
  }

  async cambioDeFecha() {

    let date = new Date(this.fechaInicial);
    let fecha = '';
    let anio = date.getFullYear().toString();
    let mes_temporal = date.getMonth() + 1;
    let dia_temporal = date.getDate() + 1
    var mes = String(mes_temporal.toString()).padStart(2, '0');
    let dia = String(dia_temporal.toString()).padStart(2, '0');
    fecha = anio + '-' + mes + '-' + dia;
    this.fechaModificada = fecha
    this.estadoFechaModificada = true

    if(this.estadoPaisSeleccionado) {
      this.obtenerTarifasFechaPaisCodigo()
    } else {
      this.obtenerTarifaFecha(this.fechaModificada)
    }
  }

  async obtenerAportaciones(filtro?: string, codigo?: string) {

  }

  // Configurar selectores
  async configurarSelectorPais() {
    // Definir direccion
    let item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
    this.confSelectorPais = {
      tituloSelector: 'Choose the country',
      mostrarModal: false,
      inputPreview: {
        mostrar: true,
        input: {
          valor: item.nombre,
          placeholder: await this.internacionalizacionNegocio.obtenerTextoLlave('m7v3texto9'),
          desactivar: false,
        },
      },
      seleccionado: item,
      elegibles: [],
      cargando: {
        mostrar: false
      },
      quitarMarginAbajo: true,
      error: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false
      },
      tipoFinanzas: true,
      evento: (data: InfoAccionSelector) => this.eventoEnSelector(data)
    }
  }

  eventoEnSelector(data: InfoAccionSelector) {
    switch (data.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorPaises()
        break
      case AccionesSelector.SELECCIONAR_ITEM:

        // Selector
        this.confSelectorPais.seleccionado = data.informacion
        this.confSelectorPais.mostrarModal = false
        this.confSelectorPais.inputPreview.input.valor = data.informacion.nombre

        
        this.paisSeleccionado = this.confSelectorPais.seleccionado

        this.estadoPaisSeleccionado = true

        let valor = Object.values(this.paisSeleccionado)

        this.codigoPais = valor[0]

        this.pais = valor[1]

        this.cambioDeFecha();
       
        this.obtenerTarifasTotalesUsuario(this.fechaModificada, this.pais, this.codigoPais )

        //this.obtenerAportaciones(TipoAportaciones.PAIS, data.informacion.codigo);
        break
      case AccionesSelector.REINTERTAR_CONTENIDO: break
      case AccionesSelector.BUSCAR_PAIS_POR_QUERY: break
      default: break;
    }
  }

  // Click en input pais
  async abrirSelectorPaises() {
    try {
      this.confSelectorPais.cargando.mostrar = true
      this.confSelectorPais.mostrarModal = true
      const items: ItemSelector[] = await this.ubicacionNegocio.obtenerCatalogoPaisesParaSelector().toPromise()
      if (!items) {
        throw new Error('')
      }

      this.ubicacionNegocio.guardarPaisesDelSelectorEnLocalStorage(items)
      this.confSelectorPais.elegibles = items
      this.confSelectorPais.cargando.mostrar = false

      if (this.confSelectorPais.elegibles.length === 0) {
        this.confSelectorPais.cargando.mostrar = false
        this.confSelectorPais.error.contenido = 'text31'
        this.confSelectorPais.error.mostrarError = true
      }
    } catch (error) {
      this.confSelectorPais.elegibles = []
      this.confSelectorPais.cargando.mostrar = false
      this.confSelectorPais.error.contenido = 'text31'
      this.confSelectorPais.error.mostrarError = true
    }
  }

  async obtenerTarifasTotalesUsuario(fecha?:string, pais?:string, codigoPais?:string) {


    let fechaActual = this.obtenerFechaActual()
    
    if(fecha&&codigoPais) {
      this.obtenerTarifasFechaPaisCodigo()
      
    }else {
        this.obtenerTarifaFecha(fechaActual)
    }
  }

  // OBTENER TARIFAS TOTALES SOLO ENVIANDO FECHA
  obtenerTarifasTotalesFecha(fecha?: string) {
    const data = this.finanzasNegocio
    .obtenerTarifasTotalesUsuario(fecha)
    .subscribe((res) => {
      this.dataTarifaTotalFecha = res;
      
      if( res.length === 0) {
        this.aportacionSuscripcionDolar = 0
        this.aportacionValorExtraDolar = 0
        this.aportacionSuscripcionEuro = 0
        this.aportacionValorExtraEuro = 0
      } 

      for( let elemento of this.dataTarifaTotalFecha) {

        if(elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.EUR) {
          this.aportacionSuscripcionEuro = 0
          this.aportacionValorExtraEuro = elemento.aportaciones
        }

        if(elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.USD) {
          this.aportacionValorExtraDolar = 0
          this.aportacionValorExtraDolar = elemento.aportaciones
        }
      }
    });
  }

  // OBTENER TARIFAS TOTALES ENVIANDO LA FECHA + CODIGO PAIS

  obtenerTarifasTotalesFechaCodigoPais(fecha?: string, codigoPais?: string) {
    
    const data = this.finanzasNegocio
    .obtenerTarifasTotalesUsuario(fecha, codigoPais)
  
    .subscribe((response) => {
      this.dataTarifaTotalFechaCodigoPais = response;

      if( response.length === 0) {
        this.aportacionSuscripcionDolar = 0
        this.aportacionValorExtraDolar = 0
        this.aportacionSuscripcionEuro = 0
        this.aportacionValorExtraEuro = 0
      } 

      for( let elemento of this.dataTarifaTotalFechaCodigoPais) {

        if(elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.EUR) {
          this.aportacionSuscripcionEuro = elemento.aportaciones
        }

        if(elemento.moneda.codNombre === CodigosCatalogoTipoMoneda.USD) {
          this.aportacionSuscripcionDolar = elemento.aportaciones
        }
      }
    });
  }

  obtenerTarifaFecha(fecha: string) {
    this.obtenerTarifasTotalesFecha(fecha)
  }

  obtenerTarifasFechaPaisCodigo() {
    let fecha = ''
    fecha = this.obtenerFechaActual()
    if(this.estadoFechaModificada) {
      this.obtenerTarifasTotalesFechaCodigoPais(this.fechaModificada, this.codigoPais)
      this.obtenerTarifaFecha(this.fechaModificada)
    } else {
      this.obtenerTarifasTotalesFechaCodigoPais(fecha, this.codigoPais)
      this.obtenerTarifaFecha(fecha)
    }
  }

  //Obtener fecha actual

    obtenerFechaActual () {

    // let fecha = ''
    // let date = new Date(this.fechaInicial);
    // let anio = date.getFullYear().toString();
    // let mes_temporal = date.getMonth() + 1;
    // var mes = String(mes_temporal.toString()).padStart(2, '0');
    // let dia = String(date.getDate().toString()).padStart(2, '0');

    // fecha = anio + '-' + mes + '-' + dia;

    return this.fechaInicial.toString()
  }





}
