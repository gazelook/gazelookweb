import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanzasRoutingModule } from './finanzas-routing.module';
import { FinanzasComponent } from './finanzas.component';
import { CompartidoModule } from 'src/app/compartido/compartido.module';
import { TranslateModule } from '@ngx-translate/core';
import { CuotaAnualComponent } from './cuota-anual/cuota-anual.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CuotaUsuarioComponent } from './cuota-usuario/cuota-usuario.component';
import { TrustAccountComponent } from './trust-account/trust-account.component';
import { NgxCurrencyModule } from 'ngx-currency';


@NgModule({
  declarations: [FinanzasComponent, CuotaAnualComponent, CuotaUsuarioComponent, TrustAccountComponent],
  imports: [
    NgxCurrencyModule,
    CommonModule,
    FinanzasRoutingModule,
    CompartidoModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    TranslateModule,
    NgxCurrencyModule
  ]
})
export class FinanzasModule { }
