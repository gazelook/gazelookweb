import { AfterViewInit, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService, VariablesGlobales } from '@core/servicios/generales';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves';
import { RutasLocales } from 'src/app/rutas-locales.enum';
@Component({
  selector: 'app-anuncio',
  templateUrl: './anuncio.component.html',
  styleUrls: ['./anuncio.component.scss']
})
export class AnuncioComponent implements OnInit, OnDestroy, AfterViewInit {

	public mostrarCapas: Array<boolean>
	public interval: any
	public pos: number
	public paginas: number

	constructor(
		public variablesGlobales: VariablesGlobales,
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private router: Router,
		private enrutadorService: EnrutadorService
	) {
		this.mostrarCapas = []
		this.pos = 0
		this.paginas = 1
	}

	ngOnInit(): void {
		this.variablesGlobales.configurarDataAnimacion(true)
		this.detectarDispositivo()
		this.validarPaginaDeDondeProviene()
		this.inicializarCapas()
	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.mostrarCapasMetodo()
		})
	}

	ngOnDestroy(): void {
		this.variablesGlobales.configurarDataAnimacion()
	}

	detectarDispositivo() {
		if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			// this.router.navigateByUrl(Rutas.ANUNCIO_MOVIL)
			document.location.href = 'https://m.gazelook.com/';
		}
	}

	@HostListener('window:resize', ['$event'])
	onResize(event: any) {
		this.detectarDispositivo()
	}

	validarPaginaDeDondeProviene() {
		const a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS)

		if (a && a !== null) {
			this.paginas = parseInt(a)
		} else {
			sessionStorage.setItem(LlavesSessionStorage.PAGINAS, this.paginas.toString())
		}
	}

	inicializarCapas() {
		for(let i = 0; i < 9; i++) {
			this.mostrarCapas[i] = (i === 0)
		}
	}

	mostrarCapasMetodo() {
		this.pos += 1	
		setTimeout(() => {
			this.mostrarCapas[this.pos] = true
			this.pos += 1	
			setTimeout(() => {
				this.mostrarCapas[this.pos] = true
				this.pos += 1	
				setTimeout(() => {
					this.mostrarCapas[this.pos] = true
					this.pos += 1	
					setTimeout(() => {
						this.mostrarCapas[this.pos] = true
						this.pos += 1	
						setTimeout(() => {
							this.mostrarCapas[this.pos] = true
							this.pos += 1	
							setTimeout(() => {
								this.mostrarCapas[this.pos] = true
								this.pos += 1	
								setTimeout(() => {
									this.mostrarCapas[this.pos] = true
									this.pos += 1	
									setTimeout(() => {
										this.mostrarCapas[this.pos] = true
										this.pos += 1	
									}, 1700)
								}, 1600)
							}, 1500)
						}, 1400)
					}, 1300)
				}, 1200)
			}, 1000)
		}, 800)
	}

	retornar() {
		this.router.navigateByUrl(RutasLocales.LANDING)
	}

}
