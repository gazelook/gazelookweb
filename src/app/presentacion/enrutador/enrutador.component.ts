import { Location } from '@angular/common';
import { AfterViewInit, Component, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ImagenPantallaCompletaService, VariablesGlobales } from '@core/servicios/generales';
import { ToastComponent } from '@shared/componentes';
import { ConfiguracionToast } from '@shared/diseno/modelos';
import { PerfilNegocio } from 'dominio/logica-negocio';
import { PerfilModel } from 'dominio/modelo/entidades';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { ConfiguracionEnrutadorNavegacion } from 'src/app/compartido/componentes/enrutador-navegacion/enrutador-navegacion.component';
import { ConfiguracionPortadaGif, PortadaGifComponent } from 'src/app/compartido/componentes/portada-gif/portada-gif.component';
import { CatalogoTipoPerfilModel } from 'src/app/dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { AlbumModel } from 'src/app/dominio/modelo/entidades/album.model';
import { NoticiaModel } from 'src/app/dominio/modelo/entidades/noticia.model';
import { ProyectoModel } from 'src/app/dominio/modelo/entidades/proyecto.model';
import { CamaraService } from 'src/app/nucleo/servicios/generales/camara.service';
import { EnrutadorService } from 'src/app/nucleo/servicios/generales/enrutador/enrutador.service';
import { MetodosSessionStorageService } from 'src/app/nucleo/util/metodos-session-storage.service';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { AlbumAudiosComponent } from '../album/album-audios/album-audios.component';
import { MenuPrincipalComponent } from '../menu-principal/menu-principal.component';
import { AlbumGeneralComponent } from './../album/album-general/album-general.component';
import { AlbumLinksComponent } from './../album/album-links/album-links.component';
import { AlbumPerfilComponent } from './../album/album-perfil/album-perfil.component';

@Component({
    selector: 'app-enrutador',
    templateUrl: './enrutador.component.html',
    styleUrls: ['./enrutador.component.scss']
})
export class EnrutadorComponent implements OnInit, AfterViewInit, OnDestroy {

    // tslint:disable-next-line: variable-name
    @ViewChild('tercio_izquierda', { read: ViewContainerRef }) tercio_izquierda: ViewContainerRef;
    // tslint:disable-next-line: variable-name
    @ViewChild('tercio_central', { read: ViewContainerRef }) tercio_central: ViewContainerRef;
    // tslint:disable-next-line: variable-name
    @ViewChild('tercio_derecha', { read: ViewContainerRef }) tercio_derecha: ViewContainerRef;
    @ViewChild('toastIzq', { static: false }) toastIzq: ToastComponent;
    @ViewChild('toastCen', { static: false }) toastCen: ToastComponent;
    @ViewChild('toastDer', { static: false }) toastDer: ToastComponent;

    public perfilSeleccionado: PerfilModel;

    public componentes: ComponentesCreados;
    public distribucion: Distribucion;
    public confEnrutadorNavegacion: ConfiguracionEnrutadorNavegacion;
    public confToastIzq: ConfiguracionToast;
    public confToastCen: ConfiguracionToast;
    public confToastDer: ConfiguracionToast;

    constructor(
        public variablesGlobales: VariablesGlobales,
        public camaraService: CamaraService,
        private perfilNegocio: PerfilNegocio,
        private router: Router,
        // tslint:disable-next-line: variable-name
        private _location: Location,
        private componentFactoryResolver: ComponentFactoryResolver,
        private enrutadorService: EnrutadorService,
        private metodosSessionStorageService: MetodosSessionStorageService,

        public imagenPantallaCompletaService: ImagenPantallaCompletaService
    ) {

    }

    ngOnInit(): void {
        this.detectarDispositivo();
        this.metodosSessionStorageService.eliminarSessionStorage();
        this.camaraService.reiniciarServicio();
        this.configurarToast();
        this.configurarPerfilSeleccionado();
        this.configurarComponentes();
        this.configurarDistribucionInicial();
        this.configurarEnrutadorNavegacion();
        this.configurarEscuchaCambioDeEstadoEnrutadorNavegacion();
        this.configurarEscuchaClickEnManosDelEnrutador();
        this.configurarEscuchaBotonBack();
        this.configurarEscuchaBotonRefresh();
        this.imagenPantallaCompletaService.configurarPortadaExp();

        if (this.perfilSeleccionado) {

        } else {
            this.navegarSeleccionPerfiles();
        }

    }

    configurarToast(): void {
        this.confToastIzq = {
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: false,
        };
        this.confToastCen = {
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: false,
        };
        this.confToastDer = {
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: false,
        };
    }


    detectarDispositivo(): void {
        if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            // this.router.navigateByUrl(Rutas.ANUNCIO_MOVIL)
            document.location.href = 'https://m.gazelook.com/';
        }
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.configurarComponenteCentralInicial();
            this.configurarComponenteIzquierdaInicial();
            this.configurarComponenteDerechaInicial();
        });
    }

    ngOnDestroy(): void {
        this.enrutadorService.desconectarDeEscuchas();
    }

    configurarPerfilSeleccionado(): void {
        this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
    }

    configurarComponentes(): void {
        this.componentes = {
            izquierda: undefined,
            central: undefined,
            derecha: undefined

        };
    }

    configurarDistribucionInicial(): void {
        this.distribucion = {
            izquierda: [],
            central: [],
            derecha: []
        };
    }

    configurarEnrutadorNavegacion(): void {
        this.confEnrutadorNavegacion = {
            mostrar: false,
            posDerecha: false,
            posCentral: true,
            posIzquierda: false
        };
    }

    navegarSeleccionPerfiles(): void {
        this._location.replaceState('');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
    }

    // Componentes
    configurarComponenteCentralInicial(): void {
        this.enrutadorService.configurarInformacionAEnrutar(MenuPrincipalComponent);
        const ubicacion: UbicacionDelComponente = UbicacionDelComponente.CENTRAL;
        this.reiniciarTercio(ubicacion);
        this.realizarDistribucion(ubicacion);
    }

    obtenerConfiguracionPortadaInicial(
        esIzquierdo: boolean = true
    ): ConfiguracionPortadaGif {
        return {
            esIzquierdo,
            urlImagen: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-a.jpg',
            urlGif: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-en-a.gif',
            llaveTexto: 'fwtexto6',
            textoEspecial: true
        };
    }

    configurarComponenteIzquierdaInicial(): void {
        const ubicacion: UbicacionDelComponente = UbicacionDelComponente.IZQUIERDA;
        this.enrutadorService.configurarInformacionAEnrutar(
            PortadaGifComponent,
            [
                {
                    nombre: 'configuracion',
                    valor: this.obtenerConfiguracionPortadaInicial()
                }
            ]
        );

        this.reiniciarTercio(ubicacion);
        this.realizarDistribucion(ubicacion);
    }

    configurarComponenteDerechaInicial(): void {
        // Izquierdo
        const ubicacion: UbicacionDelComponente = UbicacionDelComponente.DERECHA;
        this.enrutadorService.configurarInformacionAEnrutar(
            PortadaGifComponent,
            [
                {
                    nombre: 'configuracion',
                    valor: this.obtenerConfiguracionPortadaInicial(false)
                }
            ]
        );

        this.reiniciarTercio(ubicacion);
        this.realizarDistribucion(ubicacion);
    }

    mostrarErrorAlbumEnrutador(info: EstadoEnrutador){
        if (info.posicicion === UbicacionDelComponente.IZQUIERDA) {
            this.toastIzq.abrirToast('text100');
        }
        if (info.posicicion === UbicacionDelComponente.CENTRAL) {
            this.toastCen.abrirToast('text100');
        }
        if (info.posicicion === UbicacionDelComponente.DERECHA) {
            this.toastDer.abrirToast('text100');
        }
    }

    configurarEscuchaCambioDeEstadoEnrutadorNavegacion(): void {
        this.enrutadorService.susCripcionEstadoEnrutacion = this.enrutadorService.cambiarEstadoEnrutadorNavegacion$.subscribe(
            info => {
                if (!this.enrutadorService.informacionAEnrutar) {
                    return;
                }

                if (
                    !info.extras ||
                    (info.extras && info.extras.tipo === TipoDeNavegacion.NORMAL)
                ) {
                    let esAlbum = this.validarSiEsAlbum()

                    if (!esAlbum) {
                        this.confEnrutadorNavegacion.mostrar = info.estado;
                        return;
                    }
                   this.mostrarErrorAlbumEnrutador(info)
                    return

                }

                if (!info.extras) {
                    return;
                }

                if (info.extras.tipo === TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTE) {

                    let esAlbum = this.validarSiEsAlbum()
                    if (!esAlbum) {
                        this.navegacionValidarTipoComponente(info);
                        return;
                    }
                    this.mostrarErrorAlbumEnrutador(info)
                    return
                }

                if (info.extras.tipo === TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS) {
                    let esAlbum = this.validarSiEsAlbum()
                    if (!esAlbum) {
                        this.navegacionValidarTipoComponenteConParametros(info);
                        return
                    }
                    this.mostrarErrorAlbumEnrutador(info)
                    return

                }
            }
        );
    }

    navegacionNormal(info: EstadoEnrutador): void {
        this.confEnrutadorNavegacion.mostrar = info.estado;
    }

    navegacionValidarTipoComponente(info: EstadoEnrutador): void {
        this.validarSiEsAlbum()
        const existeInstanciaActiva = this.validarInstanciaActivaDelComponente();

        if (existeInstanciaActiva) {

            if (info.posicicion === UbicacionDelComponente.IZQUIERDA) {
                this.toastIzq.abrirToast('text78');
            }
            if (info.posicicion === UbicacionDelComponente.CENTRAL) {
                this.toastCen.abrirToast('text78');
            }
            if (info.posicicion === UbicacionDelComponente.DERECHA) {
                this.toastDer.abrirToast('text78');
            }

            return;
        }

        this.confEnrutadorNavegacion.mostrar = info.estado;
    }

    navegacionValidarTipoComponenteConParametros(info: EstadoEnrutador): void {
        try {
            if (!info.extras.paramAValidar) {
                throw new Error('No hay param a validar');
            }
            this.validarSiEsAlbum()
            const existeInstanciaActiva = this.validarInstanciaActivaDelComponente(
                true,
                info.extras.paramAValidar
            );

            if (existeInstanciaActiva) {
                if (info.posicicion === UbicacionDelComponente.IZQUIERDA) {
                    this.toastIzq.abrirToast('text78');
                }
                if (info.posicicion === UbicacionDelComponente.CENTRAL) {
                    this.toastCen.abrirToast('text78');
                }
                if (info.posicicion === UbicacionDelComponente.DERECHA) {
                    this.toastDer.abrirToast('text78');
                }

                return;
            }

            this.confEnrutadorNavegacion.mostrar = info.estado;
        } catch (error) {
            return;
        }
    }


    configurarEscuchaClickEnManosDelEnrutador(): void {
        this.enrutadorService.susCripcionClicEnMano = this.enrutadorService.clicEnManoDelEnrutador$.subscribe(info => {
            this.validarSiEsAlbum()
            this.reiniciarTercio(info);
            this.realizarDistribucion(info);
        });
    }

    reiniciarTercio(
        ubicacion: UbicacionDelComponente
    ): void {
        switch (ubicacion) {
            case UbicacionDelComponente.IZQUIERDA:
                if (this.componentes.izquierda) {
                    this.componentes.izquierda.destroy();
                    this.tercio_izquierda.clear();
                }
                break;
            case UbicacionDelComponente.CENTRAL:
                if (this.componentes.central) {
                    this.componentes.central.destroy();
                    this.tercio_central.clear();
                }
                break;
            case UbicacionDelComponente.DERECHA:
                if (this.componentes.derecha) {
                    this.componentes.derecha.destroy();
                    this.tercio_derecha.clear();
                }
                break;
        }
    }

    realizarDistribucion(
        ubicacion: UbicacionDelComponente
    ): void {
        try {

            // this.validarSiEsAlbum()
            const info = this.enrutadorService.informacionAEnrutar;

            const componente = this.componentFactoryResolver.resolveComponentFactory(info.componente);

            const infoAEnrutar: InformacionEnrutador = {
                posicion: ubicacion,
                params: info.params
            };

            const itemDistribucion: ItemDistribucion = {
                componente: info.componente,
                posicion: ubicacion,
                params: info.params
            };


            if (ubicacion === UbicacionDelComponente.IZQUIERDA) {
                this.componentes.izquierda = this.tercio_izquierda.createComponent(componente);
                this.componentes.izquierda.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.distribucion.izquierda.push(itemDistribucion);
                this.confEnrutadorNavegacion.mostrar = false;
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }

            if (ubicacion === UbicacionDelComponente.CENTRAL) {
                this.componentes.central = this.tercio_central.createComponent(componente);
                this.componentes.central.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.distribucion.central.push(itemDistribucion);
                this.confEnrutadorNavegacion.mostrar = false;
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }

            if (ubicacion === UbicacionDelComponente.DERECHA) {
                this.componentes.derecha = this.tercio_derecha.createComponent(componente);
                this.componentes.derecha.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.distribucion.derecha.push(itemDistribucion);
                this.confEnrutadorNavegacion.mostrar = false;
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }
        } catch (error) {
        }
    }

    definirInformacionAEnrutar(
        info: UbicacionDelComponente
    ): ItemDistribucion {
        if (info === UbicacionDelComponente.IZQUIERDA) {
            this.distribucion.izquierda.pop();
            return (this.distribucion.izquierda.length > 0) ?
                this.distribucion.izquierda[this.distribucion.izquierda.length - 1] : undefined;
        }

        if (info === UbicacionDelComponente.CENTRAL) {
            this.distribucion.central.pop();
            return (this.distribucion.central.length > 0) ?
                this.distribucion.central[this.distribucion.central.length - 1] : undefined;
        }

        if (info === UbicacionDelComponente.DERECHA) {
            this.distribucion.derecha.pop();
            return (this.distribucion.derecha.length > 0) ?
                this.distribucion.derecha[this.distribucion.derecha.length - 1] : undefined;
        }
    }

    definirInformacionRefrescar(
        info: UbicacionDelComponente
    ): ItemDistribucion {


        if (info === UbicacionDelComponente.IZQUIERDA) {
            // this.distribucion.izquierda.pop()
            return (this.distribucion.izquierda.length > 0) ?
                this.distribucion.izquierda[this.distribucion.izquierda.length - 1] : undefined;
        }

        if (info === UbicacionDelComponente.CENTRAL) {
            // this.distribucion.central.pop()
            return (this.distribucion.central.length > 0) ?
                this.distribucion.central[this.distribucion.central.length - 1] : undefined;
        }

        if (info === UbicacionDelComponente.DERECHA) {
            // this.distribucion.derecha.pop()
            return (this.distribucion.derecha.length > 0) ?
                this.distribucion.derecha[this.distribucion.derecha.length - 1] : undefined;
        }
    }

    configurarEscuchaBotonRefresh(): void {

        this.enrutadorService.susCripcionEnBotonRefresh = this.enrutadorService.clicEnBotonRefresh$.subscribe(info => {

            this.reiniciarTercio(info);
            const itemAnterior = this.definirInformacionRefrescar(info);
            if (!itemAnterior) {
                return;
            }

            this.enrutadorService.informacionAEnrutar = {
                componente: itemAnterior.componente,
                params: itemAnterior.params
            };

            this.abrirComponenteAnterior(info);
        });
    }


    configurarEscuchaBotonBack(): void {
        this.enrutadorService.susCripcionEnBotonBack = this.enrutadorService.clicEnBotonBack$.subscribe(info => {
            this.reiniciarTercio(info);
            const itemAnterior = this.definirInformacionAEnrutar(info);

            if (!itemAnterior) {
                return;
            }

            this.enrutadorService.informacionAEnrutar = {
                componente: itemAnterior.componente,
                params: itemAnterior.params
            };

            this.abrirComponenteAnterior(info);
        });
    }

    abrirComponenteAnterior(
        ubicacion: UbicacionDelComponente
    ): void {
        try {
            const info = this.enrutadorService.informacionAEnrutar;

            const componente = this.componentFactoryResolver.resolveComponentFactory(info.componente);

            const infoAEnrutar: InformacionEnrutador = {
                posicion: ubicacion,
                params: info.params
            };


            if (ubicacion === UbicacionDelComponente.IZQUIERDA) {
                this.componentes.izquierda = this.tercio_izquierda.createComponent(componente);
                this.componentes.izquierda.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }

            if (ubicacion === UbicacionDelComponente.CENTRAL) {
                this.componentes.central = this.tercio_central.createComponent(componente);
                this.componentes.central.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }

            if (ubicacion === UbicacionDelComponente.DERECHA) {
                this.componentes.derecha = this.tercio_derecha.createComponent(componente);
                this.componentes.derecha.instance.configurarInformacionDelEnrutador(infoAEnrutar);
                this.enrutadorService.informacionAEnrutar = undefined;
                return;
            }
        } catch (error) {
        }
    }

    validarSiEsAlbum() {
        const indexUno = this.distribucion.izquierda.length - 1;
        const indexDos = this.distribucion.central.length - 1;
        const indexTres = this.distribucion.derecha.length - 1;

        const paramsAEnrutar = this.enrutadorService.informacionAEnrutar.params;

        if (
            this.validarInstanciaAlbum(indexUno, this.distribucion.izquierda) ||
            this.validarInstanciaAlbum(indexDos, this.distribucion.central) ||
            this.validarInstanciaAlbum(indexTres, this.distribucion.derecha)
        ) {
            return true;
        }

        return false;
    }

    validarInstanciaAlbum(index: number, distribucion: Array<ItemDistribucion>) {

        if (distribucion[index].componente === AlbumGeneralComponent ||
            distribucion[index].componente === AlbumAudiosComponent ||
            distribucion[index].componente === AlbumLinksComponent ||
            distribucion[index].componente === AlbumPerfilComponent) {
            return true
        }
        return false
    }

    validarInstanciaActivaDelComponente(
        validarParams: boolean = false,
        paramAValidar: string = ''
    ): boolean {
        if (!this.distribucion) {
            return false;
        }

        const indexUno = this.distribucion.izquierda.length - 1;
        const indexDos = this.distribucion.central.length - 1;
        const indexTres = this.distribucion.derecha.length - 1;

        const paramsAEnrutar = this.enrutadorService.informacionAEnrutar.params;

        if (
            this.validarInstanciaConParams(indexUno, this.distribucion.izquierda, validarParams, paramsAEnrutar, paramAValidar) ||
            this.validarInstanciaConParams(indexDos, this.distribucion.central, validarParams, paramsAEnrutar, paramAValidar) ||
            this.validarInstanciaConParams(indexTres, this.distribucion.derecha, validarParams, paramsAEnrutar, paramAValidar)
        ) {
            return true;
        }

        return false;
    }




    validarInstanciaConParams(
        index: number,
        distribucion: Array<ItemDistribucion>,
        validarParams: boolean,
        paramsAEnrutar: Array<ItemParam>,
        paramAValidar: string
    ): boolean {
        return (
            index >= 0 && distribucion[index].componente === this.enrutadorService.informacionAEnrutar.componente &&
            (
                !validarParams ||
                (
                    validarParams &&
                    this.compararParametros(paramAValidar, distribucion[index].params, paramsAEnrutar)
                )
            )
        );
    }

    compararParametros(
        paramAValidar: string,
        a: Array<ItemParam>,
        b: Array<ItemParam>,
    ): boolean {
        if (a.length !== b.length) {
            return false;
        }

        const indexUno = a.findIndex(e => e.nombre === paramAValidar);
        const indexDos = b.findIndex(e => e.nombre === paramAValidar);

        if (!(indexUno >= 0 && indexDos >= 0)) {
            return false;
        }

        const aa = (a[indexUno].valor === b[indexDos].valor);

        return aa;
    }
}

export enum UbicacionDelComponente {
    IZQUIERDA = 'izquierda',
    CENTRAL = 'central',
    DERECHA = 'derecha'
}

export enum TipoDeNavegacion {
    NORMAL = 'normal',
    VALIDAR_TIPO_DE_COMPONENTE = 'validar-tipo-componente',
    VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS = 'validar-tipo-componente-con-parametros',
}

export interface ComponentesCreados {
    izquierda: ComponentRef<any>;
    central: ComponentRef<any>;
    derecha: ComponentRef<any>;
}

export interface ItemParam {
    nombre: string;
    valor: any;
}

export interface InformacionEnrutador {
    posicion: UbicacionDelComponente;
    params: Array<ItemParam>;
}

export interface InformacionAEnrutar {
    componente: any;
    params: Array<ItemParam>;
}

export interface InformacionClicEnMano {
    posicionActual: UbicacionDelComponente;
    itemAEnrutar: InformacionAEnrutar;
}

export interface ItemDistribucion {
    posicion?: UbicacionDelComponente;
    componente?: any;
    params?: Array<ItemParam>;
}

export interface Distribucion {
    izquierda: Array<ItemDistribucion>;
    central: Array<ItemDistribucion>;
    derecha: Array<ItemDistribucion>;
}

export interface Enrutador {
    informacionEnrutador: InformacionEnrutador;
    configurarInformacionDelEnrutador(info: InformacionEnrutador): void;
}
export interface EstadoEnrutador {
    estado: boolean;
    posicicion?: UbicacionDelComponente;
    extras?: ExtrasEnrutador;
}
export interface ExtrasEnrutador {
    tipo: TipoDeNavegacion;
    paramAValidar?: string;
}
export interface ProyectoActivoEnrutador {
    ubicacion: UbicacionDelComponente;
    proyecto: ProyectoModel;
}
export interface NoticiaActivaEnrutador {
    ubicacion: UbicacionDelComponente;
    noticia: NoticiaModel;
}
export interface AlbumActivoEnrutador {
    ubicacion: UbicacionDelComponente;
    album: AlbumModel;
}

export interface PerfilActivoEnrutador {
    ubicacion: UbicacionDelComponente;
    perfil: PerfilModel;
    tipoPerfil: CatalogoTipoPerfilModel;
}

export interface IntercambioActivoEnrutador {
    ubicacion: UbicacionDelComponente;
    intercambio: IntercambioModel;
}
