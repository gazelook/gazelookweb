import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  GeneradorId
} from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos';
import {
  ColorTextoBoton, TipoBoton,
  ToastComponent
} from '@shared/componentes';
import { AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, EstiloErrorInput, EstiloInput, TamanoColorDeFondo, TamanoDeTextoConInterlineado, UsoAppBar } from '@shared/diseno/enums';
import { BotonCompartido, ConfiguracionAppbarCompartida, ConfiguracionToast, InputCompartido, LineaCompartida } from '@shared/diseno/modelos';
import { CuentaNegocio, PerfilNegocio } from 'dominio/logica-negocio';
import {
  PerfilModel
} from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../enrutador/enrutador.component';
import { MenuPrincipalComponent } from './../menu-principal/menu-principal.component';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent

  public perfilSeleccionado: PerfilModel
  public confLinea: LineaCompartida
  public contactoForm: FormGroup
  public inputsForm: Array<InputCompartido> = []
  public botonEnviar: BotonCompartido
  public mensaje: string

  public confAppBar: ConfiguracionAppbarCompartida
  public confToast: ConfiguracionToast
  public tema: string;

  public mensajeCorreo: boolean;
  public errorMsj: boolean;
  public contactUs: boolean
  public textoMensaje: string;
  public textoMensajeError: string;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    private perfilNegocio: PerfilNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private _location: Location,
    private generadorId: GeneradorId,
    private formBuilder: FormBuilder,
    private cuentaNegocio: CuentaNegocio,
    private translateService: TranslateService,
    private enrutadorService: EnrutadorService
  ) {
    this.mensaje = ''
    this.tema = ''
    this.contactUs = true
    this.mensajeCorreo = false
    this.errorMsj = false
    this.textoMensaje = '';
    this.textoMensajeError = '';
  }

  ngOnInit(): void {
    this.obtenerPerfil()

    if (this.perfilSeleccionado) {
      this.prepararAppBar()
      this.configuracionBotones()
      this.configurarToast()

      this.contactoForm = this.inicializarControlesFormularioAccionCrear()
      this.inputsForm = this.configurarInputsDelFormulario(this.contactoForm)
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  obtenerPerfil() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  prepararAppBar() {
    this.confAppBar = {
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [
          ]
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      ),
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v11texto1'
        },

      }

    }

  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    }
  }

  configuracionBotones() {
    this.botonEnviar = {
      text: 'm6v11texto10',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => {
        this.enviarCorreoDeContacto()
      },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };

  }


  inicializarControlesFormularioAccionCrear(proyecto?): FormGroup {
    const registroForm: FormGroup = this.formBuilder.group({
      contacName: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z0-9 ]+$'),
          Validators.minLength(3)
        ],
      ],
      name: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],
      email: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],

      message: ['', Validators.required]

    })

    return registroForm
  }

  configurarInputsDelFormulario(proyectoForm: FormGroup): Array<InputCompartido> {
    let inputsForm: Array<InputCompartido> = []

    if (proyectoForm) {
      // 0
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto5',
        data: proyectoForm.controls.contacName,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      })
      // 1
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto6',
        data: proyectoForm.controls.name,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      })
      // 2
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto7',
        data: proyectoForm.controls.email,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      })
    }
    return inputsForm
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
    }
  }

  validarCampos() {
    const { contacName, name, email, subject } = this.contactoForm.value
    const mensajeEnviar = this.mensaje

    if (!(contacName.length > 0)) {
      return false
    }

    if (!(name.length > 0)) {
      return false
    }

    if (!(email.length > 0)) {
      return false
    }

    if (!(this.tema.length > 0)) {
      return false
    }

  
    if (!(mensajeEnviar.length > 0)) {
      return false
    }

    return true
  }

  async enviarCorreoDeContacto() {
    this.botonEnviar.enProgreso = true
    this.textoMensaje = await this.translateService.
    get('m6v12texto13').toPromise();

    if (!this.validarCampos()) {
      this.toast.abrirToast('text4')
      this.botonEnviar.enProgreso = false
      return
    }

    const { contacName, name, email } = this.contactoForm.value
    const mensajeEnviar = this.mensaje

    const correo: CorreoContacto = {
      nombre: name,
      nombreContacto: contacName,
      email: email,
      tema: this.tema,
      mensaje: this.mensaje,
    }
    console.log('correo', correo);
    

    try {
      const status: string = await this.cuentaNegocio.enviarEmailDeContacto(correo).toPromise()
      if (!status) {
        throw new Error('')
      }
     
      this.botonEnviar.enProgreso = false
      this.mensajeCorreo = true;
      this.contactUs = false;
      this.contactoForm.reset()
      this.mensaje = ''
    } catch (error) {
      this.errorMsj = true;
      this.botonEnviar.enProgreso = false
      this.textoMensajeError = await this.translateService.
        get('text4').toPromise();
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
}
export interface CorreoContacto {
  nombre?: string,
  nombreContacto?: string,
  email?: string,
  pais?: string,
  direccion?: string,
  tema?: string,
  mensaje?: string,
}
