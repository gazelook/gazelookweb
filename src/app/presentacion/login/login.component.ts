import { CodigosCatalogoIdiomaCod } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import { Location } from '@angular/common';
import {
  Component,
  HostListener,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId, VariablesGlobales } from '@core/servicios/generales';
import {
  LlavesLocalStorage,
  LlavesSessionStorage,
} from '@core/servicios/locales';
import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos';
import { MetodosLocalStorageService } from '@core/util';
import {
  ColorTextoBoton,
  PensamientoCompartidoComponent,
  PortadaGazeComponent,
  TipoBoton,
  ToastComponent,
} from '@shared/componentes';
import {
  AnchoLineaItem,
  ColorFondoLinea,
  EspesorLineaItem,
  EstiloErrorInput,
  EstiloInput,
  EstiloItemPensamiento,
  TamanoDeTextoConInterlineado,
  TamanoLista,
  TamanoPortadaGaze,
  TipoPensamiento,
} from '@shared/diseno/enums';
import {
  BotonCompartido,
  ConfiguracionLineaVerde,
  ConfiguracionToast,
  DatosLista,
  InputCompartido,
  PensamientoCompartido,
  PortadaGazeCompartido,
} from '@shared/diseno/modelos';
import {
  CuentaNegocio,
  IdiomaNegocio,
  InternacionalizacionNegocio,
} from 'dominio/logica-negocio';
import { UsuarioModel } from 'dominio/modelo/entidades';
import { ConfiguracionPortadaGif } from '../../compartido/componentes/portada-gif/portada-gif.component';
import { RutasLocales } from '../../rutas-locales.enum';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  loginForm: FormGroup;
  public mostrarBanda: boolean;

  //MODELOS-INTERFACE
  pensamientoCompartido: PensamientoCompartido;
  inputEmail: InputCompartido;
  inputContrasena: InputCompartido;
  botonCompartido: BotonCompartido;
  botonSubscribirse: BotonCompartido;
  botonSubmit: BotonCompartido;
  dataListaAleatorio: DatosLista;
  configuracionToast: ConfiguracionToast;
  configuracionLineaVerde: ConfiguracionLineaVerde;
  configuracionLineaVerdeCompleta: ConfiguracionLineaVerde;
  public confBotonAvisoIdiomas: BotonCompartido;
  configuracionPortada: PortadaGazeCompartido;

  @ViewChild('container', { read: ViewContainerRef })
  container: ViewContainerRef;
  @ViewChild(PensamientoCompartidoComponent)
  pensamiento: PensamientoCompartidoComponent;
  @ViewChild('portadaGazeComponent') portadaGazeComponent: PortadaGazeComponent;
  components = [];

  public confDialogoRecuperarContrasena: ConfiguracionRecuperarContrasena;

  //COMPONENTES
  public confPortadaGifUno: ConfiguracionPortadaGif;
  public confPortadaGifDos: ConfiguracionPortadaGif;

  constructor(
    //SERVICIOS
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public variablesGlobales: VariablesGlobales,
    private generadorId: GeneradorId,
    private formBuilder: FormBuilder,
    private metodosLocalStorageService: MetodosLocalStorageService,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private router: Router,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private idiomaNegocio: IdiomaNegocio,
    public auth: AngularFireAuth
  ) {
    this.mostrarBanda = false;
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.iniciarSesion();
    }
  }

  ngOnInit(): void {
    this.detectarDispositivo();
    this.configurarVariablesGlobales();
    this.configurarPortadasGif();
    this.escucharCambioDeIdioma();
    this.escucharCambioLanding();
    this.iniciarElementos();
    this.configurarPortada();
    this.validarAvisoIdiomasEnLocalStorage();
    this.configurarBotones();
    this.validarIdiomaSeleccionado();
    this.inicializarDialogoRecuperarContrasena();
    this.configurarLineaVerde();
    this.verificarBanda();
    this.configurarListaPensamientoAleatorio();
  }

  luhn(value) {
    // Accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(value)) return false;
    // The Luhn Algorithm. It's so pretty.
    let nCheck = 0,
      bEven = false;
    value = value.replace(/\D/g, '');
    for (var n = value.length - 1; n >= 0; n--) {
      var cDigit = value.charAt(n),
        nDigit = parseInt(cDigit, 10);
      if (bEven && (nDigit *= 2) > 9) nDigit -= 9;
      nCheck += nDigit;
      bEven = !bEven;
    }
    return nCheck % 10 == 0;
  }

  detectarDispositivo() {
      console.log('voy a dectar');
      
    if (
      /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      console.log('navigator.userAgent MOVIL', navigator.userAgent);
      // this.router.navigateByUrl(Rutas.ANUNCIO_MOVIL)
        window.location.href = 'https://m.gazelook.com/';
    
    } else {
        console.log('navigator.userAgent', navigator.userAgent);
    }
  }
  configurarPortada() {
    this.configuracionPortada = {
      tamano: TamanoPortadaGaze.PORTADACOMPLETA,
      espacioDerecha: false,
    };
  }

  configurarListaPensamientoAleatorio() {
    this.dataListaAleatorio = {
      tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
      lista: [],
      cargarMas: () => {},
    };
  }

  configurarVariablesGlobales() {
    this.variablesGlobales.configurarDataAnimacion();
  }

  configurarLineaVerde() {
    this.configuracionLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false,
    };

    this.configuracionLineaVerdeCompleta = {
      ancho: AnchoLineaItem.ANCHO100,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEACELESTE,
      forzarAlFinal: false,
    };
  }

  verificarBanda() {
    if (this.variablesGlobales.mostrarAvisoIdiomas === false) {
      this.mostrarBanda = true;
    }
  }

  validarTipoEmail(data: { id: string; texto: string }) {
    console.log(data.texto);

    let validar_email =
      /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
    if (data.texto === '') {
      this.asignarMensajeDeErrorEnInput(data.id, 'text1');
      return;
    }
    if (!validar_email.test(data.texto)) {
      this.asignarMensajeDeErrorEnInput(data.id, 'text7');
      return;
    }
  }

  asignarMensajeDeErrorEnInput(id: string, mensaje: string): void {
    this.inputEmail.errorPersonalizado = mensaje;
  }

  //Inicia todos los componentes
  async iniciarElementos() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      contrasena: ['', [Validators.required]],
    });

    this.configuracionToast = {
      cerrarClickOutside: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };

    this.inputEmail = {
      id: this.generadorId.generarIdConSemilla(),
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.AMARILLO,
        estiloInput: EstiloInput.LOGIN,
      },
      placeholder: '',
      data: this.loginForm.controls.email,
      bloquearCopy: false,
      validarCampo: {
        validar: true,
        validador: (data: any) => {
          this.validarTipoEmail(data);
        },
      },
    };

    this.inputContrasena = {
      id: this.generadorId.generarIdConSemilla(),
      tipo: 'password',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.AMARILLO,
        estiloInput: EstiloInput.LOGIN,
      },
      placeholder: '',
      data: this.loginForm.controls.contrasena,
      contrasena: true,
      bloquearCopy: false,
    };

    this.pensamientoCompartido = {
      tipoPensamiento: TipoPensamiento.PENSAMIENTO_ALEATORIO,
      tituloPensamiento: 'm1v2texto17',
      esLista: false,
      configuracionItem: {
        estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
      },
    };
  }

  async configurarBotones() {
    this.botonSubscribirse = {
      text: 'texto11',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this._location.replaceState('/');
        // this.router.navigateByUrl(RutasLocales.MENU_PERFILES.toString())
      },
    };

    this.botonCompartido = {
      text: 'm1v2texto10',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.enviarLanding(),
    };

    this.botonSubmit = {
      text: 'm1v2texto16',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.CELESTEMAIN,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.iniciarSesion(),
    };

    this.confBotonAvisoIdiomas = {
      text: 'm1v2texto6',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.variablesGlobales.mostrarAvisoIdiomas = false;
        this.mostrarBanda = true;
        this.metodosLocalStorageService.guardar(
          LlavesLocalStorage.AVISO_IDIOMAS,
          this.variablesGlobales.mostrarAvisoIdiomas ? 1 : 0
        );
      },
    };
  }

  async validarIdiomaSeleccionado() {
    let idiomasArray = ['en', 'it', 'es', 'fr', 'de', 'pt'];
    let idiomaNavegador = window.navigator.language.substring(0, 2);
    let existe = idiomasArray.includes(idiomaNavegador);
    let idioma: CatalogoIdiomaEntity;
    idioma = {};
    if (existe) {
      let idiomaPrincipal = CodigosCatalogoIdiomaCod[idiomaNavegador];

      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma[idiomaPrincipal]
      );
    } else {
      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma.INGLES
      );
    }
    const idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (!idiomaSeleccionado && idioma) {
      
      this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
      this.internacionalizacionNegocio.usarIidoma(idioma.codNombre);
    }
  }


  enviarLanding() {

    this.variablesGlobales.configurarDataAnimacionParaGifs(true, true, true);
    this.variablesGlobales.animacion.$subjectCapaGif.next(true);
    // sessionStorage.setItem(LlavesSessionStorage.PAGINAS, '0')
    // sessionStorage.setItem(LlavesSessionStorage.MENU_ACTIVO, '-1')
    // sessionStorage.setItem(LlavesSessionStorage.MENUS_YA_ABIERTOS, JSON.stringify([]))
    // this.variablesGlobales.configurarDataAnimacion(true)
    // this.router.navigateByUrl(RutasLocales.LANDING.toString())
  }

  async iniciarSesion() {
    try {
      this.botonSubmit.enProgreso = true;

      if (!this.loginForm.valid) {
        this.botonSubmit.enProgreso = false;
        this.botonSubmit.enProgreso = false;
        this.inputEmail.error = true;
        this.inputContrasena.error = true;
        return;
      }

      const res: UsuarioModel = await this.cuentaNegocio
        .iniciarSesion(
          this.loginForm.value.email.trim().toLowerCase(),
          this.loginForm.value.contrasena.trim()
        )
        .toPromise();

      if (!res) {
        throw new Error('');
      }

      const dataFirebase = await this.auth.signInAnonymously();
      if (!dataFirebase || !dataFirebase.user || !dataFirebase.user.uid) {
        this.cuentaNegocio.cerrarSession();
        throw new Error('');
      }
      this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(
        dataFirebase.user.uid
      );

      this.loginForm.reset();
      this.navegarmenuSeleccionarPerfiles();
    } catch (error) {
      this.botonSubmit.enProgreso = false;
      this.toast.abrirToast('text7');
    }
  }

  navegarmenuSeleccionarPerfiles() {
    this._location.replaceState('/');
    this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES, {
      replaceUrl: true,
    });
  }

  //Cuando el usuario cambie de idioma los elemento cambia de idioma
  async cambiarIdioma() {
    if (this.pensamiento) {
      this.pensamiento.obtenerPensamientoAleatorio();
    }
  }

  inicializarDialogoRecuperarContrasena(mostrarDialogo: boolean = false) {
    this.confDialogoRecuperarContrasena = {
      mostrar: mostrarDialogo,
      email: '',
      mostrarStatus: false,
      status: '',
      mostrarLoader: false,
      mostrarError: false,
      error: '',
    };
  }

  async recuperarContrasena() {
    if (
      !this.confDialogoRecuperarContrasena.email ||
      !(this.confDialogoRecuperarContrasena.email.length > 0) ||
      !(this.confDialogoRecuperarContrasena.email.indexOf('@') >= 0) ||
      !(this.confDialogoRecuperarContrasena.email.indexOf('.') >= 0)
    ) {
      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarError = true;
      this.confDialogoRecuperarContrasena.error = 'text5';
      return;
    }

    try {
      this.confDialogoRecuperarContrasena.mostrarError = false;
      this.confDialogoRecuperarContrasena.mostrarLoader = true;
      const estatus: string = await this.cuentaNegocio
        .recuperarContrasena(
          this.confDialogoRecuperarContrasena.email.trim().toLowerCase()
        )
        .toPromise();

      if (!estatus) {
        throw new Error('');
      }

      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarStatus = true;
      this.confDialogoRecuperarContrasena.status = 'text65';
    } catch (error) {
      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarError = true;
      this.confDialogoRecuperarContrasena.error = 'text37';
    }
  }

  eventoTapEnModal(target: any) {
    target.classList.forEach((clase: any) => {
      if (
        clase === 'modal-recuperar-contrasena' &&
        !this.confDialogoRecuperarContrasena.mostrarLoader
      ) {
        this.inicializarDialogoRecuperarContrasena(false);
      }
    });
  }

  validarAvisoIdiomasEnLocalStorage() {
    const estado = this.metodosLocalStorageService.obtener(
      LlavesLocalStorage.AVISO_IDIOMAS
    );
    if (estado === 0) {
      this.variablesGlobales.mostrarAvisoIdiomas = false;
    }

    this.metodosLocalStorageService.guardar(
      LlavesLocalStorage.AVISO_IDIOMAS,
      this.variablesGlobales.mostrarAvisoIdiomas ? 1 : 0
    );
  }

  configurarPortadasGif() {
    const idioma =
      this.internacionalizacionNegocio.obtenerIdiomaInternacionalizacion();
    let codigo = 'en';
    if (idioma && idioma !== null) {
      codigo = idioma.toLowerCase();
    }

    this.confPortadaGifUno = {
      esIzquierdo: true,
      urlImagen:
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-a.jpg',
      llaveTexto: 'fwtexto5',
      imagenFija: false,
      // vaCentrado: true
    };

    this.confPortadaGifDos = {
      esIzquierdo: false,
      urlImagen:
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-b.jpg',
      llaveTexto: 'fwtexto5',
      imagenFija: false,
    };
  }

  escucharCambioDeIdioma() {
    this.variablesGlobales.animacion.$cambioDeIdioma.subscribe((codigo) => {
      if (!codigo) {
        return;
      }

      if (!(this.confPortadaGifUno && this.confPortadaGifDos)) {
        return;
      }

      if (this.variablesGlobales.animacion.mostrarGifUno) {
        return;
      }
      this.confPortadaGifUno.urlGif =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-' +
        codigo +
        '-a.gif';
      this.confPortadaGifDos.urlGif =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-' +
        codigo +
        '-b.gif';
    });
  }

  escucharCambioLanding() {
    this.variablesGlobales.animacion.$subjectCapaGif.subscribe((valor) => {
      if (valor) {
        setTimeout(() => {
          sessionStorage.setItem(LlavesSessionStorage.PAGINAS, '0');
          sessionStorage.setItem(LlavesSessionStorage.MENU_ACTIVO, '-1');
          sessionStorage.setItem(
            LlavesSessionStorage.MENUS_YA_ABIERTOS,
            JSON.stringify([])
          );
          this.variablesGlobales.configurarDataAnimacion(true);
          this.router.navigateByUrl(RutasLocales.LANDING.toString());
          sessionStorage.setItem(
            LlavesSessionStorage.ANIMACION_FINALIZADA,
            '0'
          );
          sessionStorage.setItem(
            LlavesSessionStorage.VIDE_DEMO,
            '0'
          );
        }, 4000);
      }
    });
  }
}

export interface ConfiguracionRecuperarContrasena {
  mostrar?: boolean;
  email?: string;
  mostrarLoader?: boolean;
  mostrarError?: boolean;
  error?: string;
  mostrarStatus?: boolean;
  status?: string;
}
