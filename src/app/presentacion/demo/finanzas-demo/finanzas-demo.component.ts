import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { Component, OnInit } from '@angular/core';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { Location } from '@angular/common';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
    ColorFondoLinea,
    EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { Enrutador, InformacionEnrutador } from 'presentacion/enrutador/enrutador.component';
import { EnrutadorService } from '@core/servicios/generales';

@Component({
	selector: 'app-finanzas',
	templateUrl: './finanzas-demo.component.html',
	styleUrls: ['./finanzas-demo.component.scss']
})
export class FinanzasDemoComponent implements OnInit, Enrutador {

	configuracionAppBar: ConfiguracionAppbarCompartida;
	perfilSeleccionado: PerfilModel;
	confLinea: LineaCompartida;

	botonCompra: BotonCompartido
	botonIntercambio: BotonCompartido

	botonBuscarCultural: BotonCompartido;
	botonPublicarCultural: BotonCompartido;

	mostrarCuotaAnual: boolean;
	mostrarCuotaUsuario: boolean;

	public informacionEnrutador: InformacionEnrutador

	constructor(
		private perfilNegocio: PerfilNegocio,
		public estilosDelTextoServicio: EstiloDelTextoServicio,
		private _location: Location,
		private variablesGlobales: VariablesGlobales,
		private enrutadorService: EnrutadorService
	) {
		this.mostrarCuotaAnual = false;
		this.mostrarCuotaUsuario = false;
	}

	ngOnInit(): void {
		this.prepararAppBar()
		this.configuracionBotones()
		this.variablesGlobales.mostrarMundo = false
	}

	prepararAppBar() {
		this.configuracionAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v20texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
			}
		}
	}

	volverAtras() {
		this._location.back()
	}

	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
			forzarAlFinal: false
		}
	}

	configuracionBotones() {
		this.botonBuscarCultural = {
			text: 'm6v5texto5', 
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => {
				// this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.INTERCAMBIO)
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

		this.botonPublicarCultural = {
			text: 'm6v5texto6', 
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => {
				// this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.COMPRA)

			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

	}


	abrirCuotaAnual() {
		this.mostrarCuotaAnual = !this.mostrarCuotaAnual;
		this.mostrarCuotaUsuario = false;

	}

	abrirCuotaUsuario() {
		this.mostrarCuotaUsuario = !this.mostrarCuotaUsuario;
		this.mostrarCuotaAnual = false;

	}

	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

}