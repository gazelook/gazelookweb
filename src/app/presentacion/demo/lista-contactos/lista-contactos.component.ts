import { CodigosCatalogoArchivosPorDefecto } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { MediaNegocio } from './../../../dominio/logica-negocio/media.negocio';
import { ArchivoModel } from './../../../dominio/modelo/entidades/archivo.model';
import { TranslateService } from '@ngx-translate/core';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { JsonDemoNegocio } from 'dominio/logica-negocio/json-demo.negocio';
import { MensajesAsociacionNegocio } from 'dominio/logica-negocio/mensajes-asociacion.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { MensajeModel } from 'dominio/modelo/entidades/mensaje.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos/resumen-perfil.interface';
import {
  ConfiguracionItemListaContactosCompartido,
  ConfigCruzContacto,
  ConfiguracionLineaVerde,
} from '@shared/diseno/modelos/lista-contactos.interface';
import { ItemResultadoBusqueda } from 'dominio/modelo/item-resultado-busqueda';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { AppbarComponent } from '@shared/componentes/appbar/appbar.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TamanoColorDeFondo } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import {
  EstilosDelTexto,
  ColorFondoLinea,
  EspesorLineaItem,
  ColorDelTexto,
  ColorDeBorde,
  ColorDeFondo,
} from '@shared/diseno/enums/estilos-colores-general';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { ParticipanteAsociacionModel } from 'src/app/dominio/modelo/entidades/participante-asociacion.model';

import { ConversacionModel } from 'src/app/dominio/modelo/entidades/conversacion.model';
import { HttpClient } from '@angular/common/http';
import {
  Enrutador,
  InformacionEnrutador,
} from '../../enrutador/enrutador.component';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';

@Component({
  selector: 'app-lista-contactos',
  templateUrl: './lista-contactos.component.html',
  styleUrls: ['./lista-contactos.component.scss'],
})
export class ListaContactosDemoComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @ViewChild('barra', { static: false }) appBar: AppbarComponent;
  public configuracionAppBar: ConfiguracionAppbarCompartida;
  public resultados: ItemResultadoBusqueda[];
  public cargando: boolean;
  public configListaDeseaGeneralContacto: ConfiguracionItemListaContactosCompartido;
  public configItemDeseanContactarmeContador: ConfiguracionItemListaContactosCompartido;
  public configResumenPerfil: ConfiguracionResumenPerfil;
  public configPensamiento: PensamientoCompartido;
  public configBoton: BotonCompartido;
  public idAsociacion: string;
  public configuacionLineaVerde: LineaCompartida;
  public util = FuncionesCompartidas;
  public configDialoEliminarContacto: DialogoCompartido;
  public tituloContactoResumenActivo: string;
  public archivosLocalStorage: ArchivoModel[];
  public archivosContactos: ArchivoModel[];
  posicionEncuentraContacto: number;
  dataListaContactos: DatosLista;
  dataListaPensamientos: DatosLista;
  dataListaMensajesNoLeidos: DatosLista;
  mostrarMensajesNoLeidos: boolean;
  error: string;
  ejecutarMetodo: true; //PARA SABER CUAL DE LOS METODOS SE VAN A EJECUTAR
  configuracionToast: ConfiguracionToast;
  configCruzContacto: ConfigCruzContacto;
  paginas: number;
  paginacionContactos: PaginacionModel<ParticipanteAsociacionModel>;
  paginacionMensajeNoLeidos: PaginacionModel<MensajeModel>;

  paginacionBusquedaPerfiles: PaginacionModel<PerfilModel>;
  eventoBuscador: Function;
  eventoCargarMas: Function;
  idPerfilBasico: string;
  totalMensajesNoLeidos: number;

  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private router: Router,
    private _location: Location,
    public estiloTexto: EstiloDelTextoServicio,
    private generadorId: GeneradorId,
    public internacionalizacionNegocio: InternacionalizacionNegocio,
    public mensajesAsociacionNegocio: MensajesAsociacionNegocio,
    private http: HttpClient,
    public jsonNegocio: JsonDemoNegocio,
    private translateService: TranslateService,
    private enrutadorService: EnrutadorService,
    private mediaNegocio: MediaNegocio
  ) {
    this.totalMensajesNoLeidos = 0;
    this.cargando = true;
    this.mostrarMensajesNoLeidos = false;
    this.tituloContactoResumenActivo = '';
  }

  ngOnInit(): void {
    this.obtenerArchivos();
    this.cargarContactos();
    this.prepararAppBar();
    this.configurarDialogoEliminar();
    this.configurarLineaVerde();
    this.cargarContadorMensajesNoLeidos();
    this.listarContactosPerfilDemo();
  }

  obtenerArchivos() {
    this.archivosLocalStorage =
      this.mediaNegocio.obtenerListaArchivosDefaultDelLocal();
    this.archivosContactos = this.archivosLocalStorage.filter(
      (element) =>
        element.catalogoArchivoDefault ===
        CodigosCatalogoArchivosPorDefecto.DEMO_LISTA_CONTACTOS
    );
  }

  cargarContadorMensajesNoLeidos() {
    let img = this.archivosLocalStorage.filter(
      (element) =>
        element.catalogoArchivoDefault ===
        CodigosCatalogoArchivosPorDefecto.CONTACTOS
    );

    this.cargando = false;
    this.configItemDeseanContactarmeContador = {
      id: '',
      usoItem: UsoItemListaContacto.USO_MENSAJE,
      mensaje: {
        contacto: {
          nombreContacto: 'm3v7texto3',
          estilosTextoSuperior: {
            color: ColorDelTexto.TEXTOAZULBASE,
            estiloTexto: EstilosDelTexto.BOLD,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          },
          estilosTextoInferior: {
            color: ColorDelTexto.TEXTOROJOBASE,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          },
          idPerfil: '',
        },
        contadorMensajes: 26,
      },
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        img[0].url,
        ColorDeBorde.BORDER_AZUL,
        this.util.obtenerColorFondoAleatorio(),
        true,
        UsoItemCircular.CIREXCLAMACION,
        true
      ),
      mostrarX: (this.configCruzContacto = {
        mostrar: false,
      }),
    };
  }

  configurarMensajeNoLeidos(
    conversacion: ConversacionModel
  ): ConfiguracionItemListaContactosCompartido {
    let usoCirculo: UsoItemCircular;
    let mensajeMostrar = '';
    if (conversacion.ultimoMensaje.contenido) {
      mensajeMostrar = conversacion.ultimoMensaje.contenido;
    } else {
      mensajeMostrar = 'Ha Enviado Adjunto ';
    }
    if (
      conversacion.ultimoMensaje.propietario.perfil.album[0].portada.principal
        .fileDefault
    ) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
    }

    return {
      id: conversacion.id,
      usoItem: UsoItemListaContacto.USO_MENSAJE,
      mensaje: {
        contacto: {
          nombreContacto:
            conversacion.ultimoMensaje.propietario.perfil.nombreContacto,
          estilosTextoSuperior: {
            color: ColorDelTexto.TEXTOAZULBASE,
            estiloTexto: EstilosDelTexto.BOLD,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          },
          estilosTextoInferior: {
            color: ColorDelTexto.TEXTONEGRO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
            textoOverflow: true,
          },
          idPerfil: conversacion.ultimoMensaje.propietario.perfil._id,
        },

        mensaje: mensajeMostrar,
      },
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        conversacion.ultimoMensaje.propietario.perfil.album[0].portada.principal
          .url,
        ColorDeBorde.BORDER_AZUL,
        this.util.obtenerColorFondoAleatorio(),
        true,
        usoCirculo,
        true
      ),
      mostrarX: (this.configCruzContacto = {
        mostrar: false,
      }),
      configuracionLineaVerde: this.configurarLineaVerde(
        PaddingLineaVerdeContactos.PADDING_1542_267
      ),
      overflowTexto: true,
      eventoCirculoNombre: (idAsociacion: string) => {},
    };
  }

  mostrarModalEliminar(idAsociacion: string) {
    this.idAsociacion = idAsociacion;
    this.posicionEncuentraContacto = this.dataListaContactos.lista.findIndex(
      (element) => element.id === idAsociacion
    );
    // this.dataListaContactos.lista[this.posicionEncuentraContacto]['mostrarX']['color'] = false
    this.configDialoEliminarContacto.mostrarDialogo = true;
  }

  cerrarModalEliminar() {
    this.dataListaContactos.lista[this.posicionEncuentraContacto]['mostrarX'][
      'color'
    ] = true;
    this.configDialoEliminarContacto.mostrarDialogo = false;
  }

  async configurarDialogoEliminar() {
    this.configDialoEliminarContacto = {
      mostrarDialogo: false,
      descripcion: '',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoEliminarContacto.mostrarDialogo = false;
          },
        },
        {
          text: 'm3v9texto3',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoEliminarContacto.mostrarDialogo = false;
          },
        },
      ],
    };
    this.configDialoEliminarContacto.descripcion = await this.translateService
      .get('m3v9texto1')
      .toPromise();
    this.configDialoEliminarContacto.listaAcciones[0].text =
      await this.translateService.get('m3v9texto2').toPromise();
    this.configDialoEliminarContacto.listaAcciones[1].text =
      await this.translateService.get('m3v9texto3').toPromise();
  }

  listarContactosPerfilDemo() {
    this.jsonNegocio.obtenerContactosDemo().subscribe((data) => {
      for (const perfilDemo of data) {
        this.dataListaContactos.lista.push(
          this.configuracionContactos(perfilDemo)
        );
      }
      this.dataListaContactos.cargando = false;

      let index1 = this.archivosContactos.find((e) =>
        e.url.includes('8d20f732-5c12-4c63-9b30-f87e88a8ae38.jpg')
      );
      this.dataListaContactos.lista[0].configCirculoFoto.urlMedia = index1.url;

      let index2 = this.archivosContactos.find((e) =>
        e.url.includes('6d004af6-d00b-4bab-9039-2c44d3a59d64.jpg')
      );
      this.dataListaContactos.lista[1].configCirculoFoto.urlMedia = index2.url;

      let index3 = this.archivosContactos.find((e) =>
        e.url.includes('e190b3fa-21fc-4eef-8675-561b4a051cdf.jpg')
      );
      this.dataListaContactos.lista[2].configCirculoFoto.urlMedia = index3.url;

      let index4 = this.archivosContactos.find((e) =>
        e.url.includes('5c189e1c-35ba-4877-89a5-abe0a282c405.jpg')
      );
      this.dataListaContactos.lista[3].configCirculoFoto.urlMedia = index4.url;

      let index5 = this.archivosContactos.find((e) =>
        e.url.includes('b48e4fc2-17b1-4479-a52b-9d2ef34f34bd.jpg')
      );
      this.dataListaContactos.lista[4].configCirculoFoto.urlMedia = index5.url;

      let index6 = this.archivosContactos.find((e) =>
        e.url.includes('2c4160a1-1180-46ae-9a0e-402d973bf8e8.jpg')
      );
      this.dataListaContactos.lista[5].configCirculoFoto.urlMedia = index6.url;

      let index7 = this.archivosContactos.find((e) =>
        e.url.includes('5250d3a0-9861-4506-8484-fcc5f4d4ef81.jpg')
      );
      this.dataListaContactos.lista[6].configCirculoFoto.urlMedia = index7.url;

      let index8 = this.archivosContactos.find((e) =>
        e.url.includes('bb148600-3053-45c0-b468-5b250884ff5c.jpg')
      );
      this.dataListaContactos.lista[7].configCirculoFoto.urlMedia = index8.url;
    });
  }

  configuracionContactos(
    contacto: ParticipanteAsociacionModel
  ): ConfiguracionItemListaContactosCompartido {
    let usoCirculo;
    this.cargando = false;
    if (contacto.contactoDe.album[0].portada.principal.fileDefault) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
    }
    return {
      id: contacto.contactoDe._id,
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: contacto.contactoDe.nombre,
        nombre: contacto.contactoDe.nombre,
        estilosTextoSuperior: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: ColorDelTexto.TEXTONEGRO,
          estiloTexto: EstilosDelTexto.REGULAR,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: '',
      },
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        contacto.contactoDe.album[0].portada.principal.url,
        ColorDeBorde.BORDER_ROJO,
        this.util.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        true
      ),
      mostrarX: (this.configCruzContacto = {
        mostrar: true,
        color: true,
      }),
      configuracionLineaVerde: this.configurarLineaVerde(
        PaddingLineaVerdeContactos.PADDING_1542_267
      ),
      eventoCirculoNombre: (idAsociacion: string) => {},
      eventoIconoX: (idAsociacion: string) => {
        // this.mostrarModalEliminar(idAsociacion)
      },
    };
  }

  cargarContactos() {
    this.configuracionToast = {
      cerrarClickOutside: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };
    this.dataListaContactos = {
      tamanoLista: TamanoLista.LISTA_CONTACTOS,
      lista: [],
      cargando: false,
    };
  }

  cerrarResumenPerfil() {
    this.configResumenPerfil.mostrar = false;
  }

  configurarLineaVerde(
    paddingLineaVerdeContactos?: PaddingLineaVerdeContactos
  ): ConfiguracionLineaVerde {
    return {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false,
      paddingLineaVerde: paddingLineaVerdeContactos,
    };
  }

  configurarGeneralFotoPerfilContactos(
    urlMedia: string,
    colorBorde: ColorDeBorde,
    colorFondo: ColorDeFondo,
    mostrarCorazon: boolean,
    usoItemCirculo: UsoItemCircular,
    activarClick: boolean
  ): ItemCircularCompartido {
    return {
      id: '',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: usoItemCirculo,
      esVisitante: false,
      urlMedia: urlMedia,
      activarClick: activarClick,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: true,
      textoBoton: '',
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: colorBorde,
      colorDeFondo: colorFondo,
      mostrarCorazon: mostrarCorazon,
    };
  }

  clickMostrarMensajesNoLeidos() {
    if (!this.mostrarMensajesNoLeidos) {
      this.mostrarMensajesNoLeidos = true;
    } else {
      this.mostrarMensajesNoLeidos = false;
    }
  }

  reitentarBusqueda() {}

  async prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v7texto2',
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      },
    };
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}
