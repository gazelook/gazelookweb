import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { EnrutadorService } from '@core/servicios/generales/enrutador/enrutador.service';
import { InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { MenuprincipalDemoComponent } from 'presentacion/demo/menuprincipal/menuprincipal.component';


@Component({
	selector: 'app-informacion-util',
	templateUrl: './informacion-util.component.html',
	styleUrls: ['./informacion-util.component.scss']
})
export class InformacionUtilDemoComponent implements OnInit {

	public sesionIniciada: boolean
	public perfilSeleccionado: PerfilModel

	public confAppBar: ConfiguracionAppbarCompartida
	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private cuentaNegocio: CuentaNegocio,
		private perfilNegocio: PerfilNegocio,
		private _location: Location,
		private enrutadorService: EnrutadorService
	) {

	}

	ngOnInit(): void {
		this.configurarEstadoSesion()
		this.configurarPerfilSeleccionado()
		this.configurarAppBar()
	
	}

	configurarEstadoSesion() {
		this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			
			accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v11texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				mostrarCasaHome: true,
				accionCasaHome: () => {

					this.enrutadorService.navegar(
						{
							componente: MenuprincipalDemoComponent,
							params: []
						},
						{
							estado: true,
							posicicion: this.informacionEnrutador.posicion,
							extras: {
								tipo: TipoDeNavegacion.NORMAL
							}
						}
					)
				}
			}
			
		}
	}


	
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

}
