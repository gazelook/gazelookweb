import { UbicacionDelComponente } from './../../enrutador/enrutador.component';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';
import { ConfiguracionLineaVerde } from '@shared/diseno/modelos';
import { TipoMenu } from 'src/app/compartido/componentes/item-menu/item-menu.component';
import { AnchoLineaItem } from 'src/app/compartido/diseno/enums/ancho-linea-item.enum';
import { ColorFondoItemMenu } from 'src/app/compartido/diseno/enums/estilos-colores-general';
import {
  TamanoColorDeFondo,
  TamanoItemMenu,
  TamanoLista,
} from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from 'src/app/compartido/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from 'src/app/compartido/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from 'src/app/compartido/diseno/modelos/appbar.interface';
import { DatosLista } from 'src/app/compartido/diseno/modelos/datos-lista.interface';
import { DialogoCompartido } from 'src/app/compartido/diseno/modelos/dialogo.interface';
import { ItemMenuCompartido } from 'src/app/compartido/diseno/modelos/item-menu.interface';
import { CuentaNegocio } from 'src/app/dominio/logica-negocio/cuenta.negocio';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '../../../compartido/diseno/enums/estilos-colores-general';
import {
  ItemAccion,
  ItemMenuModel,
  ItemSubMenu,
} from '../../../dominio/modelo/entidades/item-menu.model';
import {
  Enrutador,
  InformacionEnrutador,
  TipoDeNavegacion,
} from '../../enrutador/enrutador.component';
import { CompraIntermacioInicioDemoComponent } from '../compra-intermacio-inicio/compra-intermacio-inicio.component';
import { FinanzasDemoComponent } from '../finanzas-demo/finanzas-demo.component';
import { ListaContactosDemoComponent } from '../lista-contactos/lista-contactos.component';
import { MenuListaTipoProyectoDemoComponent } from '../menu-lista-tipo-proyecto/menu-lista-tipo-proyecto.component';
import { MenuPerfilesDemoComponent } from '../menu-perfiles/menu-perfiles.component';
import { NoticiasUsuariosDemoComponent } from '../noticias-usuarios/noticias-usuarios.component';
import { PensamientoDemoComponent } from '../pensamiento/pensamiento.component';
import { PublicarNoticiasProyectosDemoComponent } from '../publicar-noticias-proyectos-demo/publicar-noticias-proyectos-demo.component';
import {
  ColorTextoBoton,
  TipoBoton,
} from './../../../compartido/componentes/button/button.component';
import {
  TamanoDeTextoConInterlineado,
  TamanoPortadaGaze,
} from './../../../compartido/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from './../../../compartido/diseno/modelos/boton.interface';
import { PortadaGazeCompartido } from './../../../compartido/diseno/modelos/portada-gaze.interface';
import { VariablesGlobales } from './../../../nucleo/servicios/generales/variables-globales.service';
import { RutasDemo } from './../rutas-demo.enum';

@Component({
  selector: 'app-menuprincipal',
  templateUrl: './menuprincipal.component.html',
  styleUrls: ['./menuprincipal.component.scss'],
})
export class MenuprincipalDemoComponent implements OnInit, Enrutador {
  configuracionAppBar: ConfiguracionAppbarCompartida;
  listaMenu: ItemMenuModel[];
  itemSubMenu3Puntos: ItemSubMenu;
  itemMenuMultipleAccion: ItemMenuModel;
  public mostrarLinea: boolean;
  sesionIniciada = false;
  dataLista: DatosLista = {
    cargando: false,
    reintentar: () => {},
    tamanoLista: TamanoLista.TIPO_MENU_PRINCIPAL,
  };
  perfilSeleccionado: PerfilModel;
  configuracionPortada: PortadaGazeCompartido;
  configDialogoLateral: DialogoCompartido;
  botonEnter: BotonCompartido;
  public confLinea: ConfiguracionLineaVerde;
  public confBoton: BotonCompartido;
  public informacionEnrutador: InformacionEnrutador;
  public confDialogoConstruccion: DialogoCompartido;

  constructor(
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private perfilNegocio: PerfilNegocio,
    private cuentaNegocio: CuentaNegocio,
    private variablesGlobales: VariablesGlobales,
    private enrutadorService: EnrutadorService
  ) {
    this.mostrarLinea = false;
    this.prepararItemsMenu();
    this.prepararDataSubMenu3puntos();
    this.prepararDatosParaMenuMultipleAccion();
    this.configurarBotonPrincipal();
  }

  ngOnInit(): void {
    this.configuracionPortada = { tamano: TamanoPortadaGaze.PORTADACOMPLETA };
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
    this.variablesGlobales.mostrarMundo = true;
    this.cargarDatos();
    this.prepararAppBar();
    this.configurarLineas();
    this.configurarDialogoDemo();
    this.configurarDialogos();
    this.configuracionAppBar.gazeAppBar.subtituloDemo =
      this.obtenerTituloPrincipal(false);
  }

  async configurarBotonPrincipal() {
    this.confBoton = {
      text: 'm1v4texto1',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO);
      },
    };

    //this.confBoton.text = await this.servicioIdiomas.obtenerTextoLlave(this.configuracion.demoAppbar.boton.llaveTexto ? this.configuracion.demoAppbar.boton.llaveTexto : 'undefined')
  }

  configurarDialogos(): void {
    this.confDialogoConstruccion = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      descripcion: 'm7v4texto3',
    };
  }

  configurarLineas() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }
  configurarDialogoDemo() {
    this.configDialogoLateral = {
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      completo: true,
      descripcion: 'm1v3texto3',
    };
  }
  cargarDatos() {
    this.botonEnter = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m1v1texto6'),
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.navegarMenuPrincipal(),
    };
  }

  navegarMenuPrincipal() {}

  async prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () =>
        // this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
        this._location.back(),
      demoAppbar: {
        mostrarLineaVerde: false,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v4texto2',
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      },
    };
  }

  obtenerTituloPrincipal(profileCreated: boolean) {
    if (profileCreated) {
      return {
        mostrar: true,
        llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre,
      };
    } else {
      return {
        mostrar: true,
        llaveTexto: 'm1v3texto2',
      };
    }
  }

  async prepararItemsMenu() {
    this.listaMenu = [
      {
        id: MenuPrincipal.PERFILES,
        titulo: ['m1v3texto1'],
        ruta: MenuPerfilesDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.MIS_PENSAMIENTOS,
        titulo: ['m2v11texto7', 'm2v11texto8', 'm2v11texto9'],
        ruta: PensamientoDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.GAZING,
        titulo: ['m2v11texto10', 'm2v11texto11'],
        ruta: ListaContactosDemoComponent,
        tipo: TipoMenu.ACCION,
      },

      {
        id: MenuPrincipal.PUBLICAR,
        titulo: ['m2v11texto12', 'm2v11texto13', 'm2v11texto14'],
        ruta: PublicarNoticiasProyectosDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.PROYECTOS,
        titulo: ['m2v11texto15', 'm2v11texto16'],
        ruta: MenuListaTipoProyectoDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.NOTICIAS,
        titulo: ['m2v11texto17', 'm2v11texto18'],
        ruta: NoticiasUsuariosDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.COMPRAS,
        titulo: ['m2v11texto19', 'm2v11texto19.1'],
        ruta: CompraIntermacioInicioDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.FINANZAS,
        titulo: ['m2v11texto21'],
        ruta: FinanzasDemoComponent,
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.ANUNCIOS,
        titulo: ['m2v11texto22', 'm2v11texto23'],
        tipo: TipoMenu.ANUNCIOS,
      },
    ];
  }

  prepararItemMenu(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo:
        item.id == MenuPrincipal.ANUNCIOS
          ? ColorFondoItemMenu.TRANSPARENTE
          : ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo[0] ? item.titulo[0].toString() : null,
      texto2: item.titulo[1] ? item.titulo[1].toString() : null,
      texto3: item.titulo[2] ? item.titulo[2].toString() : null,
      tipoMenu: item.tipo,
      linea: {
        mostrar: true,
        configuracion: {
          ancho:
            item.id == MenuPrincipal.ANUNCIOS ||
            item.id == MenuPrincipal.FINANZAS
              ? AnchoLineaItem.ANCHO6028
              : AnchoLineaItem.ANCHO6382,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
          cajaGaze: item.id == MenuPrincipal.ANUNCIOS,
        },
      },
      gazeAnuncios: item.id == MenuPrincipal.ANUNCIOS,
      idInterno: item.id,
      onclick: () => this.navigationSubMenu(item.ruta),
      dobleClick: () => {},
    };
  }

  async prepararDataSubMenu3puntos() {
    this.itemSubMenu3Puntos = {
      id: '',
      titulo: '',
      mostrarDescripcion: false,
      menusInternos: [
        {
          id: 'mya',
          titulo: ['m2v11texto24'],
          action: () => {},
        },
        {
          id: 'he',
          titulo: ['m2v11texto25'],
          action: () => {},
        },
        {
          id: 'faq',
          titulo: ['m2v11texto26'],
          action: () => {},
        },
        {
          id: 'our',
          titulo: ['m2v11texto27'],
          action: () => {},
        },
        {
          id: 'web',
          titulo: ['m2v11texto28'],
          action: () => {},
        },
      ],
    };
  }

  navigationSubMenu(ruta: RutasLocales) {
    if (!ruta) {
      this.configDialogoLateral.mostrarDialogo = true;
      return;
    }

    this.enrutadorService.navegar(
      {
        componente: ruta,
        params: [],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL,
        },
      }
    );
  }

  prepararItemSubMenu(item: ItemSubMenu): ItemMenuCompartido {
    return {
      id: '',
      submenus: item.menusInternos ? item.menusInternos : [],
      mostrarDescripcion: item.mostrarDescripcion,
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo,
      tipoMenu: TipoMenu.SUBMENU,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {
        this.configDialogoLateral.mostrarDialogo = true;
      },
      dobleClick: () => {},
    };
  }

  mostrarDescripcion(item: any) {
    const elemento: HTMLElement = document.getElementById(
      'flecha' + item.id
    ) as HTMLElement;
    if (item.mostrarDescripcion) {
      item.mostrarDescripcion = false;
      elemento.classList.remove('rotar-flecha');
    } else {
      item.mostrarDescripcion = true;
      elemento.classList.add('rotar-flecha');
    }
  }

  prepararDatosParaMenuMultipleAccion() {
    this.itemMenuMultipleAccion = {
      id: 'multip',
      titulo: [
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto24',
          codigo: 'g',
        },
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto25',
          codigo: 'm',
        },
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto26',
          codigo: 'p',
        },
      ],
      tipo: TipoMenu.LEGAL,
    };
  }

  scroolEnContenedor() {
    const elemento = document.getElementById(
      'contenedorListaMenuPrincipal'
    ) as HTMLElement;
    if (!elemento) {
      return;
    }

    if (elemento.scrollTop < 30) {
      this.mostrarLinea = false;
      return;
    }

    if (elemento.scrollTop > 30) {
      this.mostrarLinea = true;
      return;
    }

    this.mostrarLinea = elemento.scrollTop >= 30;
  }

  prepararItemsParaMenuMultipleAccion(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      mostrarDescripcion: false,
      tamano: TamanoItemMenu.ITEM_MENU_CONTENIDO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      acciones: item.titulo as ItemAccion[],
      tipoMenu: TipoMenu.LEGAL,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {},
      dobleClick: () => {},
    };
  }

  clickBotonAtras() {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }
  async cambiarIdioma() {
    location.reload();
    // this.botonEnter.text = await this.internacionalizacionNegocio.obtenerTextoLlave('m1v1texto6')
    // this.enrutadorService.reiniciarTercio(UbicacionDelComponente.DERECHA)
    // this.enrutadorService.reiniciarTercio(UbicacionDelComponente.IZQUIERDA)
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}

export enum MenuPrincipal {
  PERFILES,
  MIS_PENSAMIENTOS,
  MIS_CONTACTOS,
  PUBLICAR,
  PROYECTOS,
  NOTICIAS,
  COMPRAS,
  FINANZAS,
  ANUNCIOS,
  GAZING,
}
