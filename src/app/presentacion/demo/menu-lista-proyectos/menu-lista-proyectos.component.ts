import { MenuprincipalDemoComponent } from './../menuprincipal/menuprincipal.component';
import { RutasDemo } from './../rutas-demo.enum';
import { RutasLocales } from './../../../rutas-locales.enum';
import {
    ColorFondoLinea,
    EspesorLineaItem,
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from './../../../compartido/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo } from './../../../compartido/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from './../../../compartido/diseno/enums/uso-appbar.enum';
import { TipoDialogo } from './../../../compartido/diseno/enums/tipo-dialogo.enum';
import { EstiloDelTextoServicio } from './../../../nucleo/servicios/diseno/estilo-del-texto.service';
import { DialogoCompartido } from './../../../compartido/diseno/modelos/dialogo.interface';
import { LineaCompartida } from './../../../compartido/diseno/modelos/linea.interface';
import { ConfiguracionAppbarCompartida } from './../../../compartido/diseno/modelos/appbar.interface';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';
import { ListaProyectosMundialesDemoComponent } from '../lista-proyectos/lista-proyectos.component';

@Component({
	selector: 'app-menu-lista-proyectos',
	templateUrl: './menu-lista-proyectos.component.html',
	styleUrls: ['./menu-lista-proyectos.component.scss']
})
export class MenuListaProyectosDemoComponent implements OnInit, Enrutador {
	public MenuListaProyectosEnum = MenuListaProyectos

	// Configuracion hijos
	public confAppBar: ConfiguracionAppbarCompartida
	public confLineaVerde: LineaCompartida
	public confLineaVerdeAll: LineaCompartida
	public confDialogoInicial: DialogoCompartido

	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location,
		private enrutadorService: EnrutadorService
	) {

	}

	ngOnInit(): void {
		this.configurarAppBar()
		this.configurarLinea()
		this.configurarDialogoDemo()
	}

	configurarDialogoDemo() {
		this.confDialogoInicial = {
			completo: true,
			mostrarDialogo: false,
			tipo: TipoDialogo.INFO_VERTICAL,
			descripcion: 'm1v15texto3',
		}
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,        botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v15texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
				mostrarCasaHome: true,
				accionCasaHome: () => {

					this.enrutadorService.navegar(
						{
							componente: MenuprincipalDemoComponent,
							params: []
						},
						{
							estado: true,
							posicicion: this.informacionEnrutador.posicion,
							extras: {
								tipo: TipoDeNavegacion.NORMAL
							}
						}
					)
				}
			}
		}
	}

	volverAtras() {
		this._location.back()
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
		this.confLineaVerdeAll = {
			ancho: AnchoLineaItem.ANCHO6920,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
	}

	navegarSegunMenu(menuTipo: MenuListaProyectos) {
		switch (menuTipo) {
			case MenuListaProyectos.NUEVOS_PROYECTOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_RECOMENDADOS_POR_ADMIN:
				this.abrirListaProyectos()
				break
			case MenuListaProyectos.FORO_DE_LOS_PROYECTOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_SELECCIONADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_ESPERA_DE_FONDOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_ESTRATEGIA:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_EJECUCION:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EJECUTADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_MENOS_VOTADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			default: break
		}
	}

	accionAtras() {
		this._location.back()
	}

	abrirListaProyectos() {
		this.enrutadorService.navegar(
			{
				componente: ListaProyectosMundialesDemoComponent,
				params: []
			},
			{
				estado: true,
				posicicion: this.informacionEnrutador.posicion,
				extras: {
					tipo: TipoDeNavegacion.NORMAL
				}
			}
		)
	}

	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}
}

export enum MenuListaProyectos {
	NUEVOS_PROYECTOS = '0',
	PROYECTOS_RECOMENDADOS_POR_ADMIN = '1',
	FORO_DE_LOS_PROYECTOS = '2',
	PROYECTOS_SELECCIONADOS = '3',
	PROYECTOS_EN_ESPERA_DE_FONDOS = '4',
	PROYECTOS_EN_ESTRATEGIA = '5',
	PROYECTOS_EN_EJECUCION = '6',
	PROYECTOS_EJECUTADOS = '7',
	PROYECTOS_MENOS_VOTADOS = '8',
}