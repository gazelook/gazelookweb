import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';
import { AnchoLineaItem } from 'src/app/compartido/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from 'src/app/compartido/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from 'src/app/compartido/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from 'src/app/compartido/diseno/modelos/appbar.interface';
import { DialogoCompartido } from 'src/app/compartido/diseno/modelos/dialogo.interface';
import { LineaCompartida } from 'src/app/compartido/diseno/modelos/linea.interface';
import { CuentaNegocio } from 'src/app/dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
import {
  ColorFondoLinea,
  EspesorLineaItem
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { NoticiaPublicarComponent } from '../publicar-noticia/publicar-noticia.component';
import { PublicarProyectoComponent } from '../publicar-proyecto/publicar-proyecto.component';
import { CodigosCatalogoTipoProyecto } from './../../../nucleo/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { InformacionUtilDemoComponent } from './../informacion-util/informacion-util.component';

@Component({
  selector: 'app-publicar',
  templateUrl: './publicar-noticias-proyectos-demo.component.html',
  styleUrls: ['./publicar-noticias-proyectos-demo.component.scss']
})
export class PublicarNoticiasProyectosDemoComponent implements OnInit, Enrutador {

  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaVerde: LineaCompartida
  public confLineaVerdeAll: LineaCompartida
  public confDialogoInicial: DialogoCompartido

  public informacionEnrutador: InformacionEnrutador

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location,
    private enrutadorService: EnrutadorService
  ) { }

  ngOnInit(): void {
    this.configurarAppBar()
    this.configurarLinea()
    this.configurarDialogo()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v10texto2'
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        // mostrarCasaHome: true
      }
    }
  }

  accionAtras() {
    this._location.back()
  }

  configurarLinea() {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  configurarDialogo() {
    this.confDialogoInicial = {
      completo: true,
      tipo: TipoDialogo.INFO_VERTICAL,
      mostrarDialogo: false,
      descripcion: 'm1v10texto3',
    }
  }

  irACrearProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
    this.enrutadorService.navegar(
      {
        componente: PublicarProyectoComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: codigo
          }
        ]
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL
        }
      }
    )
  }

  irAInformacionUtilDeProyectos() {
    this.enrutadorService.navegar(
      {
        componente: InformacionUtilDemoComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL
        }
      }
    )
  }

  irACrearNoticia() {
    this.enrutadorService.navegar(
      {
        componente: NoticiaPublicarComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL
        }
      }
    )
  }

  navegarSegunMenu(menu: number) {
    switch (menu) {
      case 0:
        this.confDialogoInicial.mostrarDialogo = true
        break
      case 1:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS)
        break
      case 2:
        this.confDialogoInicial.mostrarDialogo = true
        break
      case 3:
        this.confDialogoInicial.mostrarDialogo = true
        break
      case 4:
        this.irACrearNoticia()
        break
      case 5:
        this.irAInformacionUtilDeProyectos()
        break
    }
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }


}

