import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnrutadorService } from '@core/servicios/generales';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { Enrutador, InformacionEnrutador } from 'presentacion/enrutador/enrutador.component';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
 

@Component({
	selector: 'app-compra-intermacio-inicio',
	templateUrl: './compra-intermacio-inicio.component.html',
	styleUrls: ['./compra-intermacio-inicio.component.scss']
})
export class CompraIntermacioInicioDemoComponent implements OnInit, Enrutador {
	configuracionAppBar: ConfiguracionAppbarCompartida;
	listaTipoPerfil: CatalogoTipoPerfilModel[];
	dataLista: DatosLista;

	//contenido
	tituloCompra: string;
	subtituloCompra: string;
	cuerpoCompra: string;
	tituloIntercambio: string;
	subtituloIntercambio: string;
	cuerpoIntercambio: string;

	//botones
	botonCompra: BotonCompartido
	botonIntercambio: BotonCompartido

	confLinea: LineaCompartida
	perfilSeleccionado: PerfilModel

	public configDialogoLateral: DialogoCompartido

	mostrarDialogo = false;

	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private internacionalizacionNegocio: InternacionalizacionNegocio,
		private _location: Location,
		private translateService: TranslateService,
		private variablesGlobales: VariablesGlobales,
		private enrutadorService: EnrutadorService
	) {

	}

	ngOnInit(): void {
		this.prepararAppBar()
		this.configuracionBotones()
		this.configurarLinea()
		this.configurarDialogoDemo()
		this.variablesGlobales.mostrarMundo = false
	}

	configurarDialogoDemo() {
		this.configDialogoLateral = {
			mostrarDialogo: false,
			tipo: TipoDialogo.INFO_VERTICAL,
			completo: true,
			descripcion: 'm1v15texto3',
		}
	}

	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
			forzarAlFinal: false
		}
	}

	async configuracionBotones() {
		this.botonIntercambio = {
			text: 'm6v4texto9',
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.CELESTE,
			ejecutar: () => {
				this.mostrarDialogo = true;
				this.configDialogoLateral.mostrarDialogo = true;
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

		this.botonCompra = {
			text: 'm6v4texto10', 
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => {
				this.mostrarDialogo = true;
				this.configDialogoLateral.mostrarDialogo = true;
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

		// const textoa = await this.translateService.get('m6v4texto10').toPromise()
		// const textob = await this.translateService.get('m6v4texto15').toPromise()
		// this.botonIntercambio.text = textoa
		// this.botonCompra.text = textob
	}



	prepararAppBar() {
		this.configuracionAppBar = {
            usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
            accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
            demoAppbar: {
                mostrarLineaVerde: true,
                nombrePerfil: {
                    mostrar: false,
                },
                subtitulo: {
                    mostrar: true,
                    llaveTexto: 'm1v19texto2'
                },
                tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
            }
        }
	}

	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}
}
