import { FinanzasDemoComponent } from './finanzas-demo/finanzas-demo.component';
import { ActualizarProyectoComponent } from './actualizar-proyecto/actualizar-proyecto.component';
import { PublicarNoticiasProyectosDemoComponent } from './publicar-noticias-proyectos-demo/publicar-noticias-proyectos-demo.component';
import { NoticiaPublicarComponent } from './publicar-noticia/publicar-noticia.component';
import { PublicarProyectoComponent } from './publicar-proyecto/publicar-proyecto.component';
import { CompraIntermacioInicioDemoComponent } from './compra-intermacio-inicio/compra-intermacio-inicio.component';
import { NoticiasUsuariosDemoComponent } from './noticias-usuarios/noticias-usuarios.component';
import { ListaProyectosMundialesDemoComponent } from './lista-proyectos/lista-proyectos.component';
import { MenuListaProyectosDemoComponent } from './menu-lista-proyectos/menu-lista-proyectos.component';

import { InformacionUtilDemoComponent } from './informacion-util/informacion-util.component';
import { MenuListaTipoProyectoDemoComponent } from './menu-lista-tipo-proyecto/menu-lista-tipo-proyecto.component';
import { MenuPerfilesDemoComponent } from './menu-perfiles/menu-perfiles.component';
import { PensamientoDemoComponent } from './pensamiento/pensamiento.component';
import { ListaContactosDemoComponent } from './lista-contactos/lista-contactos.component';
import { MenuprincipalDemoComponent } from './menuprincipal/menuprincipal.component'
import { RutasDemo } from './rutas-demo.enum'
import { DemoComponent } from './demo.component'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DetalleDemoPerfilComponent } from './detalle-perfil/detalle-perfil.component';

const routes: Routes = [
  {
    path: '',
    component: DemoComponent,
    children: [
      {
        path: RutasDemo.MENU_PRINCIPAL.toString(),
        component: MenuprincipalDemoComponent
      },
      {
        path: RutasDemo.MENU_PERFILES.toString(),
        component: MenuPerfilesDemoComponent
      },
      {
        path: RutasDemo.MI_PERFIL.toString(),
        component: DetalleDemoPerfilComponent
      },
      {
        path: RutasDemo.MIS_CONTACTOS.toString(),
        component: ListaContactosDemoComponent
      },
      {
        path: RutasDemo.CREAR_PENSAMIENTO.toString(),
        component: PensamientoDemoComponent
      },
      {
        path: RutasDemo.MENU_LISTA_TIPO_PROYECTOS.toString(),
        component: MenuListaTipoProyectoDemoComponent
      },
      {
        path: RutasDemo.MENU_LISTA_PROYECTOS.toString(),
        component: MenuListaProyectosDemoComponent
      },
      {
        path: RutasDemo.INFORMACION_UTIL.toString(),
        component: InformacionUtilDemoComponent
      },
      {
        path: RutasDemo.MENU_PUBLICAR_PROYECTOS_NOTICIAS.toString(),
        component: PublicarNoticiasProyectosDemoComponent
      },
      {
        path: RutasDemo.LISTA_PROYECTOS.toString(),
        component: ListaProyectosMundialesDemoComponent
      },
      {
        path: RutasDemo.NOTICIAS_USUARIOS.toString(),
        component: NoticiasUsuariosDemoComponent
      },
      {
        path: RutasDemo.COMPRAS_INTER.toString(),
        component: CompraIntermacioInicioDemoComponent
      },
      {
        path: RutasDemo.PUBLICAR_PROYECTO.toString(),
        component: PublicarProyectoComponent
      },
      {
        path: RutasDemo.PUBLICAR_NOTICIA.toString(),
        component: NoticiaPublicarComponent
      },
      {
        path: RutasDemo.VISITAR_PROYECTO.toString(),
        component: ActualizarProyectoComponent
      },
      {
        path: RutasDemo.FINANZAS.toString(),
        component: FinanzasDemoComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule {

}
