import { InformacionUtilDemoComponent } from './../informacion-util/informacion-util.component';
import { TipoDialogo } from '../../../compartido/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '../../../compartido/diseno/modelos/dialogo.interface';
import { RutasDemo } from '../rutas-demo.enum';
import { CodigosCatalogoTipoPerfil } from '../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { RutasNoticias } from '../../noticias/rutas-noticias.enum';
import { RutasProyectos } from '../../proyectos/rutas-proyectos.enum';
import { CodigosCatalogoTipoProyecto } from '../../../nucleo/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { TamanoColorDeFondo } from '../../../compartido/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '../../../compartido/diseno/enums/uso-appbar.enum';
import { Router } from '@angular/router';
import { PerfilNegocio } from '../../../dominio/logica-negocio/perfil.negocio';
import { CuentaNegocio } from '../../../dominio/logica-negocio/cuenta.negocio';
import { EstiloDelTextoServicio } from '../../../nucleo/servicios/diseno/estilo-del-texto.service';
import { LineaCompartida } from '../../../compartido/diseno/modelos/linea.interface';
import { ConfiguracionAppbarCompartida } from '../../../compartido/diseno/modelos/appbar.interface';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Component, OnInit } from '@angular/core';
import { AnchoLineaItem } from 'src/app/compartido/diseno/enums/ancho-linea-item.enum';
import {
    ColorFondoLinea,
    EspesorLineaItem,
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { Location } from '@angular/common';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';
import { PublicarProyectoComponent } from '../publicar-proyecto/publicar-proyecto.component';
import { MenuListaProyectosDemoComponent } from '../menu-lista-proyectos/menu-lista-proyectos.component';

@Component({
	selector: 'app-menu-lista-tipo-proyecto',
	templateUrl: './menu-lista-tipo-proyecto.component.html',
	styleUrls: ['./menu-lista-tipo-proyecto.component.scss']
})
export class MenuListaTipoProyectoDemoComponent implements OnInit, Enrutador {

	public confAppBar: ConfiguracionAppbarCompartida
	public confLineaVerde: LineaCompartida
	public confLineaVerdeAll: LineaCompartida

	public configDialogoInicial: DialogoCompartido

	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private cuentaNegocio: CuentaNegocio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private _location: Location,
		private enrutadorService: EnrutadorService
	) {

	}

	ngOnInit(): void {
		this.configurarAppBar()
		this.configurarLinea()
		this.configurarDialogoDemo()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v14texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
			}
		}
	}

	volverAtras() {
		this._location.back()
	}

	configurarDialogoDemo() {
		this.configDialogoInicial = {
			completo: true,
			mostrarDialogo: false,
			tipo: TipoDialogo.INFO_VERTICAL,
			descripcion: 'm1v14texto3',
		}
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}

		this.confLineaVerdeAll = {
			ancho: AnchoLineaItem.ANCHO6920,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
	}

	irAListaProyectosSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
		this.enrutadorService.navegar(
			{
				componente: MenuListaProyectosDemoComponent,
				params: []
			},
			{
				estado: true,
				posicicion: this.informacionEnrutador.posicion,
				extras: {
					tipo: TipoDeNavegacion.NORMAL
				}
			}
		)
	}


	
	irAInformacionUtilDeProyectos() {
		this.enrutadorService.navegar(
			{
				componente: InformacionUtilDemoComponent,
				params: []
			},
			{
				estado: true,
				posicicion: this.informacionEnrutador.posicion,
				extras: {
					tipo: TipoDeNavegacion.NORMAL
				}
			}
		)
	}

	navegarSegunMenu(menu: number) {
		
		switch (menu) {
			case 0:
				this.irAListaProyectosSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL)
				break
			case 1:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 2:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 3:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 4:
				// this.configDialogoInicial.mostrarDialogo = true
				this.irAInformacionUtilDeProyectos()
				
				// this.enrutadorService.navegar(
				// 	{
				// 		componente: InformacionUtilDemoComponent,
				// 		params: []
				// 	},
				// 	{
				// 		estado: true,
				// 		posicicion: this.informacionEnrutador.posicion,
				// 		extras: {
				// 			tipo: TipoDeNavegacion.NORMAL
				// 		}
				// 	}
				// )
				break
		}
	}

	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

}
