import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
  CodigoEstadoParticipanteAsociacion,
  TipoParticipanteAsociacion,
} from '@core/servicios/remotos/codigos-catalogos/codigo-estado-partic-aso.enum';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { EnrutadorService } from '@core/servicios/generales';
import { TranslateService } from '@ngx-translate/core';
import { AppbarComponent } from '@shared/componentes/appbar/appbar.component';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import { ItemCirculoComponent } from '@shared/componentes/item-circulo/item-circulo.component';
import { ItemRectanguloComponent } from '@shared/componentes/item-rectangulo/item-rectangulo.component';
import { PortadaGazeComponent } from '@shared/componentes/portada-gaze/portada-gaze.component';
import { AccionesItemCircularRectangular } from '@shared/diseno/enums/acciones-general.enum';
import { AlturaResumenPerfil } from '@shared/diseno/enums/altura-resumen-perfil.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';
import {
  TamanoDeTextoConInterlineado,
  TamanoPortadaGaze,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import {
  EstiloItemPensamiento,
  TipoPensamiento,
} from '@shared/diseno/enums/tipo-pensamiento.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import {
  UsoItemCircular,
  UsoItemRectangular,
} from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { InfoAccionCirRec } from '@shared/diseno/modelos/info-accion-cir-rec.interface';
import { ItemRectangularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import {
  ConfigCruzContacto,
  ConfiguracionItemListaContactosCompartido,
  ConfiguracionLineaVerde,
  ConfiguracionListaContactosCircular,
} from '@shared/diseno/modelos/lista-contactos.interface';
import { ConfiguracionListaNoticias } from '@shared/diseno/modelos/lista-noticias.interface';
import { ConfiguracionListaProyectos } from '@shared/diseno/modelos/lista-proyectos.interface';
import { PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos/resumen-perfil.interface';
import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos/titulo-rectangulo.interface';
import { AlbumEntity } from 'dominio/entidades/album.entity';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';
import { PerfilEntity } from 'dominio/entidades/perfil.entity';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { ParticipanteAsociacionNegocio } from 'dominio/logica-negocio/participante-asociacion.negocio';
import { PensamientoNegocio } from 'dominio/logica-negocio/pensamiento.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { MenuprincipalDemoComponent } from 'presentacion/demo/menuprincipal/menuprincipal.component';
import { TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { ListaContactosCircularComponent } from '@shared/componentes/lista-contactos-circular/lista-contactos-circular.component';
import { PensamientoCompartidoComponent } from '@shared/componentes/pensamiento/pensamiento-compartido.component';
import {
  TamanoColorDeFondo,
  TamanoLista,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { ProyectoEntity } from 'dominio/entidades/proyecto.entity';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { JsonDemoNegocio } from 'dominio/logica-negocio/json-demo.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { JsonDemoServiceLocal } from '@core/servicios/locales/json-demo.service';
import {
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  ColorFondoLinea,
  EspesorLineaItem,
  EstilosDelTexto,
} from '@shared/diseno/enums/estilos-colores-general';
import {
  Enrutador,
  InformacionEnrutador,
} from 'presentacion/enrutador/enrutador.component';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';

@Component({
  selector: 'app-detalle-perfil',
  templateUrl: './detalle-perfil.component.html',
  styleUrls: ['./detalle-perfil.component.scss'],
})
export class DetalleDemoPerfilComponent implements OnInit, Enrutador {
  @ViewChild('listaContactos', { static: false })
  listaContactos: ListaContactosCircularComponent;
  @ViewChild('noticia', { static: false }) noticia: ItemRectanguloComponent;
  @ViewChild('portadaGaze', { static: false }) portada: PortadaGazeComponent;
  @ViewChild('albumPerfil', { static: false })
  albumPerfil: ItemCirculoComponent;
  @ViewChild('albumGeneral', { static: false })
  albumGeneral: ItemRectanguloComponent;
  @ViewChild('pensamientos', { static: false })
  pensamientos: PensamientoCompartidoComponent;
  @ViewChild('barra', { static: false }) appBar: AppbarComponent;
  @ViewChild('botonInvitacion') botonInvitacion: ElementRef<HTMLElement>;

  public configuracionAppBar: ConfiguracionAppbarCompartida; //Para enviar la configuracion de para presentar el appBar
  public perfilSeleccionado: PerfilEntity;
  public miPerfil: PerfilModel;
  public esMiPerfil: boolean;
  public confItemCir: ItemCircularCompartido; // Configuracion item del circulo
  public confItemRec: ItemRectangularCompartido; // Configuracion item del rectangulo
  public perfil: PerfilModel; // Model del perfil

  public configuracionPortada: PortadaGazeCompartido;
  public archivosPorDefecto: Array<ArchivoModel>;
  public mensajeError: string;

  // Parametros internos - generales
  public mostrarCuerpoLoader: boolean; // Oculta o muestra el loader
  public mostrarError: boolean;
  public util = FuncionesCompartidas;
  public dataLista: DatosLista;
  public cargando: boolean; //PRESENTAR EL CARGANDO
  public esPublico: boolean;
  public nombreUsuario: string;

  // Noticias
  public archivoPorDefectoNoticia: ArchivoModel;
  public configuracionesNoticias: Array<CongifuracionItemProyectosNoticias>;
  public placeholderNoticia: CongifuracionItemProyectosNoticias;
  public configNoticias: ConfiguracionListaNoticias;
  public paginacionNoticias: PaginacionModel<NoticiaEntity>;
  public mostrarListaNoticias = false;

  //Proyectos
  public configuracionesProyectos: Array<CongifuracionItemProyectosNoticias>;
  public placeholderProyecto: CongifuracionItemProyectosNoticias;
  public configProyectos: ConfiguracionListaProyectos;
  public paginacionProyectos: PaginacionModel<ProyectoEntity>;
  public mostrarListaProyectos = false;

  //Contactacos
  public tituloNoContactos: CongifuracionTituloRectangulo;
  public configContactoCircular: ConfiguracionListaContactosCircular;
  public configuracionBotonContatos: BotonCompartido;
  public paginacionContactos: PaginacionModel<ParticipanteAsociacionModel>;
  public numeroContactos: number;
  public configCruzContacto: ConfigCruzContacto;
  public dataListaContactos: DatosLista;

  //pensamientos
  public pensamientoCompartido: PensamientoCompartido;
  public paginacionPensamiento: PaginacionModel<PensamientoModel>;
  public dataListaPensamientos: DatosLista;

  public abrirModal: boolean = false;
  public error: string;
  public botonesInvitacion = [];
  public mostrarBotonesInvitacion: boolean = false;
  public configResumenPerfil: ConfiguracionResumenPerfil;
  public paginacionBusquedaPerfiles: PaginacionModel<PerfilModel>;
  public eventoBuscador: Function;
  public eventoCargarMas: Function;
  public idPerfilBasico: string;
  public esAccionAtras: boolean;

  dataListaPublico: DatosLista; //lISTA DE
  dataListaPrivado: DatosLista; //lISTA DE

  public archivosLocalStorage: ArchivoModel[];
  public archivosContactos: ArchivoModel[];
  public archivosNoticias: ArchivoModel[];
  public archivosProyectos: ArchivoModel[];

  public fotosPrueba: any;
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private albumNegocio: AlbumNegocio,
    private generadorId: GeneradorId,
    private translateService: TranslateService,
    public estiloTexto: EstiloDelTextoServicio,
    private route: ActivatedRoute,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private perfilNegocio: PerfilNegocio,
    public mediaNegocio: MediaNegocio,
    private participanteAsoNegocio: ParticipanteAsociacionNegocio,
    private pensamientoNegocio: PensamientoNegocio,
    private noticiaNegocio: NoticiaNegocio,
    public proyectoNegocio: ProyectoNegocio,
    private router: Router,
    private _location: Location,
    public jsonNegocio: JsonDemoNegocio,
    private jsonDemoNegocio: JsonDemoNegocio,
    private jsonDemoServiceLocal: JsonDemoServiceLocal,
    private variablesGlobales: VariablesGlobales,
    private enrutadorService: EnrutadorService
  ) {
    this.controlNavigation();
    this.archivosLocalStorage = [];
    this.archivosContactos = [];
    this.archivosNoticias = [];
    this.archivosProyectos = [];

    this.numeroContactos = 12;
    this.configNoticias = {};
    this.configuracionesNoticias = [];
    this.configuracionesProyectos = [];
    this.configProyectos = {};
    this.archivosPorDefecto = [];
    this.mostrarCuerpoLoader = true;
    this.mostrarError = false;
    this.mensajeError = '';
    this.esMiPerfil = true;
    this.paginacionPensamiento = {
      paginaActual: 0,
      proximaPagina: true,
    };

    this.paginacionNoticias = {
      paginaActual: 0,
      proximaPagina: true,
    };

    this.paginacionProyectos = {
      paginaActual: 0,
      proximaPagina: true,
    };

    this.paginacionContactos = {
      paginaActual: 0,
      proximaPagina: true,
    };

    this.paginacionBusquedaPerfiles = {
      paginaActual: 0,
      proximaPagina: true,
    };
    this.eventoBuscador = () => {
      this.reiniciarBusqueda();

      if (
        this.configuracionAppBar.searchBarAppBar.buscador.configuracion
          .valorBusqueda.length > 0
      ) {
        if (
          this.configuracionAppBar.searchBarAppBar.buscador.configuracion
            .valorBusqueda.length > 0
        ) {
          this.buscarPerfiles(
            this.configuracionAppBar.searchBarAppBar.buscador.configuracion
              .valorBusqueda
          );
        }
      }
    };
    this.eventoCargarMas = () => {
      if (
        this.configuracionAppBar.searchBarAppBar.buscador.configuracion
          .valorBusqueda.length > 0
      ) {
        this.buscarPerfiles(
          this.configuracionAppBar.searchBarAppBar.buscador.configuracion
            .valorBusqueda
        );
      }
    };
  }

  ngAfterViewInit() {}
  ngOnInit() {
    this.variablesGlobales.mostrarMundo = false;

    this.obtenerArchivos();
    this.obtenerPerfilSeleccionado();
    this.cargarContactos();
    this.cargarfotosPrueba();
  }

  obtenerArchivos() {
    this.archivosLocalStorage =
      this.mediaNegocio.obtenerListaArchivosDefaultDelLocal();
    this.archivosContactos = this.archivosLocalStorage.filter(
      (element) =>
        element.catalogoArchivoDefault ===
        CodigosCatalogoArchivosPorDefecto.DEMO_LISTA_CONTACTOS
    );
  }

  cargarContactos() {
    this.dataListaContactos = {
      tamanoLista: TamanoLista.LISTA_CONTACTOS,
      lista: [],
      cargarMas: () => this.configuracionContactos(),
      cargando: false,
    };
  }

  async iniciarComponentes() {
    this.prepararAppBar();
    this.configurarProyectos(this.perfilSeleccionado.proyectos);
    this.configurarNoticias(this.perfilSeleccionado.noticias);
    this.cambiarValorCapasDelCuerpo(false);
    this.configurarAlbumPerfil();
    this.configurarAlbumGeneral();
    this.configurarPensamientos();
    // this.seleccionarPensamientoMostrar(0)
    // this.cargarPensamientos();

    this.obtenerArchivosPorDefecto();
    this.configurarTituloNocontactos();
    this.configurarPlaceholders();
    this.configurarPortada();
    this.configuracionBotonContactos();
    // this.configuracionBotonesInvitacion();
    this.configuracionListaNoticias();
  }

  ngOnDestroy() {}
  // Cambia el valor de las capas del cuerpo
  cambiarValorCapasDelCuerpo(
    mostrarCuerpoLoader: boolean,
    mostrarError: boolean = false,
    mensajeError: string = ''
  ) {
    this.mostrarCuerpoLoader = mostrarCuerpoLoader;
    this.mostrarError = mostrarError;
    this.mensajeError = mensajeError;
  }

  // prepararAppBar() {
  //   // this.cargando = false
  //   let llaveTitulo = '';
  //   if (this.esMiPerfil) {
  //     llaveTitulo = 'MI PERFIL';
  //   } else {
  //     llaveTitulo = this.perfilSeleccionado?.nombreContacto + ' perfil';
  //   }
  //   this.configuracionAppBar = {
  //     usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
  //     accionAtras: () => {
  //       this.accionAtras();
  //     },
  //     searchBarAppBar: {
  //       nombrePerfil: {
  //         mostrar: true,
  //         llaveTexto: this.miPerfil.tipoPerfil.nombre,
  //       },
  //       buscador: {
  //         mostrar: true,
  //         configuracion: {
  //           disable: false,
  //           placeholder: "SEARCH PROFILES",
  //           valorBusqueda: '',
  //           entidad: CodigosCatalogoEntidad.PERFIL,
  //           buscar: this.eventoBuscador,
  //           cerrarBuscador: () => this.reiniciarBusqueda(),
  //           configuracionLista: {
  //             cargando: false,
  //             tamanoLista: TamanoLista.LISTA_CONTACTOS,
  //             lista: [],
  //             reintentar: this.eventoBuscador,
  //             cargarMas: this.eventoCargarMas
  //           },
  //           mostrarIconoLupa: true,
  //           mostrarResultado: false,
  //           fondoUniverso: true
  //         }

  //       },
  //       mostrarDivBack: {
  //         icono: true,
  //         texto: true,
  //       },
  //       mostrarTextoHome: true,
  //       subtitulo: {
  //         mostrar: true,
  //         llaveTexto: llaveTitulo,
  //       },
  //       mostrarLineaVerde: true,
  //       tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
  //     },
  //   };
  // }

  async prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v7texto2',
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        mostrarCasaHome: true,
        accionCasaHome: () => {
          this.enrutadorService.navegar(
            {
              componente: MenuprincipalDemoComponent,
              params: [],
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.NORMAL,
              },
            }
          );
        },
      },
    };
  }

  async accionAtras() {
    this.esAccionAtras = true;

    this._location.back();
  }

  reiniciarBusqueda() {
    this.paginacionBusquedaPerfiles.paginaActual = 0;
    this.paginacionBusquedaPerfiles.proximaPagina = true;
  }

  configuracionPerfil(
    perfil: PerfilModel
  ): ConfiguracionItemListaContactosCompartido {
    let usoCirculo: UsoItemCircular;
    let corazon: boolean = false;
    this.cargando = false;

    if (perfil.album[0].portada.principal.fileDefault) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
    }
    if (perfil.asociaciones.length > 0) {
      corazon = true;
    }

    return {
      id: perfil._id,
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: perfil.nombreContacto,
        nombre: perfil.nombre,
        contacto: false,
        estilosTextoSuperior: {
          color: ColorDelTexto.TEXTOAMARILLOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: ColorDelTexto.TEXTOBLANCO,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: perfil._id,
      },
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        perfil.album[0].portada.principal.url,
        ColorDeBorde.BORDER_ROJO,
        this.util.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        true
      ),
      mostrarX: (this.configCruzContacto = {
        mostrar: false,
        color: true,
      }),
      mostrarCorazon: corazon,
      configuracionLineaVerde: this.configurarLineaVerde(
        PaddingLineaVerdeContactos.PADDING_1542_267
      ),
      eventoCirculoNombre: (idPerfil?: string) => {
        this.obtenerPerfilBasico(idPerfil);
      },
    };
  }

  configurarGeneralFotoPerfilContactos(
    urlMedia: string,
    colorBorde: any,
    colorFondo: any,
    mostrarCorazon: boolean,
    usoItemCirculo: UsoItemCircular,
    activarClick: boolean
  ): ItemCircularCompartido {
    return {
      id: this.idPerfilBasico,
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: usoItemCirculo,
      esVisitante: this.esMiPerfil ? false : true,
      urlMedia: urlMedia,
      activarClick: activarClick,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: true,
      textoBoton: '',
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: colorBorde,
      colorDeFondo: colorFondo,
      mostrarCorazon: mostrarCorazon,
    };
  }

  configurarLineaVerde(
    paddingLineaVerdeContactos?: PaddingLineaVerdeContactos
  ): ConfiguracionLineaVerde {
    return {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false,
      paddingLineaVerde: paddingLineaVerdeContactos,
    };
  }
  async obtenerPerfilSeleccionado(idPerfil?) {
    this.configuracionesNoticias = [];

    this.jsonNegocio.obtenerPerfilGeneralDemo().subscribe(async (data) => {
      this.perfilSeleccionado = data[0];
      this.nombreUsuario = this.perfilSeleccionado.nombre;
      let indexFotoPerfil = this.archivosContactos.find((e) =>
        e.url.includes('c9e4d20f-71fe-4607-be54-eb8a9c1627a5.jpg')
      );
      this.perfilSeleccionado.album[0].portada.principal.url =
        indexFotoPerfil.url;
      let indexFotoPortada = this.archivosContactos.find((e) =>
        e.url.includes('ff51bfce-8ac3-4d11-ae53-07e9cc63d01b.jpg')
      );
      this.perfilSeleccionado.album[1].portada.principal.url =
        indexFotoPortada.url;
      let index1 = this.archivosContactos.find((e) =>
        e.url.includes('18ce5cbe-2e13-4c6d-b65a-af94676125a3.jpg')
      );
      this.perfilSeleccionado.asociaciones[0].participantes[0].perfil.album[0].portada.principal.url =
        index1.url;

      let index2 = this.archivosContactos.find((e) =>
        e.url.includes('e190b3fa-21fc-4eef-8675-561b4a051cdf.jpg')
      );
      this.perfilSeleccionado.asociaciones[1].participantes[0].perfil.album[0].portada.principal.url =
        index2.url;

      let index3 = this.archivosContactos.find((e) =>
        e.url.includes('8d20f732-5c12-4c63-9b30-f87e88a8ae38.jpg')
      );
      this.perfilSeleccionado.asociaciones[2].participantes[0].perfil.album[0].portada.principal.url =
        index3.url;

      let index4 = this.archivosContactos.find((e) =>
        e.url.includes('5c189e1c-35ba-4877-89a5-abe0a282c405.jpg')
      );
      this.perfilSeleccionado.asociaciones[3].participantes[0].perfil.album[0].portada.principal.url =
        index4.url;

      let noticias1 = this.archivosContactos.find((e) =>
        e.url.includes('3b10f5b7-1545-454e-84b8-99c41cdae115.jpg')
      );
      this.perfilSeleccionado.noticias[0].adjuntos[0].portada.principal.url =
        noticias1.url;

      let noticias2 = this.archivosContactos.find((e) =>
        e.url.includes('5c189e1c-35ba-4877-89a5-abe0a282c405.jpg')
      );
      this.perfilSeleccionado.noticias[1].adjuntos[0].portada.principal.url =
        noticias2.url;

      await this.iniciarComponentes();
      this.configuracionContactos();
    });
  }

  async configuracionContactos(esInvitacion?: boolean, contactos?) {
    // this.configContactos = [];

    this.dataListaContactos.lista = [];
    for (const contacto of this.perfilSeleccionado.asociaciones) {
      // this.configContactos.push(this.configContacto(contacto,esInvitacion));
      this.dataListaContactos.lista.push(
        this.configContacto(contacto.participantes[0], esInvitacion)
      );
    }
  }

  configContacto(contacto, esInvitacion: boolean): ItemCircularCompartido {
    let usoCirculo;
    let urlMedia: string;
    let idPerfil: string;
    const colorFondo = this.util.obtenerColorFondoAleatorio();
    const semilla = this.generadorId.generarIdConSemilla();
    if (contacto.invitadoPor) {
      urlMedia = contacto.invitadoPor.perfil.album[0].portada.principal.url;
      idPerfil = contacto.invitadoPor.perfil._id;
      if (contacto.invitadoPor?.perfil.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    } else if (contacto.perfil) {
      idPerfil = contacto.perfil._id;
      urlMedia = contacto.perfil.album[0].portada.principal.url;
      if (contacto.perfil.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    } else if (contacto.contactoDe) {
      idPerfil = contacto.contactoDe._id;
      urlMedia = contacto.contactoDe.album[0].portada.principal.url;
      if (contacto.contactoDe.album[0].portada.principal.fileDefault) {
        usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      } else {
        usoCirculo = UsoItemCircular.CIRCONTACTO;
      }
    }

    return {
      id: idPerfil,
      idInterno: semilla,
      usoDelItem: usoCirculo,
      esVisitante: this.esMiPerfil ? false : true,
      urlMedia: urlMedia,
      activarClick: true,
      activarDobleClick: this.esMiPerfil ? true : false,
      activarLongPress: false,
      mostrarBoton: false,
      mostrarLoader: true,
      textoBoton: '',
      eventoEnItem: () => {},
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: colorFondo,
      mostrarCorazon: false,
      abrirResumenPerfil: false,
    };
  }

  clickContacto(data: InfoAccionCirRec) {
    this.abrirModal = false;
    switch (data.accion) {
      case AccionesItemCircularRectangular.VER_RESUMEN_CONTACTOS:
        // cuando hay mas de 4 contactos se muestra el detalle de lista si hay menos solo el resumen
        if (this.configContactoCircular.contactos.length > 4) {
          this.configContactoCircular.contactos = [];
          this.listaContactos.dataListaContactos.lista = [];
          this.configContactoCircular.resumen = false;
          // ocultar noticias y proyectos
          // pasar nuevos estilos a la lista
          // consultar api paginacion de contactos
          this.paginacionContactos = {
            paginaActual: 0,
            proximaPagina: true,
          };
          this.obtenerContactosPerfil();
        }

        break;
      case AccionesItemCircularRectangular.ABRIR_MODAL_RESUMEN_PERFIL:
        // abrir modal
        this.listaContactos.obtenerPerfilBasico(data.informacion.id);
        // this.obtenerPerfilBasico(data.informacion.id);

        break;
      case AccionesItemCircularRectangular.CANCELAR_INVITACION:
        let contactoPos =
          this.listaContactos.dataListaContactos.lista.findIndex(
            (contacto) => contacto.id === data.informacion.id
          );

        if (contactoPos >= 0) {
          let urlMedia =
            this.listaContactos.dataListaContactos.lista[contactoPos].urlMedia;
          this.listaContactos.dataListaContactos.lista[contactoPos].urlMedia =
            '';
          this.listaContactos.dataListaContactos.lista[
            contactoPos
          ].mostrarLoader = true;
          this.cancelarInvitacionDobleClic(
            data.informacion.id,
            contactoPos,
            urlMedia
          );
        }

        // abrir modal

        // this.obtenerPerfilBasico(data.informacion.id);

        break;
      default:
        break;
    }
  }

  configurarResumenPerfil() {
    this.dataListaPensamientos = {
      tamanoLista: TamanoLista.LISTA_PENSAMIENTO_RESUMEN_MODAL_PERFIL,
      lista: [],
    };
    this.configResumenPerfil = {
      titulo: '',
      alturaModal: AlturaResumenPerfil.VISTA_BUSCAR_CONTACTOS_100,
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        '',
        ColorDeBorde.BORDER_AMARILLO,
        ColorDeFondo.FONDO_BLANCO,
        false,
        UsoItemCircular.CIRCONTACTO,
        false
      ),
      configuracionPensamiento: {
        tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
        esLista: true,
        subtitulo: true,
        configuracionItem: {
          estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
        },
      },
      confDataListaPensamientos: this.dataListaPensamientos,
      botones: [
        {
          text: 'm3v5texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.AMARRILLO,
          ejecutar: () => {
            // this.configuracionesNoticias = [];
            if (this.idPerfilBasico) {
              // this.router.navigateByUrl(
              //   RutasLocales.MODULO_PERFIL + '/' +
              //   RutasPerfil.VER.toString().replace(':id', this.idPerfilBasico));
              // this.obtenerPerfilSeleccionado(this.idPerfilBasico);
              this.cerrarResumenPerfil();
              // this.ngOnInit();
            }
          },
          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,
        },
        {
          text: 'm3v5texto4',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.ROJO,
          ejecutar: () => {
            this.cerrarResumenPerfil();
          },
          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,
        },
      ],
      reintentar: (idPerfil) => {
        this.obtenerPerfilBasico(idPerfil);
      },
      mostrar: false,
      error: false,
      loader: false,
    };
  }
  cerrarResumenPerfil() {
    this.configResumenPerfil.mostrar = false;
  }
  obtenerPerfilBasico(idPerfil: string) {
    this.idPerfilBasico = idPerfil;
    this.cargando = false;
    this.configResumenPerfil.loader = true;
    this.configResumenPerfil.mostrar = true;

    this.configResumenPerfil.confDataListaPensamientos.lista = [];
    this.perfilNegocio.obtenerPerfilBasico(idPerfil).subscribe(
      (resumenPerfil: PerfilModel) => {
        if (resumenPerfil === null) {
        } else {
          this.configResumenPerfil.configCirculoFoto.urlMedia =
            resumenPerfil.album[0].portada.principal.url;
          this.configResumenPerfil.configuracionPensamiento.subtitulo = true;
          // let titulo = resumenPerfil.nombre + '(' + resumenPerfil.nombreContacto + ')'
          this.configResumenPerfil.titulo = resumenPerfil.nombreContacto;

          if (resumenPerfil.album[0].portada.principal.fileDefault) {
            this.configResumenPerfil.configCirculoFoto.usoDelItem =
              UsoItemCircular.CIRCARITACONTACTODEFECTO;
          } else {
            this.configResumenPerfil.configCirculoFoto.usoDelItem =
              UsoItemCircular.CIRCONTACTO;
          }
          if (resumenPerfil.pensamientos) {
            for (const iterator of resumenPerfil.pensamientos) {
              this.configResumenPerfil.confDataListaPensamientos.lista.push(
                iterator
              );
            }
          }

          this.configResumenPerfil.loader = false;
        }
      },
      (error) => {
        this.prepararAppBar();
        this.configResumenPerfil.error = true;
        this.cargando = false;
      }
    );
  }

  // CAPA NEGOCIO
  obtenerContactosPerfil() {
    let tipoParticipante = this.esMiPerfil
      ? CodigoEstadoParticipanteAsociacion.INVITADO
      : CodigoEstadoParticipanteAsociacion.CONTACTO;

    this.configContactoCircular.cargando = true;
    if (this.paginacionContactos.proximaPagina) {
      this.paginacionContactos.paginaActual++;
    }

    if (this.configContactoCircular.contactos.length === 0) {
      this.configContactoCircular.cargando = true;
    }

    if (this.paginacionContactos.proximaPagina) {
      this.error = '';
      this.participanteAsoNegocio
        .obtenerParticipanteAsoTipo(
          this.perfilSeleccionado._id,
          this.numeroContactos,
          this.paginacionContactos.paginaActual,
          tipoParticipante,
          FiltroGeneral.ALFA
        )
        .subscribe(
          (res: PaginacionModel<ParticipanteAsociacionModel>) => {
            this.paginacionContactos.proximaPagina = res.proximaPagina;
            if (res) {
              for (const contacto of res.lista) {
                this.configContactoCircular.contactos.push(contacto);
              }

              this.listaContactos.configuracionContactos(true);
            }
            this.configContactoCircular.cargando = false;
          },
          (error) => {
            this.prepararAppBar();
            this.error = error;
            this.configContactoCircular.cargando = false;
          }
        );
    } else {
      this.configContactoCircular.cargando = false;
    }
  }

  obtenerArchivosPorDefecto() {
    this.mediaNegocio.obtenerListaArchivosDefault().subscribe((archivos) => {
      for (const archivo of archivos) {
        if (
          archivo.catalogoArchivoDefault ===
          CodigosCatalogoArchivosPorDefecto.PROYECTOS
        ) {
          this.archivosPorDefecto.push(archivo);
        }
      }

      this.archivoPorDefectoNoticia = this.util.randomItem(
        this.archivosPorDefecto
      );
      const archivoDefectoProyecto = this.util.randomItem(
        this.archivosPorDefecto
      );

      if (this.placeholderNoticia) {
        this.placeholderNoticia.loader = true;
        this.placeholderNoticia.urlMedia = this.archivoPorDefectoNoticia.url;
      }

      if (this.placeholderProyecto) {
        this.placeholderProyecto.loader = true;
        this.placeholderProyecto.urlMedia = archivoDefectoProyecto.url;
      }

      (error) => {
        if (this.placeholderNoticia) {
          this.placeholderNoticia.loader = false;
        }

        if (this.placeholderProyecto) {
          this.placeholderProyecto.loader = false;
        }
      };
    });
  }

  // Configurar album perfil
  async configurarAlbumPerfil() {
    const album: AlbumEntity =
      this.albumNegocio.obtenerAlbumDelPerfilEntitySegunTipo(
        CodigosCatalogoTipoAlbum.PERFIL,
        this.perfilSeleccionado
      );

    const infoPortada =
      this.albumNegocio.definirDataItemCircularSegunPortadaAlbum(album);

    let usoCirculo;
    if (this.perfilSeleccionado.album[0].portada.principal.fileDefault) {
      usoCirculo = UsoItemCircular.CIRCARITAPERFIL;
      infoPortada.mostrarLoader = false;
    } else {
      usoCirculo = UsoItemCircular.CIRPERFIL;
    }

    this.confItemCir = {
      id: this.perfilSeleccionado._id,
      idInterno: this.generadorId.generarIdConSemilla(),

      usoDelItem: usoCirculo,
      esVisitante: this.esMiPerfil ? false : true,
      urlMedia: infoPortada.urlMedia,
      activarClick: true,
      activarDobleClick: this.esMiPerfil ? false : true,
      activarLongPress: false,
      mostrarBoton: infoPortada.mostrarBoton,
      mostrarLoader: infoPortada.mostrarLoader,
      textoBoton: '',
      eventoEnItem: (data: InfoAccionCirRec) => {
        this.eventoEnItemPerfil(data);
      },
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
    };

    if (this.albumPerfil) {
      this.albumPerfil.configuracion = this.confItemCir;
    }
    this.confItemCir.colorDeFondo =
      await this.util.obtenerColorFondoAleatorio();

    const texto = await this.translateService.get('subirFotos').toPromise();
    this.confItemCir.textoBoton = texto;
  }

  eventoEnItemPerfil(item: InfoAccionCirRec) {
    switch (item.accion) {
      case AccionesItemCircularRectangular.ACTUALIZAR_PERFIL:
        this.actualizarMiPerfil(item);
        break;
      case AccionesItemCircularRectangular.EXPANDIR_FOTO_DEL_ITEM:
        this.expandirFoto(item);
        break;
      case AccionesItemCircularRectangular.VISITAR_ALBUM_GENERAL:
        this.visitarAlbum(item);
        break;
      default:
        break;
    }
  }

  visitarAlbum(item: InfoAccionCirRec) {}
  expandirFoto(item) {}

  actualizarMiPerfil(item) {}

  // Configurar portada
  configurarPortada() {
    this.configuracionPortada = {
      tamano: TamanoPortadaGaze.PORTADACORTADA,
    };
    // this.portada.configuracionPortada = configuracion;
  }

  // Configurar album generarl
  async configurarAlbumGeneral() {
    const album: AlbumEntity =
      this.albumNegocio.obtenerAlbumDelPerfilEntitySegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        this.perfilSeleccionado
      );

    const infoPortada =
      this.albumNegocio.definirDataItemCircularSegunPortadaAlbum(album);

    this.confItemRec = {
      id: this.perfilSeleccionado._id,
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemRectangular.RECPERFIL,
      esVisitante: this.esMiPerfil ? false : true,
      urlMedia: infoPortada.urlMedia,
      activarClick: true,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: infoPortada.mostrarBoton,
      mostrarLoader: infoPortada.mostrarLoader,
      textoBoton: '',
      descripcion: '',
      mostrarIconoExpandirFoto: false,
      textoCerrarEditarDescripcion: '',
      mostrarCapaImagenSeleccionadaConBorde: false,

      // colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      // eventoEnItem: (data: InfoAccionCirRec) => {
      //   this.eventoEnItem(data)
      // },
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
    };
    const texto = await this.translateService.get('subirFotos').toPromise();
    this.confItemRec.textoBoton = texto;

    if (this.albumGeneral) {
      this.albumGeneral.configuracion = this.confItemRec;
    }
  }

  configurarTituloNocontactos() {
    this.tituloNoContactos = {
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
      textoBoton1: 'm3v2texto6',
      textoBoton2: 'm3v2texto7',
    };
  }

  configurarPlaceholders() {
    this.placeholderNoticia = {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: 'm3v1texto12',
          textoBoton2: 'm3v1texto13',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: { fecha: new Date(), formato: 'dd/MM/yyyy' },
      },
      loader: true,
      urlMedia: '',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {
          // this.click = h;
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
    };

    this.placeholderProyecto = {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: 'm3v1texto15',
          textoBoton2: 'm3v1texto16',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: { fecha: new Date(), formato: 'dd/MM/yyyy' },
      },
      loader: true,
      urlMedia: '',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {
          // this.click = h;
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      actualizado: false,
    };
  }

  reintentar() {
    // switch (this.accionEntidad) {
    //   case AccionEntidad.REGISTRO: break
    //   case AccionEntidad.CREAR:
    //     break
    //   case AccionEntidad.ACTUALIZAR:
    //     this.inicializarDataUsuario()
    //     break
    //   default: break;
    // }
  }

  async cargarPensamientos() {
    let pensa = {
      estado: null,
      fechaActualizacion: '2021-05-03T18:15:06.151Z',
      id: '5f8a1b77595be94a493d4154',
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v7texto11').toPromise(),
    };
    this.dataLista.lista.push(pensa);

    let pensa2 = {
      estado: null,
      fechaActualizacion: '2021-05-03T18:15:06.151Z',
      id: '5f8a1b77595be94a493d4154',
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v7texto13').toPromise(),
    };
    this.dataLista.lista.push(pensa2);

    let pensa3 = {
      estado: null,
      fechaActualizacion: '2021-05-03T18:15:06.151Z',
      id: '5f8a1b77595be94a493d4154',
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v7texto15').toPromise(),
    };
    this.dataLista.lista.push(pensa3);
  }

  cargarfotosPrueba() {
    this.jsonDemoServiceLocal.obtenerFotosPrueba().subscribe((data) => {
      this.fotosPrueba = data;
    });
  }

  seleccionarPensamientoMostrar(data: number) {
    switch (data) {
      case 0:
        this.cargando = false;
        this.pensamientoCompartido = {
          tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR,
          configuracionItem: {
            estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
          },
          subtitulo: false,
        };
        this.dataListaPublico = {
          tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
          lista: [],
          cargarMas: () => {},
        };
        this.dataListaPrivado = {
          tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
          lista: [],
          cargarMas: () => {},
        };
        break;
      case 1:
        this.esPublico = true;
        this.pensamientoCompartido = {
          esLista: true,
          tipoPensamiento: TipoPensamiento.PENSAMIENTO_PUBLICO_CREACION,
          configuracionItem: {
            estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
            presentarX: true,
          },
          subtitulo: true,
        };
        this.cargarPensamientos();
        if (this.pensamientos) {
          this.pensamientos.divPensamiento = 'divPensamientoFormaAmarilla';
        }
        break;
      case 2:
        this.esPublico = false;
        this.pensamientoCompartido = {
          esLista: true,
          tipoPensamiento: TipoPensamiento.PENSAMIENTO_PRIVADO_CREACION,
          configuracionItem: {
            estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
            presentarX: true,
          },
          subtitulo: true,
        };
        this.cargarPensamientos();
        break;
      default:
        this.cargando = false;
        this.pensamientoCompartido = {
          tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR,
          configuracionItem: {
            estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
          },
          subtitulo: false,
        };
        break;
    }
  }

  async configurarPensamientos() {
    this.dataLista = {
      tamanoLista: TamanoLista.LISTA_PENSAMIENTO_PERFIL,
      lista: [],
      cargarMas: () => {},
    };

    this.pensamientoCompartido = {
      tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
      tituloPensamiento: '', //await this.internacionalizacionNegocio.obtenerTextoLlave('reflexion'),
      esLista: true,
      configuracionItem: { estilo: EstiloItemPensamiento.ITEM_ALEATORIO },
    };

    // this.pensamientos.dataLista = this.dataLista;
    // this.pensamientos.pensamientoCompartido = this.pensamientoCompartido;

    if (this.pensamientos) {
      this.pensamientos.dataLista = this.dataLista;
      this.pensamientos.pensamientoCompartido = this.pensamientoCompartido;
      this.paginacionPensamiento.paginaActual = 0;
      this.paginacionPensamiento.proximaPagina = true;
    }
    this.cargarPensamientos();
  }

  cargarMasPensamientos() {
    if (this.paginacionPensamiento.proximaPagina) {
      this.paginacionPensamiento.paginaActual++;
    }
    if (this.paginacionPensamiento.proximaPagina) {
      this.cargando = false;
      let listaMomentanea = [];
    }
  }
  buscarPerfiles(palabra: string, pagina?: number) {
    if (this.paginacionBusquedaPerfiles.proximaPagina) {
      this.paginacionBusquedaPerfiles.paginaActual++;
    }

    if (pagina) {
      this.paginacionBusquedaPerfiles.paginaActual = pagina;
    }

    if (this.paginacionBusquedaPerfiles.proximaPagina) {
      // this.configuracionAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = true
      this.perfilNegocio
        .buscarPerfiles(
          palabra,
          this.miPerfil._id,
          this.paginacionBusquedaPerfiles.paginaActual
        )
        .subscribe(
          (res: PaginacionModel<ParticipanteAsociacionModel>) => {
            this.paginacionBusquedaPerfiles.proximaPagina = res.proximaPagina;
            for (const iterator of res.lista) {
              // this.configuracionAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.lista.push(this.configuracionPerfil(iterator))
              // this.configuracionAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
              // this.appBar.buscador.mostrarProgreso(false);
            }
            // this.appBar.buscador.mostrarProgreso(false);
          },
          (error) => {
            // this.appBar.buscador.mostrarProgreso(false);
            // this.configuracionAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
            // this.appBar.buscador.mostrarProgreso(false);
          }
        );
    }
  }

  configuracionBotonContactos() {
    this.configuracionBotonContatos = {
      colorTexto: ColorTextoBoton.VERDE,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      text: 'm3v2texto5',
      ejecutar: () => {},
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };
  }

  configuracionBotonesInvitacion() {
    if (
      !this.perfilSeleccionado?.participanteAsociacion ||
      this.perfilSeleccionado?.participanteAsociacion.estado.codigo ===
        CodigoEstadoParticipanteAsociacion.RECHAZADA ||
      this.perfilSeleccionado?.participanteAsociacion.estado.codigo ===
        CodigoEstadoParticipanteAsociacion.CANCELADA ||
      this.perfilSeleccionado?.participanteAsociacion.estado.codigo ===
        CodigoEstadoParticipanteAsociacion.ELIMINADO
    ) {
      if (this.perfilSeleccionado._id != this.miPerfil._id) {
        this.botonesInvitacion = this.crearBotonesInvitacion(
          CodigoEstadoParticipanteAsociacion.INVITANTE
        );
      }
    } else if (
      this.perfilSeleccionado?.participanteAsociacion.estado.codigo ===
      CodigoEstadoParticipanteAsociacion.INVITADO
    ) {
      if (this.perfilSeleccionado._id != this.miPerfil._id) {
        this.botonesInvitacion = this.crearBotonesInvitacion(
          CodigoEstadoParticipanteAsociacion.CONTACTO
        );
      }
    } else if (
      this.perfilSeleccionado?.participanteAsociacion.estado.codigo ===
      CodigoEstadoParticipanteAsociacion.INVITANTE
    ) {
      this.botonesInvitacion = this.crearBotonesInvitacion(
        CodigoEstadoParticipanteAsociacion.CANCELADA
      );
    }
  }

  cancelarInvitacionDobleClic(idPerfil, contactoPos, urlMedia) {
    let perfilSeleccionado: PerfilEntity;

    this.perfilNegocio
      .obtenerPerfilGeneral(idPerfil, this.miPerfil._id)
      .subscribe(
        (data) => {
          perfilSeleccionado = data;
          this.actualizarEstadoAsociacion(
            CodigoEstadoParticipanteAsociacion.CANCELADA,
            perfilSeleccionado
          );
          this.configContactoCircular.contactos = this.eliminarcontactoArray(
            this.configContactoCircular.contactos,
            perfilSeleccionado._id
          );
          this.listaContactos.configuracionContactos(false);
        },
        (error) => {
          this.listaContactos.dataListaContactos.lista[contactoPos].urlMedia =
            urlMedia;
          this.listaContactos.dataListaContactos.lista[
            contactoPos
          ].mostrarLoader = false;

          this.cambiarValorCapasDelCuerpo(
            false,
            true,
            error.mensaje || 'text37'
          );
        }
      );
  }

  eliminarcontactoArray(arr, value) {
    return arr.filter((ele) => {
      return ele.perfil._id != value;
    });
  }

  actualizarEstadoAsociacion(estado, perfil?) {
    let asociacionId: string;
    let idPerfilSeleccionado: string;

    if (perfil) {
      asociacionId = perfil.participanteAsociacion.asociacion._id;
      idPerfilSeleccionado = perfil._id;
    } else {
      asociacionId =
        this.perfilSeleccionado.participanteAsociacion.asociacion._id;
      idPerfilSeleccionado = this.perfilSeleccionado._id;
    }

    this.participanteAsoNegocio
      .cambiarEstadoAsociacion(asociacionId, estado, idPerfilSeleccionado)
      .subscribe(
        (res: string) => {
          if (
            estado === CodigoEstadoParticipanteAsociacion.CANCELADA ||
            estado === CodigoEstadoParticipanteAsociacion.RECHAZADA
          ) {
            this.botonesInvitacion = this.crearBotonesInvitacion(
              CodigoEstadoParticipanteAsociacion.INVITANTE
            );
            // pop contacto del array

            this.configContactoCircular.contactos = this.eliminarcontactoArray(
              this.configContactoCircular.contactos,
              idPerfilSeleccionado
            );
          } else if (CodigoEstadoParticipanteAsociacion.CONTACTO) {
            this.botonesInvitacion = [];
          }
        },
        (error) => {
          this.error = error;
        }
      );
  }

  crearAsociacion() {
    const participantes = [
      { perfil: { _id: this.miPerfil._id } },
      { perfil: { _id: this.perfilSeleccionado._id } },
    ];

    this.participanteAsoNegocio
      .crearAsociacion(
        'Nuevo contacto',
        TipoParticipanteAsociacion.CONTACTO,
        participantes
      )
      .subscribe(
        (data) => {
          this.botonesInvitacion = this.crearBotonesInvitacion(
            CodigoEstadoParticipanteAsociacion.CANCELADA
          );
          // Cambiar botones
        },
        (error) => {
          this.error = error;
        }
      );
  }

  cabiarPerfil() {}

  // ============ FUNCIONES PROYECTOS ===========

  async configurarProyectos(proyectos?) {
    let proyecto1 = this.archivosContactos.find((e) =>
      e.url.includes('ff85b67e-32f9-4356-abe6-a0e29e361584.jpg')
    );

    this.configuracionesProyectos.push({
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: await this.translateService
            .get('m1v7texto8')
            .toPromise(),
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date('2021/04/18'),
          formato: 'dd/MM/yyyy',
        },
      },
      loader: true,
      urlMedia: proyecto1.url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (item: string) => {
          // this.verDetalleProyectos(item);
          // this.click = h;
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '90900990909009',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      actualizado: false,
      resumen: this.mostrarListaProyectos,
    });

    this.configuracionListaProyectos();
  }

  verDetalleProyectos(proyecto?) {
    if (
      this.configuracionesProyectos.length > 2 &&
      !this.mostrarListaProyectos
    ) {
      this.mostrarListaProyectos = true;
      this.configuracionesProyectos = [];
      this.obtenerProyectosPerfil();
    } else if (
      this.configuracionesProyectos.length > 2 &&
      this.mostrarListaProyectos
    ) {
    } else {
      this.mostrarListaProyectos = false;
    }
  }

  configuracionListaProyectos() {
    let subtitulo: string;

    // if (this.configuracionesNoticias.length > 0 && this.mostrarListaNoticias) {
    //   subtitulo = 'CLICK TO OPEN';
    // }

    // if (this.esMiPerfil && this.configuracionesNoticias.length > 0 && !this.mostrarListaNoticias) {
    //   subtitulo = 'CLICK TO UPDATE';
    // }

    this.configProyectos = {
      titulo: this.esMiPerfil
        ? 'm3v2texto11'
        : this.nombreUsuario + ' texto834',
      subtitulo: subtitulo,
      resumen: !this.mostrarListaProyectos,
      esMiPerfil: this.esMiPerfil,
      perfilSeleccionado: this.perfilSeleccionado,
      proyectos: this.configuracionesProyectos,
      // cargando: true,
      ultimaPagina: this.paginacionProyectos.proximaPagina,
      evento: (data: any) => {
        // this.mostrarNoticias = data;
        // this.clickContacto(data);
      },
      eventoScroll: () => {
        // this.obtenerContactosPerfil();
        this.obtenerProyectosPerfil();
      },
    };
  }

  obtenerProyectosPerfil() {
    this.configProyectos.cargando = false;
    this.configProyectos.ultimaPagina = this.paginacionProyectos.proximaPagina;

    // if (this.listaNoticias.lista.length === 0) {
    //   this.listaNoticias.cargando = true;
    // }

    if (this.paginacionProyectos.proximaPagina && this.mostrarListaProyectos) {
      this.paginacionProyectos.paginaActual++;
      this.configProyectos.cargando = true;
      this.error = '';
      // this.configNoticias.ultimaPagina = false;

      this.proyectoNegocio
        .obtenerProyectosPerfil(
          this.perfilSeleccionado._id,
          4,
          this.paginacionProyectos.paginaActual
        )
        .subscribe(
          async (data: PaginacionModel<NoticiaEntity>) => {
            this.paginacionProyectos.proximaPagina = data.proximaPagina;
            this.configProyectos.cargando = false;

            if (data) {
              let proyectos = [];
              for (const proyecto of data.lista) {
                proyectos.push(proyecto);
              }
              // configurar noticias
              await this.configurarProyectos(proyectos);
            }
          },
          (error) => {
            this.error = error;
            this.configProyectos.cargando = false;
          }
        );
    }
  }

  // ========================= FUNCIONES NOTICIAS =========================

  async configurarNoticias(noticias?) {
    let defauultNoti = this.archivosLocalStorage.filter(
      (element) =>
        element.catalogoArchivoDefault ===
        CodigosCatalogoArchivosPorDefecto.PROYECTOS
    );

    let noticias1 = this.archivosContactos.find((e) =>
      e.url.includes('3b10f5b7-1545-454e-84b8-99c41cdae115.jpg')
    );
    this.perfilSeleccionado.noticias[0].adjuntos[0].portada.principal.url =
      noticias1.url;

    this.configuracionesNoticias.push({
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: await this.translateService
            .get('m1v7texto4')
            .toPromise(),
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date('2021/01/20'),
          formato: 'dd/MM/yyyy',
        },
      },
      loader: true,
      urlMedia: noticias1.url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (item: string) => {},
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '9898989898923332',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      tipo: 'CATALB_1',
      actualizado: false,
    });

    this.configuracionesNoticias.push({
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: await this.translateService
            .get('m1v7texto6')
            .toPromise(),
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date('2021/02/01'),
          formato: 'dd/MM/yyyy',
        },
      },
      loader: true,
      urlMedia: defauultNoti[5].url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (item: string) => {},
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '9898989898923332',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      tipo: 'CATALB_1',
      actualizado: false,
    });

    this.configuracionListaNoticias();
  }

  verDetalleNoticia(noticia?) {
    if (this.configuracionesNoticias.length > 2 && !this.mostrarListaNoticias) {
      this.mostrarListaNoticias = true;

      this.configuracionesNoticias = [];
      this.obtenerNoticiasPerfil();
    } else if (
      this.configuracionesNoticias.length > 2 &&
      this.mostrarListaNoticias
    ) {
    } else {
      this.mostrarListaNoticias = false;
    }
  }

  configuracionListaNoticias() {
    let subtitulo: string;

    if (this.configuracionesNoticias.length > 0 && this.mostrarListaNoticias) {
      subtitulo = 'texto493';
    }

    if (
      this.esMiPerfil &&
      this.configuracionesNoticias.length > 0 &&
      !this.mostrarListaNoticias
    ) {
      subtitulo = 'm3v2texto10';
    }

    this.configNoticias = {
      titulo: this.esMiPerfil ? 'm3v2texto9' : this.nombreUsuario + 'texto614',
      subtitulo: subtitulo,
      resumen: !this.mostrarListaNoticias,
      esMiPerfil: this.esMiPerfil,
      perfilSeleccionado: this.perfilSeleccionado,
      noticias: this.configuracionesNoticias,
      // cargando: true,
      ultimaPagina: this.paginacionNoticias.proximaPagina,
      evento: (data: any) => {},
      eventoScroll: () => {
        // this.obtenerContactosPerfil();
        this.obtenerNoticiasPerfil();
      },
    };
  }

  obtenerNoticiasPerfil() {
    this.configNoticias.cargando = false;
    this.configNoticias.ultimaPagina = this.paginacionNoticias.proximaPagina;

    // if (this.listaNoticias.lista.length === 0) {
    //   this.listaNoticias.cargando = true;
    // }

    if (this.paginacionNoticias.proximaPagina && this.mostrarListaNoticias) {
      this.paginacionNoticias.paginaActual++;
      this.configNoticias.cargando = true;
      this.error = '';
      // this.configNoticias.ultimaPagina = false;

      this.noticiaNegocio
        .obtenerNoticiasPerfil(
          this.perfilSeleccionado._id,
          6,
          this.paginacionNoticias.paginaActual
        )
        .subscribe(
          async (data: PaginacionModel<NoticiaEntity>) => {
            this.paginacionNoticias.proximaPagina = data.proximaPagina;
            this.configNoticias.cargando = false;

            if (data) {
              let noticias = [];
              for (const noticia of data.lista) {
                noticias.push(noticia);
              }
              // configurar noticias
              await this.configurarNoticias(noticias);
            }
          },
          (error) => {
            this.error = error;
            this.configNoticias.cargando = false;
          }
        );
    }
  }

  cargarMasNoticias() {
    let cargarMas = false;
    let element = document.getElementById('scroll');

    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
      // if (!this.configNoticias.cargando) {
      this.obtenerNoticiasPerfil();
      // }
    }
  }

  // controlar navegacion
  controlNavigation() {
    let currentUrl = this.router.url;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
      }

      if (
        event instanceof NavigationEnd &&
        currentUrl.includes('verperfil') &&
        this.esAccionAtras
      ) {
        window.location.reload();
      }

      if (event instanceof NavigationError) {
      }
    });
  }

  // Botones de invitacion
  crearBotonesInvitacion(boton) {
    let botonInvitacion;
    switch (boton) {
      case CodigoEstadoParticipanteAsociacion.INVITANTE:
        botonInvitacion = [
          {
            text: 'm3v6texto4',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.AMARRILLO,
            ejecutar: () => {
              // ENVIAR INVITACION
              this.crearAsociacion();
            },
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
        ];

        break;
      case CodigoEstadoParticipanteAsociacion.CANCELADA:
        botonInvitacion = [
          {
            text: 'm3v6texto5',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.ROJO,
            ejecutar: () => {
              this.actualizarEstadoAsociacion(
                CodigoEstadoParticipanteAsociacion.CANCELADA
              );
            },
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
        ];

        break;

      case CodigoEstadoParticipanteAsociacion.CONTACTO:
        botonInvitacion = [
          {
            text: 'm3v3texto13',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.AMARRILLO,
            ejecutar: () => {
              // this.idAsociacion = idAsociacion
              this.actualizarEstadoAsociacion(
                CodigoEstadoParticipanteAsociacion.CONTACTO
              );
            },
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
          {
            text: 'm3v3texto14',
            tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
            colorTexto: ColorTextoBoton.ROJO,
            ejecutar: () => {
              this.actualizarEstadoAsociacion(
                CodigoEstadoParticipanteAsociacion.RECHAZADA
              );
            },
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
          },
        ];

        break;

      default:
        break;
    }

    return botonInvitacion;
  }

  tapPerfil() {}

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}
