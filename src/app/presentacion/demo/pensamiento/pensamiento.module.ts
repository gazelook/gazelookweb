import { CompartidoModule } from './../../../compartido/compartido.module';
import { PensamientoDemoComponent } from './pensamiento.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { PensamientoRoutingModule } from "./pensamiento-routing.module";
import { CrearPensamientoComponent } from "./crear-pensamiento/crear-pensamiento.component";

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PensamientoDemoComponent,
    CrearPensamientoComponent,
  ],
  imports: [
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    CompartidoModule,
    PensamientoRoutingModule,

  ],
  exports:[
    TranslateModule
  ],
  providers:[
  ]
})
export class PensamientoModule { }
