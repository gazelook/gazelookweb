import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ColorDeFondo, ColorDelTexto, EstilosDelTexto, TipoIconoBarraInferior } from '@env/src/app/compartido/diseno/enums';
import { ConfiguracionBarraInferiorInline } from '@env/src/app/compartido/diseno/modelos';
import { EnrutadorService } from '@env/src/app/nucleo/servicios/generales';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton } from 'src/app/compartido/componentes/button/button.component';
import { PensamientoCompartidoComponent } from 'src/app/compartido/componentes/pensamiento/pensamiento-compartido.component';
import { ToastComponent } from 'src/app/compartido/componentes/toast/toast.component';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { TipoInput } from 'src/app/compartido/diseno/enums/tipo-input.enum';
import { EstiloItemPensamiento, TipoPensamiento } from 'src/app/compartido/diseno/enums/tipo-pensamiento.enum';
import { UsoAppBar } from 'src/app/compartido/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from 'src/app/compartido/diseno/modelos/appbar.interface';
import { BarraInferior } from 'src/app/compartido/diseno/modelos/barra-inferior.interfce';
import { BotonCompartido } from 'src/app/compartido/diseno/modelos/boton.interface';
import { DatosLista } from 'src/app/compartido/diseno/modelos/datos-lista.interface';
import { ItemPensamiento, PensamientoCompartido } from 'src/app/compartido/diseno/modelos/pensamiento';
import { ConfiguracionToast } from 'src/app/compartido/diseno/modelos/toast.interface';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { Enrutador, InformacionEnrutador } from '../../enrutador/enrutador.component';
import { JsonDemoNegocio } from './../../../dominio/logica-negocio/json-demo.negocio';

@Component({
  selector: 'app-pensamiento',
  templateUrl: './pensamiento.component.html',
  styleUrls: ['./pensamiento.component.scss']
})
export class PensamientoDemoComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent
  @ViewChild('pensamientoCompartidoComponent', { static: false }) pensamientoCompartidoComponent: PensamientoCompartidoComponent

  configuracionAppBar: ConfiguracionAppbarCompartida //Para enviar la configuracion de para presentar el appBar
  botonPublico: BotonCompartido //Para enviar la configuracion del boton publico
  botonPrivado: BotonCompartido //Para enviar la configuracion del boton privado
  barraInferior: BarraInferior //Barra inferior
  cargando: boolean; //PRESENTAR EL CARGANDO
  pensamientoCompartido: PensamientoCompartido
  dataListaPublico: DatosLista //lISTA DE
  dataListaPrivado: DatosLista //lISTA DE
  botonCrearPensamiento: BotonCompartido;
  esPublico: boolean; //Para enviar a la base de datos true o false de acuerdo a lo seleccionado por el usuario
  ejecutarMetodo: true //PARA SABER CUAL DE LOS METODOS SE VAN A EJECUTAR
  configuracionToast: ConfiguracionToast //Presentar el toats
  perfilSeleccionado: PerfilModel
  mostrarTitulosPublico: boolean
  mostrarTitulosPrivado: boolean

  public confBarraInferior: ConfiguracionBarraInferiorInline
  public informacionEnrutador: InformacionEnrutador

  constructor(
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private jsonDemoNegocio: JsonDemoNegocio,
    private perfilNegocio: PerfilNegocio,
    private _location: Location,
    private translateService: TranslateService,
    private enrutadorService: EnrutadorService
  ) {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    this.esPublico = true
    this.prepararAppBar()
    this.mostrarTitulosPublico = false
    this.mostrarTitulosPrivado = false
  }

  ngOnInit(): void {
    this.cargando = true
    this.iniciarDatos()
    this.cambiarEstado(1)
    this.configurarBarraInferiorInline()
  }

  prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      tituloAppbar: {
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        tituloPrincipal: {
          mostrar: true,
          llaveTexto: 'texto1126'
        },
        mostrarLineaVerde: true,
        mostrarDivBack: true
      },
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v9texto2'
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      }
    }
  }


  volverAtras() {

    this._location.back()
  }


  iniciarDatos() {
    this.guardarPensamientos()
    this.enviarPensamienroCrear(false)
    this.seleccionarPensamientoMostrar(0)
    this.botonPublico = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m6v3texto4'),
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: (param) => { this.cambiarEstado(1) }
    }

    this.botonPrivado = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m6v3texto5'),
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: (param) => this.cambiarEstado(2)
    }
    this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: false, mostrarToast: false, texto: "" }
    //this.configuracionToast = {cerrarClickOutside:false,mostrarLoader:false,mostrarToast:false,texto:""}
  }
  async guardarPensamientos() {
    let pensa = {
      estado: null,
      fechaActualizacion: "2021-02-28T18:15:06.151Z",
      id: "5f8a1b77595be94a493d4154",
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v8texto4').toPromise(),
    }
    this.dataListaPublico.lista.push(pensa)

    let pensa2 = {
      estado: null,
      fechaActualizacion: "2021-04-10T18:15:06.151Z",
      id: "5f8a1b77595be94a493d4154",
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v8texto6').toPromise(),
    }
    this.dataListaPublico.lista.push(pensa2)

    let pensa3 = {
      estado: null,
      fechaActualizacion: "2021-05-30T18:15:06.151Z",
      id: "5f8a1b77595be94a493d4154",
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v8texto8').toPromise(),
    }
    this.dataListaPublico.lista.push(pensa3)

    let pensa4 = {
      estado: null,
      fechaActualizacion: "2021-09-13T18:15:06.151Z",
      id: "5f8a1b77595be94a493d4154",
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v8texto10').toPromise(),
    }

    this.dataListaPublico.lista.push(pensa4)

    let pensa5 = {
      estado: null,
      fechaActualizacion: "2021-07-5T18:15:06.151Z",
      id: "5f8a1b77595be94a493d4154",
      perfil: null,
      publico: null,
      texto: await this.translateService.get('m1v8texto10').toPromise(),
    }

    this.dataListaPrivado.lista.push(pensa4)


  }
  //Cuando se le presenta la pagina principal y tiene que seleccionar un tipo de pensamiento(PensamientoCompartido)
  cambiarEstado(param: number) {

    this.barraInferior.activarBarra = true
    this.enviarPensamienroCrear(true)
    this.seleccionarPensamientoMostrar(param)
    if (param === 1) {
      this.mostrarTitulosPublico = true
      this.mostrarTitulosPrivado = false
    }
    if (param === 2) {
      this.mostrarTitulosPublico = false
      this.mostrarTitulosPrivado = true
    }
  }
  //Selecciona el tipo de diseno del pensamiento que se va a mostrar privado, publico, o por defecto
  seleccionarPensamientoMostrar(data: number) {
    switch (data) {
      case 0:
        this.cargando = false
        this.pensamientoCompartido = { tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR, configuracionItem: { estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO }, subtitulo: false }
        this.dataListaPublico = { tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR, lista: [], cargarMas: () => { } }
        this.dataListaPrivado = { tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR, lista: [], cargarMas: () => { } }
        break;
      case 1:
        this.esPublico = true
        this.pensamientoCompartido = { esLista: true, tipoPensamiento: TipoPensamiento.PENSAMIENTO_PUBLICO_CREACION, configuracionItem: { estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO, presentarX: true }, subtitulo: true }
        this.cargarPensamientos()
        if (this.pensamientoCompartidoComponent) {
          this.pensamientoCompartidoComponent.divPensamiento = 'divPensamientoFormaAmarilla'
        }
        break;
      case 2:
        this.esPublico = false
        this.pensamientoCompartido = { esLista: true, tipoPensamiento: TipoPensamiento.PENSAMIENTO_PRIVADO_CREACION, configuracionItem: { estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO, presentarX: true }, subtitulo: true }
        this.cargarPensamientos()
        break;
      default:
        this.cargando = false
        this.pensamientoCompartido = { tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR, configuracionItem: { estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO }, subtitulo: false }
        break;
    }
  }

  unClick(itemPensamiento: ItemPensamiento) { }

  dobleClick(itemPensamiento: ItemPensamiento) { }

  clickLargo(itemPensamiento?: ItemPensamiento) { }

  enviarPensamienroCrear(activar: boolean) {
    this.barraInferior = {
      input: {
        maximo: 230,
        placeholder: "m6v3texto10",
        data: { texto: "" },
        tipo: TipoInput.TEXTO
      },
      activarBarra: activar,
      variosIconos: false,
      enviar: () => { }
    }
  }

  async cargarPensamientos() { }

  configurarBarraInferiorInline() {
    this.confBarraInferior = {
      capaColorFondo: {
        mostrar: true,
        anchoCapa: TamanoColorDeFondo.TAMANO100,
        colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE
      },
      estatusError: (error: string) => { },
      placeholder: {
        mostrar: true,
        texto: 'm6v1texto6',
        configuracion: {
          color: ColorDelTexto.TEXTOROJOBASE,
          enMayusculas: true,
          estiloTexto: EstilosDelTexto.REGULAR,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2
        }
      },
      iconoTexto: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_TEXTO,
        },
        capa: {
          mostrar: false,
          contador: {
            mostrar: true,
            maximo: 230
          }
        }
      },
      botonSend: {
        mostrar: true,
      },
    }
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
}
