import { MenuListaTipoProyectoDemoComponent } from './menu-lista-tipo-proyecto/menu-lista-tipo-proyecto.component';
import { MenuListaProyectosDemoComponent } from './menu-lista-proyectos/menu-lista-proyectos.component';
import { MenuPerfilesDemoComponent } from './menu-perfiles/menu-perfiles.component';
import { PublicarNoticiasProyectosDemoComponent } from './publicar-noticias-proyectos-demo/publicar-noticias-proyectos-demo.component';
import { CompraIntermacioInicioDemoComponent } from './compra-intermacio-inicio/compra-intermacio-inicio.component';
import { NoticiasUsuariosDemoComponent } from './noticias-usuarios/noticias-usuarios.component';
import { ListaProyectosMundialesDemoComponent } from './lista-proyectos/lista-proyectos.component';
import { InformacionUtilDemoComponent } from './informacion-util/informacion-util.component';
import { PensamientoDemoComponent } from './pensamiento/pensamiento.component';
import { ListaContactosDemoComponent } from './lista-contactos/lista-contactos.component';

import { CompartidoModule } from './../../compartido/compartido.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { DemoComponent } from './demo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { MenuprincipalDemoComponent } from './menuprincipal/menuprincipal.component';
import { PublicarProyectoComponent } from './publicar-proyecto/publicar-proyecto.component';
import { NoticiaPublicarComponent } from './publicar-noticia/publicar-noticia.component';
import { ActualizarProyectoComponent } from './actualizar-proyecto/actualizar-proyecto.component';
import { FinanzasDemoComponent } from './finanzas-demo/finanzas-demo.component';
import { DetalleDemoPerfilComponent } from './detalle-perfil/detalle-perfil.component';
import { ListaContactosCircularComponent } from 'src/app/compartido/componentes/lista-contactos-circular/lista-contactos-circular.component';
import { NgxCurrencyModule } from 'ngx-currency';


@NgModule({
  declarations: [
    DemoComponent,
    MenuprincipalDemoComponent,
    MenuPerfilesDemoComponent,
    ListaContactosDemoComponent,
    PensamientoDemoComponent,
    MenuListaTipoProyectoDemoComponent,
    InformacionUtilDemoComponent,
    MenuListaProyectosDemoComponent,
    ListaProyectosMundialesDemoComponent,
    NoticiasUsuariosDemoComponent,
    CompraIntermacioInicioDemoComponent,
    PublicarProyectoComponent,
    NoticiaPublicarComponent,
    PublicarNoticiasProyectosDemoComponent,
    ActualizarProyectoComponent,
    FinanzasDemoComponent,
    DetalleDemoPerfilComponent,
  ],
  imports: [
    NgxCurrencyModule,
    CommonModule,
    DemoRoutingModule,
    FormsModule,
    TranslateModule,
    CommonModule,
    CompartidoModule,
    DemoRoutingModule,
    FormsModule,
  ],
  exports: [
    NgxCurrencyModule,
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class DemoModule { }
