import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { RegistroService } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones';
import { CatalogoEstadoUsuario, CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton, ToastComponent } from '@shared/componentes';
import { AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, EstiloInput, TamanoColorDeFondo, TamanoDeTextoConInterlineado, UsoAppBar } from '@shared/diseno/enums';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import {
    BotonCompartido, ConfiguracionAppbarCompartida,
    ConfiguracionBuscadorModal, ConfiguracionDone, ConfiguracionSelector,
    ConfiguracionToast, InputCompartido, ItemSelector, LineaCompartida
} from '@shared/diseno/modelos';
import { CuentaNegocio, PerfilNegocio } from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import { PerfilModel, UsuarioModel } from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../enrutador/enrutador.component';
import { MenuPrincipalComponent } from './../menu-principal/menu-principal.component';


@Component({
    selector: 'app-mi-cuenta',
    templateUrl: './mi-cuenta.component.html',
    styleUrls: ['./mi-cuenta.component.scss']
})
export class MiCuentaComponent implements OnInit, Enrutador {

    @ViewChild('toast', { static: false }) toast: ToastComponent

    public confAppBar: ConfiguracionAppbarCompartida
    public perfilSeleccionado: PerfilModel;
    private usuarioActivo: UsuarioModel
    public solicitudInfo: boolean
    public infoSolicitudInfo: string
    public solicitudEliminacion: boolean
    public infoSolicitudEliminacion: string
    public solicitudReenvioCorreo: boolean
    public infoSolicitudReenvioCorreo: string

    public registroForm: FormGroup

    public confLinea: LineaCompartida
    public inputsForm: Array<InputCompartido>
    public botonSubmit: BotonCompartido
    public confBotonInfoCuentas: BotonCompartido
    public confBotonCorreoVerifi: BotonCompartido
    public confBotonEliminarCuenta: BotonCompartido

    public accionCuenta: number
    public validadorNombreContacto: boolean
    public validadorFechaNacimiento: boolean
    public validadorEmail: boolean
    public confSelector: ConfiguracionSelector // Configuracion del selector
    public confBuscador: ConfiguracionBuscadorModal // Configuracion buscador localidades
    public confDone: ConfiguracionDone
    public confToast: ConfiguracionToast
    public listaResultadosLocalidades: PaginacionModel<ItemSelector>

    // Enrutamiento
    public informacionEnrutador: InformacionEnrutador

    constructor(
        private translateService: TranslateService,
        private perfilNegocio: PerfilNegocio,
        public estiloDelTextoServicio: EstiloDelTextoServicio,
        private _location: Location,
        private registroService: RegistroService,
        private cuentaNegocio: CuentaNegocio,
        private notificacionesUsuario: NotificacionesDeUsuario,
        private enrutadorService: EnrutadorService,
        private generadorId: GeneradorId,
    ) {
        this.accionCuenta = 0
        this.inputsForm = []
        this.solicitudInfo = false
        this.infoSolicitudInfo = ''
        this.solicitudEliminacion = false
        this.infoSolicitudEliminacion = ''
        this.solicitudReenvioCorreo = false
        this.infoSolicitudReenvioCorreo = ''
        this.validadorFechaNacimiento = false
    }

    ngOnInit(): void {
        this.configurarPerfilSeleccionado()
        this.configurarToast()
        this.configurarDone()
        this.configurarUsuarioActivo()
        this.configurarBoton()

        if (this.perfilSeleccionado) {
            this.configurarAppBar()
            this.notificacionesUsuario.validarEstadoDeLaSesion(false)
            return
        }
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
    }

    configurarUsuarioActivo() {
        this.usuarioActivo = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

    }

    configurarPerfilSeleccionado() {
        this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    }

    configurarAppBar() {
        this.confAppBar = {
            usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
            botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
            accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
            eventoHome: () => this.enrutadorService.navegar(
                {
                    componente: MenuPrincipalComponent,
                    params: [
                    ]
                },
                {
                    estado: true,
                    posicicion: this.informacionEnrutador.posicion,
                    extras: {
                        tipo: TipoDeNavegacion.NORMAL,
                    }
                }
            ),
            searchBarAppBar: {
                tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
                nombrePerfil: {
                    mostrar: true,
                    llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
                        this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
                    )
                },
                mostrarTextoHome: true,
                mostrarDivBack: {
                    icono: true,
                    texto: true,
                },
                mostrarLineaVerde: true,
                subtitulo: {
                    mostrar: true,
                    llaveTexto: 'm11v1texto0'
                },
                buscador: {
                    mostrar: false,
                    configuracion: {
                        disable: true,
                    }
                }
            },
        }
    }

    configurarToast() {
        this.confToast = {
            // texto: 'Prueba de toas',
            texto: '',
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: true,
            intervalo: 3,
            // bloquearPantalla: true
        }
    }

    configurarDone() {
        this.confDone = {
            mostrarDone: false,
            mostrarLoader: false,
            intervalo: 3
        }
    }

    accionAtras() {
        this._location.back()
    }

    configurarLinea() {
        this.confLinea = {
            ancho: AnchoLineaItem.ANCHO6382,
            colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
            espesor: EspesorLineaItem.ESPESOR071,
            forzarAlFinal: false
        }
    }

    seleccionarAccion(accion: number) {
        if (this.accionCuenta === accion) {
            this.accionCuenta = 0
            return
        }

        switch (accion) {
            case AccionesCuenta.CAMBIAR_DATOS_CUENTA:
                this.accionCuenta = 1
                this.iniciarDatos()
                this.configurarInputs()
                break;
            case AccionesCuenta.SOLICITAR_INFO_CUENTA:
                this.accionCuenta = 2
                break;
            case AccionesCuenta.REENVIO_CORREO_VERI:
                this.accionCuenta = 3
                break;
            case AccionesCuenta.ELIMINAR_CUENTA:
                this.accionCuenta = 4
                break;


            default:
                break;
        }
    }

    iniciarDatos() {
        this.registroForm = this.registroService.inicializarControlesDelFormulario(this.perfilSeleccionado, false)
    }
    async configurarBoton() {
        this.botonSubmit = {
            text: 'm2v3texto20',
            tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
            colorTexto: ColorTextoBoton.AMARRILLO,
            tipoBoton: TipoBoton.TEXTO,
            enProgreso: false,
            ejecutar: () => {
                this.submitActualizar()
            }
        }

        this.confBotonInfoCuentas = {
            text: 'm2v3texto20',
            tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
            colorTexto: ColorTextoBoton.AMARRILLO,
            tipoBoton: TipoBoton.TEXTO,
            enProgreso: false,
            ejecutar: () => {
                this.solicitarInfoCuentas()
            }
        }

        this.confBotonCorreoVerifi = {
            text: 'm2v3texto20',
            tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
            colorTexto: ColorTextoBoton.AMARRILLO,
            tipoBoton: TipoBoton.TEXTO,
            enProgreso: false,
            ejecutar: () => {
                this.solicitarCorreoVeri()
            }
        }

        this.confBotonEliminarCuenta = {
            text: 'm2v3texto20',
            tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
            colorTexto: ColorTextoBoton.AMARRILLO,
            tipoBoton: TipoBoton.TEXTO,
            enProgreso: false,
            ejecutar: () => {
                this.solicitarEliminarCuenta()
            }
        }

    }

    solicitarInfoCuentas(): void {
        this.cuentaNegocio.solicitarInformacionUsuario(this.usuarioActivo.id).subscribe(data => {
          console.log('data.datos', data);
          
          if (data === 201) {
            this.solicitudInfo = true;
            this.infoSolicitudInfo = 'm11v1texto7';
          } else {
            this.infoSolicitudInfo = 'text37';
          }
          this.toast.cerrarToast();
        }, error => {
          this.toast.abrirToast('text37');
        });
      }
    
      solicitarCorreoVeri(): void {
        if (this.usuarioActivo.estado.codigo === CatalogoEstadoUsuario.ACTIVA) {
          this.solicitudReenvioCorreo = true;
          this.infoSolicitudReenvioCorreo = 'm11v1texto9.1';
          return;
        } else {
          this.cuentaNegocio.reenviarCorreoVerificacion(this.usuarioActivo.id).subscribe(data => {
            if (data.datos) {
              this.solicitudReenvioCorreo = true;
              this.infoSolicitudReenvioCorreo = 'm11v1texto9';
            } else {
              this.infoSolicitudReenvioCorreo = data.mensaje;
            }
            this.toast.cerrarToast();
          }, error => {
            this.toast.abrirToast('text37');
          });
        }
      }
    
      solicitarEliminarCuenta(): void {
        this.cuentaNegocio.eliminarDatosUsuario(this.usuarioActivo.id).subscribe(data => {
          if (data.datos) {
            this.solicitudEliminacion = true;
            this.infoSolicitudEliminacion = 'm11v1texto11';
          } else {
            this.infoSolicitudEliminacion = data.mensaje;
          }
          this.toast.cerrarToast();
        }, error => {
          this.toast.abrirToast('text37');
        });
      }
    configurarInputsDelFormulario(
        registroForm: FormGroup
    ): Array<InputCompartido> {
        let inputsForm: Array<InputCompartido> = []
        if (registroForm) {

            // 0
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: 'Name:',
                data: registroForm.controls.nombre,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            // 1
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: 'Apellidos:',
                data: registroForm.controls.apellido,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            // 2
            inputsForm.push({
                tipo: 'email',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: 'E-mail:',
                data: registroForm.controls.email,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: true
            })



            // 3
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: '',
                data: registroForm.controls.fechaNacimiento,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                esInputParaFecha: true,
                bloquearCopy: false
            })

            // 4
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: '',
                data: registroForm.controls.anteriorContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: false,
                    bloquearCopy: true
            })

            // 5
            inputsForm.push({
                tipo: 'password',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: '',
                data: registroForm.controls.nuevaContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: true,
                    bloquearCopy: true
            })
            // 6
            inputsForm.push({
                tipo: 'password',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.MI_CUENTA
                },
                placeholder: '',
                data: registroForm.controls.confirmarNuevaContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: true,
                    bloquearCopy: true
            })


        }
        return inputsForm
    }
    async configurarInputs() {
        this.inputsForm = this.configurarInputsDelFormulario(this.registroForm)
        if (this.inputsForm.length > 0) {
            this.inputsForm[0].placeholder = await this.translateService.get('m2v3texto10').toPromise()
            this.inputsForm[1].placeholder = await this.translateService.get('m2v3texto11').toPromise()
            this.inputsForm[2].placeholder = await this.translateService.get('m2v3texto12').toPromise()
            // this.inputsForm[3].placeholder = await this.translateService.get('').toPromise()
            // this.inputsForm[4].placeholder = await this.translateService.get('').toPromise()
            // this.inputsForm[5].placeholder = await this.translateService.get('').toPromise()
            // this.inputsForm[6].placeholder = await this.translateService.get('').toPromise()
            const usuario: UsuarioModel = this.perfilSeleccionado
            if (
                usuario &&
                usuario.perfiles &&
                usuario.perfiles.length === 0
            ) {
                this.inputsForm[1].soloLectura = false
                this.inputsForm[2].soloLectura = false
                this.inputsForm[3].soloLectura = false
                this.inputsForm[7].soloLectura = false
                this.inputsForm[8].soloLectura = false
                this.inputsForm[9].soloLectura = false
                this.inputsForm[10].soloLectura = false
            }
            this.inputsForm.forEach((input, pos) => {
                if (pos === 2) {

                    input.validarCampo = {
                        validar: true,
                        validador: (data: any) => this.validarEmailUnico(data, false)
                    }

                }
                if (pos === 3) {

                    input.validarCampo = {
                        validar: true,
                        validador: (data: any) => this.validarFechaNacimiento(data, false)
                    }

                }

            })
        }
    }

    async validarEmailUnico(
        data: { id: string, texto: string },
        forzarSubmit: boolean = true
    ) {
        if (data.texto === this.usuarioActivo.email) {
            return
        }

        try {
            if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
                if (forzarSubmit) {
                    this.botonSubmit.enProgreso = false
                    this.toast.abrirToast('text4')
                }
                return
            }

            const estado: string = await this.cuentaNegocio.validarEmailUnico(data.texto).toPromise()

            if (!estado) {
                throw new Error('')
            }

            this.validadorEmail = true

            if (this.validadorEmail && this.validadorNombreContacto && forzarSubmit) {
                this.submitActualizar
            }
        } catch (error) {
            this.validadorEmail = false
            this.botonSubmit.enProgreso = false
            const errorMensaje = (error && error.message && error.message.length > 0) ? error.message : 'text6'
            this.asignarMensajeDeErrorEnInput(data.id, errorMensaje)
        }
    }

    asignarMensajeDeErrorEnInput(id: string, mensaje: string) {
        if (!(this.inputsForm.length > 0)) {
            return
        }
        this.inputsForm.forEach(item => {
            if (item.id === id) {
                item.errorPersonalizado = mensaje
            }
        })
    }

    calcularEdad(fecha_nacimiento) {
        let hoy = new Date();
        let cumpleanos = new Date(fecha_nacimiento);
        let edad = hoy.getFullYear() - cumpleanos.getFullYear();
        let m = hoy.getMonth() - cumpleanos.getMonth();
        if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
            edad--;
        }
        return edad;
    }

    async validarFechaNacimiento(
        data: { id: string, texto: string },
        forzarSubmit: boolean = true
    ) {
        try {

            if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
                if (forzarSubmit) {
                    this.botonSubmit.enProgreso = false
                    this.toast.abrirToast('text4')
                }
                return
            }


            let edad = this.calcularEdad(data.texto)

            if (edad >= 18) {
                return true

            } else {
                this.asignarMensajeDeErrorEnInput(data.id, 'text84')
                return false
            }


        } catch (error) {

            this.validadorFechaNacimiento = false
            this.botonSubmit.enProgreso = false
            const errorMensaje = (error && error.message && error.message.length > 0) ? error.message : 'text84'
            this.asignarMensajeDeErrorEnInput(data.id, errorMensaje)
            return false
        }

    }

    async submitActualizar() {

        try {
            this.botonSubmit.enProgreso = true
            let errorEnCampos = true
            if (this.registroForm.valid) {
                errorEnCampos = false
            }
            if (!this.validadorFechaNacimiento) {
                let validFechaNac = await this.validarFechaNacimiento(
                    {
                        id: this.inputsForm[3].id,
                        texto: this.inputsForm[3].data.value
                    }
                )


                if (!validFechaNac) {

                    this.botonSubmit.enProgreso = false
                    return

                }
            }
            if (
                (
                    this.registroForm.value['anteriorContrasena'] === ''
                    && this.registroForm.value['confirmarNuevaContrasena'] === ''
                    && this.registroForm.value['nuevaContrasena'] === ''
                )
                ||
                (
                    this.registroForm.value['anteriorContrasena'] !== ''
                    && this.registroForm.value['confirmarNuevaContrasena'] !== ''
                    && this.registroForm.value['nuevaContrasena'] !== ''
                )

            ) {
                errorEnCampos = false
            }


            if (
                (
                    this.registroForm.value['anteriorContrasena'] !== ''
                    && this.registroForm.value['confirmarNuevaContrasena'] === ''
                    && this.registroForm.value['nuevaContrasena'] === ''
                )
                ||
                (
                    this.registroForm.value['anteriorContrasena'] === ''
                    && this.registroForm.value['confirmarNuevaContrasena'] !== ''
                    && this.registroForm.value['nuevaContrasena'] === ''
                )
                ||
                (
                    this.registroForm.value['anteriorContrasena'] === ''
                    && this.registroForm.value['confirmarNuevaContrasena'] === ''
                    && this.registroForm.value['nuevaContrasena'] !== ''
                )


            ) {
                errorEnCampos = true
            }

            if (errorEnCampos) {
                this.botonSubmit.enProgreso = false
                this.toast.abrirToast('text4')
                return
            }

            if (this.registroForm.value['nuevaContrasena'] !== this.registroForm.value['confirmarNuevaContrasena']) {
                this.toast.abrirToast('text88')
                return

            }
            let nombreCompleto = this.registroForm.value['nombre'] + '-' + this.registroForm.value['apellido']
            this.registroForm.value['nombre'] = nombreCompleto

            let usuario: UsuarioModel
            usuario = {}

            if (this.registroForm.value['nombre']) {
                usuario.nombre = this.registroForm.value['nombre']
            }
            if (this.registroForm.value['email'] && this.registroForm.value['email'] !== this.usuarioActivo.email) {
                usuario.email = this.registroForm.value['email']
            }
            if (this.registroForm.value['anteriorContrasena']) {
                usuario.anteriorContrasena = this.registroForm.value['anteriorContrasena']
            }

            if (this.registroForm.value['nuevaContrasena']) {
                usuario.nuevaContrasena = this.registroForm.value['nuevaContrasena']
            }
            if (this.registroForm.value['fechaNacimiento']) {
                usuario.fechaNacimiento = this.registroForm.value['fechaNacimiento']
            }
            usuario.idDispositivo = this.usuarioActivo.dispositivos[0].id
            const usuarioActualizado = await this.perfilNegocio.actualizarDatosUsuario(usuario, this.usuarioActivo.id).toPromise()
            if (!usuarioActualizado) {
                throw new Error("")
            }
            this.actualizarEnLocalStorage(usuario)
            this.toast.cerrarToast()
            this.confDone.mostrarDone = true
            setTimeout(() => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            }, 1500)
        } catch (error) {
            this.botonSubmit.enProgreso = false;
            this.toast.abrirToast('text37');
        }
    }

    actualizarEnLocalStorage(usuario: UsuarioModel) {
        if (usuario.nombre) {
            let perfil: PerfilModel = this.perfilNegocio.obtenerPerfilSeleccionado()
            perfil.nombre = usuario.nombre
            this.perfilNegocio.actualizarPerfilSeleccionado(perfil)
        }
        if (usuario.fechaNacimiento) {
            let usuarioModel: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
            usuarioModel.fechaNacimiento = usuario.fechaNacimiento
            this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuarioModel)
        }

    }

    // Enrutamiento
    configurarInformacionDelEnrutador(item: InformacionEnrutador) {
        this.informacionEnrutador = item
    }
}

export enum AccionesCuenta {
    CAMBIAR_DATOS_CUENTA = 1,
    SOLICITAR_INFO_CUENTA = 2,
    REENVIO_CORREO_VERI = 3,
    ELIMINAR_CUENTA = 4

}


