import { Component, OnInit } from '@angular/core';
import { NotificacionesDeUsuario } from '@core/servicios/generales';

@Component({
  selector: 'app-gazing',
  templateUrl: './gazing.component.html',
  styleUrls: ['./gazing.component.scss']
})
export class GazingComponent implements OnInit {

  constructor(
    private notificacionesUsuario: NotificacionesDeUsuario
  ) { }

  ngOnInit(): void {
    this.notificacionesUsuario.validarEstadoDeLaSesion(false)
  }

}
