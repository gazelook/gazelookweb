export enum RutasGazing {
    CHAT_GAZE = 'chat-gaze/:id',
    CHAT_CONTACTOS = 'contactos',
    CONFERENCIA = 'conferencia'
}