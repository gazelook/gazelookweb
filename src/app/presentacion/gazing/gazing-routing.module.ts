import { ChatContactosComponent } from './chat-contactos/chat-contactos.component';
import { ChatGazeComponent } from './chat-gaze/chat-gaze.component';
import { RutasGazing } from './rutas-gazing.enum';
import { GazingComponent } from './gazing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
	{
		path: '',
		component: GazingComponent,
		children: [
			{
				path: RutasGazing.CHAT_GAZE.toString(),
				component: ChatGazeComponent
			},
			{
				path: RutasGazing.CHAT_CONTACTOS.toString(),
				component: ChatContactosComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GazingRoutingModule {

}
