import { environment } from './../../../environments/environment';
// import { NgxAgoraModule } from 'ngx-agora';
import { FormsModule } from '@angular/forms';
import { CompartidoModule } from './../../compartido/compartido.module';
import { TranslateModule } from '@ngx-translate/core';
import { GazingRoutingModule } from './gazing-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GazingComponent } from './gazing.component';


// import { AngularAgoraRtcModule, AgoraConfig } from 'angular-agora-rtc';
import { ChatGazeComponent } from './chat-gaze/chat-gaze.component';
import { ChatContactosComponent } from './chat-contactos/chat-contactos.component'; // Add


@NgModule({
  declarations: [
    GazingComponent,
    ChatGazeComponent,
    ChatContactosComponent,
  ],
  imports: [
    CommonModule,
    GazingRoutingModule,
    CompartidoModule,
    TranslateModule, 
    FormsModule
  ],
  exports: [
    TranslateModule
  ]
})
export class GazingModule { }
