import { CodigosCatalogoArchivosPorDefecto } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { MediaNegocio } from './../../dominio/logica-negocio/media.negocio';
import { GeneradorId } from './../../nucleo/servicios/generales/generador-id.service';
import { NoticiaModel } from './../../dominio/modelo/entidades/noticia.model';
import { AsociacionModel } from './../../dominio/modelo/entidades/asociacion.model';
import { BarraBusqueda } from './../../compartido/diseno/modelos/appbar.interface';
import { AlbumModel } from './../../dominio/modelo/entidades/album.model';
import { CodigosCatalogoTipoAlbum } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { AlbumNegocio } from './../../dominio/logica-negocio/album.negocio';
import { ProyectoModel } from './../../dominio/modelo/entidades/proyecto.model';
import { PerfilModel } from './../../dominio/modelo/entidades/perfil.model';
import { CatalogoTipoMensaje, EstadoMensaje, CatalogoEstatusMensaje } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { ParticipanteAsociacionModel } from './../../dominio/modelo/entidades/participante-asociacion.model';
import { SubirArchivoData } from './../../dominio/modelo/subir-archivo.interface';
import { MediaModel } from './../../dominio/modelo/entidades/media.model'
import { ConfiguracionMensajeFirebase } from './../../compartido/componentes/mensaje-gaze/mensaje-gaze.component'
import { MensajeModelFirebase } from './../../dominio/modelo/entidades/mensaje.model'
import { Injectable } from '@angular/core'
import { Fecha, IndicadoresDelScrool, NotificacionInterno, VariablesDelScrool, MarcadorDeNotificacion } from './chat-gaze/chat-gaze.component'
import * as Firebase from 'firebase/app'
import { CodigosCatalogoEntidad } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ModoBusqueda } from 'src/app/compartido/diseno/modelos/buscador.interface';
import { ConversacionModel } from 'src/app/dominio/modelo/entidades/conversacion.model';

@Injectable({ providedIn: 'root' })
export class ChatMetodosCompartidosService {

    public imagenesPorDefecto: Array<string>

    constructor(
        private albumNegocio: AlbumNegocio,
        private generadorId: GeneradorId,
        private mediaNegocio: MediaNegocio,
    ) {
        let a = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal()   
        let b =  a.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.PROYECTOS) 
        
        this.imagenesPorDefecto = [
         
        ]

        this.imagenesPorDefecto.push(b[0].url)
        this.imagenesPorDefecto.push(b[1].url)
        this.imagenesPorDefecto.push(b[2].url)
        this.imagenesPorDefecto.push(b[3].url)
        this.imagenesPorDefecto.push(b[4].url)
        this.imagenesPorDefecto.push(b[5].url)
        this.imagenesPorDefecto.push(b[6].url)
        this.imagenesPorDefecto.push(b[7].url)
    }

    ordenarItemEnListaPorFechaDeFormaAscendente(
        items: Array<MensajeModelFirebase>
    ): Array<MensajeModelFirebase> {
        return items.sort((a, b) => a.fechaCreacion - b.fechaCreacion)
    }

    ordenarItemEnListaDeNotificaciones(
        items: Array<NotificacionInterno>
    ): Array<NotificacionInterno> {
        return items.sort((a, b) => a.fechaCreacion - b.fechaCreacion)
    }

    ordenarItemsEnListaDeFechasDeFormaAscendente(
        items: Array<Fecha>
    ): Array<Fecha> {
        return items.sort((a, b) => a.timestamp - b.timestamp)
    }

    ordenarItemEnListaDeConfiguracionDeFormaAscendente(
        items: Array<ConfiguracionMensajeFirebase>
    ): Array<ConfiguracionMensajeFirebase> {
        return items.sort((a, b) => a.mensaje.fechaCreacion - b.mensaje.fechaCreacion)
    }

    crearListaDeConfiguraCionDeMensajes(
        items: Array<MensajeModelFirebase>,
        perfil: PerfilModel,
        listaMensajesSeleccionados: Array<MensajeModelFirebase>,
        lista: Array<ConfiguracionMensajeFirebase>,
    ): Array<ConfiguracionMensajeFirebase> {
        items.forEach( item => {
            const index = lista.findIndex(e => e.mensaje.id === item.id)
            
            if (index < 0) {
                lista.push(this.crearConfiguraCionMensajeUnica(
                    item, 
                    perfil,
                    false,
                    false,
                    listaMensajesSeleccionados.length > 0
                ))
            }
            
            if (index >= 0){
                lista[index].mensaje.estatus = item.estatus
                lista[index].mensaje.estado = item.estado
                lista[index].mensaje.leidoPor = item.leidoPor

                if (item.tipo.codigo === CatalogoTipoMensaje.TRANSFERIR_PROYECTO) {
                    lista[index].mensaje.referenciaEntidadCompartida['perfil'] = item.referenciaEntidadCompartida['perfil']
                }
            }
        })

        return lista
    }

    crearConfiguraCionMensajeUnica(
        item: MensajeModelFirebase,
        perfil: PerfilModel,
        seleccionado: boolean = false,
        mostrarLoader: boolean = false,
        capaEncima: boolean = false,
    ): ConfiguracionMensajeFirebase {
        let leido: boolean = false
        for (const partici of item.leidoPor) {
            if (partici.perfil._id !== item.propietario.perfil._id) {
                leido = true
            }
        }
        return {
            mensaje: item,
            esPropietario: (perfil._id === item.propietario.perfil._id),
            seleccionado: seleccionado,
            mostrarCargando: mostrarLoader,
            contenidoError: '',
            capaEncima: capaEncima,
            leido: leido
        }
    }

    crearListaDeFechasDesdeListaDeMensajes(
        items: Array<MensajeModelFirebase>,
        configuraciones: Array<ConfiguracionMensajeFirebase>,
        lista: Array<Fecha>,
        insertarAlFinal: boolean
    ): Array<Fecha> {
        // const aux: Array<Fecha> = []
        items.forEach(item => {
            const index = lista.findIndex(e => this.compararFechas(e.date, item.fechaCreacion))
            if (index < 0) {
                lista.push({
                    id: this.generadorId.generarIdConSemilla(),
                    timestamp: item.fechaCreacion,
                    date: new Date(item.fechaCreacion),
                    mensajes: []
                })
            }
        })

        lista.forEach(fecha => {
            configuraciones.forEach(configuracion => {
                if (this.compararFechas(fecha.date, configuracion.mensaje.fechaCreacion)) {
                    const index = fecha.mensajes.findIndex(e => e.mensaje.id === configuracion.mensaje.id)

                    if (index < 0) {
                        fecha.mensajes.push(configuracion)
                    }
                }
            })
        })

        lista.forEach(fecha => {
            fecha.mensajes = this.ordenarItemEnListaDeConfiguracionDeFormaAscendente(fecha.mensajes)
        })

        return lista
    }

    compararFechas(
        a: Date,
        aux: any
    ) {
        const b = new Date(aux)
        
        return (
            a.getFullYear() === b.getFullYear() &&
            a.getMonth() === b.getMonth() &&
            a.getDate() === b.getDate()
        )
    }

    validarScroolAlFinal(
        dataScrool: VariablesDelScrool,
        idLista: string
    ) {
        const e = document.getElementById(idLista) as HTMLElement
        if (!e) {
            return
        }

        if (!dataScrool.scroolAlFinal) {
            return
        }

        e.scrollTop = e.scrollHeight + e.offsetHeight
    }

    obtenerDataScroll(
        idLista: string,
        indicadores: IndicadoresDelScrool,
        antes: boolean = true,
    ): IndicadoresDelScrool {
        const e = document.getElementById(idLista) as HTMLElement

        const data  = {
            height: e.scrollHeight,
            offset: e.scrollTop,
        }

        if (antes) {
            indicadores.antes = data
        }

        if (!antes) {
            indicadores.despues = data
        }

        return indicadores
    }

    establecerPosicionDelScrollTop(
        delta: number,
        idLista: string
	): void {
        const e = document.getElementById(idLista) as HTMLElement
        
        if (!e) {
            return
        }

        e.scrollTop = delta
    }
    
    validarScroolAlFinalParaNotificaciones(
        idLista: string
    ): boolean {
        const elemento = document.getElementById(idLista) as HTMLElement
        if (!elemento) {
            return false
        }

        if (elemento.scrollTop >= 0 && elemento.scrollTop < (elemento.scrollHeight - elemento.clientHeight)) {
            return true
        }

        return false
    }

    actualizarEstadoIndicadorNotificaciones(
        indicadorNotificacionesInterno: MarcadorDeNotificacion,
        mensaje: MensajeModelFirebase,
        idPerfil: string
    ) {
        const indexUno = indicadorNotificacionesInterno.idLeidos.findIndex(e => e.id === mensaje.id)

        if (indexUno >= 0) {
            return indicadorNotificacionesInterno
        }

        const indexDos = mensaje.leidoPor.findIndex(e => e.perfil._id === idPerfil)
        if (indexDos < 0) {
            const indexTres = indicadorNotificacionesInterno.idPorLeer.findIndex(e => e.id === mensaje.id)
            if (indexTres < 0) {
                indicadorNotificacionesInterno.idPorLeer.push({
                    id: mensaje.id,
                    fechaCreacion: mensaje.fechaCreacion
                })
                indicadorNotificacionesInterno.idPorLeer = this.ordenarItemEnListaDeNotificaciones(
                    indicadorNotificacionesInterno.idPorLeer
                )
            }
        } else {
            const indexTres = indicadorNotificacionesInterno.idLeidos.findIndex(e => e.id === mensaje.id)
            if (indexTres < 0) {
                indicadorNotificacionesInterno.idLeidos.push({
                    id: mensaje.id,
                    fechaCreacion: mensaje.fechaCreacion
                })
                indicadorNotificacionesInterno.idLeidos = this.ordenarItemEnListaDeNotificaciones(
                    indicadorNotificacionesInterno.idLeidos
                )
            }
        }

        return indicadorNotificacionesInterno
    }

    crearObjetoMensajeAEnviar(
        idMensaje: string,
        dataApiArchivo: SubirArchivoData,
        propietario: ParticipanteAsociacionModel,
        tipo: CatalogoTipoMensaje,
        idAsociacion: string,
        idPerfilSeleccionado: string,
        adjuntos: Array<MediaModel> = [],
        estado: EstadoMensaje = EstadoMensaje.ACTIVA,
    ): MensajeModelFirebase {
        return {
            id: idMensaje,
            estado: {
                codigo: estado
            },
            fechaCreacion: Firebase.default.database.ServerValue.TIMESTAMP,
            fechaActualizacion: Firebase.default.database.ServerValue.TIMESTAMP,
            tipo: {
                codigo: tipo
            },
            importante: false,
            conversacion: {
                id: idAsociacion,
            },
            propietario: {
                id: propietario.id || '',
                perfil: {
                    _id: idPerfilSeleccionado,
                    nombreContacto: propietario.perfil.nombreContacto
                }
            },
            contenido: dataApiArchivo.descripcion || '',
            adjuntos: adjuntos,
            entregadoA: [
                {
                    id: propietario.id || '',
                    perfil: {
                        _id: idPerfilSeleccionado,
                        nombreContacto: propietario.perfil.nombreContacto
                    }
                }
            ],
            eliminadoPor: [],
            leidoPor: [
                {
                    id: propietario.id || '',
                    perfil: {
                        _id: idPerfilSeleccionado,
                        nombreContacto: propietario.perfil.nombreContacto
                    }
                }
            ],
            listaIdConversacion: [],
            identificadorTemporal: new Date().getTime(),
            esLocal: true,
        }
    }

    crearObjetoMensajeParaProyecto(
        idMensaje: string,
        idAsociacion: string,
        tipo: CatalogoTipoMensaje,
        propietario: ParticipanteAsociacionModel,
        proyecto: ProyectoModel,
        adjuntos: Array<MediaModel> = [],
        estado: EstadoMensaje = EstadoMensaje.ACTIVA,
    ): MensajeModelFirebase {
        return {
            id: idMensaje,
            estado: {
                codigo: estado
            },
            estatus: {
                codigo: CatalogoEstatusMensaje.ENVIADO,
            },
            fechaCreacion: Firebase.default.database.ServerValue.TIMESTAMP,
            fechaActualizacion: Firebase.default.database.ServerValue.TIMESTAMP,
            tipo: {
                codigo: tipo
            },
            importante: false,
            conversacion: {
                id: idAsociacion,
            },
            propietario: propietario,
            contenido: '',
            adjuntos: adjuntos,
            entregadoA: [ propietario ],
            eliminadoPor: [],
            leidoPor: [ propietario ],
            listaIdConversacion: [],
            identificadorTemporal: new Date().getTime(),
            esLocal: false,
            referenciaEntidadCompartida: this.crearObjetoProyectoParaMensaje(proyecto)
        }
    }

    crearObjetoProyectoParaMensaje(
        proyecto: ProyectoModel
    ): ProyectoModel {
        const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.GENERAL,
            proyecto.adjuntos,
        )

        const albumObj: AlbumModel = this.albumNegocio.crearObjetoDeAlbumVacio(CodigosCatalogoTipoAlbum.GENERAL)

        if (album) {
            albumObj._id = album._id
        }

        albumObj.portada = { 
            principal: {
                fileDefault: true,
                url: this.obtenerUrlRamdon()
            }
        }

        return {
            id: proyecto.id,
            tituloCorto: proyecto.tituloCorto,
            fechaCreacion: proyecto.fechaCreacion,
            adjuntos: [ albumObj ],
            perfil: proyecto.perfil,
        }
    }

    obtenerUrlRamdon() {
        return this.imagenesPorDefecto[Math.floor(Math.random() * this.imagenesPorDefecto.length)]
    }

    configurarBuscadorParaBarraReenvio(
        buscar: Function,
    ): BarraBusqueda {
        return {
            mostrar: true,
            configuracion: {
                disable: false,
                entidad: CodigosCatalogoEntidad.CONTACTO,
                modoBusqueda: ModoBusqueda.BUSQUEDA_LOCAL,
                buscar: buscar,
                valorBusqueda: '',
                placeholder: 'm3v12texto1'
            }
        }
    }

    obtenerUrlMediaDeAdjuntos(mensaje: MensajeModelFirebase) {
        if (
            mensaje.adjuntos &&
            mensaje.adjuntos.length > 0
        ) {
            return mensaje.adjuntos[0].principal.url
        }

        return ''
    }
    obtenerFilenameMediaDeAdjuntos(mensaje: MensajeModelFirebase) {
        if (
            mensaje.adjuntos &&
            mensaje.adjuntos.length > 0
        ) {
            return mensaje.adjuntos[0].principal.filename
        }

        return ''
    }

    filtrarParticipantesDeAsociacion(
        asociacion: AsociacionModel
    ): ParticipanteAsociacionModel[] {
        const participantes: ParticipanteAsociacionModel[] = []

        if (
            !asociacion ||
            !asociacion.participantes ||
            asociacion.participantes.length === 0
        ) {
            return participantes
        }

        asociacion.participantes.forEach(participante => {
            if (participante) {
                participantes.push({
                    id: participante.id,
                    perfil: {
                        _id: participante.perfil._id,
                        nombreContacto: participante.perfil.nombreContacto
                    }
                })
            }
        })

        return participantes
    }

    crearObjetoMensajeParaNoticia(
        idMensaje: string,
        idAsociacion: string,
        tipo: CatalogoTipoMensaje,
        propietario: ParticipanteAsociacionModel,
        noticia: NoticiaModel,
        adjuntos: Array<MediaModel> = [],
        estado: EstadoMensaje = EstadoMensaje.ACTIVA,
    ): MensajeModelFirebase {
        return {
            id: idMensaje,
            estado: {
                codigo: estado
            },
            estatus: {
                codigo: CatalogoEstatusMensaje.ENVIADO,
            },
            fechaCreacion: Firebase.default.database.ServerValue.TIMESTAMP,
            fechaActualizacion: Firebase.default.database.ServerValue.TIMESTAMP,
            tipo: {
                codigo: tipo
            },
            importante: false,
            conversacion: {
                id: idAsociacion,
            },
            propietario: propietario,
            contenido: '',
            adjuntos: adjuntos,
            entregadoA: [ propietario ],
            eliminadoPor: [],
            leidoPor: [ propietario ],
            listaIdConversacion: [],
            identificadorTemporal: new Date().getTime(),
            esLocal: false,
            referenciaEntidadCompartida: this.crearObjetoNoticiaParaMensaje(noticia)
        }
    }

    crearObjetoNoticiaParaMensaje(
        noticia: NoticiaModel
    ): ProyectoModel {
        const album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(noticia.adjuntos,)

        const albumObj: AlbumModel = this.albumNegocio.crearObjetoDeAlbumVacio(CodigosCatalogoTipoAlbum.GENERAL)

        if (album) {
            albumObj._id = album._id
        }

        albumObj.portada = { 
            principal: {
                fileDefault: true,
                url: this.obtenerUrlRamdon()
            }
        }
        albumObj.predeterminado = true

        return {
            id: noticia.id,
            tituloCorto: noticia.tituloCorto,
            fechaCreacion: noticia.fechaCreacion,
            adjuntos: [ albumObj ],
            perfil: noticia.perfil,
        }
    }

    crearObjetoDeConversacion(
        asociacion: AsociacionModel,
    ): ConversacionModel {
        return {
            id: asociacion.id,
            asociacion: {
                id: asociacion.id,
                participantes: this.filtrarParticipantesDeAsociacion(asociacion)
            },
            ultimoMensaje: {
                id: ''
            }
        }
    }

    crearObjetoDeConversacionParaReenvio(
        propietario: ParticipanteAsociacionModel,
        contactoDe: PerfilModel,
        asociacion: AsociacionModel
    ): ConversacionModel {

        const conversacion: ConversacionModel = {
            id: asociacion.id,
            asociacion: {
                id: asociacion.id,
                participantes: []
            },
            ultimoMensaje: {
                id: ''
            }
        }

        // Propietario
        conversacion.asociacion.participantes.push({
            id: propietario.id,
            perfil: {
                _id: propietario.perfil._id,
                nombreContacto: propietario.perfil.nombreContacto
            }
        })

        // Contacto
        conversacion.asociacion.participantes.push({
            id: '',
            perfil: {
                _id: contactoDe._id,
                nombreContacto: contactoDe.nombreContacto
            }
        })

        return conversacion
    }

}