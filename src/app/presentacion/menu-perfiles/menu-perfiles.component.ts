import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogoServicie } from '@core/servicios/diseno';
import {
  GeneradorId, VariablesGlobales
} from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
  AccionEntidad, CodigosCatalogosEstadoPerfiles, CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import { MetodosSessionStorageService } from '@core/util';
import { ColorTextoBoton, ModalContenido, ModalInferior, TipoBoton, TipoMenu, ToastComponent } from '@shared/componentes';
import {
  AnchoLineaItem, ColorDelTexto, ColorFondoItemMenu, ColorFondoLinea, EspesorLineaItem,
  EstiloErrorInput, EstiloInput, EstilosDelTexto,
  TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoItemMenu, TamanoLista, UsoAppBar
} from '@shared/diseno/enums';
import { BotonCompartido, ConfiguracionAppbarCompartida, ConfiguracionToast, DatosLista, InputCompartido, ItemMenuCompartido } from '@shared/diseno/modelos';
import { CuentaNegocio, IdiomaNegocio, InternacionalizacionNegocio, PerfilNegocio } from 'dominio/logica-negocio';
import { InformacionModel } from 'dominio/modelo';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos';
import {
  PerfilModel
} from 'dominio/modelo/entidades';
import { CatalogoIdiomaEntity } from '../../dominio/entidades/catalogos';
import { Enrutador, InformacionEnrutador } from '../enrutador/enrutador.component';
import { RegistroComponent } from '../registro/registro.component';
@Component({
  selector: 'app-menu-perfiles',
  templateUrl: './menu-perfiles.component.html',
  styleUrls: ['./menu-perfiles.component.scss']
})
export class MenuPerfilesComponent implements OnInit, Enrutador {

  @ViewChild('toast', { static: false }) toast: ToastComponent

  // Parametros internos
  public idPerfilIncompatibleDialogo = "aviso-tipo-perfil"
  public tipoPerfil: CatalogoTipoPerfilModel
  public listaTipoPerfil: CatalogoTipoPerfilModel[]
  public itemInformacion: InformacionModel
  public menorEdadForm: FormGroup
  public dataLista: DatosLista
  public itemSeleccionado: any

  // Configuraciones
  public confAppBar: ConfiguracionAppbarCompartida
  public confBotonPerfilIncopatible: BotonCompartido
  public confDialogoPerfilIncompatible: ModalContenido
  public confInputNombresResponsable: InputCompartido
  public confInputCorreoResponsable: InputCompartido
  public confModalTerminosCondiciones: ModalInferior
  public confToast: ConfiguracionToast
	isChecked: boolean
  public idiomaSeleccionado: CatalogoIdiomaEntity
  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    public variablesGlobales: VariablesGlobales,
    private perfilNegocio: PerfilNegocio,
    private dialogoServicie: DialogoServicie,
    private router: Router,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private formBuilder: FormBuilder,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private metodosSessionStorageService: MetodosSessionStorageService,
    private enrutadorService: EnrutadorService,
    private generadorId: GeneradorId,
    private idiomaNegocio: IdiomaNegocio,
  ) {
    this.listaTipoPerfil = []
    this.isChecked = false
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true
    this.configurarAppBar()
    this.obtenerIdiomaSeleccionado()
    this.configurarBotonAceptar()
    this.prepararInfoTipoPerfiles()
    this.configurarDialogoContenido()
    this.iniciarFormMenorEdad()
    this.prepararModalTerminosCondiciones()
    this.preperarListaMenuTipoPerfil()
    this.obtenerCatalogoTipoPerfil()
    this.configurarToast()

    this.verificarAceptacionTerminosCondiciones()
  }


  verificarAceptacionTerminosCondiciones() {
    this.confModalTerminosCondiciones.abierto = this.cuentaNegocio.verificarAceptacionTerminosCondiciones();
  }

  obtenerIdiomaSeleccionado(){
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
  }

  irDocumentosLegales(): void {
    console.log('this.idiomaSeleccionado.codNombre', this.idiomaSeleccionado.codNombre);
    
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/terminos-condiciones-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
}

  obtenerCatalogoTipoPerfil() {
    this.perfilNegocio.obtenerCatalogoTipoPerfilConPerfil().subscribe((res: CatalogoTipoPerfilModel[]) => {
      this.listaTipoPerfil = res
      this.dataLista.cargando = false;
    }, error => {
      this.dataLista.error = error;
      this.dataLista.cargando = false;
    })
  }

  obtenerLlavesTextosSegunCodigoPerfil(
    codigoPerfil: CodigosCatalogoTipoPerfil
  ) {
    const textos = {
      texto1: '',
      texto2: '',
      texto3: ''
    }
    switch (codigoPerfil) {
      case CodigosCatalogoTipoPerfil.CLASSIC:
        textos.texto1 = 'm2v1texto5'
        textos.texto2 = 'm2v1texto6'
        textos.texto3 = 'm2v1texto7'
        break
      case CodigosCatalogoTipoPerfil.PLAYFUL:
        textos.texto1 = 'm2v1texto9'
        textos.texto2 = 'm2v1texto10'
        textos.texto3 = 'm2v1texto11'
        break
      case CodigosCatalogoTipoPerfil.SUBSTITUTE:
        textos.texto1 = 'm2v1texto13'
        textos.texto2 = 'm2v1texto14'
        textos.texto3 = 'm2v1texto15'
        break
      case CodigosCatalogoTipoPerfil.GROUP:
        textos.texto1 = 'm2v1texto18'
        textos.texto2 = 'm2v1texto19'
        textos.texto3 = 'm2v1texto20'
        break
      default: break
    }

    return textos
  }

  // Devuelve la llave de la traduccion del texto segun el tipo de perfil
  obtenerLlaveSegunCodigoPerfil(
    codigoPerfil: CodigosCatalogoTipoPerfil
  ) {
    switch (codigoPerfil) {
      case CodigosCatalogoTipoPerfil.CLASSIC:
        return 'texto322'
      case CodigosCatalogoTipoPerfil.PLAYFUL:
        return 'texto325'
      case CodigosCatalogoTipoPerfil.SUBSTITUTE:
        return 'texto328'
      case CodigosCatalogoTipoPerfil.GROUP:
        return 'texto331'
      default:
        return ''
    }
  }

  obtenerLlaveDescripcionSegunTipoPerfil(
    codigoPerfil: CodigosCatalogoTipoPerfil
  ) {
    switch (codigoPerfil) {
      case CodigosCatalogoTipoPerfil.CLASSIC:
        return ['m2v1texto8']
      case CodigosCatalogoTipoPerfil.PLAYFUL:
        return ['m2v1texto12']
      case CodigosCatalogoTipoPerfil.SUBSTITUTE:
        return ['m2v1texto16', 'm2v1texto17']
      case CodigosCatalogoTipoPerfil.GROUP:
        return ['m2v1texto21']
      default:
        return ''
    }
  }

  prepararItemTipoPerfil(tipoPerfil: CatalogoTipoPerfilModel): ItemMenuCompartido {
    const descripcion: any = this.obtenerLlaveDescripcionSegunTipoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
    const textos = this.obtenerLlavesTextosSegunCodigoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)

    const data = {
      id: '',
      tamano: TamanoItemMenu.ITEMMENUCREARPERFIL, // Indica el tamano del item (altura)
      colorFondo: this.obtenerColorPerfil(tipoPerfil.perfil),
      mostrarDescripcion: tipoPerfil.mostrarDescripcion ?? false,
      texto1: this.obtenerEstadoPerfil(tipoPerfil.perfil),
      texto2: textos.texto2,
      texto3: textos.texto3,
      tipoMenu: TipoMenu.GESTION_PROFILE,
      descripcion: [],
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6386,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: false
        }
      },
      gazeAnuncios: false,
      idInterno: tipoPerfil.codigo,
      onclick: () => this.mostrarDescripcion(tipoPerfil),
      dobleClick: () => this.gestionarPerfil(tipoPerfil)
    }

    descripcion.forEach((d: any) => {
      data.descripcion.push(
        {
          texto: this.internacionalizacionNegocio.obtenerTextoSincrono(d),
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
          color: ColorDelTexto.TEXTOBOTONBLANCO,
          estiloTexto: EstilosDelTexto.REGULAR,
          enMayusculas: true
        }
      )
    })

    return data
  }

  gestionarPerfil(tipoPerfil: CatalogoTipoPerfilModel) {
    this.tipoPerfil = tipoPerfil;
    if (this.perfilNegocio.conflictoCrearPerfil(tipoPerfil, this.listaTipoPerfil)) {
      if (tipoPerfil.codigo === CodigosCatalogoTipoPerfil.GROUP) {

        this.toast.abrirToast('text15.0')
      } else {
        this.toast.abrirToast('text15')
      }
    } else {
      this.navegarCrearPerfil(tipoPerfil);
    }
  }

  limpiarPerfiles(tipoPerfil: CatalogoTipoPerfilModel) {
    this.perfilNegocio.limpiarPerfiles(this.listaTipoPerfil);
    this.dialogoServicie.close(this.idPerfilIncompatibleDialogo)
    this.navegarCrearPerfil(tipoPerfil);
  }

  navegarCrearPerfil(tipoPerfil: CatalogoTipoPerfilModel) {
    if (
      !tipoPerfil ||
      !tipoPerfil.codigo
    ) {
      return
    }

    this.enrutadorService.navegarConPosicionFija(
      {
        componente: RegistroComponent,
        params: [
          {
            nombre: 'accionEntidad',
            valor: AccionEntidad.REGISTRO
          },
          {
            nombre: 'codigoTipoPerfil',
            valor: tipoPerfil.codigo
          }
        ]
      },
      this.informacionEnrutador.posicion
    )
  }

  mostrarDescripcion(item: any) {
    if (this.itemSeleccionado === undefined) {
      this.itemSeleccionado = item
    }

    if (this.itemSeleccionado.codigo === item.codigo) {
      const elemento: HTMLElement = document.getElementById('flecha' + item.codigo) as HTMLElement;
      if (item.mostrarDescripcion) {
        item.mostrarDescripcion = false;
        this.itemSeleccionado.mostrarDescripcion = false;
        elemento.classList.remove("rotar-flecha")
      } else {
        item.mostrarDescripcion = true;
        this.itemSeleccionado.mostrarDescripcion = true;
        elemento.classList.add("rotar-flecha");
      }
    }

    if (this.itemSeleccionado.codigo !== item.codigo) {
      if (this.itemSeleccionado.mostrarDescripcion) {
        const elemento: HTMLElement = document.getElementById('flecha' + this.itemSeleccionado.codigo) as HTMLElement;
        if (this.itemSeleccionado.mostrarDescripcion) {
          this.itemSeleccionado.mostrarDescripcion = false;
          elemento.classList.remove("rotar-flecha")
        } else {
          this.itemSeleccionado.mostrarDescripcion = true;
          elemento.classList.add("rotar-flecha");
        }
      }

      const elemento2: HTMLElement = document.getElementById('flecha' + item.codigo) as HTMLElement;
      if (item.mostrarDescripcion) {
        item.mostrarDescripcion = false;
        elemento2.classList.remove("rotar-flecha")
      } else {
        item.mostrarDescripcion = true;
        elemento2.classList.add("rotar-flecha");
      }
      this.itemSeleccionado = item
    }
  }

  prepareItemInstrucciones(): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_NUEVO_INFO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
      mostrarDescripcion: false,
      tipoMenu: TipoMenu.INSTRUCCIONES,
      texto1: "m2v1texto2",
      texto2: 'm2v1texto3',
      texto3: 'm2v1texto4',
      descripcion: null,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6386,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: false
        }
      },
      gazeAnuncios: false,
      idInterno: "",
      onclick: () => { },
      dobleClick: () => { }
    };
  }

  prepareItemInformacion(informacion: InformacionModel): ItemMenuCompartido {
    try {
      return {
        id: '',
        tamano: TamanoItemMenu.ITEMMENUCREARPERFIL, // Indica el tamano del item (altura)
        colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
        mostrarDescripcion: informacion.mostrarDescripcion ?? false,
        tipoMenu: TipoMenu.ACCION,
        texto1: 'm2v1texto22',
        descripcion: [
          {
            texto: informacion.descripcion[0],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[1],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOAMARILLOBASE,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[2],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[3],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[4],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[5],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[6],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
          {
            texto: informacion.descripcion[7],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true
          },
        ],
        linea: {
          mostrar: true,
          configuracion: {
            ancho: AnchoLineaItem.ANCHO6382,
            espesor: EspesorLineaItem.ESPESOR071,
            colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
            forzarAlFinal: false
          }
        },
        gazeAnuncios: false,
        idInterno: informacion.codigo,
        onclick: () => this.mostrarDescripcion(informacion),
        dobleClick: () => { }

      };
    } catch (error) {

    }

  }

  async configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.SOLO_TITULO,
      
      accionAtras: () => this.volverAtras(),
      tituloAppbar: {
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        tituloPrincipal: {
          mostrar: true,
          llaveTexto: 'm2v1texto1'
        },
        mostrarLineaVerde: true,
        mostrarDivBack: true
      }
    }
  }

  volverAtras() {
    this.metodosSessionStorageService.eliminarSessionStorage()
    this._location.back()
  }

  async prepararInfoTipoPerfiles() {
    this.itemInformacion = {
      codigo: "info",
      nombre: await this.internacionalizacionNegocio.obtenerTextoLlave('texto255'),
      descripcion: [
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto1'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto2'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto3'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto4'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto5'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto6'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto7'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto8'),
      ],
    }
  }

  configurarBotonAceptar() {
    this.confBotonPerfilIncopatible = {
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      text: "text14",
      ejecutar: () => { this.confDialogoPerfilIncompatible.abierto = false },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO
    }
  }

  aceptarTerminosCondicionesMenorEdad() {
    // if (this.menorEdadForm.value.nombreResposanble.length >= 1 || this.menorEdadForm.value.nombreResposanble.length >= 1) {
    //   if (this.menorEdadForm.valid) {
    //     this.confModalTerminosCondiciones.abierto = false;
    //     this.cuentaNegocio.guardarAceptacionMenorEdad
    //       (
    //         this.menorEdadForm.value.correoResponsable,
    //         this.menorEdadForm.value.nombreResposanble,
    //         new Date()
    //       );
    //   }
    //   this.confInputNombresResponsable.error = true;
    //   this.confInputCorreoResponsable.error = true;
    // } else {
    //   this.confModalTerminosCondiciones.abierto = false;
    //   this.cuentaNegocio.aceptoTerminosCondiciones();
    // }
    this.confModalTerminosCondiciones.abierto = false;
    this.cuentaNegocio.aceptoTerminosCondiciones();

  }

  configurarDialogoContenido() {
    this.confDialogoPerfilIncompatible = {
      titulo: "text15",
      abierto: false,
      bloqueado: true,
      id: "perfil-incompatible"
    }
  }

  async iniciarFormMenorEdad() {
    this.menorEdadForm = this.formBuilder.group({
      //fechaNacimiento: ['', [Validators.required]],
      nombreResposanble: ['', [Validators.minLength(5)]],
      correoResponsable: ['', [Validators.email, Validators.minLength(3)]],
    });
    // this.inputFechaNacimiento = { tipo: 'date', error: false, estilo: { estiloError: EstiloErrorInput.ROJO, estiloInput: EstiloInput.DEFECTO }, placeholder: 'Tu fecha de nacimiento', data: this.menorEdadForm.controls.fechaNacimiento }
    this.confInputNombresResponsable = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text28',
      data: this.menorEdadForm.controls.nombreResposanble,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    }
    this.confInputCorreoResponsable = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text29',
      data: this.menorEdadForm.controls.correoResponsable,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    }
  }

  prepararModalTerminosCondiciones() {
    this.confModalTerminosCondiciones = {
      abierto: true,
      bloqueado: true,
      id: "modal-terms"
    }
  }

  obtenerEstadoPerfil(perfil: PerfilModel) {
    if (perfil) {
      switch (perfil.estado.codigo) {
        case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO || CodigosCatalogosEstadoPerfiles.PERFIL_CREADO:
          return "m2v10texto4"
        case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
          return "m2v10texto10"
      }
    }
    return "m2v1texto5";
  }

  obtenerColorPerfil(perfil: PerfilModel) {
    if (perfil) {
      // return ColorFondoItemMenu.PERFILHIBERNADO; //codigo temporal
      switch (perfil.estado.codigo) {
        case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO || CodigosCatalogosEstadoPerfiles.PERFIL_CREADO:
          return ColorFondoItemMenu.PERFILCREADO
        case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
          return ColorFondoItemMenu.PERFILHIBERNADO

      }
    }
    return ColorFondoItemMenu.PREDETERMINADO
  }

  preperarListaMenuTipoPerfil() {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoTipoPerfil,
      lista: this.listaTipoPerfil,
      tamanoLista: TamanoLista.TIPO_PERFILES
    }
  }

  configurarToast() {
    this.confToast = {
      texto: '',
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: true,
      intervalo: 3,
    }
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
  checkValue(event: any){
		
	}
}
