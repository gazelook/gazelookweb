export enum RutasNoticias {
    PUBLICAR = 'publicar',
    ACTUALIZAR = 'actualizar/:id',
    VISITAR = 'visitar/:id',
    NOTICIAS_USUARIOS = 'ver',
}