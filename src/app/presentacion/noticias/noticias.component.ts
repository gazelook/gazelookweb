import { Component, OnInit } from '@angular/core';
import { NotificacionesDeUsuario } from '@core/servicios/generales';
@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss']
})
export class NoticiasComponent implements OnInit {

  constructor(
    private notificacionesUsuario: NotificacionesDeUsuario
  ) { }

  ngOnInit(): void {
    this.notificacionesUsuario.validarEstadoDeLaSesion(false)
  }
}
