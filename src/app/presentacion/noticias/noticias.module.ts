import { CompartidoModule } from './../../compartido/compartido.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoticiasRoutingModule } from './noticias-routing.module';
import { NoticiasComponent } from './noticias.component';
import { PublicarComponent } from './publicar/publicar.component';
import { NoticiasUsuariosComponent } from './noticias-usuarios/noticias-usuarios.component';


@NgModule({
  declarations: [
    NoticiasComponent,
    PublicarComponent,
    NoticiasUsuariosComponent
  ],
  imports: [
    FormsModule,
    TranslateModule,
    CommonModule,
    CompartidoModule,
    NoticiasRoutingModule
  ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class NoticiasModule { }
