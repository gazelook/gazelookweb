import { Component, OnInit, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { NoticiaService, VariablesGlobales } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { AccionEntidad, CodigosCatalogoEntidad, CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos';
import { ColorTextoBoton, TipoBoton, ToastComponent } from '@shared/componentes';
import {
  AnchoLineaItem, ColorDeBorde, ColorDeFondo,
  ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, TamanoDeTextoConInterlineado, UsoAppBar, UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {
  BotonCompartido, ConfiguracionAppbarCompartida,
  ConfiguracionToast, CongifuracionItemProyectosNoticias, LineaCompartida, ModoBusqueda
} from '@shared/diseno/modelos';
import {
  AlbumNegocio, NoticiaNegocio, PerfilNegocio
} from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import { AlbumModel, NoticiaModel, PerfilModel } from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { PublicarComponent } from '../publicar/publicar.component';
import { MenuPrincipalComponent } from './../../menu-principal/menu-principal.component';

@Component({
  selector: 'app-noticias-usuarios',
  templateUrl: './noticias-usuarios.component.html',
  styleUrls: ['./noticias-usuarios.component.scss']
})
export class NoticiasUsuariosComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent

  // Configuracion de capas
  public mostrarCapaLoader: boolean
  public mostrarCapaError: boolean
  public mensajeCapaError: string
  public mostrarCapaNormal: boolean
  public puedeCargarMas: boolean
  public FiltroBusquedaEnum = FiltroBusqueda

  // Parametros internos
  public fechaInicial: Date
  public fechaFinal: Date
  public filtroActivoDeBusqueda: FiltroBusqueda
  public perfilSeleccionado: PerfilModel
  public idCapaCuerpo: string
  public currentDay: string
  public listaNoticiaModel: Array<NoticiaModel>
  public listaNoticiasModelFiltroOtro: Array<NoticiaModel>

  // Configuracion hijos
  public confToast: ConfiguracionToast
  public confAppbar: ConfiguracionAppbarCompartida
  public confBotonRecientes: BotonCompartido
  public confBotonMasVotados: BotonCompartido
  public confLineaNoticias: LineaCompartida

  public listaNoticias: PaginacionModel<CongifuracionItemProyectosNoticias>
  public confResultadoBusqueda: Array<CongifuracionItemProyectosNoticias>

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private noticiaNegocio: NoticiaNegocio,
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private variablesGlobales: VariablesGlobales,
    private enrutadorService: EnrutadorService
  ) {
    this.mostrarCapaLoader = false
    this.mostrarCapaError = false
    this.puedeCargarMas = true
    this.mensajeCapaError = ''
    this.fechaInicial = new Date('2021-01-02')
    this.fechaFinal = new Date()
    this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES
    this.idCapaCuerpo = 'capa-cuerpo-noticias'
    this.confResultadoBusqueda = []

    this.listaNoticiaModel = []
    this.listaNoticiasModelFiltroOtro = []
    this.currentDay = new Date().toISOString().slice(0, 10);
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = false
    this.inicializarPerfilSeleccionado()

    if (this.perfilSeleccionado) {
      this.configurarToast()
      this.configurarAppBar()
      this.configurarBotones()
      this.configurarLineas()
      this.ejecutarBusqueda()
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  obtenerFechaParaParametro(fecha: Date) {
    const dia = (fecha.getDate() < 10) ? '0' + fecha.getDate() : fecha.getDate()
    const mes = (fecha.getMonth() + 1 < 10) ? '0' + (fecha.getMonth() + 1) : fecha.getMonth() + 1
    return fecha.getFullYear() + '-' + mes + '-' + dia
  }

  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false, //True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    }
  }

  configurarAppBar() {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [
            ]
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            }
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v12texto3'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.NOTICIA,
            placeholder: 'm4v12texto2',
            valorBusqueda: '',
            posicion: this.informacionEnrutador.posicion
          }
        }
      },
    }
  }

  configurarBotones() {
    this.confBotonRecientes = {
      text: 'm4v12texto4',
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES
        this.ejecutarBusqueda()
      }
    }

    this.confBotonMasVotados = {
      text: 'm4v12texto5',
      colorTexto: ColorTextoBoton.ROJO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.filtroActivoDeBusqueda = FiltroBusqueda.MAS_VOTADAS
        this.ejecutarBusqueda()
      }
    }
  }

  configurarLineas() {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    }
  }

  inicializarListaNoticias() {
    this.listaNoticiaModel = []

    this.listaNoticias = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    }
  }

  tapEnInputFecha(id: string) {
    const elemento: HTMLElement = document.getElementById(id) as HTMLElement
    if (elemento) {
      elemento.click()
    }
  }

  async obtenerNoticiasSinFiltroFechas(
    perfil: string,
  ) {
    if (!this.listaNoticias.proximaPagina) {
      this.puedeCargarMas = true
      return
    }

    try {
      this.mostrarCapaError = false
      this.mostrarCapaLoader = (this.listaNoticias.lista.length === 0)

      const dataPaginacion = await this.noticiaNegocio.buscarNoticiasSinfiltroFechas(
        20,
        this.listaNoticias.paginaActual,
        perfil
      ).toPromise()

      this.listaNoticias.totalDatos = dataPaginacion.totalDatos
      this.listaNoticias.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(noticia => {
        this.listaNoticiaModel.push(noticia)
        this.listaNoticias.lista.push(this.configurarItemListaNoticias(noticia))
      })
      this.mostrarCapaLoader = false
      this.puedeCargarMas = true

      if (this.listaNoticias.proximaPagina) {
        this.listaNoticias.paginaActual += 1
      }
    } catch (error) {
      this.mostrarCapaLoader = false
      this.puedeCargarMas = false
      this.mostrarCapaError = true
    }
  }

  async obtenerNoticiasPorFechaFiltro(
    fechaInicial: string,
    fechaFinal: string,
    perfil: string,
    filtro: string
  ) {
    if (!this.listaNoticias.proximaPagina) {
      this.puedeCargarMas = true
      return
    }

    try {
      this.mostrarCapaError = false
      this.mostrarCapaLoader = (this.listaNoticias.lista.length === 0)
      const dataPaginacion = await this.noticiaNegocio.buscarNoticiasPorFechas(
        16,
        this.listaNoticias.paginaActual,
        fechaInicial,
        fechaFinal,
        filtro,
        perfil
      ).toPromise()

      this.listaNoticias.totalDatos = dataPaginacion.totalDatos
      this.listaNoticias.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(noticia => {
        this.listaNoticiaModel.push(noticia)
        this.listaNoticias.lista.push(this.configurarItemListaNoticias(noticia))
      })
      this.mostrarCapaLoader = false
      this.puedeCargarMas = true

      if (this.listaNoticias.proximaPagina) {
        this.listaNoticias.paginaActual += 1
      }
    } catch (error) {
      this.mostrarCapaLoader = false
      this.puedeCargarMas = false
      this.mostrarCapaError = true
    }
  }

  configurarItemListaNoticias(noticia: NoticiaModel): CongifuracionItemProyectosNoticias {

    const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(noticia.adjuntos)

    let a = (album && album.portada) ? album.portada.miniatura?.url || album.portada.principal?.url || '' : ''

    return {
      id: noticia.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(noticia.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: noticia.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: noticia.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.miniatura?.url || album.portada.principal?.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.noticiaNegocio.removerNoticiaActivaDelSessionStorage()

          if (!data.id) {
            return
          }

          const noticia: NoticiaModel = this.obtenerNoticiaDeLista(data.id)

          if (
            !this.perfilSeleccionado ||
            !noticia ||
            noticia === null
          ) {
            return
          }

          let accionEntidad = AccionEntidad.VISITAR

          if (
            noticia &&
            noticia !== null &&
            this.perfilSeleccionado._id === noticia.perfil._id
          ) {
            accionEntidad = AccionEntidad.ACTUALIZAR
          }

          this.enrutadorService.navegar(
            {
              componente: PublicarComponent,
              params: [
                {
                  nombre: 'id',
                  valor: data.id
                },
                {
                  nombre: 'accionEntidad',
                  valor: accionEntidad
                }
              ]
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id'
              }
            }
          )
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  obtenerNoticiaDeLista(id: string): NoticiaModel {
    const index: number = this.listaNoticiaModel.findIndex(e => e.id === id)

    if (index >= 0) {
      return this.listaNoticiaModel[index]
    }

    return null
  }

  scroolEnCapaCuerpo() {
    if (!this.puedeCargarMas) {
      return
    }

    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false

      if (this.filtroActivoDeBusqueda === FiltroBusqueda.RECIENTES) {
        this.obtenerNoticiasSinFiltroFechas(
          this.perfilSeleccionado._id,
        )
        return
      }

      this.obtenerNoticiasPorFechaFiltro(
        this.obtenerFechaParaParametro(this.fechaInicial),
        this.obtenerFechaParaParametro(this.fechaFinal),
        this.perfilSeleccionado._id,
        this.filtroActivoDeBusqueda
      )

    }
  }

  validarFechasIngresadas() {
    if (this.fechaInicial.getTime() > this.fechaFinal.getTime()) {
      this.toast.abrirToast('text52')
      return false
    }

    return true
  }

  ejecutarBusqueda() {
    if (this.mostrarCapaLoader) {
      return
    }

    if (
      this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS &&
      !this.validarFechasIngresadas()
    ) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES
      return
    }

    this.inicializarListaNoticias()

    if (this.filtroActivoDeBusqueda === FiltroBusqueda.RECIENTES) {
      this.obtenerNoticiasSinFiltroFechas(
        this.perfilSeleccionado._id,
      )
      return
    }

    this.obtenerNoticiasPorFechaFiltro(
      this.obtenerFechaParaParametro(this.fechaInicial),
      this.obtenerFechaParaParametro(this.fechaFinal),
      this.perfilSeleccionado._id,
      this.filtroActivoDeBusqueda
    )
  }

  cambioDeFecha(orden: number) {
    if (orden === 0) {
      let fechaInicial = this.fechaInicial
			this.fechaInicial = new Date(this.obtenerFechaSeleccionada(fechaInicial))    }

    if (orden === 1) {
      let fechaFinal = this.fechaFinal
			this.fechaFinal = new Date(this.obtenerFechaSeleccionada(fechaFinal))
    }

    this.filtroActivoDeBusqueda = FiltroBusqueda.RANGO_FECHAS
    this.ejecutarBusqueda()

  }
  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }

  obtenerFechaSeleccionada(fecha): string {
		// let date = new Date(fecha);
		// let anio = date.getFullYear().toString();
		// let mes_temporal = date.getMonth() + 1;
		// let dia_temporal = date.getDate() + 1
		// var mes = String(mes_temporal.toString()).padStart(2, '0');
		// let dia = String(dia_temporal.toString()).padStart(2, '0');

		// let fechaSeleccionada = anio + '/' + mes + '/' + dia;

		// return fechaSeleccionada
    let fechaAServer1 =  fecha.replace('-','/')
    let fechaAServer2 =  fechaAServer1.replace('-','/')
return fechaAServer2
	}



}

export enum FiltroBusqueda {
  RECIENTES = 'none',
  RANGO_FECHAS = 'fecha',
  MAS_VOTADAS = 'voto'
}
