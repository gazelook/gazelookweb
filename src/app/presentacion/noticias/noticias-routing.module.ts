import { NoticiasUsuariosComponent } from './noticias-usuarios/noticias-usuarios.component';
import { PublicarComponent } from './publicar/publicar.component';
import { RutasNoticias } from './rutas-noticias.enum';
import { NoticiasComponent } from './noticias.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: NoticiasComponent,
    children: [
      {
        path: RutasNoticias.PUBLICAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.ACTUALIZAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.VISITAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.NOTICIAS_USUARIOS.toString(),
        component: NoticiasUsuariosComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoticiasRoutingModule { }
