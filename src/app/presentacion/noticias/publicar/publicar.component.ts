import { MenuPrincipalComponent } from './../../menu-principal/menu-principal.component';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId, ImagenPantallaCompletaService, MetodosParaFotos, NoticiaService } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
	AccionEntidad, CatalogoTipoMensaje, CodigoEstadoParticipanteAsociacion,
	CodigosCatalogoArchivosPorDefecto, CodigosCatalogoEntidad, CodigosCatalogoEstadoAlbum, CodigosCatalogoTipoAlbum, CodigosCatalogoTipoMedia, CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda';
import { FuncionesCompartidas } from '@core/util';
import { TranslateService } from '@ngx-translate/core';
import {
	ColorIconoBoton, ColorTextoBoton, PortadaExpandidaComponent,
	TipoBoton, ToastComponent
} from '@shared/componentes';
import {
	ColorDeBorde, ColorDeFondo, ColorDelTexto, EstilosDelTexto,
	TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista, TipoDialogo, UsoAppBar, UsoItemCircular, UsoItemListaContacto
} from '@shared/diseno/enums';
import {
	BarraBusqueda, BloquePortada, BotonCompartido,
	ColoresBloquePortada, ConfiguracionAppbarCompartida, ConfiguracionDone,
	ConfiguracionItemListaContactosCompartido, ConfiguracionListaContactoCompartido, ConfiguracionPortadaExandida,
	ConfiguracionToast, ConfiguracionVotarEntidad, DialogoCompartido, InputCompartido, ItemDialogoHorizontal, ModoBusqueda, SombraBloque, TipoBloqueBortada
} from '@shared/diseno/modelos';
import {
	AlbumNegocio, MediaNegocio, NoticiaNegocio,
	ParticipanteAsociacionNegocio, PerfilNegocio
} from 'dominio/logica-negocio';
import { PaginacionModel, SubirArchivoData } from 'dominio/modelo';
import {
	AlbumModel, ArchivoModel, MediaModel, NoticiaModel,
	ParticipanteAsociacionModel, PerfilModel, VotoNoticiaModel
} from 'dominio/modelo/entidades';
import { ProyectoParams } from 'dominio/modelo/parametros';
import { AlbumAudiosComponent } from '../../album/album-audios/album-audios.component';
import { AlbumGeneralComponent } from '../../album/album-general/album-general.component';
import { AlbumLinksComponent } from '../../album/album-links/album-links.component';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { ChatMetodosCompartidosService } from '../../gazing/chat-metodos-comunes.service';
import { InputKey, Inputs, OrigenValidacionDeCambios } from '../../proyectos/publicar/publicar.component';







@Component({
	selector: 'app-publicar',
	templateUrl: './publicar.component.html',
	styleUrls: ['./publicar.component.scss']
})
export class PublicarComponent implements OnInit, Enrutador {
	@ViewChild('toast', { static: false }) toast: ToastComponent
	@ViewChild('portadaExpandida', { static: false }) portadaExpandida: PortadaExpandidaComponent

	public util = FuncionesCompartidas
	public AccionEntidadEnum = AccionEntidad
	public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum
	public InputKeyEnum = InputKey
	public imagenesDefecto: Array<ArchivoModel>
	public portadaUrl: string
	public idCapaFormulario: string
	private tituloOriginal: string
	private tituloCortoOriginal: string
	private descripcionOriginal: string
	private direccionOriginal: string
	private esOriginal: boolean

	// Parametros de la url
	public params: ProyectoParams

	// Configuracion de capas
	public mostrarCapaLoader: boolean
	public mostrarCapaError: boolean
	public mensajeCapaError: string
	public mostrarCapaNormal: boolean

	// Parametros internos
	public perfilSeleccionado: PerfilModel
	public noticia: NoticiaModel
	public noticiaForm: FormGroup
	public inputsForm: Array<Inputs>
	public paraCompartir: boolean
	public inputArticuloNoticia: string
	public idPerfilCoautorParaResponder: string
	public idInternoParaResponder: string
	public llaveTextoBotonesExtra: string
	public listaContactos: PaginacionModel<ParticipanteAsociacionModel>
	public listaContactosSeleccionados: Array<string>
	public listaContactosOriginal: Array<ConfiguracionItemListaContactosCompartido>
	public listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido>
	public origenValidacionCambios: OrigenValidacionDeCambios

	// Configuraciones
	public confToast: ConfiguracionToast
	public confAppbar: ConfiguracionAppbarCompartida
	public confPortadaExpandida: ConfiguracionPortadaExandida
	public confBotonCompartir: BotonCompartido
	public confBotonArticulo: BotonCompartido
	public confBotonPublish: BotonCompartido
	public confBotonLinks: BotonCompartido
	public confBotonPhotos: BotonCompartido
	public confBotonEliminar: BotonCompartido
	public confVotarEntidad: ConfiguracionVotarEntidad
	public confBotonesExtras: BotonCompartido[]
	public confDialogoEliminarNoticia: DialogoCompartido
	public confListaContactoCompartido: ConfiguracionListaContactoCompartido
	public confDialogoSalida: DialogoCompartido
	public confDone: ConfiguracionDone
	public confDialogoFullNoticia: DialogoCompartido
	public confDialogoEliminarPDF: DialogoCompartido

	public infoPropietarioNoticia: ConfiguracionItemListaContactosCompartido


	// Enrutamiento
	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		public noticiaService: NoticiaService,
		private noticiaNegocio: NoticiaNegocio,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location,
		private perfilNegocio: PerfilNegocio,
		private albumNegocio: AlbumNegocio,
		private mediaNegocio: MediaNegocio,
		private translateService: TranslateService,
		private generadorId: GeneradorId,
		private participanteAsociacionNegocio: ParticipanteAsociacionNegocio,
		private chatMetodosCompartidosService: ChatMetodosCompartidosService,
		private db: AngularFireDatabase,
		private metodosParaFotos: MetodosParaFotos,
		private enrutadorService: EnrutadorService,
		private imagenPantallaCompletaService: ImagenPantallaCompletaService
	) {
		this.params = { estado: false }
		this.mostrarCapaLoader = false
		this.mostrarCapaError = false
		this.mensajeCapaError = ''
		this.inputArticuloNoticia = 'input-file-pdf'
		this.imagenesDefecto = []
		this.confBotonesExtras = []
		this.llaveTextoBotonesExtra = ''
		this.listaContactosSeleccionados = []
		this.listaContactosBuscador = []
		this.listaContactosOriginal = []
		this.esOriginal = false
	}

	ngOnInit(): void {
		this.imagenPantallaCompletaService.configurarPortadaExp()
		this.configurarParametrosDeLaUrl()
		this.inicializarPerfilSeleccionado()
		this.inicializarDataListaContactos()
		if (this.params.estado && this.perfilSeleccionado) {
			// Core
			this.inicializarDataDeLaEntidad()
			// Componentes hijos
			this.configurarToast()
			this.configurarAppBar(-1)
			this.configurarDialogoEliminarPDF()
			this.configurarDialogoEliminarNoticia()
			this.configurarListaContactoCompartido()
			this.configurarDone()
			this.configurarDialogoConfirmarSalida()
			this.configurarDialogoFullNoticia()
			return
		}

		this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
	}

	reintentar() {

	}

	configurarParametrosDeLaUrl() {
		if (
			!this.informacionEnrutador ||
			!this.informacionEnrutador.params
		) {
			this.params.estado = false
			return
		}

		this.informacionEnrutador.params.forEach(item => {
			if (item.nombre === 'id') {
				this.params.id = item.valor
			}

			if (item.nombre === 'accionEntidad') {
				this.params.accionEntidad = item.valor
			}
		})

		this.params = this.noticiaService.validarParametrosSegunAccionEntidadEnrutador(this.params)
	}

	inicializarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	async inicializarImagenesPorDefecto() {
		try {
			const archivos = this.mediaNegocio.obtenerArchivosDefaultPorTipo(
				CodigosCatalogoArchivosPorDefecto.PROYECTOS
			)

			if (!archivos || archivos === null) {
				throw new Error('')
			}

			this.imagenesDefecto = archivos
			this.definirUrlMediaDeLaPortada()
		} catch (error) {
			this.imagenesDefecto = []
			this.definirUrlMediaDeLaPortada()
		}
	}

	definirUrlMediaDeLaPortada() {
		if (!this.noticia || !this.confPortadaExpandida) {
			return
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(this.noticia.adjuntos)
		const data = this.noticiaService.determinarUrlImagenPortada(album, this.imagenesDefecto)
		this.confPortadaExpandida.urlMedia = data.url

		this.confPortadaExpandida.bloques.forEach((bloque, pos) => {
			if (pos === 0) {
				bloque.colorFondo = (data.porDefecto) ? ColoresBloquePortada.AZUL_FUERTE : ColoresBloquePortada.AZUL_FUERTE_CON_OPACIDAD
			}
		})
	}

	inicializarDataDeLaEntidad() {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.inicializarContenidoParaAccionCrear()
				break
			case AccionEntidad.ACTUALIZAR:
				this.inicializarDataParaAccionActualizar()
				break
			case AccionEntidad.VISITAR:
				this.inicializarDataParaAccionVisitar()
				break
			default: break;
		}
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false, //True para mostrar
			mostrarLoader: false, // true para mostrar cargando en el toast
			cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
		}
	}

	async configurarAppBar(
		estatusSubTitulo: number,
		buscador?: BarraBusqueda,
	) {

		let subtitulo: string = this.noticiaService.obtenerSubtituloAppBarSegunAccionEntidadParaNoticia(this.params)

		if (estatusSubTitulo === 1) {
			subtitulo = 'm4v3texto10'
		}

		this.confAppbar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			botonRefresh: true,
			eventoRefresh: () => {
				this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
			},
			accionAtras: () => {
				this.accionAtras()
			},
			eventoHome: () => {
				if (
					(
						this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
						this.params.accionEntidad === AccionEntidad.CREAR
					) &&
					this.noticia &&
					this.noticia.perfil &&
					this.noticia.perfil._id === this.perfilSeleccionado._id &&
					this.validarSiExistenCambios()
				) {
					this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_HOME
					this.configurarDialogoConfirmarSalida(true)
					return
				}

				this.navegarAlHome(false)
			},
			searchBarAppBar: {
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
						this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
					)
				},
				mostrarTextoHome: true,
				mostrarDivBack: {
					icono: true,
					texto: true,
				},
				mostrarLineaVerde: true,
				subtitulo: {
					mostrar: true,
					llaveTexto: subtitulo
				},
				buscador: (buscador) ? buscador : {
					mostrar: false,
					configuracion: {
						disable: true,
					}
				},
				idiomaOriginal: {
					mostrarOriginal: false,
					llaveTexto: 'm4v1texto2',
					clickMostrarOriginal: () => {
						if (this.esOriginal) {
							this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto = 'm4v1texto2'
							this.noticia.tituloCorto = this.tituloCortoOriginal
							this.noticia.titulo = this.tituloOriginal
							this.noticia.descripcion = this.descripcionOriginal
							this.noticia.direccion.descripcion = this.direccionOriginal
							this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
							this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm, true)
							this.esOriginal = false
							return
						}

						if (!this.esOriginal) {
							this.esOriginal = true
							this.tituloOriginal = this.noticia.titulo
							this.tituloCortoOriginal = this.noticia.tituloCorto
							this.descripcionOriginal = this.noticia.descripcion
							this.direccionOriginal = this.noticia.direccion?.descripcion

							if (
								this.noticia.tituloCortoOriginal &&
								this.noticia.tituloOriginal
							) {
								this.noticia.tituloCorto = this.noticia.tituloCortoOriginal
								this.noticia.titulo = this.noticia.tituloOriginal


								if (this.noticia.descripcionOriginal) {
									this.noticia.descripcion = this.noticia.descripcionOriginal
								}
								if (this.noticia.direccion.descripcionOriginal) {
									this.noticia.direccion.descripcion = this.noticia.direccion.descripcionOriginal
								}
							}

							this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
							this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm, true)
							this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto = 'm4v1texto2.1'
							return
						}


					}
				}
			},
		}
	}

	accionAtras() {
		if (this.esOriginal) {
			this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto = 'm4v1texto2'
			this.noticia.tituloCorto = this.tituloCortoOriginal
			this.noticia.titulo = this.tituloOriginal
			this.noticia.descripcion = this.descripcionOriginal
			this.noticia.direccion.descripcion = this.direccionOriginal
			this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
			this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm, true)
			this.esOriginal = false
			return
		}

		if (
			this.params.accionEntidad !== AccionEntidad.REGISTRO &&
			this.confListaContactoCompartido && this.confListaContactoCompartido.mostrar
		) {
			this.confListaContactoCompartido.mostrar = false
			this.confListaContactoCompartido.botonAccion.mostrarDialogo = false
			this.confListaContactoCompartido.listaContactos.lista = []
			this.inicializarDataListaContactos()
			this.configurarListaContactoCompartido()
			this.cambiarEstadoAppBarParaListaDeContactos(false)
			return
		}

		if (
			(
				this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
				this.params.accionEntidad === AccionEntidad.CREAR
			) &&
			this.noticia &&
			this.noticia.perfil &&
			this.noticia.perfil._id === this.perfilSeleccionado._id &&
			this.validarSiExistenCambios()
		) {
			this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_BACK
			this.configurarDialogoConfirmarSalida(true)
			return
		}

		this.navegarAlBack()
	}

	inicializarContenidoParaAccionCrear() {
		this.noticia = this.noticiaNegocio.validarNoticiaActivoSegunAccionCrear(
			this.perfilSeleccionado,
			this.informacionEnrutador.posicion
		)

		// Utils
		this.inicializarControlesSegunAccion()
		this.inicializarInputs()
		// Componentes hijos
		this.configurarPortadaExpandida()
		this.definirBotonesParaPortadaExpandida()
		this.configurarBotones()
		this.configurarBotonesExtras()
		this.inicializarImagenesPorDefecto()
	}

	async inicializarDataParaAccionActualizar() {
		try {
			this.mostrarCapaLoader = true
			this.noticia = await this.noticiaNegocio.obtenerInformacionDeLaNoticia(
				this.params.id,
				this.perfilSeleccionado._id
			).toPromise()

			this.noticiaNegocio.validarNoticiaActivaSegunAccionActualizarVisitar(
				this.noticia,
				this.informacionEnrutador.posicion
			)

			if (this.perfilSeleccionado && this.perfilSeleccionado._id !== this.noticia.perfil._id) {
				this.noticiaNegocio.removerNoticiaDeNoticiaActivaTercios(this.informacionEnrutador.posicion)
				throw new Error('text31')
			}

			// Utils
			this.inicializarControlesSegunAccion()
			this.inicializarInputs()
			// Componentes hijos
			this.configurarPortadaExpandida()
			this.definirBotonesParaPortadaExpandida()
			this.configurarBotones()
			this.configurarBotonesExtras()
			this.inicializarImagenesPorDefecto()
			this.mostrarCapaLoader = false
		} catch (error) {
			this.mensajeCapaError = 'text31'
			this.mostrarCapaLoader = false
			this.mostrarCapaError = true
		}
	}

	async inicializarDataParaAccionVisitar() {
		try {
			this.mostrarCapaLoader = true
			this.noticia = await this.noticiaNegocio.obtenerInformacionDeLaNoticia(
				this.params.id,
				this.perfilSeleccionado._id
			).toPromise()

			this.noticiaNegocio.validarNoticiaActivaSegunAccionActualizarVisitar(
				this.noticia,
				this.informacionEnrutador.posicion
			)

			// Utils
			this.configurarAppBar(-1)
			this.inicializarControlesSegunAccion()
			this.inicializarInputs()
			// Componentes hijos
			this.configurarPortadaExpandidaParaVisita()
			this.configurarBotones()
			this.configurarBotonesExtras()
			this.configurarVotarEntidad()
			this.inicializarImagenesPorDefecto()

			this.infoPropietarioNoticia = this.configuracionPropietarioNoticia()

			this.mostrarCapaLoader = false
			if (
				this.noticia.tituloCortoOriginal &&
				this.noticia.tituloOriginal
			) {
				this.confAppbar.searchBarAppBar.idiomaOriginal.mostrarOriginal = true
			}


		} catch (error) {
			this.mensajeCapaError = 'text31'
			this.mostrarCapaLoader = false
			this.mostrarCapaError = true
		}
	}

	inicializarControlesSegunAccion() {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
				break
			case AccionEntidad.ACTUALIZAR:
				this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
				break
			case AccionEntidad.VISITAR:
				this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
				break
			default: break;
		}
	}

	inicializarInputs() {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm)
				break
			case AccionEntidad.ACTUALIZAR:
				this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm)
				break
			case AccionEntidad.VISITAR:
				this.inputsForm = this.noticiaService.configurarInputsDelFormularioConKey(this.noticiaForm, true)
				break
			default: break;
		}
	}

	configurarPortadaExpandida() {
		this.confPortadaExpandida = {
			urlMedia: '',
			mostrarLoader: true,
			bloques: [
				{
					tipo: TipoBloqueBortada.BOTON_NOTICIA,
					colorFondo: ColoresBloquePortada.AZUL_FUERTE,
					conSombra: SombraBloque.SOMBRA_NEGRA,
					llaveTexto: 'm4v15texto2',
					botones: []
				}
			],
			botones: []
		}
	}

	configurarPortadaExpandidaParaVisita() {
		this.confPortadaExpandida = {
			urlMedia: '',
			mostrarLoader: false,
			bloques: [],
			eventoDobleTapPortada: {
				activarEvento: true,
				evento: () => {
					this.validarAccionDobleTapEnPortadaExpandida()
				}
			}
		}

		const albumPredeterminado: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
			this.noticia.adjuntos
		)

		const albumPhotos: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.LINK,
			this.noticia.adjuntos
		)

		const albumLinks: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.LINK,
			this.noticia.adjuntos
		)

		if (!albumPredeterminado) {
			if (!albumPhotos) {
				const bloquePhotos: BloquePortada = {
					colorFondo: ColoresBloquePortada.AZUL_DEBIL,
					llaveTexto: 'm4v3texto3',
					tipo: TipoBloqueBortada.NO_ADDED_INFO
				}
				this.confPortadaExpandida.bloques.push(bloquePhotos)
			}

			if (!albumLinks) {
				const bloqueLinks: BloquePortada = {
					colorFondo: ColoresBloquePortada.AZUL_DEBIL,
					llaveTexto: 'm4v3texto4',
					tipo: TipoBloqueBortada.NO_ADDED_INFO
				}
				this.confPortadaExpandida.bloques.push(bloqueLinks)
			}
		}

		if (
			albumPredeterminado &&
			albumPredeterminado.media &&
			albumPredeterminado.media.length === 0
		) {
			const bloque: BloquePortada = {
				colorFondo: ColoresBloquePortada.AZUL_DEBIL,
				llaveTexto: (albumPredeterminado.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) ? 'm4v3texto3' : 'm4v3texto4',
				tipo: TipoBloqueBortada.NO_ADDED_INFO
			}
			this.confPortadaExpandida.bloques.push(bloque)
		}

		if (this.noticia.actualizado) {
			this.confPortadaExpandida.bloques.push({
				tipo: TipoBloqueBortada.ACTUALUZADO_INFO,
				llaveTexto: 'm4v1texto4',
				colorFondo: ColoresBloquePortada.AMARILLO_BASE,
			})
		}
	}

	validarAccionDobleTapEnPortadaExpandida() {
		if (this.noticia && this.noticia.adjuntos) {
			const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
				this.noticia.adjuntos
			)

			if (!album) {
				return
			}

			if (album.media && album.media.length === 1) {
				if (!this.imagenPantallaCompletaService.confImagenPantallaCompleta) {
					this.toast.abrirToast('text37')
					return
				}

				// if (album.tipo && album.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) {


				// 	this.imagenPantallaCompletaService.configurarImagenPantallaCompleta(
				// 		true, true, this.confPortadaExpandida.urlMedia
				// 	)
				// 	return
				// }

				if (album.tipo && album.tipo.codigo === CodigosCatalogoTipoAlbum.LINK) {
					window.open(album.media[0].principal.url)
					return
				}

				// returnublica

			}

			if (album.tipo && album.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) {
				this.irAlAlbumGeneral(false)
				return
			}

			if (album.tipo && album.tipo.codigo === CodigosCatalogoTipoAlbum.LINK) {
				this.irAlAlbumDeLinks(false)
				return
			}
		}
	}



	definirBotonesParaPortadaExpandida() {
		if (this.noticia) {
			const albumGeneral: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.GENERAL,
				this.noticia.adjuntos
			)
			const albumLinks: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.LINK,
				this.noticia.adjuntos
			)

			if (
				this.params.accionEntidad !== AccionEntidad.VISITAR &&
				!albumGeneral &&
				!albumLinks
			) {
				this.insertarBotonesEnPortadaExpandida(true, true)
				this.definirTextoBloquePortada(true)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(null, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}

			if (
				this.params.accionEntidad !== AccionEntidad.VISITAR &&
				(
					(albumGeneral && !albumGeneral.predeterminado) ||
					(albumLinks && !albumLinks.predeterminado)
				)
			) {
				this.insertarBotonesEnPortadaExpandida(true, true)
				this.definirTextoBloquePortada(true)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(null, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}

			if (albumGeneral && albumGeneral.predeterminado) {
				this.insertarBotonesEnPortadaExpandida(true, false)
				this.definirTextoBloquePortada(false, true)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(albumGeneral, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}

			if (albumLinks && albumLinks.predeterminado) {
				this.insertarBotonesEnPortadaExpandida(false, true)
				this.definirTextoBloquePortada(false, false)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(null, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}
		}
	}

	insertarBotonesEnPortadaExpandida(
		usarBotonPhotos: boolean = false,
		usarBotonLinks: boolean = false
	) {
		const tamanoBloques = this.confPortadaExpandida.bloques.length - 1
		if (!(tamanoBloques >= 0)) {
			return
		}

		this.confPortadaExpandida.bloques[tamanoBloques].botones = []
		if (usarBotonLinks) {
			this.confPortadaExpandida.bloques[tamanoBloques].botones.push({
				text: 'm4v15texto3',
				enProgreso: false,
				tipoBoton: TipoBoton.TEXTO,
				colorTexto: ColorTextoBoton.VERDE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				ejecutar: () => {
					this.validarAccionAlbumPortada(
						CodigosCatalogoTipoAlbum.LINK
					)
				}
			})
		}

		if (usarBotonPhotos) {
			this.confPortadaExpandida.bloques[tamanoBloques].botones.push({
				text: 'm4v15texto4',
				enProgreso: false,
				tipoBoton: TipoBoton.TEXTO,
				colorTexto: ColorTextoBoton.CELESTE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				ejecutar: () => {
					this.validarAccionAlbumPortada(
						CodigosCatalogoTipoAlbum.GENERAL
					)
				}
			})
		}
	}

	definirTextoBloquePortada(
		general: boolean,
		fotos?: boolean,
	) {
		const tamanoBloques = this.confPortadaExpandida.bloques.length - 1
		if (!(tamanoBloques >= 0)) {
			return
		}

		if (general) {
			this.confPortadaExpandida.bloques[tamanoBloques].llaveTexto = 'm4v15texto2'
			return
		}

		if (fotos) {
			this.confPortadaExpandida.bloques[tamanoBloques].llaveTexto = 'm4v16texto2'
			return
		}

		if (!fotos) {
			this.confPortadaExpandida.bloques[tamanoBloques].llaveTexto = 'm4v16texto2'
			return
		}
	}

	validarAccionAlbumPortada(
		tipo: CodigosCatalogoTipoAlbum
	) {
		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			tipo,
			this.noticia.adjuntos
		)

		if (!album || (album && !album.predeterminado)) {
			this.definirAlbumSegunTipoComoPredeterminado(tipo)
		}
		const albumUpd: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			tipo,
			this.noticia.adjuntos
		)

		this.albumNegocio.guardarAlbumActivoDelTercio(
			this.informacionEnrutador.posicion,
			albumUpd
		)

		if (albumUpd.tipo.codigo === CodigosCatalogoTipoAlbum.LINK) {
			this.irAlAlbumDeLinks(true, albumUpd)
			return
		}

		if (albumUpd.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) {
			this.irAlAlbumGeneral(true, albumUpd)
			return
		}
	}

	definirAlbumSegunTipoComoPredeterminado(
		tipo: CodigosCatalogoTipoAlbum,
		guardarAlbumActivo: boolean = false
	) {
		const album = this.albumNegocio.validarAlbumEnNoticiaActiva(
			this.informacionEnrutador.posicion,
			tipo,
			guardarAlbumActivo,
			true
		)

		if (album) {
			this.noticia.adjuntos.push(album)
			this.definirBotonesParaPortadaExpandida()
			this.configurarBotonesExtras()
		}
	}

	async configurarBotones() {
		// Boton compartir
		this.confBotonCompartir = {
			text: 'm4v3texto10',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.validarAccionCompartirProyecto()
			}
		}
		// Boton articulo o proyecto completo
		this.confBotonArticulo = {
			text: 'm4v15texto16',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.eventoBotonArticulo()
			}
		}
		// Boton submit
		this.confBotonPublish = {
			text: 'm4v15texto17',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => this.eventoBotonPublish()
		}
		// Boton Eliminar
		this.confBotonEliminar = {
			text: 'm4v16texto9',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.confDialogoEliminarNoticia.mostrarDialogo = true
			}
		}
	}

	async configurarBotonesExtras() {
		this.confBotonesExtras = []
		const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(this.noticia.adjuntos)

		if (!album) {
			return
		}


		if (album.tipo.codigo === CodigosCatalogoTipoAlbum.LINK) {

			if (this.params.accionEntidad === AccionEntidad.VISITAR) {
				const albumExtra: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
					CodigosCatalogoTipoAlbum.GENERAL,
					this.noticia.adjuntos
				)

				if (!albumExtra) {
					return
				}
			}

			this.confBotonesExtras.push({
				text: 'PHOTOS',
				colorTexto: ColorTextoBoton.CELESTE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				tipoBoton: TipoBoton.TEXTO,
				enProgreso: false,
				ejecutar: () => {
					this.irAlAlbumGeneral(this.params.accionEntidad === AccionEntidad.ACTUALIZAR)
				}
			})

			this.confBotonesExtras[0].text = await this.translateService.get('m4v16texto6').toPromise()
			this.llaveTextoBotonesExtra = 'm4v16texto3'
			return
		}

		if (album.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) {

			if (this.params.accionEntidad === AccionEntidad.VISITAR) {
				const albumExtra: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
					CodigosCatalogoTipoAlbum.LINK,
					this.noticia.adjuntos
				)

				if (!albumExtra) {
					return
				}
			}

			this.confBotonesExtras.push({
				text: 'LINKS',
				colorTexto: ColorTextoBoton.VERDE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				tipoBoton: TipoBoton.TEXTO,
				enProgreso: false,
				ejecutar: () => {
					this.irAlAlbumDeLinks(this.params.accionEntidad === AccionEntidad.ACTUALIZAR)
				}
			})

			this.confBotonesExtras[0].text = await this.translateService.get('m4v16texto5').toPromise()
			this.llaveTextoBotonesExtra = 'm4v16texto4'
			return
		}
	}

	configurarDialogoEliminarNoticia() {
		this.confDialogoEliminarNoticia = {
			mostrarDialogo: false,
			completo: true,
			tipo: TipoDialogo.CONFIRMACION,
			descripcion: 'm4v16texto10',
			listaAcciones: [
				{
					text: 'm3v9texto2',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarNoticia.mostrarDialogo = false
						this.eliminarNoticia()
					},
				},
				{
					text: 'm3v9texto3',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarNoticia.mostrarDialogo = false
					},
				}
			]
		}
	}

	determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum) {
		if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
			return true
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			codigo,
			this.noticia.adjuntos
		)

		if (album) {
			return true
		}

		return false
	}

	configurarVotarEntidad() {
		this.confVotarEntidad = {
			id: this.noticia.id || '',
			entidad: CodigosCatalogoEntidad.NOTICIA,
			voto: this.noticia.voto,
			bloqueTitulo: {
				coloDeFondo: ColorDeFondo.FONDO_CELESTE_CON_OPACIDAD,
				llavesTexto: (!this.noticia.voto) ? [
					'm4v3texto11',
					'm4v3texto12'
				] : [
					'm4v3texto13',
					'm4v3texto14'
				]
			},
			bloqueBoton: {
				llaveTexto: 'm3v9texto2',
				activarEventoTap: !this.noticia.voto,
				eventoTap: () => {
					this.apoyarNoticia()
				}
			}
		}
	}

	async agregarMediaNoticia(files: FileList) {

		try {
			this.confBotonArticulo.enProgreso = true
			const dataApiArchivo: SubirArchivoData = {
				archivo: files[0],
				formato: 'application/pdf',
				catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
				descripcion: '',
				relacionAspecto: '1:1',
			}

			if (!this.noticia.medias) {
				this.noticia.medias = []
			}
			const media: MediaModel = await this.mediaNegocio.subirMedia(dataApiArchivo).toPromise()

			if (!media || media === null) {
				throw new Error('')
			}

			this.noticia.medias[0] = media
			this.confBotonArticulo.enProgreso = false

			const input = document.getElementById(this.inputArticuloNoticia) as HTMLInputElement
			if (input) {
				input.value = ''
			}
		} catch (error) {
			this.confBotonArticulo.enProgreso = false
			this.toast.abrirToast('text37')
		}
	}

	validarAlbumPredeterminado(tipo: CodigosCatalogoTipoAlbum) {
		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(tipo, this.noticia.adjuntos)

		if (!album) {
			return false
		}
		return !album.predeterminado
	}

	validarInformacionDelaNoticiaAntesDeRecargarOCambiarDePaginaHaciaDelante(
		eliminarData: boolean = false
	) {
		this.noticia = this.noticiaNegocio.asignarValoresDeLosCamposALaNoticia(
			this.params,
			this.noticia,
			this.noticiaForm,
			eliminarData,
			this.informacionEnrutador.posicion
		)
	}

	irAlAlbumDeLinks(
		validarCambios: boolean = true,
		albumParam?: AlbumModel
	) {
		const album: AlbumModel = (albumParam) ? albumParam : this.albumNegocio.validarAlbumEnNoticiaActiva(
			this.informacionEnrutador.posicion,
			CodigosCatalogoTipoAlbum.LINK,
			true
		)

		if (
			!album ||
			(
				this.params.accionEntidad === AccionEntidad.VISITAR &&
				album.estado && album.estado.codigo && album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR
			)
		) {
			return
		}

		if (
			validarCambios &&
			this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
			this.validarSiExistenCambios()
		) {
			this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_ALBUM_LINKS
			this.configurarDialogoConfirmarSalida(true)
			return
		}

		if (this.params.accionEntidad === AccionEntidad.CREAR) {
			this.validarInformacionDelaNoticiaAntesDeRecargarOCambiarDePaginaHaciaDelante()
		}

		let titulo = (this.noticia.tituloCorto.length > 0) ? this.noticia.tituloCorto : 'm3v10texto7'
		titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ')

		let accionEntidad: AccionEntidad

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			accionEntidad = AccionEntidad.VISITAR
		} else {
			accionEntidad = (!album._id) ? AccionEntidad.CREAR : AccionEntidad.ACTUALIZAR
		}

		this.enrutadorService.navegarConPosicionFija(
			{
				componente: AlbumLinksComponent,
				params: [
					{
						nombre: 'titulo',
						valor: titulo
					},
					{
						nombre: 'accionEntidad',
						valor: accionEntidad
					},
					{
						nombre: 'entidad',
						valor: CodigosCatalogoEntidad.NOTICIA
					}
				]
			},
			this.informacionEnrutador.posicion
		)
	}

	irAlAlbumGeneral(
		validarCambios: boolean = true,
		albumParam?: AlbumModel
	) {
		const album: AlbumModel = (albumParam) ? albumParam : this.albumNegocio.validarAlbumEnNoticiaActiva(
			this.informacionEnrutador.posicion,
			CodigosCatalogoTipoAlbum.GENERAL,
			true
		)

		if (
			!album ||
			(
				this.params.accionEntidad === AccionEntidad.VISITAR &&
				album.estado && album.estado.codigo && album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR
			)
		) {
			return
		}

		if (
			validarCambios &&
			this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
			this.validarSiExistenCambios()
		) {
			this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL
			this.configurarDialogoConfirmarSalida(true)
			return
		}

		if (this.params.accionEntidad === AccionEntidad.CREAR) {
			this.validarInformacionDelaNoticiaAntesDeRecargarOCambiarDePaginaHaciaDelante()
		}

		let titulo = (this.noticia.tituloCorto.length > 0) ? this.noticia.tituloCorto : 'm3v10texto7'
		titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ')

		let accionEntidad: AccionEntidad

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			accionEntidad = AccionEntidad.VISITAR
		} else {
			accionEntidad = (!album._id) ? AccionEntidad.CREAR : AccionEntidad.ACTUALIZAR
		}

		this.enrutadorService.navegarConPosicionFija(
			{
				componente: AlbumGeneralComponent,
				params: [
					{
						nombre: 'titulo',
						valor: titulo
					},
					{
						nombre: 'accionEntidad',
						valor: accionEntidad
					},
					{
						nombre: 'entidad',
						valor: CodigosCatalogoEntidad.NOTICIA
					}
				]
			},
			this.informacionEnrutador.posicion
		)
	}

	irAlAlbumAudios(
		validarCambios: boolean = true,
		albumParam?: AlbumModel
	) {
		const album: AlbumModel = (albumParam) ? albumParam : this.albumNegocio.validarAlbumEnNoticiaActiva(
			this.informacionEnrutador.posicion,
			CodigosCatalogoTipoAlbum.AUDIOS,
			true
		)

		if (
			!album ||
			(
				this.params.accionEntidad === AccionEntidad.VISITAR &&
				album.estado && album.estado.codigo && album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR
			)
		) {
			return
		}

		if (
			validarCambios &&
			this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
			this.validarSiExistenCambios()
		) {
			this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS
			this.configurarDialogoConfirmarSalida(true)
			return
		}

		if (this.params.accionEntidad === AccionEntidad.CREAR) {
			this.validarInformacionDelaNoticiaAntesDeRecargarOCambiarDePaginaHaciaDelante()
		}

		let titulo = (this.noticia.tituloCorto.length > 0) ? this.noticia.tituloCorto : 'm3v10texto7'
		titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ')

		let accionEntidad: AccionEntidad

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			accionEntidad = AccionEntidad.VISITAR
		} else {
			accionEntidad = (!album._id) ? AccionEntidad.CREAR : AccionEntidad.ACTUALIZAR
		}

		this.enrutadorService.navegarConPosicionFija(
			{
				componente: AlbumAudiosComponent,
				params: [
					{
						nombre: 'titulo',
						valor: titulo
					},
					{
						nombre: 'accionEntidad',
						valor: accionEntidad
					},
					{
						nombre: 'entidad',
						valor: CodigosCatalogoEntidad.NOTICIA
					}
				]
			},
			this.informacionEnrutador.posicion
		)
	}

	async agregarMediaANoticia(files: FileList) {
		try {

			this.confDialogoFullNoticia.mostrarDialogo = false
			this.confBotonArticulo.enProgreso = true

			if (files.length <= 0) {
				throw new Error('')
			}

			const file = files[0]
			if (file && file.type !== 'application/pdf') {
				this.confBotonArticulo.enProgreso = false
				this.toast.abrirToast('m4v5texto24')
				return
			}

			const dataApiArchivo: SubirArchivoData = {
				archivo: files[0],
				formato: 'application/pdf',
				catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
				descripcion: '',
				relacionAspecto: '1:1',
			}

			if (!this.noticia.medias) {
				this.noticia.medias = []
			}

			const media: MediaModel = await this.mediaNegocio.subirMedia(dataApiArchivo).toPromise()

			if (!media) {
				throw new Error('')
			}

			this.noticia.medias[0] = media
			this.confBotonArticulo.enProgreso = false

			const elemento = document.getElementById(this.inputArticuloNoticia) as HTMLInputElement
			if (elemento) {
				elemento.value = ''
			}
		} catch (error) {
			this.confBotonArticulo.enProgreso = false
			this.toast.abrirToast('text33')
		}
	}

	abrirSelectorArchivo(id: string) {

		const elemento = document.getElementById(id) as HTMLElement
		if (elemento) {
			elemento.click()
		}
	}
	async eventoBotonArticulo() {

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			// this.abrirSelectorArchivo(this.inputArticuloNoticia)
			this.eventoRevisarPDFFullNoticia()
			return
		}

		if (
			this.noticia &&
			this.noticia.medias &&
			this.noticia.medias.length === 0
		) {
			this.abrirSelectorArchivo(this.inputArticuloNoticia)
			return
		}

		let botones: Array<ItemDialogoHorizontal> = await this.configurarBotonReviewPDF([])
		botones = await this.configurarBotonCambiarPDF(botones)
		botones = await this.configurarBotonEliminarPDF(botones)

		this.configurarDialogoFullNoticia(
			true,
			botones
		)
	}

	async configurarBotonReviewPDF(
		botones: Array<ItemDialogoHorizontal>
	) {

		const texto = await this.translateService.get('m4v6texto21').toPromise()
		const texto2 = await this.translateService.get('m4v6texto22').toPromise()

		let descripcion = texto + '\n' + texto2
		if (
			this.noticia &&
			this.noticia.medias &&
			this.noticia.medias.length > 0
		) {
			botones.push({
				descripcion: descripcion,
				accion: {
					tipoBoton: TipoBoton.ICON_COLOR,
					colorIcono: ColorIconoBoton.AZUL,
					enProgreso: false,
					ejecutar: () => this.eventoRevisarPDFFullNoticia(),
				},
				estilosTexto: {
					color: ColorDelTexto.TEXTOAZULBASE,
					enMayusculas: true,
					estiloTexto: EstilosDelTexto.BOLD,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
				}
			})
		}

		return botones
	}
	async configurarBotonCambiarPDF(
		botones: Array<ItemDialogoHorizontal>
	): Promise<ItemDialogoHorizontal[]> {

		const texto = await this.translateService.get('m4v6texto23').toPromise()
		const texto2 = await this.translateService.get('m4v6texto24').toPromise()

		let descripcion = texto + '\n' + texto2
		botones.push({
			descripcion: descripcion,
			accion: {
				tipoBoton: TipoBoton.ICON_COLOR,
				colorIcono: ColorIconoBoton.AMARILLO,
				enProgreso: false,
				ejecutar: () => {
					this.abrirSelectorArchivo(this.inputArticuloNoticia)
				},
			},
			estilosTexto: {
				color: ColorDelTexto.TEXTOAZULBASE,
				enMayusculas: true,
				estiloTexto: EstilosDelTexto.BOLD,
				tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
			}
		})

		return botones
	}

	async configurarBotonEliminarPDF(
		botones: Array<ItemDialogoHorizontal>
	): Promise<ItemDialogoHorizontal[]> {

		const texto = await this.translateService.get('m4v6texto24.1').toPromise()
		const texto2 = await this.translateService.get('m4v6texto24.2').toPromise()

		let descripcion = texto + '\n' + texto2
		botones.push({
			descripcion: descripcion,
			accion: {
				tipoBoton: TipoBoton.ICON_COLOR,
				colorIcono: ColorIconoBoton.ROJO,
				enProgreso: false,
				ejecutar: () => {
					this.confDialogoEliminarPDF.mostrarDialogo = true
				},
			},
			estilosTexto: {
				color: ColorDelTexto.TEXTOAZULBASE,
				enMayusculas: true,
				estiloTexto: EstilosDelTexto.BOLD,
				tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
			}
		})

		return botones
	}


	eventoRevisarPDFFullNoticia() {

		if (
			this.noticia &&
			this.noticia.medias &&
			this.noticia.medias.length > 0
		) {
			const media: MediaModel = this.noticia.medias[0]

			if (
				media &&
				media.principal &&
				media.principal.url
			) {
				window.open(media.principal.url)
			}
		}
	}
	configurarDialogoFullNoticia(
		mostrar: boolean = false,
		botones: Array<ItemDialogoHorizontal> = []
	) {
		this.confDialogoFullNoticia = {
			mostrarDialogo: mostrar,
			completo: true,
			tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
			accionesDialogoHorizontal: botones
		}
	}

	async validarAccionCompartirProyecto() {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				const texto = await this.translateService.get('text30').toPromise()
				this.toast.abrirToast(texto)
				break
			case AccionEntidad.ACTUALIZAR:
				this.obtenerContactos(true)
				break
			case AccionEntidad.VISITAR:
				this.obtenerContactos(true)
				break
			default: break
		}
	}

	async publicarNoticia() {
		try {
			this.confBotonPublish.enProgreso = true
			const noticia: NoticiaModel = await this.noticiaNegocio.crearNoticia(this.noticia).toPromise()

			this.noticiaNegocio.removerNoticiaDeNoticiaActivaTercios(this.informacionEnrutador.posicion)
			this.toast.cerrarToast()
			this.validarAccionDespuesDeCambiosSegunElOrigen()
			this.confBotonPublish.enProgreso = false
		} catch (error) {
			this.confBotonPublish.enProgreso = false

			this.toast.abrirToast('text36')
		}
	}

	async actualizarNoticia() {
		try {
			this.confBotonPublish.enProgreso = true
			const noticia: NoticiaModel = await this.noticiaNegocio.actualizarNoticia({ ...this.noticia }).toPromise()

			this.toast.cerrarToast()
			this.validarAccionDespuesDeCambiosSegunElOrigen()
			this.confBotonPublish.enProgreso = false
		} catch (error) {
			this.confBotonPublish.enProgreso = false
			this.toast.abrirToast('text36')
		}
	}

	validarAccionDespuesDeCambiosSegunElOrigen(
		mostrarDone: boolean = true
	) {
		if (mostrarDone) {
			this.confDone.mostrarDone = mostrarDone
			setTimeout(() => {
				this.ejecutarAccionDespuesDelCambio()
			}, 1500)
			return
		}

		this.ejecutarAccionDespuesDelCambio()
	}

	ejecutarAccionDespuesDelCambio() {
		switch (this.origenValidacionCambios) {
			case OrigenValidacionDeCambios.BOTON_BACK:
				this.navegarAlBack()
				break
			case OrigenValidacionDeCambios.BOTON_HOME:
				this.navegarAlHome(true)
				break
			case OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL:
				this.irAlAlbumGeneral(false)
				break
			case OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS:
				this.irAlAlbumAudios(false)
				break
			case OrigenValidacionDeCambios.BOTON_ALBUM_LINKS:
				this.irAlAlbumDeLinks(false)
				break
			default:
				this.navegarAlBack()
				break
		}
	}

	eventoBotonPublish(
		validarCambios: boolean = true
	) {
		if (
			validarCambios &&
			!this.validarSiExistenCambios()
		) {
			if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.navegarAlBack()
				}, 1500)
				return
			}
			return
		}


		this.validarInformacionDelaNoticiaAntesDeRecargarOCambiarDePaginaHaciaDelante()
		const estado: boolean = this.noticiaService.validarCamposEnNoticia(this.noticia)

		if (!estado) {
			this.toast.abrirToast('text4')
			// if (this.params.accionEntidad === AccionEntidad.CREAR) {
			// 	this.noticiaNegocio.crearObjetoDeNoticiaVacio(
			// 		this.perfilSeleccionado,
			// 		this.informacionEnrutador.posicion
			// 	)
			// }
			return
		}

		// Publicar proyecto
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.publicarNoticia()
				break
			case AccionEntidad.ACTUALIZAR:
				this.actualizarNoticia()
				break
			case AccionEntidad.VISITAR:
				break
			default: break;
		}
	}

	async eliminarNoticia() {
		try {
			const estatus = await this.noticiaNegocio.eliminarNoticia(
				this.perfilSeleccionado._id,
				this.noticia.id
			).toPromise()

			this.toast.cerrarToast()
			this.confDone.mostrarDone = true
			setTimeout(() => {
				this.navegarAlBack()
			}, 1500)
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	inicializarDataListaContactos() {
		this.listaContactos = {
			anteriorPagina: false,
			paginaActual: 1,
			proximaPagina: true,
			totalDatos: 0,
			totalPaginas: 0,
			lista: [],
		}
	}


	configurarListaContactoCompartido() {
		this.confListaContactoCompartido = {
			mostrar: false,
			llaveSubtitulo: 'm4v2texto4',
			listaContactos: {
				lista: [],
				cargarMas: () => {
					if (this.listaContactos.proximaPagina) {
						this.listaContactos.paginaActual += 1
						this.obtenerContactos()
					}
				},
				reintentar: () => {
					this.inicializarDataListaContactos()
					this.obtenerContactos()
				},
				cargando: false,
				tamanoLista: TamanoLista.LISTA_CONTACTOS
			},
			botonAccion: {
				mostrarDialogo: false,
				mostrarDialogoLibre: true,
				tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
				completo: false,
				accionesDialogoHorizontal: [
					{
						accion: {
							ejecutar: () => {
								this.enviarNoticiaComoMensaje(
									CatalogoTipoMensaje.COMPARTIR_NOTICIA
								)
							},
							enProgreso: false,
							tipoBoton: TipoBoton.ICON_COLOR,
							colorIcono: ColorIconoBoton.ROJO,
						},
						descripcion: 'm4v2texto6',
						estilosTexto: {
							color: ColorDelTexto.TEXTOROJOBASE,
							estiloTexto: EstilosDelTexto.BOLD,
							enMayusculas: true,
							tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_IGUAL
						}
					},
				],
			},
			contactoSeleccionado: (idAsociacion: string) => {
				const posEnSeleccionado: number = this.listaContactosSeleccionados.findIndex(e => e === idAsociacion)
				const posEnListaContactos: number = this.confListaContactoCompartido.listaContactos.lista.findIndex(e => e.id === idAsociacion)
				const contacto = this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos]

				if (posEnSeleccionado >= 0) {
					this.listaContactosSeleccionados.splice(posEnSeleccionado, 1)
					if (posEnListaContactos >= 0) {
						contacto.configCirculoFoto.mostrarCorazon = false
						contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_ROJO
						this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos] = contacto
					}

					this.confListaContactoCompartido.botonAccion.mostrarDialogo = (this.listaContactosSeleccionados.length > 0)
					return
				}

				if (posEnSeleccionado <= 0) {
					this.listaContactosSeleccionados.push(idAsociacion)
					if (posEnListaContactos >= 0) {
						contacto.configCirculoFoto.mostrarCorazon = true
						contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_AZUL
						this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos] = contacto
					}

					this.confListaContactoCompartido.botonAccion.mostrarDialogo = (this.listaContactosSeleccionados.length > 0)
					return
				}
			}
		}
	}

	cambiarEstadoAppBarParaListaDeContactos(
		paraBuscar: boolean = true
	) {
		if (paraBuscar) {
			const buscador: BarraBusqueda = {
				mostrar: true,
				configuracion: {
					disable: false,
					modoBusqueda: ModoBusqueda.BUSQUEDA_LOCAL,
					buscar: (reiniciar: boolean) => {
						if (reiniciar) {
							this.listaContactosBuscador = []
							this.confListaContactoCompartido.listaContactos.lista = this.listaContactosOriginal
							return
						}

						const buscar: string = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda
						if (buscar.length > 0) {
							this.buscarContactos(buscar)
						}
					},
					entidad: CodigosCatalogoEntidad.CONTACTO,
					cerrarBuscador: () => {
						this.cerrarBuscador()
					},
					placeholder: 'm4v2texto1',
					valorBusqueda: '',
				}
			}
			this.configurarAppBar(1, buscador)
		} else {
			this.configurarAppBar(-1)
		}
	}

	async obtenerContactos(primeraVez?: boolean) {
		if (primeraVez) {
			this.configurarListaContactoCompartido()
		}



		this.cambiarEstadoAppBarParaListaDeContactos()
		this.confListaContactoCompartido.mostrar = true

		try {
			if (!this.listaContactos.proximaPagina) {
				return
			}

			this.confListaContactoCompartido.listaContactos.cargando = true
			const contactos: PaginacionModel<ParticipanteAsociacionModel> =
				await this.participanteAsociacionNegocio.obtenerParticipanteAsoTipo(
					this.perfilSeleccionado._id,
					15,
					this.listaContactos.paginaActual,
					CodigoEstadoParticipanteAsociacion.CONTACTO,
					FiltroGeneral.ALFA
				).toPromise()

			this.listaContactos.proximaPagina = contactos.proximaPagina
			contactos.lista.forEach(item => {
				this.listaContactos.lista.push(item)
				this.confListaContactoCompartido.listaContactos.lista.push(
					this.metodosParaFotos.configurarItemListaContacto(
						item.contactoDe,
						item.asociacion
					)
				)
			})

			this.confListaContactoCompartido.listaContactos.cargando = false
		} catch (error) {
			this.confListaContactoCompartido.botonAccion.mostrarDialogo = false
			this.confListaContactoCompartido.listaContactos.cargando = false
			this.confListaContactoCompartido.listaContactos.error = 'text37'
		}

	}

	enviarNoticiaComoMensaje(
		tipoMensaje: CatalogoTipoMensaje
	) {
		if (this.listaContactosSeleccionados.length <= 0) {
			this.toast.abrirToast('text49')
			return
		}

		const querys = {}
		this.listaContactosSeleccionados.forEach(idAsociacion => {
			const idMensaje = this.db.database.ref('mensajes/' + idAsociacion).push().key
			const mensajeModel = this.chatMetodosCompartidosService.crearObjetoMensajeParaNoticia(
				idMensaje,
				idAsociacion,
				tipoMensaje,
				{
					id: '',
					perfil: { _id: this.perfilSeleccionado._id }
				},
				this.noticia,
			)

			const path = idAsociacion + '/' + idMensaje
			querys[path] = mensajeModel
		})

		this.confListaContactoCompartido.mostrar = false
		this.confListaContactoCompartido.botonAccion.mostrarDialogo = false

		this.db.database.ref('mensajes').update(querys).then(b => {
			this.reiniciarListaContactos()
			this.toast.cerrarToast()
			this.confDone.mostrarDone = true
			setTimeout(() => {
				this.confDone.mostrarDone = false
			}, 1500)
		})
			.catch(error => {
				this.toast.abrirToast('text37')
			})
	}

	reiniciarListaContactos() {
		this.confListaContactoCompartido.mostrar = false
		this.confListaContactoCompartido.listaContactos.lista = []
		this.listaContactosSeleccionados = []
		this.inicializarDataListaContactos()
		this.configurarAppBar(-1)
	}

	configurarDone() {
		this.confDone = {
			mostrarDone: false,
			intervalo: 4000,
			mostrarLoader: false
		}
	}

	configurarDialogoConfirmarSalida(
		mostrarDialogo: boolean = false
	) {
		this.confDialogoSalida = {
			mostrarDialogo: mostrarDialogo,
			descripcion: 'm2v3texto21',
			tipo: TipoDialogo.CONFIRMACION,
			completo: true,
			listaAcciones: [
				{
					text: 'm2v13texto9',
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoSalida.mostrarDialogo = false
						this.eventoBotonPublish(false)
					}
				},
				{
					text: 'm2v13texto10',
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					enProgreso: false,
					ejecutar: () => {
						this.validarAccionDespuesDeCambiosSegunElOrigen(false)
					}
				}
			]
		}
	}

	buscarContactos(buscar: string) {
		if (!(this.confListaContactoCompartido.listaContactos.lista.length > 0)) {
			return
		}

		this.listaContactosBuscador = []
		if (this.listaContactosOriginal.length > 0) {
			this.confListaContactoCompartido.listaContactos.lista = this.listaContactosOriginal
		}

		const contactos: Array<ConfiguracionItemListaContactosCompartido> = this.confListaContactoCompartido.listaContactos.lista

		contactos.forEach(item => {
			if (
				item &&
				item.contacto &&
				item.contacto.nombreContacto &&
				item.contacto.nombreContacto.toLowerCase().includes(buscar)
			) {
				this.listaContactosBuscador.push(item)
			}
		})

		this.listaContactosOriginal = this.confListaContactoCompartido.listaContactos.lista
		this.confListaContactoCompartido.listaContactos.lista = this.listaContactosBuscador
	}

	cerrarBuscador() {
		this.listaContactosBuscador = []

		if (this.listaContactosOriginal.length > 0) {
			this.confListaContactoCompartido.listaContactos.lista = this.listaContactosOriginal
		}

		if (this.confListaContactoCompartido.listaContactos.lista.length === 0) {
			this.inicializarDataListaContactos()
			this.obtenerContactos(true)
		}
	}

	async apoyarNoticia() {
		try {
			const votoModel: VotoNoticiaModel = {
				perfil: {
					_id: this.perfilSeleccionado._id
				},
				noticia: {
					id: this.noticia.id
				},
				descripcion: 'Voto'
			}
			const estatus: string = await this.noticiaNegocio.apoyarNoticia(votoModel).toPromise()
			this.noticia.voto = true
			this.configurarVotarEntidad()
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	validarSiExistenCambios(): boolean {
		const noticiaAux = this.noticiaNegocio.asignarValoresDeLosCamposALaNoticiaParaValidarCambios(
			{ ...this.noticia },
			this.noticiaForm
		)

		const resultado: boolean = this.noticiaNegocio.validarSiExistenCambiosEnLaNoticia(
			noticiaAux,
			this.params.accionEntidad,
			this.informacionEnrutador.posicion
		)

		return resultado
	}

	navegarAlHome(posicionCambiar: boolean) {

		if (posicionCambiar) {
			this.enrutadorService.navegarConPosicionFija(
				{
					componente: MenuPrincipalComponent,
					params: []
				},
				this.informacionEnrutador.posicion
			)
		} else {
			this.enrutadorService.navegar(
				{
					componente: MenuPrincipalComponent,
					params: [
					]
				},
				{
					estado: true,
					posicicion: this.informacionEnrutador.posicion,
					extras: {
						tipo: TipoDeNavegacion.NORMAL,
					}
				}
			)
		}


	}

	navegarAlBack() {
		this.noticiaNegocio.removerNoticiaDeNoticiaActivaTercios(this.informacionEnrutador.posicion)
		this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
	}

	validarSiHayMediasEnElAlbum(): boolean {
		try {
			const album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(this.noticia.adjuntos)

			if (!album) {
				throw new Error('')
			}

			return (album.media.length > 0)
		} catch (error) {
			return false
		}
	}

	// Enrutamiento
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

	obtenerInputSegunKey(
		key: InputKey
	): InputCompartido {
		const index = this.inputsForm.findIndex(e => e.key === key)

		if (index < 0) {
			return undefined
		}

		return this.inputsForm[index].input
	}

	async configurarDialogoEliminarPDF() {
		this.confDialogoEliminarPDF = {
			mostrarDialogo: false,
			descripcion: 'm3v11texto7',
			tipo: TipoDialogo.CONFIRMACION,
			completo: true,
			listaAcciones: [
				{
					text: 'm2v13texto9',
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					enProgreso: false,
					ejecutar: () => {
						this.noticia.medias = []
						this.confDialogoEliminarPDF.mostrarDialogo = false
						this.confDialogoFullNoticia.mostrarDialogo = false
					}
				},
				{
					text: 'm2v13texto10',
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarPDF.mostrarDialogo = false
						this.confDialogoFullNoticia.mostrarDialogo = false
					}
				}
			]
		}
	}

	configuracionPropietarioNoticia(): ConfiguracionItemListaContactosCompartido {
		let usoCirculo: UsoItemCircular
		let urlMedia: string

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.PERFIL,
			this.noticia.perfil.album,
		)

		if (
			!album ||
			(
				album.portada &&
				album.portada.principal &&
				album.portada.principal.fileDefault
			)
		) {
			usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO
			urlMedia = album.portada.principal.url
		} else {
			usoCirculo = UsoItemCircular.CIRCONTACTO
			urlMedia = album.portada.principal.url
		}

		return {
			id: this.noticia.perfil._id || '',
			usoItem: UsoItemListaContacto.USO_CONTACTO,
			contacto: {
				nombreContacto: this.noticia.perfil.nombreContacto,
				nombreContactoTraducido: this.noticia.perfil.nombreContactoTraducido,
				nombre: this.noticia.perfil.nombre,
				estilosTextoSuperior: {
					color: ColorDelTexto.TEXTOAZULBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
				},
				estilosTextoInferior: {
					color: ColorDelTexto.TEXTONEGRO,
					estiloTexto: EstilosDelTexto.REGULAR,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
				},
				idPerfil: this.noticia.perfil._id || ''

			},
			configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
				urlMedia,
				ColorDeBorde.BORDER_ROJO,
				this.metodosParaFotos.obtenerColorFondoAleatorio(),
				false,
				usoCirculo,
				true
			),
			mostrarX: {
				mostrar: false,
				color: true
			},
			eventoCirculoNombre: () => {

				// this.abrirChatValidandoNotificaciones(idAsociacion)
			},



		}
	}

}
