import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  EnrutadorService,
  NotificacionesDeUsuario,
  VariablesGlobales,
} from '@core/servicios/generales';
import {
  AccionEntidad,
  CodigosCatalogosEstadoPerfiles,
  CodigosCatalogoTipoPerfil,
  CatalogoEstadoUsuario,
} from '@core/servicios/remotos/codigos-catalogos';
import {
  ColorTextoBoton,
  TipoBoton,
  TipoMenu,
  ToastComponent,
} from '@shared/componentes';
import {
  AnchoLineaItem,
  ColorDelTexto,
  ColorFondoItemMenu,
  ColorFondoLinea,
  EspesorLineaItem,
  EstilosDelTexto,
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  TamanoItemMenu,
  TamanoLista,
  UsoAppBar,
} from '@shared/diseno/enums';
import {
  BotonCompartido,
  ConfiguracionAppbarCompartida,
  ConfiguracionToast,
  DatosLista,
  ItemMenuCompartido,
} from '@shared/diseno/modelos';
import {
  CuentaNegocio,
  IdiomaNegocio,
  InternacionalizacionNegocio,
  PerfilNegocio,
} from 'dominio/logica-negocio';
import { InformacionModel } from 'dominio/modelo';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos';
import { PerfilModel, UsuarioModel } from 'dominio/modelo/entidades';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import { RutasLocales } from '../../rutas-locales.enum';
import {
  Enrutador,
  ExtrasEnrutador,
  InformacionEnrutador,
  ItemParam,
  TipoDeNavegacion,
} from '../enrutador/enrutador.component';
import { RegistroComponent } from '../registro/registro.component';

import { eachDayOfInterval, addDays, differenceInCalendarDays } from 'date-fns';

@Component({
  selector: 'app-menu-seleccion-perfiles',
  templateUrl: './menu-seleccion-perfiles.component.html',
  styleUrls: ['./menu-seleccion-perfiles.component.scss'],
})
export class MenuSeleccionPerfilesComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  public usuario: UsuarioModel;
  public mostrarModalVerificarCuenta: boolean;
  public dataLista: DatosLista;
  public listaTipoPerfil: CatalogoTipoPerfilModel[];

  public confAppBar: ConfiguracionAppbarCompartida;
  public confToast: ConfiguracionToast;
  public botonReenvioEmail: BotonCompartido;
  public botoncerrarSesionEmail: BotonCompartido;
  public botonMensajeTiempoVerificacion: BotonCompartido;
  public confBotonCambiarEmail: BotonCompartido;
  public confDialogoCambiarEmail: ConfiguracionCambiarEmail;
  public itemInformacion: InformacionModel;
  public itemSeleccionado: any;
  public correoGazelook: string;
  public idiomaSeleccionado: CatalogoIdiomaEntity;
  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;
  public mostrarModalCerrarCuentaVerificacion = false;
  public mostrarMensajeConfirmarCuenta = false;
  public mostrarMensajeCuentaCripto: boolean;

  public diferenciaActivarCuenta: number;
  public mensajeTiempoVerificacion: string;

  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private perfilNegocio: PerfilNegocio,
    private _location: Location,
    private router: Router,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private cuentaNegocio: CuentaNegocio,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private enrutadorService: EnrutadorService,
    private idiomaNegocio: IdiomaNegocio
  ) {
    this.mostrarModalVerificarCuenta = false;
    this.mostrarMensajeCuentaCripto = false;
    this.listaTipoPerfil = [];
    this.correoGazelook = 'inter@gazelook.com';
  }

  ngOnInit(): void {
    window.localStorage.removeItem('respCoiPaiments');
    this.inicializarDataDeLaEntidad();
    this.configurarListaTipoPerfiles();
    this.obtenerCatalogoTipoPerfil();
    if (this.usuario) {
      this.prepararAppBar();
      this.prepararInfoTipoPerfiles();
      this.configurarBotonesVerificarCuenta();
      this.comprobarVerificarCuentaInicial();
      this.comprobarPagoCriptoMensaje();
      this.configurarToast();
      this.validarEstadoDeLaCuenta();
      this.escucharCambioEnEstadoDeLosPerfiles();
      this.escucharCambiosDeNuevosPerfilesEnElUsuario();
      this.notificacionesUsuario.validarEstadoDeLaSesion();
      return;
    }
  }

  inicializarDataDeLaEntidad(): void {
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
  }

  async obtenerCatalogoTipoPerfil(): Promise<void> {
    try {
      this.listaTipoPerfil = await this.perfilNegocio
        .obtenerCatalogoTipoPerfilConPerfil(false)
        .toPromise();
      this.dataLista.cargando = false;
    } catch (error) {
      this.listaTipoPerfil = [];
      this.dataLista.cargando = false;
      this.dataLista.error = 'text37';
    }
  }

  comprobarPagoCriptoMensaje(): void {
    if (this.usuario.estado.codigo === CatalogoEstadoUsuario.ACTIVA) {
      return;
    }

    if (
      this.usuario.estado.codigo === CatalogoEstadoUsuario.PENDIENTE_PAGO_CRIPTO
    ) {
      this.mostrarMensajeCuentaCripto = true;
      this.mostrarModalVerificarCuenta = false;
      return;
    }
  }

  comprobarVerificarCuentaInicial(): void {
    if (this.usuario.estado.codigo === CatalogoEstadoUsuario.ACTIVA) {
      return;
    }
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();

    const fechaCreacion = new Date(this.usuario.fechaCreacion);
    const fechaActual = new Date();
    // mensaje after 10 days
    const fechaMensaje = addDays(fechaCreacion, 10);
    const dias = eachDayOfInterval({ start: fechaCreacion, end: fechaMensaje });
    if (
      fechaActual < fechaMensaje &&
      this.usuario.estado.codigo === CatalogoEstadoUsuario.ACTIVA_NO_VERIFICADO
    ) {
      dias.map((day) => {
        this.diferenciaActivarCuenta = differenceInCalendarDays(
          day,
          fechaActual
        );
        if (this.diferenciaActivarCuenta === 0) {
          this.mostrarModalVerificarCuenta = true;
          this.mostrarMensajeConfirmarCuenta = false;
        } else {
          this.mostrarModalVerificarCuenta = false;
          this.mostrarMensajeConfirmarCuenta = true;
        }
      });
    } else {
      this.mostrarModalVerificarCuenta = true;
    }
  }

  logoutVerificarCuenta(): void {
    this.cuentaNegocio
      .cerrarSesion()
      .then(() => {
        console.log('cerrar sesion');
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  configurarBotonesVerificarCuenta(): void {
    this.botonReenvioEmail = {
      text: 'm15v1texto7',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.solicitarCorreoVeri();
      },
    };

    this.botoncerrarSesionEmail = {
      text: 'm15v1texto10',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.logoutVerificarCuenta();
      },
    };

    this.botonMensajeTiempoVerificacion = {
      text: 'm15v1texto4',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.mostrarMensajeConfirmarCuenta = false;
      },
    };

    this.confBotonCambiarEmail = {
      text: 'Cambiar correo',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.cambiarEmail();
      },
    };

    this.confDialogoCambiarEmail = {
      mostrar: true,
      email: '',
      mostrarStatus: false,
      status: '',
      mostrarLoader: false,
      mostrarError: false,
      error: '',
    };
  }

  solicitarCorreoVeri(): void {
    this.botonReenvioEmail.enProgreso = true;
    this.cuentaNegocio.reenviarCorreoVerificacion(this.usuario.id).subscribe(
      (data) => {
        if (data.datos) {
          this.confDialogoCambiarEmail.mostrarLoader = false;
          this.confDialogoCambiarEmail.mostrarStatus = true;
          this.confDialogoCambiarEmail.status = 'm11v1texto7';
          this.confDialogoCambiarEmail.mostrarCerrarSesion = true;
          this.mostrarModalVerificarCuenta = false;
          this.mostrarModalCerrarCuentaVerificacion = true;
        } else {
          this.confDialogoCambiarEmail.mostrarLoader = false;
          this.confDialogoCambiarEmail.mostrarStatus = true;
          this.confDialogoCambiarEmail.status = data.mensaje;
          this.mostrarModalCerrarCuentaVerificacion = false;
        }
        this.botonReenvioEmail.enProgreso = false;
        this.toast.cerrarToast();
      },
      (error) => {
        this.toast.abrirToast('text37');
      }
    );
  }

  async cambiarEmail(): Promise<void> {
    if (
      !this.confDialogoCambiarEmail.email ||
      !(this.confDialogoCambiarEmail.email.length > 0) ||
      !(this.confDialogoCambiarEmail.email.indexOf('@') >= 0) ||
      !(this.confDialogoCambiarEmail.email.indexOf('.') >= 0)
    ) {
      this.confDialogoCambiarEmail.mostrarLoader = false;
      this.confDialogoCambiarEmail.mostrarError = true;
      this.confDialogoCambiarEmail.error = 'text5';
      return;
    }

    try {
      this.confDialogoCambiarEmail.mostrarError = false;
      this.confDialogoCambiarEmail.mostrarLoader = true;
      let usuario: UsuarioModel;
      usuario = {};
      usuario.idDispositivo = this.usuario.dispositivos[0].id;
      usuario.email = this.confDialogoCambiarEmail.email;
      const estatus: string = await this.perfilNegocio
        .actualizarDatosUsuario(usuario, this.usuario.id)
        .toPromise();
      if (!estatus) {
        throw new Error('');
      }

      this.confDialogoCambiarEmail.mostrarLoader = false;
      this.confDialogoCambiarEmail.mostrarStatus = true;
      this.confDialogoCambiarEmail.status = 'Tu correo ha sido cambiado';
      this.confDialogoCambiarEmail.mostrarCerrarSesion = true;
      this.toast.cerrarToast();
    } catch (error) {
      this.confDialogoCambiarEmail.mostrarLoader = false;
      this.confDialogoCambiarEmail.mostrarError = true;
      this.confDialogoCambiarEmail.error = 'text37';
    }
  }

  validarEstadoDeLaCuenta() {
    let fechaActual = new Date();
    let manana = new Date(this.usuario.fechaCreacion);
    let diferencia = fechaActual.getTime() - manana.getTime();
  }

  configurarListaTipoPerfiles() {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoTipoPerfil,
      lista: this.listaTipoPerfil,
      tamanoLista: TamanoLista.TIPO_PERFILES,
    };
  }

  prepararAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.SOLO_TITULO,
      tituloAppbar: {
        mostrarBotonXRoja: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        tituloPrincipal: {
          mostrar: true,
          llaveTexto: 'm2v10texto1',
        },
        mostrarLineaVerde: true,
        mostrarDivBack: false,
      },
      sinColorPrincipal: true,
      conColorSecundario: true,
    };
  }

  obtenerLlavesTextosSegunCodigoPerfil(
    codigoPerfil: CodigosCatalogoTipoPerfil
  ) {
    const textos = {
      texto2: '',
      texto3: '',
    };
    switch (codigoPerfil) {
      case CodigosCatalogoTipoPerfil.CLASSIC:
        textos.texto2 = 'm2v10texto5';
        textos.texto3 = 'm2v10texto6';
        break;
      case CodigosCatalogoTipoPerfil.PLAYFUL:
        textos.texto2 = 'm2v10texto8';
        textos.texto3 = 'm2v10texto9';
        break;
      case CodigosCatalogoTipoPerfil.SUBSTITUTE:
        textos.texto2 = 'm2v10texto11';
        textos.texto3 = 'm2v10texto12';
        break;
      case CodigosCatalogoTipoPerfil.GROUP:
        textos.texto2 = 'm2v10texto14';
        textos.texto3 = 'm2v10texto15';
        break;
      default:
        break;
    }

    return textos;
  }

  prepararItemTipoPerfil(
    tipoPerfil: CatalogoTipoPerfilModel
  ): ItemMenuCompartido {
    const textos = this.obtenerLlavesTextosSegunCodigoPerfil(
      tipoPerfil.codigo as CodigosCatalogoTipoPerfil
    );

    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_ELEGIR_PERFIL,
      colorFondo: this.obtenerColorPerfil(tipoPerfil.perfil),
      mostrarDescripcion: tipoPerfil.mostrarDescripcion ?? false,
      texto1: this.obtenerEstadoPerfil(tipoPerfil.perfil),
      texto2: textos.texto2,
      texto3: textos.texto3,
      tipoMenu: TipoMenu.GESTION_PROFILE,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6386,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: tipoPerfil.codigo,
      onclick: () => this.gestionarPerfil(tipoPerfil, true),
      dobleClick: () => this.gestionarPerfil(tipoPerfil),
    };
  }

  prepararItemInformacion(informacion: InformacionModel): ItemMenuCompartido {
    try {
      return {
        id: '',
        tamano: TamanoItemMenu.ITEM_MENU_ELEGIR_PERFIL, // Indica el tamano del item (altura)
        colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
        mostrarDescripcion: informacion.mostrarDescripcion ?? false,
        tipoMenu: TipoMenu.ACCION,
        texto1: 'm2v1texto22',
        descripcion: [
          {
            texto: informacion.descripcion[0],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[1],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOAMARILLOBASE,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[2],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[3],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[4],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[5],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[6],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
          {
            texto: informacion.descripcion[7],
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
            color: ColorDelTexto.TEXTOBLANCO,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
          },
        ],
        linea: {
          mostrar: true,
          configuracion: {
            ancho: AnchoLineaItem.ANCHO6920,
            espesor: EspesorLineaItem.ESPESOR071,
            colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
            forzarAlFinal: false,
          },
        },
        gazeAnuncios: false,
        idInterno: informacion.codigo,
        onclick: () => this.mostrarDescripcion(informacion),
        dobleClick: () => {},
      };
    } catch (error) {}
  }

  async prepararInfoTipoPerfiles() {
    this.itemInformacion = {
      codigo: 'info',
      nombre: await this.internacionalizacionNegocio.obtenerTextoLlave(
        'texto255'
      ),
      descripcion: [
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto1'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto2'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto3'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto4'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto5'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto6'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto7'),
        await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto8'),
      ],
    };
  }

  mostrarDescripcion(item: any) {
    if (this.itemSeleccionado === undefined) {
      this.itemSeleccionado = item;
    }

    if (this.itemSeleccionado.codigo === item.codigo) {
      const elemento: HTMLElement = document.getElementById(
        'flecha' + item.codigo
      ) as HTMLElement;
      if (item.mostrarDescripcion) {
        item.mostrarDescripcion = false;
        this.itemSeleccionado.mostrarDescripcion = false;
        elemento.classList.remove('rotar-flecha');
      } else {
        item.mostrarDescripcion = true;
        this.itemSeleccionado.mostrarDescripcion = true;
        elemento.classList.add('rotar-flecha');
      }
    }

    if (this.itemSeleccionado.codigo !== item.codigo) {
      if (this.itemSeleccionado.mostrarDescripcion) {
        const elemento: HTMLElement = document.getElementById(
          'flecha' + this.itemSeleccionado.codigo
        ) as HTMLElement;
        if (this.itemSeleccionado.mostrarDescripcion) {
          this.itemSeleccionado.mostrarDescripcion = false;
          elemento.classList.remove('rotar-flecha');
        } else {
          this.itemSeleccionado.mostrarDescripcion = true;
          elemento.classList.add('rotar-flecha');
        }
      }

      const elemento2: HTMLElement = document.getElementById(
        'flecha' + item.codigo
      ) as HTMLElement;
      if (item.mostrarDescripcion) {
        item.mostrarDescripcion = false;
        elemento2.classList.remove('rotar-flecha');
      } else {
        item.mostrarDescripcion = true;
        elemento2.classList.add('rotar-flecha');
      }
      this.itemSeleccionado = item;
    }
  }

  navegarMenuPrincipal(tipoPerfil: CatalogoTipoPerfilModel) {
    this.perfilNegocio.almacenarPerfilSeleccionado(tipoPerfil);

    if (tipoPerfil.perfil) {
      this.router.navigateByUrl(RutasLocales.ENRUTADOR);
    }
  }

  gestionarPerfil(
    tipoPerfil: CatalogoTipoPerfilModel,
    irMenuPrincipal: boolean = false
  ) {
    if (!tipoPerfil.perfil || tipoPerfil.perfil === null) {
      const perfil: PerfilModel = this.perfilNegocio.crearObjetoPerfilVacio(
        tipoPerfil.codigo as CodigosCatalogoTipoPerfil
      );
      tipoPerfil.perfil = perfil;
    }

    this.perfilNegocio.almacenarPerfilSeleccionado(tipoPerfil);

    if (
      this.perfilNegocio.conflictoCrearPerfil(tipoPerfil, this.listaTipoPerfil)
    ) {
      if (tipoPerfil.codigo === CodigosCatalogoTipoPerfil.GROUP) {
        this.toast.abrirToast('text15.0');
      } else {
        this.toast.abrirToast('text15');
      }
    } else {
      if (irMenuPrincipal) {
        const perfilAux: PerfilModel =
          this.perfilNegocio.obtenerPerfilDelUsuarioSegunTipo(
            tipoPerfil.codigo as CodigosCatalogoTipoPerfil,
            false
          );

        if (
          perfilAux &&
          perfilAux.estado &&
          perfilAux.estado.codigo &&
          perfilAux.estado.codigo ===
            CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
        ) {
          this.navegarMenuPrincipal(tipoPerfil);
        }
      } else {
        this.navegarCrearEditarPerfil(tipoPerfil);
      }
    }
  }

  navegarCrearEditarPerfil(tipoPerfil: CatalogoTipoPerfilModel) {
    const params: Array<ItemParam> = [
      {
        nombre: 'codigoTipoPerfil',
        valor: tipoPerfil.codigo,
      },
    ];
    const extras: ExtrasEnrutador = {
      tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
      paramAValidar: '',
    };

    if (tipoPerfil.perfil && tipoPerfil.perfil._id) {
      params.push({
        nombre: 'id',
        valor: tipoPerfil.perfil._id,
      });
      params.push({
        nombre: 'accionEntidad',
        valor: AccionEntidad.ACTUALIZAR,
      });
    } else {
      params.push({
        nombre: 'accionEntidad',
        valor: AccionEntidad.CREAR,
      });
    }

    this.enrutadorService.navegarConPosicionFija(
      {
        componente: RegistroComponent,
        params: params,
      },
      this.informacionEnrutador.posicion
    );
  }

  obtenerEstadoPerfil(perfil: PerfilModel) {
    if (perfil) {
      switch (perfil.estado.codigo) {
        case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO:
          return 'm2v10texto4';
        case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
          return 'm2v10texto10';
      }
    }
    return 'm2v10texto7';
  }

  obtenerColorPerfil(perfil: PerfilModel) {
    if (perfil) {
      switch (perfil.estado.codigo) {
        case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO:
          return ColorFondoItemMenu.PERFILCREADO;
        case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
          return ColorFondoItemMenu.PERFILHIBERNADO;
      }
    }
    return ColorFondoItemMenu.PREDETERMINADO;
  }

  prepareItemInstrucciones(): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_ELEGIR_PERFIL_INFO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
      mostrarDescripcion: false,
      tipoMenu: TipoMenu.INSTRUCCIONES,
      texto1: null,
      texto2: 'm2v10texto2',
      texto3: 'm2v10texto2.1',
      texto4: 'm2v10texto3',
      texto5: 'm2v10texto3.1',
      texto6: 'm2v10texto3.2',
      descripcion: null,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6386,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: '',
      onclick: () => {},
      dobleClick: () => {},
    };
  }

  configurarToast() {
    this.confToast = {
      texto: '',
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: true,
      intervalo: 3,
    };
  }

  escucharCambioEnEstadoDeLosPerfiles() {
    this.variablesGlobales.cambiosDeEstadoEnElPerfil$.subscribe((perfil) => {
      this.atualizarItemTipoPerfil(perfil);
    });
  }

  atualizarItemTipoPerfil(perfil: PerfilModel) {
    const index = this.listaTipoPerfil.findIndex(
      (e) => e && e !== null && e.perfil && e.perfil._id === perfil._id
    );

    if (index >= 0) {
      if (
        perfil.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ELIMINADO
      ) {
        this.listaTipoPerfil[index].perfil = null;
      } else {
        this.listaTipoPerfil[index].perfil.estado = perfil.estado;
      }
      return;
    }

    const indexDos = this.listaTipoPerfil.findIndex(
      (e) => e && e.codigo && e.codigo === perfil.tipoPerfil.codigo
    );
    if (
      indexDos >= 0 &&
      perfil.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
    ) {
      this.listaTipoPerfil[indexDos].perfil = perfil;
    }
  }

  escucharCambiosDeNuevosPerfilesEnElUsuario() {
    this.variablesGlobales.nuevoPerfilAnadido$.subscribe(async (perfil) => {
      this.atualizarItemTipoPerfil(perfil);
    });
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}

export interface ConfiguracionCambiarEmail {
  mostrar?: boolean;
  email?: string;
  mostrarLoader?: boolean;
  mostrarError?: boolean;
  error?: string;
  mostrarStatus?: boolean;
  status?: string;
  mostrarCerrarSesion?: boolean;
}

const CORREOS_CONTACT_GAZE = {
  en: 'contacten@gazelook.com',
  es: 'contactes@gazelook.com',
  fr: 'contactfr@gazelook.com',
  de: 'contactde@gazelook.com',
  it: 'contactit@gazelook.com',
  pt: 'contactpt@gazelook.com',
};
