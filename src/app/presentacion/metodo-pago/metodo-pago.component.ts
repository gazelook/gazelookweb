import { Title } from '@angular/platform-browser';

import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ConfiguracionDialogoInline,
  LineaCompartida,
} from '@shared/diseno/modelos';

import { Location } from '@angular/common';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  GeneradorId,
  MonedaPickerService,
  VariablesGlobales,
} from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
  CodigosCatalogoMetodoPago,
  CodigosCatalogoOrigenTransaccion,
  CodigosCatalogosMetodosDePago,
  CodigosCatalogoTipoMoneda,
  CodigosEstadoMetodoPago,
} from '@core/servicios/remotos/codigos-catalogos';
import { TranslateService } from '@ngx-translate/core';
import {
  ColorTextoBoton,
  ModalContenido,
  TipoBoton,
  ToastComponent,
} from '@shared/componentes';
import {
  AccionesSelector,
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  EspesorDelBorde,
  EstiloErrorInput,
  EstiloInput,
  EstilosDelTexto,
  PaddingIzqDerDelTexto,
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  TamanoLista,
  UsoAppBar,
} from '@shared/diseno/enums';
import {
  BotonCompartido,
  ConfiguracionAppbarCompartida,
  ConfiguracionMonedaPicker,
  ConfiguracionToast,
  DatosLista,
  InfoAccionSelector,
  InputCompartido,
  ItemSelector,
  ResumenDataMonedaPicker,
  ValorBase,
} from '@shared/diseno/modelos';
import {
  StripeCardElementOptions,
  StripeElementLocale,
  StripeElementsOptions,
} from '@stripe/stripe-js';
import {
  CatalogoIdiomaEntity,
  PagoFacturacionEntity,
} from 'dominio/entidades/catalogos';
import {
  CuentaNegocio,
  IdiomaNegocio,
  InternacionalizacionNegocio,
  PagoNegocio,
  TipoMonedaNegocio,
} from 'dominio/logica-negocio';
import { PagoModel } from 'dominio/modelo';
import {
  CatalogoMetodoPagoModel,
  CatalogoTipoMonedaModel,
  PaymentezTransaccion,
} from 'dominio/modelo/catalogos';
import { UsuarioModel } from 'dominio/modelo/entidades';

import { StripeCardComponent, StripeService } from 'ngx-stripe';
import { RutasLocales } from '../../rutas-locales.enum';
import {
  Enrutador,
  InformacionEnrutador,
} from '../enrutador/enrutador.component';
import { environment } from '@env/src/environments/environment';
import { RegistroComponent } from '../registro/registro.component';
import { CoinPaymentsRatesEntity } from 'dominio/entidades/catalogos/coinpayments-rates.entity';

declare var PaymentCheckout;
const COD_MONEDAS_PAYMENTEZ = [
  'TIPMON_1',
  'TIPMON_2',
  'TIPMON_9',
  'TIPMON_21',
  'TIPMON_22',
  'TIPMON_33',
  'TIPMON_36',
  'TIPMON_103',
  'TIPMON_113',
  'TIPMON_119',
  'TIPMON_114',
  'TIPMON_152',
  'TIPMON_59',
  'TIPMON_62',
  'TIPMON_108',
];

@Component({
  selector: 'app-metodo-pago',
  templateUrl: './metodo-pago.component.html',
  styleUrls: ['./metodo-pago.component.scss'],
})
export class MetodoPagoComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  configuracionAppBar: ConfiguracionAppbarCompartida;
  listaMetodoPago: CatalogoMetodoPagoModel[];
  codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  codigoPago: string;
  pagoForm: FormGroup;
  criptoForm: FormGroup;
  idiomaSeleccionado: CatalogoIdiomaEntity;
  inputNombre: InputCompartido;
  inputTelefono: InputCompartido;
  inputDireccion: InputCompartido;
  inputEmail: InputCompartido;
  dataModalPaymentez: ModalContenido;
  dataModalStripe: ModalContenido;
  dataModalCoinPayments: ModalContenido;
  dataModalMontoPago: ModalContenido;
  configuracionToast: ConfiguracionToast;
  dataLista: DatosLista;
  copiaMonedas: any;
  confLinea: LineaCompartida;
  public configDialoPago: DialogoCompartido;
  public confDialogoEnConstruccion: ConfiguracionDialogoInline;
  public montoPago: string;
  public codMoneda: string;
  public confBotonMontoPago: BotonCompartido;
  public catalogoTipoMoneda: Array<CatalogoTipoMonedaModel>;
  public confMonedaPicker: ConfiguracionMonedaPicker;

  public valorBaseAPagar: ValorBase;
  public idInputCantidadAPagar: string;
  public metodoPagoBitcoin = '';
  public isChecked: boolean;
  // Informacion
  public informacionEnrutador: InformacionEnrutador;

  public tipoMonedaPaymentez: ItemSelector[] = [];
  public tipoMonedaLocales: ItemSelector[] = [];

  private paymentCheckout;
  private pagoNoconfirmado: boolean;
  private tipoMoneda: string;
  public cuotaMinima: number;
  public mostrarMessage = false;

  private usuario: UsuarioModel;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;

  public coinPaymentsRates: CoinPaymentsRatesEntity[] = [];
  public mostrarSelectorCoinPayments = false;
  public mostrarRespuestaCoinPayments = false;
  public mensajePaimentez: string;

  public botonContinuarPerfiles: BotonCompartido;
  public mostrarModalPagoContinuar = false;
  public descripcionPagoContinuar: string;

  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  cardOptions: StripeCardElementOptions = {
    hidePostalCode: true,
    style: {
      base: {
        color: '#03486b',
        fontSmoothing: 'antialiased',
        fontSize: '6vw',
        fontFamily: 'Source Sans Pro, sans-serif',
        '::placeholder': {
          color: '#03486b',
        },
        fontWeight: 'bold',
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
      },
    },
  };

  elementsOptions: StripeElementsOptions = {
    locale: 'en',
  };

  constructor(
    private pagoNegocio: PagoNegocio,
    private cuentaNegocio: CuentaNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private router: Router,
    private stripeService: StripeService,
    private fb: FormBuilder,
    private idiomaNegocio: IdiomaNegocio,
    // tslint:disable-next-line: variable-name
    private _location: Location,
    private translateService: TranslateService,
    private monedaPickerService: MonedaPickerService,
    private variablesGlobales: VariablesGlobales,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    public auth: AngularFireAuth,
    private enrutadorService: EnrutadorService,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private generadorId: GeneradorId,
    private ngZone: NgZone,
    private title: Title,
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {
    this.metodoPagoBitcoin = CodigosCatalogosMetodosDePago.CRIPTOMONEDA;
    this.idInputCantidadAPagar = 'KJ21-0982jjs';
    this.catalogoTipoMoneda = [];
    this.isChecked = false;
  }

  checkValue(event: any): void {}

  // todo : arreglar mensaje cuenta
  ngOnInit(): void {
    this.title.setTitle('Payment Gazelook');
    this.comprobarPagoLocalContinuar();
    this.obtenerIdioma();
    this.configurarLinea();
    this.pagoForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      telefono: ['', Validators.required],
      direccion: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      checkBox: [],
    });
    this.criptoForm = this.fb.group({
      currency2: ['', [Validators.required]],
    });

    this.configurarDialogosEnConstruccion();
    this.inicializarInputsCompartidos();
    this.configurarBotones();
    this.configurarBotonPago();
    this.configurarTextoPaymentz();
    this.preperarLista();
    this.obtenerCatalogoMetodosPago();
    this.configurarAppBar();
    this.configurarDialogoContenido();
    this.configurarToast();
    this.configurarMonedaPicker();
    this.obtenerCoinPaymentsRates();

    this.inicializarDataCatalogoMoneda();

    const respCoiPaimentsLocal = JSON.parse(
      localStorage.getItem('respCoiPaiments')
    );
    respCoiPaimentsLocal
      ? (this.mostrarRespuestaCoinPayments = true)
      : (this.mostrarRespuestaCoinPayments = false);

    // Inicializar funcion de redondeo
    // tslint:disable-next-line: no-string-literal
    if (!Math['round10']) {
      // tslint:disable-next-line: no-string-literal
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp);
      };
    }

    this.paymentCheckout = new PaymentCheckout.modal({
      client_app_code: environment.clientAppCodePaymentez, // Client Credentials
      client_app_key: environment.clientAppKeyPaymentez, // Client Credentials
      locale:
        this.idiomaSeleccionado?.codNombre === 'en' ||
        this.idiomaSeleccionado?.codNombre === 'es' ||
        this.idiomaSeleccionado?.codNombre === 'pt'
          ? this.idiomaSeleccionado?.codNombre
          : 'en', // User's preferred language (es, en, pt). English will be used by default.
      env_mode: environment.envModePaymentez, // `prod`, `stg`, `local` to change environment. Default is `stg`
      onOpen: () => {
        console.log('modal open');
      },
      onClose: () => {
        console.log('modal closed');
        this.configurarTextoPaymentz(true).then();
        this.mostrarMessage = false;
      },
      onResponse: (response: PaymentezTransaccion) => {
        const { transaction } = response;
        if (transaction.status === 'success') {
          this.pagoNoconfirmado = false;
          this.configurarDialogoPago();
          this.toast.abrirToast(' ', true);
          this.cuentaNegocio
            .activarCuentaPaymentez(
              this.codigoPago,
              response.transaction.amount,
              transaction.id,
              this.pagoNoconfirmado,
              this.tipoMoneda,
              this.cuotaMinima,
              response.transaction.authorization_code
            )
            .subscribe(
              (resp) => {
                this.ngZone
                  .runOutsideAngular(async () => {
                    this.cuentaNegocio.eliminarSessionStorage();
                    const dataFirebase = await this.auth.signInAnonymously();
                    console.log(dataFirebase);
                    if (
                      !dataFirebase ||
                      !dataFirebase.user ||
                      !dataFirebase.user.uid
                    ) {
                      console.log('sesi');
                      throw new Error('');
                    }
                    this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(
                      dataFirebase.user.uid
                    );
                    this.toast.cerrarToast();
                    this.mostrarModalPagoContinuar = true;
                    window.localStorage.setItem(
                      'mensajePago',
                      this.descripcionPagoContinuar
                    );
                    console.log(this.descripcionPagoContinuar);
                  })
                  .then((_) => {
                    console.log('termino');
                  });
              },
              (error) => {
                this.toast.cerrarToast();
                console.log('Error al activar la cuenta');
              }
            );
        } else {
          this.pagoNoconfirmado = true;
          this.activarCuentaPaymentez(
            response.transaction.amount,
            transaction.id,
            this.pagoNoconfirmado
          );
        }
      },
    });

    window.addEventListener('popstate', () => {
      this.paymentCheckout.close();
    });
  }

  comprobarPagoLocalContinuar(): void {
    this.mostrarModalPagoContinuar =
      !!window.localStorage.getItem('mensajePago');
    this.descripcionPagoContinuar =
      window.localStorage.getItem('mensajePago') !== null
        ? window.localStorage.getItem('mensajePago')
        : '';
  }

  configurarBotonPago(): void {
    this.botonContinuarPerfiles = {
      text: 'm2v9texto13',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.mostrarModalPagoContinuar = false;
        window.localStorage.removeItem('mensajePago');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      },
    };
  }

  async configurarTextoPaymentz(estado: boolean = false): Promise<void> {
    const texto = 'text107';
    const texto2 = 'text37';
    estado
      ? (this.mensajePaimentez = await this.translateService
          .get(texto2)
          .toPromise())
      : (this.mensajePaimentez = await this.translateService
          .get(texto)
          .toPromise());
  }

  configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false,
    };
  }

  obtenerCoinPaymentsRates(): void {
    console.log('ejecuto');
    this.cuentaNegocio.obtenerCoinPaymentsRates().subscribe(
      (resp) => (this.coinPaymentsRates = resp),
      (error) => console.log(error)
    );
  }

  async configurarBotones(): Promise<void> {
    this.confBotonMontoPago = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        if (
          this.codigosCatalogos.PAYMENTEZ === this.codigoPago ||
          this.codigoPago === this.codigosCatalogos.PAYMENTEZ2
        ) {
          if (this.isChecked) {
            this.confirmarMontoDePago();
          } else {
            this.toast.abrirToast('text101');
          }
        } else {
          this.confirmarMontoDePago();
        }
      },
    };
  }

  navegarMetodoPago(metodoPago: CatalogoMetodoPagoModel): void {
    console.log('aa', metodoPago);

    if (metodoPago.codigo === this.codigosCatalogos.CRIPTOMONEDA) {
      this.confDialogoEnConstruccion.noMostrar = false;
      return;
    } else {
      this.confDialogoEnConstruccion.noMostrar = true;
    }
    this.codigoPago = metodoPago.codigo;
    this.dataModalMontoPago.abierto = true;
    this.codMonedasPaymentez();
    if (this.codigoPago === this.codigosCatalogos.CRIPTOMONEDA) {
      this.mostrarSelectorCoinPayments = true;
    } else {
      this.mostrarSelectorCoinPayments = false;
    }
  }

  codMonedasPaymentez(): void {
    this.tipoMonedaPaymentez = [];
    this.tipoMonedaLocales.map((item) => {
      if (COD_MONEDAS_PAYMENTEZ.includes(item.codigo)) {
        this.tipoMonedaPaymentez.push(item);
      }
    });
  }

  async confirmarMontoDePago(): Promise<void> {
    this.dataModalMontoPago.bloqueado = true;
    this.confBotonMontoPago.enProgreso = true;
    this.dataModalMontoPago.error = false;
    this.dataModalMontoPago.errorContenido = '';

    if (
      !(
        parseFloat(
          this.confMonedaPicker.inputCantidadMoneda.valor.valorFormateado
        ) > 0
      )
    ) {
      this.dataModalMontoPago.bloqueado = false;
      this.confBotonMontoPago.enProgreso = false;
      this.dataModalMontoPago.error = true;
      this.dataModalMontoPago.errorContenido = 'text40';
      return;
    }

    if (
      !(
        this.confMonedaPicker.selectorTipoMoneda.seleccionado.codigo &&
        this.confMonedaPicker.selectorTipoMoneda.seleccionado.auxiliar
      )
    ) {
      this.dataModalMontoPago.bloqueado = false;
      this.confBotonMontoPago.enProgreso = false;
      this.dataModalMontoPago.error = true;
      this.dataModalMontoPago.errorContenido = 'text41';
      return;
    }
    if (this.codigoPago === this.codigosCatalogos.CRIPTOMONEDA) {
      if (this.criptoForm.invalid) {
        this.dataModalMontoPago.bloqueado = false;
        this.confBotonMontoPago.enProgreso = false;
        this.dataModalMontoPago.error = true;
        this.dataModalMontoPago.errorContenido =
          'Seleccionar Criptomoneda Traducir';
        return;
      }
    }

    try {
      // tslint:disable-next-line: no-string-literal
      this.cuotaMinima = Math['round10'](
        await this.tipoMonedaNegocio
          .convertirMontoEntreMonedas(
            this.variablesGlobales.montoBaseSuspcripcion,
            this.variablesGlobales.tipoDeMonedaBase,
            CodigosCatalogoTipoMoneda.USD
          )
          .toPromise(),
        -2
      );
      let montoDelUsuario = 0;
      const montoIngresado = parseFloat(
        this.confMonedaPicker.inputCantidadMoneda.valor.valorFormateado
      );
      this.montoPago = montoIngresado.toString();
      if (
        (this.confMonedaPicker.selectorTipoMoneda.seleccionado
          .auxiliar as CodigosCatalogoTipoMoneda) ===
        CodigosCatalogoTipoMoneda.USD
      ) {
        montoDelUsuario = montoIngresado;
      } else {
        // tslint:disable-next-line: no-string-literal
        montoDelUsuario = Math['round10'](
          await this.tipoMonedaNegocio
            .convertirMontoEntreMonedas(
              montoIngresado,
              this.confMonedaPicker.selectorTipoMoneda.seleccionado
                .auxiliar as CodigosCatalogoTipoMoneda,
              CodigosCatalogoTipoMoneda.USD
            )
            .toPromise(),
          -2
        );
      }

      if (montoDelUsuario < this.cuotaMinima) {
        // tslint:disable-next-line: no-string-literal
        const cuotaMinimaEnMonedaUsuario = Math['round10'](
          await this.tipoMonedaNegocio
            .convertirMontoEntreMonedas(
              this.variablesGlobales.montoBaseSuspcripcion,
              this.variablesGlobales.tipoDeMonedaBase,
              this.confMonedaPicker.selectorTipoMoneda.seleccionado
                .auxiliar as CodigosCatalogoTipoMoneda
            )
            .toPromise(),
          -2
        );

        this.dataModalMontoPago.bloqueado = false;
        this.confBotonMontoPago.enProgreso = false;
        this.dataModalMontoPago.error = true;
        this.dataModalMontoPago.error = true;
        const textoError =
          await this.internacionalizacionNegocio.obtenerTextoLlave('text64');
        this.dataModalMontoPago.errorContenido =
          textoError +
          ' ' +
          cuotaMinimaEnMonedaUsuario +
          ' ' +
          this.confMonedaPicker.selectorTipoMoneda.seleccionado.auxiliar;
        return;
      }

      this.usuario = this.cuentaNegocio.obtenerUsuarioDelSessionStorage();

      this.usuario.transacciones = [];
      this.usuario.transacciones.push({
        moneda: {
          codNombre: CodigosCatalogoTipoMoneda.USD,
        },
        monto: this.cuotaMinima,
        origen: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO,
        },
        destino: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO,
        },
      });

      if (
        montoDelUsuario > this.cuotaMinima &&
        // tslint:disable-next-line: no-string-literal
        Math['round10'](montoDelUsuario - this.cuotaMinima, -2) >= 0.1
      ) {
        this.usuario.transacciones.push({
          moneda: {
            codNombre: CodigosCatalogoTipoMoneda.USD,
          },
          // tslint:disable-next-line: no-string-literal
          monto: Math['round10'](montoDelUsuario - this.cuotaMinima, -2),
          origen: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA,
          },
          destino: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA,
          },
        });
      }
      (this.usuario.monedaRegistro = {
        codNombre:
          this.confMonedaPicker.selectorTipoMoneda.seleccionado.auxiliar,
      }),
        this.cuentaNegocio.guardarUsuarioEnSessionStorage(this.usuario);

      this.dataModalMontoPago.abierto = true;
      this.dataModalMontoPago.bloqueado = true;
      this.confBotonMontoPago.enProgreso = true;
      this.mostrarMessage = true;

      switch (this.codigoPago) {
        case this.codigosCatalogos.PAYMENTEZ:
          this.dataModalPaymentez.abierto = true;
          this.configurarDialogoPagoErrorPaymentezUsuarioExiste();
          const monto = await this.convertirMontoPaymentez();
          setTimeout(() => {
            this.dataModalMontoPago.abierto = false;
            this.dataModalMontoPago.bloqueado = false;
            this.confBotonMontoPago.enProgreso = false;
            this.mostrarMessage = false;
          }, 8000);
          setTimeout(() => {
            this.cuentaNegocio.crearCuentaPaymentez().subscribe(
              (resp: any) => {
                console.log(resp);
                this.paymentCheckout.open({
                  user_id: resp.usuario._id,
                  user_email: resp.usuario.email,
                  user_phone: '',
                  order_description: 'Suscripción Gazeloook',
                  order_amount: monto,
                  order_vat: 0,
                  order_reference: '#234323411',
                  order_installments_type: -1,
                  order_taxable_amount: 0,
                  order_tax_percentage: 0,
                });
              },
              (error) => {
                console.log(error);
                window.location.reload();
                this.router.navigateByUrl('enrutador-registro');
              }
            );
          }, 4500);
          break;
        case this.codigosCatalogos.PAYMENTEZ2:
          this.dataModalPaymentez.abierto = true;
          this.configurarDialogoPagoErrorPaymentezUsuarioExiste();
          const monto2 = await this.convertirMontoPaymentez();
          setTimeout(() => {
            this.dataModalMontoPago.abierto = false;
            this.dataModalMontoPago.bloqueado = false;
            this.confBotonMontoPago.enProgreso = false;
            this.mostrarMessage = false;
          }, 8000);
          setTimeout(() => {
            this.cuentaNegocio.crearCuentaPaymentez().subscribe(
              (resp: any) => {
                console.log(resp);
                this.paymentCheckout.open({
                  user_id: resp.usuario._id,
                  user_email: resp.usuario.email,
                  user_phone: '',
                  order_description: 'Suscripción Gazeloook',
                  order_amount: monto2,
                  order_vat: 0,
                  order_reference: '#234323411',
                  order_installments_type: -1,
                  order_taxable_amount: 0,
                  order_tax_percentage: 0,
                });
              },
              (error) => {
                console.log(error);
                window.location.reload();
                this.router.navigateByUrl('enrutador-registro');
              }
            );
          }, 4500);
          break;
        case this.codigosCatalogos.TARJETA:
          this.dataModalMontoPago.abierto = false;
          this.dataModalMontoPago.bloqueado = false;
          this.confBotonMontoPago.enProgreso = false;
          this.mostrarMessage = false;
          this.dataModalStripe.abierto = true;
          break;
        case this.codigosCatalogos.CRIPTOMONEDA:
          console.log('Criptomoneda');
          localStorage.setItem('respCoiPaiments', JSON.stringify(null));
          this.dataModalMontoPago.abierto = false;
          this.dataModalMontoPago.bloqueado = false;
          this.confBotonMontoPago.enProgreso = false;
          this.mostrarMessage = false;
          this.dataModalCoinPayments.abierto = true;
          break;
      }
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago().subscribe(
      (res: CatalogoMetodoPagoModel[]) => {
        this.dataLista.cargando = false;
        this.listaMetodoPago = res;
      },
      (error) => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      }
    );
  }

  configurarAppBar(): void {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          },
        },
        nombrePerfil: {
          mostrar: false,
        },
        mostrarDivBack: {
          icono: true,
          texto: false,
        },
        mostrarTextoHome: false,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto1',
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      },
    };
  }

  preperarLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES,
    };
  }

  pagarCoinPayments(): void {
    const currency = this.criptoForm.value.currency2;
    // const monto = await this.convertirMontoPaymentez();
    // console.log(monto);
    if (this.pagoForm.valid) {
      const datosPago: PagoFacturacionEntity = {
        nombres: this.pagoForm.value.nombre,
        telefono: this.pagoForm.value.telefono,
        direccion: this.pagoForm.value.direccion,
        email: this.pagoForm.value.email,
      };

      this.dataModalCoinPayments.abierto = false;
      this.toast.abrirToast(' ', true);
      this.cuentaNegocio
        .crearCuentaCoinPayments(this.codigoPago, datosPago, currency)
        .subscribe(
          (resp) => {
            console.log(resp);
            localStorage.setItem(
              'respCoiPaiments',
              JSON.stringify(resp.coinpayments)
            );
            this.cuentaNegocio.activarCuentaCoinPayments(resp);
            this.toast.cerrarToast();
            this.mostrarRespuestaCoinPayments = true;
          },
          (err) => {
            this.mostrarRespuestaCoinPayments = false;
            this.toast.cerrarToast();
            this.toast.abrirToast('text37');
          }
        );
    } else {
      this.toast.abrirToast('text4');
    }
  }

  pagarStripe(): void {
    if (this.pagoForm.valid) {
      const datosPago: PagoFacturacionEntity = {
        nombres: this.pagoForm.value.nombre,
        telefono: this.pagoForm.value.telefono,
        direccion: this.pagoForm.value.direccion,
        email: this.pagoForm.value.email,
      };
      this.toast.abrirToast('', true);
      this.dataModalStripe.abierto = false;
      this.cuentaNegocio.crearCuenta(this.codigoPago, datosPago).subscribe(
        (pagoModel: PagoModel) => {
          this.toast.cerrarToast();
          this.toast.abrirToast('text21', true);
          this.stripeService
            .confirmCardPayment(pagoModel.idPago, {
              payment_method: {
                card: this.card.element,
                billing_details: {
                  name: datosPago.nombres,
                  email: datosPago.email,
                },
              },
            })
            .subscribe((result) => {
              this.toast.cerrarToast();
              if (result.error) {
                // this.toast.abrirToast(result.error?.message)
                this.activarCuenta(pagoModel.idTransaccion, true);
              } else {
                if (result.paymentIntent?.status === 'succeeded') {
                  this.activarCuenta(pagoModel.idTransaccion);
                }
              }
            });
        },
        (err) => {
          console.log(err);
          this.toast.cerrarToast();
          this.toast.abrirToast('text37');
          setTimeout(() => {
            window.location.reload();
          }, 3000);
        }
      );
    } else {
      this.toast.abrirToast('text4');
    }
  }

  configurarDialogoContenido(): void {
    this.dataModalStripe = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagotarjeta',
    };
    this.dataModalPaymentez = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagopaymentez',
    };
    this.dataModalCoinPayments = {
      titulo: 'PAGAR CON CRIPTOMONEDAS Traducir',
      abierto: false,
      bloqueado: false,
      id: 'pagocoinpayments',
    };
    this.dataModalMontoPago = {
      titulo: 'text74',
      abierto: false,
      bloqueado: false,
      id: 'montopago',
    };
  }

  obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (this.idiomaSeleccionado) {
      this.elementsOptions.locale = this.idiomaSeleccionado
        .codNombre as StripeElementLocale;
    }
  }

  irDocumentosLegales(): void {
    console.log(
      'this.idiomaSeleccionado.codNombre',
      this.idiomaSeleccionado.codNombre
    );

    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `https://d3ubht94yroq8c.cloudfront.net/condiciones-pago/condiciones-pago-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(
      new MouseEvent('click', {
        view: window,
        bubbles: false,
        cancelable: true,
      })
    );
  }

  async activarCuenta(
    idTransaccion: string,
    pagoNoConfirmado: boolean = false
  ): Promise<void> {
    try {
      this.toast.abrirToast('', true);
      const res: UsuarioModel = await this.cuentaNegocio
        .activarCuenta(idTransaccion, pagoNoConfirmado)
        .toPromise();
      if (!res) {
        throw new Error('');
      }
      this.configurarDialogoPago();
      this.cuentaNegocio.eliminarSessionStorage();
      const dataFirebase = await this.auth.signInAnonymously();
      if (!dataFirebase || !dataFirebase.user || !dataFirebase.user.uid) {
        throw new Error('');
      }
      this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(
        dataFirebase.user.uid
      );
      this.toast.cerrarToast();
      this.mostrarModalPagoContinuar = true;
      this._location.replaceState('/');
      // this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES)
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async configurarDialogoPago(): Promise<void> {
    this.configDialoPago = {
      mostrarDialogo: false,
      descripcion: '',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPago.mostrarDialogo = false;
            this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
          },
        },
      ],
    };

    const firstDescri = await this.translateService
      .get('m5v9texto10')
      .toPromise();

    const descripcion =
      firstDescri +
      ' ' +
      this.montoPago +
      ' ' +
      this.confMonedaPicker.selectorTipoMoneda.seleccionado.auxiliar;
    this.configDialoPago.descripcion = descripcion;
    this.descripcionPagoContinuar = descripcion;
  }

  configurarDialogoPagoErrorPaymentez(): void {
    this.configDialoPago = {
      mostrarDialogo: false,
      descripcion: 'text37',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPago.mostrarDialogo = false;
            this.router.navigateByUrl('enrutador-registro');
          },
        },
      ],
    };
  }

  configurarDialogoPagoErrorPaymentezUsuarioExiste(): void {
    this.configDialoPago = {
      mostrarDialogo: false,
      descripcion: 'text37',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPago.mostrarDialogo = false;
            this.enrutadorService.navegarConPosicionFija(
              {
                componente: RegistroComponent,
                params: [
                  {
                    nombre: 'accionEntidad',
                    valor: 'registro',
                  },
                  {
                    nombre: 'codigoTipoPerfil',
                    valor: this.usuario.perfiles[0].tipoPerfil.codigo,
                  },
                ],
              },
              this.informacionEnrutador.posicion
            );
          },
        },
      ],
    };
  }

  configurarToast(): void {
    this.configuracionToast = {
      cerrarClickOutside: false,
      bloquearPantalla: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };
  }

  configurarMonedaPicker(): void {
    const dataMoneda: ResumenDataMonedaPicker =
      this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();

    this.confMonedaPicker = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0',
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMoneda(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
      },
    };
  }

  // Inicializar data catalogo moneda
  async inicializarDataCatalogoMoneda(): Promise<void> {
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(
      (elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
        this.tipoMonedaLocales = elegibles;
        this.confMonedaPicker.selectorTipoMoneda.elegibles = elegibles;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = false;
        this.copiaMonedas = this.confMonedaPicker.selectorTipoMoneda.elegibles;

        if (
          this.codigoPago === this.codigosCatalogos.PAYMENTEZ ||
          this.codigoPago === this.codigosCatalogos.PAYMENTEZ2
        ) {
          this.confMonedaPicker.selectorTipoMoneda.elegibles =
            this.tipoMonedaPaymentez;
        }
      },
      (error) => {
        this.confMonedaPicker.selectorTipoMoneda.elegibles = [];
        this.confMonedaPicker.selectorTipoMoneda.error.llaveTexto = 'text31';
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = true;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
      }
    );
  }

  // Abrir Selector
  abrirSelectorMoneda(): void {
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMoneda();
    this.codMonedasPaymentez();
  }

  seleccionarTipoMoneda(item: ItemSelector): void {
    // Ocultar selector
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPicker.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPicker.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado =
      item.auxiliar || '';
  }

  // Metodos del selector de moneda
  eventoEnSelectorDeTipoMoneda(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMoneda();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMoneda(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMoneda();
        break;
      default:
        break;
    }
  }

  filtrarContenido(buscar: string): any {
    return this.copiaMonedas.filter((place) => {
      const regex = new RegExp(buscar, 'gi');
      return place.nombre.match(regex) || place.nombre.match(regex);
    });
  }

  obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    // tslint:disable-next-line: radix
    const valorEstimado =
      this.valorBaseAPagar && this.valorBaseAPagar.valorNeto
        ? parseInt(this.valorBaseAPagar.valorNeto)
        : 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    };

    const data = {
      valorEstimado:
        this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(
          valorEstimado
        ),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda),
    };
    return data;
  }

  eventoModal(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto') {
        if (!this.dataModalMontoPago.bloqueado) {
          this.dataModalMontoPago.abierto = false;
        }
      }
    });
  }

  ajustarDecimales(type: any, value: any, exp: any): any {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? +value[1] + exp : exp));
  }

  obtenerClasesInputCantidadMoneda(): any {
    const clases = {};
    clases['input-cantidad'] = true;
    clases[this.confMonedaPicker.inputCantidadMoneda.colorFondo.toString()] =
      true;
    clases[this.confMonedaPicker.inputCantidadMoneda.colorTexto.toString()] =
      true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.estiloDelTexto.toString()
    ] = true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.tamanoDelTexto.toString()
    ] = true;

    if (this.confMonedaPicker.inputCantidadMoneda.estiloBorde) {
      clases[this.confMonedaPicker.inputCantidadMoneda.estiloBorde.toString()] =
        true;
    }
    return clases;
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }

  activarCuentaPaymentez(
    monto: number,
    idPago: string,
    pagoNoConfirmado: boolean
  ): void {
    this.configurarDialogoPagoErrorPaymentez();
    this.cuentaNegocio
      .activarCuentaPaymentez(
        this.codigoPago,
        monto,
        idPago,
        pagoNoConfirmado,
        this.tipoMoneda,
        this.cuotaMinima
      )
      .subscribe(
        (resp) => (this.configDialoPago.mostrarDialogo = true),
        (error) => (this.configDialoPago.mostrarDialogo = true)
      );
  }

  async convertirMontoPaymentez(): Promise<number> {
    const montoIngresado = parseFloat(
      this.confMonedaPicker.inputCantidadMoneda.valor.valorFormateado
    );
    this.tipoMoneda =
      this.confMonedaPicker.selectorTipoMoneda.seleccionado.auxiliar;
    this.montoPago = montoIngresado.toString();
    if (
      (this.confMonedaPicker.selectorTipoMoneda.seleccionado
        .auxiliar as CodigosCatalogoTipoMoneda) ===
      CodigosCatalogoTipoMoneda.USD
    ) {
      return montoIngresado;
    } else {
      // tslint:disable-next-line: no-string-literal
      const monto = Math['round10'](
        await this.tipoMonedaNegocio
          .convertirMontoEntreMonedas(
            montoIngresado,
            this.confMonedaPicker.selectorTipoMoneda.seleccionado
              .auxiliar as CodigosCatalogoTipoMoneda,
            CodigosCatalogoTipoMoneda.USD
          )
          .toPromise(),
        -2
      );
      return monto;
    }
  }

  inicializarInputsCompartidos(): void {
    this.inputNombre = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO,
      },
      placeholder: 'text66',
      data: this.pagoForm.controls.nombre,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false,
    };

    this.inputTelefono = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO,
      },
      placeholder: 'text67',
      data: this.pagoForm.controls.telefono,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false,
    };
    this.inputDireccion = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO,
      },
      placeholder: 'text68',
      data: this.pagoForm.controls.direccion,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false,
    };
    this.inputEmail = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO,
      },
      placeholder: 'text69',
      data: this.pagoForm.controls.email,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false,
    };
  }

  configurarDialogosEnConstruccion() {
    // Proyectos seleccionados
    this.confDialogoEnConstruccion = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoEnConstruccion.descripcion.push({
      texto: 'm7v4texto3',
      estilo: {
        color: ColorDelTexto.TEXTOROJOBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });
  }
}
