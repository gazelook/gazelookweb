import { Component, Input, OnInit } from '@angular/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes';
import { EstiloItemPensamiento, TamanoDeTextoConInterlineado, TamanoLista, TipoPensamiento } from '@shared/diseno/enums';
import { BotonCompartido, ConfiguracionToast, DatosLista, PensamientoCompartido } from '@shared/diseno/modelos';
import { PensamientoNegocio } from 'dominio/logica-negocio';
@Component({
    selector: 'app-crear-pensamiento',
    templateUrl: './crear-pensamiento.component.html',
    styleUrls: ['./crear-pensamiento.component.scss']
})
export class CrearPensamientoComponent implements OnInit {
    pensamientoCompartido: PensamientoCompartido
    dataLista: DatosLista //lISTA DE
    botonCrearPensamiento: BotonCompartido;
    idPerfil = "5f3e907015ae58647c0d3e1d"
    esPrivado: boolean; //Para enviar a la base de datos true o false de acuerdo a lo seleccionado por el usuario
    configuracionToast: ConfiguracionToast; //Presentar el toats
    cargando: boolean; //PRESENTAR EL CARGANDO
    @Input() estadosPensamiento: number //Para saber que boton presiono el usuario PUBLICO / PRIVADO

    constructor(
        private pensamientoNegocio: PensamientoNegocio,
    ) {}

    ngOnInit(): void {
        this.cargando = true
        this.iniciarDatos()
    }
    ngOnChanges() {
        this.cargando = true
        this.seleccionarPensamientoMostrar()
        if (this.estadosPensamiento > 0) {
            this.obtenerPensamientos()
        }
    }
    iniciarDatos() {

        this.seleccionarPensamientoMostrar()
        this.dataLista = {
            tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
            lista: [],
            cargarMas: () => this.crearPensamiento()
        }
        this.botonCrearPensamiento = {
            text: 'Enviar',
            tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
            colorTexto: ColorTextoBoton.AMARRILLO,
            tipoBoton: TipoBoton.TEXTO,
            enProgreso: false, ejecutar:
                this.crearPensamiento
        }
        this.configuracionToast = {
            cerrarClickOutside: false,
            mostrarLoader: false,
            mostrarToast: false,
            texto: ""
        }
    }
    seleccionarPensamientoMostrar() {
        switch (this.estadosPensamiento) {
            case 0:
                this.cargando = false
                this.pensamientoCompartido = {
                    tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR,
                    configuracionItem: {
                        estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO
                    },
                    subtitulo: false
                }
                break;
            case 1:
                this.esPrivado = false
                this.pensamientoCompartido = {
                    esLista: true,
                    tipoPensamiento: TipoPensamiento.PENSAMIENTO_PUBLICO_CREACION,
                    configuracionItem: {
                        estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
                        presentarX: true
                    },
                    subtitulo: true
                }
                break;
            case 2:
                this.esPrivado = true
                this.pensamientoCompartido = {
                    esLista: true,
                    tipoPensamiento: TipoPensamiento.PENSAMIENTO_PRIVADO_CREACION,
                    configuracionItem: {
                        estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
                        presentarX: true
                    },
                    subtitulo: true
                }
                break;
            default:
                this.cargando = false
                this.pensamientoCompartido = {
                    tipoPensamiento: TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR,
                    configuracionItem: {
                        estilo: EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO
                    },
                    subtitulo: false
                }
                break;
        }
    }
    //Escuchando el emit() que vienen de pensamiento compartido
    //Obtener pensamientos
    unClick(objeto: object) {
        //Primero se carga un dialogo
        this.actualizarPensamiento(objeto)
        return objeto
    }
    dobleClick(objeto: object) {
        this.actualizarEstadoPensamiento(objeto)
        return objeto
    }
    clickLargo(objeto?: object) {
        this.eliminarPensamiento(objeto)
        return objeto
    }
    crearPensamiento() {

    }
    obtenerPensamientos() {
        this.dataLista.lista.push({ id: '12323', texto: "HOLA ES ES MI CONTACTO", fechaActualizacion: new Date() })
        this.cargando = false

    }

    actualizarPensamiento = (objeto: object) => {}

    actualizarEstadoPensamiento = (objeto: object) => {
        this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: true, mostrarToast: true, texto: "" }
        this.pensamientoNegocio.actualizarEstadoPensamiento(objeto['pensamientoModel']['id'])
            .subscribe(res => {
                this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: false, mostrarToast: false, texto: "" }
                this.dataLista.lista.splice(objeto['index'], 1)
            }, error => {
                this.configuracionToast = { cerrarClickOutside: true, mostrarLoader: false, mostrarToast: true, texto: error }
            })
    }
    eliminarPensamiento = (objeto: object) => {
        this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: true, mostrarToast: true, texto: "." }
        this.pensamientoNegocio.eliminarPensamiento(objeto['pensamientoModel']['id'])
            .subscribe(res => {
                this.configuracionToast = { cerrarClickOutside: false, mostrarLoader: false, mostrarToast: false, texto: "" }
                this.dataLista.lista.splice(objeto['index'], 1)
            }, error => {
                this.configuracionToast = { cerrarClickOutside: true, mostrarLoader: false, mostrarToast: true, texto: error }
            })
    }
}
