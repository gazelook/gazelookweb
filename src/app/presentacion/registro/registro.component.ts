import {Location} from '@angular/common';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {GeneradorId, RegistroService} from '@core/servicios/generales';
import {EnrutadorService} from '@core/servicios/generales/enrutador';
import {NotificacionesDeUsuario} from '@core/servicios/generales/notificaciones';
import {
  AccionEntidad,
  CodigosCatalogoEntidad,
  CodigosCatalogosEstadoPerfiles, CodigosCatalogoTipoAlbum, CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import {TranslateService} from '@ngx-translate/core';
import {
  AppbarComponent, BuscadorModalComponent, ColorTextoBoton,
  DialogoInlineComponent, ItemCirculoComponent,
  ItemRectanguloComponent, ListaBuscadorModalComponent,
  ListaSelectorComponent, PortadaGazeComponent,
  SelectorComponent, TipoBoton, ToastComponent
} from '@shared/componentes';
import {
  AccionesBuscadorLocalidadModal, AccionesItemCircularRectangular,
  AccionesSelector, ColorDeBorde, ColorDeFondo,
  ColorDelTexto, EstilosDelTexto, PaddingIzqDerDelTexto,
  TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoPortadaGaze,
  TipoDialogo, UsoAppBar, UsoItemCircular, UsoItemRectangular
} from '@shared/diseno/enums';
import {
  BotonCompartido, ConfiguracionAppbarCompartida,
  ConfiguracionBuscadorModal, ConfiguracionDialogoInline,
  ConfiguracionDone, ConfiguracionSelector, ConfiguracionToast,
  DialogoCompartido, InfoAccionBuscadorLocalidades, InfoAccionCirRec,
  InfoAccionSelector, InputCompartido, ItemCircularCompartido,
  ItemRectangularCompartido, ItemSelector, PortadaGazeCompartido
} from '@shared/diseno/modelos';
import {AlbumNegocio, CuentaNegocio, PerfilNegocio, UbicacionNegocio} from 'dominio/logica-negocio';
import {PaginacionModel} from 'dominio/modelo';
import {CatalogoTipoPerfilModel} from 'dominio/modelo/catalogos';
import {AlbumModel, PerfilModel, UsuarioModel} from 'dominio/modelo/entidades';
import {RegistroParams} from 'dominio/modelo/parametros';
import {RutasLocales} from '../../rutas-locales.enum';
import {AlbumGeneralComponent} from '../album/album-general/album-general.component';
import {AlbumPerfilComponent} from '../album/album-perfil/album-perfil.component';
import {Enrutador, InformacionEnrutador} from '../enrutador/enrutador.component';
import {MetodoPagoComponent} from '../metodo-pago/metodo-pago.component';
import {TipoDeNavegacion} from './../enrutador/enrutador.component';
import {MenuPrincipalComponent} from './../menu-principal/menu-principal.component';
import {MiCuentaComponent} from './../mi-cuenta/mi-cuenta.component';
import {MetodosPagoComponent} from 'presentacion/metodos-pago/metodos-pago.component';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit, Enrutador {
  @ViewChild('appbar', {static: false}) appbar: AppbarComponent;
  @ViewChild('portadaGaze', {static: false}) portada: PortadaGazeComponent;
  @ViewChild('albumPerfil', {static: false}) albumPerfil: ItemCirculoComponent;
  @ViewChild('albumGeneral', {static: false}) albumGeneral: ItemRectanguloComponent;
  @ViewChild('selectorPaises', {static: false}) selectorPaises: SelectorComponent;
  @ViewChild('selectorPaisesLista', {static: false}) selectorPaisesLista: ListaSelectorComponent;
  @ViewChild('buscadorLocalidades', {static: false}) buscadorLocalidades: BuscadorModalComponent;
  @ViewChild('buscadorLocalidadesLista', {static: false}) buscadorLocalidadesLista: ListaBuscadorModalComponent;
  @ViewChild('dialogoInline', {static: false}) dialogoInline: DialogoInlineComponent;
  @ViewChild('toast', {static: false}) toast: ToastComponent;

  // Utils
  public AccionEntidadEnum = AccionEntidad;
  public codigosCatalogoTipoPerfil = CodigosCatalogoTipoPerfil;
  public codigosCatalogosEstadoPerfiles = CodigosCatalogosEstadoPerfiles;
  public nombreContactoUnico: boolean;
  // Parametros url
  public params: RegistroParams;

  // Parametros internos - generales
  public mostrarCuerpoLoader: boolean; // Oculta o muestra el loader
  public mostrarError: boolean; // Indica si mostrar el error o no
  public mensajeError: string; // Mensaje de error a mostrar
  public traducirNombreContacto: boolean;
  public validadorNombreContactoTraducido: boolean;
  public mostrarSubirFoto: boolean;
  // Parametros internos - configuracion de items y demas
  public confBotonReintentar: BotonCompartido; // Boton de reintentar
  public confAppBar: ConfiguracionAppbarCompartida; // Configuracion del appbar
  public confItemCir: ItemCircularCompartido; // Configuracion item del circulo
  public confItemRec: ItemRectangularCompartido; // Configuracion item del rectangulo
  public confSelector: ConfiguracionSelector; // Configuracion del selector
  public confBuscador: ConfiguracionBuscadorModal; // Configuracion buscador localidades
  public confToast: ConfiguracionToast;  // Configuracion del toast
  public confDone: ConfiguracionDone;  // Configuracion del toast
  public confBotonPago: BotonCompartido; // Boton de pago
  public confBotonHibernar: BotonCompartido; // Boton de hibernar
  public confBotonDesHibernar: BotonCompartido; // Boton de deshibernar
  public confBotonEliminar: BotonCompartido; // Boton de eliminar
  public confDialogoMasPerfiles: ConfiguracionDialogoInline; // Dialogo compartido
  public confDialogoHibernar: DialogoCompartido; // Dialogo de hibernar
  public confDialogoDesHibernar: DialogoCompartido; // Dialogo de deshibernar
  public confDialogoEliminar: DialogoCompartido; // Dialogo de eliminar
  public confPortada: PortadaGazeCompartido;
  public confDialogoSalida: DialogoCompartido;
  public confBotonSubirFoto: BotonCompartido;

  public noCrearMasPerfiles: boolean; // False aparece boton no, true aparece boton payment
  public noHayMasPerfilesDisponibles: boolean;
  public registroForm: FormGroup; // Formulario de registro
  public inputsForm: Array<InputCompartido>; // Configuracion de los inputs
  public botonSubmit: BotonCompartido; // Configuracion del boton compartido
  public tipoPerfil: CatalogoTipoPerfilModel; // Perfil activo
  public usuario: UsuarioModel; // Usuario activo
  public posPerfil: number; // Posicion del perfil en la lista
  public perfil: PerfilModel; // Model del perfil
  public perfilCreado: boolean; // Indica que el perfil ha sido creado y se debe mostrar la info de pago
  public listaResultadosLocalidades: PaginacionModel<ItemSelector>;

  // Utils
  public validadorNombreContacto: boolean;
  public validadorEmail: boolean;
  public validadorFechaNacimiento: boolean;
  public idCapaPrincipal: string;
  public hayMasDeUnPerfil: boolean;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private translateService: TranslateService,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private ubicacionNegocio: UbicacionNegocio,
    private rutaActual: ActivatedRoute,
    private router: Router,
    private route: ActivatedRoute,
    private perfilNegocio: PerfilNegocio,
    private cuentaNegocio: CuentaNegocio,
    private registroService: RegistroService,
    private _location: Location,
    private generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
    private notificacionesDeUsuario: NotificacionesDeUsuario,
    private enrutadorService: EnrutadorService
  ) {
    this.nombreContactoUnico = true;
    this.params = {estado: false};
    this.mostrarCuerpoLoader = false;
    this.mostrarError = false;
    this.mensajeError = '';
    this.mostrarSubirFoto = false;

    this.perfilCreado = false;
    this.noCrearMasPerfiles = false;
    this.noHayMasPerfilesDisponibles = false;
    this.inputsForm = [];
    this.validadorNombreContacto = false;
    this.validadorEmail = false;
    this.idCapaPrincipal = 'capaPrinForm001';
    this.hayMasDeUnPerfil = false;
    this.traducirNombreContacto = false;
    this.validadorNombreContactoTraducido = true;
    this.validadorFechaNacimiento = false;
  }

  ngOnInit(): void {
    window.localStorage.removeItem('respCoiPaiments');
    this.configurarParametrosUrl();
    this.configurarBoton();
    this.configurarBotonDePago();
    this.configurarPortada();
    this.configurarToast();
    this.configurarDone();
    this.configurarDialogoPerfilNormal();
    this.configurarBotonesEstadoDelPerfil();
    this.configurarDialogoHibernar();
    this.configurarDialogoDesHibernar();
    this.configurarDialogoEliminar();
    this.configurarBotonSubirFoto();
    this.configurarListaPaginacionLocalidades();
    this.configurarDialogoConfirmarSalida();


    if (this.params.estado) {
      this.inicializarDataTipoperfil();
      this.inicializarDataDeLaEntidad();
      this.configurarAppBar();
      this.validarSiHayMasDeUnPerfilCreado();
      this.configurarNotificaciones();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  validarAccionBack() {
    const haySesionIniciada = this.cuentaNegocio.sesionIniciada();
    const perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();

    if (
      haySesionIniciada &&
      (
        !perfilSeleccionado ||
        perfilSeleccionado === null
      )
    ) {
      this._location.replaceState('/');
      this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      return;
    }

    this._location.back();
  }

  reintentar() {
    this.inicializarDataDeLaEntidad();
  }

  // Validar si existe el usuario
  inicializarDataDeLaEntidad() {
    this.cambiarValorCapasDelCuerpo(true);

    switch (this.params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        this.definirDataregistroSegunAccionRegistro();
        break;
      case AccionEntidad.CREAR:
        this.definirDataRegistroSegunAccionCrear();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.cambiarValorCapasDelCuerpo(true);
        this.definirDataRegistroSegunAccionActualizar();
        break;
      default:
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
        break;
    }
  }

  definirDataregistroSegunAccionRegistro() {
    try {
      this.perfil = this.perfilNegocio.validarPerfilActivoEnTercio(
        this.informacionEnrutador.posicion,
        this.params.tipoPerfil
      );
      this.inicializarControles();
      this.configurarInputs();
      this.configurarAlbumPerfil();
      this.configurarAlbumGeneral();
      this.configurarSelectorPais();
      // this.configurarBuscadorLocalidades()
      this.cambiarValorCapasDelCuerpo(false, false);
    } catch (error) {

      this.cambiarValorCapasDelCuerpo(false, true, 'text37');
    }
  }

  definirDataRegistroSegunAccionCrear() {
    try {
      this.perfil = this.perfilNegocio.validarPerfilActivoEnTercio(
        this.informacionEnrutador.posicion,
        this.params.tipoPerfil,
        false
      );
      this.inicializarControles();
      this.configurarInputs();
      this.configurarAlbumPerfil();
      this.configurarAlbumGeneral();
      this.configurarSelectorPais();
      // this.configurarBuscadorLocalidades()
      this.cambiarValorCapasDelCuerpo(false, false);
    } catch (error) {
      this.cambiarValorCapasDelCuerpo(false, true, 'text37');
    }
  }

  // Definir data del perfil para Accion actualizar
  async definirDataRegistroSegunAccionActualizar() {
    try {
      this.perfil = await this.perfilNegocio.obtenerDatosDelPerfil(this.params.id).toPromise();
      this.perfilNegocio.guardarPerfilActivoTercio(
        this.informacionEnrutador.posicion,
        this.perfil,
        this.params.tipoPerfil
      );

      // this.reiniciarInformacionParaCambioDePerfil(this.params.tipoPerfil.codigo)
      this.inicializarControles();
      this.configurarInputs();
      this.configurarAlbumPerfil();
      this.configurarAlbumGeneral();
      this.configurarSelectorPais();
      // this.configurarBuscadorLocalidades()
      this.validarSiHayMasDeUnPerfilCreado();
      this.cambiarValorCapasDelCuerpo(false);
    } catch (error) {
      this.cambiarValorCapasDelCuerpo(false, true, 'text31');
    }
  }

  validarInformacionDeUsuarioAntesDeCambiarPagina() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        this.perfilNegocio.guardarInformacionPerfilSegunAccionRegistro(
          this.perfil,
          this.perfil.estado.codigo as CodigosCatalogosEstadoPerfiles,
          this.params.tipoPerfil.codigo,
          this.registroForm,
          this.confSelector.seleccionado,
          this.informacionEnrutador.posicion
        );
        break;
      case AccionEntidad.CREAR:
        this.perfilNegocio.guardarInformacionPerfilSegunAccionCrear(
          this.informacionEnrutador.posicion,
          this.perfil,
          this.registroForm,
          this.confSelector.seleccionado,
          this.perfil.estado.codigo as CodigosCatalogosEstadoPerfiles,
        );
        break;
      case AccionEntidad.ACTUALIZAR:
        // this.perfilNegocio.guardarInformacionPerfilSegunAccionActualizar(
        // 	this.perfil,
        // 	this.registroForm,
        // 	this.confSelector.seleccionado,
        // 	this.confBuscador.seleccionado,
        // 	this.confBuscador.inputPreview.input.auxiliar,
        // )
        break;
      default:
        break;
    }
  }

  // Cambia el valor de las capas del cuerpo
  cambiarValorCapasDelCuerpo(
    mostrarCuerpoLoader: boolean,
    mostrarError: boolean = false,
    mensajeError: string = ''
  ) {
    this.mostrarCuerpoLoader = mostrarCuerpoLoader;
    this.mostrarError = mostrarError;
    this.mensajeError = mensajeError;
  }

  cambiarPerfilActivo(codigoPerfil: string) {
    const perfil: PerfilModel = this.perfilNegocio.obtenerPerfilDelUsuarioSegunTipo(codigoPerfil as CodigosCatalogoTipoPerfil);
    this.perfilNegocio.guardarPerfilActivoEnSessionStorage(perfil);
    this.perfilNegocio.cambiarTipoPerfilActivoSegunCodigo(codigoPerfil as CodigosCatalogoTipoPerfil);
  }

  moverScrollAlInicioSegunCapaId(id: string) {
    const elemento = document.getElementById(id) as HTMLElement;
    if (elemento) {
      elemento.scrollTop = 0;
    }
  }

  configurarParametrosUrl() {
    if (
      !this.informacionEnrutador ||
      !this.informacionEnrutador.params
    ) {
      this.params.estado = false;
      return;
    }


    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'id') {
        this.params.id = item.valor;
      }

      if (item.nombre === 'accionEntidad') {
        this.params.accionEntidad = item.valor;
      }

      if (item.nombre === 'codigoTipoPerfil') {
        this.params.codigoTipoPerfil = item.valor;
      }
    });


    this.params = this.registroService.validarParametrosSegunAccionEntidadEnrutador(this.params);
  }

  irMiCuenta() {
    // this.router.navigateByUrl(RutasLocales.MI_CUENTA)
    this.enrutadorService.navegar(
      {
        componente: MiCuentaComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion
      }
    );
  }

  inicializarDataTipoperfil() {
    this.params.tipoPerfil = this.perfilNegocio.obtenerTipoPerfilSegunCodigo(this.params.codigoTipoPerfil);
    if (
      !this.params.tipoPerfil ||
      this.params.tipoPerfil === null
    ) {
      this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
    }
  }

  // Inicializar controles para el formulario
  inicializarControles() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        this.registroForm = this.registroService.inicializarControlesDelFormulario(this.perfil);
        if (this.registroForm.controls.nombreContactoTraducido.value) {
          this.traducirNombreContacto = true;
        }
        break;
      case AccionEntidad.CREAR:
        this.registroForm = this.registroService.inicializarControlesDelFormulario(this.perfil, false);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.registroForm = this.registroService.inicializarControlesDelFormulario(this.perfil, false);
        if (this.registroForm.controls.nombreContactoTraducido.value) {
          this.traducirNombreContacto = true;
        }
        break;
      default:
        break;
    }
  }

  // Accion atras appbar
  accionAtrasAppbar() {
    if (
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambiosEnElPerfil()
    ) {
      this.configurarDialogoConfirmarSalida(true, false);
      return;
    }

    this.navegarAlBack();
  }

  // Configurar AppBar
  configurarAppBar() {
    const infoAppBar = this.registroService.obtenerParametrosDelAppBarSegunAccionEntidad(
      this.params.accionEntidad,
      this.informacionEnrutador.posicion,
      this.params.tipoPerfil
    );

    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        buscador: {
          mostrar: infoAppBar.mostrarSearchBar,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: infoAppBar.mostrarNombrePerfil,
          llaveTexto: infoAppBar.llaveTextoNombrePerfil
        },
        mostrarDivBack: {
          icono: infoAppBar.mostrarIconoBack,
          texto: infoAppBar.mostrarTextoBack,
        },
        mostrarTextoHome: infoAppBar.mostrarTextoHome,
        subtitulo: {
          mostrar: true,
          llaveTexto: infoAppBar.llaveSubtitulo,
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,

      },
      accionAtras: () => {
        this.accionAtrasAppbar();
      },
      eventoHome: () => {
        if (
          this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
          this.validarSiExistenCambiosEnElPerfil()
        ) {
          this.configurarDialogoConfirmarSalida(true, true);
          return;
        }

        this.navegarAlHome();
      }
    };
  }

  // Configurar inputs
  async configurarInputs() {
    this.inputsForm = this.registroService.configurarInputsDelFormulario(this.registroForm);

    if (this.inputsForm.length > 0) {
      this.inputsForm[0].placeholder = await this.translateService.get('m2v3texto9').toPromise();
      this.inputsForm[1].placeholder = await this.translateService.get('m2v3texto10').toPromise();
      this.inputsForm[2].placeholder = await this.translateService.get('m2v3texto11').toPromise();
      this.inputsForm[3].placeholder = await this.translateService.get('m2v3texto12').toPromise();
      this.inputsForm[4].placeholder = await this.translateService.get('m2v3texto13').toPromise();
      this.inputsForm[5].placeholder = await this.translateService.get('m2v3texto15').toPromise();
      this.inputsForm[6].placeholder = await this.translateService.get('m2v3texto18').toPromise();
      this.inputsForm[7].placeholder = await this.translateService.get('m2v3texto19').toPromise();
      this.inputsForm[11].placeholder = await this.translateService.get('m2v3texto19.1').toPromise();
      this.inputsForm[12].placeholder = await this.translateService.get('m2v3texto17').toPromise();
      this.inputsForm[13].placeholder = await this.translateService.get('m2v3texto9.2').toPromise();
      this.inputsForm[14].placeholder = await this.translateService.get('m2v3texto12.1').toPromise();
      this.inputsForm[10].placeholder = await this.translateService.get('m2v3texto13.1').toPromise();

      if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
        this.inputsForm[7].tipo = 'text';
      }

      if (this.params.accionEntidad === AccionEntidad.CREAR || this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {

        const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();

        if (
          usuario &&
          usuario.perfiles &&
          usuario.perfiles.length === 0
        ) {
          this.inputsForm[1].soloLectura = false;
          this.inputsForm[2].soloLectura = false;
          this.inputsForm[3].soloLectura = false;
          this.inputsForm[4].soloLectura = false;
          this.inputsForm[5].soloLectura = false;
          this.inputsForm[6].soloLectura = false;
          this.inputsForm[7].soloLectura = false;
        }

        if (
          usuario &&
          usuario.perfiles &&
          usuario.perfiles.length > 0
        ) {
          this.inputsForm[1].soloLectura = true;
          this.inputsForm[1].soloLecturaRegistro = true;
          this.inputsForm[2].soloLectura = true;
          this.inputsForm[2].soloLecturaRegistro = true;
          this.inputsForm[3].soloLectura = true;
          this.inputsForm[3].soloLecturaRegistro = true;
          this.inputsForm[4].soloLectura = true;
          this.inputsForm[4].soloLecturaRegistro = true;
          this.inputsForm[5].soloLectura = false;
          this.inputsForm[7].soloLectura = true;
          this.inputsForm[7].soloLecturaRegistro = true;
          this.inputsForm[6].soloLectura = true;
          this.inputsForm[6].soloLecturaRegistro = true;
          this.inputsForm[11].soloLectura = true;
          this.inputsForm[11].soloLecturaRegistro = true;
          this.inputsForm[12].soloLectura = true;
          this.inputsForm[12].soloLecturaRegistro = true;
          this.inputsForm[10].soloLectura = true;
          this.inputsForm[10].soloLecturaRegistro = true;
          this.inputsForm[14].soloLectura = true;
          this.inputsForm[14].soloLecturaRegistro = true;
        }
      }
      this.inputsForm.forEach((input, pos) => {
        if (pos === 0) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarNombreDeContactoUnico(data, false)
          };
        } else if (pos === 3) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarEmailUnico(data, false)
          };
        } else if (pos === 13) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarNombreDeContactoUnico(data, false, true)
          };
        } else if (pos === 14) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarMismoEmail()
          };
        } else if (pos === 10) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarMismoPassword()
          };
        }

      });
    }
  }

  // Configurar boton
  async configurarBoton() {
    this.botonSubmit = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.submitFormPerfil();
      }
    };
  }

  // Configurar portada
  configurarPortada() {
    this.confPortada = {
      tamano: TamanoPortadaGaze.PORTADACORTADA
    };
  }

  determinarEstadoPerfil(): boolean {
    if (
      this.perfil &&
      this.perfil.estado &&
      this.perfil.estado.codigo &&
      this.perfil.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO
    ) {
      return false;
    }

    return true;
  }

  // Configurar album perfil
  async configurarAlbumPerfil() {
    const album: AlbumModel = this.registroService.obtenerPortadaAlbumSegunTipoDelAlbum(this.perfil, CodigosCatalogoTipoAlbum.PERFIL);
    const infoPortada = this.registroService.definirDataItemSegunPortadaAlbum(album);

    this.confItemCir = {
      id: '',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemCircular.CIRPERFILREGISTRO,
      esVisitante: false,
      urlMedia: infoPortada.urlMedia,
      activarClick: this.determinarEstadoPerfil(),
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: this.determinarEstadoPerfil(),
      mostrarLoader: infoPortada.mostrarLoader,
      textoBoton: '',
      eventoEnItem: (data: InfoAccionCirRec) => {
        this.eventoEnItem(data);
      },
      capaOpacidad: {
        mostrar: false
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
      botonConCapaDeOpacidad: (infoPortada.urlMedia.length > 0)
    };

    const texto = await this.translateService.get('m2v3texto1').toPromise();
    const texto2 = await this.translateService.get('m2v3texto2').toPromise();
    this.confItemCir.textoBoton = texto + '\n' + texto2;


  }

  // Configurar album generarl
  async configurarAlbumGeneral() {
    let album: AlbumModel = this.registroService.obtenerPortadaAlbumSegunTipoDelAlbum(this.perfil, CodigosCatalogoTipoAlbum.GENERAL);
    const infoPortada = this.registroService.definirDataItemSegunPortadaAlbum(album);

    this.confItemRec = {
      id: '',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemRectangular.RECPERFILREGISTRO,
      esVisitante: false,
      urlMedia: infoPortada.urlMedia,
      activarClick: this.determinarEstadoPerfil(),
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: this.determinarEstadoPerfil(),
      mostrarLoader: infoPortada.mostrarLoader,
      textoBoton: '',
      descripcion: '',
      mostrarIconoExpandirFoto: true,
      textoCerrarEditarDescripcion: '',
      mostrarCapaImagenSeleccionadaConBorde: false,
      eventoEnItem: (data: InfoAccionCirRec) => {
        this.eventoEnItem(data);
      },
      capaOpacidad: {
        mostrar: false
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
      botonConCapaDeOpacidad: (infoPortada.urlMedia.length > 0),
      usarMarcosDeConfiguracion: true
    };

    const texto = await this.translateService.get('m2v3texto4').toPromise();
    const texto2 = await this.translateService.get('m2v3texto5').toPromise();
    this.confItemRec.textoBoton = texto + ' ' + texto2;
  }

  determinarEstado() {
    let estatus = false;
    const usuario: UsuarioModel = (this.params.accionEntidad === AccionEntidad.REGISTRO) ?
      this.cuentaNegocio.obtenerUsuarioDelSessionStorage() :
      this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
    const perfilActivo: PerfilModel = this.cuentaNegocio.obtenerPerfilActivoDelUsuario(usuario);

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      if (perfilActivo) {
        estatus = true;
      }
    }

    return estatus;
  }

  // Configurar selectores
  async configurarSelectorPais() {
    // Definir direccion
    let item: ItemSelector = {codigo: '', nombre: '', auxiliar: ''};

    if (this.perfil) {
      item = this.registroService.obtenerInformacionDeUbicacion(this.perfil.direcciones);
    }

    const desactivar: boolean = this.determinarEstado();

    this.confSelector = {
      tituloSelector: 'text61',
      mostrarModal: false,
      inputPreview: {
        mostrar: true,
        input: {
          valor: item.nombre,
          placeholder: 'COUNTRY:',
        },
        error: false,
        mensajeError: ''
      },
      seleccionado: item,
      elegibles: [],
      cargando: {
        mostrar: false
      },
      error: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false
      },
      evento: (data: InfoAccionSelector) => this.eventoEnSelector(data)
    };

    this.confSelector.inputPreview.input.placeholder = await this.translateService.get('m2v3texto14').toPromise();
  }

  async configurarBuscadorLocalidades() {
    // Definir direccion
    let item: ItemSelector = {codigo: '', nombre: '', auxiliar: ''};
    let pais: ItemSelector = {codigo: '', nombre: '', auxiliar: ''};

    if (this.perfil) {
      item = this.registroService.obtenerInformacionDeUbicacion(this.perfil.direcciones, false);
      pais = this.registroService.obtenerInformacionDeUbicacion(this.perfil.direcciones);
    }

    const desactivar: boolean = this.determinarEstado();

    this.confBuscador = {
      seleccionado: item,
      inputPreview: {
        mostrar: true,
        input: {
          placeholder: '',
          valor: (item.nombre.length > 0) ? item.nombre : '',
          auxiliar: item.auxiliar,
          desactivar: desactivar
        }
      },
      mostrarModal: false,
      inputBuscador: {
        valor: '',
        placeholder: 'Busca tu localidad',
      },
      resultado: {
        mostrarElegibles: false,
        mostrarCargando: false,
        error: {
          mostrarError: false,
          contenido: '',
          tamanoCompleto: false
        },
        items: []
      },
      pais: pais,
      evento: (data: InfoAccionBuscadorLocalidades) => this.eventoEnBuscador(data)
    };

    this.confBuscador.inputPreview.input.placeholder = await this.translateService.get('m2v3texto15').toPromise();
  }

  configurarToast() {
    this.confToast = {
      // texto: 'Prueba de toas',
      texto: '',
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: true,
      intervalo: 3,
      // bloquearPantalla: true
    };
  }

  configurarDone() {
    this.confDone = {
      mostrarDone: false,
      mostrarLoader: false,
      intervalo: 3
    };
  }

  async configurarBotonDePago() {
    this.confBotonPago = {
      text: 'm2v8texto6',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        const elemento: HTMLElement = document.getElementById('adGoogle') as HTMLElement;
        elemento.click();
        this.perfilNegocio.removerPerfilActivoTercio(this.informacionEnrutador.posicion);
        this.enrutadorService.navegarConPosicionFija(
          {
            componente: MetodosPagoComponent,
            params: []
          },
          this.informacionEnrutador.posicion
        );
      }
    };
  }

  async configurarDialogoPerfilNormal() {
    this.confDialogoMasPerfiles = {
      descripcion: [],
      listaBotones: []
    };

    const descripcion = await this.translateService.get('m2v7texto5').toPromise();

    this.confDialogoMasPerfiles.descripcion.push({
      texto: descripcion,
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
      }
    });
  }

  async inicializarBotonSobrante(
    perfilUno: {
      llave: string,
      codigo: CodigosCatalogoTipoPerfil
    }
  ) {
    this.confDialogoMasPerfiles.listaBotones.push({
      text: perfilUno.llave,
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AZUL,
      enProgreso: false,
      ejecutar: () => {
        this.perfilNegocio.removerPerfilActivoTercio(this.informacionEnrutador.posicion);
        this.enrutadorService.navegarConPosicionFija(
          {
            componente: RegistroComponent,
            params: [
              {
                nombre: 'accionEntidad',
                valor: AccionEntidad.REGISTRO
              },
              {
                nombre: 'codigoTipoPerfil',
                valor: perfilUno.codigo
              }
            ]
          },
          this.informacionEnrutador.posicion
        );
        // this.reiniciarInformacionParaCambioDePerfil(perfilUno.codigo)
      },
    });
  }

  async determinarPerfilesSobrantes() {
    const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelSessionStorage();

    if (!usuario || usuario === null || !usuario.perfiles) {
      return;
    }

    const perfiles: PerfilModel[] = usuario.perfiles;
    const codigosPerfiles: Array<{
      llave: string,
      codigo: CodigosCatalogoTipoPerfil
    }> = [];

    if (perfiles.findIndex(e =>
      e.tipoPerfil.codigo === CodigosCatalogoTipoPerfil.CLASSIC &&
      e.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
    ) < 0) {
      codigosPerfiles.push({
        llave: 'm2v7texto8',
        codigo: CodigosCatalogoTipoPerfil.CLASSIC
      });
    }

    if (perfiles.findIndex(e =>
      e.tipoPerfil.codigo === CodigosCatalogoTipoPerfil.PLAYFUL &&
      e.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
    ) < 0) {
      codigosPerfiles.push({
        llave: 'm2v7texto7',
        codigo: CodigosCatalogoTipoPerfil.PLAYFUL
      });
    }

    if (perfiles.findIndex(e =>
      e.tipoPerfil.codigo === CodigosCatalogoTipoPerfil.SUBSTITUTE &&
      e.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
    ) < 0) {
      codigosPerfiles.push({
        llave: 'm2v7texto6',
        codigo: CodigosCatalogoTipoPerfil.SUBSTITUTE
      });
    }
    if (codigosPerfiles.length > 0) {
      this.noHayMasPerfilesDisponibles = false;
      codigosPerfiles.forEach(codigoPerfil => {
        this.inicializarBotonSobrante(codigoPerfil);
      });

      this.determinarBotonNoBotonPaymente();
      return;
    }

    if (codigosPerfiles.length <= 0) {
      this.noHayMasPerfilesDisponibles = true;
      return;
    }
  }

  async determinarBotonNoBotonPaymente(pos?: number) {
    const configuracion: BotonCompartido = {
      text: 'no',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
      },
    };
    if (pos) {
      this.confDialogoMasPerfiles.listaBotones[pos] = configuracion;
    } else {
      this.confDialogoMasPerfiles.listaBotones[this.confDialogoMasPerfiles.listaBotones.length] = configuracion;
    }

    if (!this.noCrearMasPerfiles) {
      const texto = await this.translateService.get('m2v7texto9').toPromise();
      this.confDialogoMasPerfiles.listaBotones[this.confDialogoMasPerfiles.listaBotones.length - 1].text = texto;
      this.confDialogoMasPerfiles.listaBotones[this.confDialogoMasPerfiles.listaBotones.length - 1].ejecutar = () => {
        this.noCrearMasPerfiles = true;
        // this.mostrarSubirFoto = true
        this.determinarBotonNoBotonPaymente(this.confDialogoMasPerfiles.listaBotones.length - 1);


      };
    } else {
      const texto = await this.translateService.get('m2v7texto10').toPromise();
      this.confDialogoMasPerfiles.listaBotones[this.confDialogoMasPerfiles.listaBotones.length - 1].text = texto;
      this.confDialogoMasPerfiles.listaBotones[this.confDialogoMasPerfiles.listaBotones.length - 1].ejecutar = () => {

        // const elemento: HTMLElement = document.getElementById('adGoogle') as HTMLElement;
        // elemento.click();
        this.perfilNegocio.removerPerfilActivoTercio(this.informacionEnrutador.posicion);
        this.enrutadorService.navegarConPosicionFija(
          {
            componente: MetodosPagoComponent,
            params: []
          },
          this.informacionEnrutador.posicion
        );
      };
    }
  }

  async configurarBotonesEstadoDelPerfil() {
    this.confBotonHibernar = {
      text: 'm2v13texto14',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoHibernar.mostrarDialogo = true;
      }
    };

    this.confBotonDesHibernar = {
      text: 'm2v13texto15',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoDesHibernar.mostrarDialogo = true;
      }
    };

    this.confBotonEliminar = {
      text: 'm2v13texto16',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AZUL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoEliminar.mostrarDialogo = true;
      }
    };
  }

  async configurarDialogoHibernar() {
    this.confDialogoHibernar = {
      mostrarDialogo: false,
      descripcion: 'm2v13texto4',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoHibernar.mostrarDialogo = false;
            this.hibernarPerfil();
          }
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoHibernar.mostrarDialogo = false;
          }
        }
      ]
    };
  }

  async configurarDialogoDesHibernar() {
    this.confDialogoDesHibernar = {
      mostrarDialogo: false,
      descripcion: 'm2v13texto5',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoDesHibernar.mostrarDialogo = false;
            this.desHibernarPerfil();
          }
        },
        {
          text: 'm3v9texto3',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoDesHibernar.mostrarDialogo = false;
          }
        }
      ]
    };
  }

  configurarDialogoEliminar() {
    this.confDialogoEliminar = {
      mostrarDialogo: false,
      descripcion: 'm2v13texto8',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminar.mostrarDialogo = false;
            this.eliminarPerfil();
          }
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminar.mostrarDialogo = false;
          }
        }
      ]
    };
  }

  configurarDialogoConfirmarSalida(
    mostrarDialogo: boolean = false,
    esHome: boolean = false
  ) {
    this.confDialogoSalida = {
      mostrarDialogo: mostrarDialogo,
      descripcion: 'm2v3texto21',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoSalida.mostrarDialogo = false;
            this.submitFormPerfil();
          }
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            if (esHome) {
              this.navegarAlHome();
              return;
            }

            this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
          }
        }
      ]
    };
  }

  configurarListaPaginacionLocalidades() {
    this.listaResultadosLocalidades = {
      lista: [],
      paginaActual: 1,
      totalDatos: 0,
      proximaPagina: true,
    };
  }

  configurarNotificaciones() {
    if (this.params.accionEntidad === AccionEntidad.REGISTRO) {
      this.notificacionesDeUsuario.desconectarDeEscuchaNotificaciones();
      return;
    }

    this.notificacionesDeUsuario.validarEstadoDeLaSesion();
  }

  //
  mostrarBotonesEstadoPerfil() {
    let estato = false;
    if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
      return true;
    }
  }

  // Click en input pais
  async abrirSelectorPaises() {
    try {
      this.confSelector.cargando.mostrar = true;
      this.confSelector.mostrarModal = true;
      const items: ItemSelector[] = await this.ubicacionNegocio.obtenerCatalogoPaisesParaSelector().toPromise();
      if (!items) {
        throw new Error('');
      }

      this.ubicacionNegocio.guardarPaisesDelSelectorEnLocalStorage(items);
      this.confSelector.elegibles = items;
      this.confSelector.cargando.mostrar = false;

      if (this.confSelector.elegibles.length === 0) {
        this.confSelector.cargando.mostrar = false;
        this.confSelector.error.contenido = 'text31';
        this.confSelector.error.mostrarError = true;
      }
    } catch (error) {
      this.confSelector.elegibles = [];
      this.confSelector.cargando.mostrar = false;
      this.confSelector.error.contenido = 'text31';
      this.confSelector.error.mostrarError = true;
    }
  }

  // Click en input localidades
  abrirBuscadorLocalidades() {
    if (!this.confBuscador.pais || this.confBuscador.pais.codigo.length === 0) {
      this.toast.abrirToast('text54');
      return;
    }
    this.confBuscador.mostrarModal = true;
  }

  mostrarErrorEnBuscador(contenido: string, mostrar: boolean, tamanoCompleto: boolean = false) {
    this.confBuscador.resultado.error.contenido = contenido;
    this.confBuscador.resultado.error.tamanoCompleto = tamanoCompleto;
    this.confBuscador.resultado.error.mostrarError = mostrar;
    this.confBuscador.resultado.mostrarCargando = false;
  }

  reiniciarBuscador() {
    this.confBuscador.mostrarModal = false;
    this.confBuscador.inputBuscador.valor = '';
    this.confBuscador.resultado.items = [];
    this.confBuscador.resultado.mostrarElegibles = false;
    this.confBuscador.resultado.mostrarCargando = false;
    this.confBuscador.resultado.error.contenido = '';
    this.confBuscador.resultado.error.tamanoCompleto = false;
    this.confBuscador.resultado.error.mostrarError = false;
  }

  // Buscador localidades
  async buscarLocalidades(
    pais: string,
    query: string,
    reiniciarLista: boolean = true
  ) {
    try {
      this.mostrarErrorEnBuscador('', false);
      this.confBuscador.resultado.mostrarCargando = (reiniciarLista);
      this.confBuscador.resultado.mostrarElegibles = (!reiniciarLista);
      this.confBuscador.resultado.puedeCargarMas = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = (!reiniciarLista);

      if (reiniciarLista) {
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
      }

      const localidades: PaginacionModel<ItemSelector> = await this.ubicacionNegocio.buscarLocalidadesPorNombrePaisConPaginacion(
        25,
        this.listaResultadosLocalidades.paginaActual,
        pais,
        query
      ).toPromise();

      if (!localidades) {
        throw new Error();
      }

      localidades.lista.forEach(item => {
        this.listaResultadosLocalidades.lista.push(item);
      });
      this.listaResultadosLocalidades.proximaPagina = localidades.proximaPagina;

      this.confBuscador.resultado.items = this.listaResultadosLocalidades.lista;
      this.confBuscador.resultado.mostrarElegibles = true;
      this.confBuscador.resultado.puedeCargarMas = true;
      this.confBuscador.resultado.mostrarCargando = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = false;
    } catch (error) {
      this.mostrarErrorEnBuscador('text31', true);
    }
  }

  eventoEnSelector(data: InfoAccionSelector) {
    switch (data.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorPaises();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        // // Buscador
        // this.confBuscador.pais = data.informacion
        // this.confBuscador.seleccionado.codigo = ''
        // this.confBuscador.seleccionado.nombre = ''
        // this.confBuscador.inputPreview.input.valor = ''
        // Selector
        this.confSelector.seleccionado = data.informacion;
        this.confSelector.mostrarModal = false;
        this.confSelector.inputPreview.input.valor = data.informacion.nombre;
        this.confSelector.inputPreview.error = false;
        this.confSelector.quitarMarginAbajo = false;
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        break;
      case AccionesSelector.BUSCAR_PAIS_POR_QUERY:
        break;
      default:
        break;
    }
  }

  eventoEnBuscador(data: InfoAccionBuscadorLocalidades) {
    switch (data.accion) {
      case AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR:
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
        this.abrirBuscadorLocalidades();
        break;
      case AccionesBuscadorLocalidadModal.REALIZAR_BUSQUEDA:
        this.buscarLocalidades(data.informacion.pais, data.informacion.query);
        break;
      case AccionesBuscadorLocalidadModal.CARGAR_MAS_RESULTADOS:
        if (!this.listaResultadosLocalidades.proximaPagina) {
          return;
        }

        this.listaResultadosLocalidades.paginaActual += 1;
        this.buscarLocalidades(data.informacion.pais, data.informacion.query, false);
        break;
      case AccionesBuscadorLocalidadModal.SELECCIONAR_ITEM:
        if (!data.informacion || !data.informacion.item) {
          break;
        }

        this.confBuscador.seleccionado = data.informacion.item;
        this.confBuscador.inputPreview.input.valor = this.confBuscador.seleccionado.nombre;
        this.confBuscador.inputPreview.input.auxiliar = this.confBuscador.seleccionado?.auxiliar;
        this.reiniciarBuscador();
        break;
      case AccionesBuscadorLocalidadModal.CERRAR_BUSCADOR:
        this.reiniciarBuscador();
        break;
      default:
        break;
    }
  }

  irAlAlbumPerfil() {
    const album: AlbumModel = this.albumNegocio.validarAlbumEnPerfilActivo(
      this.informacionEnrutador.posicion,
      CodigosCatalogoTipoAlbum.PERFIL,
      this.perfil
    );


    if (!album) {
      return;
    }

    this.validarInformacionDeUsuarioAntesDeCambiarPagina();
    const titulo: string = this.registroForm.value.nombreContacto && this.registroForm.value.nombreContacto.length > 0 ?
      this.registroForm.value.nombreContacto :
      'm3v7texto8';

    const accionEntidad = (!album._id) ? AccionEntidad.CREAR : AccionEntidad.ACTUALIZAR;

    this.enrutadorService.navegarConPosicionFija(
      {
        componente: AlbumPerfilComponent,
        params: [
          {
            nombre: 'titulo',
            valor: titulo,
          },
          {
            nombre: 'entidad',
            valor: CodigosCatalogoEntidad.PERFIL
          },
          {
            nombre: 'accionEntidad',
            valor: accionEntidad
          }

        ]
      },
      this.informacionEnrutador.posicion
    );
  }

  irAlAlbumGeneral() {
    const album: AlbumModel = this.albumNegocio.validarAlbumEnPerfilActivo(
      this.informacionEnrutador.posicion,
      CodigosCatalogoTipoAlbum.GENERAL,
      this.perfil
    );

    if (!album) {
      return;
    }

    this.validarInformacionDeUsuarioAntesDeCambiarPagina();
    this.validarInformacionDeUsuarioAntesDeCambiarPagina();
    const titulo: string =
      this.registroForm.value.nombreContacto && this.registroForm.value.nombreContacto.length > 0 ?
        this.registroForm.value.nombreContacto :
        'm3v7texto8';

    const accionEntidad = (!album._id) ? AccionEntidad.CREAR : AccionEntidad.ACTUALIZAR;


    this.enrutadorService.navegarConPosicionFija(
      {
        componente: AlbumGeneralComponent,
        params: [
          {
            nombre: 'titulo',
            valor: titulo,
          },
          {
            nombre: 'entidad',
            valor: CodigosCatalogoEntidad.PERFIL
          },
          {
            nombre: 'accionEntidad',
            valor: accionEntidad
          }

        ]
      },
      this.informacionEnrutador.posicion
    );
  }

  // Eventos de click en items
  eventoEnItem(data: InfoAccionCirRec) {
    if (data.accion === AccionesItemCircularRectangular.ABRIR_ADMIN_ALBUM_PERFIL) {
      this.irAlAlbumPerfil();
      return;
    }

    if (data.accion === AccionesItemCircularRectangular.ABRIR_ADMIN_ALBUM_GENERAL) {
      this.irAlAlbumGeneral();
      return;
    }
  }

  // Determinar si se debe validar o no
  ejecutarValidadorDeCampo(valor: string, pos: number): boolean {
    let res = true;
    if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
      if (pos === 0) {
        res = (!(valor.trim() === this.perfil.nombreContacto.trim()));
      }
    }
    return res;
  }

  asignarMensajeDeErrorEnInput(id: string, mensaje: string) {
    if (!(this.inputsForm.length > 0)) {
      return;
    }

    this.inputsForm.forEach(item => {
      if (item.id === id) {
        item.errorPersonalizado = mensaje;
      }
    });
  }

  async validarNombreDeContactoUnico(
    data: { id: string, texto: string },
    forzarSubmit: boolean = true,
    traducirContactName: boolean = false
  ) {
    this.validadorNombreContactoTraducido = false;
    try {
      if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
        console.log('aca error');

        if (forzarSubmit) {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
        }
        return;
      }

      let ejecutar = this.ejecutarValidadorDeCampo(data.texto, 0);
      if (!ejecutar) {
        this.validadorNombreContacto = true;
        return true;
      }
      let expReg = '^[a-zA-ZÀ-ÖØ-öø-ÿ0-9]+$';
      let bandCaracter = false;
      let caracteres = [
        '!', '@', '#', '$', '%', '^', '\'',
        '"', '&', '*', '(', ')', '=', '+',
        '[', '{', ']', '}', '|', '.',
        ':', ';', ',', '<', '.', '>', '?', '/',
        '¬', '~', '`', 'ª', 'º', '£', '¥', '€',
        '"\"',
      ];

      for (const iterator of caracteres) {
        let band = data.texto.includes(iterator);
        if (band) {
          bandCaracter = true;
          break;
        }
      }

      if (bandCaracter) {
        this.asignarMensajeDeErrorEnInput(data.id, 'text83');
        ejecutar = false;
      }

      if (ejecutar) {

        if (!traducirContactName) {

          const estado: object = await this.perfilNegocio.validarNombreDeContactoUnico(data.texto, traducirContactName).toPromise();

          if (!estado['verificaIdioma']) {
            this.traducirNombreContacto = true;
            this.validadorNombreContactoTraducido = false;


          } else {
            if (this.registroForm.value.nombreContactoTraducido !== '') {
              this.registroForm.controls['nombreContactoTraducido'].setValue('');
            }

            this.traducirNombreContacto = false;
            this.validadorNombreContactoTraducido = false;
          }

          if (!estado['nombreContactoUnico']) {
            this.asignarMensajeDeErrorEnInput(data.id, 'text82');
            throw new Error('');
          }
          this.validadorNombreContacto = true;
          return true;
        }

        if (traducirContactName) {
          const estado: object = await this.perfilNegocio.validarNombreDeContactoUnico(data.texto, traducirContactName).toPromise();

          if (!estado['nombreContactoUnico']) {
            this.asignarMensajeDeErrorEnInput(data.id, 'text82');
            throw new Error('');
          }
          if (!estado['verificaIdioma']) {
            this.asignarMensajeDeErrorEnInput(data.id, 'text85');
            throw new Error('');
          }

          this.validadorNombreContactoTraducido = true;
          return true;
        }

      } else {
        return false;
      }

    } catch (error) {
      this.validadorNombreContacto = false;
      this.botonSubmit.enProgreso = false;
      return false;
    }
  }

  async validarEmailUnico(
    data: { id: string, texto: string },
    forzarSubmit: boolean = true
  ) {
    try {
      if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
        if (forzarSubmit) {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
        }
        return;
      }

      const estado: string = await this.cuentaNegocio.validarEmailUnico(data.texto).toPromise();
      if (!estado) {
        throw new Error('');
      }

      this.validadorEmail = true;

      return true;
      // if (this.validadorEmail && this.validadorNombreContacto && forzarSubmit) {
      // 	this.submitFormPerfil()
      // }
    } catch (error) {
      let errorMensaje = 'text6';

      let validar_email = /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
      if (!validar_email.test(data.texto)) {
        errorMensaje = 'text10';

      }
      this.validadorEmail = false;
      this.botonSubmit.enProgreso = false;

      this.asignarMensajeDeErrorEnInput(data.id, errorMensaje);
      return false;
    }
  }

  calcularEdad(fecha_nacimiento) {
    let hoy = new Date();
    let cumpleanos = new Date(fecha_nacimiento);
    let edad = hoy.getFullYear() - cumpleanos.getFullYear();
    let m = hoy.getMonth() - cumpleanos.getMonth();
    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
    }
    return edad;
  }

  async validarFechaNacimiento(
    data: { id: string, texto: string },
    forzarSubmit: boolean = true
  ) {
    try {
      if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
        if (forzarSubmit) {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
        }
        return;
      }


      let edad = this.calcularEdad(data.texto);

      if (edad >= 18) {
        return true;

      } else {
        this.asignarMensajeDeErrorEnInput(data.id, 'text84');
        return false;
      }


    } catch (error) {
      this.validadorFechaNacimiento = false;
      this.botonSubmit.enProgreso = false;
      const errorMensaje = (error && error.message && error.message.length > 0) ? error.message : 'text84';
      this.asignarMensajeDeErrorEnInput(data.id, 'text84');
      return false;
    }

  }

  validarMismoEmail() {

    if (this.inputsForm[14].data.value) {
      if (this.inputsForm[3].data.value !== this.inputsForm[14].data.value) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[3].id, 'text87');
        this.asignarMensajeDeErrorEnInput(this.inputsForm[14].id, 'text87');
        this.botonSubmit.enProgreso = false;
        return;


      } else {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[3].id, '');
        this.asignarMensajeDeErrorEnInput(this.inputsForm[14].id, '');
        return;
      }
    }
  }


  validarMismoPassword() {


    if (this.inputsForm[10].data.value) {

      if (this.inputsForm[10].data.value) {
        if (this.inputsForm[4].data.value !== this.inputsForm[10].data.value) {
          this.asignarMensajeDeErrorEnInput(this.inputsForm[4].id, 'text88');
          this.asignarMensajeDeErrorEnInput(this.inputsForm[10].id, 'text88');
          this.botonSubmit.enProgreso = false;
          return;


        } else {

          this.asignarMensajeDeErrorEnInput(this.inputsForm[4].id, '');
          this.asignarMensajeDeErrorEnInput(this.inputsForm[10].id, '');
          return;
        }
      }

    }
  }

  // submit para el registro
  async submitRegistro() {
    try {

      this.botonSubmit.enProgreso = true;
      if (this.inputsForm[0].data.value.length === 0) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[0].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (this.inputsForm[1].data.value.length === 0) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[1].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (this.inputsForm[2].data.value.length === 0) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[2].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (!this.inputsForm[3].data.value) {
        console.log('no puedo crear 2');
        this.asignarMensajeDeErrorEnInput(this.inputsForm[3].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (!this.inputsForm[14].data.value) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[14].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (this.inputsForm[4].data.value.length === 0) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[4].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (!this.inputsForm[10].data.value) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[10].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }
      if (!this.inputsForm[7].data.value) {
        this.asignarMensajeDeErrorEnInput(this.inputsForm[7].id, 'text1');
        this.botonSubmit.enProgreso = false;
        return;
      }

      if (this.inputsForm[3].data.value) {

        if (this.inputsForm[14].data.value) {
          if (this.inputsForm[3].data.value !== this.inputsForm[14].data.value) {
            this.asignarMensajeDeErrorEnInput(this.inputsForm[3].id, 'text87');
            this.asignarMensajeDeErrorEnInput(this.inputsForm[14].id, 'text87');
            this.botonSubmit.enProgreso = false;
            return;


          }
        }

      }

      if (this.inputsForm[10].data.value) {

        if (this.inputsForm[4].data.value) {
          if (this.inputsForm[4].data.value !== this.inputsForm[10].data.value) {
            this.asignarMensajeDeErrorEnInput(this.inputsForm[4].id, 'text88');
            this.asignarMensajeDeErrorEnInput(this.inputsForm[10].id, 'text88');
            this.botonSubmit.enProgreso = false;
            return;


          }
        }

      }

      // if (!this.inputsForm[14].data.value || !this.inputsForm[10].data.value) {
      //   this.botonSubmit.enProgreso = false;
      //   this.toast.abrirToast('text4');
      //   return;
      //
      // }

      if (!this.validadorFechaNacimiento) {
        let validFechaNac = await this.validarFechaNacimiento(
          {
            id: this.inputsForm[7].id,
            texto: this.inputsForm[7].data.value
          }
        );

        if (!validFechaNac) {
          this.botonSubmit.enProgreso = false;
          return;

        }
      }

      if (!this.validadorEmail) {
        let validEmail = await this.validarEmailUnico(
          {
            id: this.inputsForm[3].id,
            texto: this.inputsForm[3].data.value
          }
        );

        if (!validEmail) {
          this.botonSubmit.enProgreso = false;
          return;

        }
      }

      if (!this.validadorNombreContacto) {

        let validNombreContacto = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[0].id,
            texto: this.inputsForm[0].data.value
          }
        );
        if (!validNombreContacto) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }


      if (this.traducirNombreContacto) {
        console.log('entre1');

        if (this.registroForm.value.nombreContactoTraducido) {
          console.log('entre2');
          if (this.registroForm.value.nombreContactoTraducido.length < 1 || this.registroForm.value.nombreContactoTraducido === '') {
            this.botonSubmit.enProgreso = false;
            this.toast.abrirToast('text4');
            return;
          }
        } else {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
          console.log('entre3');
          return;
        }

        let validNombreContactoTraducido = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[13].id,
            texto: this.inputsForm[13].data.value
          }, false, true
        );

        if (!validNombreContactoTraducido) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }

      let error = true;

      if (
        this.registroForm.value.nombreContacto === '' ||
        this.registroForm.value.nombre === '' ||
        this.registroForm.value.apellido === '' ||
        this.registroForm.value.email === '' ||
        this.registroForm.value.contrasena === '' ||
        this.registroForm.value.fechaNacimiento === '' ||
        this.registroForm.value.nombreContacto === null ||
        this.registroForm.value.nombre === null ||
        this.registroForm.value.apellido === null ||
        this.registroForm.value.email === null ||
        this.registroForm.value.contrasena === null ||
        this.registroForm.value.fechaNacimiento === null

      ) {
        this.toast.abrirToast('text4');
        this.botonSubmit.enProgreso = false;
        return;


      }


      if (this.perfilNegocio.validarFormularioDelPerfil(this.registroForm)) {

        if (this.confSelector.seleccionado && this.confSelector.seleccionado.codigo.length > 3) {

          this.perfilNegocio.guardarInformacionPerfilSegunAccionRegistro(
            this.perfil,
            CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO,
            this.params.tipoPerfil.codigo,
            this.registroForm,
            this.confSelector.seleccionado,
            this.informacionEnrutador.posicion
          );
          this.perfil.estado.codigo = CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO;
          this.determinarPerfilesSobrantes();
          this.perfilCreado = true;
          this.botonSubmit.enProgreso = false;
          error = false;
        }
      }

      if (error) {
        this.botonSubmit.enProgreso = false;
        this.confSelector.inputPreview.error = true;
        this.confSelector.inputPreview.mensajeError = 'text1';
        this.confSelector.quitarMarginAbajo = true;
      }
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }


  // Submit actualizar
  async submitActualizar() {
    try {
      this.botonSubmit.enProgreso = true;
      this.validadorEmail = true;
      if (!this.validadorNombreContacto) {

        let validNombreContacto = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[0].id,
            texto: this.inputsForm[0].data.value
          }
        );
        if (!validNombreContacto) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }


      if (this.traducirNombreContacto) {

        if (this.registroForm.value.nombreContactoTraducido) {
          if (this.registroForm.value.nombreContactoTraducido.length < 2 || this.registroForm.value.nombreContactoTraducido === '') {
            this.botonSubmit.enProgreso = false;
            this.toast.abrirToast('text4');
            return;
          }
        } else {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
          return;
        }

        let validNombreContactoTraducido = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[13].id,
            texto: this.inputsForm[13].data.value
          }, false, true
        );

        if (!validNombreContactoTraducido) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }
      let errorEnCampos = true;
      if (!this.validadorNombreContacto) {
        return;
      }
      this.botonSubmit.enProgreso = false;


      if (this.confSelector.seleccionado && this.confSelector.seleccionado.codigo.length > 0) {
        errorEnCampos = false;

        this.perfilNegocio.guardarInformacionPerfilSegunAccionActualizar(
          this.perfil,
          this.registroForm,
          this.confSelector.seleccionado,
          this.informacionEnrutador.posicion
        );

        const perfilActivo: PerfilModel = this.perfilNegocio.obtenerPerfilActivoTercio(
          this.informacionEnrutador.posicion
        );

        const perfilActualizado: PerfilModel = await this.perfilNegocio.actualizarPerfil(perfilActivo).toPromise();

        if (!perfilActualizado) {
          throw new Error('');
        }

        this.perfil = perfilActualizado;
        this.perfilNegocio.actualizarDataDelPerfilEnElUsuarioDelLocalStorage(this.perfil);
        this.perfilNegocio.actualizarCamposEnLosDemasPerfiles(
          this.perfil.direcciones,
          this.perfil.telefonos
        );
        this.toast.cerrarToast();
        this.confDone.mostrarDone = true;

        setTimeout(() => {
          this.navegarAlBack();
        }, 600);
      }

      if (errorEnCampos) {
        this.toast.abrirToast('text4');
      }
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  // Submit para Accion Crear
  async submitCrear() {
    try {
      this.botonSubmit.enProgreso = true;
      this.validadorEmail = true;


      if (!this.validadorNombreContacto) {

        let validNombreContacto = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[0].id,
            texto: this.inputsForm[0].data.value
          }
        );
        if (!validNombreContacto) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }


      if (this.traducirNombreContacto) {

        if (this.registroForm.value.nombreContactoTraducido) {
          if (this.registroForm.value.nombreContactoTraducido.length < 2 || this.registroForm.value.nombreContactoTraducido === '') {
            this.botonSubmit.enProgreso = false;
            this.toast.abrirToast('text4');
            return;
          }
        } else {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
          return;
        }

        let validNombreContactoTraducido = await this.validarNombreDeContactoUnico(
          {
            id: this.inputsForm[13].id,
            texto: this.inputsForm[13].data.value
          }, false, true
        );

        if (!validNombreContactoTraducido) {
          this.botonSubmit.enProgreso = false;
          return;
        }
      }

      let errorEnCampos = true;
      if (!this.validadorNombreContacto) {
        return;
      }
      this.botonSubmit.enProgreso = false;

      if (this.confSelector.seleccionado && this.confSelector.seleccionado.codigo.length > 0) {
        errorEnCampos = false;

        this.perfilNegocio.guardarInformacionPerfilSegunAccionCrear(
          this.informacionEnrutador.posicion,
          this.perfil,
          this.registroForm,
          this.confSelector.seleccionado,
          CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR
        );

        const perfilActivo: PerfilModel = this.perfilNegocio.obtenerPerfilActivoTercio(
          this.informacionEnrutador.posicion
        );
        const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();

        const perfilCreado: PerfilModel = await this.perfilNegocio.crearPerfilEnElUsuario(
          perfilActivo,
          {id: usuario.id}
        ).toPromise();

        if (!perfilCreado) {
          throw new Error('');
        }

        this.perfil = perfilCreado;
        this.perfilNegocio.insertarPerfilEnElUsuario(this.perfil);
        this.perfilNegocio.actualizarCamposEnLosDemasPerfiles(
          this.perfil.direcciones,
          this.perfil.telefonos
        );
        this.toast.cerrarToast();
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this.navegarAlBack();
        }, 600);
      }

      if (errorEnCampos) {
        this.botonSubmit.enProgreso = false;
        this.toast.abrirToast('text4');
      }
    } catch (error) {
      this.botonSubmit.enProgreso = false;
      this.toast.abrirToast('text37');
    }
  }

  // Submit formulario
  submitFormPerfil() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        this.submitRegistro();
        break;
      case AccionEntidad.CREAR:
        this.submitCrear();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.submitActualizar();
        break;
      default:
        break;
    }
  }

  // Hibernar perfil
  hibernarPerfil() {
    this.eliminarHibernarElPerfil(CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO);
  }

  // Deshibernar perfil
  async desHibernarPerfil() {
    try {
      const perfil: PerfilModel = {
        _id: this.perfil._id,
        estado: {
          codigo: CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
        }
      };


      const data = await this.perfilNegocio.activarPerfil(perfil).toPromise();
      this.perfil.estado.codigo = CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO;
      this.perfilNegocio.guardarPerfilActivoTercio(
        this.informacionEnrutador.posicion,
        this.perfil
      );
      this.perfilNegocio.actualizarDataDelPerfilEnElUsuarioDelLocalStorage(this.perfil);
      this.toast.cerrarToast();
      this.confDone.mostrarDone = true;
      setTimeout(() => {
        this.navegarAlBack();
      }, 600);
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  // Eliminar perfil
  eliminarPerfil() {
    this.eliminarHibernarElPerfil(CodigosCatalogosEstadoPerfiles.PERFIL_ELIMINADO);
  }

  async eliminarHibernarElPerfil(codigoEstado: CodigosCatalogosEstadoPerfiles) {
    try {
      const perfil: PerfilModel = {
        _id: this.perfil._id,
        estado: {
          codigo: codigoEstado
        }
      };

      const perfilUpd = await this.perfilNegocio.eliminarHibernarElPerfil(perfil).toPromise();

      this.perfil.estado.codigo = perfil.estado.codigo;

      if (codigoEstado === CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO) {
        this.perfilNegocio.actualizarDataDelPerfilEnElUsuarioDelLocalStorage(this.perfil);
        this.perfilNegocio.removerPerfilSeleccionado();
      }

      if (codigoEstado === CodigosCatalogosEstadoPerfiles.PERFIL_ELIMINADO) {
        this.perfilNegocio.eliminarDataDelPerfilEnElUsuarioDelLocalStorage(this.perfil);
        this.perfilNegocio.removerPerfilSeleccionado();
      }

      this.toast.cerrarToast();
      this.confDone.mostrarDone = true;
      setTimeout(() => {
        // this.enrutadorService.navegar(
        // 	{
        // 		componente: MenuPrincipalComponent,
        // 		params: [
        // 		]
        // 	},
        // 	{
        // 		estado: true,
        // 		posicicion: this.informacionEnrutador.posicion,
        // 		extras: {
        // 			tipo: TipoDeNavegacion.NORMAL,
        // 		}
        // 	}
        // )
        this.navegarAlBack();
      }, 600);
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  validarSiHayMasDeUnPerfilCreado() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        this.hayMasDeUnPerfil = this.perfilNegocio.validarSiHayMasDeUnPerfilCreado();
        break;
      case AccionEntidad.CREAR:
        this.hayMasDeUnPerfil = this.perfilNegocio.validarSiHayMasDeUnPerfilCreado(false);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.hayMasDeUnPerfil = this.perfilNegocio.validarSiHayMasDeUnPerfilCreado(false);
        break;
      default:
        this.hayMasDeUnPerfil = false;
        break;
    }
  }

  validarSiExistenCambiosEnElPerfil(): boolean {
    const perfilAux: PerfilModel = this.perfilNegocio.guardarInformacionPerfilSegunAccionActualizarSinSessionStorage(
      {...this.perfil},
      this.registroForm,
      this.confSelector.seleccionado
    );

    const resultado: boolean = this.perfilNegocio.validarSiExistenCambiosEnElPerfil(
      perfilAux,
      this.informacionEnrutador.posicion
    );

    return resultado;
  }

  navegarAlBack() {
    this.perfilNegocio.removerPerfilActivoTercio(this.informacionEnrutador.posicion);
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  navegarAlHome() {
    this.enrutadorService.navegar(
      {
        componente: MenuPrincipalComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL,
        }
      }
    );
  }


  // FOTOS

  async configurarBotonSubirFoto() {
    this.confBotonSubirFoto = {
      text: 'UPLOAD PHOTO',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      enProgreso: false,
      ejecutar: () => {
        // this.router.navigateByUrl(RutasLocales.METODO_PAGO)
        // this.eventoEnitemFuncion
        // AccionesItemCircularRectangular.TOMAR_FOTO
        // this.eventoEnItemFoto()
      }
    };
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}
