import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  EnrutadorService,
  MonedaPickerService,
  NotificacionesDeUsuario,
} from '@core/servicios/generales';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
  CodigosCatalogosMetodosDePago,
  CodigosEstadoMetodoPago,
} from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { ToastComponent } from '@shared/componentes';
import { UsoAppBar } from '@shared/diseno/enums';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import {
  TamanoColorDeFondo,
  TamanoLista,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import {
  BotonCompartido,
  ConfiguracionAppbarCompartida,
  ConfiguracionDone,
  ConfiguracionToast,
} from '@shared/diseno/modelos';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import {
  ConfiguracionMonedaPicker,
  ValorBase,
} from '@shared/diseno/modelos/moneda-picker.interface';
import {
  StripeCardElementOptions,
  StripeElementLocale,
  StripeElementsOptions,
} from '@stripe/stripe-js';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import {
  CuentaNegocio,
  IdiomaNegocio,
  PagoNegocio,
  PerfilNegocio,
  TipoMonedaNegocio,
} from 'dominio/logica-negocio';
import { CatalogoMetodoPagoModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import { PerfilModel, UsuarioModel } from 'dominio/modelo/entidades';
import { StripeCardComponent, StripeService } from 'ngx-stripe';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
import {
  Enrutador,
  InformacionEnrutador,
  TipoDeNavegacion,
} from './../enrutador/enrutador.component';

@Component({
  selector: 'app-cuota-extra',
  templateUrl: './cuota-extra.component.html',
  styleUrls: ['./cuota-extra.component.scss'],
})
export class CuotaExtraComponent implements OnInit, Enrutador {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  public cardOptions: StripeCardElementOptions = {
    hidePostalCode: true,
    style: {
      base: {
        color: '#bec6ca',
        fontSmoothing: 'antialiased',
        fontSize: '18px',
        fontFamily: 'Source Sans Pro, sans-serif',
        '::placeholder': {
          color: '#bec6ca',
        },
        fontWeight: 'bold',
      },
      invalid: {
        color: '#F5350E',
        iconColor: '#F5350E',
      },
    },
  };
  public elementsOptions: StripeElementsOptions = {
    locale: 'en',
  };

  @ViewChild('toast', { static: false }) toast: ToastComponent;

  public perfilSeleccionado: PerfilModel;
  public errorMessageTarjeta = '';
  public confDone: ConfiguracionDone;

  public idInputCantidadAPagar: string;
  public confMonedaPicker: ConfiguracionMonedaPicker;
  public valorBaseAPagar: ValorBase;
  public copiaMonedas: any;
  private montoPagar: any;
  private tipoMoneda: string;
  public configuracionToast: ConfiguracionToast;
  private idTransaccion: string;

  public configBotonPagoExtra: BotonCompartido;
  public isChecked: boolean;

  //
  public confAppBar: ConfiguracionAppbarCompartida;
  idiomaSeleccionado: CatalogoIdiomaEntity;
  private usuario: UsuarioModel;
  public dataLista: DatosLista;
  public listaMetodoPago: CatalogoMetodoPagoModel[];
  public confLinea: LineaCompartida;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;
  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public metodoPagoCriptomoneda: string =
    CodigosCatalogosMetodosDePago.CRIPTOMONEDA;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private enrutadorService: EnrutadorService,
    private monedaPickerService: MonedaPickerService,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private perfilNegocio: PerfilNegocio,
    private pagonegocio: PagoNegocio,
    private stripeService: StripeService,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private cuentaNegocio: CuentaNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private router: Router,
    private idiomaNegocio: IdiomaNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
    private pagoNegocio: PagoNegocio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.isChecked = false;
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado();
    if (this.perfilSeleccionado) {
      this.configurarAppBar();
      this.obtenerIdioma();
      this.prepararLista();
      this.configurarLinea();
      this.obtenerCatalogoMetodosPago();
      this.variablesGlobales.mostrarMundo = false;
      this.notificacionesUsuario.validarEstadoDeLaSesion(false);
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  // Métodos pago
  private obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago().subscribe(
      (resp: CatalogoMetodoPagoModel[]) => {
        console.log('RESP', resp);

        this.dataLista.cargando = false;
        this.listaMetodoPago = resp;
      },
      (error) => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      }
    );
  }

  
  public navegarAtras(event): void {
    console.log(event);
    this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
  }

  //  conf linea verde
  private configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false,
    };
  }

  private prepararLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES,
    };
  }

  obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (this.idiomaSeleccionado) {
      this.elementsOptions.locale = this.idiomaSeleccionado
        .codNombre as StripeElementLocale;
    }
  }
  // implements enrutamiento metodo
  configurarInformacionDelEnrutador(info: InformacionEnrutador): void {
    this.informacionEnrutador = info;
  }

  configurarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarAppBar(): void {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [],
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            },
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto16',
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          },
        },
      },
    };
  }
}
