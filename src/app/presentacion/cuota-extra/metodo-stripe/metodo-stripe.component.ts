import {Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy} from '@angular/core';
import {catchError, tap, switchMap} from 'rxjs/operators';
import {throwError} from 'rxjs';

import {CatalogoMetodoPagoModel, CatalogoTipoMonedaModel, PagoFacturacionModelDonacionProyectos} from 'dominio/modelo/catalogos';
import {CuentaNegocio, IdiomaNegocio, PagoNegocio, ProyectoNegocio, TipoMonedaNegocio} from 'dominio/logica-negocio';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {EnrutadorService, MonedaPickerService} from '@core/servicios/generales';
import {ColorTextoBoton, ModalContenido, TipoBoton, ToastComponent} from '@shared/componentes';
import {
  BotonCompartido, ConfiguracionDone,
  ConfiguracionMonedaPicker,
  ConfiguracionToast,
  InfoAccionSelector,
  ItemSelector, ResumenDataMonedaPicker,
  ValorBase
} from '@shared/diseno/modelos';
import {StripeCardComponent, StripeService} from 'ngx-stripe';
import {StripeCardElementChangeEvent, StripeCardElementOptions, StripeElementLocale, StripeElementsOptions} from '@stripe/stripe-js';
import {CatalogoIdiomaEntity} from 'dominio/entidades/catalogos';
import {CodigosCatalogosMetodosDePago, CodigosCatalogoTipoMoneda, CodigosEstadoMetodoPago} from '@core/servicios/remotos/codigos-catalogos';
import {DonacionProyectosModel, UsuarioModel} from 'dominio/modelo/entidades';
import {
  AccionesSelector,
  ColorDeBorde, ColorDeFondo,
  ColorDelTexto,
  EspesorDelBorde,
  EstilosDelTexto,
  TamanoDeTextoConInterlineado
} from '@shared/diseno/enums';
import {InformacionEnrutador} from 'presentacion/enrutador/enrutador.component';

@Component({
  selector: 'app-metodo-stripe',
  templateUrl: './metodo-stripe.component.html',
  styleUrls: ['./metodo-stripe.component.scss']
})
export class MetodoStripeComponent implements OnInit, OnDestroy {
  @Input() item: CatalogoMetodoPagoModel;
  @Input() idProyecto: string;
  @Output() navegarAtras: EventEmitter<boolean> = new EventEmitter<boolean>();


  @ViewChild('toast', {static: false}) toast: ToastComponent;
  public configuracionToast: ConfiguracionToast;

  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  cardOptions: StripeCardElementOptions = {
    hidePostalCode: true,
    style: {
      base: {
        color: '#ffffff',
        fontSmoothing: 'antialiased',
        fontSize: '18px',
        fontFamily: 'Source Sans Pro, sans-serif',
        '::placeholder': {
          color: '#ffffff',
        },
        fontWeight: 'bold',
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
      },
    },
  };
  elementsOptions: StripeElementsOptions = {
    locale: 'en',
  };

  public informacionEnrutador: InformacionEnrutador;

  public idiomaSeleccionado: CatalogoIdiomaEntity;

  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;

  public tipoMonedaLocales: ItemSelector[] = [];

  public montoPagarStripe: number;
  public monedaSeleccionadaStripe: string;

  // Modals
  public dataModalStripe: ModalContenido;
  public dataModalMontoStripePago: ModalContenido;

  public codigoPagoStripe: string;

  // Input Moneda
  public confMonedaPickerStripe: ConfiguracionMonedaPicker;
  public idInputCantidadAPagar: string;

  public valorBaseAPagar: ValorBase;

  public confBotonMontoPagoStripe: BotonCompartido;

  public terminosPagoStripe = false;

  private usuario: UsuarioModel;

  public confDone: ConfiguracionDone;

  public idTransaccion: string;


  constructor(
    private pagoNegocio: PagoNegocio,
    private idiomaNegocio: IdiomaNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private monedaPickerService: MonedaPickerService,
    private cuentaNegocio: CuentaNegocio,
    private enrutadorService: EnrutadorService,
    private proyectoNegocio: ProyectoNegocio,
    private stripeService: StripeService,
    
  ) {
    this.idInputCantidadAPagar = 'KJ21-0982jjs';
  }

  ngOnInit(): void {
    this.obtenerIdioma();
    this.configurarDone();
    this.obtenerUsuarioCuentaNegocio();
    this.funcionRedondear();
    this.configurarMonedaPickerStripe();
    this.configurarDialogoContenido();
    this.confDialogosMontoPagar();
    this.configurarToast();
    this.configurarBotonStripe();
  }

  ngOnDestroy(): void {
    this.navegarAtras.unsubscribe();
  }

  public navegarMetodoPagoStripe(metodoPago: CatalogoMetodoPagoModel): void {
    metodoPago.codigo === CodigosCatalogosMetodosDePago.TARJETA ?
      this.dataModalMontoStripePago.abierto = true :
      this.dataModalMontoStripePago.abierto = false;
    this.codigoPagoStripe = metodoPago.codigo;
  }

  private configurarBotonStripe(): void {
    this.confBotonMontoPagoStripe = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        console.log('ejecutar metodo-stripe');
        this.confirmarMontoPagoStripe().then();
      },
    };
  }

  private async confirmarMontoPagoStripe(): Promise<void> {
    const montoIngresadoStripe = parseFloat(this.confMonedaPickerStripe.inputCantidadMoneda.valor.valorFormateado);
    const codigoMonedaStripe = this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado.codigo;
    this.monedaSeleccionadaStripe = this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado.auxiliar;

    this.dataModalMontoStripePago.bloqueado = true;
    this.confBotonMontoPagoStripe.enProgreso = true;
    this.dataModalMontoStripePago.error = false;
    this.dataModalMontoStripePago.errorContenido = '';

    if (!(montoIngresadoStripe > 0)) {
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalMontoStripePago.error = true;
      this.dataModalMontoStripePago.errorContenido = 'text40';
      return;
    }
    if (!(codigoMonedaStripe && this.monedaSeleccionadaStripe)) {
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalMontoStripePago.error = true;
      this.dataModalMontoStripePago.errorContenido = 'text41';
      return;
    }

    try {
      this.montoPagarStripe = await this.convertirMonto(montoIngresadoStripe, this.monedaSeleccionadaStripe);

      if (!this.terminosPagoStripe) {
        this.dataModalMontoStripePago.bloqueado = false;
        this.confBotonMontoPagoStripe.enProgreso = false;
        this.dataModalMontoStripePago.error = true;
        this.dataModalMontoStripePago.errorContenido = 'text101';
        return;
      }

      this.dataModalMontoStripePago.abierto = false;
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalStripe.abierto = true;
      this.pagarCuotaExtra();

    } catch (error) {
      console.log('error en stripe', error);
      this.toast.abrirToast('text37');
    }

  }

  private pagarCuotaExtra(): void {
    this.toast.abrirToast('', true);
    this.dataModalStripe.abierto = false;
    const datosPago: PagoFacturacionModelDonacionProyectos = {
      nombres: this.usuario.perfiles[0].nombre,
      email: this.usuario.email,
      telefono: this.usuario.perfiles[0].telefonos[0]?.numero || '09xxxxxxxx',
      direccion: this.usuario.perfiles[0].direcciones[0].pais.nombre,
      monto: this.montoPagarStripe,
    };
    const dataEnviar = {
      usuario: {
        _id: this.usuario.id,
      },
      metodoPago: {
        codigo: CodigosCatalogosMetodosDePago.TARJETA,
      },
      transacciones: [
        {
          monto: this.montoPagarStripe,
          moneda: {
            codNombre: CodigosCatalogoTipoMoneda.USD,
          },
        },
      ],
      datosFacturacion: {
        nombres: this.usuario.perfiles[0].nombre,
        email: this.usuario.email,
        telefono: this.usuario.perfiles[0].telefonos[0]?.numero,
        direccion: this.usuario.perfiles[0].direcciones[0].pais.nombre,
        monto: this.montoPagarStripe,
      },

      monedaRegistro: {
        codNombre: this.monedaSeleccionadaStripe,
      },
      email: this.usuario.email,
    };
    const dataStripe = {
      card: this.card.element,
      billing_details: {
        name: this.usuario.perfiles[0].nombre,
        email: this.usuario.email,
      },
    };

    this.pagoNegocio
      .crearOrdenPagoExtra(dataEnviar)
      .pipe(
        tap((data) => (this.idTransaccion = data.idTransaccion)),
        switchMap((data) =>
          this.stripeService.confirmCardPayment(data.idPago, {
            payment_method: dataStripe,
          })
        ),
        catchError((e) => {
          if (e) {
            this.toast.abrirToast(e);
            this.confBotonMontoPagoStripe.enProgreso = false;
            setTimeout(() => {
              window.location.reload();
            }, 3000);
            return throwError(e);
          }
          return;
        })
      )
      .subscribe((resp) => {
        if (resp.error) {
          this.toast.abrirToast(resp.error.message);
          this.dataModalMontoStripePago.error = true;
          this.dataModalMontoStripePago.errorContenido = resp.error.message;
          this.confBotonMontoPagoStripe.enProgreso = false;
        }
        if (resp.paymentIntent.status !== 'succeeded') {
          this.toast.abrirToast('text98');
          this.confBotonMontoPagoStripe.enProgreso = false;
          return;
        }
        this.pagoNegocio
          .validarPagoExtra(this.idTransaccion)
          .subscribe((validarResp: boolean) => {
            if (validarResp) {
              this.confDone.mostrarDone = true;
              // this.toast.abrirToast('text97');
              setTimeout(() => {
                // this.router.navigate([RutasLocales.MENU_PRINCIPAL]);
                console.log('voy a emitir');
                
                this.navegarAtras.emit(true);
              }, 3000);
            } else {
              this.confDone.mostrarDone = false;
              this.toast.abrirToast('text98');
              setTimeout(() => {
                // this.router.navigate([RutasLocales.MENU_PRINCIPAL]);
                this.navegarAtras.emit(true);
              }, 3000);
            }
            this.confBotonMontoPagoStripe.enProgreso = false;
          });
      });
  }

  configurarDone(): void {
    this.confDone = {
      mostrarDone: false,
      intervalo: 4000,
      mostrarLoader: false
    };
  }

//  Modals
  private configurarDialogoContenido(): void {
    this.dataModalStripe = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagotarjeta'
    };
  }

  private confDialogosMontoPagar(): void {
    this.dataModalMontoStripePago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'montopagostripe'
    };
  }

  public eventoModalStripe(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto-stripe') {
        if (!this.dataModalMontoStripePago.bloqueado) {
          this.dataModalMontoStripePago.abierto = false;
        }
      }
    });
  }

//  Input moneda
  public obtenerClasesInputCantidadMoneda(): any {
    const clases = {};
    clases['input-cantidad'] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.colorFondo.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.colorTexto.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.estiloDelTexto.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.tamanoDelTexto.toString()] = true;
    if (this.confMonedaPickerStripe.inputCantidadMoneda.estiloBorde) {
      clases[this.confMonedaPickerStripe.inputCantidadMoneda.estiloBorde.toString()] = true;
    }
    return clases;
  }

  private configurarMonedaPickerStripe(): void {
    const dataMoneda: ResumenDataMonedaPicker = this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerStripe = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0'
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15'
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO
          }
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) => this.eventoEnSelectorDeTipoMonedaStripe(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: ''
        }
      }
    };
  }


  private async convertirMonto(monto, tipoMoneda): Promise<number> {
    const montoIngresado = monto;
    if (tipoMoneda as CodigosCatalogoTipoMoneda === CodigosCatalogoTipoMoneda.USD) {
      return montoIngresado;
    } else {
      // tslint:disable-next-line: no-string-literal
      return Math['round10'](
        await this.tipoMonedaNegocio.convertirMontoEntreMonedas(
          montoIngresado,
          tipoMoneda as CodigosCatalogoTipoMoneda,
          CodigosCatalogoTipoMoneda.USD
        ).toPromise(),
        -2
      );
    }
  }

  private obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    const valorEstimado = (this.valorBaseAPagar && this.valorBaseAPagar.valorNeto) ? parseInt(this.valorBaseAPagar.valorNeto, 0) : 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    };

    return {
      valorEstimado: this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(valorEstimado),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda)
    };
  }

  // Metodos del selector de moneda
  private eventoEnSelectorDeTipoMonedaStripe(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMonedaStripe();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMonedaStripe(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMonedaStripe();
        break;
      default:
        break;
    }
  }

  private abrirSelectorMonedaStripe(): void {
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMonedaStripe();
  }

  // Metodos del selector de moneda
  seleccionarTipoMonedaStripe(item: ItemSelector): void {
    // Ocultar selector
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPickerStripe.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado = item.auxiliar || '';
  }

  private inicializarDataCatalogoMonedaStripe(): void {
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(elegibles => {
      this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
      this.tipoMonedaLocales = elegibles;
      this.confMonedaPickerStripe.selectorTipoMoneda.elegibles = elegibles;
      this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
      this.confMonedaPickerStripe.selectorTipoMoneda.error.mostrar = false;
    }, error => {
      this.confMonedaPickerStripe.selectorTipoMoneda.elegibles = [];
      this.confMonedaPickerStripe.selectorTipoMoneda.error.llaveTexto = 'text31';
      this.confMonedaPickerStripe.selectorTipoMoneda.error.mostrar = true;
      this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
    });
  }

  public onChangeStripeCard(event: StripeCardElementChangeEvent): void {
    if (event.error) {
      this.dataModalMontoStripePago.bloqueado = false;
      this.dataModalMontoStripePago.error = true;
      this.dataModalMontoStripePago.errorContenido = event.error.message;
      return;
    } else {
      this.dataModalMontoStripePago.bloqueado = false;
      this.dataModalMontoStripePago.error = false;
      this.dataModalMontoStripePago.errorContenido = '';
    }
  }

  private obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (this.idiomaSeleccionado) {
      this.elementsOptions.locale = this.idiomaSeleccionado.codNombre as StripeElementLocale;
    }
  }

  public checkValueTerminosPagoStripe(_: any): void {
  }

  obtenerUsuarioCuentaNegocio(): void {
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
  }

  private configurarToast(): void {
    this.configuracionToast = {
      cerrarClickOutside: false,
      bloquearPantalla: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: ''
    };
  }

  public irDocumentosLegales(): void {
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `https://d3ubht94yroq8c.cloudfront.net/condiciones-pago/condiciones-pago-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }

  private funcionRedondear(): void {
    // tslint:disable-next-line: no-string-literal
    if (!Math['round10']) {
      // tslint:disable-next-line: no-string-literal
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp);
      };
    }
  }

  ajustarDecimales(type: any, value: any, exp: any): any {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

}
