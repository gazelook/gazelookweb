import { Component, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService, GeneradorId } from '@core/servicios/generales';
import {
  AccionEntidad, CodigoEstadoParticipanteAsociacion, CodigosCatalogoEntidad, CodigosCatalogoTipoAlbum, CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes';
import {
  AlturaResumenPerfil, AnchoLineaItem,
  ColorDeBorde, ColorDeFondo, ColorDelTexto, ColorFondoAleatorio, ColorFondoLinea, EspesorLineaItem, EstiloItemPensamiento, EstilosDelTexto, PaddingLineaVerdeContactos, TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista, TipoPensamiento, UsoAppBar, UsoItemCircular, UsoItemListaContacto, UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida, ConfiguracionItemBuscadorProyectoNoticia, ConfiguracionItemListaContactosCompartido, ConfiguracionLineaVerde, ConfiguracionResumenPerfil, CongifuracionItemProyectosNoticias,
  DataContacto, DatosLista, ItemCircularCompartido, ModoBusqueda
} from '@shared/diseno/modelos';
import {
  AlbumNegocio, NoticiaNegocio, PerfilNegocio, ProyectoNegocio
} from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import {
  AlbumModel, NoticiaModel, PerfilModel, ProyectoModel
} from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { ChatGazeComponent } from 'presentacion/gazing/chat-gaze/chat-gaze.component';
import { PublicarComponent as PublicarNoticiaComponent } from 'presentacion/noticias/publicar/publicar.component';
import { PerfilComponent } from 'presentacion/perfiles/perfil/perfil.component';
import { PublicarComponent } from 'presentacion/proyectos/publicar/publicar.component';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss']
})
export class BuscarComponent implements OnInit, Enrutador {

  public CodigosCatalogoEntidadEnum = CodigosCatalogoEntidad
  public codigoEntidad: CodigosCatalogoEntidad
  public estadoParams: boolean
  public perfilSeleccionado: PerfilModel

  // Listas
  public paginacionProyectos: PaginacionModel<ConfiguracionItemBuscadorProyectoNoticia>
  public paginacionNoticias: PaginacionModel<ConfiguracionItemBuscadorProyectoNoticia>
  public paginacionContactos: PaginacionModel<ConfiguracionItemListaContactosCompartido>
  public paginacionPerfiles: PaginacionModel<ConfiguracionItemListaContactosCompartido>
  public listaPensamientos: DatosLista

  // Interno
  public mostrarLoader: boolean
  public mostrarCargandoPequeno: boolean
  public mostrarError: boolean
  public mensajeError: string
  public puedeCargarMas: boolean
  public idCapaCuerpo: string
  public idPerfilActivo: string
  public idAsociacionActivo: string
  public mostrarNoHayItems: boolean

  // Configuraciones
  public confAppbar: ConfiguracionAppbarCompartida
  public confResumenPerfil: ConfiguracionResumenPerfil

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private perfilNegocio: PerfilNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private albumNegocio: AlbumNegocio,
    private noticiaNegocio: NoticiaNegocio,
    private generadorId: GeneradorId,
    private enrutadorService: EnrutadorService
  ) {
    this.estadoParams = false
    this.mostrarLoader = false
    this.mostrarError = false
    this.mensajeError = ''
    this.puedeCargarMas = true
    this.mostrarCargandoPequeno = false
    this.idCapaCuerpo = 'listaConScroll'
    this.idPerfilActivo = ''
    this.idAsociacionActivo = ''
    this.mostrarNoHayItems = false
  }

  ngOnInit(): void {
    this.configurarEntidad()
    this.configurarPerfilSeleccionado()
    this.configurarListaPensamientos()
    this.configurarResumenPerfil(false, false, false, '', '', '', UsoItemCircular.CIRCONTACTO)

    if (this.estadoParams && this.perfilSeleccionado) {
      this.configurarAppbar()
      this.configurarListas()
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  configurarEntidad() {

    if (!this.informacionEnrutador) {
      this.estadoParams = false
      return
    }

    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'codigoEntidad') {
        this.codigoEntidad = item.valor
        this.estadoParams = true
      }
    })
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppbar() {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.navegarAlHome(),
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarTextoHome: true,
        mostrarLineaVerde: true,
        tamanoColorFondo: this.obtenerTamanoColorDeFondo(),
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm3v8texto2'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            entidad: this.codigoEntidad,
            modoBusqueda: ModoBusqueda.BUSQUEDA_LOCAL,
            valorBusqueda: '',
            placeholder: this.obtenerLlavePlaceholder(),
            focusEnInput: true,
            buscar: () => {
              const query = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda
              this.buscar(query)
            }
          }
        }
      }
    }
  }


  navegarAlHome() {

    this.enrutadorService.navegar(
      {
        componente: MenuPrincipalComponent,
        params: [
        ]
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL,
        }
      }
    )

  }

  configurarResumenPerfil(
    mostrar: boolean,
    error: boolean,
    loader: boolean,
    titulo: string,
    idPerfil: string,
    urlMedia: string,
    usoItem: UsoItemCircular,
    sonContactos: boolean = false
  ) {
    this.idPerfilActivo = idPerfil
    this.confResumenPerfil = {
      titulo: titulo,
      alturaModal: AlturaResumenPerfil.VISTA_BUSCAR_CONTACTOS_100,
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        idPerfil,
        urlMedia,
        ColorDeBorde.BORDER_AMARILLO,
        ColorDeFondo.FONDO_BLANCO,
        false,
        usoItem,
        false,
        false
      ),
      configuracionPensamiento: {
        tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
        esLista: true,
        subtitulo: true,
        configuracionItem: {
          estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
        },
      },
      confDataListaPensamientos: this.listaPensamientos,
      botones: [
        {
          text: 'm9v2texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.AMARRILLO,
          ejecutar: () => {
            this.enrutadorService.navegar(
              {
                componente: PerfilComponent,
                params: [
                  {
                    nombre: 'id',
                    valor: this.idPerfilActivo
                  }
                ]
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
                extras: {
                  tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                  paramAValidar: 'id'
                }
              }
            )
          },
          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,
        },
        {
          text: 'm9v2texto3',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
          colorTexto: ColorTextoBoton.ROJO,
          ejecutar: () => {
            this.confResumenPerfil.mostrar = false
            this.idPerfilActivo = ''
          },
          enProgreso: false,
          tipoBoton: TipoBoton.TEXTO,
        },
      ],
      reintentar: (id: string) => {
        this.obtenerPerfilBasico(id);
      },
      sonContactos: sonContactos,
      mostrar: mostrar,
      error: error,
      loader: loader,
    }
  }

  configurarListaPensamientos() {
    this.listaPensamientos = {
      tamanoLista: TamanoLista.LISTA_PENSAMIENTO_RESUMEN_MODAL_PERFIL,
      lista: [],
    }
  }

  obtenerLlavePlaceholder() {
    switch (this.codigoEntidad) {
      case CodigosCatalogoEntidad.PERFIL:
        return 'm3v6texto1'
      case CodigosCatalogoEntidad.PROYECTO:
        return 'm5v1texto1'
      case CodigosCatalogoEntidad.NOTICIA:
        return 'm4v3texto1'
      case CodigosCatalogoEntidad.CONTACTO:
        return 'm3v8texto1'
      default:
        return ''
    }
  }

  obtenerTamanoColorDeFondo(): TamanoColorDeFondo {
    switch (this.codigoEntidad) {
      case CodigosCatalogoEntidad.PERFIL:
        return TamanoColorDeFondo.TAMANO6920
      case CodigosCatalogoEntidad.PROYECTO:
        return TamanoColorDeFondo.TAMANO6920
      case CodigosCatalogoEntidad.NOTICIA:
        return TamanoColorDeFondo.TAMANO6920
      case CodigosCatalogoEntidad.CONTACTO:
        return TamanoColorDeFondo.TAMANO100
      default:
        return TamanoColorDeFondo.TAMANO100
    }
  }

  configurarListas() {
    switch (this.codigoEntidad) {
      case CodigosCatalogoEntidad.PERFIL:
        this.paginacionPerfiles = {
          lista: [],
          paginaActual: 1,
          proximaPagina: true,
          totalDatos: 0,
        }
        break
      case CodigosCatalogoEntidad.PROYECTO:
        this.paginacionProyectos = {
          lista: [],
          paginaActual: 1,
          proximaPagina: true,
          totalDatos: 0,
        }
        break
      case CodigosCatalogoEntidad.NOTICIA:
        this.paginacionNoticias = {
          lista: [],
          paginaActual: 1,
          proximaPagina: true,
          totalDatos: 0,
        }
        break
      case CodigosCatalogoEntidad.CONTACTO:
        this.paginacionContactos = {
          lista: [],
          paginaActual: 1,
          proximaPagina: true,
          totalDatos: 0,
        }
        break
      default:
        break
    }
  }

  buscar(
    query: string,
    configurarListas: boolean = true
  ) {
    if (this.mostrarLoader || this.mostrarCargandoPequeno) {
      return
    }

    if (configurarListas) {
      this.configurarListas()
    }

    query = query.trim()

    this.mostrarError = false
    switch (this.codigoEntidad) {
      case CodigosCatalogoEntidad.PERFIL:
        this.buscarUsuarios(query)
        break
      case CodigosCatalogoEntidad.PROYECTO:
        this.buscarProyectos(query)
        break
      case CodigosCatalogoEntidad.NOTICIA:
        this.buscarNoticias(query)
        break
      case CodigosCatalogoEntidad.CONTACTO:
        this.buscarContactos(query)
        break
      default:
        break
    }
  }

  async buscarProyectos(query: string) {
    if (!this.paginacionProyectos.proximaPagina) {
      this.puedeCargarMas = false
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      return
    }

    try {
      this.mostrarNoHayItems = false
      this.mostrarLoader = (this.paginacionProyectos.lista.length === 0)
      this.mostrarCargandoPequeno = (this.paginacionProyectos.lista.length > 0)

      const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorTitulo(
        query,
        10,
        this.paginacionProyectos.paginaActual
      ).toPromise()

      this.paginacionProyectos.totalDatos = dataPaginacion.totalDatos
      this.paginacionProyectos.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(proyecto => {
        const index: number = this.paginacionProyectos.lista.findIndex(e => e.proyecto.id === proyecto.id)
        if (index < 0) {
          this.paginacionProyectos.lista.push(
            this.configurarItemProyectos(proyecto)
          )
        }

      })
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = true
      this.mostrarNoHayItems = (this.paginacionProyectos.lista.length === 0)


      if (this.paginacionProyectos.proximaPagina) {
        this.paginacionProyectos.paginaActual += 1
      }
    } catch (error) {
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = false
    }
  }

  configurarItemProyectos(
    proyecto: ProyectoModel
  ): ConfiguracionItemBuscadorProyectoNoticia {
    return {
      codigoEntidad: CodigosCatalogoEntidad.PROYECTO,
      proyecto: proyecto,
      configuracionRectangulo: this.obtenerConfiguracionRectangulo(proyecto),
      eventoTap: (id: string) => {
        if (!id || id.length === 0) {
          return
        }

        this.enrutadorService.navegar(
          {
            componente: PublicarComponent,
            params: [
              {
                nombre: 'id',
                valor: id
              },
              {
                nombre: 'accionEntidad',
                valor: AccionEntidad.VISITAR
              }
            ]
          },
          {
            estado: true,
            extras: {
              tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
              paramAValidar: 'id'
            }
          }
        )
        return
      }
    }
  }

  configurarItemNoticias(
    noticia: NoticiaModel
  ): ConfiguracionItemBuscadorProyectoNoticia {
    return {
      codigoEntidad: CodigosCatalogoEntidad.NOTICIA,
      noticia: noticia,
      configuracionRectangulo: this.obtenerConfiguracionRectangulo(noticia, true),
      eventoTap: (id: string) => {
        if (!id || id.length === 0) {
          return
        }

        this.enrutadorService.navegar(
          {
            componente: PublicarNoticiaComponent,
            params: [
              {
                nombre: 'id',
                valor: id
              },
              {
                nombre: 'accionEntidad',
                valor: AccionEntidad.VISITAR
              }
            ]
          },
          {
            estado: true,
            extras: {
              tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
              paramAValidar: 'id'
            }
          }
        )
        return
      }
    }
  }

  obtenerConfiguracionRectangulo(
    entidad: any,
    predeterminado: boolean = false
  ): CongifuracionItemProyectosNoticias {
    let album: AlbumModel
    if (predeterminado) {
      album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(entidad.adjuntos)
    }

    if (!album) {
      album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        entidad.adjuntos
      )
    }

    const urlMedia = this.obtenerUrlMedia(album)

    return {
      usoVersionMini: true,
      id: entidad.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: false,
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: false,
      },
      urlMedia: urlMedia,
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: (urlMedia.length > 0),
      eventoTap: {
        activo: false,
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  scroolEnCapaCuerpo() {
    if (!this.puedeCargarMas) {
      return
    }

    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 3.16) {
      this.puedeCargarMas = false
      const query: string = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda
      this.buscar(query, false)
    }
  }

  async buscarNoticias(query: string) {
    if (!this.paginacionNoticias.proximaPagina) {
      this.puedeCargarMas = false
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      return
    }

    try {
      this.mostrarNoHayItems = false
      this.mostrarLoader = (this.paginacionNoticias.lista.length === 0)
      this.mostrarCargandoPequeno = (this.paginacionNoticias.lista.length > 0)

      const dataPaginacion = await this.noticiaNegocio.buscarNoticiasPorTitulo(
        query,
        10,
        this.paginacionNoticias.paginaActual
      ).toPromise()
      this.paginacionNoticias.totalDatos = dataPaginacion.totalDatos
      this.paginacionNoticias.proximaPagina = dataPaginacion.proximaPagina
      dataPaginacion.lista.forEach(noticia => {
        this.paginacionNoticias.lista.push(
          this.configurarItemNoticias(noticia)
        )
      })

      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = true
      this.mostrarNoHayItems = (this.paginacionNoticias.lista.length === 0)

      if (this.paginacionNoticias.proximaPagina) {
        this.paginacionNoticias.paginaActual += 1
      }
    } catch (error) {
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = false
    }
  }

  async buscarUsuarios(query: string) {
    if (!this.paginacionPerfiles.proximaPagina) {
      this.puedeCargarMas = false
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      return
    }

    try {
      this.mostrarNoHayItems = false
      this.mostrarLoader = (this.paginacionPerfiles.lista.length === 0)
      this.mostrarCargandoPequeno = (this.paginacionPerfiles.lista.length > 0)

      const dataPaginacion = await this.perfilNegocio.buscarPerfilesPorNombre(
        query,
        this.perfilSeleccionado._id,
        10,
        this.paginacionPerfiles.paginaActual
      ).toPromise()

      this.paginacionPerfiles.totalDatos = dataPaginacion.totalDatos
      this.paginacionPerfiles.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(perfil => {
        this.paginacionPerfiles.lista.push(
          this.configuracionPerfil(perfil)
        )
      })

      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = true
      this.mostrarNoHayItems = (this.paginacionPerfiles.lista.length === 0)

      if (this.paginacionPerfiles.proximaPagina) {
        this.paginacionPerfiles.paginaActual += 1
      }
    } catch (error) {
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = false
    }
  }

  configuracionPerfil(
    perfil: PerfilModel,
    colorDelTextoSuperior: ColorDelTexto = ColorDelTexto.TEXTOBLANCO,
    colorDelTextoInferior: ColorDelTexto = ColorDelTexto.TEXTOBLANCO,
    usarCorazon: boolean = true,
    irAlChat: boolean = false
  ): ConfiguracionItemListaContactosCompartido {

    
    let usoCirculo: UsoItemCircular = UsoItemCircular.CIRCONTACTO
    let corazon: boolean = false
    let urlMedia: string = ''

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.PERFIL,
      perfil.album
    )

    if (
      album &&
      album.portada &&
      album.portada.principal &&
      album.portada.principal.fileDefault
    ) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO
      urlMedia = album.portada.principal.url
    } else {
      urlMedia = album.portada.principal.url
    }

    if (
      perfil &&
      perfil.asociaciones &&
      perfil.asociaciones.length > 0 &&
      usarCorazon
    ) {
      corazon = true
    }

    return {
      id: perfil._id,
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: perfil.nombreContacto,
        nombreContactoTraducido: perfil.nombreContactoTraducido,
        // nombre: perfil.nombre,
        contacto: false,
        estilosTextoSuperior: {
          color: colorDelTextoSuperior,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: colorDelTextoInferior,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: perfil._id,
      },
      configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
        perfil._id,
        urlMedia,
        ColorDeBorde.BORDER_ROJO,
        this.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        false,
        true
      ),
      mostrarX: {
        mostrar: false,
        color: true,
      },
      mostrarCorazon: corazon,
      configuracionLineaVerde: this.configurarLineaVerde(
        PaddingLineaVerdeContactos.PADDING_1542_267
      ),
      eventoCirculoNombre: (idPerfil: string) => {
        if (!irAlChat) {
          this.obtenerPerfilBasico(idPerfil)
        }

        if (irAlChat) {
          this.idAsociacionActivo = perfil.asociaciones[0]._id

          if (
            !this.idAsociacionActivo ||
            this.idAsociacionActivo.length === 0
          ) {
            return
          }

          this.enrutadorService.navegar(
            {
              componente: ChatGazeComponent,
              params: [
                {
                  nombre: 'id',
                  valor: this.idAsociacionActivo
                }
              ]
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id'
              }
            }
          )
        }
      },
      eventoDobleTap: (idAsociacion: string, contacto: DataContacto) => {
        if (
          !contacto ||
          !contacto.idPerfil ||
          !(contacto.idPerfil.length > 0)
        ) {
          return
        }

        this.enrutadorService.navegar(
          {
            componente: PerfilComponent,
            params: [
              {
                nombre: 'id',
                valor: contacto.idPerfil
              }
            ]
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
              paramAValidar: 'id'
            }
          }
        )
      }
    };
  }

  obtenerColorFondoAleatorio() {
    const fondosAleatorios = Object.keys(ColorFondoAleatorio).map(function (
      type
    ) {
      return ColorFondoAleatorio[type]
    });
    const fondoAleatorio = this.randomItem(fondosAleatorios);
    return fondoAleatorio;
  }

  randomItem(items: any) {
    return items[Math.floor(Math.random() * items.length)];
  }

  configurarGeneralFotoPerfilContactos(
    idPerfil: string,
    urlMedia: string,
    colorBorde: any,
    colorFondo: any,
    mostrarCorazon: boolean,
    usoItemCirculo: UsoItemCircular,
    activarClick: boolean,
    mostrarCursor: boolean

  ): ItemCircularCompartido {
    return {
      id: idPerfil,
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: usoItemCirculo,
      esVisitante: true,
      urlMedia: urlMedia,
      activarClick: false,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: false,
      mostrarLoader: (urlMedia.length > 0),
      textoBoton: '',
      capaOpacidad: {
        mostrar: false,
      },
      esBotonUpload: false,
      colorBorde: colorBorde,
      colorDeFondo: colorFondo,
      mostrarCorazon: mostrarCorazon,
      mostrarCursor: mostrarCursor
    }
  }

  configurarLineaVerde(
    paddingLineaVerdeContactos?: PaddingLineaVerdeContactos
  ): ConfiguracionLineaVerde {
    return {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false,
      paddingLineaVerde: paddingLineaVerdeContactos,
    };
  }

  async obtenerPerfilBasico(idPerfil: string) {
    if (idPerfil.length <= 0) {
      return
    }

    try {
      this.confResumenPerfil.loader = true
      this.confResumenPerfil.mostrar = true
      this.confResumenPerfil.confDataListaPensamientos.lista = []

      const perfil: PerfilModel = await this.perfilNegocio.obtenerPerfilBasico(idPerfil, this.perfilSeleccionado._id).toPromise()
      if (!perfil) {
        throw new Error('')
      }

      this.confResumenPerfil.confDataListaPensamientos.lista = perfil.pensamientos
      const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.PERFIL,
        perfil.album
      )

      let usoItem: UsoItemCircular = UsoItemCircular.CIRCONTACTO
      let urlMedia: string = ''
      let titulo: string = ''

      if (
        album &&
        album.portada &&
        album.portada.principal &&
        album.portada.principal.fileDefault
      ) {
        usoItem = UsoItemCircular.CIRCARITACONTACTODEFECTO
        urlMedia = album.portada.principal.url
      } else {
        urlMedia = album.portada.principal.url
      }

      // titulo = perfil.nombre + '(' + perfil.nombreContacto + ')'
      titulo = perfil.nombreContacto

      let sonContactos: boolean = false
      if (perfil.participanteAsociacion) {
        sonContactos = (perfil.participanteAsociacion.estado.codigo === CodigoEstadoParticipanteAsociacion.CONTACTO)
      }

      this.configurarResumenPerfil(
        true,
        false,
        false,
        titulo,
        perfil._id,
        urlMedia,
        usoItem,
        sonContactos
      )
    } catch (error) {
      this.configurarResumenPerfil(
        true,
        error,
        false,
        '',
        idPerfil,
        '',
        UsoItemCircular.CIRCONTACTO,

      )
    }
  }

  async buscarContactos(query: string) {
    if (!this.paginacionContactos.proximaPagina) {
      this.puedeCargarMas = false
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      return
    }

    try {
      this.mostrarNoHayItems = false
      this.mostrarLoader = (this.paginacionContactos.lista.length === 0)
      this.mostrarCargandoPequeno = (this.paginacionContactos.lista.length > 0)

      const dataPaginacion = await this.perfilNegocio.buscarPerfilesPorNombre(
        query,
        this.perfilSeleccionado._id,
        10,
        this.paginacionContactos.paginaActual
      ).toPromise()

      this.paginacionContactos.totalDatos = dataPaginacion.totalDatos
      this.paginacionContactos.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(perfil => {
        if (perfil.asociaciones.length > 0) {
          this.paginacionContactos.lista.push(
            this.configuracionPerfil(
              perfil,
              ColorDelTexto.TEXTOAZULBASE,
              ColorDelTexto.TEXTONEGRO,
              false,
              true
            )
          )
        }
      })

      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = true
      this.mostrarNoHayItems = (this.paginacionContactos.lista.length === 0)

      if (this.paginacionContactos.proximaPagina) {
        this.paginacionContactos.paginaActual += 1
      }
    } catch (error) {
      this.mostrarLoader = false
      this.mostrarCargandoPequeno = false
      this.puedeCargarMas = false
    }
  }

  obtenerUrlMedia(
    album: AlbumModel
  ): string {
    try {

      if (!album.portada) {
        throw new Error('')
      }

      if (album.portada.miniatura) {
        return album.portada.miniatura.url
      }

      if (album.portada.principal) {
        return album.portada.principal.url
      }

      return ''
    } catch (error) {
      return ''
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }

}
