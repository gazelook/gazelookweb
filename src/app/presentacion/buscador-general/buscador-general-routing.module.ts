import { BuscarComponent } from './buscar/buscar.component';
import { RutasBuscadorGeneral } from './rutas-buscador-general.enum';
import { BuscadorGeneralComponent } from './buscador-general.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		component: BuscadorGeneralComponent,
		children: [
			{
				path: RutasBuscadorGeneral.BUSCAR_CONTACTOS.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_NOTICIAS.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_PERFILES.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_PROYECTOS.toString(),
				component: BuscarComponent
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class BuscadorGeneralRoutingModule { }
