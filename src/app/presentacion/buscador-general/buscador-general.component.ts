import { Component, OnInit } from '@angular/core';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';

@Component({
	selector: 'app-buscador-general',
	templateUrl: './buscador-general.component.html',
	styleUrls: ['./buscador-general.component.scss']
})
export class BuscadorGeneralComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario
	) {

	}

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
	}

}
