import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from './../../compartido/compartido.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuscadorGeneralRoutingModule } from './buscador-general-routing.module';
import { BuscadorGeneralComponent } from './buscador-general.component';
import { BuscarComponent } from './buscar/buscar.component';


@NgModule({
	declarations: [
		BuscadorGeneralComponent,
		BuscarComponent
	],
	imports: [
		CommonModule,
		BuscadorGeneralRoutingModule,
		CompartidoModule,
		TranslateModule,
		FormsModule,
	],
	exports: [
		CompartidoModule,
		TranslateModule,
		FormsModule,
	]
})
export class BuscadorGeneralModule { }
