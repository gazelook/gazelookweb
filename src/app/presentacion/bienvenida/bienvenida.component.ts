import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes';
import { AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoDeTextoConInterlineado, TamanoPortadaGaze } from '@shared/diseno/enums';
import { BotonCompartido, LineaCompartida, PortadaGazeCompartido } from '@shared/diseno/modelos';
import { CuentaNegocio, InternacionalizacionNegocio } from 'dominio/logica-negocio';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { ConfiguracionPortadaGif } from '@shared/componentes/portada-gif/portada-gif.component';
import { MenuprincipalDemoComponent } from 'presentacion/demo/menuprincipal/menuprincipal.component';
import { Enrutador, InformacionEnrutador } from 'presentacion/enrutador/enrutador.component';
@Component({
  selector: 'app-bienvenida',
  templateUrl: './bienvenida.component.html',
  styleUrls: ['./bienvenida.component.scss']
})
export class BienvenidaComponent implements OnInit, Enrutador {
  configuracionLinea0: LineaCompartida
  configuracionLinea1: LineaCompartida
  configuracionLinea2: LineaCompartida
  botonEnter: BotonCompartido
  sesionIniciada: boolean

  public portadaGazeComponent: PortadaGazeCompartido
  public confPortadaGifUno: ConfiguracionPortadaGif
  public confPortadaGifDos: ConfiguracionPortadaGif

  public informacionEnrutador: InformacionEnrutador

  constructor(
    public variablesGlobales: VariablesGlobales,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private router: Router,
    private _location: Location,
    private cuentaNegocio: CuentaNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private enrutadorService: EnrutadorService
  ) {
    this.cargarDatos()
    this.sesionIniciada = false
  }

  ngOnInit(): void {
    
    this.variablesGlobales.mostrarMundo = true
    this.portadaGazeComponent = { tamano: TamanoPortadaGaze.PORTADACOMPLETA, espacioDerecha: false, mostrarLoader: true }
    this.verificarSesion()
    this.configurarVariablesGlobales()
    this.configurarPortadasGif()
  }

  configurarVariablesGlobales() {
    this.variablesGlobales.configurarDataAnimacion()
  }

  configurarPortadasGif() {
    const idioma = this.internacionalizacionNegocio.obtenerIdiomaInternacionalizacion()
    let codigo = 'en'
    if (idioma && idioma !== null) {
      codigo = idioma.toLowerCase()
    }

    this.confPortadaGifUno = {
      esIzquierdo: true,
      urlImagen: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-a.jpg',
      // urlGif: '..https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-' + codigo + '-a.gif',
      llaveTexto: 'fwtexto5',
    }

    this.confPortadaGifDos = {
      esIzquierdo: false,
      urlImagen: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-b.jpg',
      // urlGif: '..https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-' + codigo + '-b.gif',
      llaveTexto: 'fwtexto5',
    }
  }

  verificarSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  cargarDatos() {
    this.configuracionLinea0 = {
      ancho: AnchoLineaItem.ANCHO100,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false
    }
    this.configuracionLinea1 = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false
    }
    this.configuracionLinea2 = {
      ancho: AnchoLineaItem.ANCHO6916,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false
    }
    this.botonEnter = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m1v1texto6'),
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.navegarMenuPrincipal()
    }
  }

  navegarMenuPrincipal() {
    this.enrutadorService.navegarConPosicionFija(
      {
        componente: MenuprincipalDemoComponent,
        params: []
      },
      this.informacionEnrutador.posicion
    )
  }

  ngAfterViewInit(): void {
    setTimeout(async () => {
      this.variablesGlobales.mostrarMundo = true
    })
  }
  // Eventos de click
  clickBotonAtras() {
    this._location.replaceState('')
    this.router.navigateByUrl('')
  }

  async cambiarIdioma() {
    this.botonEnter.text = await this.internacionalizacionNegocio.obtenerTextoLlave('m1v1texto6')
    
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
}
