import { Location } from '@angular/common';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Router } from '@angular/router';

import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-doc-legales',
  templateUrl: './doc-legales.component.html',
  styleUrls: ['./doc-legales.component.scss']
})
export class DocLegalesComponent implements OnInit {
  paginas = 0;
  idioma = 'en';
  docLegalMostar: number

  constructor(private router: Router, private location: Location) {
    this.docLegalMostar = 0
  }

  ngOnInit(): void {
    this.detectarDispositivo()
    this.paginas = Number(sessionStorage.getItem(LlavesSessionStorage.PAGINAS));
    const { codNombre } = JSON.parse(localStorage.getItem('idiomaSeleccionado'));
    this.idioma = codNombre;
  }


  eventoDocLegal(numDocLegal: number) {
    if (numDocLegal === this.docLegalMostar) {
      this.docLegalMostar = 0
      return
    }
    this.docLegalMostar = numDocLegal

  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.detectarDispositivo();
  }
  descargarTerminos(): void {
    const link = document.createElement('a');
    const nombre = 'terminos-condiciones-gazelook';
    link.href = ` http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/terminos-condiciones.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }
  detectarDispositivo() {
    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

      // document.location.href = 'https://m.gazelook.com/politicas';
    }
  }

  descargarPoliticasPrivacidad(): void {
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/politicas-privacidad.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));

  }

  descargarPoliticasCookies(): void {
    const link = document.createElement('a');
    const nombre = 'politica-cookies-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/politica-cookies.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));

  }
  descargarCodigoConducta(): void {
    const link = document.createElement('a');
    const nombre = 'código-conducta-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/codigo-conducta.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }
  descargarCondicionesPago(): void {
    const link = document.createElement('a');
    const nombre = 'condiciones-pago-gazelook';
    link.href = ` http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/condiciones-pago.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }
  descargarReglamentoProyectos(): void {
    const link = document.createElement('a');
    const nombre = 'reglamento-proyectos-gazelook';
    link.href = ` http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/${this.idioma}/reglamento-proyectos.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }

  public onRegresarHome(pagina: number): void {
    sessionStorage.setItem(LlavesSessionStorage.PAGINAS, pagina.toString());
    this.router.navigateByUrl(RutasLocales.LANDING);
  }

  public onRgresarObjetivos(pagina: number): void {
    sessionStorage.setItem(LlavesSessionStorage.PAGINAS, pagina.toString());
    this.router.navigateByUrl(RutasLocales.LANDING);
  }
  public onRgresarPreguntas(pagina: number): void {
    sessionStorage.setItem(LlavesSessionStorage.PAGINAS, pagina.toString());
    this.router.navigateByUrl(RutasLocales.LANDING);
  }
  public onRgresarRedSocial(pagina: number): void {
    sessionStorage.setItem(LlavesSessionStorage.PAGINAS, pagina.toString());
    this.router.navigateByUrl(RutasLocales.LANDING);
  }

  public onRegresar(): void {
    this.location.back();
  }

}
