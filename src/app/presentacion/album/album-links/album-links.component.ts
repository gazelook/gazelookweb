import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as serviciosDiseno from '@core/servicios/diseno';
import * as serviciosGenerales from '@core/servicios/generales';
import * as enrutador from '@core/servicios/generales/enrutador';
import * as serviciosRemotosCodigosEnum from '@core/servicios/remotos/codigos-catalogos';
import * as componentes from '@shared/componentes';
import * as disenoEnum from '@shared/diseno/enums';
import * as disenoModel from '@shared/diseno/modelos';
import * as logicaNegocio from 'dominio/logica-negocio';
import * as modeloPrincipal from 'dominio/modelo';
import * as modelo from 'dominio/modelo/entidades';
import * as parametros from 'dominio/modelo/parametros';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';

@Component({
	selector: 'app-album-links',
	templateUrl: './album-links.component.html',
	styleUrls: ['./album-links.component.scss']
})
export class AlbumLinksComponent implements OnInit, OnDestroy, Enrutador {
	@ViewChild('toast', { static: false }) toast: componentes.ToastComponent

	// Params
	public params: parametros.AlbumParams
	// Utils
	public AccionEntidadEnum = serviciosRemotosCodigosEnum.AccionEntidad
	public DiametroDelLoaderEnum = componentes.DiametroDelLoader
	public codigosCatalogoEntidad = serviciosRemotosCodigosEnum.CodigosCatalogoEntidad

	// Parametros internos
	public perfilSeleccionado: modelo.PerfilModel
	public album: modelo.AlbumModel
	public dataApiArchivo: modeloPrincipal.SubirArchivoData
	public mediaActivo: modelo.MediaModel
	public listaDeLinks: Array<disenoModel.ConfiguracionItemLink>

	// Configuraciones
	public confAppBar: disenoModel.ConfiguracionAppbarCompartida
	public confToast: disenoModel.ConfiguracionToast
	public confLineaVerde: disenoModel.LineaCompartida
	public confBotonReady: disenoModel.BotonCompartido
	public confBotonSubmit: disenoModel.BotonCompartido
	public confDone: disenoModel.ConfiguracionDone

	// Enrutamiento
	public informacionEnrutador: InformacionEnrutador

	constructor(
		public estiloDelTextoServicio: serviciosDiseno.EstiloDelTextoServicio,
		public variablesGlobales: serviciosGenerales.VariablesGlobales,
		private albumService: serviciosGenerales.AlbumService,
		private albumNegocio: logicaNegocio.AlbumNegocio,
		private perfilNegocio: logicaNegocio.PerfilNegocio,
		private enrutadorService: enrutador.EnrutadorService
	
	) {
		this.params = { estado: false }
		this.mediaActivo = {
			descripcion: '',
			miniatura: {},
			principal: {
				url: '',
			},
			id: '',
		}
		this.listaDeLinks = []
	}

	ngOnInit(): void {
		this.configurarParametrosDeUrl()
		this.inicializarPerfilSeleccionado()
		if (this.params.estado && this.perfilSeleccionado) {
			this.inicializarDataAlbum()
			this.configurarMediasDelAlbum()
			this.configurarToast()
			this.configurarAppBar()
			this.configurarLineas()
			this.configurarBotones()
			this.configurarDone()
			
			// En caso la pagina sea recargada, se guarda el estado del album en el session storage sotarage
			// window.onbeforeunload = () => this.guardarAlbumAntesDeSalirReload()
			return
		}

		this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
	}

	ngOnDestroy(): void {

	}

	// Parametros de url
	configurarParametrosDeUrl() {
		if (
			!this.informacionEnrutador ||
			!this.informacionEnrutador.posicion
		) {
			this.params.estado = false
			return
		}

		this.informacionEnrutador.params.forEach(item => {
			if (item.nombre === 'titulo') {
				this.params.titulo = item.valor
			}

			if (item.nombre === 'accionEntidad') {
				this.params.accionEntidad = item.valor
			}

			if (item.nombre === 'entidad') {
				this.params.entidad = item.valor
			}
		})

		this.params = this.albumService.validarParametrosDelAlbumSegunAccionEntidadEnrutador(this.params)


	}

	// Inicializar perfil seleccionado
	inicializarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	inicializarDataAlbum() {
		this.album = this.albumNegocio.obtenerAlbumActivoDelTercio(
			this.informacionEnrutador.posicion
		)

		if (!this.album) {
			this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
		}
	}

	configurarMediasDelAlbum() {
	
		if (this.album) {
			this.album.media.forEach(media => {
				
				const item: disenoModel.ConfiguracionItemLink = {
					entidad: this.params.entidad,
					esPortada: this.album.portada && this.album.portada.id === media.id,
					media: media,
					albumPredeterminado: this.album.predeterminado,
					eventoTap: (id: string, url: string) => this.abrirUrlLink(id, url),
					eventoDobleTap: (id: string) => this.validarAccionEntidadParaDefinirPortada(id),
					eventoPress: (id: string) => this.validarAccionEntidadParaEliminarMedia(id),

				}

				this.listaDeLinks.push(item)
			})

			this.ordernarListaDeLinks()
		}
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false,
			mostrarLoader: false,
			cerrarClickOutside: false,
			texto: '',
			intervalo: 5,
			bloquearPantalla: false,
		}
	}

	configurarAppBar() {
		// Determinar textos del appbar segun entidad
		const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
			this.params.entidad,
			this.perfilSeleccionado,
			serviciosRemotosCodigosEnum.CodigosCatalogoTipoAlbum.LINK
		)

		if (this.params.accionEntidad === serviciosRemotosCodigosEnum.AccionEntidad.VISITAR) {
			dataAppBar.subtitulo = 'm4v8texto10' 
		}

		this.confAppBar = {
			usoAppBar: disenoEnum.UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true
					}
				},
				nombrePerfil: {
					mostrar: true,
					llaveTexto: dataAppBar.nombrePerfil
				},
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarTextoHome: dataAppBar.mostrarTextoHome,
				subtitulo: {
					mostrar: true,
					llaveTexto: dataAppBar.subtitulo
				},
				mostrarLineaVerde: true,
				tamanoColorFondo: disenoEnum.TamanoColorDeFondo.TAMANO100,
			},
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => this.accionAtrasAppBarBack(false),
			eventoHome: () => 				this.enrutadorService.navegar(
					{
						componente: MenuPrincipalComponent,
						params: [
						]
					},
					{
						estado: true,
						posicicion: this.informacionEnrutador.posicion,
						extras: {
							tipo: TipoDeNavegacion.NORMAL,
						}
					}
				)
		}

	}

	configurarLineas() {
		this.confLineaVerde = {
			ancho: disenoEnum.AnchoLineaItem.ANCHO6382,
			colorFondo: disenoEnum.ColorFondoLinea.FONDOLINEAVERDE,
			espesor: disenoEnum.EspesorLineaItem.ESPESOR012,
			forzarAlFinal: false
		}
	}

	configurarBotones() {
		this.confBotonReady = {
			tipoBoton: componentes.TipoBoton.ICON_COLOR,
			colorIcono: componentes.ColorIconoBoton.AZUL,
			enProgreso: false,
			ejecutar: () => {
				if (this.mediaActivo.principal.url.length > 0) {
					this.definirDataParaSubirMedia()
				}
			}
		}

		this.confBotonSubmit = {
			text: 'm2v3texto20',
			tamanoTexto: disenoEnum.TamanoDeTextoConInterlineado.L7_IGUAL,
			colorTexto: componentes.ColorTextoBoton.AMARRILLO,
			tipoBoton: componentes.TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.accionAtrasAppBarBack()
			}
		}
	}

	configurarDone() {
		this.confDone = {
			intervalo: 4000,
			mostrarDone: false,
			mostrarLoader: false	
		}
	}

	obtenerEstadoModalDataLink() {
		if (this.params.accionEntidad !== serviciosRemotosCodigosEnum.AccionEntidad.VISITAR) {
			return true
		}

		return false
	}

	reiniciarData(
		enProgreso: boolean,
		descripcion: string = '',
		enlace: string = ''
	) {
		this.confBotonReady.enProgreso = enProgreso
		this.mediaActivo.descripcion = descripcion
		this.mediaActivo.principal.url = enlace
	}

	abrirUrlLink(id: string, url: string) {
		if (!(url && url.length > 0)) {
			this.toast.abrirToast('text34')
			return
		}

		const index = this.listaDeLinks.findIndex(e => e.media.id === id)
		if (index >= 0 && this.params.accionEntidad === serviciosRemotosCodigosEnum.AccionEntidad.VISITAR) {
			this.listaDeLinks[index].fueVisitado = true
		}

		window.open(url)
		return
	}

	subirMediaAlAlbumLocal(media: modelo.MediaModel) {
		const item: disenoModel.ConfiguracionItemLink = {
			entidad: this.params.entidad,
			media: media,
			esPortada: (this.album.media.length === 0 && (this.params.entidad === serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.NOTICIA || this.params.entidad === serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.PROYECTO)),
			albumPredeterminado: this.album.predeterminado,
			eventoTap: (id: string, url: string) => this.abrirUrlLink(id, url),
			eventoDobleTap: (id: string) => this.validarAccionEntidadParaDefinirPortada(id),
			eventoPress: (id: string) => this.validarAccionEntidadParaEliminarMedia(id),
		}

		if (
			(this.params.entidad === serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.NOTICIA || serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.PROYECTO) &&
			this.album.media.length === 1 && 
			this.album.portada &&
			this.album.portada.id
		) {
			this.validarAccionEntidadParaDefinirPortada(this.album.portada.id)
		}

		this.album.media.push(media)

		if (item.esPortada) {
			this.album.portada = media
		}

		this.listaDeLinks.push(item)
		this.ordernarListaDeLinks()
		this.reiniciarData(false)
	}

	definirDataParaSubirMedia() {
		const data: modeloPrincipal.SubirArchivoData = {
			catalogoMedia: serviciosRemotosCodigosEnum.CodigosCatalogoTipoMedia.TIPO_LINK,
			descripcion: this.mediaActivo.descripcion,
			enlace: this.mediaActivo.principal.url.trim(),
		}
		
		this.subirMediaAlApi(data) 
	}

	establecerPortadaEnLocal(id: string, accion: string) {
		switch (accion) {
			case 'asignar':
				const pos = this.albumService.obtenerMediaDeLaListaDeMedias(id, this.album.media)
				if (pos < 0) {
					return
				}

				const media: modelo.MediaModel = this.album.media[pos]

				this.album.portada = {
					...media,
					catalogoMedia: {
						codigo: serviciosRemotosCodigosEnum.CodigosCatalogoTipoMedia.TIPO_LINK
					},
				}

				this.listaDeLinks.forEach(link => {
					if (link.media.id === id) {
						link.esPortada = true
					} else {
						link.esPortada = false
					}
				})

				this.ordernarListaDeLinks()
				break
			case 'remover':
				this.album.portada = {}
				this.listaDeLinks.forEach(link => {
					link.esPortada = false
				})
				this.ordernarListaDeLinks()
				break
			default: break;
		}
	}

	determinarAccionParaPortada(id: string): string {
		if (
			!this.album.portada ||
			this.album.portada.id !== id
		) {
			return 'asignar'
		}

		if (
			this.album.portada &&
			this.album.portada.id === id
		) {
			return 'remover'
		}

		return null
	}

	validarAccionEntidadParaDefinirPortada(id: string) {
		const accion: string = this.determinarAccionParaPortada(id)
		switch (this.params.accionEntidad) {
			case serviciosRemotosCodigosEnum.AccionEntidad.CREAR:
				this.establecerPortadaEnLocal(id, accion)
				break
			case serviciosRemotosCodigosEnum.AccionEntidad.ACTUALIZAR:
				this.establecerPortadaEnElApi(id, accion)
				break
			default: break;
		}
	}

	establecerPortadaEnElApi(
		id: string,
		accion: string
	) {
		switch (accion) {
			case 'asignar':
				this.asignarPortada(id, accion)
				break
			case 'remover':
				this.removerPortada(id, accion)
				break
			default: break;
		}
	}


	borrarMediaDelLocal(id: string) {
		const pos = this.albumService.obtenerMediaDeLaListaDeMedias(id, this.album.media)
		if (pos < 0) {
			return
		}
		const media: modelo.MediaModel = this.album.media[pos]
		this.album.media.splice(pos, 1)

		if (
			this.album.portada &&
			this.album.portada.id === media.id
		) {
			this.album.portada = {}
		}

		let posLink = -1
		this.listaDeLinks.forEach((link, i) => {
			if (link.media.id === id) {
				posLink = i
			}
		})

		if (posLink >= 0) {
			this.listaDeLinks.splice(posLink, 1)
		}

		if (
			(this.params.entidad === serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.NOTICIA || serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.PROYECTO) &&
			this.album.media.length === 1
		) {
			this.validarAccionEntidadParaDefinirPortada(this.album.media[0].id)
		}
	}

	validarAccionEntidadParaEliminarMedia(id: string) {
		switch (this.params.accionEntidad) {
			case serviciosRemotosCodigosEnum.AccionEntidad.CREAR:
				this.borrarMediaDelLocal(id)
				break
			case serviciosRemotosCodigosEnum.AccionEntidad.ACTUALIZAR:
				this.borrarMediaDelApi(id)
				break
			default: break;
		}
	}

	async subirMediaAlApi(data: modeloPrincipal.SubirArchivoData) {
		try {
			this.confBotonReady.enProgreso = true
			const media: modelo.MediaModel = await this.albumNegocio.subirMedia(
				data, 
				this.params, 
				this.album,
				this.informacionEnrutador.posicion
			)
			if (!media) {
			
				this.toast.abrirToast('LINK_NO_VALIDO');

				this.confBotonReady.enProgreso = false;
				return;
			}

			this.subirMediaAlAlbumLocal(media)
			this.confBotonReady.enProgreso = false
		} catch (error) {
			this.confBotonReady.enProgreso = false
			this.toast.abrirToast('text37')
		}
	}

	async borrarMediaDelApi(id: string) {
		if (!(id && id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.borrarMedia(
				id, 
				this.params, 
				this.album,
				this.informacionEnrutador.posicion
			)

			if (!status) {
				throw new Error('')
			}

			this.borrarMediaDelLocal(id)

			if (this.album.media.length === 0) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.albumNegocio.removerAlbumActivoDelTercio(this.informacionEnrutador.posicion)
					this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
				})
			}
		} catch (error) {

			this.toast.abrirToast('text37')
		}
	}

	async asignarPortada(id: string, accion: string) {
		if (!(id && id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.asignarPortada(
				id, 
				this.params, 
				this.album,
				this.informacionEnrutador.posicion
			)

			if (!status) {
				throw new Error('')
			}
			
			this.establecerPortadaEnLocal(id, accion)
		} catch (error) {

			this.toast.abrirToast('text37')
		}
	}

	async removerPortada(id: string, accion: string) {
		if (!(id && id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.removerPortada(
				id, 
				this.params, 
				this.album,
				this.informacionEnrutador.posicion
			)
			if (status) {
				this.establecerPortadaEnLocal(id, accion)
			} else {
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	accionAtrasAppBarBack(
		esDelSubmit: boolean = true
	) {
		this.guardarAlbumAntesDeSalir(esDelSubmit, true)
	}

	async guardarAlbumAntesDeSalir(
		esDelSubmit: boolean,
		destruirAlbum: boolean = false
	) {
		try {
			let enviarCrearAlbum = false
			const idEntidad: string = this.albumNegocio.obtenerIdDeLaEntidadSegunCodigo(
				this.params.entidad,
				this.informacionEnrutador.posicion
			)
			
			if (
				this.params.accionEntidad === serviciosRemotosCodigosEnum.AccionEntidad.CREAR &&
				idEntidad && 
				idEntidad.length > 0 &&
				this.album.media.length > 0
			) {
				enviarCrearAlbum = true
			}

			if (enviarCrearAlbum) {
				
				const albumCreado: modelo.AlbumModel = await this.albumNegocio.agregarAlbumEnEntidad(
					idEntidad,
					this.params.entidad,
					this.album
				).toPromise()
				
				if (!albumCreado) {
					throw new Error('')
				}
				
				this.album._id = albumCreado._id	
			}

			// Actualizar estado predeterminado del album
			if (
				this.params.entidad === serviciosRemotosCodigosEnum.CodigosCatalogoEntidad.NOTICIA &&
				this.params.accionEntidad === serviciosRemotosCodigosEnum.AccionEntidad.ACTUALIZAR
			) {
				await this.albumNegocio.actualizarParametrosDelAlbum(
					idEntidad,
					this.params.entidad,
					{
						_id: this.album._id,
						predeterminado: this.album.predeterminado
					}
				).toPromise()
			}

			this.album = this.albumNegocio.actualizarEstadoDelAlbum(this.album, serviciosRemotosCodigosEnum.CodigosCatalogoEstadoAlbum.CREADO)
			this.albumNegocio.validarActualizacionDelAlbumSegunParams(
				this.album, 
				this.params, 
				destruirAlbum,
				this.informacionEnrutador.posicion
			)
			this.toast.cerrarToast()

			if (esDelSubmit) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
				}, 600)
			} 
			
			if (!esDelSubmit) {
				this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
			}
		} catch (error) {
			this.toast.abrirToast('text36')
		}
	}

	guardarAlbumAntesDeSalirReload(
		destruirAlbum: boolean = false
	) {
		this.albumNegocio.validarActualizacionDelAlbumSegunParams(
			this.album, 
			this.params, 
			destruirAlbum,
			this.informacionEnrutador.posicion
		)
	}

	ordernarListaDeLinks() {
		const index = this.listaDeLinks.findIndex(e => e.esPortada && e.esPortada === true)
		if (index >= 0) {
			const portada = this.listaDeLinks[index]
			this.listaDeLinks.splice(index, 1)
			this.listaDeLinks.unshift(portada)
		}
		if (this.listaDeLinks.length === 1) {
			this.listaDeLinks[0].esPortada = true
		}
	}

	// Enrutamiento
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

}