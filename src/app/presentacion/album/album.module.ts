import { AlbumComponent } from './album.component';
import { AlbumGeneralComponent } from './album-general/album-general.component';
import { AlbumRoutingModule } from './album-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from '../../compartido/compartido.module';
import { FormsModule } from '@angular/forms';
import { AlbumAudiosComponent } from './album-audios/album-audios.component';
import { AlbumLinksComponent } from './album-links/album-links.component';
import { AlbumPerfilComponent } from './album-perfil/album-perfil.component';


@NgModule({
  declarations: [
    AlbumComponent,
    AlbumGeneralComponent,
    AlbumAudiosComponent,
    AlbumLinksComponent,
    AlbumPerfilComponent,
  ],
  imports: [
    TranslateModule,
    CommonModule,
    AlbumRoutingModule,
    CompartidoModule,
    FormsModule
  ],
  exports: [
    TranslateModule,
    CompartidoModule,
    FormsModule
  ]
})
export class AlbumModule { }
