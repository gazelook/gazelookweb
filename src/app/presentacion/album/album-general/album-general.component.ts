import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
	AlbumService, CamaraService, EnrutadorService,
	GeneradorId, ImagenPantallaCompletaService
} from '@core/servicios/generales';
import {
	AccionEntidad, CodigosCatalogoEntidad,
	CodigosCatalogoEstadoAlbum, CodigosCatalogoTipoAlbum,
	CodigosCatalogoTipoArchivo, CodigosCatalogoTipoMedia
} from '@core/servicios/remotos/codigos-catalogos';
import { ConvertidorArchivos } from '@core/util';
import { TranslateService } from '@ngx-translate/core';
import {
	AppbarComponent, ColorTextoBoton, TipoBoton,
	ToastComponent
} from '@shared/componentes';
import {
	AccionesItemCircularRectangular, AnchoLineaItem,
	ColorCapaOpacidadItem, ColorDeBorde, ColorDeFondo,
	ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo,
	TamanoDeTextoConInterlineado, UsoAppBar, UsoItemRectangular
} from '@shared/diseno/enums';
import {
	BotonCompartido, ConfiguracionAppbarCompartida, ConfiguracionDone,
	ConfiguracionImagenPantallaCompleta, ConfiguracionToast, InfoAccionCirRec,
	ItemRectangularCompartido, LineaCompartida
} from '@shared/diseno/modelos';
import {
	AlbumNegocio, CuentaNegocio, MediaNegocio, PerfilNegocio
} from 'dominio/logica-negocio';
import { SubirArchivoData } from 'dominio/modelo';
import {
	AlbumModel, MediaModel, PerfilModel
} from 'dominio/modelo/entidades';
import { AlbumParams } from 'dominio/modelo/parametros';
import { WebcamImage } from 'ngx-webcam';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion }
	from 'presentacion/enrutador/enrutador.component';
import { MenuPrincipalComponent }
	from 'presentacion/menu-principal/menu-principal.component';


@Component({
	selector: 'app-album-general',
	templateUrl: './album-general.component.html',
	styleUrls: ['./album-general.component.scss']
})
export class AlbumGeneralComponent implements OnInit, OnDestroy, Enrutador {
	@ViewChild('appbar', { static: false }) appbar: AppbarComponent
	@ViewChild('toast', { static: false }) toast: ToastComponent


	// Utils
	public AccionEntidadEnum = AccionEntidad
	public codigosCatalogoEntidad = CodigosCatalogoEntidad
	public eventoEnitemFuncion: Function

	// Parametros de url
	public params: AlbumParams;

	// Parametros internos
	public haySesionIniciada: boolean
	public perfilSeleccionado: PerfilModel
	public idItemActivo: string // Item activo editando la descripcion
	public itemDescripcionActivo: string // Descricion activo
	public album: AlbumModel // Album en uso
	public itemsAlbum: Array<ItemRectangularCompartido> // Array de items compartido
	public itemsAlbumPorDefecto: Array<ItemRectangularCompartido> // Array de items compartido - fotos por defecto
	public cantidadItemsPorDefecto: number // Numero total de items por defecto a mostrar
	public albumEnModoPreview: boolean // Album pasa a modo preview
	public textoCerrarDescripcion: string // Texto que se muestra cuando se empiece a editar la descripcion de la foto
	public placeholderBarraInferior: string

	// Configuraciones
	public confToast: ConfiguracionToast // Configuracion del toast
	public confAppBar: ConfiguracionAppbarCompartida // Configuracion appbar compartida
	public confPortada: ItemRectangularCompartido // Portada del album
	public confBotonUpload: ItemRectangularCompartido // Boton siempre visible de upload photos
	public confLinea: LineaCompartida // Linea compartida
	public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta
	public confDone: ConfiguracionDone
	public confBotonSubmit: BotonCompartido

	// Enrutamiento
	public informacionEnrutador: InformacionEnrutador

	constructor(
		private albumService: AlbumService,
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private mediaNegocio: MediaNegocio,
		private convertidorArchivos: ConvertidorArchivos,
		private generadorId: GeneradorId,
		private albumNegocio: AlbumNegocio,
		private perfilNegocio: PerfilNegocio,
		private translateService: TranslateService,
		private cuentaNegocio: CuentaNegocio,
		private enrutadorService: EnrutadorService,
		private camaraService: CamaraService,
		private imagenPantallaCompletaService: ImagenPantallaCompletaService
	) {
		this.haySesionIniciada = false
		this.params = { estado: false }
		this.idItemActivo = ''
		this.itemDescripcionActivo = ''
		this.itemsAlbum = []
		this.itemsAlbumPorDefecto = []
		this.cantidadItemsPorDefecto = 7
		this.albumEnModoPreview = false
		this.textoCerrarDescripcion = ''
		this.placeholderBarraInferior = ''
		this.eventoEnitemFuncion = (data: InfoAccionCirRec) => {
			this.eventoEnItem(data)
		}
	}

	ngOnInit(): void {
		this.configurarParametrosDeUrl()
		this.validarSessionIniciada()
		this.inicializarPerfilSeleccionado()
		this.configurarEscuchaCamaraService()
		this.imagenPantallaCompletaService.configurarPortadaExp()
		if (this.params.estado && ((this.haySesionIniciada &&
			this.perfilSeleccionado) || (!this.haySesionIniciada))) {
			this.inicializarDataAlbum()
			this.configurarToast()
			this.configurarBoton()
			this.configurarAppBar()
			this.configurarPortada()
			this.configurarLinea()
			this.confgurarBotonUploadPhotos()
			this.configurarItemsAlbumPorDefecto()
			this.configurarItemsAlbum()
			this.configurarBarraInferior()
			this.configurarDone()

			return
		}

		this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
	}

	ngOnDestroy(): void {
		this.camaraService.deSuscribir()
	}

	configurarParametrosDeUrl() {
		if (
			!this.informacionEnrutador ||
			!this.informacionEnrutador.posicion
		) {
			this.params.estado = false
			return
		}

		this.informacionEnrutador.params.forEach(item => {
			if (item.nombre === 'titulo') {
				this.params.titulo = item.valor
			}

			if (item.nombre === 'accionEntidad') {
				this.params.accionEntidad = item.valor
			}

			if (item.nombre === 'entidad') {
				this.params.entidad = item.valor
			}
		})


		this.params = this.albumService.
			validarParametrosDelAlbumSegunAccionEntidadEnrutador(this.params)
	}

	// Inicializar perfil seleccionado
	inicializarPerfilSeleccionado() {
		this.perfilSeleccionado = (this.haySesionIniciada) ?
			this.perfilNegocio.obtenerPerfilSeleccionado() :
			this.perfilNegocio.obtenerPerfilActivoTercio(
				this.informacionEnrutador.posicion)

	}

	validarSessionIniciada() {
		this.haySesionIniciada = this.cuentaNegocio.sesionIniciada()
	}

	inicializarDataAlbum() {

		this.album = this.albumNegocio.obtenerAlbumActivoDelTercio(
			this.informacionEnrutador.posicion
		)

		if (!this.album) {
			this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
		}
	}

	configurarEscuchaCamaraService() {
		this.camaraService.suscripcionFotoCapturada$ = this.camaraService.
			escuchaFotoCapturada$.subscribe(imagen => {
				this.subirArchivoDeCamaraAlServidor(imagen)
			})
	}

	async configurarItemsAlbum() {
		if (this.album) {
			this.album.media.forEach(media => {
				this.itemsAlbum.push({
					id: media.id,
					idInterno: this.generadorId.generarIdConSemilla(),
					usoDelItem: UsoItemRectangular.RECALBUMMINI,
					esVisitante: (this.params.accionEntidad === AccionEntidad.VISITAR),
					urlMedia: media.principal.url,
					activarClick: true,
					activarDobleClick: true,
					activarLongPress: true,
					mostrarBoton: false,
					mostrarLoader: true,
					textoBoton: 'Click to upload',
					capaOpacidad: {
						mostrar: false
					},
					eventoEnItem: this.eventoEnitemFuncion,
					descripcion: media.descripcion,
					textoCerrarEditarDescripcion: this.textoCerrarDescripcion,
					mostrarIconoExpandirFoto: false,
					mostrarCapaImagenSeleccionadaConBorde: false,
					esBotonUpload: false,
					colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
					colorDeFondo: ColorDeFondo.FONDO_TRANSPARENTE,
					mostrarCorazon: false,
					barraInferior: {
						mostrar: false,
						configuracion: {
							placeholder: '',
							contenido: media.descripcion,
							mostrarIconoTexto: true,
						}
					}
				})
			})
			if (this.album.portada && this.album.portada.principal &&
				this.album.portada.principal.url.length > 0) {
				const portada: MediaModel = this.album.portada
				this.confPortada.mostrarLoader = true
				this.confPortada.id = portada.id
				this.confPortada.urlMedia = portada.principal.url
				this.confPortada.mostrarBoton = false
			}

			this.textoCerrarDescripcion = await this.translateService.
				get('m2v19texto6').toPromise()
			this.placeholderBarraInferior = await this.translateService.
				get('m2v19texto7').toPromise()
			this.itemsAlbum.forEach(item => {
				item.barraInferior.configuracion.placeholder = this.placeholderBarraInferior
				item.textoCerrarEditarDescripcion = this.textoCerrarDescripcion
			})
		}
	}



	async configurarBarraInferior() {
		this.placeholderBarraInferior = await this.translateService.
			get('m2v19texto7').toPromise()
	}

	configurarDone() {
		this.confDone = {
			intervalo: 4000,
			mostrarDone: false,
			mostrarLoader: false
		}
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false,
			mostrarLoader: false,
			cerrarClickOutside: false,
			texto: '',
			intervalo: 5,
			bloquearPantalla: false,
		}
	}

	configurarAppBar() {
		const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
			this.params.entidad,
			this.perfilSeleccionado,
			CodigosCatalogoTipoAlbum.GENERAL
		)

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			dataAppBar.subtitulo = 'm4v9texto1'
		}

		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				buscador: {
					mostrar: false,
					configuracion: {

						disable: true
					}
				},
				nombrePerfil: {
					mostrar: true,
					llaveTexto: dataAppBar.nombrePerfil
				},
				mostrarDivBack: {
					icono: true,
					texto: this.haySesionIniciada ? true : false,

				},
				mostrarTextoHome: this.haySesionIniciada ? true : false,
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm2v17texto1'
				},
				mostrarLineaVerde: true,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
			},
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => this.accionAtrasAppBarBack(false),
			eventoHome: () => this.enrutadorService.navegar(
				{
					componente: MenuPrincipalComponent,
					params: [
					]
				},
				{
					estado: true,
					posicicion: this.informacionEnrutador.posicion,
					extras: {
						tipo: TipoDeNavegacion.NORMAL,
					}
				}
			)
		}
	}

	configurarBoton() {
		this.confBotonSubmit = {
			text: 'm2v3texto20',
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			colorTexto: ColorTextoBoton.AMARRILLO,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.accionAtrasAppBarBack()
			}
		}
	}

	async configurarPortada() {
		this.confPortada = {
			id: '',
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemRectangular.RECPERFIL,
			esVisitante: true,
			urlMedia: '',
			activarClick: false,
			activarDobleClick: false,
			activarLongPress: false,
			mostrarBoton: (this.params.accionEntidad !== AccionEntidad.VISITAR),
			mostrarLoader: false,
			textoBoton: 'Chosen photos',
			eventoEnItem: this.eventoEnitemFuncion,
			capaOpacidad: {
				mostrar: false
			},
			esBotonUpload: false,
			colorBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
		}
		const texto = await this.translateService.get('m2v17texto2').toPromise()
		const texto2 = await this.translateService.get('m2v17texto3').toPromise()
		this.confPortada.textoBoton = texto + '\n' + texto2

		if (
			this.params.entidad === CodigosCatalogoEntidad.NOTICIA &&
			!this.album.predeterminado
		) {
			this.confPortada.textoBoton = ''
		}
	}

	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR041,
			forzarAlFinal: false
		}
	}

	async confgurarBotonUploadPhotos() {
		this.confBotonUpload = {
			id: 'botonUpload',
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemRectangular.RECALBUMMINI,
			esVisitante: false,
			urlMedia: '',
			activarClick: true,
			activarDobleClick: false,
			activarLongPress: false,
			mostrarBoton: true,
			mostrarLoader: false,
			textoBoton: 'Click to upload',
			eventoEnItem: this.eventoEnitemFuncion,
			capaOpacidad: {
				mostrar: false
			},
			esBotonUpload: true,
			colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
		}

		const texto: string = await this.translateService.get('m2v17texto6').
			toPromise()
		const texto2: string = await this.translateService.get('m2v17texto7').
			toPromise()
		this.confBotonUpload.textoBoton = texto + '\n' + texto2
	}

	validarDivisorGrilla(pos: number) {
		if (this.params.accionEntidad !== AccionEntidad.VISITAR &&
			(pos === 0 || pos % 2 === 0)) {
			return false
		}

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			if (pos === 0 || pos % 2 === 0) {
				return true
			}

			return false
		}

		return true
	}

	obtenerColorCapaOpacidad(pos: number) {
		if (pos === 0 || pos === 3) {
			return ColorCapaOpacidadItem.CAPA_OPACIDAD_C
		}
		if (pos === 1 || pos === 2) {
			return ColorCapaOpacidadItem.CAPA_OPACIDAD_B
		}
		return ColorCapaOpacidadItem.CAPA_OPACIDAD_A
	}

	configurarItemsAlbumPorDefecto() {
		let pos = 0
		while (pos < this.cantidadItemsPorDefecto) {
			this.itemsAlbumPorDefecto.push(
				{
					id: 'itemFotoDefecto_' + pos,
					idInterno: this.generadorId.generarIdConSemilla(),
					usoDelItem: UsoItemRectangular.RECALBUMMINI,
					esVisitante: true,
					urlMedia: '',
					activarClick: false,
					activarDobleClick: false,
					activarLongPress: false,
					mostrarBoton: false,
					mostrarLoader: true,
					textoBoton: 'Click to upload',
					capaOpacidad: {
						mostrar: true,
						colorOpacidad: this.obtenerColorCapaOpacidad(pos)
					},
					esBotonUpload: false,
					colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
					colorDeFondo: ColorDeFondo.FONDO_BLANCO,
					mostrarCorazon: false,
				}
			)
			pos += 1
		}
		const tipo = this.albumService.determinarTipoItemsPorDefectoParaAlbumGeneralSegunEntidad(
			this.params.entidad)
		pos = 0
		this.mediaNegocio.obtenerListaArchivosDefault().subscribe(data => {
			data.forEach((item) => {
				if (item.catalogoArchivoDefault === tipo) {
					if (pos < this.cantidadItemsPorDefecto) {
						this.itemsAlbumPorDefecto[pos].urlMedia = item.url
						pos += 1
					}
				}
			})
		}, error => {
			this.itemsAlbumPorDefecto.forEach(item => {
				item.mostrarBoton = true
				item.textoBoton = ''
			})
		})
	}

	subirArchivoDeCamaraAlServidor(image: WebcamImage) {
		const imagen = this.convertidorArchivos.dataURItoBlob(image.imageAsDataUrl)
		const idItem: string = this.generadorId.generarIdConSemilla()
		this.itemsAlbum.push({
			id: idItem,
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemRectangular.RECALBUMMINI,
			esVisitante: false,
			urlMedia: '',
			activarClick: true,
			activarDobleClick: true,
			activarLongPress: true,
			mostrarBoton: false,
			mostrarLoader: true,
			textoBoton: 'Click to upload',
			capaOpacidad: {
				mostrar: false
			},
			eventoEnItem: this.eventoEnitemFuncion,
			descripcion: '',
			textoCerrarEditarDescripcion: this.textoCerrarDescripcion,
			mostrarIconoExpandirFoto: false,
			mostrarCapaImagenSeleccionadaConBorde: false,
			esBotonUpload: false,
			colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
			barraInferior: {
				mostrar: false,
				configuracion: {
					placeholder: this.placeholderBarraInferior,
					contenido: '',
					mostrarIconoTexto: true,
				}
			}
		})

		const data: SubirArchivoData = {
			archivo: imagen,
			formato: 'image/jpeg',
			relacionAspecto: '1:1',
			descripcion: '',
			catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
		}

		this.subirMedia(data, idItem)
	}

	subirArchivoDeSelectorAlServidor(file: File) {
		const idItem: string = this.generadorId.generarIdConSemilla()
		this.itemsAlbum.push({
			id: idItem,
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemRectangular.RECALBUMMINI,
			esVisitante: false,
			urlMedia: '',
			activarClick: true,
			activarDobleClick: true,
			activarLongPress: true,
			mostrarBoton: false,
			mostrarLoader: true,
			textoBoton: 'Click to upload',
			capaOpacidad: {
				mostrar: false
			},
			eventoEnItem: this.eventoEnitemFuncion,
			descripcion: '',
			textoCerrarEditarDescripcion: this.textoCerrarDescripcion,
			mostrarIconoExpandirFoto: false,
			mostrarCapaImagenSeleccionadaConBorde: false,
			esBotonUpload: false,
			colorBorde: ColorDeBorde.BORDER_ROJO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
			barraInferior: {
				mostrar: false,
				configuracion: {
					placeholder: this.placeholderBarraInferior,
					contenido: '',
					mostrarIconoTexto: true,
				}
			}
		})

		const data: SubirArchivoData = {
			archivo: file,
			formato: 'image/' + file.name.split('.')[file.name.split('.').length - 1],
			relacionAspecto: '1:1',
			descripcion: '',
			catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO,
		}

		this.subirMedia(data, idItem)
	}

	async subirMedia(
		data: SubirArchivoData,
		idItem: string
	) {
		try {
			const media: MediaModel = await this.albumNegocio.subirMedia(
				data,
				this.params,
				this.album,
				this.informacionEnrutador.posicion
			)
			const pos = this.obtenerPosicionPorIdItem(idItem)
			if (media) {
				this.album.media.push(media)
				if (pos >= 0) {
					this.itemsAlbum[pos].id = media.id
					this.itemsAlbum[pos].urlMedia = media.principal.url
					this.itemsAlbum[pos].descripcion = media.descripcion
					this.itemsAlbum[pos].barraInferior.configuracion.contenido = media.descripcion
				}
			} else {
				this.itemsAlbum.splice(pos, 1)
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	async borrarMediaDelApi(data: InfoAccionCirRec) {
		if (!(data.informacion.id && data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.borrarMedia(
				data.informacion.id,
				this.params,
				this.album,
				this.informacionEnrutador.posicion
			)

			if (!status) {
				throw new Error('')
			}

			this.borrarMediaDelLocal(data)

			if (this.album.media.length === 0) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.albumNegocio.removerAlbumActivoDelTercio(
						this.informacionEnrutador.posicion)
					this.enrutadorService.navegarAlBack(
						this.informacionEnrutador.posicion)
				})
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	async asignarPortada(data: InfoAccionCirRec, accion: string) {
		if (!(data.informacion && data.informacion.id && data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.asignarPortada(
				data.informacion.id,
				this.params,
				this.album,
				this.informacionEnrutador.posicion
			)


			if (status) {
				this.establecerPortadaEnLocal(data, accion)
			} else {
				this.toast.abrirToast('text37')
			}

		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	async removerPortada(data: InfoAccionCirRec, accion: string) {
		if (!(data.informacion && data.informacion.id && data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.removerPortada(
				data.informacion.id,
				this.params,
				this.album,
				this.informacionEnrutador.posicion
			)
			if (status) {
				this.establecerPortadaEnLocal(data, accion)
			} else {
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	async actualizarDescripcionEnElApi() {
		try {
			const status: boolean = await this.albumNegocio.actualizarDescripcionDeLaMedia(
				this.idItemActivo,
				this.itemDescripcionActivo,
				this.params,
				this.album,
				this.informacionEnrutador.posicion
			)

			if (status) {
				this.actualizarDescripcionEnLocal()
			} else {
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	// Evento en items
	eventoEnItem(data: InfoAccionCirRec) {
		console.log('data', data);
		// Tomar Foto
		if (data.accion === AccionesItemCircularRectangular.TOMAR_FOTO) {
			this.camaraService.reiniciarCamara()
			this.camaraService.cambiarEstadoCamara(true)
			return
		}

		// Subir archivo
		if (data.accion === AccionesItemCircularRectangular.SUBIR_ARCHIVO) {
			this.subirArchivoDeSelectorAlServidor(data.informacion.archivo[0])
			return
		}

		// Borrar item
		if (data.accion === AccionesItemCircularRectangular.BORRAR_ITEM) {
			this.validarAccionEntidadParaBorrarItemDelAlbum(data)

			if (this.itemsAlbum.length <= 0) {
				this.albumEnModoPreview = false
			}
			return
		}

		// Establecer foto por defecto
		if (data.accion === AccionesItemCircularRectangular.ESTABLECER_ITEM_PREDETERMINADO) {
			this.validarAccioParaEstablecerPortada(data)
			return
		}

		// Cambiar a modo preview admin
		if (data.accion === AccionesItemCircularRectangular.CAMBIAR_A_MODO_ALBUM_PREVIEW_ADMIN) {
			this.cambiarAModoAlbumPreview()
			return
		}

		// Cambiar a modo preview admin
		if (data.accion === AccionesItemCircularRectangular.CAMBIAR_A_MODO_ALBUM_PREVIEW_VISITA) {
			this.cambiarAModoAlbumPreview()
			return
		}

		// Editar descripcion
		if (data.accion === AccionesItemCircularRectangular.EDITAR_DESCRIPCION) {
			this.validarAccionEntidadParaDefinirInformacionDeLaMedia(data)
			return
		}

		// Dejar de editar descripcion
		if (data.accion === AccionesItemCircularRectangular.DEJAR_DE_EDITAR_DESCRIPCION) {
			this.idItemActivo = ''
			this.itemDescripcionActivo = ''
			return
		}

		if (data.accion === AccionesItemCircularRectangular.EXPANDIR_FOTO_DEL_ITEM) {
			this.abrirImagenEnPantallaCompleta(data)
			return
		}

		if (data.accion === AccionesItemCircularRectangular.ACTUALIZAR_DESCRIPCION) {
			this.idItemActivo = data.informacion.id
			this.itemDescripcionActivo = data.informacion.contenido
			this.validarAccionEntidadParaActualizarDescripcion()
			return
		}

	}

	definirInformacionParaEditarDescripcion(data: InfoAccionCirRec) {
		this.idItemActivo = data.informacion
		this.itemsAlbum.forEach(item => {
			if (item.id === this.idItemActivo) {
				item.barraInferior.configuracion.contenido = item.descripcion
				item.barraInferior.mostrar = true
			} else {
				item.barraInferior.mostrar = false
				item.mostrarCapaImagenSeleccionadaConBorde = false
			}
		})
	}

	// Editar descripcion de la media
	validarAccionEntidadParaDefinirInformacionDeLaMedia(data: InfoAccionCirRec) {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.definirInformacionParaEditarDescripcion(data)
				break
			case AccionEntidad.ACTUALIZAR:
				this.definirInformacionParaEditarDescripcion(data)
				break
			default: break;
		}
	}

	// Cambiar a modo album preview
	cambiarAModoAlbumPreview() {
		this.albumEnModoPreview = true
		// Avisar a los item para que pasen a modo preview admin
		this.itemsAlbum.forEach(item => {
			item.usoDelItem = UsoItemRectangular.RECALBUMPREVIEW
			item.mostrarIconoExpandirFoto = (
				this.params.accionEntidad === AccionEntidad.VISITAR)
		})
	}

	borrarMediaDelLocal(data: InfoAccionCirRec) {
		let pos = -1
		this.itemsAlbum.forEach((item, i) => {
			if (item.id === data.informacion.id) {
				pos = i
			}
		})
		// Validar si el item de la portada fue borrado
		if (this.itemsAlbum[pos].id === this.confPortada.id) {
			this.confPortada.id = ''
			this.confPortada.urlMedia = ''
			this.confPortada.mostrarLoader = false
			this.confPortada.mostrarBoton = true

			// Actualizar el album
			this.album.portada.id = ''
			this.album.portada.principal.id = ''
			this.album.portada.principal.url = ''
		}
		this.album.media.splice(pos, 1)
		this.itemsAlbum.splice(pos, 1)
	}

	validarAccionEntidadParaBorrarItemDelAlbum(data: InfoAccionCirRec) {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.borrarMediaDelLocal(data)
				break
			case AccionEntidad.ACTUALIZAR:
				this.borrarMediaDelApi(data)
				break
			default: break;
		}
	}

	determinarAccionParaPortada(data: InfoAccionCirRec): string {
		if (this.confPortada.id !== data.informacion.id) {
			return 'asignar'
		}

		if (this.confPortada.id === data.informacion.id) {
			return 'remover'
		}

		return null
	}

	establecerPortadaEnLocal(data: InfoAccionCirRec, accion: string) {
		switch (accion) {
			case 'asignar':
				this.confPortada.mostrarBoton = false
				this.confPortada.id = data.informacion.id
				this.confPortada.mostrarLoader = true
				this.confPortada.urlMedia = data.informacion.urlMedia
				// Portada en item album
				const portadaMedia: MediaModel = this.obtenerMediaPorIdItem(
					this.confPortada.id)

				// Definir tipo a la portada
				portadaMedia.principal.tipo = {
					codigo: CodigosCatalogoTipoArchivo.IMAGEN
				}
				// Setear portada en el album activo
				this.album.portada = {
					...portadaMedia,
					catalogoMedia: {
						codigo: CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
					},
				}
				break
			case 'remover':
				this.confPortada.id = ''
				this.confPortada.urlMedia = ''
				this.confPortada.mostrarLoader = false
				this.confPortada.mostrarBoton = true
				// Setear portada en el album activo
				this.album.portada = {}
				break
			default: break;
		}
	}

	establecerPortadaEnElApi(
		data: InfoAccionCirRec,
		accion: string
	) {
		switch (accion) {
			case 'asignar':
				this.asignarPortada(data, accion)
				break
			case 'remover':
				this.removerPortada(data, accion)
				break
			default: break;
		}
	}

	validarAccioParaEstablecerPortada(data: InfoAccionCirRec) {
		const accion: string = this.determinarAccionParaPortada(data)
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				if (data.informacion && data.informacion.id &&
					data.informacion.id.length > 0) {
					this.establecerPortadaEnLocal(data, accion)
				}
				break
			case AccionEntidad.ACTUALIZAR:
				this.establecerPortadaEnElApi(data, accion)
				break
			default: break;
		}
	}

	obtenerPosicionPorIdItem(id: string) {
		let pos = -1
		this.itemsAlbum.forEach((item, i) => {
			if (item.id === id) {
				pos = i
			}
		})
		return pos
	}

	obtenerMediaPorIdItem(id: string): MediaModel {
		let media: MediaModel
		this.album.media.forEach((item, i) => {
			if (item.id === id) {
				media = item
			}
		})
		return media
	}

	actualizarDescripcionEnLocal() {
		// Actualizar item
		this.itemsAlbum.forEach(item => {
			if (item.id === this.idItemActivo) {
				item.descripcion = this.itemDescripcionActivo
				item.mostrarCapaImagenSeleccionadaConBorde = false
				item.barraInferior.configuracion.contenido = ''
				item.barraInferior.mostrar = false
			}
		})

		// Actualizar media
		let pos = -1
		this.album.media.forEach((item, i) => {
			if (item.id === this.idItemActivo) {
				item.descripcion = this.itemDescripcionActivo
				pos = i
			}
		})

		this.idItemActivo = ''
		this.itemDescripcionActivo = ''
	}

	actualizarDescripcionSegunAccionProyectoNoticia() {

	}

	// Actualizar la descripcion del item
	validarAccionEntidadParaActualizarDescripcion() {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.actualizarDescripcionEnLocal()
				break
			case AccionEntidad.ACTUALIZAR:
				this.actualizarDescripcionEnElApi()
				break
			default: break;
		}
	}

	accionAtrasAppBarBack(
		esDelSubmit: boolean = true
	) {
		if (this.albumEnModoPreview) {
			this.albumEnModoPreview = false
			this.itemsAlbum.forEach(item => {
				item.usoDelItem = UsoItemRectangular.RECALBUMMINI
				item.mostrarIconoExpandirFoto = false
				item.mostrarCapaImagenSeleccionadaConBorde = false
				if (item.barraInferior) {
					item.barraInferior.mostrar = false
					item.barraInferior.configuracion.contenido = ''
				}
			})
			this.idItemActivo = ''
			this.itemDescripcionActivo = ''
			return
		}

		if (this.params.accionEntidad === AccionEntidad.VISITAR) {
			this.albumNegocio.removerAlbumActivoDelTercio(
				this.informacionEnrutador.posicion)
			this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
			return
		}

		
		this.guardarAlbumAntesDeSalir(esDelSubmit, true)
	}

	abrirImagenEnPantallaCompleta(data: InfoAccionCirRec) {

		if (
			data &&
			data.informacion &&
			data.informacion.urlMedia
		) {
			this.imagenPantallaCompletaService.configurarImagenPantallaCompleta(
				true, true, data.informacion.urlMedia)
		}
	}

	async guardarAlbumAntesDeSalir(
		esDelSubmit: boolean,
		destruirAlbum: boolean = false
	) {
		
		try {
			let enviarCrearAlbum = false
			const idEntidad: string = this.albumNegocio.obtenerIdDeLaEntidadSegunCodigo(
				this.params.entidad,
				this.informacionEnrutador.posicion
			)	

			if (
				this.params.accionEntidad === AccionEntidad.CREAR &&
				idEntidad &&
				idEntidad.length > 0 &&
				this.album.media.length > 0
			) {
				enviarCrearAlbum = true
			}

			if (enviarCrearAlbum) {

				const albumCreado: AlbumModel = await this.albumNegocio.agregarAlbumEnEntidad(
					idEntidad,
					this.params.entidad,
					this.album
				).toPromise()

				if (!albumCreado) {
					throw new Error('')
				}

				this.album._id = albumCreado._id
			}

			// Actualizar estado predeterminado del album
			if (
				this.params.entidad === CodigosCatalogoEntidad.NOTICIA &&
				this.params.accionEntidad === AccionEntidad.ACTUALIZAR
			) {

				if (this.album) {
					await this.albumNegocio.actualizarParametrosDelAlbum(
						idEntidad,
						this.params.entidad,
						{
							_id: this.album._id,
							predeterminado: this.album.predeterminado
						}
					).toPromise()
				}
				
			}

			this.album = this.albumNegocio.actualizarEstadoDelAlbum(this.album, CodigosCatalogoEstadoAlbum.CREADO)
			this.albumNegocio.validarActualizacionDelAlbumSegunParams(
				this.album,
				this.params,
				destruirAlbum,
				this.informacionEnrutador.posicion
			)
			this.toast.cerrarToast()

			if (esDelSubmit) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
				}, 600)
			}

			if (!esDelSubmit) {
				this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	guardarAlbumAntesDeSalirReload(
		destruirAlbum: boolean = false
	) {
		this.albumNegocio.validarActualizacionDelAlbumSegunParams(
			this.album,
			this.params,
			destruirAlbum,
			this.informacionEnrutador.posicion
		)
	}

	// Enrutamiento
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}

}
