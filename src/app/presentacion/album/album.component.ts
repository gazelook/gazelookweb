import { Component, OnInit } from '@angular/core'
import * as serviciosGenerales from '@core/servicios/generales';
@Component({
	selector: 'app-album-general',
	templateUrl: './album.component.html',
	styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

	constructor(
		private notificacionesUsuario: serviciosGenerales.NotificacionesDeUsuario
	) {

	}

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
	}

}