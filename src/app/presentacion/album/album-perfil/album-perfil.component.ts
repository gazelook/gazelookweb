import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { AlbumService, CamaraService, EnrutadorService, GeneradorId, VariablesGlobales } from '@core/servicios/generales';
import {
  AccionAlbum, AccionEntidad, CodigosCatalogoArchivosPorDefecto, CodigosCatalogoEntidad, CodigosCatalogoEstadoAlbum, CodigosCatalogoTipoAlbum, CodigosCatalogoTipoArchivo, CodigosCatalogoTipoMedia, CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import { ConvertidorArchivos } from '@core/util';
import { TranslateService } from '@ngx-translate/core';
import {
  AppbarComponent, ColorTextoBoton, CropperComponent, TipoBoton,
  ToastComponent
} from '@shared/componentes';
import { AccionesItemCircularRectangular, AnchoLineaItem, ColorCapaOpacidadItem, ColorDeBorde, ColorDeFondo, ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, TamanoDeTextoConInterlineado, UsoAppBar, UsoItemCircular } from '@shared/diseno/enums';
import { BotonCompartido, ConfiguracionAppbarCompartida, ConfiguracionDone, ConfiguracionToast, InfoAccionCirRec, ItemCircularCompartido, LineaCompartida } from '@shared/diseno/modelos';
import {
  AlbumNegocio, CuentaNegocio, MediaNegocio, PerfilNegocio
} from 'dominio/logica-negocio';
import { SubirArchivoData } from 'dominio/modelo';
import {
  AlbumModel, MediaModel, PerfilModel
} from 'dominio/modelo/entidades';
import { AlbumParams } from 'dominio/modelo/parametros';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';

@Component({
  selector: 'app-album-perfil',
  templateUrl: './album-perfil.component.html',
  styleUrls: ['./album-perfil.component.scss']
})
export class AlbumPerfilComponent implements OnInit, OnDestroy, Enrutador {
  @ViewChild('appbar', { static: false }) appbar: AppbarComponent
  @ViewChild('cropper', { static: false }) cropper: CropperComponent
  @ViewChild('toast', { static: false }) toast: ToastComponent

  // Utils
  public accionAlbumEnum = AccionAlbum
  public codigosTipoPerfil = CodigosCatalogoTipoPerfil
  public eventoEnitemFuncion: Function
  public observable: any
  public AccionEntidadEnum = AccionEntidad
  // Parametros de url
  public params: AlbumParams

  // Parametros internos
  public haySesionIniciada: boolean
  public perfilSeleccionado: PerfilModel
  public album: AlbumModel // Album en uso
  public cantidadMaximaFotos: number
  public listaMediaAlbum: Array<MediaModel> // Para almacenar las medias a devolver en caso de crear
  public itemsAlbum: Array<ItemCircularCompartido> // Array de items compartido
  public itemsAlbumPorDefecto: Array<ItemCircularCompartido> // Array de items compartido - fotos por defecto
  public cantidadItemsPorDefecto: number // Numero total de items por defecto a mostrar
  public confBotonSubmit: BotonCompartido

  // Configuraciones
  public confToast: ConfiguracionToast // Configuracion del toast
  public confAppBar: ConfiguracionAppbarCompartida // Configuracion appbar compartida
  public confPortada: ItemCircularCompartido // Portada del album
  public confBotonUpload: ItemCircularCompartido // Boton siempre visible de upload photos
  public confLinea: LineaCompartida // Linea compartida
  public confDone: ConfiguracionDone

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private mediaNegocio: MediaNegocio,
    private convertidorArchivos: ConvertidorArchivos,
    private generadorId: GeneradorId,
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private albumService: AlbumService,
    private cuentaNegocio: CuentaNegocio,
    private translateService: TranslateService,
    private enrutadorService: EnrutadorService,
    private camaraService: CamaraService,
  ) {
    this.haySesionIniciada = false
    this.params = { estado: false }
    this.cantidadMaximaFotos = 6
    this.listaMediaAlbum = []
    this.itemsAlbum = []
    this.itemsAlbumPorDefecto = []
    this.cantidadItemsPorDefecto = 5
    this.eventoEnitemFuncion = (data: InfoAccionCirRec) => {
      this.eventoEnItem(data)
    }
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = false
    this.validarSessionIniciada()
    this.configurarParametrosDeUrl()
    this.inicializarPerfilSeleccionado()
    this.configurarBoton()
    this.configurarEscuchaCamaraService()
    if (this.params.estado && ((this.haySesionIniciada && this.perfilSeleccionado) || (!this.haySesionIniciada))) {
      this.inicializarDataAlbum()
      this.configurarToast()
      this.configurarAppBar()
      this.configurarPortada()
      this.configurarLinea()
      this.confgurarBotonUploadPhotos()
      this.configurarItemsAlbumPorDefecto()
      this.configurarItemsAlbum()
      this.configurarDone()
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  ngOnDestroy(): void {
    this.camaraService.deSuscribir()
  }

  configurarEscuchaCamaraService() {
    this.camaraService.suscripcionFotoCortada$ = this.camaraService.escuchaFotoCortada$.subscribe(imagen => {
      this.subirArchivoAlServidor(imagen)
    })
  }

  async configurarBoton() {
    this.confBotonSubmit = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,

      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.accionAtrasAppBarBack()
      }
    }
  }

  configurarParametrosDeUrl() {
    if (
      !this.informacionEnrutador ||
      !this.informacionEnrutador.posicion
    ) {
      this.params.estado = false
      return
    }

    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'titulo') {
        this.params.titulo = item.valor
      }

      if (item.nombre === 'accionEntidad') {
        this.params.accionEntidad = item.valor
      }

      if (item.nombre === 'entidad') {
        this.params.entidad = item.valor
      }

      if (item.nombre === 'codigo') {
        this.params.codigo = item.valor
      }
    })
    this.params = this.albumService.validarParametrosDelAlbumSegunAccionEntidadEnrutador(this.params)
  }

  // Inicializar perfil seleccionado
  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = (this.haySesionIniciada) ?
      this.perfilNegocio.obtenerPerfilSeleccionado() :
      this.perfilNegocio.obtenerPerfilActivoTercio(this.informacionEnrutador.posicion)
  }

  validarSessionIniciada() {
    this.haySesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  inicializarDataAlbum() {
    this.album = this.albumNegocio.obtenerAlbumActivoDelTercio(
      this.informacionEnrutador.posicion
    )

    if (!this.album) {
      this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
    }
  }

  configurarItemsAlbum() {
    // Items para el album
    if (this.album) {
      this.album.media.forEach(media => {
        this.itemsAlbum.push({
          id: media.id,
          idInterno: this.generadorId.generarIdConSemilla(),
          usoDelItem: UsoItemCircular.CIRALBUM,
          esVisitante: false,
          urlMedia: media.principal.url,
          activarClick: false,
          activarDobleClick: true,
          activarLongPress: true,
          mostrarBoton: false,
          mostrarLoader: true,
          fotoPredeterminadaRamdon: this.determinarOrigenRamdomDeLaPortada(),
          textoBoton: 'Click to upload',
          capaOpacidad: {
            mostrar: false
          },
          eventoEnItem: this.eventoEnitemFuncion,
          esBotonUpload: false,
          colorBorde: ColorDeBorde.BORDER_ROJO,
          colorDeFondo: ColorDeFondo.FONDO_BLANCO,
          mostrarCorazon: false,
        })
      })
      // Definir la portada
      if (this.album.portada && this.album.portada.principal && this.album.portada.principal.url.length > 0) {
        const portada: MediaModel = this.album.portada
        this.confPortada.mostrarLoader = true
        this.confPortada.id = portada.id
        this.confPortada.urlMedia = portada.principal.url
        this.confPortada.mostrarBoton = false
      }
    }
  }

  configurarDone() {
    this.confDone = {
      intervalo: 4000,
      mostrarDone: false,
      mostrarLoader: false
    }
  }

  determinarOrigenRamdomDeLaPortada() {
    let origen = false
    if (this.params.entidad === CodigosCatalogoEntidad.PERFIL) {
      if (this.perfilSeleccionado.tipoPerfil.codigo === CodigosCatalogoTipoPerfil.PLAYFUL) {
        origen = true
      }
    }
    return origen
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: ''
    }
  }

  configurarAppBar() {
    const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
      this.params.entidad,
      this.perfilSeleccionado,
      CodigosCatalogoTipoAlbum.GENERAL
    )

    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarDivBack: {
          icono: true,
          texto: this.haySesionIniciada ? true : false,
        },
        mostrarTextoHome: this.haySesionIniciada ? true : false,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v14texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      },
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => this.accionAtrasAppBarBack(false),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [
          ]
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      )
    }
  }

  async configurarPortada() {
    this.confPortada = {
      id: '',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemCircular.CIRPERFIL,
      esVisitante: true,
      urlMedia: '',
      activarClick: false,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: false,
      textoBoton: ' ',
      eventoEnItem: this.eventoEnitemFuncion,
      capaOpacidad: {
        mostrar: false
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
    }


    if (this.perfilSeleccionado.tipoPerfil.codigo !== CodigosCatalogoTipoPerfil.PLAYFUL) {
      const texto = await this.translateService.get('m2v17texto2').toPromise()
      const texto2 = await this.translateService.get('m2v17texto3').toPromise()
      this.confPortada.textoBoton = texto + '\n' + texto2
    }

  }

  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false
    }
  }

  async confgurarBotonUploadPhotos() {
    this.confBotonUpload = {
      id: 'botonUpload',
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemCircular.CIRALBUM,
      esVisitante: false,
      urlMedia: '',
      activarClick: true,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: false,
      textoBoton: 'Click to upload',
      eventoEnItem: this.eventoEnitemFuncion,
      capaOpacidad: {
        mostrar: false
      },
      esBotonUpload: true,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
    }

    const texto: string = await this.translateService.get('m2v14texto7').toPromise()
    const texto2: string = await this.translateService.get('m2v14texto8').toPromise()
    const texto3: string = await this.translateService.get('m2v14texto9').toPromise()
    this.confBotonUpload.textoBoton = texto + '\n' + texto2 + '\n' + texto3
  }

  validarDivisorGrilla(pos: number) {
    if (pos === 0 || pos % 2 === 0) {
      return false
    }

    return true
  }

  obtenerColorCapaOpacidad(pos: number) {
    // Opacidad C
    if (pos === 0 || pos === 3) {
      return ColorCapaOpacidadItem.CAPA_OPACIDAD_C
    }
    // Opacidad B
    if (pos === 1 || pos === 2) {
      return ColorCapaOpacidadItem.CAPA_OPACIDAD_B
    }
    // Opacidad A
    return ColorCapaOpacidadItem.CAPA_OPACIDAD_A
  }

  configurarItemsAlbumPorDefecto() {
    let pos = 0
    while (pos < this.cantidadItemsPorDefecto) {
      // Llenar item
      this.itemsAlbumPorDefecto.push(
        {
          id: 'itemFotoDefecto_' + pos,
          idInterno: this.generadorId.generarIdConSemilla(),
          usoDelItem: UsoItemCircular.CIRALBUM,
          esVisitante: true,
          urlMedia: '',
          activarClick: false,
          activarDobleClick: false,
          activarLongPress: false,
          mostrarBoton: false,
          mostrarLoader: true,
          textoBoton: 'Click to upload',
          capaOpacidad: {
            mostrar: true,
            colorOpacidad: this.obtenerColorCapaOpacidad(pos)
          },
          esBotonUpload: false,
          colorBorde: ColorDeBorde.BORDER_ROJO,
          colorDeFondo: ColorDeFondo.FONDO_TRANSPARENTE,
          mostrarCorazon: false,
        }
      )
      pos += 1
    }

    pos = 0
    this.mediaNegocio.obtenerListaArchivosDefault().subscribe(data => {
      // Validar si existen datos
      data.forEach((item) => {
        if (item.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.ALBUM_PERFIL) {
          this.itemsAlbumPorDefecto[pos].urlMedia = item.url
          pos += 1
        }
      })

      if (pos === 0) {
        // Ocultar loader cuando no existe datos
        this.itemsAlbumPorDefecto.forEach(item => {
          item.mostrarLoader = false
        })
      }
    }, error => {
      this.itemsAlbumPorDefecto = []
    })
  }

  subirArchivoAlServidor(event: ImageCroppedEvent) {
    const imagen = this.convertidorArchivos.dataURItoBlob(event.base64)
    const idItem: string = this.generadorId.generarIdConSemilla()
    this.itemsAlbum.push({
      id: idItem,
      idInterno: this.generadorId.generarIdConSemilla(),
      usoDelItem: UsoItemCircular.CIRALBUM,
      esVisitante: false,
      urlMedia: '',
      activarClick: false,
      activarDobleClick: true,
      activarLongPress: true,
      mostrarBoton: false,
      mostrarLoader: true,
      fotoPredeterminadaRamdon: this.determinarOrigenRamdomDeLaPortada(),
      textoBoton: 'Click to upload',
      capaOpacidad: {
        mostrar: false
      },
      eventoEnItem: this.eventoEnitemFuncion,
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      mostrarCorazon: false,
    })

    const data: SubirArchivoData = {
      archivo: imagen,
      formato: 'image/jpeg',
      relacionAspecto: '1:1',
      descripcion: '',
      catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
    }

    this.subirMedia(data, idItem)
  }

  async subirMedia(
    data: SubirArchivoData,
    idItem: string
  ) {
    try {
      const media: MediaModel = await this.albumNegocio.subirMedia(
        data,
        this.params,
        this.album,
        this.informacionEnrutador.posicion
      )
      const pos = this.obtenerPosicionPorIdItem(idItem)
      if (media) {
        this.album.media.push(media)
        // Actualizar data en vista
        if (pos >= 0) {
          this.itemsAlbum[pos].id = media.id
          this.itemsAlbum[pos].urlMedia = media.principal.url
        }
      } else {
        this.itemsAlbum.splice(pos, 1)
        this.toast.abrirToast('text37')
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  borrarMediaDelLocal(data: InfoAccionCirRec) {
    let pos = -1
    this.itemsAlbum.forEach((item, i) => {
      if (item.id === data.informacion.id) {
        pos = i
      }
    })
    // Validar si el item de la portada fue borrado
    if (this.itemsAlbum[pos].id === this.confPortada.id) {
      this.confPortada.id = ''
      this.confPortada.urlMedia = ''
      this.confPortada.mostrarLoader = false
      this.confPortada.mostrarBoton = true

      // Actualizar el album
      this.album.portada.id = ''
      this.album.portada.principal.id = ''
      this.album.portada.principal.url = ''
    }
    this.album.media.splice(pos, 1)
    this.itemsAlbum.splice(pos, 1)
  }

  async borrarMediaDelApi(data: InfoAccionCirRec) {
    if (!(data.informacion.id && data.informacion.id.length > 0)) {
      return
    }

    try {
      const status: boolean = await this.albumNegocio.borrarMedia(
        data.informacion.id,
        this.params,
        this.album,
        this.informacionEnrutador.posicion
      )

      if (!status) {
        throw new Error('')
      }

      this.borrarMediaDelLocal(data)

      if (this.album.media.length === 0) {
        this.confDone.mostrarDone = true
        setTimeout(() => {
          this.albumNegocio.removerAlbumActivoDelTercio(this.informacionEnrutador.posicion)
          this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
        })
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  validarAccionEntidadParaBorrarItemDelAlbum(data: InfoAccionCirRec) {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.borrarMediaDelLocal(data)
        break
      case AccionEntidad.ACTUALIZAR:
        this.borrarMediaDelApi(data)
        break
      default: break;
    }
  }

  determinarAccionParaPortada(data: InfoAccionCirRec): string {
    if (this.confPortada.id !== data.informacion.id) {
      return 'asignar'
    }

    if (this.confPortada.id === data.informacion.id) {
      return 'remover'
    }

    return null
  }

  establecerPortadaEnLocal(data: InfoAccionCirRec, accion: string) {
    switch (accion) {
      case 'asignar':
        this.confPortada.mostrarBoton = false
        this.confPortada.id = data.informacion.id
        this.confPortada.mostrarLoader = true
        this.confPortada.urlMedia = data.informacion.urlMedia
        // Portada en item album
        const portadaMedia: MediaModel = this.obtenerMediaPorIdItem(this.confPortada.id)

        // Definir tipo a la portada
        portadaMedia.principal.tipo = {
          codigo: CodigosCatalogoTipoArchivo.IMAGEN
        }
        // Setear portada en el album activo
        this.album.portada = {
          ...portadaMedia,
          catalogoMedia: {
            codigo: CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
          },
        }
        break
      case 'remover':
        this.confPortada.id = ''
        this.confPortada.urlMedia = ''
        this.confPortada.mostrarLoader = false
        this.confPortada.mostrarBoton = true
        // Setear portada en el album activo
        this.album.portada = {}
        break
      default: break;
    }
  }

  async asignarPortada(data: InfoAccionCirRec, accion: string) {
    if (!(data.informacion && data.informacion.id && data.informacion.id.length > 0)) {
      return
    }

    try {
      const status: boolean = await this.albumNegocio.asignarPortada(
        data.informacion.id,
        this.params,
        this.album,
        this.informacionEnrutador.posicion
      )

      if (status) {
        this.establecerPortadaEnLocal(data, accion)
      } else {
        this.toast.abrirToast('text37')
      }

    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  async removerPortada(data: InfoAccionCirRec, accion: string) {
    if (!(data.informacion && data.informacion.id && data.informacion.id.length > 0)) {
      return
    }

    try {
      const status: boolean = await this.albumNegocio.removerPortada(
        data.informacion.id,
        this.params,
        this.album,
        this.informacionEnrutador.posicion
      )

      if (status) {
        this.establecerPortadaEnLocal(data, accion)
      } else {
        this.toast.abrirToast('text37')
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  establecerPortadaEnElApi(
    data: InfoAccionCirRec,
    accion: string
  ) {
    switch (accion) {
      case 'asignar':
        this.asignarPortada(data, accion)
        break
      case 'remover':
        this.removerPortada(data, accion)
        break
      default: break;
    }
  }

  validarAccioParaEstablecerPortada(data: InfoAccionCirRec) {
    const accion: string = this.determinarAccionParaPortada(data)
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        if (data.informacion && data.informacion.id && data.informacion.id.length > 0) {
          this.establecerPortadaEnLocal(data, accion)
        }
        break
      case AccionEntidad.ACTUALIZAR:
        this.establecerPortadaEnElApi(data, accion)
        break
      default: break;
    }
  }

  // Evento en items
  eventoEnItem(data: InfoAccionCirRec) {
    // Tomar Foto
    if (data.accion === AccionesItemCircularRectangular.TOMAR_FOTO) {

      if (this.itemsAlbum.length >= this.cantidadMaximaFotos) {
        this.toast.abrirToast('text63')
        return
      }

      this.camaraService.reiniciarCamara()
      this.camaraService.cambiarEstadoCamara(true, true)
      return
    }

    // Subir archivo
    if (data.accion === AccionesItemCircularRectangular.SUBIR_ARCHIVO) {
      if (this.itemsAlbum.length < this.cantidadMaximaFotos) {
        this.camaraService.configurarCropper(true, 'imageFile', data.informacion.archivo[0])
      } else {
        this.toast.abrirToast('text63')
      }
      return
    }

    // Borrar item
    if (data.accion === AccionesItemCircularRectangular.BORRAR_ITEM) {
      this.validarAccionEntidadParaBorrarItemDelAlbum(data)
      return
    }

    // Establecer foto por defecto
    if (data.accion === AccionesItemCircularRectangular.ESTABLECER_ITEM_PREDETERMINADO) {
      this.validarAccioParaEstablecerPortada(data)
      return
    }
  }

  obtenerPosicionPorIdItem(id: string) {
    let pos = -1
    this.itemsAlbum.forEach((item, i) => {
      if (item.id === id) {
        pos = i
      }
    })
    return pos
  }

  obtenerMediaPorIdItem(id: string): MediaModel {
    let media = null
    this.album.media.forEach((item, i) => {
      if (item.id === id) {
        media = item
      }
    })
    return media
  }

  accionAtrasAppBarBack(
    esDelSubmit: boolean = true
  ) {
    this.guardarAlbumAntesDeSalir(esDelSubmit, true)
  }

  async guardarAlbumAntesDeSalir(
    esDelSubmit: boolean,
    destruirAlbum: boolean = false
  ) {
    try {
      let enviarCrearAlbum = false
      const idEntidad: string = this.albumNegocio.obtenerIdDeLaEntidadSegunCodigo(
        this.params.entidad,
        this.informacionEnrutador.posicion
      )

      if (
        this.params.accionEntidad === AccionEntidad.CREAR &&
        idEntidad &&
        idEntidad.length > 0 &&
        this.album.media.length > 0
      ) {
        enviarCrearAlbum = true
      }

      if (enviarCrearAlbum) {

        const albumCreado: AlbumModel = await this.albumNegocio.agregarAlbumEnEntidad(
          idEntidad,
          this.params.entidad,
          this.album
        ).toPromise()

        if (!albumCreado) {
          throw new Error('')
        }

        this.album._id = albumCreado._id
      }

      this.album = this.albumNegocio.actualizarEstadoDelAlbum(this.album, CodigosCatalogoEstadoAlbum.CREADO)
      this.albumNegocio.validarActualizacionDelAlbumSegunParams(
        this.album,
        this.params,
        destruirAlbum,
        this.informacionEnrutador.posicion
      )
      this.toast.cerrarToast()

      if (esDelSubmit) {
        this.confDone.mostrarDone = true
        setTimeout(() => {
          this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
        }, 600)
      }

      if (!esDelSubmit) {
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
      }
    } catch (error) {
      this.toast.abrirToast('text36')
    }
  }

  guardarAlbumAntesDeSalirReload(
    destruirAlbum: boolean = false
  ) {
    this.albumNegocio.validarActualizacionDelAlbumSegunParams(
      this.album,
      this.params,
      destruirAlbum,
      this.informacionEnrutador.posicion
    )
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }

}
