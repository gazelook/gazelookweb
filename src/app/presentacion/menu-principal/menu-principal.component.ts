import { PaddingIzqDerDelTexto } from '@shared/diseno/enums/estilos-padding-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import {
  ColorDelTexto,
  EstilosDelTexto,
} from '@shared/diseno/enums/estilos-colores-general';
import { CuotaExtraComponent } from './../cuota-extra/cuota-extra.component';
import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NotificacionesDePerfil,
  NotificacionesDeUsuario,
} from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { DataNotificaciones } from '@core/servicios/generales/notificaciones';
import {
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoPerfil,
} from '@core/servicios/remotos/codigos-catalogos';
import { TipoMenu } from '@shared/componentes';
import {
  AnchoLineaItem,
  ColorFondoItemMenu,
  ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  TamanoItemMenu,
  TamanoLista,
  TamanoPortadaGaze,
  TipoDialogo,
  UsoAppBar,
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida,
  ConfiguracionLineaVerde,
  ConfiguracionDialogoInline,
  DatosLista,
  DialogoCompartido,
  ItemMenuCompartido,
  PortadaGazeCompartido,
} from '@shared/diseno/modelos';
import {
  CuentaNegocio,
  IdiomaNegocio,
  PerfilNegocio,
} from 'dominio/logica-negocio';
import {
  ItemAccion,
  ItemMenuModel,
  ItemSubMenu,
  PerfilModel,
} from 'dominio/modelo/entidades';
import { AnuncioSistema } from '../../nucleo/servicios/remotos/rutas';
import { RutasLocales } from '../../rutas-locales.enum';
import { AnunciosSistemaComponent } from '../anuncios-sistema/anuncios-sistema.component';
import { CompraIntermacioInicioComponent } from '../compra-intercambio/compra-intermacio-inicio/compra-intermacio-inicio.component';
import { ContactoComponent } from '../contacto/contacto.component';
import {
  Enrutador,
  EstadoEnrutador,
  InformacionAEnrutar,
  InformacionEnrutador,
  TipoDeNavegacion,
} from '../enrutador/enrutador.component';
import { FinanzasComponent } from '../finanzas/finanzas.component';
import { ChatContactosComponent } from '../gazing/chat-contactos/chat-contactos.component';
import { MenuPublicarProyectoNoticiaComponent } from '../menu-publicar-proyecto-noticia/menu-publicar-proyecto-noticia.component';
import { MiCuentaComponent } from '../mi-cuenta/mi-cuenta.component';
import { NoticiasUsuariosComponent } from '../noticias/noticias-usuarios/noticias-usuarios.component';
import { NuestrasMetasComponent } from '../nuestras-metas/nuestras-metas.component';
import { PensamientoComponent } from '../pensamiento/pensamiento.component';
import { PerfilComponent } from '../perfiles/perfil/perfil.component';
import { MenuVerProyectosComponent } from '../proyectos/menu-ver-proyectos/menu-ver-proyectos.component';
import { CatalogoIdiomaEntity } from '../../dominio/entidades/catalogos';
@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.scss'],
})
export class MenuPrincipalComponent implements OnInit, OnDestroy, Enrutador {
  // Enrutador
  public informacionEnrutador: InformacionEnrutador;

  public perfilSeleccionado: PerfilModel;
  public mostrarLinea: boolean;
  public notificacionesMensajes: any;
  public listaMenu: ItemMenuModel[];
  public itemSubMenu3Puntos: ItemSubMenu;
  public itemMenuMultipleAccion: ItemMenuModel;
  public sesionIniciada: boolean;
  public dataNotificacionesMensajes: DataNotificaciones;
  public idiomaSeleccionado: CatalogoIdiomaEntity;
  public dataLista: DatosLista;
  public confAppBar: ConfiguracionAppbarCompartida;
  public confLinea: ConfiguracionLineaVerde;
  public confPortada: PortadaGazeCompartido;
  public confDialogoLateral: DialogoCompartido;
  public confDialogoConstruccion: DialogoCompartido;
  constructor(
    // tslint:disable-next-line: variable-name
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private perfilNegocio: PerfilNegocio,
    private cuentaNegocio: CuentaNegocio,
    private db: AngularFireDatabase,
    private notificacionesPerfil: NotificacionesDePerfil,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private enrutadorService: EnrutadorService,
    private idiomaNegocio: IdiomaNegocio
  ) {
    this.mostrarLinea = false;
    this.sesionIniciada = false;
  }

  ngOnInit(): void {
    this.prepararAppBar();
    this.obtenerIdioma();
    this.configurarDialogo();
    this.configurarPortada();
    this.prepararItemsMenu();
    this.prepararDataSubMenu3puntos();
    this.prepararDatosParaMenuMultipleAccion();
    this.configurarEstadoDeLaSesion();
    this.configurarDataLista();
    this.configurarLineas();
    this.configurarDialogos();

    if (this.sesionIniciada) {
      this.verificarPerfilSeleccionado();
      this.configurarDataNotificacionesMensajes();
      this.configurarEscuchaNotificacionesMensajes();
      this.obtenerNotificaciones();
    } else {
      this.confAppBar.gazeAppBar.subtituloDemo =
        this.obtenerTituloPrincipal(false);
    }
  }

  ngOnDestroy(): void {
    this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
    this.notificacionesUsuario.desconectarDeEscuchaNotificaciones();
  }

  configurarEstadoDeLaSesion(): void {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
  }

  configurarDataLista(): void {
    this.dataLista = {
      cargando: false,
      reintentar: () => {},
      tamanoLista: TamanoLista.TIPO_MENU_PRINCIPAL,
    };
  }

  configurarPortada(): void {
    this.confPortada = {
      tamano: TamanoPortadaGaze.PORTADACOMPLETA,
      imagenAleatoriaRamdon: true,
    };
  }

  configurarLineas(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }

  configurarDialogo(): void {
    this.confDialogoLateral = {
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      completo: true,
      descripcion: 'm6v4texto11',
    };
  }

  verificarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();

    if (!this.perfilSeleccionado) {
      this._location.replaceState('/');
      this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      return;
    }

    this.confAppBar.gazeAppBar.subtituloNormal =
      this.obtenerTituloPrincipal(true);
    this.confAppBar.gazeAppBar.mostrarBotonXRoja = true;

    if (
      this.perfilSeleccionado &&
      this.perfilSeleccionado.tipoPerfil &&
      this.perfilSeleccionado.tipoPerfil.codigo &&
      this.perfilSeleccionado.tipoPerfil.codigo ===
        CodigosCatalogoTipoPerfil.GROUP
    ) {
      this.confAppBar.usoAppBar = UsoAppBar.USO_GAZE_BAR;
    }
  }

  async prepararAppBar(
    usoAppBar: UsoAppBar = UsoAppBar.USO_ELEGIR_PERFIL
  ): Promise<void> {
    this.confAppBar = {
      usoAppBar,
      accionAtras: () => {
        this._location.replaceState('/');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      },
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },

      gazeAppBar: {
        tituloPrincipal: {
          mostrar: true,
          llaveTexto: 'm2v11texto1',
        },
        textoElegirPerfil: {
          mostrar: true,
          llaveTexto: 'm2v11texto2',
        },

        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        clickTituloPrincipal: () => {
          this.enrutadorService.navegar(
            {
              componente: PerfilComponent,
              params: [
                {
                  nombre: 'id',
                  valor: this.perfilSeleccionado._id,
                },
              ],
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id',
              },
            }
          );
        },
        clickElegirOtroPerfil: () => {
          this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
        },
      },
      sinColorPrincipal: true,
      conColorSecundario: true,
    };
  }

  obtenerTituloPrincipal(profileCreated: boolean): {
    mostrar: boolean;
    llaveTexto: string;
  } {
    if (profileCreated) {
      return {
        mostrar: true,
        llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre,
      };
    } else {
      return {
        mostrar: true,
        llaveTexto: 'demo',
      };
    }
  }

  async prepararItemsMenu(): Promise<void> {
    this.listaMenu = [
      // Pensamientos
      {
        id: MenuPrincipal.MIS_PENSAMIENTOS,
        titulo: ['m2v11texto7', 'm2v11texto8', 'm2v11texto9'],
        tipo: TipoMenu.ACCION,
        ruta: PensamientoComponent,
      },
      // Contactos
      {
        id: MenuPrincipal.GAZING,
        titulo: ['m2v11texto10', 'm2v11texto11'],
        tipo: TipoMenu.ACCION,
        mostrarCorazon: false,
        ruta: ChatContactosComponent,
      },
      // Publicar proyectos y noticias
      {
        id: MenuPrincipal.PUBLICAR,
        titulo: ['m2v11texto12', 'm2v11texto13', 'm2v11texto14'],
        tipo: TipoMenu.ACCION,
        ruta: MenuPublicarProyectoNoticiaComponent,
      },
      // Proyectos de usuarios
      {
        id: MenuPrincipal.PROYECTOS,
        titulo: ['m2v11texto15', 'm2v11texto16'],
        tipo: TipoMenu.ACCION,
        ruta: MenuVerProyectosComponent,
      },
      // Noticias de usuarios
      {
        id: MenuPrincipal.NOTICIAS,
        titulo: ['m2v11texto17', 'm2v11texto18'],
        tipo: TipoMenu.ACCION,
        ruta: NoticiasUsuariosComponent,
      },
      // Compra o intercambio
      {
        id: MenuPrincipal.COMPRAS,
        titulo: ['m2v11texto19', 'm2v11texto19.1'],
        tipo: TipoMenu.ACCION,
        ruta: CompraIntermacioInicioComponent,
      },
      // Finanzas
      {
        id: MenuPrincipal.FINANZAS,
        titulo: ['m2v11texto21'],
        tipo: TipoMenu.ACCION,
        ruta: FinanzasComponent,
      },
      // Anuncios
      {
        id: MenuPrincipal.ANUNCIOS,
        titulo: ['m2v11texto22', 'm2v11texto23'],
        tipo: TipoMenu.ANUNCIOS,
        ruta: AnunciosSistemaComponent,
      },
    ];
  }

  configurarDialogos(): void {
    this.confDialogoConstruccion = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      descripcion: 'm7v4texto3',
    };
  }

  prepararItemMenu(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo:
        item.id === MenuPrincipal.ANUNCIOS
          ? ColorFondoItemMenu.TRANSPARENTE
          : ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo[0] ? item.titulo[0].toString() : null,
      texto2: item.titulo[1] ? item.titulo[1].toString() : null,
      texto3: item.titulo[2] ? item.titulo[2].toString() : null,
      tipoMenu: item.tipo,
      linea: {
        mostrar: true,
        configuracion: {
          ancho:
            item.id === MenuPrincipal.ANUNCIOS ||
            item.id === MenuPrincipal.FINANZAS
              ? AnchoLineaItem.ANCHO6028
              : AnchoLineaItem.ANCHO6382,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
          cajaGaze: item.id === MenuPrincipal.ANUNCIOS,
        },
      },
      gazeAnuncios: item.id === MenuPrincipal.ANUNCIOS,
      idInterno: item.id,
      onclick: () => this.navigationSubMenu(item),
      dobleClick: () => {},
    };
  }

  async prepararDataSubMenu3puntos(): Promise<void> {
    this.itemSubMenu3Puntos = {
      id: 'puntos',
      titulo: '',
      mostrarDescripcion: false,
      menusInternos: [
        {
          id: 'mya',
          titulo: ['m2v12texto1'],
          action: () => {
            this.enrutadorService.navegar(
              {
                componente: MiCuentaComponent,
                params: [],
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
              }
            );
          },
        },
        {
          id: 'iv',
          titulo: ['m2v12texto2'],
          subTitulo: ['m2v12texto3'],
          action: () => {
            this.enrutadorService.navegar(
              {
                componente: CuotaExtraComponent,
                params: [],
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
              }
            );
          },
        },
        {
          id: 'he',
          titulo: ['m2v12texto4'],
          action: () => {
            this.router.navigateByUrl(RutasLocales.LANDING);
          },
        },

        // {
        //   id: "faq",
        //   titulo: ["m2v12texto5"],
        //   action: () => { },
        // },
        // {
        //   id: "our",
        //   titulo: ["m2v12texto6"],
        //   action: () => {
        //     this.enrutadorService.navegar(
        //       {
        //         componente: NuestrasMetasComponent,
        //         params: []
        //       },
        //       {
        //         estado: true,
        //         posicicion: this.informacionEnrutador.posicion
        //       }
        //     )
        //   },
        // },
        // {
        // 	id: "web",
        // 	titulo: ["WebSite"],
        // 	action: () => { },
        // }
      ],
    };
  }

  navigationSubMenu(item: ItemMenuModel): void {
    if (!item || !item.ruta) {
      this.confDialogoLateral.mostrarDialogo = true;
      return;
    }
    const aux = item.ruta;
    const info: InformacionAEnrutar = {
      componente: item.ruta,
      params: [],
    };
    const estado: EstadoEnrutador = {
      estado: true,
      posicicion: this.informacionEnrutador.posicion,
    };

    if (item.id === MenuPrincipal.COMPRAS) {
      this.confDialogoConstruccion.mostrarDialogo = true;

      return;
    }

    if (
      item.id === MenuPrincipal.MIS_PENSAMIENTOS ||
      item.id === MenuPrincipal.GAZING ||
      item.id === MenuPrincipal.NOTICIAS ||
      item.id === MenuPrincipal.ANUNCIOS
    ) {
      estado.extras = { tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTE };
      this.enrutadorService.navegar(info, estado);
      return;
    }

    this.enrutadorService.navegar(info, estado);
  }

  prepararItemSubMenu(item: ItemSubMenu): ItemMenuCompartido {
    return {
      id: '',
      submenus: item.menusInternos ? item.menusInternos : [],
      mostrarDescripcion: item.mostrarDescripcion,
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo,
      tipoMenu: TipoMenu.SUBMENU,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => this.mostrarDescripcion(item),
      dobleClick: () => {},
    };
  }

  mostrarDescripcion(item: any): void {
    const elemento: HTMLElement = document.getElementById(
      'flecha' + item.id
    ) as HTMLElement;
    if (item.mostrarDescripcion) {
      item.mostrarDescripcion = false;
      elemento.classList.remove('rotar-flecha');
    } else {
      item.mostrarDescripcion = true;
      elemento.classList.add('rotar-flecha');
    }
  }

  obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
  }

  prepararDatosParaMenuMultipleAccion(): void {
    this.itemMenuMultipleAccion = {
      id: 'multip',
      titulo: [
        {
          nombre: 'm2v11texto24',
          codigo: 'g',
          accion: () => {
            this.enrutadorService.navegar(
              {
                componente: ContactoComponent,
                params: [],
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
              }
            );
          },
        },
        {
          nombre: 'm2v11texto25',
          codigo: 'm',
          accion: () => {
            const link = document.createElement('a');
            const nombre = 'politica-privacidad-gazelook';
            link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/terminos-condiciones-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
            link.download = nombre;
            link.target = '_blank';
            link.dispatchEvent(
              new MouseEvent('click', {
                view: window,
                bubbles: false,
                cancelable: true,
              })
            );
          },
        },
      ],
      tipo: TipoMenu.LEGAL,
    };
  }

  prepararItemsParaMenuMultipleAccion(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      mostrarDescripcion: false,
      tamano: TamanoItemMenu.ITEM_MENU_CONTENIDO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      acciones: item.titulo as ItemAccion[],
      tipoMenu: TipoMenu.LEGAL,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {},
      dobleClick: () => {},
    };
  }

  scroolEnContenedor(): void {
    const elemento = document.getElementById(
      'contenedorListaMenuPrincipal'
    ) as HTMLElement;
    if (!elemento) {
      return;
    }

    if (elemento.scrollTop < 30) {
      this.mostrarLinea = false;
      return;
    }

    if (elemento.scrollTop > 30) {
      this.mostrarLinea = true;
      return;
    }

    this.mostrarLinea = elemento.scrollTop >= 30;
  }

  configurarDataNotificacionesMensajes(): void {
    this.dataNotificacionesMensajes = {
      codigoEntidad: CodigosCatalogoEntidad.MENSAJE,
      idPropietario: this.perfilSeleccionado._id,
      nivel: CodigosCatalogoEntidad.PERFIL,
      leido: false,
      limite: 5,
    };
  }

  async obtenerNotificaciones(): Promise<void> {
    if (!this.sesionIniciada || !this.perfilSeleccionado) {
      this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
      this.notificacionesUsuario.desconectarDeEscuchaNotificaciones();
      return;
    }

    this.notificacionesUsuario.validarEstadoDeLaSesion(false);
    this.notificacionesPerfil.obtenerNotificaciones$.next(
      this.dataNotificacionesMensajes
    );
  }

  async configurarEscuchaNotificacionesMensajes(): Promise<void> {
    (this.notificacionesPerfil.subscripcionNotificaciones$ =
      this.notificacionesPerfil.notificaciones$.subscribe(
        (data) => {
          let contador = 0;
          data.forEach((item) => {
            // tslint:disable-next-line: no-string-literal
            if (!item['leido']) {
              contador += 1;
            }
          });
          const index = this.listaMenu.findIndex(
            (e) => e.id === MenuPrincipal.GAZING
          );
          this.listaMenu[index].mostrarCorazon = index >= 0 && contador > 0;
        }
        // tslint:disable-next-line: no-unused-expression
      )),
      (error: any) => {
        this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
      };
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(info: InformacionEnrutador): void {
    this.informacionEnrutador = info;
  }
}

export enum MenuPrincipal {
  MIS_PENSAMIENTOS,
  PUBLICAR,
  PROYECTOS,
  NOTICIAS,
  COMPRAS,
  FINANZAS,
  ANUNCIOS,
  GAZING,
}
