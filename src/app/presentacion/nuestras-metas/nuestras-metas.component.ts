import { Component, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos';
import { AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, UsoAppBar } from '@shared/diseno/enums';
import { ConfiguracionAppbarCompartida, ConfiguracionLineaVerde } from '@shared/diseno/modelos';
import { PerfilNegocio } from 'dominio/logica-negocio';
import { PerfilModel } from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../enrutador/enrutador.component';
import { MenuPrincipalComponent } from './../menu-principal/menu-principal.component';
@Component({
  selector: 'app-nuestras-metas',
  templateUrl: './nuestras-metas.component.html',
  styleUrls: ['./nuestras-metas.component.scss']
})
export class NuestrasMetasComponent implements OnInit, Enrutador {

  public perfilSeleccionado: PerfilModel
  public menuActivo: number
  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaCompleta: ConfiguracionLineaVerde
  public confLineaMitad: ConfiguracionLineaVerde

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador

  constructor(
    private perfilNegocio: PerfilNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private enrutadorService: EnrutadorService
  ) {
    this.menuActivo = -1
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado()
    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      this.configurarLineas()
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [
          ]
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      ),
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v13texto1'
        },
      }
    }
  }

  configurarLineas() {
    this.confLineaCompleta = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaMitad = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  clickEnMenu(menuClickeado: number) {
    if (this.menuActivo === menuClickeado) {
      this.menuActivo = -1
      return
    }

    this.menuActivo = menuClickeado
    return
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
}
