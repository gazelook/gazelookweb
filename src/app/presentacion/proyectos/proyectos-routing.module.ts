import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {RutasProyectos} from './rutas-proyectos.enum';

import {NuevosProyectosComponent} from './nuevos-proyectos/nuevos-proyectos.component';
import {MenuListaProyectosComponent} from './menu-lista-proyectos/menu-lista-proyectos.component';
import {MenuVerProyectosComponent} from './menu-ver-proyectos/menu-ver-proyectos.component';
import {InformacionUtilComponent} from './informacion-util/informacion-util.component';
import {ProyectosComponent} from './proyectos.component';
import {PublicarComponent} from './publicar/publicar.component';

import {ProyectosRecomendadosComponent} from 'presentacion/proyectos/proyectos-recomendados/proyectos-recomendados.component';
import {ProyectosForosComponent} from 'presentacion/proyectos/proyectos-foros/proyectos-foros.component';
import {ProyectosSeleccionadosComponent} from 'presentacion/proyectos/proyectos-seleccionados/proyectos-seleccionados.component';

const routes: Routes = [
  {
    path: '',
    component: ProyectosComponent,
    children: [
      {
        path: RutasProyectos.PUBLICAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.ACTUALIZAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.VISITAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.INFORMACION_UTIL.toString(),
        component: InformacionUtilComponent
      },
      {
        path: RutasProyectos.MENU_SELECCIONAR_TIPO_PROYECTOS.toString(),
        component: MenuVerProyectosComponent
      },
      {
        path: RutasProyectos.MENU_LISTA_PROYECTOS.toString(),
        component: MenuListaProyectosComponent
      },
      {
        path: RutasProyectos.LISTA_NUEVOS_PROYECTOS.toString(),
        component: NuevosProyectosComponent
      },
      {
        path: RutasProyectos.LISTA_PROYECTOS_RECOMENDADOS.toString(),
        component: ProyectosRecomendadosComponent
      },
      {
        path: RutasProyectos.LISTA_PROYECTOS_FOROS.toString(),
        component: ProyectosForosComponent
      }, {
        path: RutasProyectos.LISTA_PROYECTOS_SELECCIONADOS.toString(),
        component: ProyectosSeleccionadosComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectosRoutingModule {

}
