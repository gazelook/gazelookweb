import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos';
import { TamanoColorDeFondo, UsoAppBar } from '@shared/diseno/enums';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos';
import { CuentaNegocio, PerfilNegocio } from 'dominio/logica-negocio';
import { PerfilModel } from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { MenuPrincipalComponent } from './../../menu-principal/menu-principal.component';
@Component({
  selector: 'app-informacion-util',
  templateUrl: './informacion-util.component.html',
  styleUrls: ['./informacion-util.component.scss']
})
export class InformacionUtilComponent implements OnInit, Enrutador {

  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel

  public confAppBar: ConfiguracionAppbarCompartida

  public informacionEnrutador: InformacionEnrutador

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private enrutadorService: EnrutadorService
  ) {

  }

  ngOnInit(): void {
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()

    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      return
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
  }

  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [
          ]
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v14texto1'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }
}
