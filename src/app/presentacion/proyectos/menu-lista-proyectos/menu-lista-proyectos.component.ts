import { Component, OnInit, ViewChild } from '@angular/core';

import {
  Enrutador,
  InformacionEnrutador,
  TipoDeNavegacion,
} from '../../enrutador/enrutador.component';

import { ProyectosSeleccionadosComponent } from './../proyectos-seleccionados/proyectos-seleccionados.component';
import { ProyectosForosComponent } from './../proyectos-foros/proyectos-foros.component';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { VariablesGlobales } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoPerfil,
  CodigosCatalogoTipoProyecto,
} from '@core/servicios/remotos/codigos-catalogos';
import { ContadorDiasComponent, ToastComponent } from '@shared/componentes';
import {
  AnchoLineaItem,
  ColorDelTexto,
  ColorFondoLinea,
  EspesorLineaItem,
  EstilosDelTexto,
  PaddingIzqDerDelTexto,
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  TipoDialogo,
  UsoAppBar,
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida,
  ConfiguracionContadorDias,
  ConfiguracionDialogoInline,
  ConfiguracionToast,
  DialogoCompartido,
  LineaCompartida,
  ModoBusqueda,
} from '@shared/diseno/modelos';
import {
  CuentaNegocio,
  PerfilNegocio,
  ProyectoNegocio,
} from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import { PerfilModel } from 'dominio/modelo/entidades';
import { ProyectoParams } from 'dominio/modelo/parametros';

import { NuevosProyectosComponent } from '../nuevos-proyectos/nuevos-proyectos.component';
import { MenuPrincipalComponent } from './../../menu-principal/menu-principal.component';
import { ProyectosRecomendadosComponent } from 'presentacion/proyectos/proyectos-recomendados/proyectos-recomendados.component';
import { ProyectosEsperaFondosComponent } from 'presentacion/proyectos/proyectos-espera-fondos/proyectos-espera-fondos.component';

@Component({
  selector: 'app-menu-lista-proyectos',
  templateUrl: './menu-lista-proyectos.component.html',
  styleUrls: ['./menu-lista-proyectos.component.scss'],
})
export class MenuListaProyectosComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @ViewChild('contadorInicioForo', { static: false })
  contadorInicioForo: ContadorDiasComponent;
  @ViewChild('contadorFinalForo', { static: false })
  contadorFinalForo: ContadorDiasComponent;

  // Utils
  public MenuListaProyectosEnum = MenuListaProyectos;

  // Parametros internos
  public sesionIniciada: boolean;
  public perfilSeleccionado: PerfilModel;
  public diaEnSegundos: number;
  public horaEnSegundos: number;
  public minutoEnSegundos: number;
  public params: ProyectoParams;
  public listaProyectos: PaginacionModel<string>;
  public puedeCargarMas: boolean;

  // Configuracion hijos
  public confAppBar: ConfiguracionAppbarCompartida;
  public confLineaVerde: LineaCompartida;
  public confLineaVerdeAll: LineaCompartida;
  public confContadorDiasForoInicio: ConfiguracionContadorDias;
  public confContadorDiasForoFinal: ConfiguracionContadorDias;
  public confToast: ConfiguracionToast;
  public confDialogoProyectosEnEsperaDeFondos: ConfiguracionDialogoInline;
  public confDialogoProyectosEnEstrategia: ConfiguracionDialogoInline;
  public confDialogoProyectosEnEjecucion: ConfiguracionDialogoInline;
  public confDialogoProyectosEjecutados: ConfiguracionDialogoInline;
  public confDialogoProyectosMenosVotados: ConfiguracionDialogoInline;
  public confDialogoProyectosRecomendados: ConfiguracionDialogoInline;
  public confDialogoTiempoForoAunNoInicia: ConfiguracionDialogoInline;
  public confDialogoTiempoForoAunNoInicia2: ConfiguracionDialogoInline;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private variablesGlobales: VariablesGlobales,
    private enrutadorService: EnrutadorService
  ) {
    this.params = { estado: false };
    this.diaEnSegundos = 86400;
    this.horaEnSegundos = 3600;
    this.minutoEnSegundos = 60;
    this.puedeCargarMas = true;
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true;
    this.configurarEstadoSesion();
    this.configurarPerfilSeleccionado();
    this.configurarParametros();

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarLinea();
      this.configurarToast();
      this.configurarContadoresDias();
      this.configurarDialogos();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  configurarEstadoSesion(): void {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
  }

  configurarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros(): void {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }

    this.informacionEnrutador.params.forEach((item) => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }
    });
  }

  configurarAppBar(): void {
    let llaverTextoTipoProyecto: string;
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_LOCAL
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.2';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_PAIS
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.1';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_RED
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.3';
    }

    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () =>
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [],
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            },
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: llaverTextoTipoProyecto,
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v1texto1',
            valorBusqueda: '',
            posicion: this.informacionEnrutador.posicion,
          },
        },
      },
    };
  }

  configurarLinea(): void {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }

  configurarToast(): void {
    this.confToast = {
      mostrarToast: false, // True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  configurarContadoresDias(): void {
    this.confContadorDiasForoInicio = this.configurarContadorDias(
      false,
      'texto836'
    );
    this.confContadorDiasForoFinal = this.configurarContadorDias(
      false,
      'texto842'
    );
  }

  configurarContadorDias(
    mostrar: boolean = false,
    titulo: string = '',
    fechaInicial: Date = new Date(),
    fechaFinal: Date = new Date()
  ): ConfiguracionContadorDias {
    return {
      mostrar,
      mostrarLoader: true,
      titulo,
      fechaInicial,
      fechaFinal,
    };
  }

  configurarDialogos(): void {
    // ProyectosRecomendanos
    this.confDialogoProyectosRecomendados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };
    this.confDialogoProyectosRecomendados.descripcion.push(
      {
        texto: 'm5v2texto13.1',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto13.2',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );
    // Proyectos seleccionados
    this.confDialogoProyectosEnEsperaDeFondos = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEsperaDeFondos.descripcion.push(
      {
        texto: 'm5v2texto14',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto15',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos en estrategia
    this.confDialogoProyectosEnEstrategia = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEstrategia.descripcion.push(
      {
        texto: 'm5v2texto16',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto17',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos en ejecucion
    this.confDialogoProyectosEnEjecucion = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEjecucion.descripcion.push(
      {
        texto: 'm5v2texto18',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto19',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos ejecutados
    this.confDialogoProyectosEjecutados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEjecutados.descripcion.push(
      {
        texto: 'm5v2texto20',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto21',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // PROYECTOS MENOS VOTADOS
    this.confDialogoProyectosMenosVotados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosMenosVotados.descripcion.push(
      {
        texto: 'm5v2texto18',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto19',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // TIEMPO FORO AUN NO INICIA
    this.confDialogoTiempoForoAunNoInicia = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoTiempoForoAunNoInicia.descripcion.push({
      texto: 'm5v2texto13.3',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });

    // TIEMPO FORO AUN NO INICIA2
    this.confDialogoTiempoForoAunNoInicia2 = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoTiempoForoAunNoInicia2.descripcion.push({
      texto: 'm5v2texto13.4',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });
  }

  inicializarDataListaProyectos(): void {
    this.listaProyectos = {
      anteriorPagina: false,
      paginaActual: 1,
      proximaPagina: true,
      totalDatos: 0,
      totalPaginas: 0,
      lista: [],
    };
  }

  navegarSegunMenu(menuTipo: MenuListaProyectos): void {
    switch (menuTipo) {
      case MenuListaProyectos.NUEVOS_PROYECTOS:
        this.verNuevosProyectos();
        break;
      case MenuListaProyectos.PROYECTOS_RECOMENDADOS_POR_ADMIN:
        this.mostrarProyectosRecomendados();
        this.confDialogoProyectosRecomendados.noMostrar = true;
        break;
      case MenuListaProyectos.FORO_DE_LOS_PROYECTOS:
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos();
        this.mostrarProyectosForos();
        break;
      case MenuListaProyectos.PROYECTOS_SELECCIONADOS:
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos(false);
        this.mostrarProyectosSeleccionados();
        break;
      case MenuListaProyectos.PROYECTOS_EN_ESPERA_DE_FONDOS:
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos(false);
        this.mostrarProyectosEsperaFondos();
        break;
      case MenuListaProyectos.PROYECTOS_EN_ESTRATEGIA:
        this.reiniciarDialogos();
        this.confDialogoProyectosEnEstrategia.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_EN_EJECUCION:
        this.reiniciarDialogos();
        this.confDialogoProyectosEnEjecucion.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_EJECUTADOS:
        this.reiniciarDialogos();
        this.confDialogoProyectosEjecutados.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_MENOS_VOTADOS:
        // this.reiniciarDialogos()
        this.verMenosVotadoProyectos();
        this.confDialogoProyectosMenosVotados.noMostrar = false;
        break;
      default:
        break;
    }
  }

  async abrirContadorParaForos(inicio: boolean = true): Promise<void> {
    try {
      const fechaString = await this.proyectoNegocio
        .obtenerFechaForo(inicio)
        .toPromise();

      if (fechaString) {
        const date: Date = new Date(fechaString.fecha);
        let fechaForo = new Date(date);
        let fechaActual = new Date();

        if (inicio) {
          if (fechaForo > fechaActual) {
            this.confDialogoTiempoForoAunNoInicia.noMostrar = false;

            this.confContadorDiasForoInicio = this.configurarContadorDias(
              true,
              'm5v2texto2',
              new Date(),
              date
            );
            this.confContadorDiasForoInicio = this.calcularTiempo(
              this.confContadorDiasForoInicio
            );
            this.confContadorDiasForoInicio.mostrarLoader = false;
            return;
          }
          if (fechaForo <= fechaActual) {
            console.log('aca');

            this.mostrarProyectosForos();
            return;
          }
        }

        if (!inicio) {
          if (fechaForo > fechaActual) {
            this.confContadorDiasForoFinal = this.configurarContadorDias(
              true,
              'm5v2texto8',
              new Date(),
              date
            );
            this.confContadorDiasForoFinal = this.calcularTiempo(
              this.confContadorDiasForoFinal
            );
            this.confContadorDiasForoFinal.mostrarLoader = false;
            this.confDialogoProyectosEnEsperaDeFondos.noMostrar = false;
            return;
          }

          if (fechaForo <= fechaActual) {
            this.mostrarProyectosSeleccionados();
            this.mostrarProyectosEsperaFondos();
            return;
          }
        }
      }
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  calcularTiempo(
    configuracion: ConfiguracionContadorDias
  ): ConfiguracionContadorDias {
    if (
      configuracion.fechaFinal.getTime() < configuracion.fechaInicial.getTime()
    ) {
      configuracion.dias = 0;
      configuracion.horas = 0;
      configuracion.minutos = 0;
      configuracion.segundos = 0;
      return configuracion;
    }

    let totalSegundos =
      Math.abs(
        configuracion.fechaFinal.valueOf() -
          configuracion.fechaInicial.valueOf()
      ) / 1000;
    // Total de dias
    const dias = Math.floor(totalSegundos / this.diaEnSegundos);
    totalSegundos -= dias * this.diaEnSegundos;

    // Total de horas
    const horas = Math.floor(totalSegundos / this.horaEnSegundos) % 24;
    totalSegundos -= horas * this.horaEnSegundos;

    // Total de minutos
    const minutos = Math.floor(totalSegundos / this.minutoEnSegundos) % 60;
    totalSegundos -= minutos * this.minutoEnSegundos;

    // Total de segundos
    configuracion.dias = dias;
    configuracion.horas = horas;
    configuracion.minutos = minutos;
    configuracion.segundos = Math.round(totalSegundos);
    return configuracion;
  }

  verNuevosProyectos(): void {
    this.enrutadorService.navegar(
      {
        componente: NuevosProyectosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
          {
            nombre: 'menosVotado',
            valor: 'false',
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto',
        },
      }
    );
  }

  mostrarProyectosRecomendados(): void {
    this.enrutadorService.navegar(
      {
        componente: ProyectosRecomendadosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto',
        },
      }
    );
  }

  mostrarProyectosForos(): void {
    this.enrutadorService.navegar(
      {
        componente: ProyectosForosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto',
        },
      }
    );
  }

  mostrarProyectosSeleccionados(): void {
    this.enrutadorService.navegar(
      {
        componente: ProyectosSeleccionadosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto',
        },
      }
    );
  }

  mostrarProyectosEsperaFondos(): void {
    this.enrutadorService.navegar(
      {
        componente: ProyectosEsperaFondosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto',
        },
      }
    );
  }

  verMenosVotadoProyectos(): void {
    this.enrutadorService.navegar(
      {
        componente: NuevosProyectosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: this.params.codigoTipoProyecto,
          },
          {
            nombre: 'menosVotado',
            valor: 'true',
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.NORMAL,
          paramAValidar: 'menosVotado',
        },
      }
    );
  }

  reiniciarDialogos(): void {
    this.confContadorDiasForoFinal = this.configurarContadorDias(false);
    this.confContadorDiasForoInicio = this.configurarContadorDias(false);
    this.confDialogoProyectosEjecutados.noMostrar = true;
    this.confDialogoProyectosEnEjecucion.noMostrar = true;
    this.confDialogoProyectosEnEsperaDeFondos.noMostrar = true;
    this.confDialogoProyectosEnEstrategia.noMostrar = true;
    this.confDialogoProyectosMenosVotados.noMostrar = true;
    this.confDialogoProyectosRecomendados.noMostrar = true;
    this.confDialogoTiempoForoAunNoInicia.noMostrar = true;
    this.confDialogoTiempoForoAunNoInicia2.noMostrar = true;
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }
}

export enum MenuListaProyectos {
  NUEVOS_PROYECTOS = '0',
  PROYECTOS_RECOMENDADOS_POR_ADMIN = '1',
  FORO_DE_LOS_PROYECTOS = '2',
  PROYECTOS_SELECCIONADOS = '3',
  PROYECTOS_EN_ESPERA_DE_FONDOS = '4',
  PROYECTOS_EN_ESTRATEGIA = '5',
  PROYECTOS_EN_EJECUCION = '6',
  PROYECTOS_EJECUTADOS = '7',
  PROYECTOS_MENOS_VOTADOS = '8',
}
