import {Component, OnInit} from '@angular/core';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {EnrutadorService} from '@core/servicios/generales/enrutador';
import {CodigosCatalogoEntidad, CodigosCatalogoTipoPerfil, CodigosCatalogoTipoProyecto} from '@core/servicios/remotos/codigos-catalogos';
import {AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, UsoAppBar} from '@shared/diseno/enums';
import {ConfiguracionAppbarCompartida, LineaCompartida, ModoBusqueda} from '@shared/diseno/modelos';
import {CuentaNegocio, PerfilNegocio} from 'dominio/logica-negocio';
import {PaginacionModel} from 'dominio/modelo';
import {PerfilModel} from 'dominio/modelo/entidades';
import {Enrutador, InformacionEnrutador, TipoDeNavegacion} from '../../enrutador/enrutador.component';
import {InformacionUtilComponent} from '../informacion-util/informacion-util.component';
import {MenuListaProyectosComponent} from '../menu-lista-proyectos/menu-lista-proyectos.component';
import {VariablesGlobales} from '@core/servicios/generales';
import {MenuPrincipalComponent} from './../../menu-principal/menu-principal.component';

@Component({
  selector: 'app-menu-ver-proyectos',
  templateUrl: './menu-ver-proyectos.component.html',
  styleUrls: ['./menu-ver-proyectos.component.scss']
})
export class MenuVerProyectosComponent implements OnInit, Enrutador {

  public sesionIniciada: boolean;
  public perfilSeleccionado: PerfilModel;

  public listaProyectos: PaginacionModel<string>;
  public puedeCargarMas: boolean;

  public confAppBar: ConfiguracionAppbarCompartida;
  public confLineaVerde: LineaCompartida;
  public confLineaVerdeAll: LineaCompartida;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private enrutadorService: EnrutadorService,
    private variablesGlobales: VariablesGlobales,
  ) {
    this.puedeCargarMas = true;
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true;
    this.configurarEstadoSesion();
    this.configurarPerfilSeleccionado();

    if (this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarLinea();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  configurarEstadoSesion(): void {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
  }

  configurarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarAppBar(): void {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion);
      },
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: []
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v13texto2'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v1texto1',
            valorBusqueda: '',
            posicion: this.informacionEnrutador.posicion
          }
        }
      },
    };
  }

  configurarLinea(): void {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }

  irAVerProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto): void {
    this.enrutadorService.navegar(
      {
        componente: MenuListaProyectosComponent,
        params: [
          {
            nombre: 'codigoTipoProyecto',
            valor: codigo.toString()
          }
        ]
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'codigoTipoProyecto'
        }
      }
    );
  }

  irAInformacionUtilDeProyectos(): void {
    this.enrutadorService.navegar(
      {
        componente: InformacionUtilComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion
      }
    );
  }

  navegarSegunMenu(menu: number): void {
    switch (menu) {
      case 0:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL);
        break;
      case 1:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS);
        break;
      case 2:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_LOCAL);
        break;
      case 3:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_RED);
        break;
      case 4:
        this.irAInformacionUtilDeProyectos();
        break;
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }

}
