import {Component, OnInit} from '@angular/core';
import {
  BotonCompartido,
  ConfiguracionAppbarCompartida,
  CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida, ModoBusqueda
} from '@shared/diseno/modelos';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {InformacionEnrutador, TipoDeNavegacion} from 'presentacion/enrutador/enrutador.component';
import {AlbumModel, PerfilModel, ProyectoModel} from 'dominio/modelo/entidades';
import {AlbumNegocio, PerfilNegocio, ProyectoNegocio} from 'dominio/logica-negocio';
import {EnrutadorService, NoticiaService, VariablesGlobales} from '@core/servicios/generales';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {
  AnchoLineaItem,
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo,
  ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  UsoAppBar,
  UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {MenuPrincipalComponent} from 'presentacion/menu-principal/menu-principal.component';
import {
  AccionEntidad,
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil
} from '@core/servicios/remotos/codigos-catalogos';
import {PaginacionModel} from 'dominio/modelo';
import {ColorTextoBoton, TipoBoton} from '@shared/componentes';
import {PublicarComponent} from 'presentacion/proyectos/publicar/publicar.component';


@Component({
  selector: 'app-proyectos-foros',
  templateUrl: './proyectos-foros.component.html',
  styleUrls: ['./proyectos-foros.component.scss']
})
export class ProyectosForosComponent implements OnInit {
  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  public perfilSeleccionado: PerfilModel;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;

  public params: ProyectoParams;
  public idCapaCuerpo: string;

  public listaProyectosForos: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosForosModel: ProyectoModel[] = [];

  private limite = 12;
  public configBotonProyectos: BotonCompartido;

  constructor(
    private albumNegocio: AlbumNegocio,
    private enrutadorService: EnrutadorService,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private proyectoNegocio: ProyectoNegocio,
    private perfilNegocio: PerfilNegocio,
    private variablesGlobales: VariablesGlobales
  ) {

    this.variablesGlobales.mostrarMundo = true;

    this.params = {estado: false};
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';


    this.idCapaCuerpo = 'capa-cuerpo-noticias';
  }

  ngOnInit(): void {
    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    if (this.params.estado && this.perfilSeleccionado) {
      this.inicializarListaProyectos();
      this.configurarAppBar();
      this.configurarBoton();
      this.configurarLineas();
      this.obtenerProyectosForos();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  obtenerProyectosForos(): void {

    if (!this.listaProyectosForos.proximaPagina) {
      this.puedeCargarMas = true;
      console.log('No hay mas paginas');
      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = (this.listaProyectosForos.lista.length === 0);

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;
    this.configBotonProyectos.enProgreso = true;

    this.proyectoNegocio.obtenerProyectosForos(
      id,
      this.limite,
      this.listaProyectosForos.paginaActual,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosForos.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosForosModel = respuesta.lista;
          this.listaProyectosForosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosForos.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosForos.paginaActual += 1;
        },
        (error) => {
          console.log(error);
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
    this.configBotonProyectos.enProgreso = false;
  }

  actualizarProyectosForos(): void {
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = true;
    this.listaProyectosForos.lista = [];
    this.listaProyectosForosModel = [];

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;
    this.configBotonProyectos.enProgreso = true;

    this.proyectoNegocio.obtenerProyectosForos(
      id,
      this.limite,
      this.listaProyectosForos.paginaActual = 1,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosForos.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosForosModel = respuesta.lista;
          this.listaProyectosForosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosForos.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosForos.paginaActual += 1;
        },
        (error) => {
          console.log(error);
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
    this.configBotonProyectos.enProgreso = false;
  }

  configurarItemProyectos(proyecto: ProyectoModel, contador: number): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(data.id);
          const accionEntidad = (proyectoLista) && this.perfilSeleccionado._id === proyectoLista.perfil._id
            ? AccionEntidad.ACTUALIZAR
            : AccionEntidad.VISITAR;
          this.enrutadorService.navegar(
            {
              componente: PublicarComponent,
              params: [
                {
                  nombre: 'id',
                  valor: data.id
                },
                {
                  nombre: 'accionEntidad',
                  valor: accionEntidad
                },
              ]
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id'
              }
            }
          );
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosForosModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectosForosModel[index];
    }

    return null;
  }

  configurarBoton(): void {
    this.configBotonProyectos = {
      text: 'm5v5texto4',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.VERDE,
      enProgreso: false,
      ejecutar: () => this.actualizarProyectosForos(),
    };
  }

  scroolEnCapaCuerpo(): void {
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;
      this.obtenerProyectosForos();
    }
  }

  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  inicializarListaProyectos(): void {
    this.listaProyectosForosModel = [];
    this.listaProyectosForos = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion);
      },
      accionAtras: () => this.accionAtras(),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: []
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            }
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v15texto8'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
          }
        }
      },
    };
  }

  configurarLineas(): void {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  accionAtras(): void {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

// Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }

  configurarParametros(): void {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }
    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }
    });
  }

}
