import { Component, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import {
  EnrutadorService,
  NoticiaService,
  VariablesGlobales,
} from '@core/servicios/generales';
import {
  AccionEntidad,
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil,
} from '@core/servicios/remotos/codigos-catalogos';
import {
  AnchoLineaItem,
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo,
  ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  UsoAppBar,
  UsoItemProyectoNoticia,
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida,
  CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida,
  ModoBusqueda,
} from '@shared/diseno/modelos';
import {
  AlbumNegocio,
  PerfilNegocio,
  ProyectoNegocio,
} from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import {
  AlbumModel,
  PerfilModel,
  ProyectoModel,
} from 'dominio/modelo/entidades';
import { ProyectoParams } from 'dominio/modelo/parametros';
import {
  InformacionEnrutador,
  TipoDeNavegacion,
} from 'presentacion/enrutador/enrutador.component';
import { PublicarComponent } from 'presentacion/proyectos/publicar/publicar.component';

@Component({
  selector: 'app-proyectos-seleccionados',
  templateUrl: './proyectos-seleccionados.component.html',
  styleUrls: ['./proyectos-seleccionados.component.scss'],
})
export class ProyectosSeleccionadosComponent implements OnInit {
  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public perfilSeleccionado: PerfilModel;

  public listaProyectosSeleccionados: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosSeleccionadosModel: ProyectoModel[] = [];

  public params: ProyectoParams;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaVerde: LineaCompartida;
  public idCapaCuerpo: string;

  public fecha: Date;
  public fechaFiltro: string;

  private limite = 12;

  constructor(
    private albumNegocio: AlbumNegocio,
    private enrutadorService: EnrutadorService,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private proyectoNegocio: ProyectoNegocio,
    private perfilNegocio: PerfilNegocio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.variablesGlobales.mostrarMundo = true;
    this.params = { estado: false };
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';

    this.idCapaCuerpo = 'capa-cuerpo-noticias';
  }

  ngOnInit(): void {
    console.log('ESTOY  ACA');

    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    this.fecha = new Date();

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarLineas();
      this.inicializarListaProyectos();
      this.obtenerProyectosSeleccionados();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  obtenerProyectosSeleccionados(): void {
    if (!this.listaProyectosSeleccionados.proximaPagina) {
      this.puedeCargarMas = true;
      console.log('No hay mas paginas');
      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader =
      this.listaProyectosSeleccionados.lista.length === 0;

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio
      .obtenerProyectosSeleccionados(
        id,
        this.limite,
        this.listaProyectosSeleccionados.paginaActual,
        tipo
      )
      .subscribe(
        (respuesta) => {
          this.listaProyectosSeleccionados.proximaPagina =
            respuesta.proximaPagina;
          this.listaProyectosSeleccionadosModel = respuesta.lista;
          this.listaProyectosSeleccionadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosSeleccionados.lista.push(
              this.configurarItemProyectos(proyecto, contador)
            );
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosSeleccionados.paginaActual += 1;
        },
        (error) => {
          console.log(error);
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }

  obtenerProyectosSeleccionadosFecha(): void {
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = true;
    this.listaProyectosSeleccionados.lista = [];
    this.listaProyectosSeleccionadosModel = [];
    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio
      .obtenerProyectosSeleccionados(
        id,
        this.limite,
        (this.listaProyectosSeleccionados.paginaActual = 1),
        tipo,
        this.fechaFiltro
      )
      .subscribe(
        (respuesta) => {
          this.listaProyectosSeleccionados.proximaPagina =
            respuesta.proximaPagina;
          this.listaProyectosSeleccionadosModel = respuesta.lista;
          this.listaProyectosSeleccionadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosSeleccionados.lista.push(
              this.configurarItemProyectos(proyecto, contador)
            );
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosSeleccionados.paginaActual += 1;
        },
        (error) => {
          console.log(error);
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }

  configurarItemProyectos(
    proyecto: ProyectoModel,
    contador: number
  ): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy',
        },
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      urlMedia:
        album && album.portada
          ? album.portada.principal.url || album.portada.miniatura.url || ''
          : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        ((album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url))
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(
            data.id
          );
          const accionEntidad =
            proyectoLista &&
            this.perfilSeleccionado._id === proyectoLista.perfil._id
              ? AccionEntidad.ACTUALIZAR
              : AccionEntidad.VISITAR;
          this.enrutadorService.navegar(
            {
              componente: PublicarComponent,
              params: [
                {
                  nombre: 'id',
                  valor: data.id,
                },
                {
                  nombre: 'accionEntidad',
                  valor: accionEntidad,
                },
              ],
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id',
              },
            }
          );
        },
      },
      eventoDobleTap: {
        activo: false,
      },
      eventoPress: {
        activo: false,
      },
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosSeleccionadosModel.findIndex(
      (e) => e.id === id
    );

    if (index >= 0) {
      return this.listaProyectosSeleccionadosModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo(): void {
    console.log('scroolEnCapaCuerpo');
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(
      this.idCapaCuerpo
    ) as HTMLElement;
    if (
      elemento.offsetHeight + elemento.scrollTop >=
      elemento.scrollHeight - 5.22
    ) {
      this.puedeCargarMas = false;
      this.obtenerProyectosSeleccionados();
    }
  }

  inicializarListaProyectos(): void {
    this.listaProyectosSeleccionadosModel = [];
    this.listaProyectosSeleccionados = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  cambioDeFecha(): void {
    this.fechaFiltro = new Date(this.fecha).toISOString().substring(0, 10);
    this.obtenerProyectosSeleccionadosFecha();
  }

  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v15texto11',
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
          },
        },
      },
    };
  }

  accionAtras(): void {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }

  configurarParametros(): void {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }
    this.informacionEnrutador.params.forEach((item) => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }
    });
  }

  configurarLineas(): void {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6028,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }
}
