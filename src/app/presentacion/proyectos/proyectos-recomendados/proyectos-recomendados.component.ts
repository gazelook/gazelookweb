import {Component, OnInit} from '@angular/core';
import {
  ConfiguracionAppbarCompartida, CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida,
} from '@shared/diseno/modelos';
import {
  AnchoLineaItem,
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo,
  ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  UsoAppBar, UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {
  AccionEntidad,
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil,
} from '@core/servicios/remotos/codigos-catalogos';
import {AlbumNegocio, PerfilNegocio, ProyectoNegocio} from 'dominio/logica-negocio';
import {AlbumModel, PerfilModel, ProyectoModel} from 'dominio/modelo/entidades';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {InformacionEnrutador, TipoDeNavegacion} from 'presentacion/enrutador/enrutador.component';
import {MenuPrincipalComponent} from 'presentacion/menu-principal/menu-principal.component';
import {EnrutadorService, NoticiaService} from '@core/servicios/generales';
import {PaginacionModel} from 'dominio/modelo';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {PublicarComponent} from 'presentacion/proyectos/publicar/publicar.component';


@Component({
  selector: 'app-proyectos-recomendados',
  templateUrl: './proyectos-recomendados.component.html',
  styleUrls: ['./proyectos-recomendados.component.scss']
})
export class ProyectosRecomendadosComponent implements OnInit {

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public perfilSeleccionado: PerfilModel;

  public listaProyectosRecomendados: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosRecomendadosModel: ProyectoModel[] = [];


  public params: ProyectoParams;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;
  public idCapaCuerpo: string;


  private liminte = 12;

  constructor(
    private albumNegocio: AlbumNegocio,
    private enrutadorService: EnrutadorService,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private proyectoNegocio: ProyectoNegocio,
    private perfilNegocio: PerfilNegocio,
  ) {
    this.params = {estado: false};
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.idCapaCuerpo = 'capa-cuerpo-noticias';

  }

  ngOnInit(): void {
    this.inicializarPerfilSeleccionado();
    this.configurarParametros();

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarLineas();
      this.inicializarListaProyectos();
      this.obtenerProyectosRecomendados();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  obtenerProyectosRecomendados(): void {

    if (!this.listaProyectosRecomendados.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = (this.listaProyectosRecomendados.lista.length === 0);


    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio.obtenerProyectosRecomendados(
      id,
      this.liminte,
      this.listaProyectosRecomendados.paginaActual,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosRecomendados.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosRecomendados.paginaActual += 1;
          this.listaProyectosRecomendadosModel = respuesta.lista;
          this.listaProyectosRecomendadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosRecomendados.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
        },
        (error) => {
          console.log(error);
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }


  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros(): void {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }

    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }

    });


  }


  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion);
      },
      accionAtras: () => this.accionAtras(),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: []
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            }
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm5v4texto2'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    };
  }

  configurarLineas(): void {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  inicializarListaProyectos(): void {
    this.listaProyectosRecomendadosModel = [];
    this.listaProyectosRecomendados = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  accionAtras(): void {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }


  configurarItemProyectos(proyecto: ProyectoModel, contador: number): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(data.id);
          const accionEntidad = (proyectoLista) && this.perfilSeleccionado._id === proyectoLista.perfil._id
            ? AccionEntidad.ACTUALIZAR
            : AccionEntidad.VISITAR;
          this.enrutadorService.navegar(
            {
              componente: PublicarComponent,
              params: [
                {
                  nombre: 'id',
                  valor: data.id
                },
                {
                  nombre: 'accionEntidad',
                  valor: accionEntidad
                },
              ]
            },
            {
              estado: true,
              posicicion: this.informacionEnrutador.posicion,
              extras: {
                tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                paramAValidar: 'id'
              }
            }
          );
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    };
  }


  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosRecomendadosModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectosRecomendadosModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo(): void {
    console.log('scroolEnCapaCuerpo');
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;
      this.obtenerProyectosRecomendados();
    }
  }


  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }


}
