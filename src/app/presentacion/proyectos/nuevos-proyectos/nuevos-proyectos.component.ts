import { MenuPrincipalComponent } from './../../menu-principal/menu-principal.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { NoticiaService } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
  AccionEntidad, CodigosCatalogoEntidad, CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil, CodigosCatalogoTipoProyecto, CodigosCatatalogosEstadoProyecto
} from '@core/servicios/remotos/codigos-catalogos';
import { FiltroBusquedaProyectos } from '@core/servicios/remotos/filtro-busqueda';
import { ToastComponent } from '@shared/componentes';
import {
  AnchoLineaItem, ColorDeBorde, ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem, TamanoColorDeFondo, UsoAppBar, UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida, ConfiguracionToast,
  CongifuracionItemProyectosNoticias, LineaCompartida, ModoBusqueda
} from '@shared/diseno/modelos';
import { AlbumNegocio, PerfilNegocio, ProyectoNegocio } from 'dominio/logica-negocio';
import { PaginacionModel } from 'dominio/modelo';
import { AlbumModel, NoticiaModel, PerfilModel, ProyectoModel } from 'dominio/modelo/entidades';
import { ProyectoParams } from 'dominio/modelo/parametros';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../../enrutador/enrutador.component';
import { PublicarComponent } from '../publicar/publicar.component';
@Component({
  selector: 'app-nuevos-proyectos',
  templateUrl: './nuevos-proyectos.component.html',
  styleUrls: ['./nuevos-proyectos.component.scss']
})
export class NuevosProyectosComponent implements OnInit, Enrutador {

  @ViewChild('toast', { static: false }) toast: ToastComponent;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;
  public puedeCargarMas: boolean;

  // Parametros internos
  public fechaInicial: Date;
  public fechaFinal: Date;
  public filtroActivoDeBusqueda: FiltroBusqueda;
  public perfilSeleccionado: PerfilModel;
  public idCapaCuerpo: string;
  public params: ProyectoParams;
  public currentDay: string;
  public menosVotados: boolean;
  public filtroGeneral: FiltroBusquedaProyectos;
  public listaProyectoModel: Array<NoticiaModel>;
  public listaProyectoModelFiltroOtro: Array<NoticiaModel>;

  // Configuracion hijos
  public confToast: ConfiguracionToast;
  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;

  public listaProyectos: PaginacionModel<CongifuracionItemProyectosNoticias>;
  public confResultadoBusqueda: Array<CongifuracionItemProyectosNoticias>;

  // Enrutamiento
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private proyectoNegocio: ProyectoNegocio,
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private enrutadorService: EnrutadorService
  ) {
    this.params = { estado: false };
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.fechaInicial = new Date('2021-01-02');
    this.fechaFinal = new Date();
    this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
    this.idCapaCuerpo = 'capa-cuerpo-noticias';
    this.confResultadoBusqueda = [];
    this.menosVotados = false;
    this.listaProyectoModel = [];
    this.listaProyectoModelFiltroOtro = [];
    this.currentDay = new Date().toISOString().slice(0, 10);
  }

  ngOnInit(): void {
    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    console.log(this.informacionEnrutador);

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarToast();
      this.configurarAppBar();
      this.configurarLineas();
      this.ejecutarBusqueda();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  obtenerFechaParaParametro(fecha: Date) {
    const dia = (fecha.getDate() < 10) ? '0' + fecha.getDate() : fecha.getDate();
    const mes = (fecha.getMonth() + 1 < 10) ? '0' + (fecha.getMonth() + 1) : fecha.getMonth() + 1;
    return fecha.getFullYear() + '-' + mes + '-' + dia;
  }

  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros() {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }

    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }
      if (item.nombre === 'menosVotado') {
        this.params.menosVotado = item.valor;
        this.params.estado = true;
        if (this.params.menosVotado === 'true') {
          this.filtroGeneral = FiltroBusquedaProyectos.MENOS_VOTADOS;
          this.menosVotados = true;
        } else {
          this.filtroGeneral = FiltroBusquedaProyectos.NUEVOS_PROYECTO;
          this.menosVotados = false;
        }
      }

    });


  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false, // True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  configurarAppBar() {
    let llaverTextoTipoProyecto: string;

    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL) {
      llaverTextoTipoProyecto = 'm5v1texto2.2';
    }

    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL) {
      llaverTextoTipoProyecto = 'm5v1texto2';
    }

    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_PAIS) {
      llaverTextoTipoProyecto = 'm5v1texto2.1';
    }

    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_RED) {
      llaverTextoTipoProyecto = 'm5v1texto2.3';
    }

    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion);
      },
      accionAtras: () => this.accionAtras(),
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [
            ]
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            }
          }
        ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: llaverTextoTipoProyecto
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
            posicion: this.informacionEnrutador.posicion
          }
        }
      },
    };
  }

  configurarLineas() {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  inicializarListaProyectos() {
    this.listaProyectoModel = [];

    this.listaProyectos = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  accionAtras() {
    if (this.filtroActivoDeBusqueda !== FiltroBusqueda.RECIENTES) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
      this.ejecutarBusqueda();
      return;
    }

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);

  }

  tapEnInputFecha(id: string) {
    const elemento: HTMLElement = document.getElementById(id) as HTMLElement;
    if (elemento) {
      elemento.click();
    }
  }

  async obtenerProyectosSinFriltroFechas(
    perfil: string,
    tipo: CodigosCatalogoTipoProyecto,
  ) {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }

    try {
      this.mostrarCapaError = false;
      this.mostrarCapaLoader = (this.listaProyectos.lista.length === 0);
      const dataPaginacion = await this.proyectoNegocio.obtenerProyectosSinFriltroFechas(
        20,
        this.listaProyectos.paginaActual,
        perfil,
        tipo
      ).toPromise();

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;

      dataPaginacion.lista.forEach(proyecto => {
        this.listaProyectoModel.push(proyecto);
        this.listaProyectos.lista.push(this.configurarItemListaNoticias(proyecto));
      });
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = false;
      this.mostrarCapaError = true;
    }

  }

  async obtenerProyectosPorFiltro(
    filtro: FiltroBusquedaProyectos,
    tipo: CodigosCatalogoTipoProyecto,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
  ) {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }

    try {
      this.mostrarCapaError = false;
      this.mostrarCapaLoader = (this.listaProyectos.lista.length === 0);
      const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorFiltro(
        16,
        this.listaProyectos.paginaActual,
        filtro,
        tipo,
        perfil,
        fechaInicial,
        fechaFinal
      ).toPromise();

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;

      dataPaginacion.lista.forEach(proyecto => {
        this.listaProyectoModel.push(proyecto);
        this.listaProyectos.lista.push(this.configurarItemListaNoticias(proyecto));
      });
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = false;
      this.mostrarCapaError = true;
    }
  }

  configurarItemListaNoticias(proyecto: ProyectoModel): CongifuracionItemProyectosNoticias {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );

    return {
      id: proyecto.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (data.id) {
            const proyecto: ProyectoModel = this.obtenerProyectoDeLista(data.id);

            if (!this.perfilSeleccionado || !proyecto || proyecto === null) {
              return;
            }

            let accionEntidad: AccionEntidad = AccionEntidad.ACTUALIZAR;

            if (
              proyecto &&
              proyecto !== null &&
              this.perfilSeleccionado._id !== proyecto.perfil._id
            ) {
              accionEntidad = AccionEntidad.VISITAR;
            }

            this.enrutadorService.navegar(
              {
                componente: PublicarComponent,
                params: [
                  {
                    nombre: 'id',
                    valor: data.id
                  },
                  {
                    nombre: 'accionEntidad',
                    valor: accionEntidad
                  },
                ]
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
                extras: {
                  tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                  paramAValidar: 'id'
                }
              }
            );
          }
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectoModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectoModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo() {
    if (!this.puedeCargarMas) {
      return;
    }

    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;

      if (this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS || this.params.menosVotado === 'true') {
        this.obtenerProyectosPorFiltro(
          this.filtroGeneral,
          this.params.codigoTipoProyecto,
          this.perfilSeleccionado._id,
          this.obtenerFechaParaParametro(this.fechaInicial),
          this.obtenerFechaParaParametro(this.fechaFinal)
        );
        return;
      }

      this.obtenerProyectosSinFriltroFechas(
        this.perfilSeleccionado._id,
        this.params.codigoTipoProyecto,
      );
    }
  }

  validarFechasIngresadas() {
    if (this.fechaInicial.getTime() > this.fechaFinal.getTime()) {
      this.toast.abrirToast('text53');
      return false;
    }

    return true;
  }

  ejecutarBusqueda() {
    if (this.mostrarCapaLoader) {
      return;
    }

    if (
      this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS &&
      !this.validarFechasIngresadas()
    ) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
      return;
    }


    this.inicializarListaProyectos();

    if (this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS || this.params.menosVotado === 'true') {
      this.obtenerProyectosPorFiltro(
        this.filtroGeneral,
        this.params.codigoTipoProyecto,
        this.perfilSeleccionado._id,
        this.obtenerFechaParaParametro(this.fechaInicial),
        this.obtenerFechaParaParametro(this.fechaFinal)
      );
      return;
    }

    this.obtenerProyectosSinFriltroFechas(
      this.perfilSeleccionado._id,
      this.params.codigoTipoProyecto,
    );
  }

  cambioDeFecha(orden: number) {
    if (orden === 0) {
      const fechaInicial = this.fechaInicial;
      this.fechaInicial = new Date(this.obtenerFechaSeleccionada(fechaInicial));
    }

    if (orden === 1) {
      const fechaFinal = this.fechaFinal;
      this.fechaFinal = new Date(this.obtenerFechaSeleccionada(fechaFinal));
    }

    this.filtroActivoDeBusqueda = FiltroBusqueda.RANGO_FECHAS;
    this.ejecutarBusqueda();
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }

  obtenerFechaSeleccionada(fecha): string {
    // let date = new Date(fecha);
    // let anio = date.getFullYear().toString();
    // let mes_temporal = date.getMonth() + 1;
    // let dia_temporal = date.getDate() + 1
    // var mes = String(mes_temporal.toString()).padStart(2, '0');
    // let dia = String(dia_temporal.toString()).padStart(2, '0');

    // let fechaSeleccionada = anio + '/' + mes + '/' + dia;

    // return fechaSeleccionada

    const fechaAServer1 = fecha.replace('-', '/');
    const fechaAServer2 = fechaAServer1.replace('-', '/');
    return fechaAServer2;
  }



}

export enum FiltroBusqueda {
  RECIENTES = 'none',
  RANGO_FECHAS = 'fecha',
  MAS_VOTADAS = 'voto'
}
