import { Component, OnInit } from '@angular/core';
import { NotificacionesDeUsuario } from '@core/servicios/generales';
@Component({
    selector: 'app-proyectos',
    templateUrl: './proyectos.component.html',
    styleUrls: ['./proyectos.component.scss']
})

export class ProyectosComponent implements OnInit {

    constructor(
        private notificacionesUsuario: NotificacionesDeUsuario
    ) {
        
    }

    ngOnInit(): void {
        this.notificacionesUsuario.validarEstadoDeLaSesion(false)
    }

}