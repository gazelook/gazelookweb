import {Component, OnInit} from '@angular/core';
import {ConfiguracionAppbarCompartida, DatosLista, LineaCompartida} from '@shared/diseno/modelos';
import {AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, TamanoLista, UsoAppBar} from '@shared/diseno/enums';
import {CatalogoMetodoPagoModel} from 'dominio/modelo/catalogos';
import {CodigosCatalogosMetodosDePago, CodigosEstadoMetodoPago} from '@core/servicios/remotos/codigos-catalogos';
import {InformacionEnrutador} from 'presentacion/enrutador/enrutador.component';
import {EnrutadorService} from '@core/servicios/generales';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {PagoNegocio} from 'dominio/logica-negocio';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';

@Component({
  selector: 'app-donacion-proyectos',
  templateUrl: './donacion-proyectos.component.html',
  styleUrls: ['./donacion-proyectos.component.scss']
})
export class DonacionProyectosComponent implements OnInit {

  public informacionEnrutador: InformacionEnrutador;

  public configuracionAppBar: ConfiguracionAppbarCompartida;
  public confLinea: LineaCompartida;

  public dataLista: DatosLista;
  public listaMetodoPago: CatalogoMetodoPagoModel[];
  public codigosCatalogos = CodigosCatalogosMetodosDePago;
  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public metodoPagoCriptomoneda: string = CodigosCatalogosMetodosDePago.CRIPTOMONEDA;

  public params: ProyectoParams;

  constructor(
    private enrutadorService: EnrutadorService,
    private pagoNegocio: PagoNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
  ) {
    this.params = {estado: false};
  }

  ngOnInit(): void {
    this.configurarParametros();
    if (this.params.estado) {
      this.configurarAppBar();
      this.configurarLineas();
      this.prepararLista();
      this.obtenerCatalogoMetodosPago();
      return;
    }

    console.log(this.params);

    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  // Métodos pago
  private obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago()
      .subscribe((resp: CatalogoMetodoPagoModel[]) => {
        this.dataLista.cargando = false;
        this.listaMetodoPago = resp;
      }, error => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      });
  }

  private prepararLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES
    };
  }


  configurarParametros(): void {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }
    this.informacionEnrutador.params.forEach(item => {
      if (item.nombre === 'codigoTipoProyecto') {
        this.params.codigoTipoProyecto = item.valor;
        this.params.estado = true;
      }
    });
    console.log(this.params);
  }

  // Config AppBar
  private configurarAppBar(): void {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: false
        },
        mostrarDivBack: {
          icono: true,
          texto: false,
        },
        mostrarTextoHome: false,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      }
    };
  }

  public navegarAtras(event): void {
    console.log(event);
    this.accionAtras();
  }

  public accionAtras(): void {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  configurarLineas(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador): void {
    this.informacionEnrutador = item;
  }
}
