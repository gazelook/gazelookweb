import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { GeneradorId, VariablesGlobales } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {
  CodigosCatalogoEntidad,
  CodigosCatalogosEstadoComentario,
  CodigosCatalogoTipoPerfil,
} from '@core/servicios/remotos/codigos-catalogos';
import {
  ColorTextoBoton,
  TipoBoton,
  ToastComponent,
} from '@shared/componentes';
import {
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  TipoDialogo,
  UsoAppBar,
} from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida,
  ConfiguracionComentario,
  ConfiguracionToast,
  DialogoCompartido,
  ModoBusqueda,
} from '@shared/diseno/modelos';
import {
  ComentarioFirebaseEntity,
  ComentarioFirebaseEntityMapperService,
} from 'dominio/entidades';
import {
  ComentarioNegocio,
  CuentaNegocio,
  PerfilNegocio,
} from 'dominio/logica-negocio';
import {
  ComentarioFirebaseModel,
  ComentarioModel,
  PerfilModel,
} from 'dominio/modelo/entidades';
import {
  Enrutador,
  InformacionEnrutador,
} from '../../enrutador/enrutador.component';
import { IndicadorTotalComentarios } from '../publicar/publicar.component';
import {
  ComentariosFirebaseService,
  OrigenConexion,
} from './../../../nucleo/servicios/generales/comentarios-firebase.service';

@Component({
  selector: 'app-historico-proyecto',
  templateUrl: './historico-proyecto.component.html',
  styleUrls: ['./historico-proyecto.component.scss'],
})
export class HistoricoProyectoComponent implements OnInit, Enrutador {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  public CapaActivaEnum = CapaActiva;
  public params: HistoricoParams;
  public perfilSeleccionado: PerfilModel;
  public idCapaFormulario: string;
  public capaActiva: CapaActiva;
  public loaderPag: boolean;
  public formatoFecha: string;
  public comentarioAEliminar: ComentarioModel;

  // Comentarios
  public comentarios: ListasDinamicas<
    ComentarioFirebaseModel,
    ConfiguracionComentario,
    IndicadorTotalComentarios,
    Date
  >;
  public eventoTapComentario: Function;
  public eventoTapPerfilComentario: Function;

  public confAppBar: ConfiguracionAppbarCompartida;
  public confDialogoRecuperarComentario: DialogoCompartido;
  public confToast: ConfiguracionToast;

  public informacionEnrutador: InformacionEnrutador;

  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private route: ActivatedRoute,
    public comenFireService: ComentariosFirebaseService,
    private comentarioFirebaseEntityMapperService: ComentarioFirebaseEntityMapperService,
    private generadorId: GeneradorId,
    private comentarioNegocio: ComentarioNegocio,
    private db: AngularFireDatabase,
    private enrutadorService: EnrutadorService
  ) {
    this.formatoFecha = 'dd/MM/yyyy';
    this.idCapaFormulario =
      'capa_formulario_' + this.generadorId.generarIdConSemilla();
    this.params = { estado: false };
    this.capaActiva = CapaActiva.LOADER;
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = false;
    this.configurarPerfilSeleccionado();
    this.configurarParametrosDeLaUrl();
    this.configurarDialogoRecuperarComentario();
    this.configurarListaComentarios();
    this.configurarIndicadorTotalDeComentarios();
    this.configurarToast();
    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarEventosComentario();
      this.configurarTotalDeComentarios();
      this.obtenerTotalDeComentarios();
      this.obtenerListaDeComentariosEnPaginacion();
      this.obtenerComentariosPaginacion();
      return;
    }

    this.perfilNegocio.validarEstadoDelPerfil(
      this.perfilSeleccionado,
      this.params.estado
    );
  }

  configurarParametrosDeLaUrl() {
    if (!this.informacionEnrutador) {
      this.params.estado = false;
      return;
    }

    this.informacionEnrutador.params.forEach((item) => {
      if (item.nombre === 'id') {
        this.params.id = item.valor;
        this.params.estado = true;
      }
    });
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarDialogoRecuperarComentario() {
    this.confDialogoRecuperarComentario = {
      mostrarDialogo: false,
      completo: true,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: 'm4v11texto2',
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoRecuperarComentario.mostrarDialogo = false;
            this.reactivarComentario();
          },
        },
        {
          text: 'm3v9texto3',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoRecuperarComentario.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  configurarListaComentarios() {
    this.comentarios = {
      cargarMas: false,
      datos: [],
      configuraciones: [],
      fechas: [],
    };
  }

  configurarIndicadorTotalDeComentarios(
    total: number = 0,
    comentarios: Array<string> = []
  ) {
    this.comentarios.indicador = {
      total: total,
      comentarios: comentarios,
    };
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      accionAtras: () => {
        this.accionAtras();
      },
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm5v6texto12',
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v1texto1',
            valorBusqueda: '',
          },
        },
      },
    };
  }

  configurarEventosComentario() {
    this.eventoTapComentario = (comentario: ComentarioModel) => {
      this.comentarioAEliminar = comentario;
      this.confDialogoRecuperarComentario.mostrarDialogo = true;
    };
    this.eventoTapPerfilComentario = (
      idPerfilCoautor: string,
      idInterno: string
    ) => {};
  }

  obtenerTotalDeComentarios() {
    this.comenFireService.totalComentarios.ejecutar$.next({
      idProyecto: this.params.id,
      estado: CodigosCatalogosEstadoComentario.HISTORICO,
    });
  }

  configurarTotalDeComentarios() {
    this.comenFireService.totalComentarios.subscripcion$ =
      this.comenFireService.totalComentarios.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.TOTAL_COMENTARIOS);

          if (data) {
            this.configurarIndicadorTotalDeComentarios(
              data.length,
              data.map((c) => c.key)
            );
          }
        },
        (error) => {
          this.configurarIndicadorTotalDeComentarios();
        }
      );
  }

  obtenerComentariosPaginacion() {
    this.comenFireService.paginacion.ejecutar$.next(this.params.id);
  }

  async obtenerListaDeComentariosEnPaginacion() {
    this.comenFireService.paginacion.subscripcion$ =
      this.comenFireService.paginacion.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.PAGINACION);
          this.capaActiva =
            this.comentarios.datos.length === 0
              ? CapaActiva.LOADER
              : CapaActiva.CONTENIDO;
          this.loaderPag = this.comentarios.datos.length > 0;

          if (!data) {
            throw new Error('Error al cargar los comentarios');
          }

          this.configurarListaDeComentarios(data);

          this.loaderPag = false;
          this.capaActiva = CapaActiva.CONTENIDO;
          this.comentarios.cargarMas = true;
        },
        (error) => {
          this.comentarios.cargarMas = false;
          this.comenFireService.paginacionComentarios -= 30;
          this.capaActiva = CapaActiva.ERROR;
        }
      );
  }

  async configurarListaDeComentarios(
    actions: AngularFireAction<DataSnapshot>[]
  ) {
    try {
      const data = actions.reverse();

      data.forEach((item) => {
        const comentario: ComentarioFirebaseModel =
          this.comentarioFirebaseEntityMapperService.transform(
            item.payload.val() as ComentarioFirebaseEntity
          );

        const index = this.comentarios.datos.findIndex(
          (e) => e.id === comentario.id
        );
        if (
          comentario.estado.codigo ===
          CodigosCatalogosEstadoComentario.HISTORICO
        ) {
          if (index < 0) {
            this.comentarios.datos.push(comentario);
          }
        }
      });
      this.comentarios.datos =
        this.comenFireService.ordenarComentariosDeFormaDescendente(
          this.comentarios.datos
        );
      this.comentarios.fechas =
        this.comenFireService.configurarListaDeFechasComentarios(
          this.comentarios.fechas,
          this.comentarios.datos
        );
      this.comentarios.configuraciones =
        this.comenFireService.crearConfigurarDeLosComentarios(
          this.comentarios.fechas,
          this.comentarios.datos,
          this.comentarios.configuraciones,
          this.perfilSeleccionado._id,
          this.perfilSeleccionado._id,
          false,
          this.eventoTapComentario,
          this.eventoTapPerfilComentario,
          true
        );
    } catch (error) {
      this.capaActiva = CapaActiva.ERROR;
    }
  }

  accionAtras() {
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  async scroolEnCapaFormulario() {
    const elemento: HTMLElement = document.getElementById(
      this.idCapaFormulario
    ) as HTMLElement;
    if (
      !elemento ||
      !this.comentarios.cargarMas ||
      this.comentarios.indicador.total === this.comentarios.datos.length
    ) {
      this.comentarios.cargarMas = false;
      return;
    }

    if (
      elemento.offsetHeight + elemento.scrollTop >=
      elemento.scrollHeight - 15
    ) {
      this.comentarios.cargarMas = false;
      this.comenFireService.paginacionComentarios += 30;
      this.obtenerListaDeComentariosEnPaginacion();
      this.obtenerComentariosPaginacion();
    }
  }

  trackByFn(index: number, item: ConfiguracionComentario) {
    return item.idInterno;
  }

  reintentar() {}

  configurarToast() {
    this.confToast = {
      mostrarToast: false, //True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  async reactivarComentario() {
    if (!this.comentarioAEliminar) {
      return;
    }

    try {
      this.confDialogoRecuperarComentario.mostrarDialogo = false;

      const querys = {};
      const path =
        this.params.id + '/' + this.comentarioAEliminar.id + '/estado';
      querys[path] = {
        codigo: CodigosCatalogosEstadoComentario.ACTIVA,
      };

      this.db.database
        .ref('comentarios')
        .update(querys)
        .then(
          () => {
            const index = this.comentarios.datos.findIndex(
              (e) => e.id === this.comentarioAEliminar.id
            );
            this.eliminarComentariosDeListas(
              this.comentarioAEliminar.id,
              index
            );

            this.comentarioAEliminar = undefined;
          },
          (error) => {
            throw new Error('');
          }
        );
    } catch (error) {
      this.toast.abrirToast('texto37');
    }
  }

  eliminarComentariosDeListas(idComentario: string, index: number = -1) {
    if (index >= 0) {
      this.comentarios.datos.splice(index, 1);
    }

    // Actualizar indicador del total
    this.comentarios.indicador =
      this.comenFireService.actualizarIndicadorDelTotalDeComentarios(
        idComentario,
        this.comentarios.indicador
      );

    // Actualizar lista de configuraciones
    const listas =
      this.comenFireService.actualizarListaDeConfiguracionDeComentarios(
        idComentario,
        this.comentarios.fechas,
        this.comentarios.configuraciones
      );

    this.comentarios.fechas = listas.listaDeFechas;
    this.comentarios.configuraciones = listas.listaConfiguracionComentarios;
  }

  configurarInformacionDelEnrutador(info: InformacionEnrutador) {
    this.informacionEnrutador = info;
  }
}
export interface ListasDinamicas<A, B, C, D> {
  datos?: Array<A>;
  configuraciones?: Array<B>;
  indicador?: C;
  fechas?: Array<D>;
  cargarMas?: boolean;
}
export interface HistoricoParams {
  estado: boolean;
  id?: string;
}

export enum CapaActiva {
  LOADER = 'loader',
  ERROR = 'error',
  CONTENIDO = 'contenido',
}
