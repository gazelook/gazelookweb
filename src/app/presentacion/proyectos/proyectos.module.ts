import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProyectosRoutingModule } from './proyectos-routing.module';
import { PublicarComponent } from './publicar/publicar.component';
import { TranslateModule } from '@ngx-translate/core';
import { ProyectosComponent } from './proyectos.component';
import { CompartidoModule } from 'src/app/compartido/compartido.module';
import { InformacionUtilComponent } from './informacion-util/informacion-util.component';
import { MenuVerProyectosComponent } from './menu-ver-proyectos/menu-ver-proyectos.component';
import { MenuListaProyectosComponent } from './menu-lista-proyectos/menu-lista-proyectos.component';
import { NuevosProyectosComponent } from './nuevos-proyectos/nuevos-proyectos.component';
import { HistoricoProyectoComponent } from './historico-proyecto/historico-proyecto.component';
import { ProyectosRecomendadosComponent } from './proyectos-recomendados/proyectos-recomendados.component';
import { ProyectosForosComponent } from './proyectos-foros/proyectos-foros.component';
import { ProyectosSeleccionadosComponent } from './proyectos-seleccionados/proyectos-seleccionados.component';
import { ProyectosEsperaFondosComponent } from './proyectos-espera-fondos/proyectos-espera-fondos.component';
import { DonacionProyectosComponent } from './donacion-proyectos/donacion-proyectos.component';
import { MetodoStripeComponent } from './donacion-proyectos/metodos-pago/metodo-stripe/metodo-stripe.component';
import { MetodoCriptoComponent } from './donacion-proyectos/metodos-pago/metodo-cripto/metodo-cripto.component';
import { MetodoPaymentezComponent } from './donacion-proyectos/metodos-pago/metodo-paymentez/metodo-paymentez.component';
import {NgxStripeModule} from '@env/node_modules/ngx-stripe';


@NgModule({
  declarations: [
    ProyectosComponent,
    PublicarComponent,
    InformacionUtilComponent,
    MenuVerProyectosComponent,
    MenuListaProyectosComponent,
    NuevosProyectosComponent,
    HistoricoProyectoComponent,
    ProyectosRecomendadosComponent,
    ProyectosForosComponent,
    ProyectosSeleccionadosComponent,
    ProyectosEsperaFondosComponent,
    DonacionProyectosComponent,
    MetodoStripeComponent,
    MetodoCriptoComponent,
    MetodoPaymentezComponent,
  ],
  imports: [
    FormsModule,
    TranslateModule,
    CommonModule,
    ProyectosRoutingModule,
    CompartidoModule,
    NgxStripeModule
  ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class ProyectosModule { }
