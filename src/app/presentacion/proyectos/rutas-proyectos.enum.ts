export enum RutasProyectos {
  PUBLICAR = 'publicar/:codigoTipoProyecto',
  ACTUALIZAR = 'actualizar/:id',
  VISITAR = 'visitar/:id',
  INFORMACION_UTIL = 'util',
  MENU_SELECCIONAR_TIPO_PROYECTOS = 'tipo',
  MENU_LISTA_PROYECTOS = 'lista/:codigoTipoProyecto',
  LISTA_PROYECTOS_RECOMENDADOS = 'recomendados/:codigoTipoProyecto',
  LISTA_PROYECTOS_FOROS = 'foros/:codigoTipoProyecto',
  LISTA_PROYECTOS_SELECCIONADOS = 'seleccionados/:codigoTipoProyecto',
  LISTA_NUEVOS_PROYECTOS = 'recientes/:codigoTipoProyecto/:menosVotado',
  HISTORICO = 'historico/:idProyecto/:idPerfilProyecto',
  DONACIONES_PROYECTOS = 'donaciones/:codigoTipoProyecto',
}
