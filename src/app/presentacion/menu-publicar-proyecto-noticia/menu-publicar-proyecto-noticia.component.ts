import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { VariablesGlobales } from '@core/servicios/generales';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import {NotificacionesDeUsuario} from '@core/servicios/generales/notificaciones';
import {
  AccionEntidad, CodigosCatalogoTipoPerfil, CodigosCatalogoTipoProyecto
} from '@core/servicios/remotos/codigos-catalogos';
import { AnchoLineaItem, ColorFondoLinea, EspesorLineaItem, TamanoColorDeFondo, UsoAppBar } from '@shared/diseno/enums';
import {
  ConfiguracionAppbarCompartida, LineaCompartida
} from '@shared/diseno/modelos';
import {
  CuentaNegocio, PerfilNegocio
} from 'dominio/logica-negocio';
import {
  PerfilModel
} from 'dominio/modelo/entidades';
import { Enrutador, InformacionEnrutador, TipoDeNavegacion } from '../enrutador/enrutador.component';
import { PublicarComponent as PublicarNoticiaComponent } from '../noticias/publicar/publicar.component';
import { InformacionUtilComponent } from '../proyectos/informacion-util/informacion-util.component';
import { PublicarComponent } from '../proyectos/publicar/publicar.component';
import { MenuPrincipalComponent } from './../menu-principal/menu-principal.component';

@Component({
  selector: 'app-menu-publicar-proyecto-noticia',
  templateUrl: './menu-publicar-proyecto-noticia.component.html',
  styleUrls: ['./menu-publicar-proyecto-noticia.component.scss']
})
export class MenuPublicarProyectoNoticiaComponent implements OnInit, Enrutador {

  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel

  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaVerde: LineaCompartida
  public confLineaVerdeAll: LineaCompartida

  public informacionEnrutador: InformacionEnrutador

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location,
    private variablesGlobales: VariablesGlobales,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private enrutadorService: EnrutadorService

  ) {

  }

  ngOnInit(): void {

    this.variablesGlobales.mostrarMundo = true
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()

    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      this.configurarLinea()
      this.notificacionesUsuario.validarEstadoDeLaSesion(false)
      return
    }
  }

  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion),
      eventoHome: () => this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [
          ]
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          }
        }
      ),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v5texto29'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    }
  }

  configurarLinea() {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  irACrearProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
    this.enrutadorService.navegar(
      {
        componente: PublicarComponent,
        params: [
          {
            nombre: 'accionEntidad',
            valor: AccionEntidad.CREAR
          },
          {
            nombre: 'codigoTipoProyecto',
            valor: codigo
          }
        ]
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,

      }
    )
  }

  irAInformacionUtilDeProyectos() {
    this.enrutadorService.navegar(
      {
        componente: InformacionUtilComponent,
        params: []
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion
      }
    )
  }

  irACrearNoticia() {
    this.enrutadorService.navegar(
      {
        componente: PublicarNoticiaComponent,
        params: [
          {
            nombre: 'accionEntidad',
            valor: AccionEntidad.CREAR
          }
        ]
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion
      }
    )
  }

  navegarSegunMenu(menu: number) {
    switch (menu) {
      case 0:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL)
        break
      case 1:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS)
        break
      case 2:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_LOCAL)
        break
      case 3:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_RED)
        break
      case 4:
        this.irACrearNoticia()
        break
      case 5:
        this.irAInformacionUtilDeProyectos()
        break
    }
  }

  // Enrutamiento
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }

}
