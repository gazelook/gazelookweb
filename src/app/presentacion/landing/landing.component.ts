import { LandingNegocio } from 'dominio/logica-negocio/landing.negocio';
import { TranslateService } from '@ngx-translate/core';
import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos';
import { LlavesSessionStorage } from '@core/servicios/locales';
import {
  InternacionalizacionNegocio,
  IdiomaNegocio,
  CuentaNegocio,
} from 'dominio/logica-negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { VariablesGlobales } from '@core/servicios/generales';
import { Location } from '@angular/common';
import {
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionToast } from '@shared/diseno/modelos';
import { ColorTextoBoton } from 'src/app/compartido/componentes/button/button.component';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from 'src/app/compartido/diseno/modelos/boton.interface';
import { TipoBoton } from '@shared/componentes';

import { RutasLocales } from './../../rutas-locales.enum';
import { FormularioContactanos } from '../../dominio/modelo/entidades/fomulario-contactanos.model';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
} from '@angular/animations';
import { HttpStatusCode } from '@angular/common/http';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit, OnDestroy {
  private menuGlobal: number;
  public texto1: string;
  public texto2: string;
  public texto3: string;
  public textoRotativo: string;
  public contador: number;
  public contador2: number;
  public contador3: number;
  public interval: any;
  public interval1: any;
  public interval2: any;
  public interval3: any;
  public interval4: any;
  public interval4Delay: any;
  public interval5: any;
  public interval5Delay: any;
  public interval6: any;
  public interval7: any;
  public interval8: any;
  public tiempoIntervalo: number;
  public tiempoIntervalo2: number;
  public tiempoIntervalo3: number;
  public tiempoIntervalo8: number;
  public mostrarTexto1: number;
  public blanco1: string;
  public blanco2: string;
  public mostrarTextoVerde: boolean;
  public mostrarBlanco: boolean;
  public mostrarCuadroMadera: boolean;
  public mostrarCuadroMaderaDelay: boolean;
  public mostrarCuadroAzul: boolean;
  public mostrarCuadroAzulDelay: boolean;
  public mostrarVisitaPla: boolean;
  public mostrarLinkProfundizar: boolean;
  //textos
  public landingCambiosUrgentes: string;
  public landingCambiosUrgentes2: string;
  public landingCambiosUrgentes3: string;
  public confBoton: BotonCompartido;

  public cssTyping: boolean;
  public cssTyping2: boolean;
  public mostrarVisitar: boolean;
  public destroyed = new Subject<void>();
  public tamanioPantallaActual: string;
  public urlVideo: string;
  public temporizadorVideo: any;
  public tiempo_cerrar_video: number;

  public mostrarVideo: boolean;
  // Create a map to display breakpoint names for demonstration purposes.
  mapaNombresPantalla = new Map([
    [Breakpoints.XSmall, 'XSmall'],
    [Breakpoints.Small, 'Small'],
    [Breakpoints.Medium, 'Medium'],
    [Breakpoints.Large, 'Large'],
    [Breakpoints.XLarge, 'XLarge'],
  ]);

  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private router: Router,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private idiomaNegocio: IdiomaNegocio,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private translateService: TranslateService,
    private landingNegocio: LandingNegocio,
    private breakpointObserver: BreakpointObserver
  ) {
    this.textoMensaje = '';
    this.textoMensajeError = '';
    this.mensajeCorreo = false;
    this.contactUs = true;
    this.enContactos = false;
    this.idiomas = 'en';
    this.paginas = 0;
    this.menu = -1;
    this.leermas = -1;
    this.menusyaabiertos = [];
    this.preguntaAbierta = -1;
    this.mostrarLineaHeader = false;
    this.posiciones = [];
    this.nombre = '';
    this.email = '';
    this.pais = '';
    this.direccion = '';
    this.tema = '';
    this.mensaje = '';
    this.paginaAnterior = 0;
    this.mostrarImg = false;
    this.texto1 = '';
    this.texto2 = '';
    this.texto3 = '';
    this.textoRotativo = '';
    this.contador = 1;
    this.contador2 = 1;
    this.contador3 = 1;
    this.tiempoIntervalo = 14000;
    this.tiempoIntervalo2 = 12000;
    this.tiempoIntervalo8 = 20000;
    this.mostrarTexto1 = 1;
    this.blanco1 = '';
    this.blanco2 = '';
    this.mostrarBlanco = true;
    this.mostrarTextoVerde = false;
    this.mostrarCuadroMadera = false;
    this.mostrarCuadroMaderaDelay = false;
    this.mostrarCuadroAzul = false;
    this.mostrarCuadroAzulDelay = false;
    this.mostrarVisitaPla = false;
    this.cssTyping = true;
    this.cssTyping2 = true;
    this.mostrarVisitar = true;
    this.mostrarLinkProfundizar = false;
    this.mostrarVideo = false;

    breakpointObserver
      .observe([
        Breakpoints.XSmall,
        Breakpoints.Small,
        Breakpoints.Medium,
        Breakpoints.Large,
        Breakpoints.XLarge,
      ])
      .pipe(takeUntil(this.destroyed))
      .subscribe((result) => {
        for (const query of Object.keys(result.breakpoints)) {
          if (result.breakpoints[query]) {
            this.tamanioPantallaActual =
              this.mapaNombresPantalla.get(query) ?? 'Unknown';
          }
        }
      });
  }

  @ViewChild('panel') public panel: ElementRef;

  title = 'Mensaje enviado con éxito!';
  estado = 'inactivo';

  public enContactos: boolean;
  public mensajeCorreo: boolean;
  public contactUs: boolean;
  public idiomas: string;
  public paginas: number;
  public menu: number;
  public leermas: number;
  public menusyaabiertos: Array<number>;
  public preguntaAbierta: number;
  public textoMensaje: string;
  public textoMensajeError: string;
  public nombre: string;
  public email: string;
  public pais: string;
  public direccion: string;
  public tema: string;
  public mensaje: string;

  public errorMsj: boolean;
  public confToast: ConfiguracionToast;

  public paginaAnterior: number;
  public mostrarLineaHeader: boolean;
  public mostrarImg: boolean;
  public temaContacto: string;
  public video_precargado: any;
  // variable para boton PWA
  public promptEvent;

  public configBotonPagoExtra: BotonCompartido;

  public posiciones: Array<{
    id: string;
    pos: number;
    elemento: HTMLElement;
    altura: number;
  }>;

  cambiarEstado() {
    this.estado = this.estado === 'activo' ? 'inactivo' : 'activo';
  }

  async ngOnInit() {
    this.configurarVariablesGlobales();
    await this.validarIdiomaInicial();
    this.obtenerTextoPrincipal();
    this.obtenerTextosRotativosVerde();
    this.textoAEscribir();
    this.obtenerTextoSegundaPagina();
    this.verificarPagina();

    this.validarRutaDeContactos();
    this.detectarDispositivo();
    this.configurarVariablesDesdeLocalStorage();
    this.configurarImgCarga();
    this.configurarBoton();
    this.configurarToast();
    this.configurarBotonPrincipal();
    this.obtenerUrlVideo();
    this.video_precargado = document.getElementById('video_precargado');
    this.video_precargado.onloadeddata = (event) => {
      this.duracionVideo();
    };
  }

  verificarPagina() {
    let a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS);

    if (a === '1') {
      clearInterval(this.interval2);
      this.contador2 = 1;
      this.intervalo2();
    }

    let animFin = sessionStorage.getItem(
      LlavesSessionStorage.ANIMACION_FINALIZADA
    );
    console.log('animFin', animFin);
    

    if (animFin !== '1') {
      this.intervalo();
      this.intervalo3();
    }
    if (animFin === '1') {
      console.log('ya la visite');

      this.mostrarTextoVerde = true;
      this.mostrarBlanco = false;
      this.mostrarCuadroMadera = true;
      this.mostrarCuadroMaderaDelay = true;
      this.mostrarCuadroAzul = true;
      this.mostrarCuadroAzulDelay = true;
      this.mostrarVisitaPla = true;
      this.cssTyping = false;
      this.cssTyping2 = false;
    
      this.mostrarLinkProfundizar = true;
      this.intervalo8();
    }

    let videoVisto = sessionStorage.getItem(
      LlavesSessionStorage.VIDE_DEMO
    );

    console.log('videoVisto', videoVisto);
    
    if (videoVisto === '1' ) {
      console.log(111);
      
      this.mostrarVisitar = false;
    }
  }

  ngOnDestroy(): void {
    this.variablesGlobales.configurarDataAnimacion();
    this.destroyed.next();
    this.destroyed.complete();
  }

  public moveToSpecificView(menu: number): void {
    // // this.panel.nativeElement.scrollIntoView({behavior: 'smooth', block: 'nearest', inline: 'nearest'});
    // this.panel.nativeElement.scrollTop += 260;
    // if (this.menuGlobal === menu) {
    //   this.panel.nativeElement.scrollTop -= 280;
    // }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.detectarDispositivo();
  }

  configurarVariablesGlobales(): void {
    this.variablesGlobales.configurarDataAnimacion(true);
  }

  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(event: Event) {
    event.preventDefault();
    this.promptEvent = event;
  }

  instalarPWA() {
    this.promptEvent.prompt();
  }

  public shouldInstall(): boolean {
    return !this.isRunningStandalone() && this.promptEvent;
  }

  public isRunningStandalone(): boolean {
    return window.matchMedia('(display-mode: standalone)').matches;
  }

  configurarImgCarga() {
    let elementoImg: HTMLImageElement;

    elementoImg = document.createElement('img');
    if (this.paginas === 0) {
      elementoImg.src = '../../../assets/recursos/fondo_griego.png';
      elementoImg.onload = () => {
        const elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
      };
      setTimeout(() => {
        if (this.paginas === 0) {
          this.crearTriangulo();
        }
      }, 100);
    }
    if (this.paginas === 1) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-2-web.jpg';
      elementoImg.onload = () => {
        const elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
      };
    }
    // fondoArboles.jpg
    if (this.paginas === 2) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-3.1-web.jpg';
      // elementoImg.src = '../../../assets/fondoArboles.jpg'
      elementoImg.onload = () => {
        const elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
      };
    }
    if (this.paginas === 3) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-web-4.jpg';
      elementoImg.onload = () => {
        const elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
      };
    }
  }

  async obtenerTextoSegundaPagina() {
    let a = await this.landingNegocio
      .mostrarTextoLandingSegundaPagina()
      .toPromise();
    // this.mostrarTexto1 = a
  }

  async obtenerTextoPrincipal() {
    this.blanco1 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingGazesignifica3'
    );

    let b1 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingAhoranuestroTurno'
    );
    let b2 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingDecidirelFuturo'
    );

    this.blanco2 = b1 + ' ' + b2;
  }

  async textoAEscribir() {
    this.landingCambiosUrgentes =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes'
      );
    this.landingCambiosUrgentes2 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes2'
      );

    this.landingCambiosUrgentes3 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes3'
      );
  }

  async obtenerTextosRotativosVerde() {
    this.texto1 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo1'
    );
    this.texto2 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo2'
    );
    this.texto3 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo3'
    );
    this.textoRotativo = this.texto1;
  }
  crearTriangulo() {
    const contenedor_triangulo = document.getElementById('cont-profundizar');
    const txt_prof = document.getElementById('txt-profundizar');
    const ancho_triangulo = txt_prof.clientWidth / 2 + 22;
    const alto_triangulo = ancho_triangulo / 7.84;
    const triangulo = '<div id="triangulo" class="triangulo"></div>';
    contenedor_triangulo.insertAdjacentHTML('beforeend', triangulo);
    const triangulo_id = document.getElementById('triangulo');
    triangulo_id.style.cssText =
      'margin-top: 4px;width: 0;height: 0;border-top: ' +
      alto_triangulo +
      'px solid #9bccff;border-right: ' +
      ancho_triangulo +
      'px solid transparent;border-left: ' +
      ancho_triangulo +
      'px solid transparent; cursor:pointer';
      document.querySelector('#triangulo').addEventListener('click', (e:Event) => this.eventoclick(1));
    }

  intervalo8() {
    // this.interval8 = setInterval(() => {
    //     this.contador3 = 2;
    //     clearInterval(this.interval8);
    // }, 19000);

    this.interval8 = setInterval(() => {
      if (this.contador3 === 1) {
        this.tiempoIntervalo8 = 12000;
        this.contador3 = 2;
        clearInterval(this.interval8);
        this.intervalo8();
        return;
      }

      if (this.contador3 === 2) {
        this.tiempoIntervalo8 = 12000;
        this.contador3 = 1;
        clearInterval(this.interval8);
        this.intervalo8();
        return;
      }
    }, this.tiempoIntervalo8);


  }

  intervalo() {
    this.interval = setInterval(() => {
      if (this.contador === 1) {
        this.textoRotativo = this.texto2;
        this.tiempoIntervalo = 10000;
        this.contador = 2;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }

      if (this.contador === 2) {
        this.textoRotativo = this.texto3;
        this.tiempoIntervalo = 10000;
        this.contador = 3;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }

      if (this.contador === 3) {
        this.textoRotativo = this.texto1;
        this.tiempoIntervalo = 40000;
        this.contador = 1;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }
    }, this.tiempoIntervalo);
  }

  intervalo2() {
    this.interval2 = setInterval(() => {
      if (this.contador2 === 1) {
        this.tiempoIntervalo2 = 12000;
        this.contador2 = 2;
        clearInterval(this.interval2);
        this.intervalo2();
        return;
      }

      if (this.contador2 === 2) {
        this.tiempoIntervalo2 = 12000;
        this.contador2 = 1;
        clearInterval(this.interval2);
        this.intervalo2();
        return;
      }
    }, this.tiempoIntervalo2);
  }

  intervalo3() {
    this.interval1 = setTimeout(() => {
      this.mostrarTextoVerde = true;
      console.log('se mostro')
    }, 3500);
    this.interval3 = setTimeout(() => {
      this.cssTyping = false;
      this.mostrarBlanco = false;
    }, 12500);
    this.interval4 = setTimeout(() => {
      this.mostrarCuadroMadera = true;
    }, 15500);

    this.interval4Delay = setTimeout(() => {
      this.mostrarCuadroMaderaDelay = true;
    }, 16000);

    this.interval5 = setTimeout(() => {
      this.contador3 = 2;

      this.intervalo8();
      this.mostrarCuadroAzul = true;
      this.cssTyping2 = false;
    }, 24500);

    this.interval5Delay = setTimeout(() => {
      this.mostrarCuadroAzulDelay = true;
    }, 28500);

    let a =
      ".triangulo { display: flex; align-items: center; width: 90%; margin: auto; max-width: 22em; position: relative; padding: 30% 2em; box-sizing: border-box;$border: 5px; color: #FFF; background: #000; background-clip: padding-box; /* !importanté */ border: solid $border transparent; border-radius: 1em; &:before {content: ''; position: absolute; top: 0; right: 0; bottom: 0; left: 0; z-index: -1; margin: -$border; border-radius: inherit;  background: linear-gradient(to right, red, orange); } }";

    this.interval7 = setTimeout(() => {
      this.mostrarLinkProfundizar = true;
      this.interval5Delay = setTimeout(() => {
        if (this.paginas === 0) {
          this.crearTriangulo();
        }
      }, 100);
    }, 29000);

    this.interval6 = setTimeout(() => {
      this.mostrarVisitaPla = true;
      sessionStorage.setItem(LlavesSessionStorage.ANIMACION_FINALIZADA, '1');
    }, 30000);
  }

  validarRutaDeContactos() {
    if (this.router.url.indexOf(RutasLocales.FORM_CONTACTO.toString()) >= 0) {
      this.enContactos = true;
      this.eventoclick(4);
    }
  }

  eventoclick(pagina: number) {
    if (pagina === 1) {
      clearInterval(this.interval2);
      this.contador2 = 1;
      this.intervalo2();
    }

    if (pagina === 4) {
      this.contactUs = true;
      this.mensajeCorreo = false;
      this.nombre = '';
      this.email = '';
      this.pais = '';
      this.direccion = '';
      this.tema = '';
      this.mensaje = '';
      this.errorMsj = false;
    }

    this.paginaAnterior = this.paginas;
    this.paginas = pagina;
    this.menu = -1;
    this.leermas = -1;
    this.menusyaabiertos = [];
    this.configurarImgCarga();
    // this.mostrarLineaHeader = (this.paginas !== 0)

    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.PAGINAS,
      this.paginas.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
  }

  detectarDispositivo() {
    if (
      /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      document.location.href = 'https://m.gazelook.com/';
    }
  }

  configurarVariablesDesdeLocalStorage() {
    // update local storage
    const a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS);
    const b = sessionStorage.getItem(LlavesSessionStorage.MENU_ACTIVO);
    const c = sessionStorage.getItem(LlavesSessionStorage.MENUS_YA_ABIERTOS);

    if (a && a !== null) {
      this.paginas = parseInt(a);
      // this.mostrarLineaHeader = (this.paginas !== 0)
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.PAGINAS,
        this.paginas.toString()
      );
    }

    if (b && b !== null) {
      this.menu = parseInt(b);
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.MENU_ACTIVO,
        this.menu.toString()
      );
    }

    if (c && c !== null) {
      this.menusyaabiertos = JSON.parse(c);
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.MENUS_YA_ABIERTOS,
        JSON.stringify(this.menusyaabiertos)
      );
    }
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
    };
  }

  scroolEnBody() {
    const elemento = document.getElementById('bodyPagina0') as HTMLElement;

    if (!elemento) {
      return;
    }

    this.mostrarLineaHeader = elemento.scrollTop > 0;
  }

  async validarIdiomaInicial() {
    let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
      CodigosCatalogoIdioma.INGLES
    );
    const idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();

    if (idiomaSeleccionado && idiomaSeleccionado !== null) {
      idioma = idiomaSeleccionado;
    }

    // if (idioma.codNombre !== 'en' && idioma.codNombre !== 'es') {
    // 	idioma.codNombre = 'en'
    // }

    this.idiomas = idioma.codNombre.toLowerCase();
    this.internacionalizacionNegocio.usarIidoma(
      idioma.codNombre.toLocaleLowerCase()
    );
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
    this.idiomaNegocio.eliminarVarablesStorage();
  }

  async cambiarIdioma(codIdioma: string) {
    // let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(codIdioma as serviciosRemotosCodigosEnum.CodigosCatalogoIdioma)
    // if (!idioma || idioma === null) {
    // 	idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(serviciosRemotosCodigosEnum.CodigosCatalogoIdioma.ESPANOL)
    // }

    // if (codIdioma !== 'en' && codIdioma !== 'es') {
    // 	return
    // }

    let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
      codIdioma as CodigosCatalogoIdioma
    );
    if (!idioma || idioma === null) {
      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma.ESPANOL
      );
    }

    this.internacionalizacionNegocio.usarIidoma(
      idioma.codNombre.toLocaleLowerCase()
    );
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
    this.idiomaNegocio.eliminarVarablesStorage();

    this.idiomas = idioma.codNombre.toLowerCase();
    this.paginas = 0;
    this.menu = -1;
    this.leermas = -1;
    this.menusyaabiertos = [];

    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.PAGINAS,
      this.paginas.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
    // clearInterval(this.interval);
    this.obtenerTextosRotativosVerde();

    this.intervalo();
  }

  iralagranja(limpiarEstado: boolean = false) {
    console.log(
222
    );
    
    if (limpiarEstado) {
      sessionStorage.setItem(LlavesSessionStorage.PAGINAS, '0');
      sessionStorage.setItem(LlavesSessionStorage.MENU_ACTIVO, '-1');
      sessionStorage.setItem(
        LlavesSessionStorage.MENUS_YA_ABIERTOS,
        JSON.stringify([])
      );
    }
    this.router.navigateByUrl(RutasLocales.FINCA);
  }

  irAlAnuncio() {
    this.router.navigateByUrl(RutasLocales.ANUNCIO.toString());
  }

  eventomenu(menu: number): void {

    console.log('menu', menu);
   console.log('this.menu', menu);
    
    this.menuGlobal = menu;

    this.preguntaAbierta = -1;
    // Validar si ya esta en menus abiertos
    const index = this.menusyaabiertos.findIndex((e) => e === menu);
    console.log('index', index);
    
    if (index < 0) {
      this.menusyaabiertos.push(menu);
    }

    if (this.menu === menu) {
      console.log('22');
      
      this.menu = -1;
    } else {
      console.log('333');
      
      this.menu = menu;
    }

    if (menu > 12 && menu < 18) {
      console.log('aca');
      
      this.validarScroll(
        this.menu,
        menu,
        menu < 12 ? 0 : 13,
        menu < 12 ? 12 : 18
      );
    }

    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
  }

  validarScroll(viejo: number, nuevo: number, inicio: number, final: number) {
    const body: HTMLElement = document.getElementById(
      'bodylinea'
    ) as HTMLElement;
    const posicionesNuevas: Array<{
      id: string;
      pos: number;
      elemento: HTMLElement;
      altura: number;
    }> = [];

    let aux = inicio;
    const idBase = 'menuinterno_';

    while (aux < final) {
      const elemento = document.getElementById(idBase + aux) as HTMLElement;

      posicionesNuevas.push({
        id: idBase + aux,
        pos: elemento.offsetTop,
        elemento,
        altura: elemento.clientHeight,
      });
      aux++;
    }

    if (viejo === -1) {
      this.posiciones = posicionesNuevas;
    }

    if (inicio >= 13 && inicio < 17) {
      if (nuevo == 13) {
        this.posiciones = posicionesNuevas;
        body.scrollTop = this.posiciones[nuevo - 13].pos - 115;
      } else {
        if (nuevo === viejo) {
          if (nuevo === viejo + 1) {
            body.scrollTop = posicionesNuevas[nuevo - 14].pos - 120;
          } else {
            body.scrollTop = this.posiciones[nuevo - 14].pos - 120;
          }
        } else {
          body.scrollTop = posicionesNuevas[nuevo - 14].pos - 120;
        }
      }
    }
  }

  eventoleermas(leermas: number) {
    if (this.leermas === leermas) {
      this.leermas = -1;
    } else {
      this.leermas = leermas;
    }
  }

  cambiarpreguntaAbierta(pre: number) {
    if (this.preguntaAbierta === pre) {
      this.preguntaAbierta = -1;
    } else {
      this.preguntaAbierta = pre;
    }
  }

  validarCampos() {
    if (!(this.nombre.length > 0)) {
      // this.toast.abrirToast('landingIngreseNombre')
      return false;
    }

    if (!(this.email.length > 0)) {
      // this.toast.abrirToast('landingIngreseEmail')
      return false;
    }

    if (!(this.pais.length > 0)) {
      // this.toast.abrirToast('landingIngresePais')
      return false;
    }

    if (!(this.direccion.length > 0)) {
      // this.toast.abrirToast('landingIngreseDireccion')
      return false;
    }

    if (!(this.tema.length > 0)) {
      // this.toast.abrirToast('landingIngreseTema')
      return false;
    }

    if (!(this.mensaje.length > 0)) {
      // this.toast.abrirToast('landingIngreseMensaje')
      return false;
    }

    return true;
  }

  async enviarmensaje() {
    // this.toast.abrirToast('landingEsperePorFavor', true)

    if (!this.validarCampos()) {
      return;
    }

    const correo: CorreoContacto = {
      nombre: this.nombre,
      email: this.email,
      pais: this.pais,
      direccion: this.direccion,
      tema: this.tema,
      mensaje: this.mensaje,
    };

    try {
      // const status: string = await this.cuentaNegocio.enviarEmailDeContacto(correo).toPromise()
      const status = null;
      if (!status) {
        throw new Error('');
      }

      // this.toast.abrirToast('landingSolicitudEnviada')
      this.nombre = '';
      this.email = '';
      this.pais = '';
      this.direccion = '';
      this.tema = '';
      this.mensaje = '';
    } catch (error) {
      // this.toast.abrirToast('landingLoSentimos')
    }
  }

  obtenerUrlVideo() {
    const relacionPantalla = window.screen.width / window.screen.height;
    if (relacionPantalla > 4 / 3) {
      this.urlVideo = `https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/videos/gazelook-demo-${this.idiomas.toUpperCase()}-16-9.mp4`;
    } else {
      this.urlVideo = `https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/videos/gazelook-demo-${this.idiomas.toUpperCase()}-4-3.mp4`;
    }

    console.log('this.urlVideo', this.urlVideo);
  }

  duracionVideo() {
    this.tiempo_cerrar_video =
      document.getElementById('video_precargado')['duration'] * 1000 + 2000;
    console.log('duracion del video', this.tiempo_cerrar_video);
  }

  irAlDemo() {
    const contenedor_video = document.getElementById('video_demo');
    const alto_contenedor_landing = window.innerHeight;
    contenedor_video.style.display = 'block';
    const relacionPantalla = window.screen.width / window.screen.height;
    if (relacionPantalla > 4 / 3) {
      console.log('a');

      contenedor_video.insertAdjacentHTML(
        'afterbegin',
        '<video id="video" src="' + this.urlVideo + '" autoplay muted></video>'
      );
    } else {
      console.log('b');
      contenedor_video.insertAdjacentHTML(
        'afterbegin',
        '<video id="video" src="' + this.urlVideo + '" autoplay muted></video>'
      );
    }
    const video_id = document.getElementById('video');
    video_id.style.cssText =
      'position: absolute;top: 0;left: 0;width: 100%; height: ' +
      alto_contenedor_landing +
      'px;z-index: 999;background: linear-gradient(0deg, #408ede -80.67%, #051025 25.54%);background-image: linear-gradient(0deg, rgb(64, 142, 222) -80.67%, rgb(5, 16, 37) 25.54%);background-position-x: initial;background-position-y: initial;background-size: initial;background-repeat-x: initial;background-repeat-y: initial;background-attachment: initial;background-origin: initial;background-clip: initial;background-color: initial;';
    console.log('terminiConfiguracion', contenedor_video);

    this.cerrarVideoDuracion();
  }

  cerrarVideoDuracion() {
    this.temporizadorVideo = setTimeout(() => {
      this.cerrarDemo();
    }, this.tiempo_cerrar_video);
  }

  cerrarDemo() {
    console.log('porq se pone 1');
    
    sessionStorage.setItem(LlavesSessionStorage.VIDE_DEMO, '1');
    this.mostrarVisitar = false;
    const contenedor_video = document.getElementById('video_demo');
    const video_eliminar = document.getElementById('video');
    if (video_eliminar) {
      contenedor_video.removeChild(video_eliminar);
      contenedor_video.style.display = 'none';
      clearInterval(this.temporizadorVideo);
    }
  }
  async configurarBotonPrincipal() {
    this.confBoton = {
      text: 'm1v4texto1',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO);
      },
    };
    //this.confBoton.text = await this.servicioIdiomas.obtenerTextoLlave(this.configuracion.demoAppbar.boton.llaveTexto ? this.configuracion.demoAppbar.boton.llaveTexto : 'undefined')
  }

  irDocumentosLegales(limpiarEstado: boolean = false): void {
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/terminos-condiciones-${this.idiomas.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(
      new MouseEvent('click', {
        view: window,
        bubbles: false,
        cancelable: true,
      })
    );
  }

  async configurarBoton(): Promise<void> {
    this.configBotonPagoExtra = {
      text: 'landingEnviar',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.enviarFormularioContactanos();
      },
    };
  }

  async enviarFormularioContactanos(): Promise<void> {
    this.textoMensaje = await this.translateService
      .get('m6v12texto13')
      .toPromise();
    const elemento: HTMLElement = document.getElementById(
      'adGoogle'
    ) as HTMLElement;
    elemento.click();

    if (this.nombre && this.email && this.tema && this.mensaje) {
      let formulario: FormularioContactanos;
      formulario = {};
      formulario.nombre = this.nombre;
      formulario.email = this.email;
      formulario.tema = this.tema;
      formulario.mensaje = this.mensaje;
      if (this.pais !== '' && this.direccion !== '') {
        formulario.pais = this.pais;
        formulario.direccion = this.direccion;
      }
      this.errorMsj = false;
      this.configBotonPagoExtra.enProgreso = true;

      this.cuentaNegocio
        .enviarFormularioContactanos(formulario)
        .subscribe((res) => {
          if (res === 200) {
            this.contactUs = false;
            this.mensajeCorreo = true;
          } else {
          }
          this.configBotonPagoExtra.enProgreso = false;
        });
    } else {
      this.errorMsj = true;
      this.configBotonPagoExtra.enProgreso = false;
      this.textoMensajeError = await this.translateService
        .get('text4')
        .toPromise();
    }
  }
}

export interface CorreoContacto {
  nombre?: string;
  nombreContacto?: string;
  email?: string;
  pais?: string;
  direccion?: string;
  tema?: string;
  mensaje?: string;
}
