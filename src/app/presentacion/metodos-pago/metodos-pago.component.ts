import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

import { environment } from '@env/src/environments/environment';

import { TranslateService } from '@ngx-translate/core';
import { ModalContenido } from '@shared/componentes/dialogo-contenido/dialogo-contenido.component';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionMonedaPicker, ResumenDataMonedaPicker, ValorBase } from '@shared/diseno/modelos/moneda-picker.interface';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  ColorFondoLinea,
  EspesorDelBorde,
  EspesorLineaItem,
  EstiloErrorInput,
  EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { PagoNegocio } from 'dominio/logica-negocio/pago.negocio';
import { CatalogoMetodoPagoModel, PaymentezTransaccion } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosEstadoMetodoPago } from '@core/servicios/remotos/codigos-catalogos/catalogo-metodo-pago.enum';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { MonedaPickerService } from '@core/servicios/generales/moneda-picker.service';
import { CodigosCatalogosMetodosDePago } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogos-metodos-pago.enum';
import { CuentaNegocio, IdiomaNegocio, InternacionalizacionNegocio, TipoMonedaNegocio } from 'dominio/logica-negocio';
import { BotonCompartido, ConfiguracionToast, DialogoCompartido, ItemSelector } from '@shared/diseno/modelos';
import { AccionesSelector, TipoDialogo } from '@shared/diseno/enums';
import { StripeCardElementChangeEvent, StripeCardElementOptions, StripeElementLocale, StripeElementsOptions } from '@stripe/stripe-js';
import { StripeCardComponent, StripeService } from 'ngx-stripe';
import { ColorTextoBoton, TipoBoton, ToastComponent } from '@shared/componentes';
import { CodigosCatalogoOrigenTransaccion, CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos';
import { VariablesGlobales } from '@core/servicios/generales';
import { UsuarioModel } from 'dominio/modelo/entidades';
import { PagoFacturacionEntity } from 'dominio/entidades/catalogos';
import { PagoModel } from 'dominio/modelo';
import { RutasLocales } from '@env/src/app/rutas-locales.enum';
import { CoinPaymentsRatesEntity } from 'dominio/entidades/catalogos/coinpayments-rates.entity';
import { ConvertidorCriptoNegocio } from 'dominio/logica-negocio/convertidor.cripto.negocio';

declare var PaymentCheckout;
const COD_MONEDAS_PAYMENTEZ = ['TIPMON_1', 'TIPMON_2', 'TIPMON_9', 'TIPMON_21', 'TIPMON_22', 'TIPMON_33', 'TIPMON_36', 'TIPMON_103', 'TIPMON_113', 'TIPMON_119', 'TIPMON_114', 'TIPMON_152', 'TIPMON_59', 'TIPMON_62', 'TIPMON_108'];

@Component({
  selector: 'app-metodos-pago',
  templateUrl: './metodos-pago.component.html',
  styleUrls: ['./metodos-pago.component.scss']
})
export class MetodosPagoComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  public configuracionToast: ConfiguracionToast;
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  cardOptions: StripeCardElementOptions = {
    hidePostalCode: true,
    style: {
      base: {
        color: '#03486b',
        fontSmoothing: 'antialiased',
        fontSize: '18px',
        fontFamily: 'Source Sans Pro, sans-serif',
        '::placeholder': {
          color: '#03486b',
        },
        fontWeight: 'bold'
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
      },
    },
  };
  elementsOptions: StripeElementsOptions = {
    locale: 'en',
  };
  public errorMessageTarjeta = '';

  public configuracionAppBar: ConfiguracionAppbarCompartida;
  public confLinea: LineaCompartida;

  public metodoPagoBitcoin: string;
  public idInputCantidadAPagar: string;
  public tipoMonedaLocales: ItemSelector[] = [];
  public coinPaymentsRates: CoinPaymentsRatesEntity[] = [];

  public catalogoTipoMoneda: Array<CatalogoTipoMonedaModel>;

  public confMonedaPickerStripe: ConfiguracionMonedaPicker;
  public confMonedaPickerPaymentez: ConfiguracionMonedaPicker;
  public confMonedaPickerCripto: ConfiguracionMonedaPicker;

  public terminosPagoStripe = false;
  public terminosPagoPaymentez = false;
  public terminosPagoCripto = false;



  public valorBaseAPagar: ValorBase;

  public dataLista: DatosLista;
  public listaMetodoPago: CatalogoMetodoPagoModel[];
  public codigosCatalogos = CodigosCatalogosMetodosDePago;
  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;

  public idiomaSeleccionado: CatalogoIdiomaEntity;
  public inputNombre: InputCompartido;
  public inputTelefono: InputCompartido;
  public inputDireccion: InputCompartido;
  public inputEmail: InputCompartido;

  public mensajePaimentez: string;

  public cuotaMinimaStripe: number;
  public cuotaMinimaPaymentez: number;
  public cuotaMinimaCripto: number;
  public montoPagarPaymentez: number;
  public montoPagarStripe: number;
  private tipoMonedaSelecPaymenetez: string;
  public montoPagarCripto: number;

  private usuario: UsuarioModel;

  private codigoPagoStripe: string;
  private codigoPagoPaymentez: string;
  private codigoPagoCripto: string;


  public pagoForm: FormGroup;
  public criptoForm: FormGroup;

  public confBotonMontoPagoStripe: BotonCompartido;
  public confBotonMontoPagoPaymentez: BotonCompartido;
  public confBotonMontoPagoCripto: BotonCompartido;

  public dataModalMontoStripePago: ModalContenido;
  public dataModalMontoPaymentezPago: ModalContenido;
  public dataModalMontoCriptoPago: ModalContenido;

  public dataModalStripe: ModalContenido;
  public dataModalCripto: ModalContenido;
  public dataModalPaymentez: ModalContenido;

  public configDialoPagoStripe: DialogoCompartido;
  public configDialoPagoPaymentez: DialogoCompartido;
  public configDialoPagoPaymentezError: DialogoCompartido;
  public configDialoPagoCripto: DialogoCompartido;

  public descripcionPagoContinuarStripe: string;
  public mostrarModalPagoContinuarStripe = false;

  public descripcionPagoContinuarPaymentez: string;
  public mostrarModalPagoContinuarPaymentez = false;

  public botonContinuarPerfilesStripe: BotonCompartido;
  public botonContinuarPerfilesPaymentez: BotonCompartido;

  private idiomaPaymentez: string;
  private paymentCheckout;
  public dataModalMensajePaymentezPago: ModalContenido;


  public valorMonedaCripto: any;
  public symbolCripto: string;
  public nombreCripto: string;

  public valorMonedaCriptoBTC: number;

  public mostrarRespuestaCripto = false;

  constructor(
    private fb: FormBuilder,
    private pagoNegocio: PagoNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private generadorId: GeneradorId,
    private translateService: TranslateService,
    private monedaPickerService: MonedaPickerService,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private idiomaNegocio: IdiomaNegocio,
    private stripeService: StripeService,
    private variablesGlobales: VariablesGlobales,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private cuentaNegocio: CuentaNegocio,
    private auth: AngularFireAuth,
    private router: Router,
    private location: Location,
    private convertidorCriptoNegocio: ConvertidorCriptoNegocio,
  ) {
    this.metodoPagoBitcoin = CodigosCatalogosMetodosDePago.CRIPTOMONEDA;
    this.idInputCantidadAPagar = 'KJ21-0982jjs';
    this.catalogoTipoMoneda = [];
  }

  ngOnInit(): void {
    this.obtenerIdioma();
    this.prepararLista();
    this.obtenerCatalogoMetodosPago();
    this.funcionRedondear();

    this.configurarBotonStripe();
    this.confBtnMenuPerfilesStripe();
    this.confBtnMenuPerfilesPaymentez();

    this.configurarBotonPaymentez();
    this.configurarBotonCripto();
    this.obtenerCoinPaymentsRates();

    this.configurarAppBar();
    this.configurarToast();
    this.configurarDialogoContenido();
    this.configurarMonedaPickerStripe();
    this.configurarMonedaPickerPaymentez();
    this.configurarMonedaPickerCripto();
    this.configurarLinea();
    this.confDialogosMontoPagar();
    this.configurarTextoPaymentz().then();
    this.mostrarRespuestaCripto = !!JSON.parse(localStorage.getItem('respCoiPaiments'));

    this.pagoForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(2)]],
      telefono: ['', Validators.required],
      direccion: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      checkBox: [],
    });
    this.criptoForm = this.fb.group({
      currency2: ['', [Validators.required]],
      cantidad: [18, [Validators.required]],
    });
    this.criptoForm.valueChanges.subscribe(({ currency2, cantidad }) => {
      console.log('datos', currency2.currency);
      console.log('can', cantidad);

      if ( currency2.currency === 'ETH' && cantidad >= 18 && cantidad <= 29) {
        this.toast.abrirToast('La cantidad minima con esta criptomoneda es de 30 euros traducir');
        return;
      }
      if (cantidad >= 18 && currency2) {
        this.convertirMontoCripto(cantidad, currency2.idCripto, currency2.currency, currency2.name);
      } else {
        console.log('Cantidad menor = a 18  no hay currecy');
      }
    });
    this.inicializarInputsCompartidos();
    this.paymentCheckout = new PaymentCheckout.modal({
      client_app_code: environment.clientAppCodePaymentez,
      client_app_key: environment.clientAppKeyPaymentez,
      locale: this.idiomaPaymentez,
      env_mode: environment.envModePaymentez,
      onOpen: () => {
        console.log('modal open');
      },
      onClose: () => {
        console.log('modal closed');
        this.configurarTextoPaymentz(true).then();
      },
      onResponse: (response: PaymentezTransaccion) => {
        console.log('modal response', response);
        const { transaction: { amount, id, authorization_code, status } } = response;
        if (status === 'success') {
          const pagoNoConfirmado = false;
          this.activarCuentaPaymentez(amount, id, pagoNoConfirmado, authorization_code);
        } else {
          const pagoNoConfirmado = true;
          console.log('error en pago');
          this.activarCuentaPaymentezError(amount, id, pagoNoConfirmado);
        }
      }
    });
  }


  private configurarBotonStripe(): void {
    this.confBotonMontoPagoStripe = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        console.log('ejecutar stripe');
        this.confirmarMontoPagoStripe().then();
      },
    };
  }

  private convertirMontoCripto(cantidad: string, criptoId: number, symbold: string, nombreCripto: string): void {
    const monto = parseFloat(cantidad);
    console.log(criptoId, symbold, nombreCripto);
    this.valorMonedaCripto = null;
    this.symbolCripto = null;
    this.nombreCripto = null;
    this.dataModalMontoCriptoPago.bloqueado = true;
    this.confBotonMontoPagoCripto.enProgreso = true;
    setTimeout(() => {
      this.convertidorCriptoNegocio.convertirMonedaACripto(monto, criptoId, 'EUR')
        .subscribe((amount) => {
          console.log('respuesta convertir monto cripto', amount);
          this.valorMonedaCripto = amount;
          symbold === 'BTC' ? this.valorMonedaCriptoBTC = this.valorMonedaCripto  : this.convertirMontoCriptoBTC(amount, 1, symbold);
          this.symbolCripto = symbold;
          this.nombreCripto = nombreCripto;
          this.dataModalMontoCriptoPago.bloqueado = false;
          this.confBotonMontoPagoCripto.enProgreso = false;
          this.dataModalMontoCriptoPago.error = false;
          this.dataModalMontoCriptoPago.errorContenido = '';
        }, (error) => {
          console.log('error convertir monto cripto', error);
          this.dataModalMontoCriptoPago.bloqueado = false;
          this.confBotonMontoPagoCripto.enProgreso = false;
          this.dataModalMontoCriptoPago.error = true;
          this.dataModalMontoCriptoPago.errorContenido = 'text37';
          this.toast.abrirToast('text37');
        });
    }, 3000);
  }

  private convertirMontoCriptoBTC(cantidad: number, criptoId: number, symbol: string): void {
    console.log('final btc');
    console.log(criptoId, cantidad);
    this.valorMonedaCriptoBTC = null;
    this.dataModalMontoCriptoPago.bloqueado = true;
    this.confBotonMontoPagoCripto.enProgreso = true;

    this.convertidorCriptoNegocio.convertirMonedaACripto(cantidad, criptoId, symbol)
      .subscribe((amount) => {
        console.log('respuesta convertir monto cripto2:', amount);
        this.valorMonedaCriptoBTC = amount;
        this.dataModalMontoCriptoPago.bloqueado = false;
        this.confBotonMontoPagoCripto.enProgreso = false;
        this.dataModalMontoCriptoPago.error = false;
        this.dataModalMontoCriptoPago.errorContenido = '';
      }, (error) => {
        console.log('error convertir monto cripto', error);
        this.dataModalMontoCriptoPago.bloqueado = false;
        this.confBotonMontoPagoCripto.enProgreso = false;
        this.dataModalMontoCriptoPago.error = true;
        this.dataModalMontoCriptoPago.errorContenido = 'text37';
        this.toast.abrirToast('text37');
      });

  }

  private configurarBotonPaymentez(): void {
    this.confBotonMontoPagoPaymentez = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        console.log('ejecutar paymentez');
        this.confirmarMontoPagoPaymentez().then();
      },
    };
  }

  private configurarBotonCripto(): void {
    this.confBotonMontoPagoCripto = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        console.log('ejecutar cripto');
        this.confirmarMontoPagoCripto().then();
      },
    };
  }

  private async confirmarMontoPagoStripe(): Promise<void> {
    const montoIngresadoStripe = parseFloat(this.confMonedaPickerStripe.inputCantidadMoneda.valor.valorFormateado);
    const codigoMonedaStripe = this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado.codigo;
    const monedaSeleccionadaStripe = this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado.auxiliar;

    this.dataModalMontoStripePago.bloqueado = true;
    this.confBotonMontoPagoStripe.enProgreso = true;
    this.dataModalMontoStripePago.error = false;
    this.dataModalMontoStripePago.errorContenido = '';

    if (!(montoIngresadoStripe > 0)) {
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalMontoStripePago.error = true;
      this.dataModalMontoStripePago.errorContenido = 'text40';
      return;
    }
    this.montoPagarStripe = montoIngresadoStripe;

    if (!(codigoMonedaStripe && monedaSeleccionadaStripe)) {
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalMontoStripePago.error = true;
      this.dataModalMontoStripePago.errorContenido = 'text41';
      return;
    }

    try {
      this.cuotaMinimaStripe = await this.cuotaMinimaValor();
      const montoPagar = await this.convertirMonto(montoIngresadoStripe, monedaSeleccionadaStripe);
      if (montoPagar < this.cuotaMinimaStripe) {
        const cuotaMinimaEnMonedaUsuario = await this.cuotaMinimaValorMonedaUsuario(monedaSeleccionadaStripe);
        this.dataModalMontoStripePago.bloqueado = false;
        this.confBotonMontoPagoStripe.enProgreso = false;
        this.dataModalMontoStripePago.error = true;
        const textoError = await this.internacionalizacionNegocio.obtenerTextoLlave('text64');
        this.dataModalMontoStripePago.errorContenido = `${textoError} ${cuotaMinimaEnMonedaUsuario} ${monedaSeleccionadaStripe}`;
        return;
      }
      if (!this.terminosPagoStripe) {
        this.dataModalMontoStripePago.bloqueado = false;
        this.confBotonMontoPagoStripe.enProgreso = false;
        this.dataModalMontoStripePago.error = true;
        this.dataModalMontoStripePago.errorContenido = 'text101';
        return;
      }

      this.usuario = this.cuentaNegocio.obtenerUsuarioDelSessionStorage();
      this.usuario.transacciones = [];
      this.usuario.transacciones.push({
        moneda: {
          codNombre: CodigosCatalogoTipoMoneda.USD
        },
        monto: this.cuotaMinimaStripe,
        origen: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
        },
        destino: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
        }
      });
      if (
        montoIngresadoStripe > this.cuotaMinimaStripe &&
        // tslint:disable-next-line: no-string-literal
        Math['round10'](montoIngresadoStripe - this.cuotaMinimaStripe, -2) >= 0.1
      ) {
        this.usuario.transacciones.push({
          moneda: {
            codNombre: CodigosCatalogoTipoMoneda.USD
          },
          // tslint:disable-next-line: no-string-literal
          monto: Math['round10'](montoIngresadoStripe - this.cuotaMinimaStripe, -2),
          origen: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
          },
          destino: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
          }
        });
      }
      this.usuario.monedaRegistro = {
        codNombre: monedaSeleccionadaStripe
      };
      this.cuentaNegocio.guardarUsuarioEnSessionStorage(this.usuario);
      this.dataModalMontoStripePago.abierto = false;
      this.dataModalMontoStripePago.bloqueado = false;
      this.confBotonMontoPagoStripe.enProgreso = false;
      this.dataModalStripe.abierto = true;

    } catch (e) {
      console.log(e);
      this.toast.abrirToast('text37');
    }
  }

  public pagarStripe(): void {
    if (!this.pagoForm.valid) {
      this.toast.abrirToast('text4');
      return;
    }
    const datosPago: PagoFacturacionEntity = {
      nombres: this.pagoForm.value.nombre,
      telefono: this.pagoForm.value.telefono,
      direccion: this.pagoForm.value.direccion,
      email: this.pagoForm.value.email,
    };
    this.toast.abrirToast('', true);
    this.dataModalStripe.abierto = false;
    this.cuentaNegocio.crearCuenta(this.codigoPagoStripe, datosPago)
      .subscribe((pagoModel: PagoModel) => {
        this.toast.cerrarToast();
        this.toast.abrirToast('text21', true);
        this.stripeService.confirmCardPayment(pagoModel.idPago, {
          payment_method: {
            card: this.card.element,
            billing_details: {
              name: datosPago.nombres,
              email: datosPago.email,
            },
          },
        }).subscribe((result) => {
          this.toast.cerrarToast();
          if (result.error) {
            this.toast.abrirToast('text98');
            this.activarCuentaStripe(pagoModel.idTransaccion, true);
          } else {
            result.paymentIntent.status === 'succeeded' ?
              this.activarCuentaStripe(pagoModel.idTransaccion) :
              this.toast.abrirToast('text37');
          }
        });
      }, (error) => {
        this.toast.cerrarToast();
        this.toast.abrirToast('text37');
        setTimeout(() => {
          window.location.reload();
          this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO).then();
        }, 3000);
      });

  }

  private activarCuentaStripe(idTransaccion: string, pagoNoConfirmado: boolean = false): void {
    this.toast.abrirToast('', true);
    this.cuentaNegocio.activarCuenta(idTransaccion, pagoNoConfirmado).subscribe(async (resp) => {
      this.configurarDialogoPagoStripe().then();
      this.cuentaNegocio.eliminarSessionStorage();
      const dataFirebase = await this.auth.signInAnonymously();
      if (!dataFirebase || !dataFirebase.user || !dataFirebase.user.uid) {
        throw new Error('');
      }
      this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(dataFirebase.user.uid);
      this.toast.cerrarToast();
      this.mostrarModalPagoContinuarStripe = true;
      this.location.replaceState('/');
    }, (error) => {
      this.mostrarModalPagoContinuarStripe = false;
      this.toast.abrirToast('text37');
    });

  }

  private async configurarDialogoPagoStripe(): Promise<void> {
    this.configDialoPagoStripe = {
      mostrarDialogo: false,
      descripcion: '',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPagoStripe.mostrarDialogo = false;
            this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
          }
        },
      ]
    };
    const firstDescri = await this.translateService.get('m5v9texto10').toPromise();
    const descripcion = `${firstDescri} ${this.montoPagarStripe} ${this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado.auxiliar}`;
    this.configDialoPagoStripe.descripcion = descripcion;
    this.descripcionPagoContinuarStripe = descripcion;
  }

  private async configurarDialogoPagoPaymentez(): Promise<void> {
    this.configDialoPagoPaymentez = {
      mostrarDialogo: false,
      descripcion: '',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPagoPaymentez.mostrarDialogo = false;
            this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
          }
        },
      ]
    };
    const firstDescri = await this.translateService.get('m5v9texto10').toPromise();
    const descripcion = `${firstDescri} ${this.montoPagarPaymentez} ${CodigosCatalogoTipoMoneda.USD}`;
    this.configDialoPagoPaymentez.descripcion = descripcion;
    this.descripcionPagoContinuarPaymentez = descripcion;
  }

  private configurarDialogoPagoErrorPaymentez(): void {
    this.configDialoPagoPaymentezError = {
      mostrarDialogo: false,
      descripcion: 'text37',
      tipo: TipoDialogo.MULTIPLE_ACCION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v9texto13',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AZUL,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoPagoPaymentezError.mostrarDialogo = false;
            this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO).then();
          }
        },
      ]
    };
  }

  private pagarCripto(): void {
    this.dataModalMontoCriptoPago.abierto = false;
    this.toast.abrirToast('', true);
    this.cuentaNegocio.crearCuentaCripto(this.codigoPagoCripto)
      .subscribe((resp) => {
        console.log(resp);
        localStorage.setItem('respCoiPaiments', JSON.stringify(resp.coinpayments));
        this.cuentaNegocio.activarCuentaCripto(resp);
        this.toast.cerrarToast();
        this.mostrarRespuestaCripto = true;

      }, (error) => {
        this.mostrarRespuestaCripto = false;
        console.log(error);
        this.toast.cerrarToast();
        this.toast.abrirToast('text37');
      });
  }

  private async confirmarMontoPagoCripto(): Promise<void> {
    const { cantidad, currency2 } = this.criptoForm.value;
    console.log(cantidad, currency2);
    this.dataModalMontoCriptoPago.bloqueado = true;
    this.confBotonMontoPagoCripto.enProgreso = true;
    this.dataModalMontoCriptoPago.error = false;
    this.dataModalMontoCriptoPago.errorContenido = '';
    if (!(cantidad > 0)) {
      this.dataModalMontoCriptoPago.bloqueado = false;
      this.confBotonMontoPagoCripto.enProgreso = false;
      this.dataModalMontoCriptoPago.error = true;
      this.dataModalMontoCriptoPago.errorContenido = 'text40';
      return;
    }
    if (currency2 === '') {
      this.dataModalMontoCriptoPago.bloqueado = false;
      this.confBotonMontoPagoCripto.enProgreso = false;
      this.dataModalMontoCriptoPago.error = true;
      this.dataModalMontoCriptoPago.errorContenido = 'Seleccione una criptomoneda traducir';
      return;
    }
    try {
      this.cuotaMinimaCripto = await this.cuotaMinimaValor();
      const montoPagar = await this.convertirMonto(cantidad, CodigosCatalogoTipoMoneda.EUR);

      if (montoPagar < this.cuotaMinimaCripto) {
        this.dataModalMontoCriptoPago.bloqueado = false;
        this.confBotonMontoPagoCripto.enProgreso = false;
        this.dataModalMontoCriptoPago.error = true;
        const textoError = await this.internacionalizacionNegocio.obtenerTextoLlave('text64');
        this.dataModalMontoCriptoPago.errorContenido = `${textoError} 18 EUR`;
        return;
      }
      if (!this.terminosPagoCripto) {
        this.dataModalMontoCriptoPago.bloqueado = false;
        this.confBotonMontoPagoCripto.enProgreso = false;
        this.dataModalMontoCriptoPago.error = true;
        this.dataModalMontoCriptoPago.errorContenido = 'text101';
        return;
      }

      if (currency2.currency === 'ETH' && cantidad >= 18 && cantidad <= 29) {
        this.dataModalMontoCriptoPago.bloqueado = false;
        this.confBotonMontoPagoCripto.enProgreso = false;
        this.dataModalMontoCriptoPago.error = true;
        this.dataModalMontoCriptoPago.errorContenido = 'La cantidad minima para esta criptomoneda es de 30 euros traducir';
        return;
      }

      this.usuario = this.cuentaNegocio.obtenerUsuarioDelSessionStorage();
      this.usuario.transacciones = [];
      this.usuario.transacciones.push({
        moneda: {
          codNombre: CodigosCatalogoTipoMoneda.USD
        },
        monto: this.cuotaMinimaCripto,
        origen: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
        },
        destino: {
          codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
        }
      });
      if (
        cantidad > this.cuotaMinimaCripto &&
        // tslint:disable-next-line: no-string-literal
        Math['round10'](cantidad - this.cuotaMinimaCripto, -2) >= 0.1
      ) {
        console.log('entro');
        this.usuario.transacciones.push({
          moneda: {
            codNombre: CodigosCatalogoTipoMoneda.USD
          },
          // tslint:disable-next-line: no-string-literal
          monto: Math['round10'](cantidad - this.cuotaMinimaCripto, -2),
          origen: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
          },
          destino: {
            codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
          }
        });
      }
      this.usuario.monedaRegistro = {
        codNombre: CodigosCatalogoTipoMoneda.EUR
      };

      this.usuario.pagoCripto = {
        nombreCripto: this.nombreCripto,
        cantidad: this.valorMonedaCriptoBTC,
        simboloCripto: this.symbolCripto
      };

      console.log('usuario', this.usuario);
      this.cuentaNegocio.guardarUsuarioEnSessionStorage(this.usuario);

      this.dataModalMontoCriptoPago.bloqueado = false;
      this.confBotonMontoPagoCripto.enProgreso = false;
      this.dataModalMontoCriptoPago.error = false;
      this.dataModalMontoCriptoPago.errorContenido = '';

      this.pagarCripto();

    } catch (e) {
      console.log(e);
      this.toast.abrirToast('text37');
    }


  }

  private async confirmarMontoPagoPaymentez(): Promise<void> {
    const montoIngresadoPaymentez = parseFloat(this.confMonedaPickerPaymentez.inputCantidadMoneda.valor.valorFormateado);
    const codigoMonedaPaymentez = this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado.codigo;
    const monedaSeleccionadaPaymentez = this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado.auxiliar;

    this.dataModalMontoPaymentezPago.bloqueado = true;
    this.confBotonMontoPagoPaymentez.enProgreso = true;
    this.dataModalMontoPaymentezPago.error = false;
    this.dataModalMontoPaymentezPago.errorContenido = '';

    if (!(montoIngresadoPaymentez > 0)) {
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;
      this.dataModalMontoPaymentezPago.error = true;
      this.dataModalMontoPaymentezPago.errorContenido = 'text40';
      return;
    }

    if (!(codigoMonedaPaymentez && monedaSeleccionadaPaymentez)) {
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;
      this.dataModalMontoPaymentezPago.error = true;
      this.dataModalMontoPaymentezPago.errorContenido = 'text41';
      return;
    }
    this.tipoMonedaSelecPaymenetez = monedaSeleccionadaPaymentez;
    try {
      this.cuotaMinimaPaymentez = await this.cuotaMinimaValor();
      const montoPagar = await this.convertirMonto(montoIngresadoPaymentez, monedaSeleccionadaPaymentez);
      if (montoPagar < this.cuotaMinimaPaymentez) {
        const cuotaMinimaEnMonedaUsuario = await this.cuotaMinimaValorMonedaUsuario(monedaSeleccionadaPaymentez);
        this.dataModalMontoPaymentezPago.bloqueado = false;
        this.confBotonMontoPagoPaymentez.enProgreso = false;
        this.dataModalMontoPaymentezPago.error = true;
        const textoError = await this.internacionalizacionNegocio.obtenerTextoLlave('text64');
        this.dataModalMontoPaymentezPago.errorContenido = `${textoError} ${cuotaMinimaEnMonedaUsuario} ${monedaSeleccionadaPaymentez}`;
        return;
      }
      this.montoPagarPaymentez = montoPagar;
      if (!this.terminosPagoPaymentez) {
        this.dataModalMontoPaymentezPago.bloqueado = false;
        this.confBotonMontoPagoPaymentez.enProgreso = false;
        this.dataModalMontoPaymentezPago.error = true;
        this.dataModalMontoPaymentezPago.errorContenido = 'text101';
        return;
      }
      this.dataModalMontoPaymentezPago.abierto = false;
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;

      this.dataModalMensajePaymentezPago.abierto = true;
      this.dataModalMensajePaymentezPago.bloqueado = true;

      setTimeout(() => {
        console.log('time');
        this.dataModalMensajePaymentezPago.abierto = false;
        this.dataModalMensajePaymentezPago.bloqueado = false;
      }, 8000);
      setTimeout(() => {
        this.cuentaNegocio.crearCuentaPaymentez().subscribe((resp: any) => {
          this.paymentCheckout.open({
            user_id: resp.usuario._id,
            user_email: resp.usuario.email,
            user_phone: '',
            order_description: 'Suscripción Gazeloook',
            order_amount: this.montoPagarPaymentez,
            order_vat: 0,
            order_reference: '#234323411',
            order_installments_type: -1,
            order_taxable_amount: 0,
            order_tax_percentage: 0
          });
        }, (error) => {
          this.dataModalMensajePaymentezPago.abierto = false;
          this.dataModalMensajePaymentezPago.bloqueado = false;
          this.toast.abrirToast('text37');
          setTimeout(() => {
            window.location.reload();
            this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO);
          }, 3000);
        });
      }, 5000);
    } catch (e) {
      console.log(e);
      this.toast.abrirToast('text37');
    }
  }

  private activarCuentaPaymentez(
    amount: number,
    transactionId: string,
    pagoNoConfirmado: boolean,
    authorizationCode: string): void {
    this.toast.abrirToast('', true);
    this.cuentaNegocio.activarCuentaPaymentez(
      this.codigoPagoPaymentez,
      amount,
      transactionId,
      pagoNoConfirmado,
      this.tipoMonedaSelecPaymenetez,
      this.cuotaMinimaPaymentez,
      authorizationCode
    ).subscribe(async (resp: any) => {
      console.log(resp);
      this.configurarDialogoPagoPaymentez().then();
      this.cuentaNegocio.eliminarSessionStorage();
      const dataFirebase = await this.auth.signInAnonymously();
      if (!dataFirebase || !dataFirebase.user || !dataFirebase.user.uid) {
        throw new Error('');
      }
      this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(dataFirebase.user.uid);
      this.toast.cerrarToast();
      this.mostrarModalPagoContinuarPaymentez = true;
      this.location.replaceState('/');
      // this.cuentaNegocio.removerIdFotoDniSessionStorage();
    }, (error) => {
      console.log(error);
      this.toast.abrirToast('text37');
      setTimeout(() => {
        window.location.reload();
        this.router.navigateByUrl(RutasLocales.ENRUTADOR_REGISTRO).then();
      }, 3000);
    });

  }

  private activarCuentaPaymentezError(amount: number, transactionId: string, pagoNoConfirmado: boolean): void {
    this.configurarDialogoPagoErrorPaymentez();
    this.toast.abrirToast('', true);
    this.cuentaNegocio.activarCuentaPaymentez(
      this.codigoPagoPaymentez,
      amount,
      transactionId,
      pagoNoConfirmado,
      this.tipoMonedaSelecPaymenetez,
      this.cuotaMinimaPaymentez)
      .subscribe((resp) => {
        console.log(resp);
        this.toast.cerrarToast();
        this.configDialoPagoPaymentezError.mostrarDialogo = true;
      },
        (error) => {
          console.log(error);
          this.toast.cerrarToast();
          this.configurarTextoPaymentz(false).then();
          this.configDialoPagoPaymentezError.mostrarDialogo = true;
        });
  }

  public navegarMetodoPagoStripe(metodoPago: CatalogoMetodoPagoModel): void {
    // console.log('navegarMetodoPago stripe', metodoPago);
    metodoPago.codigo === CodigosCatalogosMetodosDePago.TARJETA ?
      this.dataModalMontoStripePago.abierto = true :
      this.dataModalMontoStripePago.abierto = false;
    this.codigoPagoStripe = metodoPago.codigo;
  }

  public navegarMetodoPagoPaymentez(metodoPago: CatalogoMetodoPagoModel): void {
    // console.log('navegarMetodoPago paymentez', metodoPago);
    metodoPago.codigo === CodigosCatalogosMetodosDePago.PAYMENTEZ ?
      this.dataModalMontoPaymentezPago.abierto = true :
      this.dataModalMontoPaymentezPago.abierto = false;
    this.codigoPagoPaymentez = metodoPago.codigo;
  }

  public navegarMetodoPagoPaymentez2(metodoPago: CatalogoMetodoPagoModel): void {
    // console.log('navegarMetodoPago paymentez', metodoPago);
    metodoPago.codigo === CodigosCatalogosMetodosDePago.PAYMENTEZ2 ?
      this.dataModalMontoPaymentezPago.abierto = true :
      this.dataModalMontoPaymentezPago.abierto = false;
    this.codigoPagoPaymentez = metodoPago.codigo;
  }

  public navegarMetodoPagoCripto(metodoPago: CatalogoMetodoPagoModel): void {
    // console.log('navegarMetodoPago cripto', metodoPago);
    metodoPago.codigo === CodigosCatalogosMetodosDePago.CRIPTOMONEDA ?
      this.dataModalMontoCriptoPago.abierto = true :
      this.dataModalMontoCriptoPago.abierto = false;
    this.codigoPagoCripto = metodoPago.codigo;
  }

  async configurarTextoPaymentz(estado: boolean = false): Promise<void> {
    const texto = 'text107';
    const texto2 = 'text37';
    estado ?
      this.mensajePaimentez = await this.translateService.get(texto2).toPromise() :
      this.mensajePaimentez = await this.translateService.get(texto).toPromise();
  }

  private configurarDialogoContenido(): void {
    this.dataModalStripe = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagotarjeta'
    };
    this.dataModalPaymentez = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagopaymentez'
    };
    this.dataModalCripto = {
      titulo: 'PAGAR CON CRIPTOMONEDAS Traducir',
      abierto: false,
      bloqueado: false,
      id: 'pagocoinpayments'
    };
  }

  private confDialogosMontoPagar(): void {
    this.dataModalMontoStripePago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'montopagostripe'
    };
    this.dataModalMontoPaymentezPago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'montopaymentez'
    };
    this.dataModalMontoCriptoPago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'montopagocripto'
    };
    this.dataModalMensajePaymentezPago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'mensajepaymentez'
    };
  }

  public eventoModalStripe(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto-stripe') {
        if (!this.dataModalMontoStripePago.bloqueado) {
          this.dataModalMontoStripePago.abierto = false;
        }
      }
    });
  }

  public eventoModalPaymentez(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto-paymentez') {
        if (!this.dataModalMontoPaymentezPago.bloqueado) {
          this.dataModalMontoPaymentezPago.abierto = false;
        }
      }
    });
  }

  public eventoModalCripto(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto-cripto') {
        if (!this.dataModalMontoCriptoPago.bloqueado) {
          this.dataModalMontoCriptoPago.abierto = false;
        }
      }
    });
  }

  private obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago()
      .subscribe((resp: CatalogoMetodoPagoModel[]) => {
        this.dataLista.cargando = false;
        this.listaMetodoPago = resp;
      }, error => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      });
  }

  private prepararLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES
    };
  }

  // Metodos del selector de moneda
  seleccionarTipoMonedaStripe(item: ItemSelector): void {
    // Ocultar selector
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPickerStripe.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPickerStripe.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado = item.auxiliar || '';
  }

  seleccionarTipoMonedaPaymentez(item: ItemSelector): void {
    // Ocultar selector
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPickerPaymentez.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado = item.auxiliar || '';
  }

  // Metodos del selector de moneda
  private eventoEnSelectorDeTipoMonedaStripe(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMonedaStripe();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMonedaStripe(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMonedaStripe();
        break;
      default:
        break;
    }
  }

  private eventoEnSelectorDeTipoMonedaPaymentez(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMonedaPaymentez();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMonedaPaymentez(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMonedaPaymentez();
        break;
      default:
        break;
    }
  }

  private abrirSelectorMonedaStripe(): void {
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMonedaStripe();
  }

  private abrirSelectorMonedaPaymentez(): void {
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMonedaPaymentez();
  }

  private obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    const valorEstimado = (this.valorBaseAPagar && this.valorBaseAPagar.valorNeto) ? parseInt(this.valorBaseAPagar.valorNeto, 0) : 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    };

    return {
      valorEstimado: this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(valorEstimado),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda)
    };
  }

  private configurarMonedaPickerStripe(): void {
    const dataMoneda: ResumenDataMonedaPicker = this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerStripe = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0'
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15'
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO
          }
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) => this.eventoEnSelectorDeTipoMonedaStripe(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: ''
        }
      }
    };
  }

  private configurarMonedaPickerCripto(): void {
    const dataMoneda: ResumenDataMonedaPicker = this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerCripto = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0'
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15'
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO
          }
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) => {
        },
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: ''
        }
      }
    };
  }

  private configurarMonedaPickerPaymentez(): void {
    const dataMoneda: ResumenDataMonedaPicker = this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerPaymentez = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0'
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15'
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO
          }
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) => this.eventoEnSelectorDeTipoMonedaPaymentez(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: ''
        }
      }
    };
  }

  private obtenerCoinPaymentsRates(): void {
    this.cuentaNegocio.obtenerCoinPaymentsRates()
      .subscribe((resp) => {
        this.coinPaymentsRates = resp;
      },
        (error) => console.log(error));
  }

  private inicializarDataCatalogoMonedaPaymentez(): void {
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(elegibles => {
      this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
      const tipoMonedaPaymentez = [];
      this.tipoMonedaLocales = elegibles;
      this.tipoMonedaLocales.map((item) => {
        if (COD_MONEDAS_PAYMENTEZ.includes(item.codigo)) {
          tipoMonedaPaymentez.push(item);
        }
      });
      this.confMonedaPickerPaymentez.selectorTipoMoneda.elegibles = tipoMonedaPaymentez;
      this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
      this.confMonedaPickerPaymentez.selectorTipoMoneda.error.mostrar = false;
    }, error => {
      this.confMonedaPickerPaymentez.selectorTipoMoneda.elegibles = [];
      this.confMonedaPickerPaymentez.selectorTipoMoneda.error.llaveTexto = 'text31';
      this.confMonedaPickerPaymentez.selectorTipoMoneda.error.mostrar = true;
      this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
    });
  }

  private inicializarDataCatalogoMonedaStripe(): void {
    this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(elegibles => {
      this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
      this.tipoMonedaLocales = elegibles;
      this.confMonedaPickerStripe.selectorTipoMoneda.elegibles = elegibles;
      this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
      this.confMonedaPickerStripe.selectorTipoMoneda.error.mostrar = false;
    }, error => {
      this.confMonedaPickerStripe.selectorTipoMoneda.elegibles = [];
      this.confMonedaPickerStripe.selectorTipoMoneda.error.llaveTexto = 'text31';
      this.confMonedaPickerStripe.selectorTipoMoneda.error.mostrar = true;
      this.confMonedaPickerStripe.selectorTipoMoneda.mostrarLoader = false;
    });
  }

  public obtenerClasesInputCantidadMoneda(): any {
    const clases = {};
    clases['input-cantidad'] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.colorFondo.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.colorTexto.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.estiloDelTexto.toString()] = true;
    clases[this.confMonedaPickerStripe.inputCantidadMoneda.tamanoDelTexto.toString()] = true;
    if (this.confMonedaPickerStripe.inputCantidadMoneda.estiloBorde) {
      clases[this.confMonedaPickerStripe.inputCantidadMoneda.estiloBorde.toString()] = true;
    }
    return clases;
  }

  private configurarToast(): void {
    this.configuracionToast = {
      cerrarClickOutside: false,
      bloquearPantalla: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: ''
    };
  }

  private confBtnMenuPerfilesStripe(): void {
    this.botonContinuarPerfilesStripe = {
      text: 'm2v9texto13',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.mostrarModalPagoContinuarStripe = false;
        window.localStorage.removeItem('mensajePago');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES).then();
      }
    };
  }

  private confBtnMenuPerfilesPaymentez(): void {
    this.botonContinuarPerfilesPaymentez = {
      text: 'm2v9texto13',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.mostrarModalPagoContinuarPaymentez = false;
        window.localStorage.removeItem('mensajePago');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES).then();
      }
    };
  }

  // Config AppBar
  private configurarAppBar(): void {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: false
        },
        mostrarDivBack: {
          icono: true,
          texto: false,
        },
        mostrarTextoHome: false,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      }
    };
  }

  private inicializarInputsCompartidos(): void {
    this.inputNombre = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text66', data: this.pagoForm.controls.nombre,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    };
    this.inputTelefono = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO,
        estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text67',
      data: this.pagoForm.controls.telefono,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    };
    this.inputDireccion = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO, estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text68',
      data: this.pagoForm.controls.direccion,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    };
    this.inputEmail = {
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.ROJO, estiloInput: EstiloInput.DEFECTO
      },
      placeholder: 'text69',
      data: this.pagoForm.controls.email,
      id: this.generadorId.generarIdConSemilla(),
      bloquearCopy: false
    };
  }

  //  conf linea verde
  private configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false
    };
  }

  private funcionRedondear(): void {
    // tslint:disable-next-line: no-string-literal
    if (!Math['round10']) {
      // tslint:disable-next-line: no-string-literal
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp);
      };
    }
  }

  public checkValueTerminosPagoStripe(event: any): void {
  }

  public checkValueTerminosPagoPaymentez(event: any): void {
  }

  public checkValueTerminosPagoCripto(event: any): void {
  }


  private async convertirMonto(monto, tipoMoneda): Promise<number> {
    const montoIngresado = monto;
    if (tipoMoneda as CodigosCatalogoTipoMoneda === CodigosCatalogoTipoMoneda.USD) {
      return montoIngresado;
    } else {
      // tslint:disable-next-line: no-string-literal
      return Math['round10'](
        await this.tipoMonedaNegocio.convertirMontoEntreMonedas(
          montoIngresado,
          tipoMoneda as CodigosCatalogoTipoMoneda,
          CodigosCatalogoTipoMoneda.USD
        ).toPromise(),
        -2
      );
    }
  }


  private async cuotaMinimaValor(): Promise<number> {
    // tslint:disable-next-line: no-string-literal
    return Math['round10'](
      await this.tipoMonedaNegocio.convertirMontoEntreMonedas(
        this.variablesGlobales.montoBaseSuspcripcion,
        this.variablesGlobales.tipoDeMonedaBase,
        CodigosCatalogoTipoMoneda.USD
      ).toPromise(),
      -2
    );
  }

  private async cuotaMinimaValorMonedaUsuario(moneda): Promise<number> {
    // tslint:disable-next-line: no-string-literal
    return Math['round10'](
      await this.tipoMonedaNegocio.convertirMontoEntreMonedas(
        this.variablesGlobales.montoBaseSuspcripcion,
        this.variablesGlobales.tipoDeMonedaBase,
        moneda as CodigosCatalogoTipoMoneda
      ).toPromise(),
      -2
    );
  }

  public onChangeStripeCardEvent(ev: StripeCardElementChangeEvent): void {
    if (ev.error) {
      this.errorMessageTarjeta = ev.error.message;
    } else {
      this.errorMessageTarjeta = '';
    }
  }

  private obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (this.idiomaSeleccionado) {
      this.elementsOptions.locale = this.idiomaSeleccionado.codNombre as StripeElementLocale;
      this.idiomaPaymentez = this.idiomaSeleccionado?.codNombre === 'en'
        || this.idiomaSeleccionado?.codNombre === 'es'
        || this.idiomaSeleccionado?.codNombre === 'pt'
        ? this.idiomaSeleccionado?.codNombre
        : 'en';
    }
  }

  public irDocumentosLegales(): void {
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `https://d3ubht94yroq8c.cloudfront.net/condiciones-pago/condiciones-pago-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    }));
  }

  ajustarDecimales(type: any, value: any, exp: any): any {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

}
