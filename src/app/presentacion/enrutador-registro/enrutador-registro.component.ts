import { Location } from '@angular/common';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionEnrutadorNavegacion } from 'src/app/compartido/componentes/enrutador-navegacion/enrutador-navegacion.component';
import { ConfiguracionPortadaGif, PortadaGifComponent } from 'src/app/compartido/componentes/portada-gif/portada-gif.component';
import { CuentaNegocio } from 'src/app/dominio/logica-negocio/cuenta.negocio';
import { UsuarioModel } from 'src/app/dominio/modelo/entidades/usuario.model';
import { CamaraService } from 'src/app/nucleo/servicios/generales/camara.service';
import { EnrutadorService } from 'src/app/nucleo/servicios/generales/enrutador/enrutador.service';
import { VariablesGlobales } from 'src/app/nucleo/servicios/generales/variables-globales.service';
import { MetodosSessionStorageService } from 'src/app/nucleo/util/metodos-session-storage.service';
import { ComponentesCreados, Distribucion, EstadoEnrutador, InformacionEnrutador, ItemDistribucion, ItemParam, TipoDeNavegacion, UbicacionDelComponente } from '../enrutador/enrutador.component';
import { MenuPerfilesComponent } from '../menu-perfiles/menu-perfiles.component';

@Component({
  selector: 'app-enrutador-registro',
  templateUrl: './enrutador-registro.component.html',
  styleUrls: ['./enrutador-registro.component.scss']
})
export class EnrutadorRegistroComponent implements OnInit {

  @ViewChild('tercio_izquierda', { read: ViewContainerRef }) tercio_izquierda: ViewContainerRef
  @ViewChild("tercio_central", { read: ViewContainerRef }) tercio_central: ViewContainerRef
  @ViewChild('tercio_derecha', { read: ViewContainerRef }) tercio_derecha: ViewContainerRef

  public usuario: UsuarioModel
  public componentes: ComponentesCreados
  public distribucion: Distribucion
  public confEnrutadorNavegacion: ConfiguracionEnrutadorNavegacion

  constructor(
    public variablesGlobales: VariablesGlobales,
    public camaraService: CamaraService,
    private cuentaNegocio: CuentaNegocio,
    private router: Router,
    private _location: Location,
    private componentFactoryResolver: ComponentFactoryResolver,
    private enrutadorService: EnrutadorService,
    private metodosSessionStorageService: MetodosSessionStorageService,
  ) {

  }

  ngOnInit(): void {
    // this.metodosSessionStorageService.eliminarSessionStorage()
    this.camaraService.reiniciarServicio()
    this.configurarUsuarioActivo()
    this.configurarComponentes()
    this.configurarDistribucionInicial()
    this.configurarEnrutadorNavegacion()
    this.configurarEscuchaCambioDeEstadoEnrutadorNavegacion()
    this.configurarEscuchaClickEnManosDelEnrutador()
    this.configurarEscuchaBotonBack()

    if (this.usuario) {

    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.configurarComponenteCentralInicial()
      this.configurarComponenteIzquierdaInicial()
      this.configurarComponenteDerechaInicial()
    })
  }

  ngOnDestroy(): void {
    this.enrutadorService.desconectarDeEscuchas()
  }

  configurarUsuarioActivo() {
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
  }

  configurarComponentes() {
    this.componentes = {
      izquierda: undefined,
      central: undefined,
      derecha: undefined
    }
  }

  configurarDistribucionInicial() {
    this.distribucion = {
      izquierda: [],
      central: [],
      derecha: []
    }
  }

  configurarEnrutadorNavegacion() {
    this.confEnrutadorNavegacion = {
      mostrar: false,
      posDerecha: false,
      posCentral: true,
      posIzquierda: false
    }
  }

  // Componentes
  configurarComponenteCentralInicial() {
    this.enrutadorService.configurarInformacionAEnrutar(MenuPerfilesComponent)
    const ubicacion: UbicacionDelComponente = UbicacionDelComponente.CENTRAL
    this.reiniciarTercio(ubicacion)
    this.realizarDistribucion(ubicacion)
  }

  obtenerConfiguracionPortadaInicial(
    esIzquierdo: boolean = true
  ): ConfiguracionPortadaGif {
    return {
      esIzquierdo: esIzquierdo,
      urlImagen: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/portada-a.jpg',
      // urlGif: 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/idiomas/gif-en-a.gif',
      llaveTexto: 'fwtexto5',
    }
  }

  configurarComponenteIzquierdaInicial() {
    const ubicacion: UbicacionDelComponente = UbicacionDelComponente.IZQUIERDA
    this.enrutadorService.configurarInformacionAEnrutar(
      PortadaGifComponent,
      [
        {
          nombre: 'configuracion',
          valor: this.obtenerConfiguracionPortadaInicial()
        }
      ]
    )

    this.reiniciarTercio(ubicacion)
    this.realizarDistribucion(ubicacion)
  }

  configurarComponenteDerechaInicial() {
    // Izquierdo
    const ubicacion: UbicacionDelComponente = UbicacionDelComponente.DERECHA
    this.enrutadorService.configurarInformacionAEnrutar(
      PortadaGifComponent,
      [
        {
          nombre: 'configuracion',
          valor: this.obtenerConfiguracionPortadaInicial(false)
        }
      ]
    )

    this.reiniciarTercio(ubicacion)
    this.realizarDistribucion(ubicacion)
  }

  configurarEscuchaCambioDeEstadoEnrutadorNavegacion() {
    // Cambiar estado del enrutador
    this.enrutadorService.susCripcionEstadoEnrutacion = this.enrutadorService.cambiarEstadoEnrutadorNavegacion$.subscribe(
      info => {
        if (!this.enrutadorService.informacionAEnrutar) {
          return
        }

        if (
          !info.extras ||
          (info.extras && info.extras.tipo === TipoDeNavegacion.NORMAL)
        ) {
          this.confEnrutadorNavegacion.mostrar = info.estado
          return
        }

        if (!info.extras) {
          return
        }

        if (info.extras.tipo === TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTE) {
          this.navegacionValidarTipoComponente(info)
          return
        }

        if (info.extras.tipo === TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS) {
          this.navegacionValidarTipoComponenteConParametros(info)
          return
        }
      }
    )
  }

  navegacionNormal(info: EstadoEnrutador) {
    this.confEnrutadorNavegacion.mostrar = info.estado
  }

  navegacionValidarTipoComponente(info: EstadoEnrutador) {
    const existeInstanciaActiva = this.validarInstanciaActivaDelComponente()

    if (existeInstanciaActiva) {
      return
    }

    this.confEnrutadorNavegacion.mostrar = info.estado
  }

  navegacionValidarTipoComponenteConParametros(info: EstadoEnrutador) {
    try {
      if (!info.extras.paramAValidar) {
        throw new Error('No hay param a validar')
      }

      const existeInstanciaActiva = this.validarInstanciaActivaDelComponente(
        true,
        info.extras.paramAValidar
      )

      if (existeInstanciaActiva) {
        return
      }

      this.confEnrutadorNavegacion.mostrar = info.estado
    } catch (error) {
      return
    }
  }


  configurarEscuchaClickEnManosDelEnrutador() {
    this.enrutadorService.susCripcionClicEnMano = this.enrutadorService.clicEnManoDelEnrutador$.subscribe(info => {
      this.reiniciarTercio(info)
      this.realizarDistribucion(info)
    })
  }

  reiniciarTercio(
    ubicacion: UbicacionDelComponente
  ) {
    switch (ubicacion) {
      case UbicacionDelComponente.IZQUIERDA:
        if (this.componentes.izquierda) {
          this.componentes.izquierda.destroy()
          this.tercio_izquierda.clear()
        }
        break
      case UbicacionDelComponente.CENTRAL:
        if (this.componentes.central) {
          this.componentes.central.destroy()
          this.tercio_central.clear()
        }
        break
      case UbicacionDelComponente.DERECHA:
        if (this.componentes.derecha) {
          this.componentes.derecha.destroy()
          this.tercio_derecha.clear()
        }
        break
    }
  }

  realizarDistribucion(
    ubicacion: UbicacionDelComponente
  ) {
    try {
      const info = this.enrutadorService.informacionAEnrutar
      const componente = this.componentFactoryResolver.resolveComponentFactory(info.componente)

      const infoAEnrutar: InformacionEnrutador = {
        posicion: ubicacion,
        params: info.params
      }

      const itemDistribucion: ItemDistribucion = {
        componente: info.componente,
        posicion: ubicacion,
        params: info.params
      }

      if (ubicacion === UbicacionDelComponente.IZQUIERDA) {
        this.componentes.izquierda = this.tercio_izquierda.createComponent(componente)
        this.componentes.izquierda.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.distribucion.izquierda.push(itemDistribucion)
        this.confEnrutadorNavegacion.mostrar = false
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }

      if (ubicacion === UbicacionDelComponente.CENTRAL) {
        this.componentes.central = this.tercio_central.createComponent(componente)
        this.componentes.central.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.distribucion.central.push(itemDistribucion)
        this.confEnrutadorNavegacion.mostrar = false
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }

      if (ubicacion === UbicacionDelComponente.DERECHA) {
        this.componentes.derecha = this.tercio_derecha.createComponent(componente)
        this.componentes.derecha.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.distribucion.derecha.push(itemDistribucion)
        this.confEnrutadorNavegacion.mostrar = false
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }
    } catch (error) {

    }
  }

  definirInformacionAEnrutar(
    info: UbicacionDelComponente
  ): ItemDistribucion {
    if (info === UbicacionDelComponente.IZQUIERDA) {
      this.distribucion.izquierda.pop()
      return (this.distribucion.izquierda.length > 0) ?
        this.distribucion.izquierda[this.distribucion.izquierda.length - 1] : undefined
    }

    if (info === UbicacionDelComponente.CENTRAL) {
      this.distribucion.central.pop()
      return (this.distribucion.central.length > 0) ?
        this.distribucion.central[this.distribucion.central.length - 1] : undefined
    }

    if (info === UbicacionDelComponente.DERECHA) {
      this.distribucion.derecha.pop()
      return (this.distribucion.derecha.length > 0) ?
        this.distribucion.derecha[this.distribucion.derecha.length - 1] : undefined
    }
  }

  configurarEscuchaBotonBack() {
    this.enrutadorService.susCripcionEnBotonBack = this.enrutadorService.clicEnBotonBack$.subscribe(info => {


      this.reiniciarTercio(info)
      const itemAnterior = this.definirInformacionAEnrutar(info)

      if (!itemAnterior) {
        return
      }

      this.enrutadorService.informacionAEnrutar = {
        componente: itemAnterior.componente,
        params: itemAnterior.params
      }

      this.abrirComponenteAnterior(info)
    })
  }

  abrirComponenteAnterior(
    ubicacion: UbicacionDelComponente
  ) {
    try {
      const info = this.enrutadorService.informacionAEnrutar

      const componente = this.componentFactoryResolver.resolveComponentFactory(info.componente)

      const infoAEnrutar: InformacionEnrutador = {
        posicion: ubicacion,
        params: info.params
      }

      if (ubicacion === UbicacionDelComponente.IZQUIERDA) {
        this.componentes.izquierda = this.tercio_izquierda.createComponent(componente)
        this.componentes.izquierda.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }

      if (ubicacion === UbicacionDelComponente.CENTRAL) {
        this.componentes.central = this.tercio_central.createComponent(componente)
        this.componentes.central.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }

      if (ubicacion === UbicacionDelComponente.DERECHA) {
        this.componentes.derecha = this.tercio_derecha.createComponent(componente)
        this.componentes.derecha.instance.configurarInformacionDelEnrutador(infoAEnrutar)
        this.enrutadorService.informacionAEnrutar = undefined
        return
      }
    } catch (error) {
    }
  }

  validarInstanciaActivaDelComponente(
    validarParams: boolean = false,
    paramAValidar: string = ''
  ): boolean {
    if (!this.distribucion) {
      return false
    }

    const indexUno = this.distribucion.izquierda.length - 1
    const indexDos = this.distribucion.central.length - 1
    const indexTres = this.distribucion.derecha.length - 1

    const paramsAEnrutar = this.enrutadorService.informacionAEnrutar.params

    if (
      this.validarInstanciaConParams(indexUno, this.distribucion.izquierda, validarParams, paramsAEnrutar, paramAValidar) ||
      this.validarInstanciaConParams(indexDos, this.distribucion.central, validarParams, paramsAEnrutar, paramAValidar) ||
      this.validarInstanciaConParams(indexTres, this.distribucion.derecha, validarParams, paramsAEnrutar, paramAValidar)
    ) {
      return true
    }

    return false
  }

  validarInstanciaConParams(
    index: number,
    distribucion: Array<ItemDistribucion>,
    validarParams: boolean,
    paramsAEnrutar: Array<ItemParam>,
    paramAValidar: string
  ): boolean {
    return (
      index >= 0 && distribucion[index].componente === this.enrutadorService.informacionAEnrutar.componente &&
      (
        !validarParams ||
        (
          validarParams &&
          this.compararParametros(paramAValidar, distribucion[index].params, paramsAEnrutar)
        )
      )
    )
  }

  compararParametros(
    paramAValidar: string,
    a: Array<ItemParam>,
    b: Array<ItemParam>,
  ): boolean {
    if (a.length !== b.length) {
      return false
    }

    const indexUno = a.findIndex(e => e.nombre === paramAValidar)
    const indexDos = b.findIndex(e => e.nombre === paramAValidar)

    if (!(indexUno >= 0 && indexDos >= 0)) {
      return false
    }
    const aa = (a[indexUno].valor === b[indexDos].valor)

    return aa
  }


}
