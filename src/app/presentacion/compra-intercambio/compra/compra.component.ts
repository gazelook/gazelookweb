import { TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { InformacionEnrutador } from 'presentacion/enrutador/enrutador.component';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { Component, OnInit } from '@angular/core';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { Location } from '@angular/common';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
	EstilosDelTexto,
	ColorFondoLinea,
	EspesorLineaItem,
	ColorDelTexto,
	ColorDeBorde,
	ColorDeFondo
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { ColorTextoBoton, TipoBoton } from 'src/app/compartido/componentes/button/button.component';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';

@Component({
	selector: 'app-compra',
	templateUrl: './compra.component.html',
	styleUrls: ['./compra.component.scss']
})
export class CompraComponent implements OnInit {
	configuracionAppBar: ConfiguracionAppbarCompartida;
	perfilSeleccionado: PerfilModel;
	confLinea: LineaCompartida;

	//botones
	botonCompra: BotonCompartido
	botonIntercambio: BotonCompartido

	botonBuscarCultural: BotonCompartido;
	botonPublicarCultural: BotonCompartido;
	// Enrutamiento
	public informacionEnrutador: InformacionEnrutador


	constructor(
		private perfilNegocio: PerfilNegocio,
		public estiloTexto: EstiloDelTextoServicio,
		private _location: Location,
		private internacionalizacionNegocio: InternacionalizacionNegocio,
		private variablesGlobales: VariablesGlobales,
		private enrutadorService: EnrutadorService
	) { }

	ngOnInit(): void {
		this.variablesGlobales.mostrarMundo = false
		this.obtenerPerfil()
		this.configurarLinea()

		if (this.perfilSeleccionado) {
			this.prepararAppBar()
			this.estiloTituloPrincipal()
			this.configuracionBotones()
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}
	}

	obtenerPerfil() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
	}

	prepararAppBar() {
		this.configuracionAppBar = {
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => {
				this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
			},
			eventoHome: () =>
				this.enrutadorService.navegar(
					{
						componente: MenuPrincipalComponent,
						params: [
						]
					},
					{
						estado: true,
						posicicion: this.informacionEnrutador.posicion,
						extras: {
							tipo: TipoDeNavegacion.NORMAL,
						}
					}
				),
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarLineaVerde: true,
				mostrarTextoHome: true,
				mostrarBotonXRoja: false,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true,

					}
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm6v10texto1'
				},
			}
		}
	}

	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
			forzarAlFinal: false
		}
	}

	estiloTituloPrincipal() {
		return this.estiloTexto.obtenerEstilosTexto({
			color: ColorDelTexto.TEXTOBLANCO,
			estiloTexto: EstilosDelTexto.BOLD,
			enMayusculas: true,
			tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I7
		})

	}

	async configuracionBotones() {
		this.botonBuscarCultural = {
			text: await this.internacionalizacionNegocio.obtenerTextoLlave('m6v5texto5'),
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => { },
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

		this.botonPublicarCultural = {
			text: await this.internacionalizacionNegocio.obtenerTextoLlave('m6v5texto6'),
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => { },
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

	}

	// Enrutamiento
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}


}
