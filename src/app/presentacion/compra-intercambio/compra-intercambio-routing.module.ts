import { BuscadorIntercambioComponent } from './intercambio/buscador-intercambio/buscador-intercambio.component';
import { ListaPublicacionIntercambioComponent } from './intercambio/lista-publicacion-intercambio/lista-publicacion-intercambio.component';
import { PublicarIntercambioComponent } from './intercambio/publicar-intercambio/publicar-intercambio.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompraIntercambioComponent } from './compra-intercambio.component';
import { CompraIntermacioInicioComponent } from './compra-intermacio-inicio/compra-intermacio-inicio.component';
import { CompraComponent } from './compra/compra.component';
import { IntercambioComponent } from './intercambio/intercambio.component';
import { RutasCompra } from './rutas-compra.enum';

const routes: Routes = [
  {
    path: '',
    component: CompraIntercambioComponent,
    children: [
      { path: '', redirectTo: RutasCompra.INICIO.toString(), pathMatch: 'full' },
  
      {
        path: RutasCompra.INICIO.toString(),
        component: CompraIntermacioInicioComponent
      },
      {
        path: RutasCompra.INTERCAMBIO.toString(),
        component: IntercambioComponent
      },
      {
        path: RutasCompra.COMPRA.toString(),
        component: CompraComponent
      },
      {
        path: RutasCompra.PUBLICAR.toString(),
        component: PublicarIntercambioComponent
      },
      {
        path: RutasCompra.ACTUALIZAR.toString(),
        component: PublicarIntercambioComponent
      },
      {
        path: RutasCompra.VISITAR.toString(),
        component: PublicarIntercambioComponent
      },
      {
        path: RutasCompra.LISTA_MIS_INTERCAMBIO.toString(),
        component: ListaPublicacionIntercambioComponent
      },
      {
        path: RutasCompra.BUSQUEDA_INTERCAMBIO_TIPO.toString(),
        component: BuscadorIntercambioComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompraIntercambioRoutingModule { }
