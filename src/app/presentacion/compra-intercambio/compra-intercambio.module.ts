import { IntercambioComponent } from './intercambio/intercambio.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompraIntercambioRoutingModule } from './compra-intercambio-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from 'src/app/compartido/compartido.module';
import { CompraIntermacioInicioComponent } from './compra-intermacio-inicio/compra-intermacio-inicio.component';
import { CompraComponent } from './compra/compra.component';
import { CompraIntercambioComponent } from './compra-intercambio.component';
import { BuscadorIntercambioComponent } from './intercambio/buscador-intercambio/buscador-intercambio.component';
import { PublicarIntercambioComponent } from './intercambio/publicar-intercambio/publicar-intercambio.component';
import { ListaPublicacionIntercambioComponent } from './intercambio/lista-publicacion-intercambio/lista-publicacion-intercambio.component';


@NgModule({
  declarations: [
    CompraIntercambioComponent,
    CompraIntermacioInicioComponent,
    CompraComponent,
    BuscadorIntercambioComponent,
    PublicarIntercambioComponent,
    IntercambioComponent,
    ListaPublicacionIntercambioComponent
  ],
  imports: [
    CommonModule,
    CompartidoModule,
    CompraIntercambioRoutingModule,
    TranslateModule,
    FormsModule,
    CompartidoModule
  ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class CompraIntercambioModule { }
