import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { EnrutadorService } from '@core/servicios/generales/enrutador/enrutador.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { SelectorComponent } from '@shared/componentes/selector/selector.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AccionesBuscadorLocalidadModal, AccionesSelector } from '@shared/diseno/enums/acciones-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { ColorDeBorde, ColorDeFondo, ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemIntercambio } from '@shared/diseno/enums/uso-item-intercambio.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { InfoAccionBuscadorLocalidades } from '@shared/diseno/modelos/info-acciones-buscador-localidades.interface';
import { ConfiguracionItemIntercambio } from '@shared/diseno/modelos/item-intercambio.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { IntercambioNegocio } from 'dominio/logica-negocio/intercambio.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { UbicacionNegocio } from 'dominio/logica-negocio/ubicacion.negocio';
import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { IntercambioParams } from 'dominio/modelo/parametros/intercambio-params.interface';
import { InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
import { FiltroBusqueda } from 'presentacion/proyectos/nuevos-proyectos/nuevos-proyectos.component';
import { PublicarIntercambioComponent } from 'presentacion/compra-intercambio/intercambio/publicar-intercambio/publicar-intercambio.component';

@Component({
  selector: 'app-buscador-intercambio',
  templateUrl: './buscador-intercambio.component.html',
  styleUrls: ['./buscador-intercambio.component.scss']
})
export class BuscadorIntercambioComponent implements OnInit {

  @ViewChild('selectorPaises', { static: false }) selectorPaises: SelectorComponent
  @ViewChild('toast', { static: false }) toast: ToastComponent


  public codigosTipoIntercambio: Array<CatalogoTipoIntercambioModel>
  // Configuracion de capas
  public mostrarCapaLoader: boolean
  public mostrarCapaError: boolean
  public mensajeCapaError: string
  public mostrarCapaNormal: boolean
  public puedeCargarMas: boolean

  // Parametros internos
  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel
  public params: IntercambioParams
  public filtroActivoDeBusqueda: FiltroBusqueda
  public tituloBuscador: string
  public paisBuscador: string
  public localidadBuscador: string
  public codigoAIntercambiarA: string
  public codigoAIntercambiarB: string
  public codigoIntercambioInicial: string
  public mostrarLineaVerde: boolean
  public fechaInicial: Date
  public fechaFinal: Date
  public idCapaCuerpo: string
  public currentDay: string
  public tipoIntercambioPrincipal: string

  // Configuracion hijos
  public confToast: ConfiguracionToast
  public confAppBar: ConfiguracionAppbarCompartida
  public listaIntercambioModel: Array<IntercambioModel>
  public listaIntercambio: PaginacionModel<ConfiguracionItemIntercambio>
  public confLineaintercambio: LineaCompartida
  public confSelector: ConfiguracionSelector
  public confBuscador: ConfiguracionBuscadorModal
  public confBotonBuscar: BotonCompartido
  public listaResultadosLocalidades: PaginacionModel<ItemSelector>


  // Enrutador
  public informacionEnrutador: InformacionEnrutador


  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private variablesGlobales: VariablesGlobales,
    private intercambioNegocio: IntercambioNegocio,
    private albumNegocio: AlbumNegocio,
    private translateService: TranslateService,
    private ubicacionNegocio: UbicacionNegocio,
    private enrutadorService: EnrutadorService,

  ) {

    this.params = { estado: false }
    this.mostrarCapaLoader = false
    this.mostrarCapaError = false
    this.puedeCargarMas = true
    this.mostrarLineaVerde = false
    this.mensajeCapaError = ''
    this.codigoAIntercambiarA = ''
    this.codigoAIntercambiarB = ''
    this.codigoIntercambioInicial = ''
    this.tipoIntercambioPrincipal = ''
    this.codigosTipoIntercambio = []
    this.fechaInicial = new Date('2021-01-02')
    this.fechaFinal = new Date()
    this.idCapaCuerpo = 'capa-cuerpo-noticias'
    this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES

    this.tituloBuscador = ''
    this.paisBuscador = ''
    this.localidadBuscador = ''
    this.listaIntercambioModel = []
    this.currentDay = new Date().toISOString().slice(0, 10);
  }

  async ngOnInit() {
    this.variablesGlobales.mostrarMundo = false
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()
    this.configurarParametrosDeLaUrl()

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarToast()
      this.configurarAppBar()
      this.configurarBotones()
      this.inicializarListaintercambio()
      await this.inicializarTiposIntercambio()
      this.definirTipoIntercambioInicial()
      this.configurarLineas()
      this.configurarSelectorPais()
      this.configurarBuscadorLocalidades()
      this.configurarListaPaginacionLocalidades()
      this.ejecutarBusqueda()

    } else {
      // this.perfilNegocio.validarEstadoDelPerfil(
      // 	this.perfilSeleccionado,
      // 	this.params.estado
      // )
    }
  }

  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarParametrosDeLaUrl() {


    this.informacionEnrutador.params.forEach(item => {


      if (item.nombre === 'accionEntidad') {
        this.params.accionEntidad = item.valor
      }

      if (item.nombre === 'codigoTipoIntercambio') {
        this.params.codigoTipoIntercambio = item.valor
        this.params.estado = true
      }

    })


  }

  configurarAppBar() {
    if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.CULTURAL) {
      this.tipoIntercambioPrincipal = 'm6v6texto6'
    }
    if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.ALOJAMIENTO) {
      this.tipoIntercambioPrincipal = 'm6v6texto7'
    }
    if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.OBJETOS_PRODUCTOS) {
      this.tipoIntercambioPrincipal = 'm6v6texto8'
    }
    if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.HABILIDADES) {
      this.tipoIntercambioPrincipal = 'm6v6texto9'
    }
    this.confAppBar = {
      botonRefresh: true,
      eventoRefresh: () => {
          this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
      },
      accionAtras: () => {
        this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
      },
      eventoHome: () =>
        this.enrutadorService.navegar(
          {
            componente: MenuPrincipalComponent,
            params: [
            ]
          },
          {
            estado: true,
            posicicion: this.informacionEnrutador.posicion,
            extras: {
              tipo: TipoDeNavegacion.NORMAL,
            }
          }
        ),
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,

          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v6texto1'
        },

      }

    }



  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false, //True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    }
  }

  async inicializarTiposIntercambio() {
    this.codigosTipoIntercambio = await this.intercambioNegocio.tiposIntercambio().toPromise()


  }


  definirTipoIntercambioInicial() {


    let mitipoIntercambio = this.codigosTipoIntercambio.filter(element => element.codigo === this.params.codigoTipoIntercambio)
    this.codigoIntercambioInicial = mitipoIntercambio[0].nombre
    this.codigoAIntercambiarA = mitipoIntercambio[0].codigo
  }
  configurarLineas() {
    this.confLineaintercambio = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    }
  }

  inicializarListaintercambio() {
    this.listaIntercambioModel = []

    this.listaIntercambio = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    }
  }
  async configurarBotones() {
    // Boton bsucar
    this.confBotonBuscar = {
      text: 'm6v6texto13',
      colorTexto: ColorTextoBoton.CELESTE,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.ejecutarBusqueda()
    }
  }

  scroolEnCapaCuerpo() {
    if (!this.puedeCargarMas) {
      return
    }

    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false



      // this.ejecutarBusqueda()
      this.obtenerIntercambiosPorFiltro(
        this.params.codigoTipoIntercambio,
        this.perfilSeleccionado._id,
        this.obtenerFechaParaParametro(this.fechaInicial),
        this.obtenerFechaParaParametro(this.fechaFinal),
        this.paisBuscador,
        this.tituloBuscador,
        this.localidadBuscador,
        this.codigoAIntercambiarA,
        this.codigoAIntercambiarB,

      )
      return

    }
  }



  ejecutarBusqueda() {


    if (this.mostrarCapaLoader) {
      return
    }

    if (
      this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS &&
      !this.validarFechasIngresadas()
    ) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES
      return
    }


    this.inicializarListaintercambio()

    this.obtenerIntercambiosPorFiltro(
      this.params.codigoTipoIntercambio,
      this.perfilSeleccionado._id,
      this.obtenerFechaParaParametro(this.fechaInicial),
      this.obtenerFechaParaParametro(this.fechaFinal),
      this.paisBuscador,
      this.tituloBuscador,
      this.localidadBuscador,
      this.codigoAIntercambiarA,
      this.codigoAIntercambiarB,

    )
    return
  }


  cambioDeFecha(orden: number) {
    if (orden === 0) {
      let fechaInicial = this.fechaInicial
			this.fechaInicial = new Date(this.obtenerFechaSeleccionada(fechaInicial))    }

    if (orden === 1) {
      let fechaFinal = this.fechaFinal
			this.fechaFinal = new Date(this.obtenerFechaSeleccionada(fechaFinal))
    }
    this.filtroActivoDeBusqueda = FiltroBusqueda.RANGO_FECHAS
    this.ejecutarBusqueda()

  }

  obtenerFechaParaParametro(fecha: Date) {
    const dia = (fecha.getDate() < 10) ? '0' + fecha.getDate() : fecha.getDate()
    const mes = (fecha.getMonth() + 1 < 10) ? '0' + (fecha.getMonth() + 1) : fecha.getMonth() + 1
    return fecha.getFullYear() + '-' + mes + '-' + dia
  }

  validarFechasIngresadas() {
    if (this.fechaInicial.getTime() > this.fechaFinal.getTime()) {
      this.toast.abrirToast('text53')
      return false
    }

    return true
  }


  async obtenerIntercambiosPorFiltro(
    tipo: CodigosCatalogoTipoIntercambio,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
    pais: string,
    titulo: string,
    localidad: string,
    codigoAIntercambiarA: string,
    codigoAIntercambiarB: string,

  ) {
    if (!this.listaIntercambio.proximaPagina) {
      this.puedeCargarMas = true
      return
    }

    try {
      this.mostrarCapaError = false
      this.mostrarCapaLoader = (this.listaIntercambio.lista.length === 0)
      const dataPaginacion = await this.intercambioNegocio.buscarIntercambioPorFiltro(
        16,
        this.listaIntercambio.paginaActual,
        tipo,
        perfil,
        fechaInicial,
        fechaFinal,
        pais,
        titulo,
        localidad,
        codigoAIntercambiarA,
        codigoAIntercambiarB,

      ).toPromise()

      this.listaIntercambio.totalDatos = dataPaginacion.totalDatos
      this.listaIntercambio.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(intercambio => {
        this.listaIntercambioModel.push(intercambio)
        this.listaIntercambio.lista.push(this.configurarItemListaIntercambio(intercambio))
      })
      this.mostrarCapaLoader = false
      this.puedeCargarMas = true
      this.mostrarLineaVerde = true

      if (this.listaIntercambio.proximaPagina) {
        this.listaIntercambio.paginaActual += 1
      }
    } catch (error) {
      this.mostrarCapaLoader = false
      this.puedeCargarMas = false
      this.mostrarCapaError = true
    }
  }


  configurarItemListaIntercambio(intercambio: IntercambioModel): ConfiguracionItemIntercambio {

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      intercambio.adjuntos
    )

    return {
      id: intercambio.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(intercambio.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: intercambio.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemIntercambio.REC_INTERC,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: true,
        evento: (data: ConfiguracionItemIntercambio) => {
          this.intercambioNegocio.removerIntercambioActivoDelSessionStorage()
          if (data.id) {
            const intercambio: IntercambioModel = this.obtenerIntercambioDeLista(data.id)

            if (
              !this.perfilSeleccionado ||
              !intercambio ||
              !(intercambio !== null)
            ) {
              return
            }


            if (
              intercambio &&
              intercambio !== null &&
              this.perfilSeleccionado._id !== intercambio.perfil._id
            ) {


              // const modulo = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
              // let ruta = RutasCompra.VISITAR.toString()
              // ruta = ruta.replace(':id', data.id)
              // this.router.navigateByUrl(modulo + '/' + ruta)

              this.enrutadorService.navegar(
                {
                  componente: PublicarIntercambioComponent,
                  params: [
                    {
                      nombre: 'accionEntidad',
                      valor: AccionEntidad.VISITAR
                    },
                    {
                      nombre: 'id',
                      valor: data.id
                    }
                  ]
                },
                {
                  estado: true,
                  posicicion: this.informacionEnrutador.posicion,
                  extras: {
                    tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                    paramAValidar: 'id'
                  }
                }
              )
              return
            }


            // const modulo = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
            // let ruta = RutasCompra.ACTUALIZAR.toString()
            // ruta = ruta.replace(':id', data.id)
            // this.router.navigateByUrl(modulo + '/' + ruta)

            this.enrutadorService.navegar(
              {
                componente: PublicarIntercambioComponent,
                params: [
                  {
                    nombre: 'accionEntidad',
                    valor: AccionEntidad.ACTUALIZAR
                  },
                  {
                    nombre: 'id',
                    valor: data.id
                  }
                ]
              },
              {
                estado: true,
                posicicion: this.informacionEnrutador.posicion,
                extras: {
                  tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
                  paramAValidar: 'id'
                }
              }
            )
          }
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  obtenerIntercambioDeLista(id: string): IntercambioModel {
    const index: number = this.listaIntercambioModel.findIndex(e => e.id === id)

    if (index >= 0) {
      return this.listaIntercambioModel[index]
    }

    return null
  }

  async configurarSelectorPais() {
    // Definir direccion
    let item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }



    this.confSelector = {
      tituloSelector: 'text61',
      mostrarModal: false,
      inputPreview: {
        mostrar: true,
        input: {
          valor: item.nombre,
          placeholder: 'COUNTRY:',
          textoBold: false,
          textoEnMayusculas: true,

        },
      },
      seleccionado: item,
      elegibles: [],
      cargando: {
        mostrar: false
      },
      error: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false
      },
      quitarMarginAbajo: true,
      tipoIntercambio: true,
      evento: (data: InfoAccionSelector) => this.eventoEnSelector(data)
    }

    this.confSelector.inputPreview.input.placeholder = await this.translateService.get('m2v3texto14').toPromise()
  }
  async configurarBuscadorLocalidades() {
    // Definir direccion
    let item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
    let pais: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }



    this.confBuscador = {
      seleccionado: item,
      inputPreview: {
        mostrar: true,
        input: {
          placeholder: '',
          valor: (item.nombre.length > 0) ? item.nombre : '',
          auxiliar: item.auxiliar,
          desactivar: false,
          textoBold: false,
          textoEnMayusculas: true
        },
        quitarMarginAbajo: true,
      },
      mostrarModal: false,
      inputBuscador: {
        valor: '',
        placeholder: 'Busca tu localidad',
      },
      resultado: {
        mostrarElegibles: false,
        mostrarCargando: false,
        error: {
          mostrarError: false,
          contenido: '',
          tamanoCompleto: false
        },
        items: []
      },
      tipoIntercambio: true,


      pais: pais,
      evento: (data: InfoAccionBuscadorLocalidades) => this.eventoEnBuscador(data)
    }

    this.confBuscador.inputPreview.input.placeholder = await this.translateService.get('m2v3texto15').toPromise()
  }


  eventoEnSelector(data: InfoAccionSelector) {
    switch (data.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorPaises()
        break
      case AccionesSelector.SELECCIONAR_ITEM:
        // // Buscador
        this.confBuscador.pais = data.informacion
        this.confBuscador.seleccionado.codigo = ''
        this.confBuscador.seleccionado.nombre = ''
        this.confBuscador.inputPreview.input.valor = ''
        // Selector
        this.confSelector.seleccionado = data.informacion
        this.confSelector.mostrarModal = false
        this.confSelector.inputPreview.input.valor = data.informacion.nombre
        this.paisBuscador = data.informacion.codigo

        break
      case AccionesSelector.REINTERTAR_CONTENIDO: break
      case AccionesSelector.BUSCAR_PAIS_POR_QUERY: break
      default: break;
    }
  }

  eventoEnBuscador(data: InfoAccionBuscadorLocalidades) {
    switch (data.accion) {
      case AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR:
        this.confBuscador.resultado.items = []
        this.configurarListaPaginacionLocalidades()
        this.abrirBuscadorLocalidades()
        break
      case AccionesBuscadorLocalidadModal.REALIZAR_BUSQUEDA:
        this.buscarLocalidades(data.informacion.pais, data.informacion.query)
        break
      case AccionesBuscadorLocalidadModal.CARGAR_MAS_RESULTADOS:
        if (!this.listaResultadosLocalidades.proximaPagina) {
          return
        }

        this.listaResultadosLocalidades.paginaActual += 1
        this.buscarLocalidades(data.informacion.pais, data.informacion.query, false)
        break
      case AccionesBuscadorLocalidadModal.SELECCIONAR_ITEM:
        if (!data.informacion || !data.informacion.item) {
          break
        }

        this.confBuscador.seleccionado = data.informacion.item
        this.confBuscador.inputPreview.input.valor = this.confBuscador.seleccionado.nombre
        this.confBuscador.inputPreview.input.auxiliar = this.confBuscador.seleccionado?.auxiliar

        this.localidadBuscador = this.confBuscador.seleccionado.nombre
        this.reiniciarBuscador()
        break
      case AccionesBuscadorLocalidadModal.CERRAR_BUSCADOR:
        this.reiniciarBuscador()
        break
      default: break;
    }
  }

  abrirBuscadorLocalidades() {
    if (!this.confBuscador.pais || this.confBuscador.pais.codigo.length === 0) {
      this.toast.abrirToast('text54')
      return
    }
    this.confBuscador.mostrarModal = true
  }


  async buscarLocalidades(
    pais: string,
    query: string,
    reiniciarLista: boolean = true
  ) {
    try {
      this.mostrarErrorEnBuscador('', false)
      this.confBuscador.resultado.mostrarCargando = (reiniciarLista)
      this.confBuscador.resultado.mostrarElegibles = (!reiniciarLista)
      this.confBuscador.resultado.puedeCargarMas = false
      this.confBuscador.resultado.mostrarCargandoPequeno = (!reiniciarLista)

      if (reiniciarLista) {
        this.confBuscador.resultado.items = []
        this.configurarListaPaginacionLocalidades()
      }

      const localidades: PaginacionModel<ItemSelector> = await this.ubicacionNegocio.buscarLocalidadesPorNombrePaisConPaginacion(
        25,
        this.listaResultadosLocalidades.paginaActual,
        pais,
        query
      ).toPromise()

      if (!localidades) {
        throw new Error()
      }

      localidades.lista.forEach(item => {
        this.listaResultadosLocalidades.lista.push(item)
      })
      this.listaResultadosLocalidades.proximaPagina = localidades.proximaPagina

      this.confBuscador.resultado.items = this.listaResultadosLocalidades.lista
      this.confBuscador.resultado.mostrarElegibles = true
      this.confBuscador.resultado.puedeCargarMas = true
      this.confBuscador.resultado.mostrarCargando = false
      this.confBuscador.resultado.mostrarCargandoPequeno = false
    } catch (error) {
      this.mostrarErrorEnBuscador('text31', true)
    }
  }

  mostrarErrorEnBuscador(contenido: string, mostrar: boolean, tamanoCompleto: boolean = false) {
    this.confBuscador.resultado.error.contenido = contenido
    this.confBuscador.resultado.error.tamanoCompleto = tamanoCompleto
    this.confBuscador.resultado.error.mostrarError = mostrar
    this.confBuscador.resultado.mostrarCargando = false
  }

  reiniciarBuscador() {
    this.confBuscador.mostrarModal = false
    this.confBuscador.inputBuscador.valor = ''
    this.confBuscador.resultado.items = []
    this.confBuscador.resultado.mostrarElegibles = false
    this.confBuscador.resultado.mostrarCargando = false
    this.confBuscador.resultado.error.contenido = ''
    this.confBuscador.resultado.error.tamanoCompleto = false
    this.confBuscador.resultado.error.mostrarError = false
  }

  configurarListaPaginacionLocalidades() {
    this.listaResultadosLocalidades = {
      lista: [],
      paginaActual: 1,
      totalDatos: 0,
      proximaPagina: true,
    }
  }

  async abrirSelectorPaises() {
    try {
      this.confSelector.cargando.mostrar = true
      this.confSelector.mostrarModal = true
      const items: ItemSelector[] = await this.ubicacionNegocio.obtenerCatalogoPaisesParaSelector().toPromise()
      if (!items) {
        throw new Error('')
      }

      this.ubicacionNegocio.guardarPaisesDelSelectorEnLocalStorage(items)
      this.confSelector.elegibles = items
      this.confSelector.cargando.mostrar = false

      if (this.confSelector.elegibles.length === 0) {
        this.confSelector.cargando.mostrar = false
        this.confSelector.error.contenido = 'text31'
        this.confSelector.error.mostrarError = true
      }
    } catch (error) {
      this.confSelector.elegibles = []
      this.confSelector.cargando.mostrar = false
      this.confSelector.error.contenido = 'text31'
      this.confSelector.error.mostrarError = true
    }
  }

  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item
  }

  obtenerFechaSeleccionada(fecha): string {
		// let date = new Date(fecha);
		// let anio = date.getFullYear().toString();
		// let mes_temporal = date.getMonth() + 1;
		// let dia_temporal = date.getDate() + 1
		// var mes = String(mes_temporal.toString()).padStart(2, '0');
		// let dia = String(dia_temporal.toString()).padStart(2, '0');

		// let fechaSeleccionada = anio + '/' + mes + '/' + dia;

		// return fechaSeleccionada
    let fechaAServer1 =  fecha.replace('-','/')
    let fechaAServer2 =  fechaAServer1.replace('-','/')
return fechaAServer2
	}
  

}
