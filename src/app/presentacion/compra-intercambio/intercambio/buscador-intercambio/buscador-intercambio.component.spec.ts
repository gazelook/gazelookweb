import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorIntercambioComponent } from './buscador-intercambio.component';

describe('BuscadorIntercambioComponent', () => {
  let component: BuscadorIntercambioComponent;
  let fixture: ComponentFixture<BuscadorIntercambioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscadorIntercambioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorIntercambioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
