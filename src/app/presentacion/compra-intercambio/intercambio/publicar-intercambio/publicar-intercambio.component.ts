import { MediaModel } from './../../../../dominio/modelo/entidades/media.model';
import { AlbumGeneralComponent } from './../../../album/album-general/album-general.component';
import { AlbumAudiosComponent } from './../../../album/album-audios/album-audios.component';
import { Location } from '@angular/common';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EnrutadorService } from '@core/servicios/generales/enrutador';
import { TranslateService } from '@ngx-translate/core';
import {
  AnchoLineaItem,
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums';
import { LineaCompartida } from '@shared/diseno/modelos';
import { SelectorComponent } from '@shared/componentes/selector/selector.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import {
  ColorDeFondo,
  ColorDelTexto,
  EstilosDelTexto,
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { UbicacionNegocio } from 'dominio/logica-negocio/ubicacion.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import {
  InformacionEnrutador,
  TipoDeNavegacion,
} from 'presentacion/enrutador/enrutador.component';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import { MapaComponent } from '@shared/componentes/mapa/mapa.component';
import { PortadaExpandidaComponent } from '@shared/componentes/portada-expandida/portada-expandida.component';
import {
  AccionesBuscadorLocalidadModal,
  AccionesSelector,
} from '@shared/diseno/enums/acciones-general.enum';
import { ColorDeBorde } from '@shared/diseno/enums/estilo-colores.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { UsoItemIntercambio } from '@shared/diseno/enums/uso-item-intercambio.enum';
import {
  BarraBusqueda,
  ConfiguracionAppbarCompartida,
} from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ConfiguracionComentario } from '@shared/diseno/modelos/comentario.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { InfoAccionBuscadorLocalidades } from '@shared/diseno/modelos/info-acciones-buscador-localidades.interface';
import { ConfiguracionItemIntercambio } from '@shared/diseno/modelos/item-intercambio.interface';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { ConfiguracionMapa } from '@shared/diseno/modelos/mapa.interface';
import {
  BloquePortada,
  ColoresBloquePortada,
  ConfiguracionPortadaExandida,
  SombraBloque,
  TipoBloqueBortada,
} from '@shared/diseno/modelos/portada-expandida.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { ComentarioIntercambioFirebaseEntityMapperService } from 'dominio/entidades/comentario-intercambio.entity';
import { ComentarioFirebaseEntity } from 'dominio/entidades/comentario.entity';
import { ComentarioIntercambioNegocio } from 'dominio/logica-negocio/comentario-intercambio.negocio';
import { IntercambioNegocio } from 'dominio/logica-negocio/intercambio.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import {
  ComentarioIntercambioFirebaseModel,
  ComentarioIntercambioModel,
} from 'dominio/modelo/entidades/comentario-intercambio.model';
import {
  ComentarioFirebaseModel,
  ComentarioModel,
} from 'dominio/modelo/entidades/comentario.model';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { IntercambioParams } from 'dominio/modelo/parametros/intercambio-params.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { OrigenConexion } from '@core/servicios/generales/comentarios-firebase.service';
import { ComentariosIntercambioFirebaseService } from '@core/servicios/generales/comentarios-intercambio-firebase.service';
import { ImagenPantallaCompletaService } from '@core/servicios/generales/imagen-pantalla-completa.service';
import { IntercambioService } from '@core/servicios/generales/intercambio.service';
import { MetodosParaFotos } from '@core/servicios/generales/metodos-para-fotos.service';
import { LocationService } from '@core/servicios/locales/location.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstatusIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-status-intercambio';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import {
  CodigosCatalogosEstadoComentarioIntercambio,
  CodigosCatatalogosTipoComentario,
} from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-comentario.enum';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  RutasAlbumAudios,
  RutasAlbumGeneral,
} from 'presentacion/album/rutas-albums.enum';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';

@Component({
  selector: 'app-publicar-intercambio',
  templateUrl: './publicar-intercambio.component.html',
  styleUrls: ['./publicar-intercambio.component.scss'],
})
export class PublicarIntercambioComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @ViewChild('portadaExpandida', { static: false })
  portadaExpandida: PortadaExpandidaComponent;
  @ViewChild('selectorPaises', { static: false })
  selectorPaises: SelectorComponent;
  @ViewChild('mapa', { static: false }) mapa: MapaComponent;
  // Utils
  public InputKeyEnum = InputKey;
  public imagenesDefecto: Array<ArchivoModel>;
  public AccionEntidadEnum = AccionEntidad;
  public codigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum;
  public codigosCatalogoEstatusIntercambio = CodigosCatalogoEstatusIntercambio;
  public codigosCatalogoTipoIntercambio = CodigosCatalogoTipoIntercambio;
  public codigosTipoIntercambio: Array<CatalogoTipoIntercambioModel>;
  public codigoIntercambioInicial: string;
  public codigoAIntercambiar: string;
  public codigoAIntercambiarDelProyecto: string;
  public codigoAIntercambiarVisitar: string;
  public idCapaFormulario: string;
  public idCapaListaIntercambios: string;
  public puedeCargarMas: boolean;
  public origenValidacionCambios: OrigenValidacionDeCambios;
  public estadoPerfilParaMisIntercambios: ModoDelPerfil;
  public ModoDelPerfilEnum = ModoDelPerfil;

  public comentarioAEliminar: ComentarioModel;
  public idPerfilCoautorParaResponder: string;
  public idInternoParaResponder: string;
  public listaDeFechas: Date[];
  public formatoFecha: string;

  // Parametros internos
  public perfilSeleccionado: PerfilModel;
  public intercambio: IntercambioModel;
  public intercambioForm: FormGroup;
  public inputsForm: Array<Inputs>;
  public listaResultadosLocalidades: PaginacionModel<ItemSelector>;
  // Parametros de la url
  public params: IntercambioParams;
  // Configuraciones
  public confToast: ConfiguracionToast;
  public confAppbar: ConfiguracionAppbarCompartida;
  public confPortadaExpandida: ConfiguracionPortadaExandida;
  public confSelector: ConfiguracionSelector;
  public confBuscador: ConfiguracionBuscadorModal;
  public confBarraInferior: ConfiguracionBarraInferiorInline;
  public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta;
  public confLineaComentarios: LineaCompartida;

  public listaIntercambioModel: Array<IntercambioModel>;
  public listaIntercambio: PaginacionModel<ConfiguracionItemIntercambio>;

  public confMapa: ConfiguracionMapa;
  public confDone: ConfiguracionDone;
  public infoPropietarioIntercambio: ConfiguracionItemListaContactosCompartido;

  //botones
  public confBotonHabilitar: BotonCompartido;
  public confBotonDeshabilitar: BotonCompartido;
  public confBotonEliminar: BotonCompartido;
  public confBotonPublish: BotonCompartido;

  //dialogos
  public confDialogoSalida: DialogoCompartido;
  public confDialogoHabilitar: DialogoCompartido; // Dialogo de hibernar
  public confDialogoDeshabilitar: DialogoCompartido; // Dialogo de deshibernar
  public confDialogoEliminar: DialogoCompartido; // Dialogo de eliminar
  public confDialogoEliminarComentario: DialogoCompartido;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;

  //interno
  public mostrarLoader: boolean;
  public mostrarError: boolean;
  public mensajeError: string;
  public mostrarNoHayItems: boolean;
  public puedeCargarMasIntercambio: boolean;
  public mostrarCargandoPequeno: boolean;

  // TEST comentarios
  public listaComentariosFirebase: Array<ComentarioIntercambioFirebaseModel>;
  public listaConfiguracionComentarios: Array<ConfiguracionComentario>;
  public indicadorTotalComentarios: IndicadorTotalComentarios;
  public coautores: Array<PerfilModel>;
  public eventoTapComentario: Function;
  public eventoTapPerfilComentario: Function;

  // Traducciones
  private tituloOriginal: string;
  private tituloCortoOriginal: string;
  private descripcionOriginal: string;
  private direccionOriginal: string;
  private esOriginal: boolean;

  // Enrutador
  public informacionEnrutador: InformacionEnrutador;

  constructor(
    private perfilNegocio: PerfilNegocio,
    private intercambioService: IntercambioService,
    private router: Router,
    private route: ActivatedRoute,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private albumNegocio: AlbumNegocio,
    private mediaNegocio: MediaNegocio,
    private intercambioNegocio: IntercambioNegocio,
    private ubicacionNegocio: UbicacionNegocio,
    private translateService: TranslateService,
    private generadorId: GeneradorId,
    private geoLocation: LocationService,
    private _location: Location,
    private metodosSessionStorageService: MetodosSessionStorageService,
    private metodosParaFotos: MetodosParaFotos,
    private enrutadorService: EnrutadorService,
    private imagenPantallaCompletaService: ImagenPantallaCompletaService,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private comentarioIntercambioNegocio: ComentarioIntercambioNegocio,
    public comenFireService: ComentariosIntercambioFirebaseService,
    private comentarioIntercambioFirebaseEntityMapperService: ComentarioIntercambioFirebaseEntityMapperService,
    private db: AngularFireDatabase
  ) {
    this.idCapaFormulario =
      'capa_formulario_' + this.generadorId.generarIdConSemilla();
    this.idCapaListaIntercambios = 'capa_perfil_intercambios';
    this.puedeCargarMas = true;
    this.params = { estado: false };
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.mensajeCapaError = '';
    this.imagenesDefecto = [];
    this.inputsForm = [];
    this.listaIntercambioModel = [];
    this.codigosTipoIntercambio = [];
    this.codigoIntercambioInicial = '';
    this.codigoAIntercambiar = '';
    this.codigoAIntercambiarDelProyecto = '';
    this.codigoAIntercambiarVisitar = '';
    this.codigoAIntercambiarDelProyecto = '';
    this.estadoPerfilParaMisIntercambios = ModoDelPerfil.DEFAULT;

    this.idPerfilCoautorParaResponder = '';
    this.idInternoParaResponder = '';
    this.listaDeFechas = [];
    this.formatoFecha = 'dd/MM/yyyy';

    this.listaComentariosFirebase = [];
    this.listaConfiguracionComentarios = [];
    this.coautores = [];
    this.esOriginal = false;
    this.mostrarLoader = false;
    this.mostrarError = false;
    this.mensajeError = '';
    this.puedeCargarMasIntercambio = true;
    this.mostrarNoHayItems = false;
    this.mostrarCargandoPequeno = false;
  }

  ngOnInit(): void {
    // this.configurarCodigosIntercambio()
    this.configurarParametrosDeLaUrl();
    this.inicializarPerfilSeleccionado();
    this.configurarDone();
    this.inicializarListaintercambio();
    this.configurarIndicadorTotalDeComentarios();
    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarToast();
      this.configurarAppBar(-1);
      this.configurarLineas();
      this.configurarBarraInferiorInline();
      this.configurarImagenPantallaCompleta();
      this.inicializarDataDeLaEntidad();
      this.configurarDialogoConfirmarSalida();
      this.configurarDialogoEliminarComentario();
      window.onbeforeunload = () =>
        this.validarInformacionDelIntercambioAntesDeRecargarOCambiarDePaginaHaciaDelante(
          this.params.accionEntidad !== AccionEntidad.CREAR,
          true
        );
    }
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this.intercambioNegocio.removerIntercambioActivoDelSessionStorage();
  }

  configurarParametrosDeLaUrl() {
    if (!this.informacionEnrutador || !this.informacionEnrutador.params) {
      this.params.estado = false;
    }

    this.informacionEnrutador.params.forEach((item) => {
      if (item.nombre === 'id') {
        this.params.id = item.valor;
      }

      if (item.nombre === 'accionEntidad') {
        this.params.accionEntidad = item.valor;
      }

      if (item.nombre === 'codigoTipoIntercambio') {
        this.params.codigoTipoIntercambio = item.valor;
      }
    });

    this.params =
      this.intercambioService.validarParametrosSegunAccionEntidadEnrutador(
        this.params
      );
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
    };
  }

  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  inicializarListaintercambio() {
    this.listaIntercambioModel = [];
    this.listaIntercambio = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }
  configurarDialogoConfirmarSalida(mostrarDialogo: boolean = false) {
    this.confDialogoSalida = {
      mostrarDialogo: mostrarDialogo,
      descripcion: 'm2v3texto21',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoSalida.mostrarDialogo = false;
            this.eventoEnBotonPublish(false);
          },
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoSalida.mostrarDialogo = false;
            this.validarAccionDespuesDeCambiosSegunElOrigen(false);
          },
        },
      ],
    };
  }

  async configurarDialogoEliminarComentario() {
    this.confDialogoEliminarComentario = {
      mostrarDialogo: false,
      completo: true,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: 'ERASE MESSAGE?',
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          enProgreso: false,
          ejecutar: () => {
            this.eliminarComentario();
          },
        },
        {
          text: 'm3v9texto3',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminarComentario.mostrarDialogo = false;
          },
        },
      ],
    };
    const texto = await this.translateService.get('m4v6texto36').toPromise();
    const texto2 = await this.translateService.get('m4v6texto37').toPromise();

    this.confDialogoEliminarComentario.descripcion = texto + '\n' + texto2;
  }

  configurarDone() {
    this.confDone = {
      mostrarDone: false,
      intervalo: 4000,
      mostrarLoader: false,
    };
  }

  // Configuracion del appbar
  async configurarAppBar(estatusSubTitulo: number, buscador?: BarraBusqueda) {
    let subtitulo: string =
      this.intercambioService.obtenerSubtituloAppBarSegunAccionEntidadParaIntercambio(
        this.params
      );

    if (estatusSubTitulo === 1) {
      subtitulo = 'm4v2texto2';
    }

    if (estatusSubTitulo === 2) {
      subtitulo = 'm4v17texto1';
    }

    this.confAppbar = {
      botonRefresh: true,
      eventoRefresh: () => {
        this.enrutadorService.refrescarTercio(
          this.informacionEnrutador.posicion
        );
      },
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras();
      },

      eventoHome: () => {
        if (
          (this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
            this.params.accionEntidad === AccionEntidad.CREAR) &&
          this.intercambio &&
          this.intercambio.perfil &&
          this.intercambio.perfil._id === this.perfilSeleccionado._id &&
          this.validarSiExistenCambios()
        ) {
          this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_HOME;
          this.configurarDialogoConfirmarSalida(true);
          return;
        }

        this.navegarAlHome(false);
      },
      searchBarAppBar: {
        tamanoColorFondo:
          estatusSubTitulo === 3
            ? TamanoColorDeFondo.TAMANO6920
            : TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: subtitulo,
        },
        buscador: buscador
          ? buscador
          : {
              mostrar: false,
              configuracion: {
                disable: true,
              },
            },
        idiomaOriginal: {
          mostrarOriginal: false,
          llaveTexto: 'm4v1texto2',
          clickMostrarOriginal: () => {
            if (this.esOriginal) {
              this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto =
                'm4v1texto2';
              this.intercambio.tituloCorto = this.tituloCortoOriginal;
              this.intercambio.titulo = this.tituloOriginal;
              this.intercambio.descripcion = this.descripcionOriginal;
              this.intercambio.direccion.descripcion = this.direccionOriginal;
              this.intercambioForm =
                this.intercambioService.inicializarControlesFormularioAccionActualizar(
                  this.intercambio
                );
              this.inputsForm =
                this.intercambioService.configurarInputsDelFormularioConKey(
                  this.intercambioForm,
                  true
                );
              this.esOriginal = false;

              return;
            }

            if (!this.esOriginal) {
              this.esOriginal = true;
              this.tituloOriginal = this.intercambio.titulo;
              this.tituloCortoOriginal = this.intercambio.tituloCorto;
              this.descripcionOriginal = this.intercambio.descripcion;
              this.direccionOriginal = this.intercambio.direccion.descripcion;

              if (
                this.intercambio.tituloCortoOriginal &&
                this.intercambio.tituloOriginal &&
                this.intercambio.descripcionOriginal
                //  && this.proyecto.direccion.descripcionOriginal
              ) {
                this.intercambio.tituloCorto =
                  this.intercambio?.tituloCortoOriginal;
                this.intercambio.titulo = this.intercambio?.tituloOriginal;
                this.intercambio.descripcion =
                  this.intercambio?.descripcionOriginal;
                this.intercambio.direccion.descripcion =
                  this.intercambio.direccion?.descripcionOriginal;
              }

              this.intercambioForm =
                this.intercambioService.inicializarControlesFormularioAccionActualizar(
                  this.intercambio
                );
              this.inputsForm =
                this.intercambioService.configurarInputsDelFormularioConKey(
                  this.intercambioForm,
                  true
                );
              this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto =
                'm4v1texto2.1';
              return;
            }
          },
        },
      },
    };
  }

  accionAtras() {
    if (
      this.estadoPerfilParaMisIntercambios ===
      ModoDelPerfil.AMPLIAR_INTERCAMBIOS
    ) {
      // this.configurarListaProyectos()
      // this.configurarProyectosDelPerfil()
      this.estadoPerfilParaMisIntercambios = ModoDelPerfil.DEFAULT;
      // this.actualizarHitorialDelPerfilActual()
      return;
    }
    if (
      (this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
        this.params.accionEntidad === AccionEntidad.CREAR) &&
      this.intercambio &&
      this.intercambio.perfil &&
      this.intercambio.perfil._id === this.perfilSeleccionado._id &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_BACK;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    this.navegarAlBack();
  }

  inicializarDataDeLaEntidad() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.inicializarContenidoParaAccionCrear();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.inicializarContenidoParaAccionActualizar();
        break;
      case AccionEntidad.VISITAR:
        this.inicializarContenidoParaAccionVisitar();
        break;
      default:
        break;
    }
  }

  async inicializarContenidoParaAccionCrear() {
    this.intercambio =
      this.intercambioNegocio.validarIntercambioActivoSegunAccionCrear(
        this.params.codigoTipoIntercambio,
        this.perfilSeleccionado,
        this.informacionEnrutador.posicion
      );

    // Utils
    this.listarMisIntercambios();
    this.inicializarControlesSegunAccion();
    this.inicializarInputs();
    // // Componentes hijos
    this.configurarPortadaExpandida();
    this.configurarSelector();
    this.configurarBuscadorLocalidades();
    this.configurarListaPaginacionLocalidades();
    this.configurarBotones();
    this.inicializarImagenesPorDefecto();
    await this.inicializarTiposIntercambio();
    this.definirTipoIntercambioInicial();
    // this.configurarMapaCrear()
    // this.infoPropietarioIntercambio = this.configuracionPropietarioIntercambio()
  }

  //COMENTARIOS START

  configurarEventosComentario() {
    this.eventoTapComentario = (comentario: ComentarioModel) => {
      this.comentarioAEliminar = comentario;
      this.confDialogoEliminarComentario.mostrarDialogo = true;
    };

    this.eventoTapPerfilComentario = (
      idPerfilCoautor: string,
      idInterno: string
    ) => {
      if (idPerfilCoautor === this.perfilSeleccionado._id) {
        return;
      }

      const index = this.listaConfiguracionComentarios.findIndex(
        (e) => e.coautor._id === idPerfilCoautor && e.idInterno == idInterno
      );

      this.listaConfiguracionComentarios.forEach((item) => {
        item.couatorSeleccionado = false;
      });

      if (index < 0 || idPerfilCoautor.length === 0 || idInterno.length === 0) {
        return;
      }

      if (
        idPerfilCoautor === this.idPerfilCoautorParaResponder &&
        idInterno === this.idInternoParaResponder
      ) {
        this.listaConfiguracionComentarios[index].couatorSeleccionado = false;
        this.idPerfilCoautorParaResponder = '';
        this.idInternoParaResponder = '';
        return;
      }

      if (
        idInterno !== this.idInternoParaResponder &&
        (idPerfilCoautor !== this.idPerfilCoautorParaResponder ||
          idPerfilCoautor === this.idPerfilCoautorParaResponder)
      ) {
        this.listaConfiguracionComentarios[index].couatorSeleccionado = true;
        this.idPerfilCoautorParaResponder = idPerfilCoautor;
        this.idInternoParaResponder = idInterno;
        return;
      }
    };
  }

  async obtenerTotalDeComentarios() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.totalComentarios.ejecutar$.next({
      idIntercambio: this.params.id,
      estado: CodigosCatalogosEstadoComentarioIntercambio.ACTIVA,
    });
  }

  async configurarEscuchaTotalDeComentarios() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.totalComentarios.subscripcion$ =
      this.comenFireService.totalComentarios.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.TOTAL_COMENTARIOS);

          if (data) {
            this.configurarIndicadorTotalDeComentarios(
              data.length,
              data.map((c) => c.key)
            );
          }
        },
        (error) => {
          this.configurarIndicadorTotalDeComentarios();
        }
      );
  }

  async obtenerComentarios() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.puedeCargarMas = false;
    this.comenFireService.comentarios.ejecutar$.next(this.params.id);
  }

  async configurarEscuchaListaDeComentarios() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.comentarios.subscripcion$ =
      this.comenFireService.comentarios.respuesta$.subscribe(
        (data) => {
          if (!data || data.length === 0) {
            this.puedeCargarMas = true;
            return;
          }
          this.configurarListaDeComentarios(data);
          this.puedeCargarMas = true;
        },
        (error) => {
          this.listaDeFechas = [];
          this.listaComentariosFirebase = [];
          this.listaConfiguracionComentarios = [];
        }
      );
  }

  async configurarListaDeComentarios(
    actions: AngularFireAction<DataSnapshot>[]
  ) {
    try {
      const data = actions.reverse();
      data.forEach((item) => {
        const comentario: ComentarioFirebaseModel =
          this.comentarioIntercambioFirebaseEntityMapperService.transform(
            item.payload.val() as ComentarioFirebaseEntity
          );

        const index = this.listaComentariosFirebase.findIndex(
          (e) => e.id === comentario.id
        );

        if (
          comentario.estado.codigo ===
          CodigosCatalogosEstadoComentarioIntercambio.ACTIVA
        ) {
          if (index < 0) {
            this.listaComentariosFirebase.push(comentario);
          }

          const indexDos = this.indicadorTotalComentarios.comentarios.findIndex(
            (e) => e === comentario.id
          );
          if (indexDos < 0) {
            this.indicadorTotalComentarios.total += 1;
            this.indicadorTotalComentarios.comentarios.unshift(comentario.id);
          }
        } else {
          this.eliminarComentariosDeListas(comentario.id, index);
        }
      });

      this.listaComentariosFirebase =
        this.comenFireService.ordenarComentariosDeFormaDescendente(
          this.listaComentariosFirebase
        );

      this.listaDeFechas =
        this.comenFireService.configurarListaDeFechasComentarios(
          this.listaDeFechas,
          this.listaComentariosFirebase
        );

      this.listaConfiguracionComentarios =
        this.comenFireService.crearConfigurarDeLosComentarios(
          this.listaDeFechas,
          this.listaComentariosFirebase,
          this.listaConfiguracionComentarios,
          this.intercambio.perfil._id,
          this.perfilSeleccionado._id,
          false,
          this.eventoTapComentario,
          this.eventoTapPerfilComentario
        );
    } catch (error) {}
  }
  eliminarComentariosDeListas(idComentario: string, index: number = -1) {
    if (index >= 0) {
      this.listaComentariosFirebase.splice(index, 1);
    }

    // Actualizar indicador del total
    this.indicadorTotalComentarios =
      this.comenFireService.actualizarIndicadorDelTotalDeComentarios(
        idComentario,
        this.indicadorTotalComentarios
      );

    // Actualizar lista de configuraciones
    const listas =
      this.comenFireService.actualizarListaDeConfiguracionDeComentarios(
        idComentario,
        this.listaDeFechas,
        this.listaConfiguracionComentarios
      );

    this.listaDeFechas = listas.listaDeFechas;
    this.listaConfiguracionComentarios = listas.listaConfiguracionComentarios;
  }

  async obtenerComentariosPaginacion() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.paginacion.ejecutar$.next(this.params.id);
  }

  async obtenerListaDeComentariosEnPaginacion() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.paginacion.subscripcion$ =
      this.comenFireService.paginacion.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.PAGINACION);
          if (!data || data.length === 0) {
            this.puedeCargarMas = true;
            return;
          }

          this.configurarListaDeComentarios(data);
          this.puedeCargarMas = true;
        },
        (error) => {
          this.puedeCargarMas = true;
          this.comenFireService.paginacionComentarios -= 30;
        }
      );
  }

  configurarIndicadorTotalDeComentarios(
    total: number = 0,
    comentarios: Array<string> = []
  ) {
    this.indicadorTotalComentarios = {
      total: total,
      comentarios: comentarios,
    };
  }

  configurarLineas() {
    this.confLineaComentarios = {
      ancho: AnchoLineaItem.ANCHO100,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  //COMENTARIOS END

  definirTipoIntercambioInicial() {
    let mitipoIntercambio = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.params.codigoTipoIntercambio
    );
    let miTipoAIntercambiar = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.intercambio.tipoIntercambiar.codigo
    );
    // this.codigoIntercambioInicial = mitipoIntercambio[0].nombre

    this.codigoIntercambioInicial =
      this.internacionalizacionNegocio.obtenerTextoSincrono('m6v8texto13', {
        nombreIntercambio: mitipoIntercambio[0].nombre,
      });
    this.codigoAIntercambiarDelProyecto = 'm6v8texto14';
  }

  definirTipoIntercambioInicialActualizar() {
    let mitipoIntercambio = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.intercambio.tipo.codigo
    );

    let miTipoAIntercambiar = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.intercambio.tipoIntercambiar.codigo
    );
    // this.codigoIntercambioInicial = mitipoIntercambio[0].nombre
    this.codigoIntercambioInicial =
      this.internacionalizacionNegocio.obtenerTextoSincrono('m6v8texto13', {
        nombreIntercambio: mitipoIntercambio[0].nombre,
      });
    this.codigoAIntercambiar = miTipoAIntercambiar[0].codigo;
    this.codigoAIntercambiarDelProyecto = miTipoAIntercambiar[0].nombre;
    this.codigoAIntercambiarDelProyecto = miTipoAIntercambiar[0].codigo;
  }
  definirTipoIntercambioInicialVisitar() {
    let mitipoIntercambio = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.intercambio.tipo.codigo
    );

    let miTipoAIntercambiar = this.codigosTipoIntercambio.filter(
      (element) => element.codigo === this.intercambio.tipoIntercambiar.codigo
    );
    let intercambioPor = this.internacionalizacionNegocio.obtenerTextoSincrono(
      'm6v8texto13.1',
      { nombreIntercambio1: mitipoIntercambio[0].nombre }
    );
    // this.codigoIntercambioInicial = mitipoIntercambio[0].nombre
    this.codigoAIntercambiarVisitar =
      intercambioPor + ' ' + miTipoAIntercambiar[0].nombre;
    // this.codigoAIntercambiarVisitar = this.internacionalizacionNegocio.obtenerTextoSincrono('m6v8tethis.internacionalizacionNegocio.obtenerTextoSincrono('m6v8texto13.1', { nombreIntercambio1: mitipoIntercambio[0].nombre })xto13.1', { nombreIntercambio2: miTipoAIntercambiar[0].nombre })
    // this.codigoAIntercambiar = miTipoAIntercambiar[0].nombre
  }

  detectarScroolEnListaIntercambios() {
    try {
      const elemento = document.getElementById(
        this.idCapaListaIntercambios
      ) as HTMLElement;
      if (!elemento) {
        return;
      }

      if (elemento.scrollTop === 0) {
        return;
      }

      if (
        elemento.offsetHeight + elemento.scrollTop >=
          elemento.scrollHeight - 1.78 &&
        !this.listaIntercambio.cargando
      ) {
        if (
          this.estadoPerfilParaMisIntercambios ===
          ModoDelPerfil.AMPLIAR_INTERCAMBIOS
        ) {
          this.listarMisIntercambios();
          return;
        }
      }
    } catch (error) {}
  }

  async inicializarTiposIntercambio() {
    this.codigosTipoIntercambio = await this.intercambioNegocio
      .tiposIntercambio()
      .toPromise();
  }

  async listarMisIntercambios() {
    if (!this.listaIntercambio.proximaPagina) {
      this.puedeCargarMasIntercambio = false;
      this.mostrarLoader = false;
      this.mostrarCargandoPequeno = false;
      return;
    }

    try {
      this.mostrarNoHayItems = false;
      this.mostrarLoader = this.listaIntercambio.lista.length === 0;
      this.mostrarCargandoPequeno = this.listaIntercambio.lista.length > 0;

      const dataPaginacion = await this.intercambioNegocio
        .buscarMisIntercambiosTipo(
          this.params.codigoTipoIntercambio,
          this.perfilSeleccionado._id,
          10,
          this.listaIntercambio.paginaActual
        )
        .toPromise();

      console.log(dataPaginacion);

      this.listaIntercambio.totalDatos = dataPaginacion.totalDatos;
      this.listaIntercambio.proximaPagina = dataPaginacion.proximaPagina;

      dataPaginacion.lista.forEach((intercambio) => {
        this.listaIntercambioModel.push(intercambio);
        this.listaIntercambio.lista.push(
          this.configurarItemListaIntercambio(intercambio)
        );
      });

      this.mostrarLoader = false;
      this.mostrarCargandoPequeno = false;
      this.puedeCargarMasIntercambio = true;
      this.mostrarNoHayItems = this.listaIntercambio.lista.length === 0;

      if (this.listaIntercambio.proximaPagina) {
        this.listaIntercambio.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarLoader = false;
      this.mostrarCargandoPequeno = false;
      this.puedeCargarMasIntercambio = false;
    }
  }

  configurarItemListaIntercambio(
    intercambio: IntercambioModel
  ): ConfiguracionItemIntercambio {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      intercambio.adjuntos
    );

    return {
      id: intercambio.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(intercambio.fechaCreacion),
          formato: 'dd/MM/yyyy',
        },
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: intercambio.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      urlMedia:
        album && album.portada
          ? album.portada.principal.url || album.portada.miniatura.url || ''
          : '',
      usoItem: UsoItemIntercambio.REC_INTERC,
      loader:
        album &&
        album.portada &&
        ((album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url))
          ? true
          : false,
      eventoTap: {
        activo: true,
        evento: (data: ConfiguracionItemIntercambio) => {
          this.intercambioNegocio.removerIntercambioActivoDelSessionStorage();
        },
      },
      eventoDobleTap: {
        activo: false,
      },
      eventoPress: {
        activo: false,
      },
    };
  }

  obtenerConfiguracionRectangulo(
    entidad: any,
    predeterminado: boolean = false
  ): ConfiguracionItemIntercambio {
    let album: AlbumModel;

    if (predeterminado) {
      album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
        entidad.adjuntos
      );
    }

    if (!album) {
      album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        entidad.adjuntos
      );
    }

    const urlMedia = this.obtenerUrlMedia(album);

    return {
      usoVersionMini: true,
      id: entidad.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: false,
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: false,
      },
      urlMedia: urlMedia,
      usoItem: UsoItemIntercambio.REC_INTERC,
      loader: urlMedia.length > 0,
      eventoTap: {
        activo: false,
      },
      eventoDobleTap: {
        activo: false,
      },
      eventoPress: {
        activo: false,
      },
    };
  }

  obtenerUrlMedia(album: AlbumModel): string {
    try {
      if (!album.portada) {
        throw new Error('');
      }

      if (album.portada.miniatura) {
        return album.portada.miniatura.url;
      }

      if (album.portada.principal) {
        return album.portada.principal.url;
      }

      return '';
    } catch (error) {
      return '';
    }
  }

  async inicializarContenidoParaAccionActualizar() {
    try {
      this.mostrarCapaLoader = true;

      this.intercambio = await this.intercambioNegocio
        .obtenerInformacionDelIntercambio(
          this.params.id,
          this.perfilSeleccionado._id
        )
        .toPromise();

      this.intercambioNegocio.validarIntercambioActivoSegunAccionActualizarVisitar(
        this.intercambio,
        this.informacionEnrutador.posicion
      );

      if (
        !this.intercambio.id ||
        !this.intercambio.tipo ||
        (this.perfilSeleccionado &&
          this.intercambio.perfil &&
          this.perfilSeleccionado._id !== this.intercambio.perfil._id)
      ) {
        this.intercambioNegocio.removerIntercambioDeIntercambioActivoTercios(
          this.informacionEnrutador.posicion
        );
        throw new Error('text31');
      }

      // this.mostrarCapaLoader = true
      // this.intercambio = await this.intercambioNegocio.obtenerInformacionDelIntercambio(
      //     this.params.id,
      //     this.perfilSeleccionado._id
      // ).toPromise()

      // this.intercambioNegocio.removerIntercambioActivoDelSessionStorage()
      // this.intercambioNegocio.guardarintercambioActivoEnSessionStorage(this.intercambio)

      // if (!this.intercambio.id || !this.intercambio.tipo) {

      //     this.intercambioNegocio.removerIntercambioActivoDelSessionStorage()
      //     throw new Error('text31')
      // }

      // if (this.perfilSeleccionado && this.intercambio.perfil && this.perfilSeleccionado._id !== this.intercambio.perfil._id) {
      //     this.intercambioNegocio.removerIntercambioActivoDelSessionStorage()

      //     throw new Error('text31')
      // }
      // Utils
      this.inicializarControlesSegunAccion();
      this.inicializarInputs();
      this.configurarPortadaExpandida();
      this.inicializarImagenesPorDefecto();
      await this.inicializarTiposIntercambio();
      this.definirTipoIntercambioInicialActualizar();
      this.configurarSelector();
      this.configurarBuscadorLocalidades();
      this.configurarListaPaginacionLocalidades();
      this.configurarBotones();
      this.configurarDialogoHabilitar();
      this.configurarDialogoDeshabilitar();
      this.configurarDialogoEliminar();
      this.configurarMapaCrear(
        this.intercambio.direccion.latitud,
        this.intercambio.direccion.longitud
      );
      this.configurarEventosComentario();
      this.configurarEscuchaTotalDeComentarios();
      this.obtenerTotalDeComentarios();
      this.configurarEscuchaListaDeComentarios();
      this.obtenerComentarios();

      this.mostrarCapaLoader = false;
      if (this.confBarraInferior) {
        this.confBarraInferior.desactivarBarra =
          this.configurarEstadoDeLaBarraInferior();
      }
    } catch (error) {
      this.mensajeCapaError = 'text31';
      this.mostrarCapaLoader = false;
      this.mostrarCapaError = true;
    }
  }

  async inicializarContenidoParaAccionVisitar() {
    try {
      this.mostrarCapaLoader = true;
      this.intercambio = await this.intercambioNegocio
        .obtenerInformacionDelIntercambio(
          this.params.id,
          this.perfilSeleccionado._id
        )
        .toPromise();

      this.intercambioNegocio.validarIntercambioActivoSegunAccionActualizarVisitar(
        this.intercambio,
        this.informacionEnrutador.posicion
      );

      if (!this.intercambio.id || !this.intercambio.tipo) {
        this.intercambioNegocio.removerIntercambioDeIntercambioActivoTercios(
          this.informacionEnrutador.posicion
        );
        throw new Error('text31');
      }

      // Utils
      this.configurarAppBar(-1);
      this.inicializarControlesSegunAccion();
      this.inicializarInputs();
      // Componentes hijos
      this.configurarPortadaExpandidaParaVisita();
      this.inicializarImagenesPorDefecto();
      await this.inicializarTiposIntercambio();
      this.configurarSelector();
      this.configurarBuscadorLocalidades();
      this.configurarBotones();
      this.definirTipoIntercambioInicialVisitar();
      this.configurarMapaCrear(
        this.intercambio.direccion.latitud,
        this.intercambio.direccion.longitud,
        true
      );
      this.infoPropietarioIntercambio =
        this.configuracionPropietarioIntercambio();
      this.configurarEventosComentario();
      this.configurarEscuchaTotalDeComentarios();
      this.obtenerTotalDeComentarios();
      this.configurarEscuchaListaDeComentarios();
      this.obtenerComentarios();

      this.mostrarCapaLoader = false;
      if (this.confBarraInferior) {
        this.confBarraInferior.desactivarBarra =
          this.configurarEstadoDeLaBarraInferior();
      }

      if (
        this.intercambio.tituloCortoOriginal &&
        this.intercambio.tituloOriginal &&
        this.intercambio.descripcionOriginal
      ) {
        this.confAppbar.searchBarAppBar.idiomaOriginal.mostrarOriginal = true;
      }
    } catch (error) {
      this.mensajeCapaError = 'text31';
      this.mostrarCapaLoader = false;
      this.mostrarCapaError = true;
    }
  }

  // Inicializar controles del formulario
  inicializarControlesSegunAccion() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.intercambioForm =
          this.intercambioService.inicializarControlesFormularioAccionCrear(
            this.intercambio
          );

        break;
      case AccionEntidad.ACTUALIZAR:
        this.intercambioForm =
          this.intercambioService.inicializarControlesFormularioAccionActualizar(
            this.intercambio
          );
        break;
      case AccionEntidad.VISITAR:
        // this.intercambioForm = this.intercambioService.inicializarControlesFormularioAccionActualizar(this.intercambio)
        break;
      default:
        break;
    }
  }

  // Inicializar los inputs
  async inicializarInputs() {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.inputsForm =
          this.intercambioService.configurarInputsDelFormularioConKey(
            this.intercambioForm
          );

        break;
      case AccionEntidad.ACTUALIZAR:
        this.inputsForm =
          this.intercambioService.configurarInputsDelFormularioConKey(
            this.intercambioForm
          );
        break;
      case AccionEntidad.VISITAR:
        this.inputsForm =
          this.intercambioService.configurarInputsDelFormularioConKey(
            this.intercambioForm,
            true
          );
        break;
      default:
        break;
    }
  }

  configuracionPropietarioIntercambio(): ConfiguracionItemListaContactosCompartido {
    let usoCirculo: UsoItemCircular;
    let urlMedia: string;

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.PERFIL,
      this.intercambio.perfil.album
    );

    if (
      !album ||
      (album.portada &&
        album.portada.principal &&
        album.portada.principal.fileDefault)
    ) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      urlMedia = album.portada.principal.url;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
      urlMedia = album.portada.principal.url;
    }

    return {
      id: this.intercambio.perfil._id || '',
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: this.intercambio.perfil.nombreContacto,
        nombreContactoTraducido:
          this.intercambio.perfil.nombreContactoTraducido,
        nombre: this.intercambio.perfil.nombre,
        estilosTextoSuperior: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: ColorDelTexto.TEXTONEGRO,
          estiloTexto: EstilosDelTexto.REGULAR,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: this.intercambio.perfil._id || '',
      },
      configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
        urlMedia,
        ColorDeBorde.BORDER_ROJO,
        this.metodosParaFotos.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        true
      ),
      mostrarX: {
        mostrar: false,
        color: true,
      },
      eventoCirculoNombre: () => {
        // this.abrirChatValidandoNotificaciones(idAsociacion)
      },
    };
  }

  async inicializarImagenesPorDefecto() {
    try {
      const archivos = this.mediaNegocio.obtenerArchivosDefaultPorTipo(
        CodigosCatalogoArchivosPorDefecto.PROYECTOS
      );

      if (!archivos || archivos === null) {
        throw new Error('');
      }

      this.imagenesDefecto = archivos;
      this.definirImagenDeLaPortadaExpandida();
    } catch (error) {
      this.imagenesDefecto = [];
      this.definirImagenDeLaPortadaExpandida();
    }
  }
  definirImagenDeLaPortadaExpandida() {
    const data = this.intercambioService.determinarUrlImagenPortada(
      this.intercambio,
      this.imagenesDefecto
    );
    this.confPortadaExpandida.urlMedia = data.url;
    this.confPortadaExpandida.mostrarLoader =
      this.confPortadaExpandida.urlMedia.length > 0;

    this.confPortadaExpandida.bloques.forEach((bloque, pos) => {
      if (pos === 0) {
        bloque.colorFondo = data.porDefecto
          ? ColoresBloquePortada.AZUL_FUERTE
          : ColoresBloquePortada.AZUL_FUERTE_CON_OPACIDAD;
      }

      if (pos === 1) {
        bloque.colorFondo = data.porDefecto
          ? ColoresBloquePortada.AZUL_DEBIL
          : ColoresBloquePortada.AZUL_DEBIL_CON_OPACIDAD;
      }
    });
  }

  configurarMapaCrear(
    latitud: number,
    longitud: number,
    visitar: boolean = false
  ) {
    this.confMapa = {
      latitudInicial: latitud,
      longitudInicial: longitud,
      mostrarMarker: true,
      visitar: visitar,
      nuevasPosiciones: (longitud: number, latitud: number) => {
        this.intercambio.direccion.longitud = longitud;
        this.intercambio.direccion.latitud = latitud;
      },
    };
  }

  configurarPortadaExpandida() {
    this.confPortadaExpandida = {
      urlMedia: '',
      mostrarLoader: false,
      bloques: [
        {
          tipo: TipoBloqueBortada.BOTON_PROYECTO,
          colorFondo: ColoresBloquePortada.AZUL_FUERTE,
          conSombra: SombraBloque.SOMBRA_NEGRA,
          llaveTexto: 'm6v8texto5',
          eventoTap: () => this.irAlAlbumGeneral(),
        },
      ],
    };
  }

  configurarPortadaExpandidaParaVisita() {
    this.confPortadaExpandida = {
      urlMedia: '',
      mostrarLoader: false,
      bloques: [],
      eventoTapPortada: {
        activarEvento: false,
        evento: () => {
          this.irAlAlbumGeneral();
        },
      },
      eventoDobleTapPortada: {
        activarEvento: true,
        evento: () => {
          this.validarAccionDobleTapEnPortadaExpandida();
        },
      },
    };

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      this.intercambio.adjuntos
    );

    if (!album || (album && album.media && album.media.length === 0)) {
      const bloque: BloquePortada = {
        colorFondo: ColoresBloquePortada.AZUL_DEBIL,
        llaveTexto: 'm4v1texto5',
        tipo: TipoBloqueBortada.NO_ADDED_INFO,
      };
      this.confPortadaExpandida.bloques.push(bloque);
    }
  }

  validarAccionDobleTapEnPortadaExpandida() {
    if (this.intercambio && this.intercambio.adjuntos) {
      const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        this.intercambio.adjuntos
      );

      if (!album) {
        return;
      }

      this.irAlAlbumGeneral();
      // if (album.media && album.media.length === 1) {
      //     if (!this.imagenPantallaCompletaService.confImagenPantallaCompleta) {
      //         this.toast.abrirToast('text37')
      //         return
      //     }
      //     this.imagenPantallaCompletaService.configurarImagenPantallaCompleta(
      //         true, true, this.confPortadaExpandida.urlMedia
      //     )
      // } else {
      //     this.irAlAlbumGeneral()
      // }
    }
  }

  configurarImagenPantallaCompleta() {
    this.confImagenPantallaCompleta = {
      mostrar: false,
      mostrarLoader: true,
      urlMedia: '',
      eventoBotonCerrar: () => {
        this.confImagenPantallaCompleta.mostrar = false;
        this.confImagenPantallaCompleta.mostrarLoader = true;
        this.confImagenPantallaCompleta.urlMedia = '';
      },
      eventoBotonDescargar: () => {
        // let link = document.createElement('a');
        // // const nombre = mensaje.adjuntos[0].principal.filename.toString()
        // link.href = this.confImagenPantallaCompleta.urlMedia;
        // // link.download = nombre;
        // link.dispatchEvent(new MouseEvent('click'));

        let link = document.createElement('a');
        link.href = this.confImagenPantallaCompleta.urlMedia;
        link.target = '_blank';
        link.dispatchEvent(
          new MouseEvent('click', {
            view: window,
            bubbles: false,
            cancelable: true,
          })
        );
      },
    };
  }

  validarSiHayMediasEnElAlbum(): boolean {
    try {
      const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        this.intercambio.adjuntos
      );

      if (!album) {
        throw new Error('');
      }

      return album.media.length > 0;
    } catch (error) {
      return false;
    }
  }

  irAlAlbumAudios(validarCambios: boolean = true) {
    // Se valida el album
    const album: AlbumModel = this.albumNegocio.validarAlbumEnIntercambioActivo(
      CodigosCatalogoTipoAlbum.AUDIOS,
      this.informacionEnrutador.posicion
    );

    if (
      !album ||
      (this.params.accionEntidad === AccionEntidad.VISITAR &&
        album.estado &&
        album.estado.codigo &&
        album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR)
    ) {
      return;
    }

    if (
      this.intercambio.adjuntos.findIndex(
        (e) => e.tipo.codigo === album.tipo.codigo
      ) < 0
    ) {
      this.intercambio.adjuntos.push(album);
    }

    if (
      validarCambios &&
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios =
        OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      this.validarInformacionDelIntercambioAntesDeRecargarOCambiarDePaginaHaciaDelante();
    }

    let titulo =
      this.intercambio.tituloCorto.length > 0
        ? this.intercambio.tituloCorto
        : 'm3v10texto7';
    // titulo =  titulo.replace(/[^a-zA-Z 0-9.]+/g,' ')
    titulo = titulo.replace(/[&\/\\#,+()$~%.'":*?<>{}]_|#|-|@|<>/g, '');

    let accionEntidad: AccionEntidad;
    if (this.params.accionEntidad === AccionEntidad.VISITAR) {
      accionEntidad = AccionEntidad.VISITAR;
    } else {
      accionEntidad = !album._id
        ? AccionEntidad.CREAR
        : AccionEntidad.ACTUALIZAR;
    }

    this.enrutadorService.navegarConPosicionFija(
      {
        componente: AlbumAudiosComponent,
        params: [
          {
            nombre: 'titulo',
            valor: titulo,
          },
          {
            nombre: 'accionEntidad',
            valor: accionEntidad,
          },
          {
            nombre: 'entidad',
            valor: CodigosCatalogoEntidad.INTERCAMBIO,
          },
        ],
      },
      this.informacionEnrutador.posicion
    );

    // let ruta = ''

    // if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
    //   if (!album._id) {
    //     ruta = RutasLocales.MODULO_ALBUM.toString() + '/' + RutasAlbumAudios.CREAR.toString()
    //     ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.INTERCAMBIO)
    //     ruta = ruta.replace(':titulo', titulo)
    //   } else {
    //     ruta = RutasLocales.MODULO_ALBUM.toString() + '/' + RutasAlbumAudios.ACTUALIZAR.toString()
    //     ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.INTERCAMBIO)
    //     ruta = ruta.replace(':titulo', titulo)
    //   }
    // } else {
    //   ruta = RutasLocales.MODULO_ALBUM.toString() + '/' + RutasAlbumAudios.VISITAR.toString()
    //   ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.INTERCAMBIO)
    //   ruta = ruta.replace(':titulo', titulo)
    // }

    // this.router.navigateByUrl(ruta)
  }

  irAlAlbumGeneral(validarCambios: boolean = true) {
    const album: AlbumModel = this.albumNegocio.validarAlbumEnIntercambioActivo(
      CodigosCatalogoTipoAlbum.GENERAL,
      this.informacionEnrutador.posicion
    );

    if (
      !album ||
      (this.params.accionEntidad === AccionEntidad.VISITAR &&
        album.estado &&
        album.estado.codigo &&
        album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR)
    ) {
      return;
    }

    if (
      this.intercambio.adjuntos.findIndex(
        (e) => e.tipo.codigo === album.tipo.codigo
      ) < 0
    ) {
      this.intercambio.adjuntos.push(album);
    }

    if (
      validarCambios &&
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios =
        OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      this.validarInformacionDelIntercambioAntesDeRecargarOCambiarDePaginaHaciaDelante();
    }

    // Se define los parametros del album
    let titulo =
      this.intercambio.tituloCorto.length > 0
        ? this.intercambio.tituloCorto
        : 'm3v10texto7';
    // titulo =  titulo.replace(/[^a-zA-Z 0-9.]+/g,' ')
    titulo = titulo.replace(/[&\/\\#,+()$~%.'":*?<>{}]_|#|-|@|<>/g, '');

    let accionEntidad: AccionEntidad;
    if (this.params.accionEntidad === AccionEntidad.VISITAR) {
      accionEntidad = AccionEntidad.VISITAR;
    } else {
      accionEntidad = !album._id
        ? AccionEntidad.CREAR
        : AccionEntidad.ACTUALIZAR;
    }

    this.enrutadorService.navegarConPosicionFija(
      {
        componente: AlbumGeneralComponent,
        params: [
          {
            nombre: 'titulo',
            valor: titulo,
          },
          {
            nombre: 'accionEntidad',
            valor: accionEntidad,
          },
          {
            nombre: 'entidad',
            valor: CodigosCatalogoEntidad.INTERCAMBIO,
          },
        ],
      },
      this.informacionEnrutador.posicion
    );
  }

  validarSiExistenCambios(): boolean {
    const proyectoAux: IntercambioModel =
      this.intercambioNegocio.asignarValoresDeLosCamposAlintercambioParaValidarCambios(
        { ...this.intercambio },
        this.confSelector.seleccionado,
        this.confBuscador.seleccionado,
        this.intercambioForm
      );

    const resultado =
      this.intercambioNegocio.validarSiExistenCambiosEnElIntercambio(
        proyectoAux,
        this.params.accionEntidad,
        this.informacionEnrutador.posicion
      );

    return resultado;
  }

  configurarEstadoDeLaBarraInferior() {
    if (this.mostrarCapaError || this.mostrarCapaLoader) {
      return true;
    }

    if (
      this.intercambio &&
      this.intercambio.estado &&
      this.intercambio.estado.codigo
    ) {
      return false;
    }

    return true;
  }

  configurarBarraInferiorInline() {
    this.confBarraInferior = {
      desactivarBarra: this.configurarEstadoDeLaBarraInferior(),
      capaColorFondo: {
        mostrar: true,
        anchoCapa: TamanoColorDeFondo.TAMANO100,
        colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE,
      },
      estatusError: (error: string) => {},
      placeholder: {
        mostrar: true,
        texto: 'm3v10texto15',
        configuracion: {
          color: ColorDelTexto.TEXTOROJOBASE,
          enMayusculas: true,
          estiloTexto: EstilosDelTexto.REGULAR,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
        },
      },
      iconoTexto: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_TEXTO,
          eventoTap: (dataApiArchivo: SubirArchivoData) => {
            if (
              dataApiArchivo.descripcion &&
              dataApiArchivo.descripcion.length > 0
            ) {
              this.enviarComentario(dataApiArchivo);
            }
          },
        },
        capa: {
          mostrar: false,
        },
      },
      iconoAudio: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_AUDIO,
          eventoTap: (dataApiArchivo: SubirArchivoData) => {
            this.enviarComentario(dataApiArchivo, false);
          },
        },
        capa: {
          siempreActiva: false,
          grabadora: {
            noPedirTituloGrabacion: true,
            usarLoader: true,
            grabando: false,
            duracionActual: 0,
            tiempoMaximo: 300,
            factorAumentoLinea: 100 / 300, // 100% del ancho divido para tiempoMaximo
          },
          clickParar: true,
        },
      },
      botonSend: {
        mostrar: true,
        evento: (dataApiArchivo: SubirArchivoData) => {
          this.enviarComentario(dataApiArchivo);
        },
      },
    };
  }

  async configurarSelector() {
    const item: ItemSelector =
      this.intercambioService.obtenerPaisSeleccionadoEnElIntercambio(
        this.intercambio
      );

    this.confSelector = {
      tituloSelector: 'text61',
      mostrarModal: false,
      inputPreview: {
        mostrar: true,
        input: {
          valor: item.nombre,
          placeholder: 'Country:',
          textoBold: true,
          textoEnMayusculas: true,
        },
      },
      seleccionado: item,
      elegibles: [],
      cargando: {
        mostrar: false,
      },
      error: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false,
      },
      quitarMarginAbajo: true,
      evento: (data: InfoAccionSelector) => this.eventoEnSelector(data),
    };

    this.confSelector.inputPreview.input.placeholder =
      await this.translateService.get('m4v5texto10').toPromise();
  }

  async configurarBuscadorLocalidades() {
    // Definir direccion

    let item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' };
    let pais: ItemSelector = { codigo: '', nombre: '', auxiliar: '' };

    if (
      this.intercambio.direccion.localidad?.nombre !== undefined &&
      this.intercambio.direccion.localidad?.codigo !== undefined
    ) {
      pais = this.intercambioService.obtenerPaisSeleccionadoEnElIntercambio(
        this.intercambio
      );
      item =
        this.intercambioService.obtenerLocalidadSeleccionadoEnElIntercambio(
          this.intercambio
        );
    }

    this.confBuscador = {
      seleccionado: item,
      inputPreview: {
        mostrar: true,
        input: {
          placeholder: '',
          valor: item.nombre.length > 0 ? item.nombre : '',
          auxiliar: item.auxiliar,
          desactivar: false,
          textoBold: true,
          textoEnMayusculas: true,
        },
        quitarMarginAbajo: true,
      },
      mostrarModal: false,
      inputBuscador: {
        valor: '',
        placeholder: 'Busca tu localidad',
      },
      resultado: {
        mostrarElegibles: false,
        mostrarCargando: false,
        error: {
          mostrarError: false,
          contenido: '',
          tamanoCompleto: false,
        },
        items: [],
      },

      pais: pais,
      evento: (data: InfoAccionBuscadorLocalidades) =>
        this.eventoEnBuscador(data),
    };

    this.confBuscador.inputPreview.input.placeholder =
      await this.translateService.get('m6v6texto5').toPromise();
  }

  eventoEnBuscador(data: InfoAccionBuscadorLocalidades) {
    switch (data.accion) {
      case AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR:
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
        this.abrirBuscadorLocalidades();
        break;
      case AccionesBuscadorLocalidadModal.REALIZAR_BUSQUEDA:
        this.buscarLocalidades(data.informacion.pais, data.informacion.query);
        break;
      case AccionesBuscadorLocalidadModal.CARGAR_MAS_RESULTADOS:
        if (!this.listaResultadosLocalidades.proximaPagina) {
          return;
        }

        this.listaResultadosLocalidades.paginaActual += 1;
        this.buscarLocalidades(
          data.informacion.pais,
          data.informacion.query,
          false
        );
        break;
      case AccionesBuscadorLocalidadModal.SELECCIONAR_ITEM:
        if (!data.informacion || !data.informacion.item) {
          break;
        }

        this.confBuscador.seleccionado = data.informacion.item;
        this.confBuscador.inputPreview.input.valor =
          this.confBuscador.seleccionado.nombre;
        this.confBuscador.inputPreview.input.auxiliar =
          this.confBuscador.seleccionado?.auxiliar;

        // this.localidadBuscador =  this.confBuscador.seleccionado.nombre
        this.reiniciarBuscador();
        break;
      case AccionesBuscadorLocalidadModal.CERRAR_BUSCADOR:
        this.reiniciarBuscador();
        break;
      default:
        break;
    }
  }

  abrirBuscadorLocalidades() {
    if (!this.confBuscador.pais || this.confBuscador.pais.codigo.length === 0) {
      this.toast.abrirToast('text54');
      return;
    }
    this.confBuscador.mostrarModal = true;
  }

  async buscarLocalidades(
    pais: string,
    query: string,
    reiniciarLista: boolean = true
  ) {
    try {
      this.mostrarErrorEnBuscador('', false);
      this.confBuscador.resultado.mostrarCargando = reiniciarLista;
      this.confBuscador.resultado.mostrarElegibles = !reiniciarLista;
      this.confBuscador.resultado.puedeCargarMas = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = !reiniciarLista;

      if (reiniciarLista) {
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
      }

      const localidades: PaginacionModel<ItemSelector> =
        await this.ubicacionNegocio
          .buscarLocalidadesPorNombrePaisConPaginacion(
            25,
            this.listaResultadosLocalidades.paginaActual,
            pais,
            query
          )
          .toPromise();

      if (!localidades) {
        throw new Error();
      }

      localidades.lista.forEach((item) => {
        this.listaResultadosLocalidades.lista.push(item);
      });
      this.listaResultadosLocalidades.proximaPagina = localidades.proximaPagina;

      this.confBuscador.resultado.items = this.listaResultadosLocalidades.lista;
      this.confBuscador.resultado.mostrarElegibles = true;
      this.confBuscador.resultado.puedeCargarMas = true;
      this.confBuscador.resultado.mostrarCargando = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = false;
    } catch (error) {
      this.mostrarErrorEnBuscador('text31', true);
    }
  }

  mostrarErrorEnBuscador(
    contenido: string,
    mostrar: boolean,
    tamanoCompleto: boolean = false
  ) {
    this.confBuscador.resultado.error.contenido = contenido;
    this.confBuscador.resultado.error.tamanoCompleto = tamanoCompleto;
    this.confBuscador.resultado.error.mostrarError = mostrar;
    this.confBuscador.resultado.mostrarCargando = false;
  }

  reiniciarBuscador() {
    this.confBuscador.mostrarModal = false;
    this.confBuscador.inputBuscador.valor = '';
    this.confBuscador.resultado.items = [];
    this.confBuscador.resultado.mostrarElegibles = false;
    this.confBuscador.resultado.mostrarCargando = false;
    this.confBuscador.resultado.error.contenido = '';
    this.confBuscador.resultado.error.tamanoCompleto = false;
    this.confBuscador.resultado.error.mostrarError = false;
  }

  configurarListaPaginacionLocalidades() {
    this.listaResultadosLocalidades = {
      lista: [],
      paginaActual: 1,
      totalDatos: 0,
      proximaPagina: true,
    };
  }

  eventoEnSelector(data: InfoAccionSelector) {
    switch (data.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorPaises();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        // Buscador
        this.confBuscador.pais = data.informacion;
        this.confBuscador.seleccionado.codigo = '';
        this.confBuscador.seleccionado.nombre = '';
        this.confBuscador.inputPreview.input.valor = '';
        // Selector
        this.confSelector.seleccionado = data.informacion;
        this.confSelector.mostrarModal = false;
        this.confSelector.inputPreview.input.valor = data.informacion.nombre;
        // this.confMapa.latitudInicial = 0
        this.intercambio.direccion.longitud = data.informacion.longitud;
        this.intercambio.direccion.latitud = data.informacion.latitud;

        if (this.confMapa === undefined) {
          this.configurarMapaCrear(
            data.informacion.latitud,
            data.informacion.longitud
          );
        } else {
          this.mapa.crearMapa(
            data.informacion.latitud,
            data.informacion.longitud
          );
        }

        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        break;
      case AccionesSelector.BUSCAR_PAIS_POR_QUERY:
        break;
      default:
        break;
    }
  }

  async configurarBotones() {
    // Boton submit
    this.confBotonPublish = {
      text: 'm4v5texto29',
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.eventoEnBotonPublish(),
    };
    this.confBotonHabilitar = {
      text: 'm2v13texto14',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoDeshabilitar.mostrarDialogo = true;
      },
    };

    this.confBotonDeshabilitar = {
      text: 'm2v13texto15',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoDeshabilitar.mostrarDialogo = true;
      },
    };

    this.confBotonEliminar = {
      text: 'm2v13texto16',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AZUL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.confDialogoEliminar.mostrarDialogo = true;
      },
    };
  }

  async configurarDialogoHabilitar() {
    this.confDialogoHabilitar = {
      mostrarDialogo: false,
      descripcion: 'm2v13texto4',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoHabilitar.mostrarDialogo = false;
            this.cambiarStatusIntercambio();
          },
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoHabilitar.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  async configurarDialogoDeshabilitar() {
    this.confDialogoDeshabilitar = {
      mostrarDialogo: false,
      descripcion: 'm6v8texto29',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoDeshabilitar.mostrarDialogo = false;
            this.cambiarStatusIntercambio();
          },
        },
        {
          text: 'm3v9texto3',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoDeshabilitar.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  configurarDialogoEliminar() {
    this.confDialogoEliminar = {
      mostrarDialogo: false,
      descripcion: 'm6v8texto31',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminar.mostrarDialogo = false;
            this.eliminarIntercambio();
          },
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminar.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  async eliminarIntercambio() {
    if (this.params.accionEntidad !== AccionEntidad.ACTUALIZAR) {
      return;
    }

    try {
      const estatus: string = await this.intercambioNegocio
        .eliminarIntercambio(this.intercambio.id, this.intercambio.perfil._id)
        .toPromise();

      this.confDone.mostrarDone = true;
      setTimeout(() => {
        this.navegarAlBack();
      }, 1500);
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async cambiarStatusIntercambio() {
    if (this.params.accionEntidad !== AccionEntidad.ACTUALIZAR) {
      return;
    }
    try {
      const estatus: string = await this.intercambioNegocio
        .cambiarStatusIntercambio(this.intercambio)
        .toPromise();

      this.confDone.mostrarDone = true;
      setTimeout(() => {
        this.navegarAlBack();
      }, 1500);
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  validarInformacionDelIntercambioAntesDeRecargarOCambiarDePaginaHaciaDelante(
    eliminarData: boolean = false,
    eliminarCodigoAIntercambiar: boolean = false
  ) {
    if (eliminarCodigoAIntercambiar || !this.codigoAIntercambiar) {
      this.codigoAIntercambiar =
        CodigosCatalogoTipoIntercambio.INTERCAMBIO_CUALQUIERA;
    }

    this.intercambio =
      this.intercambioNegocio.asignarValoresDeLosCamposAlIntercambio(
        this.params,
        this.intercambio,
        this.confSelector.seleccionado,
        this.confBuscador.seleccionado,
        this.intercambioForm,
        eliminarData,
        this.codigoAIntercambiar,
        this.informacionEnrutador.posicion
      );
  }

  eventoTapInercambio(idIntercambio: string) {
    try {
      if (
        this.estadoPerfilParaMisIntercambios ===
        ModoDelPerfil.AMPLIAR_INTERCAMBIOS
      ) {
        this.abrirIntercambio(idIntercambio);
        return;
      }

      if (this.listaIntercambio.lista.length <= 2) {
        this.abrirIntercambio(idIntercambio);
        return;
      }

      this.estadoPerfilParaMisIntercambios = ModoDelPerfil.AMPLIAR_INTERCAMBIOS;
      // this.confListaProyectos.lista = []
      // this.obtenerProyectosConPaginacion()
    } catch (error) {}
  }
  abrirIntercambio(idIntercambio: string) {
    this.enrutadorService.navegar(
      {
        componente: PublicarIntercambioComponent,
        params: [
          {
            nombre: 'accionEntidad',
            valor: AccionEntidad.ACTUALIZAR,
          },
          {
            nombre: 'id',
            valor: idIntercambio,
          },
        ],
      },
      {
        estado: true,
        posicicion: this.informacionEnrutador.posicion,
        extras: {
          tipo: TipoDeNavegacion.VALIDAR_TIPO_DE_COMPONENTEN_CON_PARAMETROS,
          paramAValidar: 'id',
        },
      }
    );
  }

  async eventoEnBotonPublish(validarCambios: boolean = true) {
    if (validarCambios && !this.validarSiExistenCambios()) {
      if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this._location.back();
        }, 1500);

        return;
      }
      return;
    }
    this.validarInformacionDelIntercambioAntesDeRecargarOCambiarDePaginaHaciaDelante();
    const estado: boolean = this.intercambioService.validarCamposEnIntercambio(
      this.intercambio
    );

    if (!estado) {
      this.toast.abrirToast('text4');
      if (this.params.accionEntidad === AccionEntidad.CREAR) {
        this.intercambioNegocio.crearObjetoVacioDeintercambio(
          this.params.codigoTipoIntercambio,
          this.perfilSeleccionado,
          this.informacionEnrutador.posicion
        );
      }
      return;
    }

    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.publicarIntercambio();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.actualizarIntercambio();
        break;
      case AccionEntidad.VISITAR:
        break;
      default:
        break;
    }
  }

  async publicarIntercambio() {
    try {
      const intercambio = await this.intercambioNegocio
        .crearIntercambio(this.intercambio)
        .toPromise();

      this.intercambioNegocio.removerIntercambioActivoDelSessionStorage();
      this.toast.cerrarToast();
      this.validarAccionDespuesDeCambiosSegunElOrigen();
    } catch (error) {
      this.toast.abrirToast('text36');
      this.intercambioNegocio.crearObjetoVacioDeintercambio(
        this.intercambio.tipo.codigo as CodigosCatalogoTipoIntercambio,
        this.perfilSeleccionado,
        this.informacionEnrutador.posicion
      );
    }
  }
  async actualizarIntercambio() {
    try {
      const proyecto = await this.intercambioNegocio
        .actualizarIntercambio({ ...this.intercambio })
        .toPromise();

      this.toast.cerrarToast();
      this.validarAccionDespuesDeCambiosSegunElOrigen();
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  // Click en input pais
  async abrirSelectorPaises() {
    try {
      this.confSelector.cargando.mostrar = true;
      this.confSelector.mostrarModal = true;
      const items: ItemSelector[] = await this.ubicacionNegocio
        .obtenerCatalogoPaisesParaSelector()
        .toPromise();

      if (!items) {
        throw new Error('');
      }

      this.ubicacionNegocio.guardarPaisesDelSelectorEnLocalStorage(items);
      this.confSelector.elegibles = items;
      this.confSelector.cargando.mostrar = false;

      if (this.confSelector.elegibles.length === 0) {
        this.confSelector.cargando.mostrar = false;
        this.confSelector.error.contenido = 'text31';
        this.confSelector.error.mostrarError = true;
      }
    } catch (error) {
      this.confSelector.elegibles = [];
      this.confSelector.cargando.mostrar = false;
      this.confSelector.error.contenido = 'text31';
      this.confSelector.error.mostrarError = true;
    }
  }

  determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum) {
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      return true;
    }

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      codigo,
      this.intercambio.adjuntos
    );

    if (album) {
      return true;
    }

    return false;
  }

  async scroolEnCapaFormulario() {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    const elemento: HTMLElement = document.getElementById(
      this.idCapaFormulario
    ) as HTMLElement;
    if (
      !elemento ||
      !this.puedeCargarMas ||
      this.indicadorTotalComentarios.total ===
        this.listaComentariosFirebase.length
    ) {
      // this.puedeCargarMas = true
      return;
    }

    if (
      elemento.offsetHeight + elemento.scrollTop >=
      elemento.scrollHeight - 15
    ) {
      this.puedeCargarMas = false;
      this.comenFireService.paginacionComentarios += 30;
      this.obtenerListaDeComentariosEnPaginacion();
      this.obtenerComentariosPaginacion();
    }
  }

  validarAccionDespuesDeCambiosSegunElOrigen(mostrarDone: boolean = true) {
    if (mostrarDone) {
      this.confDone.mostrarDone = mostrarDone;
      setTimeout(() => {
        this.ejecutarAccionDespuesDelCambio();
      }, 1500);
      return;
    }

    this.ejecutarAccionDespuesDelCambio();
  }

  ejecutarAccionDespuesDelCambio() {
    switch (this.origenValidacionCambios) {
      case OrigenValidacionDeCambios.BOTON_BACK:
        this.navegarAlBack();
        break;
      case OrigenValidacionDeCambios.BOTON_HOME:
        this.navegarAlHome(true);
        break;
      case OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL:
        this.irAlAlbumGeneral(false);
        break;
      case OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS:
        this.irAlAlbumAudios(false);
        break;

      default:
        this.navegarAlBack();
        break;
    }
  }

  navegarAlHome(posicionCambiar: boolean) {
    if (posicionCambiar) {
      this.enrutadorService.navegarConPosicionFija(
        {
          componente: MenuPrincipalComponent,
          params: [],
        },
        this.informacionEnrutador.posicion
      );
    } else {
      this.enrutadorService.navegar(
        {
          componente: MenuPrincipalComponent,
          params: [],
        },
        {
          estado: true,
          posicicion: this.informacionEnrutador.posicion,
          extras: {
            tipo: TipoDeNavegacion.NORMAL,
          },
        }
      );
    }
  }

  navegarAlBack() {
    this.intercambioNegocio.removerIntercambioActivoDelSessionStorage();
    this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion);
  }

  obtenerInputSegunKey(key: InputKey): InputCompartido {
    const index = this.inputsForm.findIndex((e) => e.key === key);

    if (index < 0) {
      return undefined;
    }

    return this.inputsForm[index].input;
  }

  mostrarBotonesEstadoIntercambio() {
    let estato = false;
    if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
      return true;
    }
    return false;
  }

  async eliminarComentario() {
    if (!this.comentarioAEliminar) {
      return;
    }

    try {
      this.confDialogoEliminarComentario.mostrarDialogo = false;
      let estado: string;
      let seFueAHistorico: boolean = false;

      if (this.intercambio.perfil._id === this.perfilSeleccionado._id) {
        estado = await this.comentarioIntercambioNegocio
          .eliminarComentario(
            this.intercambio.id,
            this.comentarioAEliminar.coautor.id,
            this.comentarioAEliminar.id
          )
          .toPromise();
        seFueAHistorico = true;
      }

      if (
        !seFueAHistorico &&
        this.perfilSeleccionado._id ===
          this.comentarioAEliminar.coautor.coautor._id
      ) {
        estado = await this.comentarioIntercambioNegocio
          .eliminarMiComentario(
            this.comentarioAEliminar.id,
            this.comentarioAEliminar.coautor.id
          )
          .toPromise();
      }

      if (!estado) {
        throw new Error('text37');
      }

      const querys = {};
      const path =
        this.params.id + '/' + this.comentarioAEliminar.id + '/estado';
      querys[path] = {
        codigo: seFueAHistorico
          ? CodigosCatalogosEstadoComentarioIntercambio.HISTORICO
          : CodigosCatalogosEstadoComentarioIntercambio.ELIMINADO,
      };

      this.db.database
        .ref('comentariosIntercambio')
        .update(querys)
        .then(
          () => {
            const index = this.listaComentariosFirebase.findIndex(
              (e) => e.id === this.comentarioAEliminar.id
            );
            this.eliminarComentariosDeListas(
              this.comentarioAEliminar.id,
              index
            );

            this.comentarioAEliminar = undefined;
          },
          (error) => {
            throw new Error('');
          }
        );
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  /// ENVIO COMENTARIO

  async enviarComentario(
    dataApiArchivo: SubirArchivoData,
    texto: boolean = true
  ) {
    try {
      const comentario: ComentarioIntercambioModel = {
        importante: false,
        intercambio: {
          id: this.intercambio.id,
        },
        coautor: {
          coautor: {
            _id: this.perfilSeleccionado._id,
          },
        },
      };

      if (
        this.idPerfilCoautorParaResponder &&
        this.idPerfilCoautorParaResponder.length > 0
      ) {
        comentario.idPerfilRespuesta = {
          _id: this.idPerfilCoautorParaResponder,
        };
      }

      if (!texto) {
        const media: MediaModel = await this.mediaNegocio
          .subirMedia(dataApiArchivo)
          .toPromise();
        comentario.adjuntos = [];
        comentario.adjuntos.push({
          id: media.id,
        });
      }

      if (texto) {
        comentario.traducciones = [
          {
            texto: dataApiArchivo.descripcion,
          },
        ];
      }

      const comentarioCreado: ComentarioModel =
        await this.comentarioIntercambioNegocio
          .crearComentario(comentario)
          .toPromise();

      if (!texto) {
        this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false;
        this.confBarraInferior.iconoAudio.capa.mostrar = false;
      }

      this.idPerfilCoautorParaResponder = '';
    } catch (error) {
      if (!texto) {
        this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false;
        this.confBarraInferior.iconoAudio.capa.mostrar = false;
      }
      this.toast.abrirToast('text37');
    }
  }

  determinarTipoDelComentario(): CodigosCatatalogosTipoComentario {
    if (this.intercambio.estado) {
      return CodigosCatatalogosTipoComentario.TIPO_NORMAL;
    }
  }
  configurarInformacionDelEnrutador(item: InformacionEnrutador) {
    this.informacionEnrutador = item;
  }
}

export interface Inputs {
  key: InputKey;
  input: InputCompartido;
}

export enum InputKey {
  TITULO_CORTO = 'titulo-corto',
  TITULO = 'titulo',
  EMAIL = 'email',
  LOCALIDAD = 'descripcion-direccion',
  UBICACION = 'ubicacion',
  AUTOR = 'autor',
}

export interface IndicadorTotalComentarios {
  total: number;
  comentarios: Array<string>;
}

export enum OrigenValidacionDeCambios {
  BOTON_BACK = 'back',
  BOTON_HOME = 'home',
  BOTON_ALBUM_GENERAL = 'album-general',
  BOTON_ALBUM_AUDIOS = 'album-audios',
  BOTON_ALBUM_LINKS = 'album-links',
}
export enum ModoDelPerfil {
  DEFAULT = 'default',
  AMPLIAR_INTERCAMBIOS = 'amp_itc',
}
