import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EnrutadorService } from '@core/servicios/generales/enrutador/enrutador.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ToastComponent } from '@shared/componentes';
import { ConfiguracionToast } from '@shared/diseno/modelos';
import { BuscadorIntercambioComponent } from 'presentacion/compra-intercambio/intercambio/buscador-intercambio/buscador-intercambio.component';
import { PublicarIntercambioComponent } from 'presentacion/compra-intercambio/intercambio/publicar-intercambio/publicar-intercambio.component';
import { MenuPrincipalComponent } from 'presentacion/menu-principal/menu-principal.component';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import {
	ColorDelTexto, ColorFondoLinea,
	EspesorLineaItem, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { InformacionEnrutador, TipoDeNavegacion } from 'presentacion/enrutador/enrutador.component';

@Component({
	selector: 'app-intercambio',
	templateUrl: './intercambio.component.html',
	styleUrls: ['./intercambio.component.scss']
})
export class IntercambioComponent implements OnInit {
	
    @ViewChild('toast', { static: false }) toast: ToastComponent

	configuracionAppBar: ConfiguracionAppbarCompartida;
	perfilSeleccionado: PerfilModel;
	confLinea: LineaCompartida;

	//botones
	botonCompra: BotonCompartido
	botonIntercambio: BotonCompartido

	//configuraciones
	public confToast: ConfiguracionToast
	public confLineaintercambio: LineaCompartida
	public botonBuscar: BotonCompartido
	public botonPublicar: BotonCompartido

	public accionCuenta: number



	// Enrutamiento
	public informacionEnrutador: InformacionEnrutador



	constructor(
		private perfilNegocio: PerfilNegocio,
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private _location: Location,
		private variablesGlobales: VariablesGlobales,
		private router: Router,
		private enrutadorService: EnrutadorService
	) { }

	ngOnInit(): void {

		this.variablesGlobales.mostrarMundo = false
		this.accionCuenta = 0
		this.obtenerPerfil()
		this.configurarLinea()		
		if (this.perfilSeleccionado) {
			this.prepararAppBar()
			this.estiloTituloPrincipal()
			this.configurarToast()
			this.configuracionBotones()
			

		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}


	}
	seleccionarAccion(accion: number) {
		if (this.accionCuenta === accion) {
			this.accionCuenta = this.accionCuenta
			return
		}
		
		switch (accion) {
			case AccionesCuenta.CULTURA:
				this.accionCuenta = 1
				break;
			case AccionesCuenta.ALOJAMIENTO:
				this.accionCuenta = 2
				break;
			case AccionesCuenta.OBJETOS_PRODUCTOS:
				this.accionCuenta = 3
				break;
			case AccionesCuenta.HABILIDADES:
				this.accionCuenta = 4
				break;


			default:
				break;
		}
	}

	obtenerPerfil() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
	}

	prepararAppBar() {
		this.configuracionAppBar = {
			botonRefresh: true,
            eventoRefresh: () => {
                this.enrutadorService.refrescarTercio(this.informacionEnrutador.posicion)
            },
			accionAtras: () => {
				this.enrutadorService.navegarAlBack(this.informacionEnrutador.posicion)
			},
			eventoHome: () =>
				this.enrutadorService.navegar(
					{
						componente: MenuPrincipalComponent,
						params: [
						]
					},
					{
						estado: true,
						posicicion: this.informacionEnrutador.posicion,
						extras: {
							tipo: TipoDeNavegacion.NORMAL,
						}
					}
				),
			

			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarLineaVerde: true,
				mostrarTextoHome: true,
				mostrarBotonXRoja: false,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true,

					}
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm6v5texto1'
				},

			}

		}

	}


	configurarToast() {
        this.confToast = {
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: false,
        }
    }
	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR012,
			forzarAlFinal: false
		}
	}

	estiloTituloPrincipal() {
		return this.estiloDelTextoServicio.obtenerEstilosTexto({
			color: ColorDelTexto.TEXTOVERDEBASE,
			estiloTexto: EstilosDelTexto.REGULAR,
			enMayusculas: true,
			tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_IGUAL // TODO: verdfificar tamaño
		})

	}


	configuracionBotones() {

		this.botonBuscar = {
			text: 'm6v5texto7',
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.CELESTE,
			ejecutar: () => {
				if (this.accionCuenta !== 0) {
					this.irABuscarIntercambioSegunTipo(TIPO_RUTAS_INTERCAMBIO[this.accionCuenta])
					return
				} else {
					this.toast.abrirToast('text77')
				}
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		}
		this.botonPublicar = {
			text: 'm6v5texto8',
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.NARANJA_INTERCAMBIO,
			ejecutar: () => {
				if (this.accionCuenta !== 0) {
					this.irAListaIntercambioSegunTipo(TIPO_RUTAS_INTERCAMBIO[this.accionCuenta])
					return
				} else {
					this.toast.abrirToast('text77')
				}
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		}

	}


	irABuscarIntercambioSegunTipo(codigo: CodigosCatalogoTipoIntercambio) {
		// let ruta = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
		// let informacion = RutasCompra.BUSQUEDA_INTERCAMBIO_TIPO.toString()
		// informacion = informacion.replace(':codigoTipoIntercambio', codigo.toString())
		// this.router.navigateByUrl(ruta + '/' + informacion)
		this.enrutadorService.navegar(
			{
				componente: BuscadorIntercambioComponent,
				params: [
					{
						nombre: 'accionEntidad',
						valor: AccionEntidad.ACTUALIZAR
					},
					{
						nombre: 'codigoTipoIntercambio',
						valor: codigo
					}
				]
			},
			{
				estado: true,
				posicicion: this.informacionEnrutador.posicion
			}
		)
	}

	irAListaIntercambioSegunTipo(codigo: CodigosCatalogoTipoIntercambio) {
		// let ruta = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
		// let informacion = RutasCompra.PUBLICAR.toString()
		// informacion = informacion.replace(':codigoTipoIntercambio', codigo.toString())
		// this.router.navigateByUrl(ruta + '/' + informacion)
		this.enrutadorService.navegar(
			{
				componente: PublicarIntercambioComponent,
				params: [
					{
						nombre: 'accionEntidad',
						valor: AccionEntidad.CREAR
					},
					{
						nombre: 'codigoTipoIntercambio',
						valor: codigo
					}
				]
			},
			{
				estado: true,
				posicicion: this.informacionEnrutador.posicion
			}
		)
	}

	// Enrutamiento
	configurarInformacionDelEnrutador(item: InformacionEnrutador) {
		this.informacionEnrutador = item
	}


}

export enum AccionesCuenta {
	CULTURA = 1,
	ALOJAMIENTO = 2,
	OBJETOS_PRODUCTOS = 3,
	HABILIDADES = 4
}

const TIPO_RUTAS_INTERCAMBIO = {
	1: CodigosCatalogoTipoIntercambio.CULTURAL,
	2: CodigosCatalogoTipoIntercambio.ALOJAMIENTO,
	3: CodigosCatalogoTipoIntercambio.OBJETOS_PRODUCTOS,
	4: CodigosCatalogoTipoIntercambio.HABILIDADES,
}