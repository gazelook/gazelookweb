import { RutasPerfiles } from './rutas-perfiles.enum';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilComponent } from './perfil/perfil.component';

const routes: Routes = [
	{
		path: RutasPerfiles.PERFIL.toString(),
		component: PerfilComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PerfilesRoutingModule { }
