import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from 'src/app/compartido/compartido.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilesRoutingModule } from './perfiles-routing.module';
import { PerfilesComponent } from './perfiles.component';
import { PerfilComponent } from './perfil/perfil.component';


@NgModule({
	declarations: [ PerfilesComponent, PerfilComponent ],
	imports: [
		CommonModule,
		PerfilesRoutingModule,
		CompartidoModule,
		TranslateModule, 
		FormsModule
	],
	exports: [
		TranslateModule
	]
})
export class PerfilesModule { }
