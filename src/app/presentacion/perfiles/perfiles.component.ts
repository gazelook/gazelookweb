import { Component, OnInit } from '@angular/core';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones';
@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.scss']
})
export class PerfilesComponent implements OnInit {

  constructor(
    private notificacionesUsuario: NotificacionesDeUsuario
  ) { }

  ngOnInit(): void {
    this.notificacionesUsuario.validarEstadoDeLaSesion(false)
  }

}
