import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, fromEvent, merge, of  } from 'rxjs';
import { map } from 'rxjs/operators';

import { VariablesGlobales } from '@core/servicios/generales';
import {NotificacionesDeUsuario} from '@core/servicios/generales/notificaciones';
import {
  InternacionalizacionNegocio, MediaNegocio
} from 'dominio/logica-negocio';
import { ArchivoModel } from 'dominio/modelo/entidades';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  public title = 'Gazelook';
  public urlGif: string;

  public networkStatus: boolean;
  private networkStatus$: Subscription = Subscription.EMPTY;

  constructor(
    public variablesGlobales: VariablesGlobales,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private notificacionesDeUsuario: NotificacionesDeUsuario,
    private mediaNegocio: MediaNegocio,
  ) {
    this.urlGif = 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/gif-en.gif';
  }

  ngOnInit(): void {
    this.internacionalizacionNegocio.guardarIdiomaDefecto();
    this.configurarEscuchasDeEstadoDelInternet();
    this.configurarImagenesPorDefecto();
    this.checkNetworkStatus();

    window.onbeforeunload = () => {
      this.notificacionesDeUsuario.desconectarDeEscuchaNotificaciones();
    };
  }

  ngOnDestroy(): void {
    this.notificacionesDeUsuario.desconectarDeEscuchaNotificaciones();
  }

  desactivarContextMenu(): void {
    window.oncontextmenu = (event: any) => {
      event.preventDefault();
      event.stopPropagation();
      return false;
    };
  }

  configurarEscuchasDeEstadoDelInternet(): void {
    window.addEventListener('offline', (e) => {
    });

    window.addEventListener('online', (e) => {
      });
  }

  checkNetworkStatus(): void {
    this.networkStatus = navigator.onLine;
    this.networkStatus$ = merge(
      of(null),
      fromEvent(window, 'online'),
      fromEvent(window, 'offline')
    )
      .pipe(map(() => navigator.onLine))
      .subscribe(status => this.networkStatus = status);
  }

  async configurarImagenesPorDefecto(): Promise<void> {
    try {

      let archivos: ArchivoModel[] = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal();

      if (!archivos || archivos === null || (archivos && archivos.length === 0)) {
        archivos = await this.mediaNegocio.obtenerListaArchivosDefaultDelApi().toPromise();
      }

      this.mediaNegocio.guardarListaArchivosDefaultEnLocal(archivos);
    } catch (error) {
      this.mediaNegocio.guardarListaArchivosDefaultEnLocal([]);
    }
  }
}
export interface DataCerrarSesion {
  idUsuario: string;
  idDispositivo: string;
}
