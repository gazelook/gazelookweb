import { AnunciosSistemaComponent } from './presentacion/anuncios-sistema/anuncios-sistema.component';
import { NucleoModule } from './nucleo/nucleo.module';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  HammerModule,
} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  CommonModule,
  CurrencyPipe,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import { WebcamModule } from 'ngx-webcam';
import { ImageCropperModule } from 'ngx-image-cropper';

import { NgxStripeModule } from 'ngx-stripe';

import { CompartidoModule } from './compartido/compartido.module';
import { DetectorGestos } from './nucleo/servicios/generales/detector-gestos.service';

//=====Modulos traducciones
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//SERVICIOS
//import { ApiService } from './nucleo/servicios/locales/api'
import { HandleError } from './nucleo/servicios/locales/handleError.service';
import { IdiomaService } from './nucleo/servicios/remotos/idioma.service';

//DIRECTIVAS
//COMPONENTES
import { AppComponent } from './app.component';

//Test
import { LoginComponent } from './presentacion/login/login.component';
import { RegistroComponent } from './presentacion/registro/registro.component';
import { MetodoPagoComponent } from './presentacion/metodo-pago/metodo-pago.component';
import { PeticionInterceptor } from './nucleo/servicios/remotos/peticion.interceptor';
import { MenuPerfilesComponent } from './presentacion/menu-perfiles/menu-perfiles.component';
import { BienvenidaComponent } from './presentacion/bienvenida/bienvenida.component';
import { MenuPrincipalComponent } from './presentacion/menu-principal/menu-principal.component';
import { MenuSeleccionPerfilesComponent } from './presentacion/menu-seleccion-perfiles/menu-seleccion-perfiles.component';
import { RestriccionRutas } from './nucleo/servicios/generales/canActivate/resticcionRutas.service';
import { RutasInicioSession } from './nucleo/servicios/generales/canActivate/rutas-inicio-session.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MenuPublicarProyectoNoticiaComponent } from './presentacion/menu-publicar-proyecto-noticia/menu-publicar-proyecto-noticia.component';
import { CompraIntercambioComponent } from './presentacion/compra-intercambio/compra-intercambio.component';
// import { NgxAgoraModule } from 'ngx-agora';
import { ContactoComponent } from './presentacion/contacto/contacto.component';
import { MiCuentaComponent } from './presentacion/mi-cuenta/mi-cuenta.component';
import { NuestrasMetasComponent } from './presentacion/nuestras-metas/nuestras-metas.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { LandingComponent } from './presentacion/landing/landing.component';
import { FincaComponent } from './presentacion/finca/finca.component';
import { AnuncioComponent } from './presentacion/anuncio/anuncio.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { EnrutadorComponent } from './presentacion/enrutador/enrutador.component';
import { ProyectosModule } from './presentacion/proyectos/proyectos.module';
import { PerfilesModule } from './presentacion/perfiles/perfiles.module';
import { NoticiasModule } from './presentacion/noticias/noticias.module';
import { CompraIntercambioModule } from './presentacion/compra-intercambio/compra-intercambio.module';
import { FinanzasModule } from './presentacion/finanzas/finanzas.module';
import { BuscadorGeneralModule } from './presentacion/buscador-general/buscador-general.module';
import { GazingModule } from './presentacion/gazing/gazing.module';
import { PensamientoModule } from './presentacion/pensamiento/pensamiento.module';
import { AlbumModule } from './presentacion/album/album.module';
import { EnrutadorPerfilComponent } from './presentacion/enrutador-perfil/enrutador-perfil.component';
import { EnrutadorRegistroComponent } from './presentacion/enrutador-registro/enrutador-registro.component';
import { EnrutadorDemoComponent } from './presentacion/enrutador-demo/enrutador-demo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DocLegalesComponent } from './presentacion/doc-legales/doc-legales.component';
import { CuotaExtraComponent } from './presentacion/cuota-extra/cuota-extra.component';

import { CountdownModule } from '@env/node_modules/ngx-countdown';
import { MetodosPagoComponent } from './presentacion/metodos-pago/metodos-pago.component';
import { MetodoStripeComponent } from './presentacion/cuota-extra/metodo-stripe/metodo-stripe.component';
import { MetodoPaymentezComponent } from './presentacion/cuota-extra/metodo-paymentez/metodo-paymentez.component';
//Traducciones
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  // return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    MetodoPagoComponent,
    MenuPerfilesComponent,
    BienvenidaComponent,
    MenuPrincipalComponent,
    MenuSeleccionPerfilesComponent,
    ContactoComponent,
    MenuPublicarProyectoNoticiaComponent,
    MiCuentaComponent,
    NuestrasMetasComponent,
    LandingComponent,
    FincaComponent,
    AnuncioComponent,
    AnunciosSistemaComponent,
    EnrutadorComponent,
    EnrutadorPerfilComponent,
    EnrutadorRegistroComponent,
    EnrutadorDemoComponent,
    DocLegalesComponent,
    CuotaExtraComponent,
    MetodosPagoComponent,
    MetodoStripeComponent,
    MetodoPaymentezComponent,
  ],
  imports: [
    NgxCurrencyModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    CompartidoModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
      //extend:true
    }),
    HammerModule,
    WebcamModule,
    ImageCropperModule,
    NgxStripeModule.forRoot(environment.pkStripe),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    // Modulos a incluir
    ProyectosModule,
    PerfilesModule,
    NoticiasModule,
    CompraIntercambioModule,
    FinanzasModule,
    BuscadorGeneralModule,
    GazingModule,
    PensamientoModule,
    AlbumModule,
    NucleoModule,
    BrowserAnimationsModule,
    CountdownModule,
  ],
  exports: [
    CompartidoModule,
    TranslateModule,
    NgxCurrencyModule,
    CountdownModule,
  ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: DetectorGestos },
    { provide: HTTP_INTERCEPTORS, useClass: PeticionInterceptor, multi: true },
    // { provide: LocationStrategy, useClass: PathLocationStrategy },
    HandleError,
    IdiomaService,
    RestriccionRutas,
    RutasInicioSession,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
