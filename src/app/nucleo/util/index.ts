import { FuncionesCompartidas } from './funciones-compartidas';
import { MetodosSessionStorageService } from './metodos-session-storage.service';
import { ConvertidorArchivos } from './caster-archivo.service';
import { MetodosLocalStorageService } from './metodos-local-storage.service';


export const nucelUtil: any[] = [
    MetodosLocalStorageService, 
    ConvertidorArchivos,
    MetodosSessionStorageService,
    FuncionesCompartidas


];

export * from './metodos-local-storage.service';
export * from './caster-archivo.service';
export * from './metodos-session-storage.service';
export * from './funciones-compartidas';
export * from './respuesta'
