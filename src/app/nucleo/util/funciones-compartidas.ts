import { ColorFondoAleatorio } from '@shared/diseno/enums/estilos-colores-general';


// fondo aletorio
export class FuncionesCompartidas {

  static obtenerColorFondoAleatorio() {
    const fondosAleatorios = Object.keys(ColorFondoAleatorio).map(function (
      type
    ) {
      return ColorFondoAleatorio[type];
    });
    const fondoAleatorio = this.randomItem(fondosAleatorios);
    return fondoAleatorio;
  }


  static randomItem(items) {
    return items[Math.floor(Math.random() * items.length)];
  }

}