import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RespuestaRemota } from '@core/util/respuesta';
import { IntercambioEntity } from 'dominio/entidades/intercambio.entity';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { Intercambio } from '@core/servicios/remotos/rutas/intercambio.enum';

@Injectable({ providedIn: 'root' })
export class IntercambioServiceRemoto {

    constructor(
        private http: HttpClient
    ) {

    }

    crearintercambio(intercambio: IntercambioEntity): Observable<RespuestaRemota<IntercambioEntity>> {
        return this.http.post<RespuestaRemota<IntercambioEntity>>(APIGAZE.BASE + Intercambio.CREAR_INTERCAMBIO, intercambio)
    }
    obtenerInformacionDelIntercambio(idIntercambio: string, idPerfil: string): Observable<RespuestaRemota<IntercambioEntity>> {
        return this.http.get<RespuestaRemota<IntercambioEntity>>(
            APIGAZE.BASE
            + Intercambio.INTERCAMBIO_UNICO
            + '/?'
            + 'idIntercambio=' + `${idIntercambio}`
            + '&idPerfil=' + `${idPerfil}`
            + '&original=' + 'true'
        )
    }

    actualizarIntercambio(intercambio: IntercambioEntity): Observable<RespuestaRemota<IntercambioEntity>> {

        return this.http.put<RespuestaRemota<IntercambioEntity>>(APIGAZE.BASE + Intercambio.ACTUALIZAR_INTERCAMBIO, intercambio)
    }


    eliminarIntercambio(idIntercambio: string, idPerfil: string): Observable<RespuestaRemota<string>> {        
        return this.http.delete<RespuestaRemota<string>>(
            APIGAZE.BASE
            + Intercambio.ELIMINAR_INTERCAMBIO
            + '?perfil=' + `${idPerfil}`
            + '&intercambio=' + `${idIntercambio}`
        )
    }
    cambiarStatusIntercambio(intercambio: IntercambioEntity): Observable<RespuestaRemota<IntercambioEntity>> {
  
        return this.http.put<RespuestaRemota<IntercambioEntity>>(APIGAZE.BASE + Intercambio.CAMBIAR_STATUS, intercambio)
    }

    tiposIntercambio(): Observable<RespuestaRemota<Array<IntercambioEntity>>>{
        return this.http.get<RespuestaRemota<Array<IntercambioEntity>>>(APIGAZE.BASE + Intercambio.TIPO_INTERCAMBIO)
    }

    buscarintercambiosPorFiltro(
        limite: number,
        pagina: number,
        tipo: CodigosCatalogoTipoIntercambio,
        perfil: string,
        fechaInicial: string,
        fechaFinal: string,
        pais: string,
        titulo: string,
        localidad: string,
        codigoAIntercambiarA: string,
        codigoAIntercambiarB: string,
    ): Observable<HttpResponse<RespuestaRemota<Array<IntercambioEntity>>>> {
        return this.http.get<RespuestaRemota<IntercambioEntity[]>>(
            APIGAZE.BASE +
            Intercambio.INTERCAMBIO_RECIENTES
            + '/?'
            + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&perfil=' + `${perfil}`
            + '&tipo=' + `${tipo.toString()}`
            + '&pais=' + `${pais.toString()}`
            + '&localidad=' + `${localidad.toString()}`
            // + '&codigoAIntercambiarA=' + `${codigoAIntercambiarA.toString()}`
            + '&tipoIntercambiar=' + `${codigoAIntercambiarB.toString()}`
            + '&titulo=' + `${titulo.toString()}`
            + '&fechaInicial=' + `${fechaInicial}`
            + '&fechaFinal=' + `${fechaFinal}`,
            { observe: 'response' }
        )
    }


    buscarMisIntercambiosTipo(
        tipoIntercambio: CodigosCatalogoTipoIntercambio,
        perfil: string,
        limite: number,
        pagina: number
    ): Observable<HttpResponse<RespuestaRemota<Array<IntercambioEntity>>>> {
        return this.http.get<RespuestaRemota<IntercambioEntity[]>>(
            APIGAZE.BASE +
            Intercambio.MIS_INTERCAMBIOS_TIPO
            + '/?'
            + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&perfil=' + `${perfil}`
            + '&tipo=' + `${tipoIntercambio}`,
            { observe: 'response' }
        )
    }

}