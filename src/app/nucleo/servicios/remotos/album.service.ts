import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { RespuestaRemota } from '@core/util/respuesta';
import { AlbumEntity } from 'dominio/entidades/album.entity';
import { Observable } from 'rxjs';
import { Album } from '@core/servicios/remotos/rutas/album.enum';

@Injectable({ providedIn: 'root' })
export class AlbumServiceRemoto {

    constructor(
        private http: HttpClient
    ) {

    }

    actualizarAlbum(album: AlbumEntity) {
        return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Album.ACTUALIZAR_ALBUM, album)
    }

    agragarMediaAlAlbum(
        idEntidad: string,
        codigoEntidad: string,
        album: AlbumEntity
    ): Observable<RespuestaRemota<string>> {
        return this.http.post<RespuestaRemota<string>>(
            APIGAZE.BASE + Album.AGREGAR_MEDIA_AL_ALBUM + '/' + idEntidad + '/' + codigoEntidad,
            album
        )
    }

    eliminarMediaDelAlbum(
        idEntidad: string,
        codigoEntidad: string,
        album: AlbumEntity
    ): Observable<RespuestaRemota<string>> {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
            body: album,
        }

        return this.http.delete<RespuestaRemota<string>>(
            APIGAZE.BASE + Album.ELIMINAR_MEDIA_DEL_ALBUM + '/' + idEntidad + '/' + codigoEntidad,
            httpOptions
        )
    }

    actualizarMediaDelAlbum(
        idEntidad: string,
        codigoEntidad: string,
        album: AlbumEntity
    ): Observable<RespuestaRemota<string>> {
        return this.http.put<RespuestaRemota<string>>(
            APIGAZE.BASE + Album.ACTUALIZAR_MEDIA_DEL_ALBUM + '/' + idEntidad + '/' + codigoEntidad,
            album
        )
    }

    agregarAlbumEnEntidad(
        idEntidad: string,
        codigoEntidad: string,
        album: AlbumEntity
    ): Observable<RespuestaRemota<AlbumEntity>> {
        return this.http.post<RespuestaRemota<AlbumEntity>>(
            APIGAZE.BASE + Album.AGREGAR_ALBUM_A_ENTIDAD + '/' + idEntidad + '/' + codigoEntidad,
            album
        )
    }

    actualizarParametrosDelAlbum(
        idEntidad: string,
        codigoEntidad: string,
        album: AlbumEntity
    ): Observable<RespuestaRemota<AlbumEntity>> {
        return this.http.put<RespuestaRemota<AlbumEntity>>(
            APIGAZE.BASE + Album.ACTUALIZAR_PARAMETROS_DEL_ALBUM_EN_ENTIDAD + '/' + idEntidad + '/' + codigoEntidad,
            album
        )
    }
}