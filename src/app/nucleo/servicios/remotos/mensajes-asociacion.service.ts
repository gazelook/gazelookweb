import { ConversacionEntity } from 'dominio/entidades/conversacion.entity';
import { MensajesAsociacion } from '@core/servicios/remotos/rutas/mensajes-asociacion.enum';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum'
import { RespuestaRemota } from '@core/util/respuesta'
import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { MensajeEntity } from 'dominio/entidades/mensaje.entity';

@Injectable({ providedIn: 'root' })
export class MensajesAsociacionServiceRemoto {

    constructor(
        private http: HttpClient,
    ) {

    }

    obtenerConversacion(
        idPerfil: string,
        idAsociacion: string,
        limite: number,
        pagina: number,
        filtro: string
    ): Observable<HttpResponse<RespuestaRemota<Array<MensajeEntity>>>> {
        return this.http.get<RespuestaRemota<MensajeEntity[]>>(APIGAZE.BASE + MensajesAsociacion.OBTENER_CONVERSACION.toString()
            + '/?'
            + 'idPerfil=' + `${idPerfil}`
            + '&asociacion=' + `${idAsociacion}`
            + '&limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&filtro=' + `${filtro}`
            , { observe: 'response' })
    }

    
    obtenerConversacionFecha(
        idPerfil: string,
        idAsociacion: string,
        limite: number,
        pagina: number,
        fecha: string
    ): Observable<HttpResponse<RespuestaRemota<Array<MensajeEntity>>>> {
        
        return this.http.get<RespuestaRemota<MensajeEntity[]>>(APIGAZE.BASE + MensajesAsociacion.OBTENER_CONVERSACION_FECHA.toString()
            + '/?'
            + '&idPerfil=' + `${idPerfil}`
            + '&asociacion=' + `${idAsociacion}`
            + '&limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&fecha=' + `${fecha}`
            , { observe: 'response' })
    }

    obtenerMsjNoLeidos(
        idPerfil: string,
        limite: number,
        pagina: number
    ): Observable<HttpResponse<RespuestaRemota<Array<ConversacionEntity>>>> {
        return this.http.get<RespuestaRemota<ConversacionEntity[]>>(APIGAZE.BASE + MensajesAsociacion.OBTENER_MSJ_NO_LEIDOS.toString()
            + '/?' 
            + '&perfil=' + `${idPerfil}`
            + '&limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            , { observe: 'response' })
    }



 




}   