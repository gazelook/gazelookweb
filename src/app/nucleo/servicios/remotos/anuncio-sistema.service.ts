import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AnuncioSistemaEntity } from 'dominio/entidades/anuncio-sistema.entity';
import { RespuestaRemota } from '@core/util/respuesta';
import { AnuncioSistema } from '@core/servicios/remotos/rutas/anuncio-sistema.enum';
import { APIGAZE } from './rutas/api-gaze.enum';

@Injectable({ providedIn: 'root' })
export class AnuncioSistemaServiceRemoto {
  constructor(private http: HttpClient) {}

  obtenerAnunciosSistema(
    perfil: string
  ): Observable<HttpResponse<RespuestaRemota<Array<AnuncioSistemaEntity>>>> {
    return this.http.get<RespuestaRemota<AnuncioSistemaEntity[]>>(
      APIGAZE.BASE +
        AnuncioSistema.OBTENER +
        '/?' +
        'limite=' +
        `100` +
        '&pagina=' +
        `1` +
        '&perfil=' +
        `${perfil}`,
      { observe: 'response' }
    );
  }

  obtenerAnunciosSistemaPublicitario(
    perfil: string
  ): Observable<HttpResponse<RespuestaRemota<Array<AnuncioSistemaEntity>>>> {
    return this.http.get<RespuestaRemota<AnuncioSistemaEntity[]>>(
      APIGAZE.BASE +
        AnuncioSistema.UNICO +
        '/?' +
        'limite=' +
        `100` +
        '&pagina=' +
        `1` +
        '&perfil=' +
        `${perfil}`,
      { observe: 'response' }
    );
  }
}
