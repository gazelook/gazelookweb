import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Noticia } from '@core/servicios/remotos/rutas/noticia.enum';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';
import { VotoNoticiaEntity } from 'dominio/entidades/voto-noticia.entity';
import { Observable } from 'rxjs';
import { RespuestaRemota } from '@core/util/respuesta';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';

@Injectable({ providedIn: 'root' })
export class NoticiaServiceRemoto {

    constructor(
        private http: HttpClient
    ) {

    }

    obtenerNoticiasPerfil(idPerfil: string, limite: number, pagina: number, traducir: boolean): Observable<HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>> {
        return this.http.get<RespuestaRemota<NoticiaEntity[]>>(
            APIGAZE.BASE + Noticia.NOTICIAS_PERFIL+
            `/?perfil=${idPerfil}&limite=${limite}&pagina=${pagina}&traduce=${traducir}`  
            , { observe: 'response' })
    }

    crearNoticia(noticia: NoticiaEntity): Observable<RespuestaRemota<NoticiaEntity>> {
        return this.http.post<RespuestaRemota<NoticiaEntity>>(
            APIGAZE.BASE + Noticia.CREAR_NOTICIA,
            noticia
        )
    }

    obtenerInformacionDeLaNoticia(idNoticia: string, idPerfil: string): Observable<RespuestaRemota<NoticiaEntity>> {
        return this.http.get<RespuestaRemota<NoticiaEntity>>(
            APIGAZE.BASE
            + Noticia.NOTICIA_UNICA
            + '/?'
            + 'idNoticia=' + `${idNoticia}`
            + '&idPerfil=' + `${idPerfil}`
        )
    }

    actualizarNoticia(noticia: NoticiaEntity): Observable<RespuestaRemota<NoticiaEntity>> {
        return this.http.put<RespuestaRemota<NoticiaEntity>>(
            APIGAZE.BASE
            + Noticia.ACTUALIZAR_NOTICIA_UNICA,
            noticia
        )
    }

    eliminarNoticia(idPerfil: string, idNoticia: string): Observable<RespuestaRemota<string>> {
        return this.http.delete<RespuestaRemota<string>>(
            APIGAZE.BASE
            + Noticia.ELIMINAR_NOTICIA_UNICA
            + '/'
            + `${idPerfil}`
            + '/'
            + `${idNoticia}`
        )
    }

    apoyarNoticia(voto: VotoNoticiaEntity): Observable<RespuestaRemota<string>> {
        return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Noticia.VOTO_NOTICIA_UNICO, voto)
    }

    buscarNoticiasSinfiltroFechas(
        limite: number,
        pagina: number,
        perfil: string,
    ): Observable<HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>> {
        return this.http.get<RespuestaRemota<NoticiaEntity[]>>(
            APIGAZE.BASE +
            Noticia.TODAS_NOTICIAS
            + '/?'
            + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&perfil=' + `${perfil}`, 
            { observe: 'response' }
        )
    }

    buscarNoticiasPorFechas(
        limite: number,
        pagina: number,
        fechaInicial: string,
        fechaFinal: string,
        filtro: string,
        perfil: string,
    ): Observable<HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>> {
        return this.http.get<RespuestaRemota<NoticiaEntity[]>>(
            APIGAZE.BASE +
            Noticia.NOTICIAS_RECIENTES_PAG
            + '/?'
            + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&fechaInicial=' + `${fechaInicial}`
            + '&fechaFinal=' + `${fechaFinal}`
            + '&filtro=' + `${filtro}`
            + '&perfil=' + `${perfil}`, 
            { observe: 'response' }
        )
    }

    buscarNoticiasPorTitulo(
        titulo: string,
        limite: number,
        pagina: number
    ): Observable<HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>> {
        return this.http.get<RespuestaRemota<NoticiaEntity[]>>(
            APIGAZE.BASE +
            Noticia.BUSCAR_NOTICIAS_POR_TITULO
            + '/?'
            + 'titulo=' + `${titulo}`
            + '&limite=' + `${limite}`
            + '&pagina=' + `${pagina}`, 
            { observe: 'response' }
        )
    }

}