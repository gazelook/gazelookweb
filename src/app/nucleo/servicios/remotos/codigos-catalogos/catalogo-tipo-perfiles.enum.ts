export enum CodigosCatalogoTipoPerfil {
    CLASSIC = 'TIPERFIL_1',
    PLAYFUL = 'TIPERFIL_2',
    SUBSTITUTE = 'TIPERFIL_3',
    GROUP = 'TIPERFIL_4',
}