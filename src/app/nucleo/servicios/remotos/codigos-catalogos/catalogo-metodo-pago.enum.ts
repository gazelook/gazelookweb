export enum CodigosCatalogoMetodoPago {
    TARJETA = 'METPAG_1',
    PAYMENTEZ = 'METPAG_3',
}

export enum CodigosEstadoMetodoPago {
    PENDIENTE = 'EST_63',
    ACTIVA = 'EST_14'
}
 