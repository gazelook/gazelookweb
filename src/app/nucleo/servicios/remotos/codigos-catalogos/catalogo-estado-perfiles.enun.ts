export enum CodigosCatalogosEstadoPerfiles {
    PERFIL_SIN_CREAR = 'EST_00',
    PERFIL_CREADO = 'EST_88',
    PERFIL_ELIMINADO = 'EST_89',
    PERFIL_HIBERNADO = 'EST_19',
    PERFIL_ACTIVO = 'EST_20',
}