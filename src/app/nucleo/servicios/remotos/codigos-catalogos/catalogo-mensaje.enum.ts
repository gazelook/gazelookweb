
export enum CatalogoTipoMensaje {
  TEXTO = 'CTMSJ_1',
  ARCHIVO = 'CTMSJ_2',
  LLAMADA = 'CTMSJ_3',
  VIDEOLLAMADA = 'CTMSJ_4',
  AUDIO = 'CTMSJ_5',
  IMAGEN = 'CTMSJ_6',
  VIDEO = 'CTMSJ_7',
  //CATMED_2
  COMPARTIR_PROYECTO = 'CTMSJ_8',
  TRANSFERIR_PROYECTO = 'CTMSJ_9',
  COMPARTIR_CONTACTO = 'CTMSJ_10',
  COMPARTIR_NOTICIA = 'CTMSJ_11',
}

export enum EstadoMensaje {
  ELIMINADO = 'EST_100',
  ACTIVA = 'EST_99'
}
export enum CatalogoEstatusMensaje {
  ENVIADO = 'CTM_1',
  ENTREGADO = 'CTM_2',
  ACEPTADO = 'CTM_4',
  RECHAZADO = 'CTM_5',
}