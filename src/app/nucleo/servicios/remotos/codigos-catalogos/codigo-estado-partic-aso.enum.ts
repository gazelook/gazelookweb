export enum CodigoEstadoParticipanteAsociacion {
    INVITANTE = 'EST_92', // Cuando el usuario envia la invitacion, ACEPTADA
    ELIMINADO = 'EST_93',
    RECHAZADA = 'EST_95',
    CANCELADA = 'EST_94', 
    INVITADO = 'EST_96', // Cuando el usuario es invitado, ENVIADA
    CONTACTO = 'EST_159', // Cuando ambos algunos de los dos ya aceptaron la invitacion, y son contactos
}


export enum TipoParticipanteAsociacion {
    CONTACTO = 'CTAS_1',
    GRUPO = 'CTAS_2',
}