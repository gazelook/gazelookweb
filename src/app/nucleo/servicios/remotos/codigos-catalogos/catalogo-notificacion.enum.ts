export enum CatalogoEventoNotificacion {
    ELIMINAR_MENSAJE = 'CATEVTNOT_1',
    TRANSFERIR_PROYECTO = 'CATEVTNOT_2',
    MENSAJE_NO_LEIDO = 'CATEVTNOT_3',
    CREAR_COMENTARIO = 'CATEVTNOT_4',
    LLAMADA = 'CATEVTNOT_5',
    VIDEOLLAMADA = 'CATEVTNOT_6',
}

export enum CatalogoEstadoNotificacion {
    ACTIVA = 'EST_204',
    ELIMINADO = 'EST_205',
}
export enum CodigosAccionNotificacion {
    LLAMANDO = 'ACN_1',
    ACEPTAR = 'ACN_2',
    RECHAZAR = '‘ACN_3',
  }