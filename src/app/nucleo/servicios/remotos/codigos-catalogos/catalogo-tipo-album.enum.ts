export enum CodigosCatalogoTipoAlbum {
    GENERAL = 'CATALB_1',
    PERFIL = 'CATALB_2',
    LINK = 'CATALB_3',
    AUDIOS = 'CATALB_4',
}