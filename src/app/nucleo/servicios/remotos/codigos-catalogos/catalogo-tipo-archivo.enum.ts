export enum CodigosCatalogoTipoArchivo {
    IMAGEN = 'CATTIPMED_1',
    VIDEO = 'CATTIPMED_2',
    AUDIO = 'CATTIPMED_3',
    FILES = 'CATTIPMED_4',
} 
