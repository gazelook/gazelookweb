export enum CodigosCatalogoIdioma {
    ESPANOL = 'es',
    INGLES = 'en',
    ALEMAN = 'de',
    PORTUGUES = 'pt',
    FRANCES = 'fr',
    ITALIANO = 'it',
}

export enum CodigosCatalogoIdiomaCod {
    es = 'ESPANOL',
    en = 'INGLES',
    de = 'ALEMAN',
    pt = 'PORTUGUES',
    fr = 'FRANCES',
    it = 'ITALIANO',
}

export enum Codigos2CatalogoIdioma {
    ESPANOL = 'es',
    INGLES = 'en',
    ALEMAN = 'de',
    PORTUGUES = 'pt',
    FRANCES = 'fr',
    ITALIANO = 'it',
}

export enum CodigosCatalogoIdiomasPrincipales {
    ESPANOL = 'IDI_1',
    INGLES = 'IDI_2',
    ALEMAN = 'IDI_3',
    PORTUGUES = 'IDI_4',
    FRANCES = 'IDI_5',
    ITALIANO = 'IDI_6',
  }