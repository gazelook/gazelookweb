export enum CodigosCatalogoTipoAnuncio {
    PUBLICIDAD = 'TIPANU_01',
	INFORMACION = 'TIPANU_02',
	REFLEXION = 'TIPANU_03',
	FINANZAS = 'TIPANU_04',
}