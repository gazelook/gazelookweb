export enum CodigosCatalogoEstadoNoticia {
    SIN_CREAR = 'EST_00',
    ACTIVA = "EST_53",
    ELIMINADA = "EST_54",
}