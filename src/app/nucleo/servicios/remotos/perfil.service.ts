import { Usuarios } from './rutas/usuarios.enum';
import { PerfilEntity } from './../../../dominio/entidades/perfil.entity';
import { Injectable } from '@angular/core';
import {
	HttpClient,
	HttpHeaders,
	HttpParams,
	HttpResponse,
} from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { APIGAZE } from './rutas/api-gaze.enum';
import { Catalogo } from './rutas/catalogos.enum';
import { Perfiles } from './rutas/perfiles.enum';
import { RespuestaRemota } from '../../util/respuesta';
import { CatalogoTipoPerfilEntity } from '../../../dominio/entidades/catalogos/catalogo-tipo-perfil.entity';
import { UsuarioEntity } from 'src/app/dominio/entidades/usuario.entity';
import { debounceTime, map } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class PerfilServiceRemoto {
	constructor(private http: HttpClient) { }

	obtenerCatalogoTipoPerfil(): Observable<
		RespuestaRemota<CatalogoTipoPerfilEntity[]>
	> {
		return this.http.get<RespuestaRemota<CatalogoTipoPerfilEntity[]>>(
			APIGAZE.BASE + Catalogo.TIPO_PERFIL.toString()
		);
	}

	validarNombreDeContactoUnico(
		nombreContacto: string,
		traducirContactName: boolean
	): Observable<RespuestaRemota<object>> {
	

		let url = 	APIGAZE.BASE + Perfiles.NOMBRE_CONTACTO_UNICO + '/' + nombreContacto.trim() + '/' + traducirContactName
		let res = encodeURI(url)
		return this.http.get<RespuestaRemota<object>>(
			res
		);
	}

	obtenerDatosDelPerfil(id: string): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.get<RespuestaRemota<PerfilEntity>>(
			APIGAZE.BASE + Perfiles.OBTENER_DATOS_DEL_PERFIL + '/' + id
		);
	}

	actualizarPerfil(
		perfil: PerfilEntity
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.patch<RespuestaRemota<PerfilEntity>>(
			APIGAZE.BASE + Perfiles.ACTUALIZAR_PERFIL,
			perfil
		);
	}


	actualizarDatosUsuario(
		usuario: UsuarioEntity,
		idUsuario: string
	): Observable<RespuestaRemota<UsuarioEntity>> {
		return this.http.put<RespuestaRemota<UsuarioEntity>>(
			APIGAZE.BASE + Usuarios.ACTUALIZAR_USUARIO + '/' + idUsuario,
			usuario
		);
	}

	eliminarHibernarElPerfil(
		perfil: PerfilEntity
	): Observable<RespuestaRemota<string>> {
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
			body: perfil,
		};
		return this.http.delete<RespuestaRemota<string>>(
			APIGAZE.BASE + Perfiles.HIBERNAR_O_ELIMINAR_PERFIL,
			httpOptions
		);
	}

	crearPerfilEnElUsuario(
		perfil: PerfilEntity,
		usuario: UsuarioEntity
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.post<RespuestaRemota<PerfilEntity>>(
			APIGAZE.BASE + Perfiles.CREAR_PERFIL_EN_USUARIO + '/' + usuario._id,
			perfil
		);
	}

	activarPerfil(perfil: PerfilEntity): Observable<RespuestaRemota<string>> {
		return this.http.put<RespuestaRemota<string>>(
			APIGAZE.BASE + Perfiles.DESHIBERNAR_PERFIL + '/' + perfil._id,
			{}
		);
	}

	buscarPerfiles(
		busqueda: string,
		limite: number,
		pagina: number,
		perfil?: string
	): Observable<HttpResponse<RespuestaRemota<Array<PerfilEntity>>>> {
		return this.http.get<RespuestaRemota<PerfilEntity[]>>(APIGAZE.BASE + Perfiles.BUSCAR_PERFILES.toString()
			+ '/?' + 'limite=' + `${limite}`
			+ '&pagina=' + `${pagina}`
			+ '&busqueda=' + `${busqueda}`
			+ '&perfil=' + `${perfil}`,
			{ observe: 'response' })
	}

	buscarPerfilesPorNombre(
        query: string,
        idPerfil: string,
        limite: number,
        pagina: number
    ): Observable<HttpResponse<RespuestaRemota<Array<PerfilEntity>>>> {
        return this.http.get<RespuestaRemota<PerfilEntity[]>>(
			APIGAZE.BASE + 
			Perfiles.BUSCAR_PERFILES.toString()
			+ '/?' 
			+ 'limite=' + `${limite}`
			+ '&pagina=' + `${pagina}`
			+ '&busqueda=' + `${query}`
			+ '&perfil=' + `${idPerfil}`,
			{ observe: 'response' })
    }

	obtenerPerfilGeneral(
		idPerfil: string,
		idPerfilOtros: string = ''
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.get<RespuestaRemota<PerfilEntity>>(
			`${APIGAZE.BASE}${Perfiles.OBTENER_PERFIL_GENERAL}/?perfil=${idPerfil}&perfilOtros=${idPerfilOtros}`,

		);
	}

	obtenerPerfilBasico(
		idPerfil: string,
		perfilVisitante: string
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.get<RespuestaRemota<PerfilEntity>>(
				APIGAZE.BASE + 
				Perfiles.OBTENER_PERFIL_BASICO + 
				'/?' +
				'idPerfil=' + idPerfil +
				'&perfilVisitante=' + perfilVisitante
			)
	}

	obtenerInformacionDelPerfil(
		idPerfil: string,
		idPerfilOtros: string = ''
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.get<RespuestaRemota<PerfilEntity>>(
			APIGAZE.BASE + Perfiles.OBTENER_PERFIL_GENERAL + 
			'/?' + 
			'perfil=' + idPerfil +
			'&perfilOtros=' + idPerfilOtros
		)
	}

	obtenerResumenPerfilParaCoautorProyecto(
		idPerfil: string
	): Observable<RespuestaRemota<PerfilEntity>> {
		return this.http.get<RespuestaRemota<PerfilEntity>>(
			APIGAZE.BASE + Perfiles.OBTENER_RESUMEN_COAUTOR + 
			'/' + idPerfil
		)
	}

}




