import { LandingService } from './landing.service';
import { AlbumServiceRemoto } from './album.service';
import { AnuncioSistemaServiceRemoto } from './anuncio-sistema.service';
import { ComentarioIntercambioServiceRemoto } from './comentario.intercambio.service';
import { CuentaServiceRemoto } from './cuenta.service';
import { FinanzasServiceRemoto } from './finanzas.service';
import { IdiomaService } from './idioma.service';
import { IntercambioServiceRemoto } from './intercambio.service';
import { MediaServiceRemoto } from './media.service';
import { MensajesAsociacionServiceRemoto } from './mensajes-asociacion.service';
import { TipoMonedaServiceRemoto } from './moneda.service';
import { NoticiaServiceRemoto } from './noticia.service';
import { PagoService } from './pago.service';
import { ParticipanteAsociacionServiceRemoto } from './participante-asociacion.service';
import { PensamientoService } from './pensamiento.service';
import { PerfilServiceRemoto } from './perfil.service';
import { ProyectoServiceRemoto } from './proyectos.service';
import { UbicacionServiceRemoto } from './ubicacion.service';


export const servicesRemotos: any[] = [
    AlbumServiceRemoto,
    AnuncioSistemaServiceRemoto,
    ComentarioIntercambioServiceRemoto,
    CuentaServiceRemoto,
    FinanzasServiceRemoto,
    IdiomaService,
    IntercambioServiceRemoto,
    MediaServiceRemoto,
    MensajesAsociacionServiceRemoto,
    TipoMonedaServiceRemoto,
    NoticiaServiceRemoto,
    PagoService,
    ParticipanteAsociacionServiceRemoto,
    PensamientoService,
    PerfilServiceRemoto,
    ProyectoServiceRemoto,
    UbicacionServiceRemoto,
    LandingService    
];

export * from './album.service';
export * from './anuncio-sistema.service';
export * from './comentario.intercambio.service';
export * from './cuenta.service';
export * from './finanzas.service';
export * from './idioma.service';
export * from './intercambio.service';
export * from './media.service';
export * from './mensajes-asociacion.service';
export * from './moneda.service';
export * from './noticia.service';
export * from './pago.service';
export * from './participante-asociacion.service';
export * from './pensamiento.service';
export * from './perfil.service';
export * from './proyectos.service';
export * from './ubicacion.service';
export * from './landing.service';