import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Base64} from 'js-base64';
import { sha256 } from 'js-sha256';

import { environment } from 'src/environments/environment';

import { Pago } from '@core/servicios/remotos/rutas/pago.enum';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { RespuestaRemota } from '@core/util/respuesta';

@Injectable({
  providedIn: 'root'
})
export class PagoPaymentezService {

  constructor(private http: HttpClient) { }


  public refundPaymentez(id: string): Observable<RespuestaRemota<any>> {
    const headers = this.headers();
    return this.http.post<any>(APIGAZE.API_PAYMENTEZ + Pago.REFUND_PAYMENTEZ.toString(), id, {headers});
  }

  private authToken(): string {
    const serverApplicationCode = environment.serverAppCodePaymentez;
    const serverAppKey = environment.serverAppKeyPaymentez;
    const unixTimestamp   = String(Math.floor(new Date().getTime() / 1000));
    const uniqTokenString = serverAppKey + unixTimestamp;
    const uniqTokenHash = sha256(uniqTokenString);
    const stringAuthToken = Base64.encode(`${serverApplicationCode};${unixTimestamp};${uniqTokenHash}`);
    console.log(stringAuthToken);
    return stringAuthToken;


  }
  private headers(): HttpHeaders {
    const authToken = this.authToken();
    const headers = new HttpHeaders()
    .set('Auth-Token', authToken);
    return headers;
  }
}
