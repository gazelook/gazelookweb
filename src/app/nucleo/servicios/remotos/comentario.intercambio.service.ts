import { ComentarioIntercambioEntity } from 'dominio/entidades/comentario-intercambio.entity';
import { RespuestaRemota } from '@core/util/respuesta';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { ComentarioEntity } from 'dominio/entidades/comentario.entity';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Comentario } from '@core/servicios/remotos/rutas/comentario.enum';

@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioServiceRemoto {

    constructor(
        private http: HttpClient
    ) {
        
    }

    crearComentario(comentario: ComentarioIntercambioEntity): Observable<RespuestaRemota<ComentarioIntercambioEntity>> {
        return this.http.post<RespuestaRemota<ComentarioIntercambioEntity>>(APIGAZE.BASE + Comentario.CREAR_COMENTARIO_INTERCAMBIO.toString(), comentario)
    }

    obtenerComentarios(
        idProyecto: string,
        limite: number,
        pagina: number
    ): Observable<HttpResponse<RespuestaRemota<ComentarioEntity[]>>> {
        return this.http.get<RespuestaRemota<ComentarioEntity[]>>(APIGAZE.BASE + Comentario.OBTENER_COMENTARIOS_PROYECTO.toString()
            + '/?'
            + 'idProyecto=' + `${idProyecto}`
            + '&limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            , { observe: 'response' }
        )
    }

    eliminarComentario(
        idIntercambio: string,
        coautor?: string,
        idComentario?: string
    ): Observable<RespuestaRemota<string>> {
        return this.http.delete<RespuestaRemota<string>>(APIGAZE.BASE + Comentario.ELIMINAR_COMENTARIO_INTERCAMBIO.toString()
            + '/?'
            + 'idIntercambio=' + `${idIntercambio}`
            + '&coautor=' + `${coautor}`
            + '&idComentario=' + `${idComentario}`
        )
    }

    eliminarMiComentario(
        idComentario?: string,
        idCoautor?: string,
    ): Observable<RespuestaRemota<string>> {
        return this.http.delete<RespuestaRemota<string>>(APIGAZE.BASE + Comentario.ELIMINAR_MI_COMENTARIO_INTERCAMBIO.toString()
            + '/?'
            + '&idComentario=' + `${idComentario}`
            + '&idCoautor=' + `${idCoautor}`
        )
    }

    obtenerContenidoComentario(
        comentarios: Array<ComentarioEntity>
    ): Observable<RespuestaRemota<ComentarioEntity[]>> {
        return this.http.post<RespuestaRemota<ComentarioEntity[]>>(
            APIGAZE.BASE + Comentario.OBTENER_LISTA_COMENTARIOS.toString(), 
            {
                'listaComentarios': comentarios
            }
        )
    }



    
}