export enum Intercambio {
    CREAR_INTERCAMBIO = 'intercambio',
    MIS_INTERCAMBIOS_TIPO = 'intercambio/intercambios-perfil',
    INTERCAMBIO_UNICO = 'intercambio',
    ACTUALIZAR_INTERCAMBIO = 'intercambio/actualizar-intercambio',
    ELIMINAR_INTERCAMBIO = 'intercambio',
    CAMBIAR_STATUS = 'intercambio/cambia-status-intercambio',
    INTERCAMBIO_RECIENTES = 'intercambio/intercambios-recientes',
    TIPO_INTERCAMBIO = 'intercambio/tipos-intercambio'
}