export enum Comentario {
    // PROYECTOS
    
    CREAR_COMENTARIO = 'comentario',
    OBTENER_COMENTARIOS_PROYECTO = 'comentario',
    ELIMINAR_COMENTARIO = 'comentario-proyecto',
    ELIMINAR_MI_COMENTARIO = 'mi-comentario',
    OBTENER_LISTA_COMENTARIOS = 'comentario/lista-comentario-id',

    // INTERCAMBIOS
    CREAR_COMENTARIO_INTERCAMBIO = 'comentario-intercambio',
    ELIMINAR_COMENTARIO_INTERCAMBIO = 'comentario-intercambio',
    ELIMINAR_MI_COMENTARIO_INTERCAMBIO = 'comentario-intercambio/mi-comentario',

}