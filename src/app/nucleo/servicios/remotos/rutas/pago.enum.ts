export enum Pago {
    PREPARAR_STRIPE = 'transaccion/stripe-pago',
    PREPARAR_PAYPAL = 'transaccion/paypal-pago',
    CREAR_ORDEN_PAGO = 'pago-valor-extra',
    VALIDAR_PAGO_EXTRA = 'pago/validar-valor-extra',
    REFUND_PAYMENTEZ = '/v2/transaction/refund',
    COINPAYMENTES_RATES = 'transaccion/coinpayments-rates'
}
