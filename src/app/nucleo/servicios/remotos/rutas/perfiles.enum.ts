export enum Perfiles {
    TIPO_PERFILES = "perfiles/tipo-perfiles",
    NOMBRE_CONTACTO_UNICO = 'perfil/nombre-contacto-unico',
    OBTENER_DATOS_DEL_PERFIL = 'perfil/datos',
    ACTUALIZAR_PERFIL = 'perfil',
    HIBERNAR_O_ELIMINAR_PERFIL = 'perfil',
    CREAR_PERFIL_EN_USUARIO = 'perfil',
    DESHIBERNAR_PERFIL = 'perfil/activar',
    BUSCAR_PERFILES = 'perfiles-sistema',
    OBTENER_PERFIL_GENERAL = 'perfil-general',
    OBTENER_PERFIL_BASICO = 'perfil-basico',
    OBTENER_RESUMEN_COAUTOR = 'perfil/album'
}