import {PagoEntity} from 'dominio/entidades/pago.entity';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {APIGAZE} from '@core/servicios/remotos/rutas/api-gaze.enum';
import {Catalogo} from '@core/servicios/remotos/rutas/catalogos.enum';
import {Pago} from '@core/servicios/remotos/rutas/pago.enum';
import {RespuestaRemota} from '@core/util/respuesta';
import {CatalogoMetodoPagoEntity, MetodoPagoStripeEntity} from 'dominio/entidades/catalogos/catalogo-metodo-pago.entity';
import {CatalogoMetodoPagoExtraModel, PagoStripeModel} from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import {CoinPaymentsRatesEntity} from 'dominio/entidades/catalogos/coinpayments-rates.entity';

@Injectable({providedIn: 'root'})
export class PagoService {

  constructor(private http: HttpClient) {

  }

  obtenerCatalogoMetodosPago(): Observable<RespuestaRemota<CatalogoMetodoPagoEntity[]>> {
    return this.http.get<RespuestaRemota<CatalogoMetodoPagoEntity[]>>(APIGAZE.BASE + Catalogo.METODOS_PAGOS.toString());
  }

  crearOrdenPagoExtra(data: CatalogoMetodoPagoExtraModel): Observable<RespuestaRemota<PagoEntity>> {
    return this.http.post<any>(APIGAZE.BASE_MANTENIMIENTO + Pago.CREAR_ORDEN_PAGO.toString(), data);
  }

  validarPagoExtra(idTransaccion: string): Observable<RespuestaRemota<boolean>> {
    return this.http.post<any>(APIGAZE.BASE_MANTENIMIENTO + Pago.VALIDAR_PAGO_EXTRA, {idTransaccion});
  }

  prepararPagoStripe(data: PagoStripeModel): Observable<RespuestaRemota<MetodoPagoStripeEntity>> {
    return this.http.post<any>(APIGAZE.BASE + Pago.PREPARAR_STRIPE.toString(), data);
  }

  prepararPagoPaypal(data: any): Observable<RespuestaRemota<any>> {
    return this.http.post<any>(APIGAZE.BASE + Pago.PREPARAR_PAYPAL.toString(), data);
  }

  obtenerCoinPaymentsRates(): Observable<RespuestaRemota<CoinPaymentsRatesEntity>> {
    return this.http.get<any>(APIGAZE.BASE + Pago.COINPAYMENTES_RATES.toString());
  }

}
