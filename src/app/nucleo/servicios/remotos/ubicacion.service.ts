import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { Catalogo } from '@core/servicios/remotos/rutas/catalogos.enum';
import { CatalogoLocalidadEntity } from 'dominio/entidades/catalogos/catalogo-localidad.entity';
import { CatalogoPaisEntity } from 'dominio/entidades/catalogos/catalogo-pais.entity';
import { Observable } from 'rxjs';
import { RespuestaRemota } from '@core/util/respuesta';

@Injectable({ providedIn: 'root' })
export class UbicacionServiceRemoto {

    constructor(
        private http: HttpClient
    ) {

    }

    // Obtener el catalogo de paises
    obtenerCatalogoPaises(): Observable<RespuestaRemota<CatalogoPaisEntity[]>> {
        return this.http.get<RespuestaRemota<CatalogoPaisEntity[]>>(APIGAZE.BASE + Catalogo.PAIS.toString());
    }

    // Obtener el catalogo de localidades por pais
    obtenerCatalogoLocalidadesPorNombrePorPaises(pais:string, query:string): Observable<RespuestaRemota<CatalogoLocalidadEntity[]>> {
        return this.http.get<RespuestaRemota<CatalogoLocalidadEntity[]>>(APIGAZE.BASE + Catalogo.BUSCAR_LOCALIDAD.toString() + pais + '/' + query);
    }

    buscarLocalidadesPorNombrePaisConPaginacion(
        limite: number,
        pagina: number,
        codigoPais: string,
        busqueda: string
    ): Observable<HttpResponse<RespuestaRemota<Array<CatalogoLocalidadEntity>>>> {
        return this.http.get<RespuestaRemota<CatalogoLocalidadEntity[]>>(
            APIGAZE.BASE +
            Catalogo.BUSCAR_LOCALIDAD
            + '/?'
            + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&codigoPais=' + `${codigoPais}`
            + '&busqueda=' + `${busqueda}`, 
            { observe: 'response' }
        )
    }

}