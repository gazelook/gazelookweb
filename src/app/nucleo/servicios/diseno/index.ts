import { ItemCircularRectangularMetodosCompartidos } from './item-cir-rec.service';
import { DialogoServicie } from './dialogo.service';
import { EstiloDelTextoServicio } from './estilo-del-texto.service';

export const servicesDiseno: any[] = [
    EstiloDelTextoServicio,
    DialogoServicie,
    ItemCircularRectangularMetodosCompartidos,
];

export * from './estilo-del-texto.service';
export * from './item-cir-rec.service';
export * from './dialogo.service';
