import { Injectable } from '@angular/core';
import { ProyectoActivoEnrutador } from 'presentacion/enrutador/enrutador.component';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class ProyectoServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService,
    private metodosSessionStorageService: MetodosSessionStorageService
  ) {

  }

  guardarProyectoActivoTerciosEnSessionStorage(proyectos: Array<ProyectoActivoEnrutador>) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.PROYECTO_ACTIVO_TERCIOS, proyectos)
  }

  obtenerProyectoActivoTerciosDelSessionStorage() {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.PROYECTO_ACTIVO_TERCIOS)
  }

  removerProyectoActivoTerciosDelSessionStorage() {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.PROYECTO_ACTIVO_TERCIOS)
  }

  guardarProyectoActivoEnSessionStorage(proyecto: ProyectoModel) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.PROYECTO_ACTIVO, proyecto)
  }

  obtenerProyectoActivoDelSessionStorage(): ProyectoModel {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.PROYECTO_ACTIVO)
  }

  removerProyectoActivoDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesSessionStorage.PROYECTO_ACTIVO)
  }

}
