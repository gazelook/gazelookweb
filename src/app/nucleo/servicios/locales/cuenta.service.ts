import { Injectable } from '@angular/core';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class CuentaServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService,
    private metodosSesionStorage: MetodosSessionStorageService,
  ) { }

  // Guardar usuario en el storage
  guardarUsuarioEnLocalStorage(usuario: UsuarioModel): void {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.USUARIO, usuario)
  }

  // OBtener usuario del storage
  obtenerUsuarioDelLocalStorage(): UsuarioModel {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.USUARIO)
  }

  removerUsuarioDelLocalStorage() {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.USUARIO)
  }

  // Guardar usuario en el storage
  guardarUsuarioEnSessionStorage(usuario: UsuarioModel): void {
    this.metodosSesionStorage.guardar(LlavesSessionStorage.USUARIO, usuario);
  }

  // OBtener usuario del storage
  obtenerUsuarioDelSessionStorage(): UsuarioModel {
    return this.metodosSesionStorage.obtener(LlavesSessionStorage.USUARIO);
  }

  removerUsuarioDelSessionStorage() {
    return this.metodosSesionStorage.remover(LlavesSessionStorage.USUARIO);
  }

  eliminarSessionStorage() {
    this.metodosSesionStorage.eliminarSessionStorage()
  }

  guardarFirebaseUIDEnLocalStorage(uid: string) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.FIREBASE_UID, uid)
  }

  obtenerFirebaseUIDEnLocalStorage(): string {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.FIREBASE_UID)
  }
}
