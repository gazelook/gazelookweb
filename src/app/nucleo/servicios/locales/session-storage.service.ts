import { Injectable } from "@angular/core";
import { MetodosSessionStorageService } from "@core/util/metodos-session-storage.service";

@Injectable({ providedIn: 'root' })
export class SessionStorageServicie {

  constructor(
    private sesionStorage: MetodosSessionStorageService
  ) { }
}
