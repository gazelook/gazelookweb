import { Injectable } from '@angular/core';
import { ItemSelector } from 'src/app/compartido/diseno/modelos/elegible.interface';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';

@Injectable({ providedIn: 'root' })
export class TipoMonedaServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService
  ) {

  }

  guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda: ItemSelector[]) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.TIPO_MONEDA, catalogoTipoMoneda)
  }

  obtenerCatalogoTipoMonedaDelLocalStorage(): ItemSelector[] {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.TIPO_MONEDA)
  }

  eliminarCatalogoTipoMonedaDelLocalStorage() {
    this.metodosLocalStorageService.remover(LlavesLocalStorage.TIPO_MONEDA)
  }

}
