import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CatalogoMetodoPagoModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import { CatalogoPaisModel } from 'dominio/modelo/catalogos/catalogo-pais.model';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';

@Injectable({ providedIn: 'root' })
export class LocalStorage {

  private observablePaises: Subject<CatalogoPaisModel[]>
  private observableCatalogoPerfiles: Subject<CatalogoTipoPerfilModel[]>

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService
  ) {
    this.observablePaises = new Subject<CatalogoPaisModel[]>()
    this.observableCatalogoPerfiles = new Subject<CatalogoTipoPerfilModel[]>()
  }
  //=====================IDIOMA LOCAL
  obtenerIdiomaLocal(): CatalogoIdiomaEntity {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.IDIOMA)
  }

  guardarIdiomaLocal(idioma: CatalogoIdiomaEntity) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.IDIOMA, idioma)
  }
  //=======================IDIOMAS
  obtenerIdiomas(): Array<CatalogoIdiomaEntity> {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.IDIOMAS)
  }

  guardarIdiomas(idiomas: Array<CatalogoIdiomaEntity>) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.IDIOMAS, idiomas)
  }
  //===================== TOKEN AUTENTICACION
  obtenerTokenAutenticacion() {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.AUTH_TOKEN)
  }
  guardarTokenAutenticacion(token: string) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.AUTH_TOKEN, token)
  }

  obtenerTokenRefresh() {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.REFRESH_TOKEN)
  }
  guardarTokenRefresh(token: string) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.REFRESH_TOKEN, token)
  }
  /*
  obtenerTipoPerfiles(){
      return this.metodosLocalStorageService.obtener(LlavesLocalStorage.TIPO_PERFILES_USER)
  }

  guardarTipoPerfiles(tipoPerfil:Array<any>){
      this.metodosLocalStorageService.guardar(LlavesLocalStorage.TIPO_PERFILES_USER, tipoPerfil)
  }*/

  almacenarCatalogoPerfiles(tipoPerfiles: CatalogoTipoPerfilModel[]) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.TIPO_PERFILES, tipoPerfiles)
  }

  obtenerCatalogoPerfiles(): CatalogoTipoPerfilModel[] {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.TIPO_PERFILES)
  }

  almacenarMetodosPago(metodos: CatalogoMetodoPagoModel[]) {
    return this.metodosLocalStorageService.guardar(LlavesLocalStorage.METODOS_PAGO, metodos);
  }

  obtenerMetodosPago(): CatalogoMetodoPagoModel[] {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.METODOS_PAGO);
  }

  eliminarUsuario() {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.USUARIO);
  }

  eliminarRefreshToken() {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.REFRESH_TOKEN);
  }
  eliminarAutenticacionToken() {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.AUTH_TOKEN);
  }

  almacenarPerfilSeleccionado(perfil: PerfilModel) {
    return this.metodosLocalStorageService.guardar(LlavesLocalStorage.PERFIL_SELECCIONADO, perfil);
  }

  removerPerfilSeleccionado() {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.PERFIL_SELECCIONADO)
  }

  obtenerPerfilSeleccionado() {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.PERFIL_SELECCIONADO);
  }
  eliminarVariableStorage(llave: string) {
    this.metodosLocalStorageService.remover(llave)
  }
}
