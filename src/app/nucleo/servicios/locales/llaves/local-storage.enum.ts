export enum LlavesLocalStorage {
    PAISES = 'paises',
    LOCALIDADES = 'localidades',
    IDIOMAS = 'idiomas',
    LISTA_ALBUM_REGISTRO = 'lista_album_registro',
    IDIOMA = 'idiomaSeleccionado',
    AUTH_TOKEN = 'auth_token',
    REFRESH_TOKEN = 'refresh_token',
    TIPO_PERFILES = 'tipoPerfiles',
    FIREBASE_UID = 'firebase-uid',
    METODOS_PAGO = 'metodopago',
    USUARIO = 'usuario',
    ALBUM_ACTIVO = 'album_activo',
    PERFIL_SELECCIONADO = 'perfil_seleccionado',
    PERFIL_ACTIVO = 'perfil_activo',
    PERFIL_ORIGINAL = 'perfil_original',
    PROYECTO_ACTIVO = 'proyecto_activo',
    TIPO_MONEDA = 'tipo_moneda',
    NOTICIA_ACTIVA = 'noticia_activa',
    NOTICIA_ACTIVA_TERCIOS = 'noticia_activa_tercios',
    AVISO_IDIOMAS = 'aviso-idiomas',
    ARCHIVOS_DEFAULT = 'archivos-default'
}