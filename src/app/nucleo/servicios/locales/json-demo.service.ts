import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProyectoEntity } from 'dominio/entidades/proyecto.entity';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { RespuestaRemota } from '@core/util/respuesta';
import { ComentarioEntity } from 'dominio/entidades/comentario.entity';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';


@Injectable({ providedIn: 'root' })
export class JsonDemoServiceLocal {

  constructor(
    private http: HttpClient
  ) {

  }

  obtenerContactosDemo(): Observable<any> {
    return this.http.get("assets/recursos/json/listaContactosDemo.json")
  }

  obtenerFotosPrueba(): Observable<any> {
    return this.http.get("assets/recursos/json/links_demo.json")
  }

  obtenerPensamientoDemo(): Observable<RespuestaRemota<PensamientoModel[]>> {
    return this.http.get<RespuestaRemota<PensamientoModel[]>>("assets/recursos/json/listaPensamientosDemo.json")
  }

  obtenerProyectosDemo(): Observable<any> {
    return this.http.get("assets/recursos/json/listaProyectosDemo.json")
  }

  obtenerNoticiasDemo(): Observable<any> {
    return this.http.get("assets/recursos/json/listaNoticiasDemo.json")
  }


  obtenerPerfilGeneralDemo(): Observable<any> {
    return this.http.get("assets/recursos/json/perfilGeneralDemo.json")
  }


  obtenerProyectoDemo(): Observable<RespuestaRemota<ProyectoEntity>> {
    return this.http.get<RespuestaRemota<ProyectoEntity>>("assets/recursos/json/proyecto-unico.json")
  }

  obtenerNoticiaDemo(): Observable<RespuestaRemota<NoticiaEntity>> {
    return this.http.get<RespuestaRemota<NoticiaEntity>>("assets/recursos/json/noticia-unico.json")
  }

  obtenerComentarios(): Observable<RespuestaRemota<ComentarioEntity[]>> {
    return this.http.get<RespuestaRemota<ComentarioEntity[]>>("assets/recursos/json/lista-comentarios.json")
  }
}
