import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { Injectable } from '@angular/core';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';
import { IntercambioActivoEnrutador } from '@env/src/app/presentacion/enrutador/enrutador.component';

@Injectable({ providedIn: 'root' })
export class IntercambioServiceLocal {

    constructor(
        private metodosLocalStorageService: MetodosLocalStorageService,
        private metodosSessionStorageService: MetodosSessionStorageService
    ) { }

    guardarIntercambioActivoTerciosEnSessionStorage(intercambio: Array<IntercambioActivoEnrutador>) {
        this.metodosSessionStorageService.guardar(LlavesSessionStorage.INTERCAMBIO_ACTIVO_TERCIOS, intercambio)
    }

    obtenerIntercambioActivoTerciosDelSessionStorage() {
        return this.metodosSessionStorageService.obtener(LlavesSessionStorage.INTERCAMBIO_ACTIVO_TERCIOS)
    }

    guardarIntercambioActivoEnSessionStorage(Intercambio: IntercambioModel) {
        this.metodosSessionStorageService.guardar(LlavesSessionStorage.INTERCAMBIO_ACTIVO, Intercambio)
    }

    obtenerIntercambioActivoDelSessionStorage(): IntercambioModel {
        return this.metodosSessionStorageService.obtener(LlavesSessionStorage.INTERCAMBIO_ACTIVO)
    }

    removerIntercambioActivoDelSessionStorage() {
        this.metodosSessionStorageService.remover(LlavesSessionStorage.INTERCAMBIO_ACTIVO)
    }
}
