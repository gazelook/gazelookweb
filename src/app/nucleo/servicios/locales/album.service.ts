import { Injectable } from '@angular/core';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { AlbumActivoEnrutador } from 'presentacion/enrutador/enrutador.component';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class AlbumServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService,
    private metodosSessionStorageService: MetodosSessionStorageService,
  ) { }

  // metodosSessionStorageService
  guardarAlbumEnSessionStorage(album: AlbumModel) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.ALBUM_ACTIVO, album)
  }

  obtenerAlbumEnSessionStorage(): AlbumModel {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.ALBUM_ACTIVO)
  }

  removerAlbumActivoDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesSessionStorage.ALBUM_ACTIVO)
  }

  guardarAlbumActivoTercioEnSessionStorage(albums: Array<AlbumActivoEnrutador>) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.ALBUM_ACTIVO_TERCIO, albums)
  }

  obtenerAlbumActivoTercioDelSessionStorage(): Array<AlbumActivoEnrutador> {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.ALBUM_ACTIVO_TERCIO)
  }

  removerAlbumActivoTercioDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesSessionStorage.ALBUM_ACTIVO)
  }

  obtenerListaArchivosDefault(): ArchivoModel[] {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.ARCHIVOS_DEFAULT)
  }

  guardarListaArchivosDefault(archivos: ArchivoModel[]) {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.ARCHIVOS_DEFAULT, archivos)
  }
}
