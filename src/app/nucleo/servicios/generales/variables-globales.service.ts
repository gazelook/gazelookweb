import { Subject } from 'rxjs';
import { CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-moneda.enum';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VariablesGlobales {
	// Generales
	public mostrarMundo: boolean
	public semillaItemsId: number

	public valorNetoInicialParaProyectos: string
	public valorFormateadoInicialParaProyectos: string
	public placeholderMonedaPicker: string
	public valorNetoInicialParaSuscripcion: string
	public valorFormateadoInicialParaSuscripcion: string

	public montoBaseSuspcripcion: number
	public tipoDeMonedaBase: CodigosCatalogoTipoMoneda
	public tamanoMaximoArchivos: number

	public cambiosDeEstadoEnElPerfil$: Subject<PerfilModel>
	public nuevoPerfilAnadido$: Subject<PerfilModel>

	public animacion: DataAnimacion

	public mostrarAvisoIdiomas: boolean

	constructor() {
		this.mostrarMundo = true
		this.semillaItemsId = 0

		this.valorNetoInicialParaProyectos = '0000000000'
		this.valorFormateadoInicialParaProyectos = '0.000.000.000'
		this.placeholderMonedaPicker = '€/$/...'
		this.valorNetoInicialParaSuscripcion = '0'
		this.valorFormateadoInicialParaSuscripcion = '0'
		this.montoBaseSuspcripcion = 18
		this.tipoDeMonedaBase = CodigosCatalogoTipoMoneda.EUR
		this.tamanoMaximoArchivos = 160000000

		this.mostrarAvisoIdiomas = false

		this.cambiosDeEstadoEnElPerfil$ = new Subject()
		this.nuevoPerfilAnadido$ = new Subject()

		this.configurarDataAnimacion()
	}

	configurarDataAnimacion(
		landingPageAbierta: boolean = false
	) {
		this.animacion = {
			mostrarCapaGif: false,
			ocultarLogin: false,
			terciosSinBorde: false,
			terciosSinBordeMedio: false,
			ocultarPortadas: false,
			mostrarGifUno: false,
			mostrarGifDos: false,
			landingPageAbierta: landingPageAbierta,
			conTransicion: false,
			imagenFondo: false,
			$subjectCapaGif: new Subject<boolean>(),
			$cambioDeIdioma: new Subject<string>()
		}
	}

	configurarDataAnimacionParaGifs(
		mostrarGifDos: boolean = false,
		terciosSinBordeMedio: boolean = false,
		lasDemasEnTrue: boolean = false
	) {
		this.animacion.mostrarGifDos = mostrarGifDos
		this.animacion.terciosSinBordeMedio = terciosSinBordeMedio
		// Demas variables
		this.animacion.conTransicion = lasDemasEnTrue
		this.animacion.ocultarPortadas = lasDemasEnTrue
		this.animacion.terciosSinBorde = lasDemasEnTrue
		this.animacion.ocultarLogin = lasDemasEnTrue
		this.animacion.mostrarCapaGif = lasDemasEnTrue
		this.animacion.imagenFondo = lasDemasEnTrue
	}
}
export interface DataAnimacion {
	mostrarCapaGif: boolean,
	ocultarLogin: boolean,
	terciosSinBorde: boolean,
	terciosSinBordeMedio: boolean,
	ocultarPortadas: boolean,
	mostrarGifUno: boolean,
	mostrarGifDos: boolean,
	landingPageAbierta: boolean,
	conTransicion: boolean,
	imagenFondo: boolean,
	$subjectCapaGif: Subject<boolean>,
	$cambioDeIdioma: Subject<string>,
}
