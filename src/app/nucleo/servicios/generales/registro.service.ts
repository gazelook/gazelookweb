import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params, Router } from '@angular/router';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { UbicacionDelComponente } from 'presentacion/enrutador/enrutador.component';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import { DireccionModel } from 'dominio/modelo/entidades/direccion.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { CodigosCatalogosEstadoPerfiles } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-perfiles.enun';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { RegistroParams } from 'dominio/modelo/parametros/registro-parametros.interface';
import { AccionAlbum, AccionEntidad, CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';

/*
    - Interfaz para metodos auxiliares del componente registro (creacion, update)
*/

@Injectable({ providedIn: 'root' })
export class RegistroService {

    constructor(
        private cuentaNegocio: CuentaNegocio,
        private formBuilder: FormBuilder,
        private generadorId: GeneradorId,
        private perfilNegocio: PerfilNegocio,
        private albumNegocio: AlbumNegocio,
    ) {

    }

    determinarAccionPerfilPorUrl(url: string): AccionEntidad {
        if (url.indexOf('registro') >= 0) {
            return AccionEntidad.REGISTRO
        }

        if (url.indexOf('crear') >= 0) {
            return AccionEntidad.CREAR
        }

        if (url.indexOf('actualizar') >= 0) {
            return AccionEntidad.ACTUALIZAR
        }
    }

    validarParametrosSegunAccionEntidad(
        params: RegistroParams,
        urlParams: Params
    ): RegistroParams {
        params.estado = false
        // Validar parametros segun accion entidad
        if (params.accionEntidad) {
            switch (params.accionEntidad) {
                case AccionEntidad.REGISTRO:
                    params.tipoPerfil = this.perfilNegocio.obtenerTipoPerfilActivo()
                    if (params.tipoPerfil && params.tipoPerfil.codigo) {
                        params.estado = true
                    }
                    return params
                case AccionEntidad.CREAR:
                    params.tipoPerfil = this.perfilNegocio.obtenerTipoPerfilActivo()
                    if (params.tipoPerfil && params.tipoPerfil.codigo) {
                        params.estado = true
                    }
                    return params
                case AccionEntidad.ACTUALIZAR:
                    params.tipoPerfil = this.perfilNegocio.obtenerTipoPerfilActivo()
                    if (params.tipoPerfil && urlParams.id) {
                        params.id = urlParams.id
                        params.estado = true
                    }
                    return params
                // case AccionEntidad.VISITAR:
                //     return params
                default: break;
            }
        }
        return params
    }

    validarParametrosSegunAccionEntidadEnrutador(
        params: RegistroParams
    ): RegistroParams {
        params.estado = false

        if (params.accionEntidad) {
            switch (params.accionEntidad) {
                case AccionEntidad.REGISTRO:
                    params.estado = (params.codigoTipoPerfil) ? true : false
                    return params
                case AccionEntidad.CREAR:
                    params.estado = (params.codigoTipoPerfil) ? true : false
                    return params
                case AccionEntidad.ACTUALIZAR:
                    params.estado = (params.codigoTipoPerfil && params.id) ? true : false
                    return params
                default: break;
            }
        }
        return params
    }

    // Define el uso del album segun la accion de la entidad desde la que se accede al perfil
    definirAccionDelAlbumSegunAccionEntidad(
        entidad: CodigosCatalogoEntidad,
        accionEntidad: AccionEntidad,
    ): AccionAlbum {
        if (entidad === CodigosCatalogoEntidad.PERFIL) {
            if (accionEntidad === AccionEntidad.REGISTRO) {
                return AccionAlbum.CREAR
            }

            if (accionEntidad === AccionEntidad.CREAR || accionEntidad === AccionEntidad.ACTUALIZAR) {
                return AccionAlbum.ACTUALIZAR
            }
        }

        if (entidad === CodigosCatalogoEntidad.NOTICIA) {
            // Por definir
            return
        }

        if (entidad === CodigosCatalogoEntidad.PROYECTO) {
            // Por definir
            return
        }
    }

    // Inicializar controles para el formulario
    inicializarControlesDelFormulario(
        perfil: PerfilModel,
        session: boolean = true
    ): FormGroup {
        // Definir email y contrasena
        const loginInfo = this.cuentaNegocio.obtenerEmailConContrasenaDelUsuario(session)


        let direccion = ''
        if (perfil && perfil.direcciones && perfil.direcciones.length > 0) {
            direccion = perfil.direcciones[0].descripcion
        }

        let telefono = ''
        if (perfil && perfil.telefonos && perfil.telefonos.length > 0) {
            telefono = perfil.telefonos[0].numero
        }

        let nombre = ''
        let apellido = ''
        if (perfil && perfil.nombre) {
            const aux = perfil.nombre.split('-')
            if (aux[0] && aux[0].length > 1) {
                nombre = aux[0]
            }

            if (aux[1] && aux[1].length > 1) {
                apellido = aux[1]
            }
        }

        let fechaNacimiento = null

        if (loginInfo.fechaNacimiento === null) {
            fechaNacimiento = ''
        } else if (
            loginInfo.fechaNacimiento !== null &&
            loginInfo.fechaNacimiento.toString().indexOf('T') >= 0
        ) {
            fechaNacimiento = loginInfo.fechaNacimiento.toString().split('T')[0]
        } else {
            fechaNacimiento = loginInfo.fechaNacimiento
        }

        const registroForm: FormGroup = this.formBuilder.group({
            nombreContacto: [
                (perfil) ? perfil.nombreContacto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z0-9 ]+$'),
                    Validators.minLength(1)
                ],
            ],
            nombre: [
                nombre,
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(1)
                ]
            ],
            apellido: [
                apellido,
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(1)
                ]
            ],
            email: [
                (loginInfo) ? loginInfo.email : '',
                [
                    Validators.required,
                    Validators.email
                ]
            ],
            confirmEmail: [
                '',
                [
                    Validators.required,
                    Validators.email
                ]
            ],
            contrasena: [
                (loginInfo) ? loginInfo.contrasena : '',
                [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.pattern('^[A-Za-z0-9@]{6,12}$')
                ]
            ],
            direccion: [
                direccion,
                [
                    Validators.pattern('^[A-Za-z0-9-, ]+$')
                ]
            ],
            telefono: [
                telefono,
                [
                    Validators.pattern('^[0-9 ]+$')
                ]
            ],
            fechaNacimiento: [
                fechaNacimiento,
                [
                    Validators.required,
                    Validators.pattern('^[0-9-/ ]+$')
                ]
            ],
            anteriorContrasena: [
                '',
                [
                    Validators.minLength(6),
                    Validators.pattern('^[A-Za-z0-9@]{6,12}$'),
                ]
            ],
            nuevaContrasena: [
                '',
                [
                    Validators.minLength(6),
                    Validators.pattern('^[A-Za-z0-9@]{6,12}$'),
                ]
            ],
            confirmarNuevaContrasena: [
                '',
                [
                    Validators.minLength(6),
                    Validators.pattern('^[A-Za-z0-9@]{6,12}$'),
                ]
            ],
            documentoIdentidad: [
                (loginInfo) ? loginInfo.documentoIdentidad : '',
                [
                    Validators.pattern('^[A-Za-z0-9-, ]+$')
                ]
            ],
            direccionDomiciliaria: [
                (loginInfo) ? loginInfo.direccionDomiciliaria : '',
                [
                    Validators.pattern(/^[0-9]\d*$/)
                ]
            ],
            nombreContactoTraducido: [
                (perfil) ? perfil.nombreContactoTraducido : '',
                [
                    Validators.pattern('^[A-Za-z0-9 ]+$'),
                    Validators.minLength(1)
                ],
            ],
        })

        return registroForm
    }

    // Inicializar inputs del formulario
    configurarInputsDelFormulario(
        registroForm: FormGroup
    ): Array<InputCompartido> {
        let inputsForm: Array<InputCompartido> = []
        if (registroForm) {
            //0
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO_NOMBRE_CONTACTO
                },
                placeholder: 'Contact Name',
                data: registroForm.controls.nombreContacto,
                contadorCaracteres: {
                    mostrar: false,
                    numeroMaximo: 28,
                    contador: 0
                },
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //1
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Name:',
                data: registroForm.controls.nombre,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //2
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Apellidos:',
                data: registroForm.controls.apellido,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //3
            inputsForm.push({
                tipo: 'email',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'E-mail:',
                data: registroForm.controls.email,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false,
            })
            //4
            inputsForm.push({
                tipo: 'password',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Password:',
                data: registroForm.controls.contrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: true,
                bloquearCopy: true
            })
            //5
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Address:',
                data: registroForm.controls.direccion,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //6
            inputsForm.push({
                tipo: 'number',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Cell Phone:',
                data: registroForm.controls.telefono,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //7
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Date of birth:',
                data: registroForm.controls.fechaNacimiento,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                esInputParaFecha: true,
                bloquearCopy: false
            })
            //8
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Old Password:',
                data: registroForm.controls.anteriorContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: false,
                bloquearCopy: true,
            })
            //9
            inputsForm.push({
                tipo: 'password',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'New Password:',
                data: registroForm.controls.nuevaContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: true,
                bloquearCopy: true,
            })
            //10
            inputsForm.push({
                tipo: 'password',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Confirm New Password:',
                data: registroForm.controls.confirmarNuevaContrasena,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                contrasena: true,
                bloquearCopy: true,
            })
            //11
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Documento Nacional Identidad:',
                data: registroForm.controls.documentoIdentidad,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })
            //12
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Dirección Domiciliaria:',
                data: registroForm.controls.direccionDomiciliaria,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })

            //13
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO_NOMBRE_CONTACTO
                },
                placeholder: 'Contact Name Traducido:',
                data: registroForm.controls.nombreContactoTraducido,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: false
            })

            //14
            inputsForm.push({
                tipo: 'email',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.REGISTRO
                },
                placeholder: 'Confirm your E-mail:',
                data: registroForm.controls.confirmEmail,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: false,
                bloquearCopy: true,
            })
        }
        return inputsForm
    }

    // Devuelve la portada del album segun el tipo del mismo
    obtenerPortadaAlbumSegunTipoDelAlbum(
        perfil: PerfilModel,
        tipoAlbum: CodigosCatalogoTipoAlbum
    ): AlbumModel {
        let portadaPerfil: AlbumModel
        if (perfil && perfil.album) {
            perfil.album.forEach(item => {
                if (item && item.tipo.codigo === tipoAlbum) {
                    portadaPerfil = item
                }
            })
        }
        return portadaPerfil
    }

    // Definir data segun portada
    definirDataItemSegunPortadaAlbum(
        album: AlbumModel
    ): { urlMedia: string, mostrarBoton: boolean, mostrarLoader: boolean } {
        let data = {
            urlMedia: '',
            mostrarBoton: true,
            mostrarLoader: false,
        }
        if (album && album.portada && album.portada.principal && album.portada.principal.url.length > 0) {
            data.urlMedia = album.portada.principal.url
            data.mostrarBoton = false
            data.mostrarLoader = true
        }

        return data
    }

    // Obtener info ubicacion: Pais
    obtenerInformacionDeUbicacion(
        direcciones: Array<DireccionModel>,
        query: boolean = true, // True = pais, false = localidad
    ): ItemSelector {
        let item: ItemSelector = {
            codigo: '',
            nombre: '',
            auxiliar: ''
        }
        // Si existe la direccion
        if (direcciones && direcciones.length > 0) {
            if (query) {
                // Info del pais
                item.codigo = direcciones[0].pais.codigo
                item.nombre = direcciones[0].pais.nombre
            }
        }
        return item
    }

    navegarAlRegistro(
        accionEntidad: AccionEntidad,
        codigoPerfil: CodigosCatalogoTipoPerfil,
        router: Router
    ) {
        let ruta = RutasLocales.REGISTRAR_PERFIL.toString()
        ruta = ruta.replace(':accionEntidad', accionEntidad)
        ruta = ruta.replace(':codigoPerfil', codigoPerfil)
        router.navigateByUrl(ruta)
    }

    obtenerParametrosDelAppBarSegunAccionEntidad(
        accionEntidad: AccionEntidad,
        ubicacion: UbicacionDelComponente,
        tipoPerfil: CatalogoTipoPerfilModel
    ): {
        mostrarSearchBar: boolean,
        mostrarTextoHome: boolean,
        mostrarIconoBack: boolean,
        mostrarTextoBack: boolean,
        llaveSubtitulo: string,
        mostrarNombrePerfil: boolean,
        llaveTextoNombrePerfil: string
    } {
        let appbar = {
            mostrarSearchBar: false,
            mostrarTextoHome: false,
            mostrarIconoBack: true,
            mostrarTextoBack: false,
            llaveSubtitulo: '',
            mostrarNombrePerfil: false,
            llaveTextoNombrePerfil: ''
        }

        switch (accionEntidad) {
            case AccionEntidad.REGISTRO:
                appbar.mostrarTextoHome = false
                appbar.llaveSubtitulo = this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
                appbar.mostrarNombrePerfil = false
                break
            case AccionEntidad.CREAR:
                appbar.mostrarSearchBar = false
                appbar.mostrarTextoHome = true
                appbar.mostrarTextoBack = true
                appbar.llaveSubtitulo = 'm2v13texto3'
                appbar.mostrarNombrePerfil = true
                appbar.llaveTextoNombrePerfil = this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
                break
            case AccionEntidad.ACTUALIZAR:
                appbar.mostrarSearchBar = false
                appbar.mostrarTextoHome = true
                appbar.mostrarTextoBack = true
                appbar.llaveSubtitulo = 'm2v13texto3'
                appbar.mostrarNombrePerfil = true
                appbar.llaveTextoNombrePerfil = this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
                break
            default: break
        }

        return appbar
    }

    // Obtener pais seleccionado en caso de existir o no
    obtenerPaisSeleccionadoEnElPerfil(
        perfil: PerfilModel
    ): ItemSelector {
        let item: ItemSelector = null
        if (perfil && perfil.direcciones && perfil.direcciones.length > 0) {
            item = {
                codigo: perfil.direcciones[0].localidad.pais.codigo,
                nombre: perfil.direcciones[0].localidad.pais.nombre,
            }
        }

        return item
    }

    validarPerfilParaDestruir(
        accionEntidad: AccionEntidad,
        codigoPerfil: string,
        tipoPerfil: CatalogoTipoPerfilModel,
        perfil: PerfilModel
    ) {
        if (perfil && tipoPerfil) {
            switch (accionEntidad) {
                case AccionEntidad.REGISTRO:
                    this.cuentaNegocio.validarEstadoPerfilParaDestruir(codigoPerfil, perfil.estado.codigo as CodigosCatalogosEstadoPerfiles)
                    break
                case AccionEntidad.CREAR:
                    this.perfilNegocio.removerPerfilActivoDelSessionStorage()
                    break
                case AccionEntidad.ACTUALIZAR:
                    this.perfilNegocio.removerPerfilActivoDelSessionStorage()
                    break
                default: break;
            }
        }
    }

    validarAlbumSegunAccionEntidad(
        ubicacion: UbicacionDelComponente,
        tipoAlbum: CodigosCatalogoTipoAlbum,
        perfil: PerfilModel,
        accionEntidad: AccionEntidad
    ) {
        switch (accionEntidad) {
            case AccionEntidad.REGISTRO:
                this.albumNegocio.validarAlbumSegunTipoEnSessionStorage(tipoAlbum, perfil)
                break
            case AccionEntidad.CREAR:
                this.albumNegocio.validarAlbumEnPerfilActivo(ubicacion, tipoAlbum, perfil)
                break
            case AccionEntidad.ACTUALIZAR:
                this.albumNegocio.validarAlbumEnPerfilActivo(ubicacion, tipoAlbum, perfil)
                break
            default: break;
        }
    }

}
