import {Location} from '@angular/common';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Subject, Subscription} from 'rxjs';
import {PerfilNegocio} from 'dominio/logica-negocio/perfil.negocio';
import {MetodosSessionStorageService} from '@core/util/metodos-session-storage.service';
import {EstadoEnrutador, InformacionAEnrutar, ItemParam, UbicacionDelComponente} from 'presentacion/enrutador/enrutador.component';
import {RutasLocales} from 'src/app/rutas-locales.enum';

@Injectable({providedIn: 'root'})
export class EnrutadorService {

  public cambiarEstadoEnrutadorNavegacion$: Subject<EstadoEnrutador>;
  public clicEnManoDelEnrutador$: Subject<UbicacionDelComponente>;
  public clicEnBotonBack$: Subject<UbicacionDelComponente>;
  public clicEnBotonRefresh$: Subject<UbicacionDelComponente>;
  public reiniciarTodoTercio$: Subject<UbicacionDelComponente>;

  public susCripcionEstadoEnrutacion: Subscription;
  public susCripcionClicEnMano: Subscription;
  public susCripcionEnBotonBack: Subscription;
  public susCripcionEnBotonRefresh: Subscription;
  public informacionAEnrutar: InformacionAEnrutar;
  public susCripcionReiniciarTercio: Subscription;

  constructor(
    private router: Router,
    private _location: Location,
    private perfilNegocio: PerfilNegocio,
    private metodosSessionStorageService: MetodosSessionStorageService
  ) {
    this.cambiarEstadoEnrutadorNavegacion$ = new Subject();
    this.clicEnManoDelEnrutador$ = new Subject();
    this.clicEnBotonBack$ = new Subject();
    this.clicEnBotonRefresh$ = new Subject();
    this.reiniciarTodoTercio$ = new Subject();

  }

  configurarInformacionAEnrutar(
    componente: any,
    params: Array<ItemParam> = []
  ) {
    this.informacionAEnrutar = {
      componente: componente,
      params: params
    };
  }

  navegar(
    info: InformacionAEnrutar,
    estado: EstadoEnrutador
  ) {
    this.informacionAEnrutar = info;
    this.cambiarEstadoEnrutadorNavegacion$.next(estado);
  }

  navegarConPosicionFija(
    info: InformacionAEnrutar,
    ubicacion: UbicacionDelComponente
  ) {
    this.informacionAEnrutar = info;
    this.clicEnManoDelEnrutador$.next(ubicacion);
  }

  navegarAlHome() {
    const perfil = this.perfilNegocio.obtenerPerfilSeleccionado();
    const url = this.router.url;
    this.metodosSessionStorageService.eliminarSessionStorage();

    if (!perfil && url === '/' + RutasLocales.ENRUTADOR) {
      this._location.replaceState('');
      this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      return;
    }
    location.reload();
  }

  navegarAlBack(posicion: UbicacionDelComponente) {
    this.clicEnBotonBack$.next(posicion);
  }

  refrescarTercio(posicion: UbicacionDelComponente) {
    this.clicEnBotonRefresh$.next(posicion);
  }

  reiniciarTercio(posicion: UbicacionDelComponente) {
    this.reiniciarTodoTercio$.next(posicion);
  }

  desconectarDeEscuchas() {
    if (this.susCripcionClicEnMano) {
      this.susCripcionClicEnMano.unsubscribe();
    }

    if (this.susCripcionEnBotonBack) {
      this.susCripcionEnBotonBack.unsubscribe();
    }

    if (this.susCripcionEstadoEnrutacion) {
      this.susCripcionEstadoEnrutacion.unsubscribe();
    }
  }
}
