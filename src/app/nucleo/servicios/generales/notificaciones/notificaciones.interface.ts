import { AngularFireAction } from '@angular/fire/database'
import { DataSnapshot } from '@angular/fire/database/interfaces'
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs'
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum'
import { CodigosCatalogoAccionNotificacion, CodigosCatalogoEstadoNotificacion, CodigosCatalogoTipoNotificacion } from '../../remotos/codigos-catalogos/codigos-catalogo-notificaciones.enum'

export interface Notificaciones {
  // Data General
  dataNotificacion?: DataNotificaciones
  sesionIniciada?: boolean
  usuarioFirebase?: firebase.default.User
  firebaseUid?: string
  notificaciones?: Array<NotificacionFirebaseModel>
  // Data Notificaciones
  notificaciones$: Observable<AngularFireAction<DataSnapshot>[]>
  obtenerNotificaciones$: BehaviorSubject<DataNotificaciones | null>
  subscripcionNotificaciones$: Subscription
  unsubscribe$: Subject<void>
  unsubscribeAction$: Observable<void>

  configurarQuerysNotificaciones(): void
  configurarAccionUnsubscribe(): void
  desconectarDeEscuchaNotificaciones(): void
}

export interface DataNotificaciones {
  nivel: CodigosCatalogoEntidad,
  idPropietario: string, // idPerfil o idUsuario
  codigoEntidad: CodigosCatalogoEntidad,
  leido?: boolean,
  limite?: number,
}

export interface NotificacionFirebaseModel {
  id?: string,
  accion?: CodigosCatalogoAccionNotificacion,
  codEntidad?: CodigosCatalogoEntidad,
  data?: any,
  estado?: CodigosCatalogoEstadoNotificacion,
  fechaCreacion?: number,
  idEntidad?: string,
  leido?: boolean,
  dispositivos?: Array<DispositivoNotificacionModel>,
  query?: string,
  tipo?: CodigosCatalogoTipoNotificacion
}

export interface DispositivoNotificacionModel {
  idDispositivo: string,
  leido: boolean
}

export interface NotificacionPorCodigoEntidad {
  codigoEntidad: CodigosCatalogoEntidad,
  notificaciones: Array<NotificacionFirebaseModel>
}

export interface ProyectoNotificacion {
  perfil?: {
    _id?: string
  },
  proyecto?: {
    _id?: string
  },
  id?: string
}
