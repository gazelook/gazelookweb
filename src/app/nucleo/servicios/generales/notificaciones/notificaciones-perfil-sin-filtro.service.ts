import { Injectable } from '@angular/core';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { BehaviorSubject, Observable, Subject, Subscription, throwError } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { DataNotificaciones, Notificaciones, NotificacionFirebaseModel, NotificacionPorCodigoEntidad, ProyectoNotificacion } from './notificaciones.interface';
@Injectable({ providedIn: 'root' })

export class NotificacionesDePerfilSinFiltro implements Notificaciones {
  public notificaciones$: Observable<AngularFireAction<DataSnapshot>[]>
  public obtenerNotificaciones$: BehaviorSubject<DataNotificaciones | null>
  public subscripcionNotificaciones$: Subscription
  public unsubscribe$: Subject<void>
  public unsubscribeAction$: Observable<void>

  constructor(
    private db: AngularFireDatabase
  ) {
    this.configurarQuerysNotificaciones()
    this.configurarAccionUnsubscribe()
  }

  configurarQuerysNotificaciones() {
    this.obtenerNotificaciones$ = new BehaviorSubject(null)
    this.notificaciones$ = this.obtenerNotificaciones$.pipe(
      switchMap(query => {

        return (!query || query === null) ?
          [] :
          this.db.list(
            'notificaciones/' + query.nivel + '/' + query.idPropietario,
            ref => ref.orderByChild('leido').equalTo(query.leido)
          ).snapshotChanges(['child_added']).pipe(
            takeUntil(this.unsubscribeAction$)
          )
      }
      ),
      catchError(error => throwError(error))
    )
  }

  configurarAccionUnsubscribe() {
    this.unsubscribe$ = new Subject()
    this.unsubscribeAction$ = this.unsubscribe$.asObservable()
  }

  desconectarDeEscuchaNotificaciones() {
    if (this.subscripcionNotificaciones$) {

      this.unsubscribe$.next()
      this.subscripcionNotificaciones$.unsubscribe()
    }

    this.subscripcionNotificaciones$ = undefined
  }

  filtrarNotificacionesPorCodigoEntidad(
    notificaciones: Array<NotificacionFirebaseModel>
  ): Array<NotificacionPorCodigoEntidad> {

    if (!notificaciones) {
      return []
    }

    const data: Array<NotificacionPorCodigoEntidad> = []

    notificaciones.forEach(e => {
      const index = data.findIndex(a => a.codigoEntidad === e.codEntidad)

      if (index < 0) {
        data.push({
          codigoEntidad: e.codEntidad,
          notificaciones: [e]
        })
      } else {
        const indexDos = data[index].notificaciones.findIndex(a => a.id === e.id)

        if (indexDos < 0) {
          data[index].notificaciones.push(e)
          // Ordenar por fecha, forma descendente
          const aux = data[index].notificaciones.sort((a, b) => a.fechaCreacion = b.fechaCreacion)
          data[index].notificaciones = aux
        }
      }
    })

    return data
  }

  convertirNotificacionesEnArrayDeAsociaciones(
    notificaciones: Array<NotificacionFirebaseModel>
  ): Array<AsociacionModel> {

    const asociaciones: Array<AsociacionModel> = []

    notificaciones.forEach(e => {
      asociaciones.push(e.data as AsociacionModel)
    })

    return asociaciones
  }

  convertirNotificacionesEnArrayDeProyectoNotificacion(
    notificaciones: Array<NotificacionFirebaseModel>
  ): Array<ProyectoNotificacion> {

    const data: Array<ProyectoNotificacion> = []

    notificaciones.forEach(e => {
      data.push({
        id: e.id,
        ...e.data as Object
      })
    })

    return data
  }

  async actualizarEstadoNotificaciones(
    notificaciones: Array<NotificacionFirebaseModel>,
    data: DataNotificaciones
  ) {
    try {
      const querys = {}

      notificaciones.forEach(a => {
        querys[a.id] = null
      })

      if (Object.keys(querys).length < 0) {
        return
      }

      await this.db.database.ref('notificaciones/' + data.nivel + '/' + data.idPropietario).update(querys)
    } catch (error) {
    }
  }

  async actualizarEstadoNotificacionesProyecto(
    notificaciones: Array<ProyectoNotificacion>,
    data: DataNotificaciones,
    idProyecto: string
  ) {
    try {
      const querys = {}

      notificaciones.forEach(a => {
        if (a.proyecto._id === idProyecto) {
          querys[a.id] = null
        }
      })

      if (Object.keys(querys).length < 0) {
        return
      }
      await this.db.database.ref('notificaciones/' + data.nivel + '/' + data.idPropietario).update(querys)
    } catch (error) {
      throw new Error("Prueba 1")
    }
  }
}
