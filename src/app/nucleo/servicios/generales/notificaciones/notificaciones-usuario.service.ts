import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject, Subscription, throwError } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { PerfilEntity, PerfilEntityMapperService } from 'dominio/entidades/perfil.entity';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogosEstadoPerfiles } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-perfiles.enun';
import { CodigosCatalogoAccionNotificacion, CodigosCatalogoTipoNotificacion } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-notificaciones.enum';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { DataNotificaciones, Notificaciones, NotificacionFirebaseModel } from '@core/servicios/generales/notificaciones/notificaciones.interface';

@Injectable({ providedIn: 'root' })
export class NotificacionesDeUsuario implements Notificaciones {

  public notificaciones$: Observable<AngularFireAction<DataSnapshot>[]>
  public obtenerNotificaciones$: BehaviorSubject<DataNotificaciones | null>
  public subscripcionNotificaciones$: Subscription
  public unsubscribe$: Subject<void>
  public unsubscribeAction$: Observable<void>
  public dataNotificacion: DataNotificaciones
  public sesionIniciada: boolean
  public usuarioFirebase: firebase.default.User
  public firebaseUid: string
  public notificaciones: Array<NotificacionFirebaseModel>

  constructor(
    private db: AngularFireDatabase,
    private auth: AngularFireAuth,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private perfilEntityMapperService: PerfilEntityMapperService,
    private router: Router,
    private _location: Location,
    private variablesGlobales: VariablesGlobales
  ) {
    this.sesionIniciada = false
    this.firebaseUid = ''
    this.notificaciones = []

    this.configurarQuerysNotificaciones()
    this.configurarAccionUnsubscribe()
  }


  configurarQuerysNotificaciones() {
    this.obtenerNotificaciones$ = new BehaviorSubject(null)
    this.notificaciones$ = this.obtenerNotificaciones$.pipe(
      switchMap(query => {
        return (!query || query === null) ?
          [] :
          this.db.list(
            'notificaciones/' + query.nivel + '/' + query.idPropietario,
            ref => ref.orderByChild('leido').equalTo(false)
          ).snapshotChanges(['child_added', 'child_changed']).pipe(
            takeUntil(this.unsubscribeAction$)
          )
      }
      ),
      catchError(error => throwError(error))
    )
  }

  configurarAccionUnsubscribe() {
    this.unsubscribe$ = new Subject()
    this.unsubscribeAction$ = this.unsubscribe$.asObservable()
  }

  async validarEstadoDeLaSesion(
    mantenerConexion: boolean = true
  ) {
    this.auth.user.subscribe(async user => {
      this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
      this.firebaseUid = this.cuentaNegocio.obtenerFirebaseUIDEnLocalStorage()
      this.usuarioFirebase = user

      if (
        !this.sesionIniciada &&
        (
          !this.usuarioFirebase ||
          this.usuarioFirebase === null
        )
      ) {
        this.desconectarDeEscuchaNotificaciones()
        return
      }

      if (
        this.sesionIniciada &&
        !this.usuarioFirebase
      ) {
        const dataAuth = await this.auth.signInAnonymously()
        this.usuarioFirebase = dataAuth.user
        this.firebaseUid = dataAuth.user.uid
        this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(this.firebaseUid)
      }

      if (this.sesionIniciada && this.usuarioFirebase) {
        this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(this.usuarioFirebase.uid)

        this.desconectarDeEscuchaNotificaciones()
        this.configurarDataNotificaciones()
        this.configurarEscuchaNotificaciones(mantenerConexion)
        this.obtenerNotificaciones()
      }
    }, error => {
      this.desconectarDeEscuchaNotificaciones()
    })
  }

  desconectarDeEscuchaNotificaciones() {
    if (this.subscripcionNotificaciones$) {
      this.unsubscribe$.next()
      this.subscripcionNotificaciones$.unsubscribe()
    }

    this.subscripcionNotificaciones$ = undefined
  }

  configurarDataNotificaciones() {
    const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

    this.dataNotificacion = {
      codigoEntidad: CodigosCatalogoEntidad.USUARIO,
      idPropietario: usuario.id,
      nivel: CodigosCatalogoEntidad.USUARIO,
      leido: false,
      limite: 5
    }
  }

  async obtenerNotificaciones() {
    this.obtenerNotificaciones$.next(this.dataNotificacion)
  }

  async configurarEscuchaNotificaciones(
    mantenerConexion: boolean
  ) {
    this.subscripcionNotificaciones$ = this.notificaciones$.subscribe(
      data => {
        data.forEach(item => {
          const notificacion: NotificacionFirebaseModel = {
            id: item.key,
            ...item.payload.val() as Object
          }

          if (!notificacion.leido) {
            const index = this.notificaciones.findIndex(e => e.id === notificacion.id)

            if (index < 0) {
              this.notificaciones.push(notificacion)
            } else {
              this.notificaciones[index] = notificacion
            }
          }
        })

        this.notificaciones = this.notificaciones.sort((a, b) => a.fechaCreacion - b.fechaCreacion)
        this.validarAccionSegunNotificaciones()
        this.validarEstadoLeidoDeLaNotificacion()

        if (!mantenerConexion) {
          this.desconectarDeEscuchaNotificaciones()
        }
      }, error => {

      })
  }

  async validarAccionSegunNotificaciones() {
    try {
      const querys = {}
      const idDispositivo = this.cuentaNegocio.obtenerIdDispositivoDelUsuario()

      this.notificaciones.forEach(notificacion => {
        if (notificacion.codEntidad === CodigosCatalogoEntidad.USUARIO) {
          this.realizarAccionSegunNotificacioDeUsuario(notificacion)
        }
        if (notificacion.dispositivos) {
          const index = notificacion.dispositivos.findIndex(e => e.idDispositivo === idDispositivo)

          if (
            index >= 0 &&
            !notificacion.dispositivos[index].leido
          ) {
            if (notificacion.codEntidad === CodigosCatalogoEntidad.PERFIL) {
              this.realizarAccionSegunNotificacioDePerfil(notificacion)
            }

            const path = notificacion.id + '/dispositivos/' + index + '/leido'
            querys[path] = true
          }
        }
      })

      const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

      if (Object.keys(querys).length > 0) {
        await this.db.database.ref(
          'notificaciones/' +
          CodigosCatalogoEntidad.USUARIO + '/' +
          usuario.id
        ).update(querys)
      }
    } catch (error) {

    }
  }

  async validarEstadoLeidoDeLaNotificacion() {
    try {
      const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
      // Validar si todos los dispositivos han leido la notificacion
      const querysNotificaciones = {}
      this.notificaciones.forEach(notificacion => {
        let modificar = true
        notificacion.dispositivos.forEach(dispositivo => {
          if (!dispositivo.leido) {
            modificar = false
          }
        })

        if (
          modificar &&
          !notificacion.leido
        ) {
          querysNotificaciones[notificacion.id + '/leido'] = true
        }
      })

      if (Object.keys(querysNotificaciones).length > 0) {
        await this.db.database.ref(
          'notificaciones/' +
          CodigosCatalogoEntidad.USUARIO + '/' +
          usuario.id
        ).update(querysNotificaciones)
      }
    } catch (error) { }
  }

  async realizarAccionSegunNotificacioDeUsuario(
    notificacion: NotificacionFirebaseModel
  ){
    
    if (notificacion.accion === CodigosCatalogoAccionNotificacion.NOTIFI_ELI_CUENTA ) {
        this.cuentaNegocio.cerrarSesion()
        this.cuentaNegocio.cerrarSession()
        this.notificaciones = []
        location.reload()
      }
  }

  async realizarAccionSegunNotificacioDePerfil(
    notificacion: NotificacionFirebaseModel
  ) {
    const perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

    switch (notificacion.tipo) {
      case CodigosCatalogoTipoNotificacion.SISTEMA:
        const perfilNotificacion: PerfilModel = this.perfilEntityMapperService.transform(notificacion.data as PerfilEntity)

        const index = usuario.perfiles.findIndex(e => e._id === perfilNotificacion._id)
        if (index >= 0) {
          const perfilLocal: PerfilModel = { ...usuario.perfiles[index] }
          usuario.perfiles[index].estado = perfilNotificacion.estado
          perfilLocal.estado = perfilNotificacion.estado

          if (perfilNotificacion.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ELIMINADO) {
            usuario.perfiles.splice(index, 1)
          }

        
          
          this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)

          if (this.router.url.includes('enrutador-perfil')) {
            this.variablesGlobales.cambiosDeEstadoEnElPerfil$.next(perfilLocal)
          }
        } else {
          // El perfil no esta en el usuario, es nuevo. Verificar estado activo para anadir
          if (perfilNotificacion.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO) {
            this.insertarNuevoPerfilEnElUsuario(perfilNotificacion)
          }
        }

        if (!this.router.url.includes('enrutador-perfil')) {
          this.perfilNegocio.removerPerfilSeleccionado()
          this._location.replaceState('')
          this.router.navigateByUrl('/')
        }
        break
      default: break
    }
  }

  async insertarNuevoPerfilEnElUsuario(perfil: PerfilModel) {
    try {
      const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
      const perfilData = await this.perfilNegocio.obtenerDatosDelPerfil(perfil._id).toPromise()

      if (!perfilData) {
        throw new Error('No hay llegado el perfil');
      }

      const perfilAux: PerfilModel = {
        _id: perfilData._id,
        album: null,
        direcciones: perfilData.direcciones,
        estado: perfilData.estado,
        nombre: perfilData.nombre,
        nombreContacto: perfilData.nombreContacto,
        telefonos: perfilData.telefonos,
        tipoPerfil: perfilData.tipoPerfil
      }

      usuario.perfiles.push(perfilAux)
      this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)

      if (this.router.url.includes('elegir-perfil')) {
        this.variablesGlobales.nuevoPerfilAnadido$.next(perfilAux)
      }
    } catch (error) { }
  }
}
