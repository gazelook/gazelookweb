import { ProyectoService } from './proyecto.service';
import { NoticiaService } from './noticia.service';
import { RegistroService } from './registro.service';
import { NotificacionesDePerfil } from './notificaciones/notificaciones-perfil.service';
import { MetodosParaFotos } from './metodos-para-fotos.service';
import { MonedaPickerService } from './moneda-picker.service';
import { ImagenPantallaCompletaService } from './imagen-pantalla-completa.service';
import { CamaraService } from './camara.service';
import { AlbumService } from './album.service';
import { NotificacionesDeUsuario } from './notificaciones/notificaciones-usuario.service';
import { EnrutadorService } from './enrutador/enrutador.service';
import { VariablesGlobales } from './variables-globales.service';
import { GeneradorId } from './generador-id.service';

export const servicesGenerales: any[] = [
    AlbumService,
    GeneradorId,
    VariablesGlobales,
    EnrutadorService,
    NotificacionesDeUsuario,
    CamaraService,
    ImagenPantallaCompletaService,
    MonedaPickerService,
    MetodosParaFotos,
    NotificacionesDePerfil,
    RegistroService,
    NoticiaService,
    // ComentariosFirebaseService,
    ProyectoService
];

export * from './generador-id.service';
export * from './variables-globales.service';
export * from './notificaciones/notificaciones-usuario.service';
export * from './enrutador/enrutador.service';
export * from './album.service';
export * from './camara.service';
export * from './imagen-pantalla-completa.service';
export * from './moneda-picker.service';
export * from './notificaciones/notificaciones-perfil.service';
export * from './metodos-para-fotos.service';
export * from './registro.service';
export * from './noticia.service';
export * from './proyecto.service';
