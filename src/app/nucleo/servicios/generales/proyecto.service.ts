import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params } from '@angular/router';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { MonedaPickerService } from '@core/servicios/generales/moneda-picker.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { TranslateService } from '@ngx-translate/core';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ResumenDataMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { InputKey, Inputs } from 'presentacion/proyectos/publicar/publicar.component';

@Injectable({ providedIn: 'root' })
export class ProyectoService {

    constructor(
        private formBuilder: FormBuilder,
        private generadorId: GeneradorId,
        private monedaPickerService: MonedaPickerService,
        private albumNegocio: AlbumNegocio,
        private translateService: TranslateService
    ) { }

    inicializarControlesFormularioAccionCrear(proyecto: ProyectoModel): FormGroup {
        // Definir email y contrasena
        const registroForm: FormGroup = this.formBuilder.group({
            tituloCorto: [
                (proyecto && proyecto.tituloCorto) ? proyecto.tituloCorto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z0-9 ]+$'),
                    Validators.minLength(3)
                ],
            ],
            titulo: [
                (proyecto && proyecto.titulo) ? proyecto.titulo : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            autor: [
                (proyecto && proyecto.perfil && proyecto.perfil.nombreContacto) ? proyecto.perfil.nombreContacto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            descripcion: [
                (proyecto && proyecto.descripcion) ? proyecto.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ],
            ubicacion: [
                (proyecto && proyecto.direccion && proyecto.direccion.descripcion) ? proyecto.direccion.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ]
        })
        return registroForm
    }

    inicializarControlesFormularioAccionActualizar(proyecto: ProyectoModel): FormGroup {
        // Definir email y contrasena
        const updateForm: FormGroup = this.formBuilder.group({
            tituloCorto: [
                (proyecto && proyecto.tituloCorto) ? proyecto.tituloCorto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z0-9 ]+$'),
                    Validators.minLength(3)
                ],
            ],
            titulo: [
                (proyecto && proyecto.titulo) ? proyecto.titulo : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            autor: [
                (proyecto && proyecto.perfil && proyecto.perfil.nombreContacto) ? proyecto.perfil.nombreContacto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            descripcion: [
                (proyecto && proyecto.descripcion && proyecto.descripcion) ? proyecto.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ],
            ubicacion: [
                (proyecto && proyecto.direccion && proyecto.direccion.descripcion) ? proyecto.direccion.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ]
        })
        return updateForm
    }

    inicializarControlesFormularioAccionVisitar(proyecto: ProyectoModel): FormGroup {
        // Definir email y contrasena
        const updateForm: FormGroup = this.formBuilder.group({
            tituloCorto: [
                (proyecto && proyecto.tituloCorto) ? proyecto.tituloCorto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z0-9 ]+$'),
                    Validators.minLength(3)
                ],
            ],
            titulo: [
                (proyecto && proyecto.titulo) ? proyecto.titulo : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            autor: [
                (proyecto && proyecto.perfil && proyecto.perfil.nombreContacto) ? proyecto.perfil.nombreContacto : '',
                [
                    Validators.required,
                    Validators.pattern('^[A-Za-z ]+$'),
                    Validators.minLength(3)
                ]
            ],
            descripcion: [
                (proyecto && proyecto.descripcion) ? proyecto.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ],
            ubicacion: [
                (proyecto && proyecto.direccion && proyecto.direccion.descripcion) ? proyecto.direccion.descripcion : '',
                [
                    Validators.pattern('^[A-Za-z0-9-,. ]+$')
                ]
            ]
        })
        return updateForm
    }

    configurarInputsDelFormularioConKey(
        proyectoForm: FormGroup,
        soloLectura: boolean = false
    ): Array<Inputs> {
        let inputsForm: Array<Inputs> = []

        if (proyectoForm) {
            // 0
            inputsForm.push({
                key: InputKey.TITULO_CORTO,
                input: {
                    tipo: 'text',
                    error: false,
                    estilo: {
                        estiloError: EstiloErrorInput.ROJO,
                        estiloInput: EstiloInput.PROYECTO_CENTRADO
                    },
                    ocultarMensajeError: true,
                    placeholder: 'm4v5texto8',
                    data: proyectoForm.controls.tituloCorto,
                    contadorCaracteres: {
                        mostrar: false,
                        numeroMaximo: 25,
                        contador: 0
                    },
                    id: this.generadorId.generarIdConSemilla(),
                    errorPersonalizado: '',
                    soloLectura: soloLectura,
                        bloquearCopy: false
                }
            })
            // 1
            inputsForm.push({
                key: InputKey.TITULO,
                input: {
                    tipo: 'text',
                    error: false,
                    estilo: {
                        estiloError: EstiloErrorInput.ROJO,
                        estiloInput: EstiloInput.PROYECTO
                    },
                    ocultarMensajeError: true,
                    placeholder: 'm4v5texto9',
                    data: proyectoForm.controls.titulo,
                    contadorCaracteres: {
                        mostrar: false,
                        numeroMaximo: 40,
                        contador: 0
                    },
                    id: this.generadorId.generarIdConSemilla(),
                    errorPersonalizado: '',
                    soloLectura: soloLectura,
                        bloquearCopy: false
                }
            })
            // 2
            inputsForm.push({
                key: InputKey.UBICACION,
                input: {
                    tipo: 'text',
                    error: false,
                    estilo: {
                        estiloError: EstiloErrorInput.ROJO,
                        estiloInput: EstiloInput.PROYECTO
                    },
                    ocultarMensajeError: true,
                    placeholder: 'm4v6texto5',
                    data: proyectoForm.controls.ubicacion,
                    id: this.generadorId.generarIdConSemilla(),
                    errorPersonalizado: '',
                    soloLectura: soloLectura,
                        bloquearCopy: false
                }
            })
            // 3
            inputsForm.push({
                key: InputKey.AUTOR,
                input: {
                    tipo: 'text',
                    error: false,
                    estilo: {
                        estiloError: EstiloErrorInput.ROJO,
                        estiloInput: EstiloInput.PROYECTO_AUTOR
                    },
                    ocultarMensajeError: true,
                    placeholder: 'm4v5texto12',
                    data: proyectoForm.controls.autor,
                    soloLectura: true,
                        bloquearCopy: false
                }
            })
            // 4
            inputsForm.push({
                key: InputKey.LOCALIDAD,
                input: {
                    tipo: 'text',
                    error: false,
                    estilo: {
                        estiloError: EstiloErrorInput.ROJO,
                        estiloInput: EstiloInput.PROYECTO
                    },
                    ocultarMensajeError: true,
                    placeholder: 'm2v3texto15',
                    data: proyectoForm.controls.ubicacion,
                    id: this.generadorId.generarIdConSemilla(),
                    errorPersonalizado: '',
                    soloLectura: soloLectura,
                        bloquearCopy: false
                }
            })
        }
        return inputsForm
    }

    // Inicializar inputs del formulario
    configurarInputsDelFormulario(
        proyectoForm: FormGroup,
        soloLectura: boolean = false
    ): Array<InputCompartido> {
        let inputsForm: Array<InputCompartido> = []

        if (proyectoForm) {
            // 0
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.PROYECTO_CENTRADO
                },
                ocultarMensajeError: true,
                placeholder: 'm4v5texto8',
                data: proyectoForm.controls.tituloCorto,
                contadorCaracteres: {
                    mostrar: false,
                    numeroMaximo: 25,
                    contador: 0
                },
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: soloLectura,
                    bloquearCopy: false
            })
            // 1
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.PROYECTO
                },
                ocultarMensajeError: true,
                placeholder: 'm4v5texto9',
                data: proyectoForm.controls.titulo,
                contadorCaracteres: {
                    mostrar: false,
                    numeroMaximo: 40,
                    contador: 0
                },
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: soloLectura,
                    bloquearCopy: false
            })
            // 2
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.PROYECTO
                },
                ocultarMensajeError: true,
                placeholder: 'm4v6texto5',
                data: proyectoForm.controls.ubicacion,
                id: this.generadorId.generarIdConSemilla(),
                errorPersonalizado: '',
                soloLectura: soloLectura,
                    bloquearCopy: false
            })
            // 3
            inputsForm.push({
                tipo: 'text',
                error: false,
                estilo: {
                    estiloError: EstiloErrorInput.ROJO,
                    estiloInput: EstiloInput.PROYECTO_AUTOR
                },
                ocultarMensajeError: true,
                placeholder: 'm4v5texto12',
                data: proyectoForm.controls.autor,
                soloLectura: true,
                    bloquearCopy: false
            })
        }
        return inputsForm
    }

    determinarAccionProyectoPorUrl(url: string): AccionEntidad {
        if (url.indexOf('publicar') >= 0) {
            return AccionEntidad.CREAR
        }

        if (url.indexOf('actualizar') >= 0) {
            return AccionEntidad.ACTUALIZAR
        }

        if (url.indexOf('visitar') >= 0) {
            return AccionEntidad.VISITAR
        }
    }

    validarParametrosSegunAccionEntidad(
        params: ProyectoParams,
        urlParams: Params
    ): ProyectoParams {
        params.estado = false
        // Validar parametros segun accion entidad
        if (params.accionEntidad) {
            switch (params.accionEntidad) {
                case AccionEntidad.CREAR:
                    if (urlParams.codigoTipoProyecto && urlParams.codigoTipoProyecto !== ':codigoTipoProyecto') {
                        params.estado = true
                        params.codigoTipoProyecto = urlParams.codigoTipoProyecto
                    }
                    return params
                case AccionEntidad.ACTUALIZAR:
                    if (urlParams.id) {
                        params.estado = true
                        params.id = urlParams.id
                    }
                    return params
                case AccionEntidad.VISITAR:
                    if (urlParams.id) {
                        params.estado = true
                        params.id = urlParams.id
                    }
                    return params
                default: break;
            }
        }
        return params
    }

    validarParametrosSegunAccionEntidadEnrutador(
        params: ProyectoParams
    ): ProyectoParams {

        

        params.estado = false
        // Validar parametros segun accion entidad
        if (params.accionEntidad) {
            if (params.accionEntidad === AccionEntidad.CREAR) {
                params.estado = (params.codigoTipoProyecto) ? true : false
                return params
            }

            if (
                params.accionEntidad === AccionEntidad.ACTUALIZAR ||
                params.accionEntidad === AccionEntidad.VISITAR
            ) {
                params.estado = (params.id) ? true : false
                return params
            }
        }

        return params
    }

    determinarUrlImagenPortada(
        proyecto: ProyectoModel,
        imagenesDefecto: Array<ArchivoModel>,
    ): {
        url: string,
        porDefecto: boolean
    } {
        let data = {
            url: '',
            porDefecto: false
        }
        const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.GENERAL,
            proyecto.adjuntos
        )

        const url = this.definirUrlPortada(album)

        if (!url) {
            const min = 0
            const max = imagenesDefecto.length
            if (imagenesDefecto.length > 0) {
                const pos = Math.floor(Math.random() * ((max - 1) - min + 1) + min)
                data.url = imagenesDefecto[pos].url
                data.porDefecto = true
            }
            return data
        }
        data.url = url
        return data
    }

    obtenerPaisSeleccionadoEnElProyecto(
        proyecto: ProyectoModel
    ): ItemSelector {
        const item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
        if (
            proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
            proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_PAIS
        ) {
            if (proyecto && proyecto.direccion && proyecto.direccion.pais) {
                const { codigo, nombre } = proyecto.direccion.pais
                item.codigo = codigo
                item.nombre = nombre
            }
        }
        return item
    }

    obtenerLocalidadSeleccionadaEnElProyecto(
        proyecto: ProyectoModel
    ): ItemSelector {
        const item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
        if (proyecto && proyecto.direccion && proyecto.direccion.localidad) {
            const { codigo, nombre } = proyecto.direccion.localidad
            item.codigo = codigo
            item.nombre = nombre
        }
        return item
    }

    obtenerLlaveTituloTipoProyecto(
        codigoTipoProyecto: CodigosCatalogoTipoProyecto
    ) {
        switch (codigoTipoProyecto) {
            case CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL:
                return "m4v6texto2"
            case CodigosCatalogoTipoProyecto.PROYECTO_LOCAL:
                return "m4v5texto6" // revisar porq no habia
            case CodigosCatalogoTipoProyecto.PROYECTO_RED:
                return "m4v10texto2"
            case CodigosCatalogoTipoProyecto.PROYECTO_PAIS:
                return "m4v5texto5"
            default:
                return "undefined"
        }
    }

    obtenerTextoDescripcionContador(
        proyecto: ProyectoModel
    ) {
        let texto = '0000/3500'
        if (proyecto && proyecto.descripcion) {
            const numero = proyecto.descripcion.length
            if (numero < 10) {
                return '000' + numero + '/3500'
            }

            if (numero >= 10 && numero < 100) {
                return '00' + numero + '/3500'
            }

            if (numero >= 100 && numero < 1000) {
                return '0' + numero + '/3500'
            }

            return numero + '/3500'
        }
        return '0000/3500'
    }

    validarCamposEnProyecto(
        proyecto: ProyectoModel
    ) {
        let res = true
        // Titulo Corto
        if (!(proyecto.tituloCorto && proyecto.tituloCorto.length > 0)) {
            return false
        }
        // Titulo largo
        if (!(proyecto.titulo && proyecto.titulo.length > 0)) {
            return false
        }
        // Direccion
        // if (
        //     proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL &&
        //     !(proyecto.direccion.descripcion && proyecto.direccion.descripcion.length > 0)
        // ) {
        //     return false
        // }

        // Proyecto pais - Pais
        if (
            proyecto.tipo.codigo !== CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL &&
            proyecto.tipo.codigo !== CodigosCatalogoTipoProyecto.PROYECTO_RED &&
            !(proyecto.direccion.pais && proyecto.direccion.pais.codigo.length > 0)
        ) {
            return false
        }

        // Autor o perfil
        if (!(proyecto.perfil && proyecto.perfil._id && proyecto.perfil._id.length > 0)) {
            return false
        }
        // Valor estimado
        if (!(proyecto.valorEstimado && proyecto.valorEstimado !== null && proyecto.valorEstimado > 0)) {
            return false
        }
        // Valor estimado
        if (!(proyecto.moneda && proyecto.moneda.codigo && proyecto.moneda.codigo.length > 0)) {
            return false
        }
        // Descripcion
        if (!(proyecto.descripcion && proyecto.descripcion.length > 0)) {
            return false
        }
        return true
    }

    obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(
        proyecto: ProyectoModel
    ): ResumenDataMonedaPicker {
        const valorEstimado = (proyecto && proyecto.valorEstimado) ? proyecto.valorEstimado : 0
        const moneda: CatalogoTipoMonedaModel = (proyecto && proyecto.moneda) ? proyecto.moneda : { codigo: '' }
        const data = {
            valorEstimado: this.monedaPickerService.obtenerValorEstimadoDelProyecto(valorEstimado),
            tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda)
        }
        return data
    }

    
    obtenerSubtituloAppBarSegunAccionEntidadParaProyecto(
        params: ProyectoParams
    ): string {
        let subtitulo: string = ''
        switch (params.accionEntidad) {
            case AccionEntidad.CREAR:
                subtitulo = 'm4v5texto1'
                break
            case AccionEntidad.ACTUALIZAR:
                subtitulo = 'm4v6texto1'
                break
            case AccionEntidad.VISITAR:
                subtitulo = 'm4v1texto3'
                break
            default: break;
        }
        return subtitulo
    }

    definirUrlPortada(album: AlbumModel): string {
        try {


            if (album && album.portada && album.portada.principal) {
                return album.portada.principal.url
            }

            if (album && album.portada && album.portada.miniatura) {
                return album.portada.miniatura.url
            }

            return undefined
        } catch (error) {
            return undefined
        }
    }
}
