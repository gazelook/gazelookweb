import { Injectable } from '@angular/core';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
@Injectable({ providedIn: 'root' })
export class ImagenPantallaCompletaService {

  public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta

  constructor() {
    this.configurarPortadaExp()
  }

  configurarPortadaExp() {
    this.confImagenPantallaCompleta = {
      urlMedia: '',
      mostrar: false,
      mostrarLoader: true,
      eventoBotonCerrar: () => {
        this.confImagenPantallaCompleta.mostrar = false
        this.confImagenPantallaCompleta.mostrarLoader = true
        this.confImagenPantallaCompleta.urlMedia = ''
      },
      eventoBotonDescargar: () => {
      }
    }
  }

  configurarImagenPantallaCompleta(
    mostrar: boolean = false,
    mostrarLoader: boolean = true,
    urlMedia: string = ''
  ) {

    this.confImagenPantallaCompleta = {
      urlMedia: urlMedia,
      mostrar: mostrar,
      mostrarLoader: mostrarLoader,
      eventoBotonCerrar: () => {
        this.confImagenPantallaCompleta.mostrar = false
        this.confImagenPantallaCompleta.mostrarLoader = true
        this.confImagenPantallaCompleta.urlMedia = ''
      },
      eventoBotonDescargar: () => {
        // let link = document.createElement('a');
        // link.href = this.confImagenPantallaCompleta.urlMedia;
        // link.dispatchEvent(new MouseEvent('click'));
        let link = document.createElement('a');
				link.href = this.confImagenPantallaCompleta.urlMedia;
				link.target = '_blank'
				link.dispatchEvent(new MouseEvent('click', {
					view: window,
					bubbles: false,
					cancelable: true
				}));
      }
    }
  }
}
