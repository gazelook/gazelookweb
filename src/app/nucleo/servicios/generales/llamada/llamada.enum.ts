export enum CodigosCatalogoEstatusPerfil {
    PERFIL_DESCONECTADO = 'EST_PER_00',
    PERFIL_ACTIVO = 'EST_PER_01',
    PERFIL_OCUPADO = 'EST_PER_02',
}

export enum CodigoCatalogoTipoLlamada {
    NO_DEFINIDO = 'none',
    VIDEO = 'video',
    AUDIO = 'audio',
}

export enum CodigoCatalogoAccionLlamada {
    CONTESTAR = 'contestar',
    RECHAZAR = 'rechazar',
    LLAMAR = 'llamar',
}

export enum CodigosCatalogoEstatusLLamada {
    CONECTANDO = 'ESTATUS_LLAMADA_00',
    LLAMANDO = 'ESTATUS_LLAMADA_01',
    EN_CURSO = 'ESTATUS_LLAMADA_02',
    RECHAZADA = 'ESTATUS_LLAMADA_03',
    CANCELADA = 'ESTATUS_LLAMADA_04',
    TERMINADA = 'ESTATUS_LLAMADA_05',
}

export enum CodigosCatalogoEstadoLlamada {
    ACTIVO = 'ESTADO_LLAMADA_00',
    ELIMINADA = 'ESTADO_LLAMADA_01',
}