// import { Injectable, OnDestroy } from '@angular/core'
// import { AngularFireDatabase } from '@angular/fire/database'
// import { Router } from '@angular/router'
// import { GeneradorId } from '@core/servicios/generales'
// import { CanalActivo, EstadoPerfil, FirebaseQuery, IndicadorAgora, Llamada } from '@core/servicios/generales/llamada/llamada.interface'
// import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model'
// import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model'
// import { PerfilModel } from 'dominio/modelo/entidades/perfil.model'
// import * as Firebase from 'firebase/app'
// import { RutasGazing } from 'presentacion/gazing/rutas-gazing.enum'
// import { BehaviorSubject, Subject, throwError } from 'rxjs'
// import { catchError, switchMap, takeUntil } from 'rxjs/operators'
// import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio'
// import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio'
// import { ChatLlamadaParams } from 'presentacion/gazing/chat-llamada/chat-llamada.component'
// import { RutasLocales } from 'src/app/rutas-locales.enum'
// import { CodigoCatalogoAccionLlamada, CodigoCatalogoTipoLlamada, CodigosCatalogoEstadoLlamada, CodigosCatalogoEstatusLLamada, CodigosCatalogoEstatusPerfil } from '@core/servicios/generales/llamada/llamada.enum'

// @Injectable({ providedIn: 'root' })
// export class LlamadaFirebaseService implements OnDestroy {

// 	public params: ChatLlamadaParams
// 	public llamadas: FirebaseQuery<string>
// 	public llamadaActiva: FirebaseQuery<string>

// 	constructor(
// 		private db: AngularFireDatabase,
// 		private generadorId: GeneradorId,
// 		private router: Router,
// 		private perfilNegocio: PerfilNegocio,
// 		private cuentaNegocio: CuentaNegocio
// 	) {
// 		this.llamadas = {}
// 		this.llamadaActiva = {}
// 		this.configurarAccionUnsubscribe()
// 		this.configurarQueryLlamadas()
// 		this.configurarQueryLlamadaActiva()
// 	}

// 	ngOnDestroy(): void {
// 		this.desconectarDeEscuchaLlamadas()
// 	}

// 	configurarAccionUnsubscribe() {
// 		this.llamadas.unsubscribe$ = new Subject()
// 		this.llamadas.desconectar$ = this.llamadas.unsubscribe$.asObservable()

// 		this.llamadaActiva.unsubscribe$ = new Subject()
// 		this.llamadaActiva.desconectar$ = this.llamadaActiva.unsubscribe$.asObservable()
// 	}

// 	configurarQueryLlamadas() {
// 		this.llamadas.ejecutar$ = new BehaviorSubject(null)
// 		this.llamadas.respuesta$ = this.llamadas.ejecutar$.pipe(
// 			switchMap(query => {
// 				return (!query || query === null) ? 
// 					[] :
// 					this.db.list(
// 						'llamadas',
// 						ref => ref.orderByChild('busqueda').equalTo(query).limitToLast(1)
// 					).snapshotChanges(['child_added']).pipe(
// 						takeUntil(this.llamadas.desconectar$)
// 					)
// 				}
// 			),
// 			catchError(error => throwError(error))
// 		)
// 	}

// 	configurarQueryLlamadaActiva() {
// 		this.llamadaActiva.ejecutar$ = new BehaviorSubject(null)
// 		this.llamadaActiva.respuesta$ = this.llamadaActiva.ejecutar$.pipe(
// 			switchMap(query => {
// 				return (!query || query === null) ? 
// 					[] :
// 					this.db.list(
// 						'llamadas/' + query,
// 					).snapshotChanges(['child_changed']).pipe(
// 						takeUntil(this.llamadaActiva.desconectar$)
// 					)
// 				}
// 			),
// 			catchError(error => throwError(error))
// 		)
// 	}

// 	desconectarDeEscuchaLlamadas() {
// 		if (this.llamadas.subscripcion$) {
// 			this.llamadas.unsubscribe$.next()
// 			this.llamadas.subscripcion$.unsubscribe()
// 		}

// 		if (this.llamadaActiva.subscripcion$) {
// 			this.llamadaActiva.unsubscribe$.next()
// 			this.llamadaActiva.subscripcion$.unsubscribe()
// 		}

// 		this.llamadas.subscripcion$ = undefined
// 		this.llamadaActiva.subscripcion$ = undefined
// 	}

// 	configurarParams(
// 		params: ChatLlamadaParams
// 	) {
// 		this.params = params
// 	}

// 	obtenerIdParaLLamada(): string {
// 		return this.db.database.ref('llamadas').push().key
// 	}

// 	crearObjetoDeLlamada(
// 		asociacion: string,
// 		tipo: CodigoCatalogoTipoLlamada,
// 		idHost: string,
// 		idReceptor: string = '',
// 		busqueda: string = 'none',
// 		estatus: CodigosCatalogoEstatusLLamada = CodigosCatalogoEstatusLLamada.CONECTANDO,
// 		estado: CodigosCatalogoEstadoLlamada = CodigosCatalogoEstadoLlamada.ACTIVO,
// 	): Llamada {
// 		return {
// 			id: this.obtenerIdParaLLamada(),
// 			estatus: estatus,
// 			estado: estado,
// 			tipo: tipo,
// 			host: idHost,
// 			receptor: idReceptor,
// 			busqueda: busqueda,
// 			idCanal: '',
// 			asociacion: asociacion,
// 			canal: -1,
// 		}
// 	}

// 	async obtenerInformacionDeLaLLamada(
// 		idLLamada: string
// 	): Promise<Llamada> {
// 		const data = await this.db.database.ref('llamadas/' + idLLamada).get()

// 		return {
// 			id: data.key,
// 			...data.val()
// 		}
// 	}

// 	actualizarEstatusDeLaLLamada(
// 		llamada: Llamada
// 	) {
// 		if (!llamada || !llamada.estatus) {
// 			return 'undefined'
// 		}

// 		switch (llamada.estatus) {
// 			case CodigosCatalogoEstatusLLamada.CONECTANDO: 
// 				return 'CONECTANDO'
// 			case CodigosCatalogoEstatusLLamada.LLAMANDO:
// 				return (this.params.host) ? 'LLAMANDO' : 'LLAMADA ENTRANTE'
// 			case CodigosCatalogoEstatusLLamada.EN_CURSO: 
// 				return 'EN CURSO'
// 			case CodigosCatalogoEstatusLLamada.CANCELADA: 
// 				return (this.params.host) ? 'LLAMADA CANCELADA' : 'LA LLAMADA FUE CANCELADA'
// 			case CodigosCatalogoEstatusLLamada.RECHAZADA: 
// 				return (this.params.host) ? 'LA LLAMADA FUE RECHAZADA' : 'RECHAZASTE LA LLAMADA'
// 			case CodigosCatalogoEstatusLLamada.TERMINADA: 
// 				return 'LLAMADA TERMINADA'
// 			default:
// 				return 'undefined'
// 		}
// 	}

// 	async configurarIndicadorAgora() {
// 		try {
// 			const agoraKeys: IndicadorAgora = {
// 				channel: 0,
// 				key: 0
// 			}
// 			const data = await this.db.database.ref('agora-keys').get()

// 			if (!data.exists()) {
// 				await this.db.database.ref('indicadores-agora').set(agoraKeys)
// 				return
// 			}
// 		} catch (error) {
// 		}
// 	}

// 	async obtenerCanalesActivos(): Promise<Array<CanalActivo>> {

// 		const data = await this.db.database.ref('canales-activos').get()
// 		const canales: Array<CanalActivo> = []

// 		data.forEach(a => {
// 			canales.push({
// 				idCanal: a.key,
// 				...a.val()
// 			})
// 		})

// 		return canales
// 	}

// 	async actualizarCanalActivo(
// 		canalActivo: CanalActivo,
// 		eliminar: boolean = false
// 	) {
// 		await this.db.database.ref('canales-activos/' + canalActivo.idCanal).set((!eliminar) ? canalActivo : null)
// 	}

// 	crearCanalActivo(
// 		asociacion: string,
// 		canales: Array<CanalActivo>
// 	): CanalActivo {

// 		let noEncontrado = true
// 		const canalActivo: CanalActivo = {
// 			idCanal: this.db.database.ref('canales-activos').push().key,
// 			asociacion: asociacion,
// 			canal: -1
// 		}

// 		if (canales.length === 100000) {
// 			throw new Error('No hay mas canales disponibles para ejecutar la llamada, intenta mas tarde')
// 		}

// 		while(noEncontrado) {
// 			const aux = Math.floor(Math.random() * 100000)
// 			const index = canales.findIndex(e => e.canal === aux)

// 			if (index < 0) {
// 				noEncontrado = false
// 				canalActivo.canal = aux
// 			}
// 		}

// 		return canalActivo
// 	}

// 	cambiarEstadoAudioNotificacion(
// 		mutear: boolean
// 	) {
// 		const elemento = this.obtenerElementoPorId('audio_notificacion')
// 		if (!elemento) {
// 			return
// 		}

// 		elemento.volume = 0.6
// 		elemento.muted = mutear
// 	}

// 	mostrarNotificacion(
// 		idPerfilSeleccionado: string,
// 		asociacion: AsociacionModel
// 	) {
// 		if (this.params.accion !== CodigoCatalogoAccionLlamada.CONTESTAR) {
// 			return
// 		}

// 		const otroPerfil = this.obtenerParticipantesDeAsociacion(idPerfilSeleccionado, asociacion, false).perfil

// 		const notificacion = new Notification(
// 			'Llamada entrante',
// 			{
// 				icon: "https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/botones/mundogazelook-landing.png",
// 				vibrate: [200, 100, 200],
// 				body: otroPerfil.nombreContacto + ' te esta llamando'
// 			}
// 		)
// 	}

// 	obtenerElementoPorId(id: string) {
// 		return document.getElementById(id) as HTMLAudioElement
// 	}

// 	async cambiarEstadoDelPerfil(
// 		perfil: PerfilModel,
// 		codigoEstado: CodigosCatalogoEstatusPerfil
// 	) {
// 		const estado: EstadoPerfil = {
// 			id: perfil._id,
// 			estatus: {
// 				codigo: codigoEstado
// 			},
// 			perfil: this.obtenerPerfilResumido(perfil),
// 			fecha: Firebase.default.database.ServerValue.TIMESTAMP
// 		}


// 		await this.db.database.ref(
// 			'estado-perfil/' + perfil._id
// 		).set(estado)
// 	}

//     async obtenerEstadoDelOtroParticipante(
//         idPerfilSeleccionado: string,
//         asociacion: AsociacionModel
//     ): Promise<EstadoPerfil> {
//         try {
//             const otroParticipante: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion(
// 				idPerfilSeleccionado,
// 				asociacion,
// 				false
// 			)

// 			if (!otroParticipante) {
// 				throw new Error('No se pudo obtener el otro participante');
// 			}

// 			const data = await this.db.database.ref(
// 				'estado-perfil/' + otroParticipante.perfil._id
// 			).get()

// 			if (!data) {
// 				throw new Error('No se pudo obtener el otro participante');
// 			}

// 			if (!data.exists()) {
// 				throw new Error('Error no hay data del estado del participante')
// 			}

// 			return {
// 				id: data.key,
// 				...data.val() as Object
// 			}
//         } catch (error) {
// 			return undefined
//         }
//     }

// 	async registrarLlamada(
// 		llamada: Llamada
// 	) {
// 		try {
// 			llamada.fecha = Firebase.default.database.ServerValue.TIMESTAMP
// 			await this.db.database.ref('llamadas/' + llamada.id).set(llamada)
// 		} catch (error) {
// 		}
// 	}

// 	async actualizarEstadoDeLaLlamada(
// 		llamada: Llamada
// 	): Promise<boolean> {
// 		try {
// 			await this.db.database.ref('llamadas/' + llamada.id).update({
// 				estatus: llamada.estatus,
// 				busqueda: 'none'
// 			})
// 			return true	
// 		} catch (error) {
// 			return false
// 		}
// 	}

// 	obtenerParticipantesDeAsociacion(
// 		idPerfilSeleccionado: string,
// 		asociacion: AsociacionModel,
//         propietario: boolean = true
//     ): ParticipanteAsociacionModel {
//         const index = asociacion.participantes.findIndex(e => (
//             e &&
//             e.perfil &&
//             (
//                 (propietario && e.perfil._id === idPerfilSeleccionado) ||
//                 (!propietario && e.perfil._id !== idPerfilSeleccionado)
//             )
//         ))

//         if (index < 0) {
// 			return undefined
//         }

// 		return asociacion.participantes[index]
//     }

// 	obtenerPerfilResumido(
// 		perfil: PerfilModel
// 	): PerfilModel {
// 		return {
// 			_id: perfil._id,
// 			nombre: perfil.nombre,
// 			nombreContacto: perfil.nombreContacto
// 		}
// 	}

// 	suscribir(
// 		estado: CodigosCatalogoEstatusLLamada
// 	) {
// 		const perfil = this.perfilNegocio.obtenerPerfilSeleccionado()
// 		const haySesion = this.cuentaNegocio.sesionIniciada()

// 		if (!perfil || perfil === null || !haySesion) {
// 			this.desconectarDeEscuchaLlamadas()
// 			return
// 		}

// 		const busqueda = perfil._id + ' -- ' + estado

// 		this.desconectarDeEscuchaLlamadas()
// 		this.suscribirEscuchaLLamadas()
// 		this.cambiarEstadoDelPerfil(
// 			perfil,
// 			CodigosCatalogoEstatusPerfil.PERFIL_ACTIVO
// 		)
// 		this.llamadas.ejecutar$.next(busqueda)
// 	}

// 	suscribirEscuchaLLamadas() {
// 		this.llamadas.subscripcion$ = this.llamadas.respuesta$.subscribe(
// 			data => {

// 				if (!data || data.length === 0) {
// 					return
// 				}
				
// 				let listaLlamada: Llamada[] = []
// 				data.forEach(e => {
// 					listaLlamada.push({
// 						id: e.key,
// 						...e.payload.val()
// 					})
// 				})

// 				listaLlamada = listaLlamada.sort((a, b) =>  a.fecha - b.fecha)
// 				const llamada = listaLlamada[listaLlamada.length - 1]

// 				if (!llamada) {
// 					return
// 				}

// 				if (this.router.url.includes(RutasGazing.CONFERENCIA.toString())) {

// 					return
// 				}

// 				this.configurarParams({
// 					estado: false,
// 					id: llamada.id,
// 					tipo: llamada.tipo,
// 					accion: CodigoCatalogoAccionLlamada.CONTESTAR,
// 					host: false
// 				})

// 				this.router.navigateByUrl(RutasLocales.GAZING + '/' + RutasGazing.CONFERENCIA)
// 			},
// 			error => {

// 			}
// 		)
// 	}
// }
