import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { IntercambioParams } from 'dominio/modelo/parametros/intercambio-params.interface';
import { InputKey, Inputs } from 'presentacion/compra-intercambio/intercambio/publicar-intercambio/publicar-intercambio.component';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
@Injectable({ providedIn: 'root' })
export class IntercambioService {

  constructor(
    private formBuilder: FormBuilder,
    private generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
    private translateService: TranslateService
  ) { }

  validarParametrosSegunAccionEntidad(
    params: IntercambioParams,
    urlParams: Params
  ): IntercambioParams {
    params.estado = false
    // Validar parametros segun accion entidad
    if (params.accionEntidad) {
      switch (params.accionEntidad) {
        case AccionEntidad.CREAR:
          if (urlParams.codigoTipoIntercambio && urlParams.codigoTipoIntercambio !== ':codigoTipoIntercambio') {
            params.estado = true
            params.codigoTipoIntercambio = urlParams.codigoTipoIntercambio
          }
          return params
        case AccionEntidad.ACTUALIZAR:
          if (urlParams.id) {
            params.estado = true
            params.id = urlParams.id
          }
          return params
        case AccionEntidad.VISITAR:
          if (urlParams.id) {
            params.estado = true
            params.id = urlParams.id
          }
          return params
        default: break;
      }
    }
    return params
  }

  determinarAccionProyectoPorUrl(url: string): AccionEntidad {
    if (url.indexOf('publicar') >= 0) {
      return AccionEntidad.CREAR
    }

    if (url.indexOf('actualizar') >= 0) {
      return AccionEntidad.ACTUALIZAR
    }

    if (url.indexOf('visitar') >= 0) {
      return AccionEntidad.VISITAR
    }
  }

  obtenerSubtituloAppBarSegunAccionEntidadParaIntercambio(
    params: IntercambioParams
  ): string {
    let subtitulo: string = ''
    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:
        subtitulo = 'm4v5texto1'
        break
      case AccionEntidad.ACTUALIZAR:
        subtitulo = 'm4v6texto1'
        break
      case AccionEntidad.VISITAR:
        subtitulo = 'm4v1texto3'
        break
      default: break;
    }
    return subtitulo
  }

  validarParametrosSegunAccionEntidadEnrutador(
    params: IntercambioParams
  ): IntercambioParams {
    params.estado = false
    // Validar parametros segun accion entidad

    if (params.accionEntidad) {
      if (params.accionEntidad === AccionEntidad.CREAR) {
        params.estado = (params.codigoTipoIntercambio) ? true : false
        return params
      }

      if (
        params.accionEntidad === AccionEntidad.ACTUALIZAR ||
        params.accionEntidad === AccionEntidad.VISITAR
      ) {
        params.estado = (params.id) ? true : false
        return params
      }
    }
    return params
  }

  determinarUrlImagenPortada(
    intercambio: IntercambioModel,
    imagenesDefecto: Array<ArchivoModel>,
  ): {
    url: string,
    porDefecto: boolean
  } {
    let data = {
      url: '',
      porDefecto: false
    }
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      intercambio.adjuntos
    )
    const url = this.definirUrlPortada(album)

    if (!url) {
      const min = 0
      const max = imagenesDefecto.length
      if (imagenesDefecto.length > 0) {
        const pos = Math.floor(Math.random() * ((max - 1) - min + 1) + min)
        data.url = imagenesDefecto[pos].url
        data.porDefecto = true
      }
      return data
    }

    data.url = url
    return data
  }

  definirUrlPortada(album: AlbumModel): string {
    try {

      if (album && album.portada && album.portada.principal) {
        return album.portada.principal.url
      }

      if (album && album.portada && album.portada.miniatura) {
        return album.portada.miniatura.url
      }



      return undefined
    } catch (error) {
      return undefined
    }
  }

  obtenerTextoDescripcionContador(
    intercambio: IntercambioModel
  ) {
    let texto = '0000/1000'
    if (intercambio && intercambio.descripcion) {
      const numero = intercambio.descripcion.length
      if (numero < 10) {
        return '000' + numero + '/1000'
      }

      if (numero >= 10 && numero < 100) {
        return '00' + numero + '/1000'
      }

      if (numero >= 100 && numero < 1000) {
        return '0' + numero + '/1000'
      }

      return numero + '/1000'
    }
    return '0000/1000'
  }

  inicializarControlesFormularioAccionCrear(intercambio: IntercambioModel): FormGroup {
    // Definir email y contrasena
    const registroForm: FormGroup = this.formBuilder.group({
      tituloCorto: [
        (intercambio && intercambio.tituloCorto) ? intercambio.tituloCorto : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z0-9 ]+$'),
          Validators.minLength(3)
        ],
      ],
      titulo: [
        (intercambio && intercambio.titulo) ? intercambio.titulo : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],
      descripcion: [
        (intercambio && intercambio.descripcion) ? intercambio.descripcion : '',
        [
          Validators.required,
          Validators.email

        ]
      ],
      email: [
        (intercambio && intercambio.email) ? intercambio.email : '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      ubicacion: [
        (intercambio && intercambio.direccion && intercambio.direccion.descripcion) ? intercambio.direccion.descripcion : '',
        [
          Validators.pattern('^[A-Za-z0-9-,. ]+$')
        ]
      ]
    })
    return registroForm
  }

  inicializarControlesFormularioAccionActualizar(intercambio: IntercambioModel): FormGroup {
    // Definir email y contrasena
    const updateForm: FormGroup = this.formBuilder.group({
      tituloCorto: [
        (intercambio && intercambio.tituloCorto) ? intercambio.tituloCorto : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z0-9 ]+$'),
          Validators.minLength(3)
        ],
      ],
      titulo: [
        (intercambio && intercambio.titulo) ? intercambio.titulo : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],

      descripcion: [
        (intercambio && intercambio.descripcion && intercambio.descripcion) ? intercambio.descripcion : '',
        [
          Validators.pattern('^[A-Za-z0-9-,. ]+$')
        ]
      ],
      email: [
        (intercambio && intercambio.email) ? intercambio.email : '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      ubicacion: [
        (intercambio && intercambio.direccion && intercambio.direccion.descripcion) ? intercambio.direccion.descripcion : '',
        [
          Validators.pattern('^[A-Za-z0-9-,. ]+$')
        ]
      ]
    })
    return updateForm
  }

  configurarInputsDelFormularioConKey(
    intercambioForm: FormGroup,
    soloLectura: boolean = false
  ): Array<Inputs> {
    let inputsForm: Array<Inputs> = []

    if (intercambioForm) {

      // 0
      inputsForm.push({
        key: InputKey.TITULO_CORTO,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO_CENTRADO
          },
          ocultarMensajeError: true,
          placeholder: 'm6v8texto6',
          data: intercambioForm.controls.tituloCorto,
          contadorCaracteres: {
            mostrar: false,
            numeroMaximo: 25,
            contador: 0
          },
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 1
      inputsForm.push({
        key: InputKey.TITULO,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm6v8texto8',
          data: intercambioForm.controls.titulo,
          contadorCaracteres: {
            mostrar: false,
            numeroMaximo: 40,
            contador: 0
          },
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 2
      inputsForm.push({
        key: InputKey.UBICACION,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm4v6texto11',
          data: intercambioForm.controls.ubicacion,
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })

      // 4
      inputsForm.push({
        key: InputKey.LOCALIDAD,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm2v3texto12',
          data: intercambioForm.controls.ubicacion,
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 5
      inputsForm.push({
        key: InputKey.EMAIL,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm6v8texto10',
          data: intercambioForm.controls.email,
          contadorCaracteres: {
            mostrar: false,
            numeroMaximo: 40,
            contador: 0
          },
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
    }
    return inputsForm
  }

  obtenerPaisSeleccionadoEnElIntercambio(
    intercambio: IntercambioModel
  ): ItemSelector {
    const item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
    if (intercambio && intercambio.direccion && intercambio.direccion.pais) {
      const { codigo, nombre } = intercambio.direccion.pais
      item.codigo = codigo
      item.nombre = nombre
    }
    return item
  }

  obtenerLocalidadSeleccionadoEnElIntercambio(
    intercambio: IntercambioModel
  ): ItemSelector {
    const item: ItemSelector = { codigo: '', nombre: '', auxiliar: '' }
    if (intercambio && intercambio.direccion && intercambio.direccion.localidad) {
      const { codigo, nombre } = intercambio.direccion.localidad
      item.codigo = codigo
      item.nombre = nombre
    }
    return item
  }

  validarCamposEnIntercambio(
    intercambio: IntercambioModel
  ) {

    let res = true
    // Titulo Corto
    if (!(intercambio.tituloCorto && intercambio.tituloCorto.length > 0)) {
      return false
    }
    // Titulo largo
    if (!(intercambio.titulo && intercambio.titulo.length > 0)) {
      return false
    }

    // Proyecto local - Localidad
    if (

      !(intercambio.direccion.localidad && intercambio.direccion.localidad.codigo.length > 0)
    ) {
      return false
    }

    if (

      !(intercambio.direccion.pais && intercambio.direccion.pais.codigo.length > 0)
    ) {
      return false
    }

    // Autor o perfil
    if (!(intercambio.perfil && intercambio.perfil._id && intercambio.perfil._id.length > 0)) {
      return false
    }

    // Autor o perfil
    if (!(intercambio.email && intercambio.email.length > 0)) {
      return false
    }
    return true
  }
}
