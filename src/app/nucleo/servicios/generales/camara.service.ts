import { Injectable } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { WebcamImage } from 'ngx-webcam';
import { Subject, Subscription } from 'rxjs';
import { ConfiguracionCamara, ConfiguracionCropper } from '@shared/diseno/modelos/foto-editor.interface';

@Injectable({ providedIn: 'root' })
export class CamaraService {

  public confCamara: ConfiguracionCamara
  public escuchaFotoCapturada$: Subject<WebcamImage>
  public suscripcionFotoCapturada$: Subscription
  public confCropper: ConfiguracionCropper
  public escuchaFotoCortada$: Subject<ImageCroppedEvent>
  public suscripcionFotoCortada$: Subscription

  constructor() {
    this.escuchaFotoCapturada$ = new Subject()
    this.escuchaFotoCortada$ = new Subject()
    this.configurarCamara()
    this.configurarCropper()
  }

  reiniciarServicio() {
    this.reiniciarCamara()
    this.reiniciarCropper()
  }

  configurarCamara(idConversacion?: string) {
    this.confCamara = {
      mostrarModal: false,
      mostrarLoader: false,
      mensajeError: {
        contenido: '',
        mostrarError: false,
        tamanoCompleto: false
      },
      usarCroopper: false,
      fotoCapturada: (imagen: WebcamImage) => this.fotoCapturada(imagen, idConversacion)
    }
  }

  configurarCropper(
    mostrar: boolean = false,
    key: string = '',
    file: any = undefined
  ) {
    this.confCropper = {
      mostrarModal: mostrar,
      mostrarLoader: true,
      mostrarCaja: false,
      mensajeError: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false
      },
      imagenCortada: (event: ImageCroppedEvent) => this.imagenCortada(event)
    }

    if (key && key.length > 0) {
      this.confCropper[key] = file
    }
  }

  cambiarEstadoCamara(
    estado: boolean,
    usarCroopper: boolean = false
  ) {
    this.confCamara.mostrarModal = estado
    this.confCamara.usarCroopper = usarCroopper
  }

  reiniciarCamara(idConversacion?: string) {
    this.configurarCamara(idConversacion)
  }

  fotoCapturada(imagen: WebcamImage, idConversacion?: string) {

    try {
      if (this.confCamara.usarCroopper) {
        this.configurarCropper(true, 'imageURL', imagen.imageAsDataUrl)
        this.reiniciarCamara()
        return
      }
      
      if (idConversacion) {
        this.escuchaFotoCapturada$.next(imagen)
      } else {
        this.escuchaFotoCapturada$.next(imagen)
      }
     
      this.reiniciarCamara()
    } catch (error) {
      this.confCamara.mostrarLoader = false
      this.confCamara.mensajeError.contenido = 'text37'
      this.confCamara.mensajeError.mostrarError = true
    }
  }

  reiniciarCropper() {
    this.configurarCropper()
  }

  imagenCortada(event: ImageCroppedEvent) {
    this.escuchaFotoCortada$.next(event)
    this.reiniciarCropper()
  }

  deSuscribir() {
    if (this.suscripcionFotoCapturada$) {
      this.suscripcionFotoCapturada$.unsubscribe()
    }

    if (this.suscripcionFotoCortada$) {
      this.suscripcionFotoCortada$.unsubscribe()
    }
    this.suscripcionFotoCapturada$ = undefined
    this.suscripcionFotoCortada$ = undefined
  }

}
