
import { Injectable } from '@angular/core'
import { Observable, throwError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'
import { LocalStorage } from '@core/servicios/locales/local-storage.service'
import { IdiomaService } from '@core/servicios/remotos/idioma.service'
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity'
@Injectable({
  providedIn: 'root'
})
export class IdiomaRepository {

  constructor(
    private idiomaService: IdiomaService,
    private localStorage: LocalStorage
  ) { }
  guardarIdiomaLocal(idioma: CatalogoIdiomaEntity) {
    this.localStorage.guardarIdiomaLocal(idioma)
  }
  obtenerIdiomaLocal(): CatalogoIdiomaEntity {
    return this.localStorage.obtenerIdiomaLocal()
  }
  guardarIdiomas(idiomas: Array<CatalogoIdiomaEntity>) {
    this.localStorage.guardarIdiomas(idiomas)
  }
  obtenerIdiomas(): Array<CatalogoIdiomaEntity> {
    return this.localStorage.obtenerIdiomas()
  }
  //  obtenerCatalogoIdiomas():Observable<RespuestaRepositorio<any>>{
  obtenerCatalogoIdiomas(): Observable<CatalogoIdiomaEntity[]> {
    return this.idiomaService.obtenerCatalogoIdiomas()
      .pipe(
        map(data => {
          return data.respuesta.datos;
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }
}
