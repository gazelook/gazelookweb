import { Injectable } from '@angular/core';
import { ConversacionEntityMapperService } from 'dominio/entidades/conversacion.entity';
import { MensajeMapperService } from 'dominio/entidades/mensaje.entity';
import { NotificacionEntityMapperService } from 'dominio/entidades/notificacion.entity';

@Injectable({
  providedIn: 'root'
})
export class NotificacionRepository {

  constructor(
    private conversacionEntityMapperService: ConversacionEntityMapperService,
    private mensajeMapperService: MensajeMapperService,
    private notificacionEntityMapperService: NotificacionEntityMapperService
  ) { }
}
