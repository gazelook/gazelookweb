import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { TipoMonedaServiceLocal } from '@core/servicios/locales/moneda.service';
import { TipoMonedaServiceRemoto } from '@core/servicios/remotos/moneda.service';
import { CatalogoTipoMonedaElegibleMapperService, CatalogoTipoMonedaEntityMapperService } from 'dominio/entidades/catalogos/catalogo-tipo-moneda.entity';
import { CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-moneda.enum';

@Injectable({ providedIn: 'root' })
export class TipoMonedaRepository {

  constructor(
    private tipoMonedaServiceLocal: TipoMonedaServiceLocal,
    private tipoMonedaServiceRemoto: TipoMonedaServiceRemoto,
    private catalogoTipoMonedaMapperService: CatalogoTipoMonedaEntityMapperService,
    private catalogoTipoMonedaElegibleMapperService: CatalogoTipoMonedaElegibleMapperService
  ) { }

  guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda: ItemSelector[]) {
    this.tipoMonedaServiceLocal.guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda)
  }

  obtenerCatalogoTipoMonedaDelLocalStorage(): ItemSelector[] {
    return this.tipoMonedaServiceLocal.obtenerCatalogoTipoMonedaDelLocalStorage()
  }

  eliminarCatalogoTipoMonedaDelLocalStorage() {
    this.tipoMonedaServiceLocal.eliminarCatalogoTipoMonedaDelLocalStorage()
  }

  obtenerCatalogoTipoMoneda(): Observable<ItemSelector[]> {
    return this.tipoMonedaServiceRemoto.obtenerCatalogoTipoMoneda()
      .pipe(
        map(data => {
          const test = this.catalogoTipoMonedaMapperService.transform(data.respuesta.datos)

          return this.catalogoTipoMonedaElegibleMapperService.transform(test)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  convertirMontoEntreMonedas(
    monto: number,
    convertirDe: CodigosCatalogoTipoMoneda,
    convertirA: CodigosCatalogoTipoMoneda,
  ): Observable<{ monto: string }> {
    return this.tipoMonedaServiceRemoto.convertirMontoEntreMonedas(monto, convertirDe, convertirA)
      .pipe(
        map(data => {
          return data.respuesta.datos
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  
  convertirMontoEntreMonedasProyectos(
    monto: number,
    convertirDe: string,
    convertirA: string,
  ): Observable<{ monto: string }> {
    return this.tipoMonedaServiceRemoto.convertirMontoEntreMonedasProyectos(monto, convertirDe, convertirA)
      .pipe(
        map(data => {
          return data.respuesta.datos
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }


}
