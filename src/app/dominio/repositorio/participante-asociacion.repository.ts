import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { PaginacionModel } from "dominio/modelo/paginacion-model";
import { ParticipanteAsociacionServiceRemoto } from '@core/servicios/remotos/participante-asociacion.service';
import { RespuestaRemota } from '@core/util/respuesta';
import { AsociacionEntityMapperService } from 'dominio/entidades/asociacion.entity';
import { ParticipanteAsociacionEntity, ParticipanteAsociacionMapperService } from 'dominio/entidades/participante-asociacion.entity';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';

@Injectable({ providedIn: 'root' })
export class ParticipanteAsociacionRepository {

  constructor(
    private participanteAsociacionServiceRemoto: ParticipanteAsociacionServiceRemoto,
    private participanteAsociacionMapperService: ParticipanteAsociacionMapperService,
    private asociacionEntityMapperService: AsociacionEntityMapperService
  ) { }

  obtenerParticipanteAsoTipo(idPerfil: string, limite: number, pagina: number, filtro: string, filtroOrden: string): Observable<PaginacionModel<ParticipanteAsociacionModel>> {
    return this.participanteAsociacionServiceRemoto.obtenerParticipanteAsoTipo(idPerfil, limite, pagina, filtro, filtroOrden)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<ParticipanteAsociacionEntity[]>>) => {
          const paginas: PaginacionModel<ParticipanteAsociacionModel> = {
            proximaPagina: data.headers.get("proximaPagina") == "true",
            totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
            lista: this.participanteAsociacionMapperService.transform(data.body.respuesta.datos)
          }
          return paginas
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  cambiarEstadoAsociacion(datos: ParticipanteAsociacionEntity): Observable<string> {
    return this.participanteAsociacionServiceRemoto.cambiarEstadoAsociacion(datos)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  crearAsociacion(datos: any): Observable<string> {

    return this.participanteAsociacionServiceRemoto.crearAsociacion(datos)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  obtenerParticipantesAsociacion(idAsociacion: string, idPerfil: string): Observable<AsociacionModel> {
    return this.participanteAsociacionServiceRemoto.obtenerParticipantesAsociacion(idAsociacion, idPerfil)
      .pipe(
        map(data => {
          return this.asociacionEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }
}
