import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AlbumServiceLocal } from '@core/servicios/locales/album.service';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { MediaServiceRemoto } from '@core/servicios/remotos/media.service';
import { MapearArchivoAlArchivoDefaultModelo } from 'dominio/entidades/archivo.entity';
import { MediaEntityMapperService } from 'dominio/entidades/media.entity';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';

@Injectable({ providedIn: 'root' })
export class MediaRepository {
  constructor(
    private mediaServiceRemoto: MediaServiceRemoto,
    private mediaServiceLocal: AlbumServiceLocal,
    private archivoEntityMapperDefault: MapearArchivoAlArchivoDefaultModelo,
    private mediaEntityMapperService: MediaEntityMapperService
  ) {}

  obtenerListaArchivosDefaultDelLocal(): ArchivoModel[] {
    return this.mediaServiceLocal.obtenerListaArchivosDefault();
  }

  guardarListaArchivosDefaultEnLocal(archivos: ArchivoModel[]) {
    this.mediaServiceLocal.guardarListaArchivosDefault(archivos);
  }

  obtenerListaArchivosDefaultDelApi(): Observable<ArchivoModel[]> {
    return this.mediaServiceRemoto.obtenerListaArchivosDefault().pipe(
      map((data) => {
        return this.archivoEntityMapperDefault.transform(data.respuesta.datos);
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  obtenerListaArchivosDefault(): Observable<ArchivoModel[]> {
    return of(this.mediaServiceLocal.obtenerListaArchivosDefault());
  }

  obtenerListaArchivosDefaultDemo(filtro: string): Observable<ArchivoModel[]> {
    return this.mediaServiceRemoto.obtenerListaArchivosDefaultDemo(filtro).pipe(
      map((data) => {
        return this.archivoEntityMapperDefault.transform(data.respuesta.datos);
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  subirArchivoAlservidor(body: any): Observable<MediaModel> {
    return this.mediaServiceRemoto.subirArchivoAlservidor(body).pipe(
      map((data) => {
        return this.mediaEntityMapperService.transform(data.respuesta.datos);
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  subirMedia(body: any): Observable<MediaModel> {
    return this.mediaServiceRemoto.subirMedia(body).pipe(
      map((data) => {
        return this.mediaEntityMapperService.transform(data.respuesta.datos);
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  subirVariasMedia(medias: any): Observable<MediaModel[]> {
    return this.mediaServiceRemoto.subirVariasMedia(medias).pipe(
      map((data) => {
        return this.mediaEntityMapperService.transform(data.respuesta.datos);
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }
}
