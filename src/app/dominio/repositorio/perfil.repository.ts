import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { LocalStorage } from '@core/servicios/locales/local-storage.service';
import { PerfilActivoEnrutador } from 'presentacion/enrutador/enrutador.component';
import { PerfilServiceRemoto } from '@core/servicios/remotos/perfil.service';
import { CatalogoTipoPerfilMapperService2 } from 'dominio/entidades/catalogos/catalogo-tipo-perfil.entity';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { PerfilModelEstadoMapperService, PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';
import { UsuarioModel, UsuarioModelMapperService } from 'dominio/modelo/entidades/usuario.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { PerfilServiceLocal } from '@core/servicios/locales/perfil.service';
import { RespuestaRemota } from '@core/util/respuesta';
import { PerfilEntity, PerfilEntityGeneralMapperService, PerfilEntityMapperServicePerfil, PerfilResumenEntityMapper } from 'dominio/entidades/perfil.entity';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ActualizarDatosUsuarioModelMapperService } from 'dominio/modelo/entidades/usuario.model';

@Injectable({ providedIn: 'root' })
export class PerfilRepository {


    constructor(
        private perfilServicieRemoto: PerfilServiceRemoto,
        private perfilServicieLocal: PerfilServiceLocal,
        private mapeador: CatalogoTipoPerfilMapperService2,
        private perfilEntityMapperService: PerfilEntityMapperServicePerfil,
        private perfilModelMapperService: PerfilModelMapperService,
        private perfilModelEstadoMapperService: PerfilModelEstadoMapperService,
        private usuarioModelMapperService: UsuarioModelMapperService,
        private actualizarDatosUsuarioModelMapperService: ActualizarDatosUsuarioModelMapperService,
        private mapeadorTipoPerfiles: CatalogoTipoPerfilMapperService2,
        private perfileMapper: PerfilEntityMapperServicePerfil,
        private perfilResumenEntityMapper: PerfilResumenEntityMapper,
        private localStorage: LocalStorage,
        private perfilEntityGeneralMapperService: PerfilEntityGeneralMapperService
    ) {

    }

    obtenerCatalogoTipoPerfil(): Observable<CatalogoTipoPerfilModel[]> {
        return this.perfilServicieRemoto.obtenerCatalogoTipoPerfil()
            .pipe( 
                map(data => {       
                    return this.mapeadorTipoPerfiles.transform(data.respuesta.datos);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }

    almacenarCatalogoPerfiles(tipoPerfiles: CatalogoTipoPerfilModel[]) {
        return this.localStorage.almacenarCatalogoPerfiles(tipoPerfiles);
    }

    obtenerCatalogoTipoPerfilLocal(): CatalogoTipoPerfilModel[] {
        return this.localStorage.obtenerCatalogoPerfiles();
    }

    almacenarPerfilSeleccionado(perfil: PerfilModel) {
        this.localStorage.almacenarPerfilSeleccionado(perfil)
    }

    removerPerfilSeleccionado() {
        this.localStorage.removerPerfilSeleccionado()
    }

    obtenerPerfilSeleccionado(): PerfilModel {
        return this.localStorage.obtenerPerfilSeleccionado();
    }

    validarNombreDeContactoUnico(nombreContacto: string, traducirContactName:boolean): Observable<object> {
        return this.perfilServicieRemoto.validarNombreDeContactoUnico(nombreContacto, traducirContactName)
            .pipe(
                map(data => { 
                    return data.respuesta.datos
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerDatosDelPerfil(id: string): Observable<PerfilModel> {
        return this.perfilServicieRemoto.obtenerDatosDelPerfil(id)
            .pipe(
                map(data => {
                    return this.perfilEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    guardarPerfilActivoEnrutador(perfiles: Array<PerfilActivoEnrutador>) {
        this.perfilServicieLocal.guardarPerfilActivoEnrutador(perfiles)
    }

    obtenerPerfilActivoEnrutador(): Array<PerfilActivoEnrutador> {
        return this.perfilServicieLocal.obtenerPerfilActivoEnrutador()
    }

    removerPerfilActivoEnrutador() {
        this.perfilServicieLocal.removerPerfilActivoEnrutador()
    }

    guardarPerfilActivoEnSessionStorage(perfil: PerfilModel) {
        this.perfilServicieLocal.guardarPerfilActivoEnSessionStorage(perfil)
    }

    obtenerPerfilActivoDelSessionStorage(): PerfilModel {
        return this.perfilServicieLocal.obtenerPerfilActivoDelSessionStorage()
    }
    
    removerPerfilActivoDelSessionStorage() {
        this.perfilServicieLocal.removerPerfilActivoDelSessionStorage()
    }
    eliminarVariableStorage(llave: string) {
        this.localStorage.eliminarVariableStorage(llave)
    }

    actualizarPerfil(perfil: PerfilModel): Observable<PerfilModel> {
        const perfilEntity = this.perfilModelMapperService.transform(perfil)

        return this.perfilServicieRemoto.actualizarPerfil(perfilEntity)
            .pipe(
                map(data => {
                    return this.perfilEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            ) 
    } 

    actualizarDatosUsuario(usuario: UsuarioModel, idUsuario: string): Observable<string> {
        
        const usuarioEntity = this.actualizarDatosUsuarioModelMapperService.transform(usuario)
        return this.perfilServicieRemoto.actualizarDatosUsuario(usuarioEntity, idUsuario)
            .pipe(
                map(data => {;
                    return (data.respuesta.mensaje)
                }),
                catchError(error => {
                    return throwError(error)
                })
            ) 
    }

    eliminarHibernarElPerfil(perfil: PerfilModel): Observable<object> {
        const perfilEntity = this.perfilModelEstadoMapperService.transform(perfil)

        return this.perfilServicieRemoto.eliminarHibernarElPerfil(perfilEntity)
    }

    crearPerfilEnElUsuario(perfil: PerfilModel, usuario: UsuarioModel): Observable<PerfilModel> {
        const perfilEntity = this.perfilModelMapperService.transform(perfil)
        const usuarioEntity = this.usuarioModelMapperService.transform(usuario)
        return this.perfilServicieRemoto.crearPerfilEnElUsuario(perfilEntity, usuarioEntity)
            .pipe(
                map(data => {
                    return this.perfilEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    activarPerfil(perfil: PerfilModel): Observable<object> {
        const perfilEntity : PerfilEntity = { _id: perfil._id }
        return this.perfilServicieRemoto.activarPerfil(perfilEntity)
    }

    buscarPerfiles(palabra: string, limite: number, pagina: number, perfil: string): Observable<PaginacionModel<PerfilModel>> {        
        return this.perfilServicieRemoto.buscarPerfiles(palabra, limite, pagina, perfil).pipe(
            map(data => {
                let cargarMas = data.headers.get("proximaPagina") == "true"
                let resultado: PaginacionModel<PerfilModel> =
                {
                    proximaPagina: cargarMas,
                    lista: this.perfileMapper.transform(data.body.respuesta.datos)
                }
                return resultado; 
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

    buscarPerfilesPorNombre(
        query: string,
        idPerfil: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<PerfilModel>> {
        return this.perfilServicieRemoto.buscarPerfilesPorNombre(
            query,
            idPerfil, 
            limite,
            pagina
        ).pipe(
            map((data: HttpResponse<RespuestaRemota<Array<PerfilEntity>>>) => {
                const dataPagina: PaginacionModel<PerfilModel> = {
                    proximaPagina: data.headers.get("proximaPagina") == "true",
                    totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
                    lista: this.perfileMapper.transform(data.body.respuesta.datos)
                }
                return dataPagina
            }),
            catchError(error => {
                return throwError(error)
            })
        )
    }

    obtenerPerfilGeneral(idPerfil: string, idPerfilOtros?: string): Observable<PerfilEntity> {
        return this.perfilServicieRemoto.obtenerPerfilGeneral(idPerfil, idPerfilOtros)
            .pipe(
                map(data => {
      
                    
                    return data.respuesta.datos
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    
    }

    obtenerPerfilBasico(
        idPerfil: string,
        perfilVisitante: string
    ): Observable<PerfilModel> {
        return this.perfilServicieRemoto.obtenerPerfilBasico(idPerfil, perfilVisitante)
            .pipe(
                map(data => { 
                    return this.perfilEntityGeneralMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    guardarTipoPerfilActivo(tipoPerfil: CatalogoTipoPerfilModel) {
        this.perfilServicieLocal.guardarTipoPerfilActivo(tipoPerfil)
    }

    obtenerTipoPerfilActivo(): CatalogoTipoPerfilModel {
        return this.perfilServicieLocal.obtenerTipoPerfilActivo()
    }

    removerTipoPerfilActivoDelSessionStorage() {
        this.perfilServicieLocal.removerTipoPerfilActivoDelSessionStorage()
    }

    obtenerInformacionDelPerfil(
        idPerfil: string,
        idPerfilOtros?: string
    ): Observable<PerfilModel> {
        return this.perfilServicieRemoto.obtenerInformacionDelPerfil(idPerfil, idPerfilOtros).pipe(
            map(data => {       
                return this.perfilEntityGeneralMapperService.transform(data.respuesta.datos)
            }),
            catchError(error => {
                return throwError(error)
            })
        )
    }

    obtenerResumenPerfilParaCoautorProyecto(
		idPerfil: string
	): Observable<PerfilModel> {
        return this.perfilServicieRemoto.obtenerResumenPerfilParaCoautorProyecto(idPerfil)
            .pipe(
                map(data => {
                    return this.perfilEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }
 
    
} 
