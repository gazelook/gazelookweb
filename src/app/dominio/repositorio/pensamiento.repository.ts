import { PaginacionModel } from '../modelo/paginacion-model';
import { Injectable } from "@angular/core"
import { PensamientoService } from "@core/servicios/remotos/pensamiento.service"
import { Observable, throwError } from "rxjs"
import { catchError, map } from 'rxjs/operators'
import { PensamientoEntity, PensamientoMapperService } from "dominio/entidades/pensamiento.entity"
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model'
import { HttpResponse } from '@angular/common/http'
import { RespuestaRemota } from '@core/util/respuesta'
@Injectable({
    providedIn: 'root'
})

export class PensamientoRepository {
    constructor(
        private pensamientoService: PensamientoService,
        private pensamientoMapperService: PensamientoMapperService
    ) {
    }

    obtenerPensamientoAleatorio(): Observable<PensamientoModel> {
        return this.pensamientoService.obtenerPensamientoAleatorio()
            .pipe(
                map(data => {
                    return this.pensamientoMapperService.transform(data.respuesta.datos);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }
    obtenerPensamientos(idPerfil: string, esPrivado: boolean): Observable<Array<PensamientoModel>> {
        return this.pensamientoService.obtenerPensamientos(idPerfil, esPrivado)
            .pipe(
                map(data => {
                    return this.pensamientoMapperService.transform(data.respuesta.datos);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }
    //PensamientoRemotoModel
    crearPensamiento(datos: PensamientoEntity): Observable<PensamientoModel> {

        return this.pensamientoService.crearPensamiento(datos)
            .pipe(
                map(data => {

                    return this.pensamientoMapperService.transform(data.respuesta.datos);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }
    actualizarPensamiento(datos: PensamientoEntity): Observable<string> {
        return this.pensamientoService.actualizarPensamiento(datos)
            .pipe(
                map(data => {

                    return data.respuesta.mensaje;
                }),
                catchError(err => {

                    return throwError(err)
                })
            )
    }
    actualizarEstadoPensamiento(idPensamiento: string): Observable<PensamientoModel> {
        return this.pensamientoService.actualizarEstadoPensamiento(idPensamiento)
            .pipe(
                map(data => {

                    return this.pensamientoMapperService.transform(data.respuesta.datos)
                }),
                catchError(err => {

                    return throwError(err)
                })
            )
    }

    eliminarPensamiento(idPensamiento: string): Observable<string> {
        return this.pensamientoService.eliminarPensamiento(idPensamiento)
            .pipe(
                map(data => {

                    return data.respuesta.mensaje
                }),
                catchError(err => {

                    return throwError(err)
                })
            )
    }
    cargarMasPensamientos(
        perfil: string,
        limite: number,
        pagina: number,
        esPublico: boolean,
        traducir: boolean,
    ): Observable<PaginacionModel<PensamientoModel>> {
        return this.pensamientoService.cargarMasPensamientos(
            perfil,
            limite,
            pagina,
            esPublico,
            traducir
        ).pipe(
            map((data:HttpResponse<RespuestaRemota<PensamientoEntity[]>>) => {
                const dataPaginacion: PaginacionModel<PensamientoModel> = {
                    proximaPagina: data.headers.get("proximaPagina") === "true",
                    lista: this.pensamientoMapperService.transform(data.body.respuesta.datos)
                }
                return dataPaginacion
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }
}
