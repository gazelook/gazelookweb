import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {DataCerrarSesion} from 'src/app/app.component';
import {LocalStorage} from '@core/servicios/locales/local-storage.service';
import {CuentaServiceRemoto} from '@core/servicios/remotos/cuenta.service';
import {CorreoContacto} from 'presentacion/contacto/contacto.component';
import {IniciarSesionMapperService} from 'dominio/entidades/iniciar-sesion.entity';
import {PagoEntity} from 'dominio/entidades/pago.entity';
import {UsuarioEntity} from 'dominio/entidades/usuario.entity';
import {CatalogoTipoPerfilModel} from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import {UsuarioModel} from 'dominio/modelo/entidades/usuario.model';
import {IniciarSesionModel} from 'dominio/modelo/iniciar-sesion.model';
import {PagoModel} from 'dominio/modelo/pago.model';
import {TokenModel} from 'dominio/modelo/token.model';
import {CuentaServiceLocal} from '@core/servicios/locales/cuenta.service';
import {FormularioContactanos} from '../modelo/entidades/fomulario-contactanos.model';
import {PagoService} from '@core/servicios/remotos';
import {CoinPaymentesResponse, CoinPaymentsRatesEntity} from 'dominio/entidades/catalogos/coinpayments-rates.entity';

const cripto = [
  {n: 0, symbol: 'BTC', idCripto: 1},
  {n: 1, symbol: 'ETH', idCripto: 1027},
  {n: 2, symbol: 'USDT', idCripto: 825},
  {n: 3, symbol: 'SOL', idCripto: 5426},
  {n: 4, symbol: 'ADA', idCripto: 2010},
  {n: 5, symbol: 'XRP', idCripto: 52},
  {n: 6, symbol: 'USDC', idCripto: 3408},
  {n: 7, symbol: 'DOT', idCripto: 6636},
  {n: 8, symbol: 'DOGE', idCripto: 74},
  {n: 9, symbol: 'SHIB', idCripto: 5994},
  {n: 10, symbol: 'BUSD', idCripto: 4687},
  {n: 11, symbol: 'LTC', idCripto: 2},
  {n: 12, symbol: 'TRX', idCripto: 1958},
  {n: 13, symbol: 'LINK', idCripto: 1975},
  {n: 14, symbol: 'BCH', idCripto: 1831},
  {n: 15, symbol: 'DAI', idCripto: 4943},
  {n: 16, symbol: 'BCH.BEP2', idCripto: 1831},
  {n: 17, symbol: 'ETC.BEP20', idCripto: 1321},
  {n: 18, symbol: 'XMR', idCripto: 328},
  {n: 19, symbol: 'BTT', idCripto: 3718},
  {n: 20, symbol: 'LTCT', idCripto: 2}
];

@Injectable({
  providedIn: 'root'
})
export class CuentaRepository {

  constructor(
    private localStorage: LocalStorage,
    private cuentaServiceRemoto: CuentaServiceRemoto,
    private cuentaServiceLocal: CuentaServiceLocal,
    private pagoService: PagoService,
    private iniciarSesionMapperService: IniciarSesionMapperService
  ) {
  }

  iniciarSesion(datos: Object): Observable<IniciarSesionModel> {
    return this.cuentaServiceRemoto.iniciarSesion(datos)
      .pipe(
        map(data => {
          return this.iniciarSesionMapperService.transform(data.respuesta.datos);
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  guardarTokenAutenticacion(token: string) {
    this.localStorage.guardarTokenAutenticacion(token);
  }

  obtenerTokenRefresh(): string {
    return this.localStorage.obtenerTokenRefresh();
  }

  obtenerTokenAutenticacion(): string {
    return this.localStorage.obtenerTokenAutenticacion();
  }

  guardarTokenRefresh(token: string) {
    this.localStorage.guardarTokenRefresh(token);
  }

  guardarFirebaseUIDEnLocalStorage(uid: string) {
    this.cuentaServiceLocal.guardarFirebaseUIDEnLocalStorage(uid);
  }

  obtenerFirebaseUIDEnLocalStorage(): string {
    return this.cuentaServiceLocal.obtenerFirebaseUIDEnLocalStorage();
  }

  //GUARDA EL TIPO LA LISTA DE PERFILES DEL CATALOGO JUNTO A LOS PERFILES QUE TIENE EL USUARIO
  almacenarCatalogoPerfiles(tipoPerfiesUser: CatalogoTipoPerfilModel[]) {
    this.localStorage.almacenarCatalogoPerfiles(tipoPerfiesUser);
  }

  obtenerTipoPerfiles(): Array<CatalogoTipoPerfilModel> {
    return this.localStorage.obtenerCatalogoPerfiles();
  }

  crearCuenta(usuario: UsuarioEntity): Observable<PagoEntity> {
    return this.cuentaServiceRemoto.crearCuenta(usuario)
      .pipe(
        map(data => {
          return data.respuesta.datos as PagoModel;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  activarCuenta(data: any): Observable<IniciarSesionModel> {
    return this.cuentaServiceRemoto.activarCuenta(data)
      .pipe(
        map(data => {
          return this.iniciarSesionMapperService.transform(data.respuesta.datos);
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  crearCuentaPaymentez(usuario: UsuarioEntity): Observable<any> {
    return this.cuentaServiceRemoto.crearCuentaPaymentez(usuario)
      .pipe(
        map(data => data.respuesta.datos),
        catchError(err => throwError(err))
      );
  }

  activarCuentaPaymentez(usuario: any): Observable<any> {
    return this.cuentaServiceRemoto.activarCuentaPaymentez(usuario)
      .pipe(
        map(data => this.iniciarSesionMapperService.transform(data.respuesta.datos)),
        catchError(err => throwError(err))
      );
  }

  crearCuentaCoinPayments(usuario: UsuarioEntity): Observable<CoinPaymentesResponse> {
    return this.cuentaServiceRemoto.crearCuentaCoinPayments(usuario)
      .pipe(
        map(data => data.respuesta.datos),
        catchError(err => throwError(err))
      );
  }

  obtenerCoinPaymentsRates(): Observable<CoinPaymentsRatesEntity[]> {
    return this.pagoService.obtenerCoinPaymentsRates()
      .pipe(
        map(data => {
          const resp = Object.entries(data.respuesta.datos);
          const datos: CoinPaymentsRatesEntity[] = [];
          resp.map((item) => {
            if (item[1].is_fiat === 0) {
              cripto.map(c => {
                if (c.symbol === item[0]) {
                  datos.push({...item[1], currency: item[0], idCripto: c.idCripto});
                }
              });
            }
          });
          return datos;
        }),
        catchError(err => throwError(err))
      );
  }

  refrescarToken(tokenRefrescar: string): Observable<TokenModel> {
    return this.cuentaServiceRemoto.refrescarToken(tokenRefrescar)
      .pipe(
        map(data => {
          return data.respuesta.datos as TokenModel;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  // Guardar usuario en el local storage
  guardarUsuarioEnLocalStorage(usuario: UsuarioModel) {
    this.cuentaServiceLocal.guardarUsuarioEnLocalStorage(usuario);
  }

  // Obtener usuario del local storage
  obtenerUsuarioDelLocalStorage(): UsuarioModel {
    return this.cuentaServiceLocal.obtenerUsuarioDelLocalStorage();
  }

  removerUsuarioDelLocalStorage() {
    return this.cuentaServiceLocal.removerUsuarioDelLocalStorage();
  }

  // Guardar usuario en el session storage
  guardarUsuarioEnSessionStorage(usuario: UsuarioModel) {
    this.cuentaServiceLocal.guardarUsuarioEnSessionStorage(usuario);
  }

  // Obtener usuario del session storage
  obtenerUsuarioDelSessionStorage(): UsuarioModel {
    return this.cuentaServiceLocal.obtenerUsuarioDelSessionStorage();
  }

  removerUsuarioDelSessionStorage() {
    this.cuentaServiceLocal.removerUsuarioDelSessionStorage();
  }

  eliminarSessionStorage() {
    this.cuentaServiceLocal.eliminarSessionStorage();
  }

  validarEmailUnico(email: string): Observable<string> {
    return this.cuentaServiceRemoto.validarEmailUnico(email)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  recuperarContrasena(email: string): Observable<string> {
    return this.cuentaServiceRemoto.recuperarContrasena(email)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  enviarEmailDeContacto(correo: CorreoContacto): Observable<string> {
    return this.cuentaServiceRemoto.enviarEmailDeContacto(correo)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  solicitarInformacionUsuario(idUsuario: string): Observable<any> {
    return this.cuentaServiceRemoto.solicitarInformacionUsuario(idUsuario)
      .pipe(
        map(data => {
          return data.codigoEstado;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  eliminarDatosUsuario(idUsuario: string): Observable<any> {
    return this.cuentaServiceRemoto.eliminarDatosUsuario(idUsuario)
      .pipe(
        map(data => {
          return data.respuesta;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  reenviarCorreoVerificacion(idUsuario: string): Observable<any> {
    return this.cuentaServiceRemoto.reenviarCorreoVerificacion(idUsuario)
      .pipe(
        map(data => {
          return data.respuesta;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  cerrarSessionEnElApi(
    usuario: UsuarioModel
  ): Observable<number> {
    const data: DataCerrarSesion = {
      idUsuario: usuario.id,
      idDispositivo: (usuario.dispositivos && usuario.dispositivos.length > 0) ? usuario.dispositivos[0].id : ''
    };

    return this.cuentaServiceRemoto.cerrarSessionEnElApi(data).pipe(
      map(data => {
        return data.codigoEstado;
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }

  enviarFormularioContactanos(formulario: FormularioContactanos): Observable<number> {

    return this.cuentaServiceRemoto.enviarFormularioContactanos(formulario)
      .pipe(
        map(data => {

          return data.codigoEstado;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }
}
