import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { RespuestaRemota } from '@core/util/respuesta';
import { UbicacionServiceRemoto } from '@core/servicios/remotos/ubicacion.service';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { UbicacionServiceLocal } from '@core/servicios/locales/ubicacion.service';
import { CatalogoLocalidadEntity } from 'dominio/entidades/catalogos/catalogo-localidad.entity';
import { CatalogoPaisEntityMapperService, CatalogoPaisMapperAItemSelectorService } from 'dominio/entidades/catalogos/catalogo-pais.entity';
import { CatalogoLocalidadMapperAItemSelectorService } from 'dominio/modelo/catalogos/catalogo-localidad.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';

@Injectable({ providedIn: 'root' })
export class UbicacionRepository {

  constructor(
    private ubicacionServiceRemoto: UbicacionServiceRemoto,
    private ubicacionServiceLocal: UbicacionServiceLocal,
    private mapearAEntidad: CatalogoPaisEntityMapperService,
    private mapearPaisAItemSelector: CatalogoPaisMapperAItemSelectorService,
    private mapearLocalidadAItemSelector: CatalogoLocalidadMapperAItemSelectorService
  ) { }

  // Obtener catalogo de paises - Remoto
  obtenerCatalogoPaisesParaSelector(): Observable<ItemSelector[]> {
    return this.ubicacionServiceRemoto.obtenerCatalogoPaises().pipe(
      map(data => {

        return this.mapearPaisAItemSelector.transform(this.mapearAEntidad.transform(data.respuesta.datos))
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  // Obtener catalogo de localidades por pais - Remoto
  obtenerCatalogoLocalidadesPorNombrePorPaisParaSelector(pais: string, query: string): Observable<ItemSelector[]> {
    return this.ubicacionServiceRemoto.obtenerCatalogoLocalidadesPorNombrePorPaises(pais, query).pipe(
      map(data => {
        return this.mapearLocalidadAItemSelector.transform(data.respuesta.datos)
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  // Obtener paises del local storage - Local
  obtenerPaisesDelLocalStorageParaItemSelector(): ItemSelector[] {
    return this.ubicacionServiceLocal.obtenerPaisesParaItemSelector()
  }
  // Guardar paises del local storage - Local
  guardarPaisesEnElLocalStorageDesdeElItemSelector(paises: ItemSelector[]) {
    this.ubicacionServiceLocal.guardarPaisesDelItemSelector(paises)
  }

  eliminarVariableStorage(llave: string) {
    this.ubicacionServiceLocal.eliminarVariableStorage(llave)
  }

  buscarLocalidadesPorNombrePaisConPaginacion(
    limite: number,
    pagina: number,
    codigoPais: string,
    busqueda: string
  ): Observable<PaginacionModel<ItemSelector>> {
    return this.ubicacionServiceRemoto.buscarLocalidadesPorNombrePaisConPaginacion(
      limite,
      pagina,
      codigoPais,
      busqueda
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<CatalogoLocalidadEntity>>>) => {
        const dataPagina: PaginacionModel<ItemSelector> = {
          proximaPagina: data.headers.get("proximaPagina") == "true",
          totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
          lista: this.mapearLocalidadAItemSelector.transform(data.body.respuesta.datos)
        }
        return dataPagina
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }
}
