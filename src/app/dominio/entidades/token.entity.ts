export interface TokenEntity {
  tokenAccess: string,
  tokenRefresh: string
}
