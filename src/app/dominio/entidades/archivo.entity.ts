import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model'
import { Injectable } from '@angular/core'
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity"
import { CatalogoTipoMediaEntity, CatalogoTipoMediaEntityMapperService } from "dominio/entidades/catalogos/catalogo-tipo-media.entity"
import { MapedorService } from "@core/base/mapeador.interface"

export interface ArchivoEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity,
    fechaCreacion?: Date
    fechaActualizacion?: Date,
    url?: string,
    tipo?: CatalogoTipoMediaEntity,
    peso?: number,
    relacionAspecto?: number,
    path?: string,
    duracion?: string,
    catalogoArchivoDefault?: string
    fileDefault?: boolean
    filename?: string
}

@Injectable({ providedIn: 'root' })
export class ArchivoEntityMapperService extends MapedorService<ArchivoEntity, ArchivoModel> {

    constructor(
        private catalogoTipoMediaEntityMapper: CatalogoTipoMediaEntityMapperService,
        private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService
    ) {
        super()
    }

    protected map(entity: ArchivoEntity): ArchivoModel {
        if (entity) {
            const model: ArchivoModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
            }

            if (entity.url) {
                model.url = entity.url
            }

            if (entity.tipo) {
                model.tipo = this.catalogoTipoMediaEntityMapper.transform(entity.tipo)
            }

            if (entity.peso) {
                model.peso = entity.peso
            }

            if (entity.relacionAspecto) {
                model.relacionAspecto = entity.relacionAspecto
            }

            if (entity.path) {
                model.path = entity.path
            }
            if (entity.filename) {
                model.filename = entity.filename
            }

            if (entity.catalogoArchivoDefault) {
                model.catalogoArchivoDefault = entity.catalogoArchivoDefault
            }
            if (entity.fileDefault) {
                model.fileDefault = entity.fileDefault
            }

            if (entity.duracion) {
                model.duracion = entity.duracion
            }
            return model
        }
        return null
    }

}
@Injectable({ providedIn: 'root' })
export class MapearArchivoAlArchivoDefaultModelo extends MapedorService<ArchivoEntity, ArchivoModel> {

    protected map(entity: ArchivoEntity): ArchivoModel {
        return {
            id: entity._id,
            url: entity.url,
            catalogoArchivoDefault: entity.catalogoArchivoDefault
        };
    }
}
