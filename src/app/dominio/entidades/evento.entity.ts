import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ConfiguracionEventoEntity } from "dominio/entidades/catalogos/configuracion-evento.entity";
import { ProyectoEntity } from "dominio/entidades/proyecto.entity";
export interface EventoEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  fechaInicio?: Date,
  fechaFin?: Date,
  configuracionEvento?: ConfiguracionEventoEntity,
  proyectos?: Array<ProyectoEntity>
}
