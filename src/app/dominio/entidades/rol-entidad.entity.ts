import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { RolEntidadModel } from 'dominio/modelo/entidades/rol-entidad.model';
import { CatalogoAccionEntity, CatalogoAccionEntityMapperService } from "dominio/entidades/catalogos/catalogo-accion.entity";
import { CatalogoEntidadEntity } from "dominio/entidades/catalogos/catalogo-entidad.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoRolEntity, CatalogoRolEntityMapperService } from "dominio/entidades/catalogos/catalogo-rol.entity";
export interface RolEntidadEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  acciones?: Array<CatalogoAccionEntity>
  entidad?: CatalogoEntidadEntity
  rol?: CatalogoRolEntity
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class RolEntidadEntityMapperService extends MapedorService<RolEntidadEntity, RolEntidadModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private catalogoAccionEntityMapperService: CatalogoAccionEntityMapperService,
    private catalogoRolEntityMapperService: CatalogoRolEntityMapperService,
  ) {
    super()
  }

  protected map(entity: RolEntidadEntity): RolEntidadModel {

    if (entity) {
      const model: RolEntidadModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.acciones) {
        model.acciones = this.catalogoAccionEntityMapperService.transform(entity.acciones)
      }

      if (entity.entidad) {
        model.entidad = this.catalogoRolEntityMapperService.transform(entity.entidad)
      }

      if (entity.rol) {
        model.rol = this.catalogoRolEntityMapperService.transform(entity.rol)
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }
      return model
    }
    return null
  }
}
