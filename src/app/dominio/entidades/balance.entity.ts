import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { TransaccionEntity } from "dominio/entidades/transaccion.entity";
export interface BalanceEntity {
  id: string,
  fechaCreacion: Date,
  fechaActualizacion: Date,
  estado: CatalogoEstadoEntity,
  valorActual: number,
  totalIngreso: number,
  totalEgreso: number,
  proviene: BalanceEntity,
  transacciones: Array<TransaccionEntity>
}
