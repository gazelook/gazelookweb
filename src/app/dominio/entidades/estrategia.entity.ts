import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoMonedaEntity } from "dominio/entidades/catalogos/catalogo-tipo-moneda.entity";
import { MediaEntity } from "dominio/entidades/media.entity";

export interface EstrategiaEntity {
  id: string,
  estado: CatalogoEstadoEntity,
  fechaCreacion: Date,
  fecaActualizacion: Date,
  fechaCaducidad: Date,
  presupuesto: number,
  justificacion: string,
  adjuntos: Array<MediaEntity>,
  moneda: CatalogoTipoMonedaEntity
}
