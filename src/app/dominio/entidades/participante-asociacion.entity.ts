import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { AsociacionEntity, AsociacionResumenEntityMapperService } from 'dominio/entidades/asociacion.entity';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ConfiguracionEstiloEntity } from "dominio/entidades/configuracion-estilo.entity";
import { PerfilEntity, PerfilEntityMapperServicePerfil } from "dominio/entidades/perfil.entity";
import { RolEntidadEntity, RolEntidadEntityMapperService } from 'dominio/entidades/rol-entidad.entity';
export interface ParticipanteAsociacionEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  roles?: Array<RolEntidadEntity>
  invitadoPor?: ParticipanteAsociacionEntity
  configuraciones?: Array<ConfiguracionEstiloEntity>
  perfil?: PerfilEntity
  contactoDe?: PerfilEntity
  sobrenombre?: string,
  asociacion?: AsociacionEntity
}
@Injectable({ providedIn: 'root' })
//llega entidad envio PensamientoModel
export class ParticipanteAsociacionMapperService extends MapedorService<ParticipanteAsociacionEntity, ParticipanteAsociacionModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil,
    private rolSistemaEntityMapperService: RolEntidadEntityMapperService,
    private asociacionResumenEntityMapperService: AsociacionResumenEntityMapperService
  ) {
    super();
  }

  protected map(entity: ParticipanteAsociacionEntity): ParticipanteAsociacionModel {

    if (entity) {
      const participanteAsoModel: ParticipanteAsociacionModel = {}
      if (entity._id) {
        participanteAsoModel.id = entity._id
      }

      if (entity.roles) {
        participanteAsoModel.roles = this.rolSistemaEntityMapperService.transform(entity.roles)
      }

      if (entity.estado) {
        participanteAsoModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.invitadoPor) {
        participanteAsoModel.invitadoPor = this.transform(entity.invitadoPor)
      }

      if (entity.perfil) {
        participanteAsoModel.perfil = this.perfilEntityMapperServicePerfil.transform(entity.perfil)
      }

      if (entity.contactoDe) {
        participanteAsoModel.contactoDe = this.perfilEntityMapperServicePerfil.transform(entity.contactoDe)
      }

      if (entity.invitadoPor) {
        participanteAsoModel.contactoDe = this.perfilEntityMapperServicePerfil.transform(entity.invitadoPor.perfil)
      }

      if (entity.asociacion) {
        participanteAsoModel.asociacion = this.asociacionResumenEntityMapperService.transform(entity.asociacion)
      }
      return participanteAsoModel
    }
    return null
  }
}
