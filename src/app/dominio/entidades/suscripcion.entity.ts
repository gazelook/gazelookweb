import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoSuscripcionEntity } from "dominio/entidades/catalogos/catalogo-suscripcion.entity";
import { TransaccionEntity } from "dominio/entidades/transaccion.entity";
export interface SuscripcionEntity {
  id: string,
  fechaCreacion: Date,
  fechaRenovacion: Date,
  estado: CatalogoEstadoEntity,
  tipo: CatalogoSuscripcionEntity,
  transacion: TransaccionEntity
}
