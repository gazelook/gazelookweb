import { CatalogoTipoEstiloAnuncioSistemaModel } from 'dominio/modelo/catalogos/catalogo-tipo-estilo-anuncio-sistema.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';

export interface CatalogoTipoEstiloAnuncioSistemaEntity {
    codigo?: string,
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoEstiloAnuncioSistemaElegibleMapperService extends MapedorService<CatalogoTipoEstiloAnuncioSistemaEntity, CatalogoTipoEstiloAnuncioSistemaModel> {

    protected map(model: CatalogoTipoEstiloAnuncioSistemaEntity): CatalogoTipoEstiloAnuncioSistemaModel {
        return model;
    }
}