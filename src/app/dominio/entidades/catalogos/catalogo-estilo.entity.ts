import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstiloModel } from 'dominio/modelo/catalogos/catalogo-estilo.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoEstiloEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity,
  nombre?: string,
  descripcion?: string,
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}



@Injectable({ providedIn: 'root' })
export class CatalogoEstiloMapperService extends MapedorService<CatalogoEstiloEntity, CatalogoEstiloModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoEstiloEntity): CatalogoEstiloModel {
    if (entity) {
      const catalogoEstiloModel: CatalogoEstiloModel = {}

      if (entity._id) {
        catalogoEstiloModel.id = entity._id
      }

      if (entity.codigo) {
        catalogoEstiloModel.codigo = entity.codigo
      }

      if (entity.estado) {
        catalogoEstiloModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }


      if (entity.nombre) {
        catalogoEstiloModel.nombre = entity.nombre
      }


      return catalogoEstiloModel
    }

    return null
  }

}
