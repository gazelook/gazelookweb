import { CatalogoTipoRolModel } from 'dominio/modelo/catalogos/catalogo-tipo-rol.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
export interface CatalogoTipoRolEntity {
    _id?:string,
    codigo?:string,
    estado?:CatalogoEstadoEntity
    nombre?:string,
    fechaCreacion?:Date,
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoRolEntityMapperService extends MapedorService<CatalogoTipoRolEntity, CatalogoTipoRolModel> {

    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    ) {
        super()
    }

    protected map(entity: CatalogoTipoRolEntity): CatalogoTipoRolModel {
        if (entity) {
            const model: CatalogoTipoRolModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.codigo) {
                model.codigo = entity.codigo
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.nombre) {
                model.nombre = entity.nombre
            }
            return model
        }
        return null
    }
}
