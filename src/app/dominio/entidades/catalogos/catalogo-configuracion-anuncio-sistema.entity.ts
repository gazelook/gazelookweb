import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoConfiguracionAnuncioSistemaModel } from 'dominio/modelo/catalogos/catalogo-configuracion-anuncio-sistema';
import { CatalogoEstiloAnuncioSistemaEntity, CatalogoEstiloAnuncioSistemaMapperService } from 'dominio/entidades/catalogos/catalogo-estilo-anuncio-sistema';
export interface CatalogoConfiguracionAnuncioSistemaEntity {
    estilos?: Array<CatalogoEstiloAnuncioSistemaEntity>
}

@Injectable({ providedIn: 'root' })
export class CatalogoConfiguracionAnuncioSistemaMapperService extends MapedorService<CatalogoConfiguracionAnuncioSistemaEntity, CatalogoConfiguracionAnuncioSistemaModel> {

    constructor(
        private catalogoEstiloAnuncioSistemaMapperService: CatalogoEstiloAnuncioSistemaMapperService,
    ) {
        super()
    }

    protected map(entity: CatalogoConfiguracionAnuncioSistemaEntity): CatalogoConfiguracionAnuncioSistemaModel {
        if (entity) {
            const model: CatalogoConfiguracionAnuncioSistemaEntity = {}
          
            if (entity.estilos) model.estilos = entity.estilos

            return model
        }

        return null
    }

}