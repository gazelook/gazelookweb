import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoPaticipanteConfiguracionModel } from 'dominio/modelo/catalogos/catalogo-participante-configuracion.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoPaticipanteConfiguracionEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  codigo?: string,
  nombre?: string,
}
@Injectable({ providedIn: 'root' })
export class CatalogoPaticipanteConfiguracionEntityMapperService extends MapedorService<CatalogoPaticipanteConfiguracionEntity, CatalogoPaticipanteConfiguracionModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoPaticipanteConfiguracionEntity): CatalogoPaticipanteConfiguracionModel {

    if (entity) {

      const model: CatalogoPaticipanteConfiguracionModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }
      return model
    }
    return null
  }
}
