import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoAsociacionModel } from 'dominio/modelo/catalogos/catalogo-tipo-asociacion.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoTipoAsociacionEntity {
  _id?: string
  estado?: CatalogoEstadoEntity //CatalogoEstado
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoAsociacionEntityMapperService extends MapedorService<CatalogoTipoAsociacionEntity, CatalogoTipoAsociacionModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoTipoAsociacionEntity): CatalogoTipoAsociacionModel {
    if (entity) {
      const catalogoTipoAsociacionModel: CatalogoTipoAsociacionModel = {}

      if (entity._id) {
        catalogoTipoAsociacionModel.id = entity._id
      }

      if (entity.codigo) {
        catalogoTipoAsociacionModel.codigo = entity.codigo
      }

      if (entity.nombre) {
        catalogoTipoAsociacionModel.nombre = entity.nombre
      }

      if (entity.estado) {
        catalogoTipoAsociacionModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }
      return catalogoTipoAsociacionModel
    }
    return null
  }
}
