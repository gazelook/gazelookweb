import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEntidadModel } from 'dominio/modelo/catalogos';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-estado.entity';
export interface CatalogoEntidadEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity, // CatalogoEstado
  codigo?: string,
  nombre?: string,
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}
@Injectable({ providedIn: 'root' })
export class CatalogoEntidadMapperService extends MapedorService<CatalogoEntidadEntity, CatalogoEntidadModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoEntidadEntity): CatalogoEntidadModel {
    if (entity) {
      const catalogoEntidadModel: CatalogoEntidadModel = {}

      if (entity._id) {
        catalogoEntidadModel.id = entity._id
      }

      if (entity.codigo) {
        catalogoEntidadModel.codigo = entity.codigo
      }

      if (entity.estado) {
        catalogoEntidadModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.nombre) {
        catalogoEntidadModel.nombre = entity.nombre
      }
      return catalogoEntidadModel
    }
    return null
  }
}
