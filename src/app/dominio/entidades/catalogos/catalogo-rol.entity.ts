import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoRolModel } from 'dominio/modelo/catalogos/catalogo-rol.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoRolEntity, CatalogoTipoRolEntityMapperService } from "dominio/entidades/catalogos/catalogo-tipo-rol.entity";
export interface CatalogoRolEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity
  nombre?: string,
  fechaCreacion?: Date,
  tipo?: CatalogoTipoRolEntity,
  descripcion?: string
}



@Injectable({ providedIn: 'root' })
export class CatalogoRolEntityMapperService extends MapedorService<CatalogoRolEntity, CatalogoRolModel> {

  constructor(
    private catalogoEstadoEntityMapperServic: CatalogoEstadoEntityMapperService,
    private catalogoTipoRolEntityMapperService: CatalogoTipoRolEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoRolEntity): CatalogoRolModel {
    if (entity) {
      const model: CatalogoRolModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperServic.transform(entity.estado)
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoRolEntityMapperService.transform(entity.tipo)
      }

      if (entity.descripcion) {
        model.descripcion = entity.descripcion
      }
      return model
    }
    return null
  }
}
