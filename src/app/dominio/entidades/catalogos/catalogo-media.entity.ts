import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoMediaModel } from 'dominio/modelo/catalogos/catalogo-media.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoMediaEntity {
  id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoMediaEntityMapperService extends MapedorService<CatalogoMediaEntity, CatalogoMediaModel> {
  protected map(entity: CatalogoMediaEntity): CatalogoMediaModel {
    if (entity) {
      const mediaModel: CatalogoMediaModel = {}
      if (entity.id) {
          mediaModel.id = entity.id
      }

      if (entity.codigo) {
          mediaModel.codigo = entity.codigo
      }


      return mediaModel
  }
  return null
  }
}
