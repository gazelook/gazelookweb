import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoColorModel } from 'dominio/modelo/catalogos';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoColorEntity, CatalogoTipoColorMapperService } from "dominio/entidades/catalogos/catalogo-tipo-color.entity";
export interface CatalogoColorEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity,
  tipo?: CatalogoTipoColorEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}
@Injectable({ providedIn: 'root' })
export class CatalogoColorMapperService extends MapedorService<CatalogoColorEntity, CatalogoColorModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private catalogoTipoColorMapperService: CatalogoTipoColorMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoColorEntity): CatalogoColorModel {
    if (entity) {
      const model: CatalogoColorModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.estado) {
        model.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoColorMapperService.transform(entity.tipo)
      }
      return model
    }
    return null
  }
}
