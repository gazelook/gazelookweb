import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoMensajeModel } from 'dominio/modelo/catalogos/catalogo-tipo-mensaje.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoMensajeEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoMensajeEntityMapperService extends MapedorService<CatalogoMensajeEntity, CatalogoMensajeModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }
  protected map(entity: CatalogoMensajeEntity): CatalogoMensajeModel {
    if (entity) {
      const model: CatalogoMensajeModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.estado) {
        model.estado = this.estadoEntityMapper.transform(entity.estado)
      }
      return model
    }
    return null
  }
}
