import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoLocalidadModel } from 'dominio/modelo/catalogos/catalogo-localidad.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoPaisEntity, CatalogoPaisEntityMapperService } from "dominio/entidades/catalogos/catalogo-pais.entity";
export interface CatalogoLocalidadEntity {
  id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  codigoPostal?: string
  catalogoPais?: CatalogoPaisEntity
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoLocalidadEntitytMapperService extends MapedorService<CatalogoLocalidadEntity, CatalogoLocalidadModel> {

  constructor(
    private catalogoPaisEntityMapper: CatalogoPaisEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoLocalidadEntity): CatalogoLocalidadModel {
    if (entity) {
      const model: CatalogoLocalidadModel = {}

      if (entity.id) {
        model.id = entity.id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.codigoPostal) {
        model.codigoPostal = entity.codigoPostal
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }

      if (entity.catalogoPais) {
        model.pais = this.catalogoPaisEntityMapper.transform(entity.catalogoPais)
      }
      return model
    }
    return null
  }
}
