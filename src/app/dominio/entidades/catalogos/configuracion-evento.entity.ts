import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoEventoEntity } from "dominio/entidades/catalogos/catalogo-evento.entity";
import { FormulaEventoEntity } from "dominio/entidades/catalogos/formula-evento.entity";
export interface ConfiguracionEventoEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity, // CatalogoEstado
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  intervalo?: number,
  duracion?: number,
  ciclico?: boolean
  catalogoEvento?: CatalogoEventoEntity,
  formulas?: Array<FormulaEventoEntity>
}
