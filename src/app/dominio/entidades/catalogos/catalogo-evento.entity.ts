import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ConfiguracionEventoEntity } from "dominio/entidades/catalogos/configuracion-evento.entity";
import { FormulaEventoEntity } from 'dominio/entidades/catalogos/formula-evento.entity';
export interface CatalogoEventoEntity{
    _id?:string,
    codigo?:string,
    estado?:CatalogoEstadoEntity //Catalogo estados
    fechaCreacion?:Date,
    fechaActualizacion?:Date,
    nombre?:string,
    configuraciones?:Array<ConfiguracionEventoEntity>,
    formulas?:Array<FormulaEventoEntity>
}
