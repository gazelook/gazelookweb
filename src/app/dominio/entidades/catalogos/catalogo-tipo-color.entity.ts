import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoColorModel } from 'dominio/modelo/catalogos/catalogo-tipo-color.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoTipoColorEntity {
  _id: string,
  codigo: string,
  estado: CatalogoEstadoEntity, // CatalogoEstado
  nombre: string,
  formula: string,
  fechaCreacion: Date,
  fechaActualizacion: Date
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoColorMapperService extends MapedorService<CatalogoTipoColorEntity, CatalogoTipoColorModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoTipoColorEntity): CatalogoTipoColorModel {
    if (entity) {
      const model: CatalogoTipoColorModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.estado) {
        model.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.formula) {
        model.formula = entity.formula
      }
      return model
    }
    return null
  }
}
