import { UsuarioEntity} from 'dominio/entidades';

export interface CoinPaymentsRatesEntity {
  isFiat?: number;
  rateBtc?: string;
  lastUpdate?: string;
  txFee?: string;
  status?: Status;
  name?: string;
  confirms?: string;
  canConvert?: number;
  capabilities?: Capability[];
  explorer?: string;
  accepted?: number;
  currency?: string;
}

export enum Capability {
  Convert = 'convert',
  DestTag = 'dest_tag',
  Payments = 'payments',
  Transfers = 'transfers',
  Wallet = 'wallet',
}

export enum Status {
  Online = 'online',
}

export interface CoinPaymentesResponse {
  coinpayments?: Coinpayments;
  idTransaccion?: string;
  usuario?: UsuarioEntity;
  tokenAccess?: string;
  tokenRefresh?: string;
  idDispositivo?: string;
}

export interface Coinpayments {
  amount?: string;
  txn_id?: string;
  address?: string;
  confirms_needed?: string;
  timeout?: number;
  checkout_url?: string;
  status_url?: string;
  qrcode_url?: string;
}
