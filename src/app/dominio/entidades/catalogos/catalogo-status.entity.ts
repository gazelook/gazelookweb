import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoStatusModel } from 'dominio/modelo/catalogos/catalogo-status.model';
export interface CatalogoStatusEntity {
  _id?: String
  codigo?: string
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoStatusEntityMapperService extends MapedorService<CatalogoStatusEntity, CatalogoStatusModel> {
  protected map(entity: CatalogoStatusEntity): CatalogoStatusModel {
    if (entity) {
      return {
        codigo: entity.codigo,
        nombre: entity.nombre
      };
    }
    return null;
  }
}
