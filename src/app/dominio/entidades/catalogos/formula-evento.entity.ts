import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoEventoEntity } from 'dominio/entidades/catalogos/catalogo-evento.entity';
export interface FormulaEventoEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  formula?: string,
  descripcion?: string,
  catalogoEvento?: CatalogoEventoEntity
  prioridad?: number
}
