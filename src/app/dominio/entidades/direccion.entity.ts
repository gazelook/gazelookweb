import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { DireccionModel } from 'dominio/modelo/entidades/direccion.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CatalogoLocalidadEntity, CatalogoLocalidadEntitytMapperService } from "dominio/entidades/catalogos/catalogo-localidad.entity";
import { CatalogoPaisEntity, CatalogoPaisEntityMapperService } from 'dominio/entidades/catalogos/catalogo-pais.entity';
export interface DireccionEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  latitud?: number,
  longitud?: number,
  traducciones?: Array<TraduccionDireccionEntity>,
  localidad?: CatalogoLocalidadEntity,
  pais?: CatalogoPaisEntity
}
export interface TraduccionDireccionEntity {
  id?: string,
  descripcion?: string,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean
}
@Injectable({ providedIn: 'root' })
export class DireccionEntityMapperService extends MapedorService<DireccionEntity, DireccionModel> {
  constructor(
    private catalogoLocalidadEntityMapper: CatalogoLocalidadEntitytMapperService,
    private catalogoPaisEntityMapperService: CatalogoPaisEntityMapperService
  ) {
    super();
  }

  protected map(entity: DireccionEntity): DireccionModel {
    if (entity) {
      const model: DireccionModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.traducciones) {

        if (entity.traducciones.length === 1) {
          model.descripcion = entity.traducciones[0].descripcion
          model.descripcionOriginal = entity.traducciones[0].descripcion
        }

        if (entity.traducciones.length > 1) {
          // Original
          const indexOriginal = entity.traducciones.findIndex(e => e.original)
          if (indexOriginal >= 0) {
            model.descripcionOriginal = entity.traducciones[indexOriginal].descripcion
          }

          // Idioma
          const indexNoOriginal = entity.traducciones.findIndex(e => !(e.original))
          if (indexNoOriginal >= 0) {
            model.descripcion = entity.traducciones[indexNoOriginal].descripcion
          }
        }
      }

      if (entity.localidad) {
        model.localidad = this.catalogoLocalidadEntityMapper.transform(entity.localidad)
      }

      if (entity.latitud) {
        model.latitud = entity.latitud
      }

      if (entity.longitud) {
        model.longitud = entity.longitud
      }

      if (entity.pais) {
        model.pais = this.catalogoPaisEntityMapperService.transform(entity.pais)
      }
      return model
    }
    return null
  }
}
