import { Injectable } from '@angular/core';
import { CatalogoTipoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { MapedorService } from '@core/base/mapeador.interface';
import { MensajeModel } from 'dominio/modelo/entidades/mensaje.model';
import { CatalogoEntidadEntity, CatalogoEntidadMapperService } from 'dominio/entidades/catalogos/catalogo-entidad.entity';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { CatalogoMensajeEntity, CatalogoMensajeEntityMapperService } from 'dominio/entidades/catalogos/catalogo-mensaje.entity';
import { ConversacionEntity, ConversacionEntityMapperServiceParaMensajeEntity } from "dominio/entidades/conversacion.entity";
import { MediaEntity, MediaEntityMapperService } from "dominio/entidades/media.entity";
import { ParticipanteAsociacionEntity, ParticipanteAsociacionMapperService } from 'dominio/entidades/participante-asociacion.entity';
export interface MensajeEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  traducciones?: Array<TraduccionMensajeEntity>
  tipo?: CatalogoMensajeEntity //CatalogoMensaje
  importante?: boolean
  adjuntos?: Array<MediaEntity> //Media
  conversacion?: ConversacionEntity
  propietario?: ParticipanteAsociacionEntity //Participante Asociacion
  entregadoA?: Array<ParticipanteAsociacionEntity>
  eliminadoPor?: Array<ParticipanteAsociacionEntity>
  leidoPor?: Array<ParticipanteAsociacionEntity>
  listaIdConversacion?: Array<ConversacionEntity>
  identificadorTemporal?: number
  estatus?: CatalogoMensajeEntity,
  entidadCompartida?: CatalogoEntidadEntity,
  referenciaEntidadCompartida?: any,
  idConversacionAgora?: string
  infoOtroParticipanteLlamada?: any
}

export interface TraduccionMensajeEntity {
  id?: string,
  contenido?: string,
  idioma?: CatalogoIdiomaEntity, // CatalogoIdioma
  original?: boolean
}

@Injectable({ providedIn: 'root' })
//llega entidad envio PensamientoModel
export class MensajeMapperService extends MapedorService<MensajeEntity, MensajeModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private participanteAsociacionMapperService: ParticipanteAsociacionMapperService,
    private catalogoMensajeEntityMapperService: CatalogoMensajeEntityMapperService,
    private conversacionEntityMapperServiceParaMensajeEntity: ConversacionEntityMapperServiceParaMensajeEntity,
    private catalogoEntidadMapperService: CatalogoEntidadMapperService

  ) {
    super();
  }

  protected map(entity: MensajeEntity): MensajeModel {
    if (entity) {
      const mensajeModel: MensajeModel = {}

      if (entity._id) {
        mensajeModel.id = entity._id
      }
      if (entity.estado) {
        mensajeModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        mensajeModel.contenido = entity.traducciones[0].contenido
      }

      if (entity.importante) {
        mensajeModel.importante = entity.importante
      }

      if (entity.adjuntos && entity.adjuntos.length > 0) {
        mensajeModel.adjuntos = this.mediaEntityMapperService.transform(entity.adjuntos)
      }

      if (entity.fechaCreacion) {
        mensajeModel.fechaCreacion = entity.fechaCreacion
      }

      if (entity.conversacion) {
        mensajeModel.conversacion = this.conversacionEntityMapperServiceParaMensajeEntity.transform(entity.conversacion)
      }

      if (entity.propietario) {
        mensajeModel.propietario = this.participanteAsociacionMapperService.transform(entity.propietario)
      }
      if (entity.entregadoA) {
        mensajeModel.entregadoA = this.participanteAsociacionMapperService.transform(entity.entregadoA)
      }
      if (entity.eliminadoPor) {
        mensajeModel.eliminadoPor = this.participanteAsociacionMapperService.transform(entity.eliminadoPor)
      }
      if (entity.leidoPor) {
        mensajeModel.leidoPor = this.participanteAsociacionMapperService.transform(entity.leidoPor)
      }

      if (entity.tipo) {
        mensajeModel.tipo = this.catalogoMensajeEntityMapperService.transform(entity.tipo)
      }

      if (entity.identificadorTemporal) {
        mensajeModel.identificadorTemporal = entity.identificadorTemporal
      }

      if (entity.estatus) {
        mensajeModel.estatus = this.catalogoMensajeEntityMapperService.transform(entity.estatus)
      }

      if (entity.referenciaEntidadCompartida) {
        mensajeModel.referenciaEntidadCompartida = entity.referenciaEntidadCompartida
      }

      if (entity.entidadCompartida) {
        mensajeModel.entidadCompartida = this.catalogoEntidadMapperService.transform(entity.entidadCompartida)
      }

      if (entity.listaIdConversacion && (entity.tipo.codigo === CatalogoTipoMensaje.VIDEOLLAMADA || entity.tipo === CatalogoTipoMensaje.LLAMADA)) {

        if (entity.listaIdConversacion[0]._id) {
          mensajeModel.idConversacionAgora = entity.listaIdConversacion[0]._id
        }
      }

      if (entity.listaIdConversacion && entity.listaIdConversacion.length > 0 && entity.tipo.codigo === CatalogoTipoMensaje.VIDEOLLAMADA) {
        mensajeModel.idConversacionAgora = entity.listaIdConversacion[0]._id
      }

      if (entity.listaIdConversacion && entity.listaIdConversacion.length > 0 && entity.tipo.codigo === CatalogoTipoMensaje.LLAMADA) {
        mensajeModel.idConversacionAgora = entity.listaIdConversacion[0]._id
      }

      if (entity.idConversacionAgora) {
        mensajeModel.idConversacionAgora = entity.idConversacionAgora
      }

      if (entity.infoOtroParticipanteLlamada) {
        mensajeModel.infoOtroParticipanteLlamada = entity.infoOtroParticipanteLlamada
      }
      return mensajeModel
    }
    return null
  }
}
