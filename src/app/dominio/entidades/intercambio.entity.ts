import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoProyectoModel } from 'dominio/modelo/catalogos/catalogo-tipo-proyecto.model';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { AlbumEntity, AlbumEntityMapperService } from 'dominio/entidades/album.entity';
import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-evento-notificacion';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CatalogoStatusEntity, CatalogoStatusEntityMapperService } from 'dominio/entidades/catalogos/catalogo-status.entity';
import { CatalogoTipoIntercambioEntity, CatalogoTipoIntercambioEntityMapperService } from 'dominio/entidades/catalogos/catalogo-tipo-intercambio.entity';
import { ComentarioEntity, ComentarioEntityMapperService } from 'dominio/entidades/comentario.entity';
import { DireccionEntity, DireccionEntityMapperService } from 'dominio/entidades/direccion.entity';
import { EstrategiaEntity } from 'dominio/entidades/estrategia.entity';
import { MediaEntity, MediaEntityMapperService } from 'dominio/entidades/media.entity';
import { PerfilEntity, PerfilEntityGeneralMapperService, PerfilEntityMapperService, PerfilResumenEntityMapper } from './perfil.entity';

export interface IntercambioEntity {
  _id?: string;
  estado?: CatalogoEstadoEntity;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  totalVotos?: number;
  actualizado?: boolean
  tipo?: CatalogoTipoIntercambioEntity;
  tipoIntercambiar?: CatalogoTipoProyectoModel,
  perfil?: PerfilEntity;
  adjuntos?: Array<AlbumEntity>;
  direccion?: DireccionEntity,
  traducciones?: Array<TraduccionIntercambioEntity>;
  estrategia?: EstrategiaEntity;
  comentarios?: Array<ComentarioEntity>;
  medias?: Array<MediaEntity>;
  email?: string;
  status?: CatalogoStatusEntity
}

export interface TraduccionIntercambioEntity {
  _id?: string;
  titulo?: string;
  tituloCorto?: string;
  descripcion?: string;
  tituloOriginal?: string;
  tituloCortoOriginal?: string;
  descripcionOriginal?: string;
  idioma?: CatalogoIdiomaEntity;
  original?: boolean;
}
@Injectable({ providedIn: 'root' })
export class IntercambioEntityMapperService extends MapedorService<IntercambioEntity, IntercambioModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
    private perfilEntityMapperService: PerfilEntityMapperService,
    private albumEntityMapperService: AlbumEntityMapperService,
    private direccionEntityMapperService: DireccionEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private comentarioEntityMapperService: ComentarioEntityMapperService,
    private catalogoTipoIntercambioEntityMapperService: CatalogoTipoIntercambioEntityMapperService,
    private catalogoStatusEntityMapperService: CatalogoStatusEntityMapperService,
    private perfilEntityGeneralMapperService: PerfilEntityGeneralMapperService,
    private perfilResumenEntityMapper: PerfilResumenEntityMapper

  ) {
    super()
  }

  protected map(entity: IntercambioEntity): IntercambioModel {
    if (entity) {
      const model: IntercambioModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoIntercambioEntityMapperService.transform(entity.tipo)
      }
      if (entity.tipoIntercambiar) {
        model.tipoIntercambiar = this.catalogoTipoIntercambioEntityMapperService.transform(entity.tipoIntercambiar)
      }

      if (entity.perfil) {
        model.perfil = this.perfilResumenEntityMapper.transform(entity.perfil)
      }

      if (entity.status) {
        model.status = this.catalogoStatusEntityMapperService.transform(entity.status)
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        for (const traduccion of entity.traducciones) {
          if (traduccion.original && entity.traducciones.length > 1) {
            model.descripcionOriginal = traduccion.descripcion;
            model.tituloOriginal = traduccion.titulo;
            model.tituloCortoOriginal = traduccion.tituloCorto;

          }
          if (!traduccion.original || entity.traducciones.length === 1) {
            model.descripcion = traduccion.descripcion;
            model.titulo = traduccion.titulo;
            model.tituloCorto = traduccion.tituloCorto;
          }
        }
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.adjuntos) {
        model.adjuntos = this.albumEntityMapperService.transform(entity.adjuntos)
      }

      if (entity.direccion) {
        model.direccion = this.direccionEntityMapperService.transform(entity.direccion)
      }

      if (entity.comentarios) {
        model.comentarios = this.comentarioEntityMapperService.transform(entity.comentarios)
      }

      if (entity.email) {
        model.email = entity.email
      }
      return model;
    }
    return null;
  }
}
@Injectable({ providedIn: 'root' })
export class IntercambioEntityMapperServiceParaComentarios extends MapedorService<IntercambioEntity, IntercambioModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
    private catalogoTipoProyectoEntityMapperService: CatalogoTipoIntercambioEntityMapperService,
  ) {
    super()
  }

  protected map(entity: IntercambioEntity): IntercambioModel {
    if (entity) {
      const model: IntercambioModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoProyectoEntityMapperService.transform(entity.tipo)
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        model.descripcion = entity.traducciones[0].descripcion;
        model.titulo = entity.traducciones[0].titulo;
        model.tituloCorto = entity.traducciones[0].tituloCorto;
      }
      return model;
    }
    return null;
  }
}
