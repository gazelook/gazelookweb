import { CatalogoConfiguracionAnuncioSistemaEntity, CatalogoConfiguracionAnuncioSistemaMapperService } from 'dominio/entidades/catalogos/catalogo-configuracion-anuncio-sistema.entity';
import { CatalogoTipoAnuncioSistemaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-anuncio-sistema.model';
import { CatalogoTipoAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-anuncio-sistema.entity';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { AnuncioSistemaModel } from 'dominio/modelo/entidades/anuncio-sistema.model';

export interface AnuncioSistemaEntity {
    _id?: string
    estado?: CatalogoEstadoEntity
    fechaCreacion?: Date
    fechaActualizacion?: Date
    traducciones?: Array<TraduccionAnuncioSistemaEntity>
    tematica?: Array<TraduccionAnuncioSistemaEntity>
    tipo?: CatalogoTipoAnuncioSistemaEntity,
    fechaInicio?: Date
    fechaFin?: Date
    configuraciones?: Array<CatalogoConfiguracionAnuncioSistemaEntity>
    intervaloVisualizacion?: number
}

export interface TraduccionAnuncioSistemaEntity {
    _id?: string;
    titulo?: string;
    descripcion?: string;
    idioma?: CatalogoIdiomaEntity;
}

@Injectable({ providedIn: 'root' })
export class AnuncioSistemaMapperService extends MapedorService<AnuncioSistemaEntity, AnuncioSistemaModel> {

    constructor(
        private estadoEntityMapper: CatalogoEstadoEntityMapperService,
        private tipoAnuncioSistemaMapper: CatalogoTipoAnuncioSistemaModelMapperService,
        private catalogoConfiguracionAnuncioSistemaMapperService: CatalogoConfiguracionAnuncioSistemaMapperService
    ) {
        super();
    }

    protected map(entity: AnuncioSistemaEntity): AnuncioSistemaModel {
        if (entity) {
            let model: AnuncioSistemaModel = {}

            if (entity._id) model.id = entity._id
            if (entity.estado) model.estado = this.estadoEntityMapper.transform(entity.estado)
            if (entity.tipo) model.tipo = this.tipoAnuncioSistemaMapper.transform(entity.tipo)
            if (entity.configuraciones) model.configuraciones = entity.configuraciones
            if (entity.intervaloVisualizacion) model.intervaloVisualizacion = entity.intervaloVisualizacion
            if (entity.fechaInicio) model.fechaInicio = entity.fechaInicio
            if (entity.fechaFin) model.fechaInicio = entity.fechaInicio
            if (entity.traducciones && entity.traducciones.length > 0) {
                model.descripcion = entity.traducciones[0].descripcion;
                model.titulo = entity.traducciones[0].titulo;
            }
            if (entity.tematica && entity.tematica.length > 0) {
                model.tematica = entity.tematica[0].titulo;
            }

            return model
        }

        return null
    }

}