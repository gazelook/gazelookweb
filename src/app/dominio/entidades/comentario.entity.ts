import { PerfilEntity, PerfilEntityMapperService, PerfilEntityMapperServicePerfil } from 'dominio/entidades/perfil.entity';
import { ProyectoEntity, ProyectoEntityMapperServiceParaComentarios } from 'dominio/entidades/proyecto.entity';
import { ComentarioFirebaseModel, ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { Injectable } from '@angular/core';
import { ParticipanteProyectoEntity, ParticipanteProyectoEntityMapperService } from "dominio/entidades/participante-proyecto.entity";
import { CatalogoTipoComentarioEntityMapperService, CatalogoTipoComentarioEntity } from "dominio/entidades/catalogos/catalogo-tipo-comentario.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "./catalogos/catalogo-estado.entity";
import { MediaEntity, MediaEntityMapperService } from "dominio/entidades/media.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { MapedorService } from '@core/base/mapeador.interface';
export interface ComentarioEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    coautor?: ParticipanteProyectoEntity,
    adjuntos?: Array<MediaEntity>,
    importante?: boolean,
    traducciones?: Array<TraduccionComentarioEntity>,
    tipo?: CatalogoTipoComentarioEntity,
    proyecto?: ProyectoEntity,
    idPerfilRespuesta?: PerfilEntity
}
export interface ComentarioFirebaseEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity,
    fechaCreacionFirebase?: number,
    coautor?: ParticipanteProyectoEntity,
    adjuntos?: Array<MediaEntity>,
    importante?: boolean,
    tipo?: CatalogoTipoComentarioEntity,
    proyecto?: ProyectoEntity,
    idPerfilRespuesta?: PerfilEntity,
    traducciones?: Array<TraduccionComentarioEntity>,
    idProyecto?: string
}
export interface TraduccionComentarioEntity {
    _id?: string
    texto?: string
    idioma?: CatalogoIdiomaEntity
    original?: boolean
}
@Injectable({ providedIn: 'root' })
export class ComentarioEntityMapperService extends MapedorService<ComentarioEntity, ComentarioModel> {

    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
        private participanteProyectoMapperService: ParticipanteProyectoEntityMapperService,
        private mediaEntityMapperService: MediaEntityMapperService,
        private catalogoTipoComentarioEntityMapperService: CatalogoTipoComentarioEntityMapperService,
        private proyectoEntityMapperServiceParaComentarios: ProyectoEntityMapperServiceParaComentarios,
        private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil
    ) {
        super()
    }

    protected map(entity: ComentarioEntity): ComentarioModel {
        if (entity) {

            const model: ComentarioModel = {}

            // tipo?: CatalogoTipoComentarioEntity

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.fechaCreacion) {
                model.fechaCreacion = entity.fechaCreacion
            }

            if (entity.fechaActualizacion) {
                model.fechaActualizacion = entity.fechaActualizacion
            }

            if (entity.coautor) {
                model.coautor = this.participanteProyectoMapperService.transform(entity.coautor)
            }

            if (entity.adjuntos) {
                model.adjuntos = this.mediaEntityMapperService.transform(entity.adjuntos)
            }

            if (entity.importante) {
                model.importante = entity.importante
            }

            if (entity.traducciones) {
                model.traducciones = entity.traducciones
            }

            if (entity.tipo) {
                model.tipo = this.catalogoTipoComentarioEntityMapperService.transform(entity.tipo)
            }

            if (entity.proyecto) {
                model.proyecto = this.proyectoEntityMapperServiceParaComentarios.transform(entity.proyecto)
            }

            if (entity.idPerfilRespuesta) {
                model.idPerfilRespuesta = this.perfilEntityMapperServicePerfil.transform(entity.idPerfilRespuesta)
            }
            return model
        }
        return null
    }
}

@Injectable({ providedIn: 'root' })
export class ComentarioFirebaseEntityMapperService extends MapedorService<ComentarioFirebaseEntity, ComentarioFirebaseModel> {

    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
        private participanteProyectoMapperService: ParticipanteProyectoEntityMapperService,
        private mediaEntityMapperService: MediaEntityMapperService,
        private catalogoTipoComentarioEntityMapperService: CatalogoTipoComentarioEntityMapperService,
        private proyectoEntityMapperServiceParaComentarios: ProyectoEntityMapperServiceParaComentarios,
        private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil
    ) {
        super()
    }

    protected map(entity: ComentarioFirebaseEntity): ComentarioFirebaseModel {
        if (entity) {

            const model: ComentarioFirebaseModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.fechaCreacionFirebase) {
                model.fechaCreacionFirebase = entity.fechaCreacionFirebase
            }

            if (entity.coautor) {
                model.coautor = this.participanteProyectoMapperService.transform(entity.coautor)
            }

            if (entity.adjuntos) {
                model.adjuntos = this.mediaEntityMapperService.transform(entity.adjuntos)
            }

            if (entity.importante) {
                model.importante = entity.importante
            }

            if (entity.traducciones) {
                model.traducciones = entity.traducciones
            }

            if (entity.tipo) {
                model.tipo = this.catalogoTipoComentarioEntityMapperService.transform(entity.tipo)
            }

            if (entity.proyecto) {
                model.proyecto = this.proyectoEntityMapperServiceParaComentarios.transform(entity.proyecto)
            }

            if (entity.idPerfilRespuesta) {
                model.idPerfilRespuesta = this.perfilEntityMapperServicePerfil.transform(entity.idPerfilRespuesta)
            }
            if (entity.idProyecto) {
                model.idProyecto = entity.idProyecto
                
              }
            return model
        }
        return null
    }
}
