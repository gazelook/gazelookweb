import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { IniciarSesionModel } from 'dominio/modelo/iniciar-sesion.model';
import { TokenEntity } from 'dominio/entidades/token.entity';
import { UsuarioEntity, UsuarioEntityMapperService } from 'dominio/entidades/usuario.entity';
export interface IniciarSesionEntity extends TokenEntity {
  usuario: UsuarioEntity
}
@Injectable({ providedIn: 'root' })
export class IniciarSesionMapperService extends MapedorService<IniciarSesionEntity, IniciarSesionModel> {
  constructor
    (
      private usuarioMapper: UsuarioEntityMapperService
    ) {
    super();
  }

  protected map(entity: IniciarSesionEntity): IniciarSesionModel {
    return {
      tokenAccess: entity.tokenAccess,
      tokenRefresh: entity.tokenRefresh,
      usuario: this.usuarioMapper.transform(entity.usuario)
    };
  }
}
