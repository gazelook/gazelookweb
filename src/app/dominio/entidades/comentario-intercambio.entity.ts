import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ComentarioIntercambioFirebaseModel, ComentarioIntercambioModel } from 'dominio/modelo/entidades/comentario-intercambio.model';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "./catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { CatalogoTipoComentarioEntity, CatalogoTipoComentarioEntityMapperService } from "dominio/entidades/catalogos/catalogo-tipo-comentario.entity";
import { IntercambioEntity, IntercambioEntityMapperServiceParaComentarios } from 'dominio/entidades/intercambio.entity';
import { MediaEntity, MediaEntityMapperService } from "dominio/entidades/media.entity";
import { ParticipanteIntercambioEntity, ParticipanteIntercambioEntityMapperService } from 'dominio/entidades/participante-intercambio.entity';
import { PerfilEntity, PerfilEntityMapperServicePerfil } from 'dominio/entidades/perfil.entity';
export interface ComentarioIntercambioEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  coautor?: ParticipanteIntercambioEntity,
  adjuntos?: Array<MediaEntity>,
  importante?: boolean,
  traducciones?: Array<TraduccionComentarioEntity>,
  tipo?: CatalogoTipoComentarioEntity,
  intercambio?: IntercambioEntity,
  idPerfilRespuesta?: PerfilEntity
}
export interface ComentarioIntercambioFirebaseEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacionFirebase?: number,
  coautor?: ParticipanteIntercambioEntity,
  adjuntos?: Array<MediaEntity>,
  importante?: boolean,
  tipo?: CatalogoTipoComentarioEntity,
  intercambio?: IntercambioEntity,
  idPerfilRespuesta?: PerfilEntity,
  traducciones?: Array<TraduccionComentarioEntity>,
}
export interface TraduccionComentarioEntity {
  _id?: string
  texto?: string
  idioma?: CatalogoIdiomaEntity
  original?: boolean
}
@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioEntityMapperService extends MapedorService<ComentarioIntercambioEntity, ComentarioIntercambioModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private participanteIntercambioEntityMapperService: ParticipanteIntercambioEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private catalogoTipoComentarioEntityMapperService: CatalogoTipoComentarioEntityMapperService,
    private intercambioEntityMapperServiceParaComentarios: IntercambioEntityMapperServiceParaComentarios,
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil
  ) {
    super()
  }

  protected map(entity: ComentarioIntercambioEntity): ComentarioModel {
    if (entity) {

      const model: ComentarioModel = {}

      // tipo?: CatalogoTipoComentarioEntity

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.coautor) {
        model.coautor = this.participanteIntercambioEntityMapperService.transform(entity.coautor)
      }

      if (entity.adjuntos) {
        model.adjuntos = this.mediaEntityMapperService.transform(entity.adjuntos)
      }

      if (entity.importante) {
        model.importante = entity.importante
      }

      if (entity.traducciones) {
        model.traducciones = entity.traducciones
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoComentarioEntityMapperService.transform(entity.tipo)
      }

      if (entity.intercambio) {
        model.proyecto = this.intercambioEntityMapperServiceParaComentarios.transform(entity.intercambio)
      }

      if (entity.idPerfilRespuesta) {
        model.idPerfilRespuesta = this.perfilEntityMapperServicePerfil.transform(entity.idPerfilRespuesta)
      }
      return model
    }
    return null
  }
}
@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioFirebaseEntityMapperService extends MapedorService<ComentarioIntercambioFirebaseEntity, ComentarioIntercambioFirebaseModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private catalogoTipoComentarioEntityMapperService: CatalogoTipoComentarioEntityMapperService,
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil,
    private participanteIntercambioEntityMapperService: ParticipanteIntercambioEntityMapperService,
    private intercambioEntityMapperServiceParaComentarios: IntercambioEntityMapperServiceParaComentarios,
  ) {
    super()
  }

  protected map(entity: ComentarioIntercambioFirebaseEntity): ComentarioIntercambioFirebaseModel {
    if (entity) {

      const model: ComentarioIntercambioFirebaseModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.fechaCreacionFirebase) {
        model.fechaCreacionFirebase = entity.fechaCreacionFirebase
      }

      if (entity.coautor) {
        model.coautor = this.participanteIntercambioEntityMapperService.transform(entity.coautor)
      }

      if (entity.adjuntos) {
        model.adjuntos = this.mediaEntityMapperService.transform(entity.adjuntos)
      }

      if (entity.importante) {
        model.importante = entity.importante
      }

      if (entity.traducciones) {
        model.traducciones = entity.traducciones
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoComentarioEntityMapperService.transform(entity.tipo)
      }

      if (entity.intercambio) {
        model.intercambio = this.intercambioEntityMapperServiceParaComentarios.transform(entity.intercambio)
      }

      if (entity.idPerfilRespuesta) {
        model.idPerfilRespuesta = this.perfilEntityMapperServicePerfil.transform(entity.idPerfilRespuesta)
      }
      return model
    }
    return null
  }
}
