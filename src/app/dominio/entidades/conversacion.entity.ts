import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { ConversacionModel } from 'dominio/modelo/entidades/conversacion.model';
import { MensajeEntity, MensajeMapperService } from "dominio/entidades/mensaje.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { AsociacionEntity, AsociacionEntityMapperService } from "dominio/entidades/asociacion.entity";
export interface ConversacionEntity {
  _id?: string
  estado?: CatalogoEstadoEntity //CatalogoEstado
  fechaCreacion?: Date
  fechaActualizacion?: Date
  asociacion?: AsociacionEntity //Asociacion
  ultimoMensaje?: MensajeEntity //Mensaje
  mensajes?: Array<MensajeEntity> //Mensaje
  listaUltimoMensaje?: MensajeEntity //Mensaje
}
@Injectable({ providedIn: 'root' })
export class ConversacionEntityMapperService extends MapedorService<ConversacionEntity, ConversacionModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private asociacionEntityMapperService: AsociacionEntityMapperService,
    private mensajeMapperService: MensajeMapperService
  ) {
    super()
  }

  protected map(entity: ConversacionEntity): ConversacionModel {

    if (entity) {
      const conversacionModel: ConversacionModel = {}
      if (entity._id) {
        conversacionModel.id = entity._id
      }

      if (entity.asociacion) {
        conversacionModel.asociacion = this.asociacionEntityMapperService.transform(entity.asociacion)
      }

      if (entity.ultimoMensaje) {
        conversacionModel.ultimoMensaje = this.mensajeMapperService.transform(entity.ultimoMensaje)
      }
      return conversacionModel
    }
    return null
  }
}
@Injectable({ providedIn: 'root' })
export class ConversacionEntityMapperServiceParaMensajeEntity extends MapedorService<ConversacionEntity, ConversacionModel> {

  constructor(
  ) {
    super()
  }

  protected map(entity: ConversacionEntity): ConversacionModel {
    if (entity) {
      const conversacionModel: ConversacionModel = {}
      if (entity._id) {
        conversacionModel.id = entity._id
      }
      return conversacionModel
    }
    return null
  }
}
