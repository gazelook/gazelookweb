import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { RolSistemaModel } from 'dominio/modelo/entidades/rol-sistema.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoRolEntity, CatalogoRolEntityMapperService } from "dominio/entidades/catalogos/catalogo-rol.entity";
import { RolEntidadEntity } from "dominio/entidades/rol-entidad.entity";
export interface RolSistemaEntity {
  _id: string
  estado: CatalogoEstadoEntity
  fechaCreacion: Date
  fechaActualizacion: Date
  rolesEspecificos: RolEntidadEntity
  rol: CatalogoRolEntity
  nombre: string
}
@Injectable({ providedIn: 'root' })
export class RolSistemaEntityMapperService extends MapedorService<RolSistemaEntity, RolSistemaModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private rolSistemaEntityMapperService: RolSistemaEntityMapperService,
    private catalogoRolEntityMapperService: CatalogoRolEntityMapperService,
  ) {
    super()
  }

  protected map(entity: RolSistemaEntity): RolSistemaModel {
    if (entity) {
      const rolModel: RolSistemaModel = {}

      if (entity._id) {
        rolModel.id = entity._id
      }

      if (entity.estado) {
        rolModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.nombre) {
        rolModel.nombre = entity.nombre
      }

      if (entity.rol) {
        rolModel.rol = this.catalogoRolEntityMapperService.transform(entity.rol)
      }
      return rolModel
    }
    return null
  }
}
