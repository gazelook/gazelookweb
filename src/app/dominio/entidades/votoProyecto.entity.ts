import { VotoProyectoModel } from 'dominio/modelo/entidades/voto-proyecto.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { ProyectoEntity } from "dominio/entidades/proyecto.entity";
import { PerfilEntity } from "dominio/entidades/perfil.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoVotoEntity, CatalogoTipoVotoEntityMapperService } from "dominio/entidades/catalogos/catalogo-tipo-voto.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { ConfiguracionEventoEntity } from "dominio/entidades/catalogos/configuracion-evento.entity";
export interface VotoProyectoEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    proyecto?: ProyectoEntity,
    perfil?: PerfilEntity,
    traducciones?: Array<TraduccionVotoProyectoEntity>,
    tipo?: CatalogoTipoVotoEntity,
    numeroVoto?: number,
    configuracion?: ConfiguracionEventoEntity
}
export interface TraduccionVotoProyectoEntity {
    _id?: string
    descripcion?: string
    idioma?: CatalogoIdiomaEntity //Catalogo Idioma
    original?: boolean
}
@Injectable({ providedIn: 'root' })
export class VotoProyectoEntityMapperService extends MapedorService<VotoProyectoEntity, VotoProyectoModel> {

	constructor(
        private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
        private catalogoTipoVotoEntityMapperService: CatalogoTipoVotoEntityMapperService,
	) {
		super()
	}

	protected map(entity: VotoProyectoEntity): VotoProyectoModel {
		if (entity) {
			const model: VotoProyectoModel = {}

			if (entity._id) {
				model.id = entity._id
			}

			if (entity.estado) {
				model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
            }

            if (entity.fechaCreacion) {
                model.fechaCreacion = entity.fechaCreacion
            }

            if (entity.fechaActualizacion) {
                model.fechaActualizacion = entity.fechaActualizacion
            }

			if (entity.proyecto) {
                // proyecto?: ProyectoEntity,
            }

            if (entity.perfil) {
                // perfil?: PerfilEntity,
            }

            if (entity.traducciones && entity.traducciones.length > 0) {
                model.descripcion = entity.traducciones[0].descripcion
            }

            if (entity.tipo) {
                model.tipo = this.catalogoTipoVotoEntityMapperService.transform(entity.tipo)
            }

            if (entity.numeroVoto) {
                model.numeroVoto = entity.numeroVoto
            }

            if (entity.configuracion) {
                // configuracion?: ConfiguracionEventoEntity
            }
			return model;
		}
		return null;
	}
}
