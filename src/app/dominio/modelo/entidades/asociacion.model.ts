import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoAsociacionModel } from "dominio/modelo/catalogos/catalogo-tipo-asociacion.model";
import { ConversacionModel } from "dominio/modelo/entidades/conversacion.model";
import { ParticipanteAsociacionModel } from "dominio/modelo/entidades/participante-asociacion.model";
export interface AsociacionModel {
  id?: string,
  estado?: CatalogoEstadoModel, // CatalogoEstado
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  tipo?: CatalogoTipoAsociacionModel, // CatalogoTipoAsociacion
  nombre?: string,
  //foto: MediaModel,
  participantes?: Array<ParticipanteAsociacionModel>, // Participante
  conversacion?: ConversacionModel, // Conversacion
  privado?: boolean
}
