import { UsoDelEstiloModel } from './uso-del-estilo.model';
import { MediaModel } from 'src/app/dominio/modelo/entidades/media.model';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoEstiloModel } from "dominio/modelo/catalogos/catalogo-estilo.model";
import { CatalogoColorModel } from "dominio/modelo/catalogos/catalogo-color.model";
export interface EstiloModel {
    id?: string
    estado?: CatalogoEstadoModel //CatalogoEstado
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
    media?: MediaModel
    color?: CatalogoColorModel
    tipo?: CatalogoEstiloModel
    usoDelEstilo?: UsoDelEstiloModel
}

