import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { AnuncioSistemaEntity, TraduccionAnuncioSistemaEntity } from 'dominio/entidades/anuncio-sistema.entity';
import { CatalogoConfiguracionAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-configuracion-anuncio-sistema.entity';
import { CatalogoConfiguracionAnuncioSistemaModel, CatalogoConfiguracionAnuncioSistemaModelMapperService } from './../catalogos/catalogo-configuracion-anuncio-sistema';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { CatalogoTipoAnuncioSistemaModel, CatalogoTipoAnuncioSistemaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-anuncio-sistema.model';

export interface AnuncioSistemaModel {
    id?: string,
    estado?: CatalogoEstadoModel,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    titulo?: string,
    descripcion?: string,
    tematica?: string,
    tipo?: CatalogoTipoAnuncioSistemaModel,
    fechaInicio?: Date,
    fechaFin?: Date,
    configuraciones?: Array<CatalogoConfiguracionAnuncioSistemaModel>,
    intervaloVisualizacion?: number
}

@Injectable({ providedIn: 'root' })
export class AnuncioSistemaModelMapperService extends MapedorService<AnuncioSistemaModel, AnuncioSistemaEntity> {

    constructor(
        private estadoMapperService: CatalogoEstadoModelMapperService,
        private catalogoTipoAnuncioSistemaModelMapperService: CatalogoTipoAnuncioSistemaModelMapperService,
        private catalogoConfiguracionAnuncioSistemaModelMapperService: CatalogoConfiguracionAnuncioSistemaModelMapperService
    ) {
        super()
    }

    protected map(model: AnuncioSistemaModel): AnuncioSistemaEntity {
        let entity: AnuncioSistemaEntity = {}

        if (model.id) entity._id = model.id
        if (model.estado) entity.estado = this.estadoMapperService.transform(model.estado)
        if (model.intervaloVisualizacion) entity.intervaloVisualizacion = model.intervaloVisualizacion
        if (model.fechaCreacion) entity.fechaCreacion = model.fechaCreacion
        if (model.fechaActualizacion) entity.fechaActualizacion = model.fechaActualizacion
        if (model.tipo) entity.tipo = this.catalogoTipoAnuncioSistemaModelMapperService.transform(model.tipo)
        if (model.fechaInicio) entity.fechaInicio = model.fechaInicio
        if (model.fechaFin) entity.fechaFin = model.fechaFin
        if (model.configuraciones) {
            let lista:Array<CatalogoConfiguracionAnuncioSistemaEntity> = [];
            for (const config of model.configuraciones) {
                lista.push(this.catalogoConfiguracionAnuncioSistemaModelMapperService.transform(config));
            }
            model.configuraciones = lista;
        }

        entity.traducciones = []
        let traduccion: TraduccionAnuncioSistemaEntity = {}
        if (model.titulo) traduccion.titulo = model.titulo
        if (model.descripcion) traduccion.descripcion = model.descripcion
        if (traduccion.titulo || traduccion.descripcion) entity.traducciones.push(traduccion)

        return entity
    }

}