import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { NoticiaModel } from "dominio/modelo/entidades/noticia.model";
import { PerfilModel } from "dominio/modelo/entidades/perfil.model";
import { VotoNoticiaEntity } from 'dominio/entidades/voto-noticia.entity';
import { CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { NoticiaModelMapperService } from 'dominio/modelo/entidades/noticia.model';
import { PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';
export interface VotoNoticiaModel {
  id?: string
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date
  fechaActualizacion?: Date
  perfil?: PerfilModel
  noticia?: NoticiaModel
  descripcion?: string
}
@Injectable({ providedIn: 'root' })
export class VotoNoticiaModelMapperService extends MapedorService<VotoNoticiaModel, VotoNoticiaEntity> {

  constructor(
    private catalogoEstadoModelMapper: CatalogoEstadoModelMapperService,
    private noticiaModelMapperService: NoticiaModelMapperService,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: VotoNoticiaModel): VotoNoticiaEntity {
    if (model) {
      const entity: VotoNoticiaEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapper.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.noticia) {
        entity.noticia = this.noticiaModelMapperService.transform(model.noticia)
      }

      if (model.perfil) {
        entity.perfil = this.perfilModelMapperService.transform(model.perfil)
      }

      entity.traducciones = []

      if (model.descripcion) {

        entity.traducciones.push({
          descripcion: model.descripcion
        })
      }
      return entity;
    }
    return null;
  }
}
