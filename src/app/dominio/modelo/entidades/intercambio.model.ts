import { Injectable } from "@angular/core";
import { MapedorService } from "@core/base/mapeador.interface";
import { IntercambioEntity } from 'dominio/entidades/intercambio.entity';
import { TraduccionProyectoEntity } from 'dominio/entidades/proyecto.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoProyectoModel } from "dominio/modelo/catalogos/catalogo-tipo-proyecto.model";
import { CatalogoEstatusModelMapperService, CatalogoStatusModel } from 'dominio/modelo/catalogos/catalogo-status.model';
import { CatalogoTipoIntercambioModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { AlbumModel, AlbumModelMapperService } from 'dominio/modelo/entidades/album.model';
import { ComentarioModel, ComentarioModelMapperService } from "dominio/modelo/entidades/comentario.model";
import { DireccionModel, DireccionModelMapperService } from 'dominio/modelo/entidades/direccion.model';
import { MediaModel, MediaModelMapperService } from 'dominio/modelo/entidades/media.model';
import { PerfilModel, PerfilModelEstadoMapperService } from "dominio/modelo/entidades/perfil.model";
export interface IntercambioModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  actualizado?: boolean,
  tipo?: CatalogoTipoProyectoModel,
  tipoIntercambiar?: CatalogoTipoProyectoModel,
  perfil?: PerfilModel,
  adjuntos?: Array<AlbumModel>,
  direccion?: DireccionModel,
  comentarios?: Array<ComentarioModel>,
  titulo?: string,
  tituloCorto?: string,
  descripcion?: string,
  original?: boolean,
  tituloOriginal?: string,
  tituloCortoOriginal?: string,
  descripcionOriginal?: string,
  medias?: Array<MediaModel>,
  email?: string,
  status?: CatalogoStatusModel
}
@Injectable({ providedIn: 'root' })
export class IntercambioModelMapperService extends MapedorService<IntercambioModel, IntercambioEntity> {

  constructor(
    private estadoMapperService: CatalogoEstadoModelMapperService,
    private catalogoTipoProyectoModelMapperService: CatalogoTipoIntercambioModelMapperService,
    private perfilModelEstadoMapperService: PerfilModelEstadoMapperService,
    private direccionModelMapperService: DireccionModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private comentarioModelMapperService: ComentarioModelMapperService,
    private albumModelMapperService: AlbumModelMapperService,
    private catalogoEstatusModelMapperService: CatalogoEstatusModelMapperService
  ) {
    super()
  }

  protected map(model: IntercambioModel): IntercambioEntity {
    const entity: IntercambioEntity = {}

    if (model.id) {
      entity._id = model.id
    }

    if (model.estado) {
      entity.estado = this.estadoMapperService.transform(model.estado)
    }

    if (model.fechaCreacion) {
      entity.fechaCreacion = model.fechaCreacion
    }

    if (model.fechaActualizacion) {
      entity.fechaActualizacion = model.fechaActualizacion
    }

    if (model.tipo) {
      entity.tipo = this.catalogoTipoProyectoModelMapperService.transform(model.tipo)
    }

    if (model.tipoIntercambiar) {
      entity.tipoIntercambiar = this.catalogoTipoProyectoModelMapperService.transform(model.tipoIntercambiar)
    }

    if (model.perfil) {
      entity.perfil = this.perfilModelEstadoMapperService.transform(model.perfil)
    }

    if (model.adjuntos) {
      entity.adjuntos = this.albumModelMapperService.transform(model.adjuntos)
    }

    if (model.direccion) {
      entity.direccion = this.direccionModelMapperService.transform(model.direccion)
    }

    if (model.status) {
      entity.status = this.catalogoEstatusModelMapperService.transform(model.status)
    }

    if (model.comentarios) {
      entity.comentarios = this.comentarioModelMapperService.transform(model.comentarios)
    }

    entity.traducciones = []
    const traduccion: TraduccionProyectoEntity = {}
    if (model.titulo) {
      traduccion.titulo = model.titulo
    }
    if (model.tituloCorto) {
      traduccion.tituloCorto = model.tituloCorto
    }
    if (model.descripcion) {
      traduccion.descripcion = model.descripcion
    }
    if (traduccion.titulo || traduccion.tituloCorto || traduccion.descripcion) {
      entity.traducciones.push(traduccion)
    }

    if (model.email) {
      entity.email = model.email
    }
    return entity
  }
}
@Injectable({ providedIn: 'root' })
export class IntercambioStatusModelMapperService extends MapedorService<IntercambioModel, IntercambioEntity> {

  constructor(

    private perfilModelEstadoMapperService: PerfilModelEstadoMapperService,
  ) {
    super()
  }

  protected map(model: IntercambioModel): IntercambioEntity {
    const entity: IntercambioEntity = {}

    if (model.id) {
      entity._id = model.id
    }

    if (model.perfil) {
      entity.perfil = this.perfilModelEstadoMapperService.transform(model.perfil)
    }
    return entity
  }
}

@Injectable({ providedIn: 'root' })
export class IntercambioModelMapperServiceParaComentarios extends MapedorService<IntercambioModel, IntercambioEntity> {

  constructor() { super(); }

  protected map(model: IntercambioModel): IntercambioEntity {
    if (model) {
      const entity: IntercambioEntity = {}

      if (model.id) {
        entity._id = model.id
      }
      return entity
    }
    return null
  }
}
