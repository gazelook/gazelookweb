import { MapedorService } from '@core/base/mapeador.interface';
import {
  PerfilModel,
  PerfilModelEstadoMapperService,
} from 'dominio/modelo/entidades/perfil.model';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { ConfiguracionEstiloModel } from 'dominio/modelo/entidades/configuracion-estilo.model';
import { RolEntidadModel } from 'dominio/modelo/entidades/rol-entidad.model';

import { Injectable } from '@angular/core';
import { ParticipanteAsociacionEntity } from 'dominio/entidades/participante-asociacion.entity';
export interface ParticipanteAsociacionModel {
  id?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  roles?: Array<RolEntidadModel>;
  invitadoPor?: ParticipanteAsociacionModel;
  configuraciones?: Array<ConfiguracionEstiloModel>;
  perfil?: PerfilModel;
  sobrenombre?: string;
  asociacion?: AsociacionModel;
  contactoDe?: PerfilModel;
}

@Injectable({ providedIn: 'root' })
export class ParticipanteModelMapperResultadoBusqueda extends MapedorService<
  ParticipanteAsociacionModel,
  ParticipanteAsociacionEntity
> {
  constructor(
    private perfilModelEstadoMapperService: PerfilModelEstadoMapperService
  ) {
    super();
  }
  protected map(
    model: ParticipanteAsociacionModel
  ): ParticipanteAsociacionEntity {
    return {
      _id: model.id,
      perfil: this.perfilModelEstadoMapperService.transform(model.perfil),
    };
  }
}
