import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoVotoModel, CatalogoTipoVotoModelMapperService } from "dominio/modelo/catalogos/catalogo-tipo-voto.model";
import { ConfiguracionEventoModel } from "dominio/modelo/catalogos/configuracion-evento.model";
import { PerfilModel } from "dominio/modelo/entidades/perfil.model";
import { ProyectoModel } from "dominio/modelo/entidades/proyecto.model";
import { VotoProyectoEntity } from 'dominio/entidades/votoProyecto.entity';
import { PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModelMapperService } from 'dominio/modelo/entidades/proyecto.model';

export interface VotoProyectoModel {
  id?: string,
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  proyecto?: ProyectoModel,
  perfil?: PerfilModel,
  tipo?: CatalogoTipoVotoModel,
  numeroVoto?: number,
  configuracion?: ConfiguracionEventoModel,
  descripcion?: string
}
@Injectable({ providedIn: 'root' })
export class VotoProyectoModelMapperService extends MapedorService<VotoProyectoModel, VotoProyectoEntity> {

  constructor(
    private catalogoEstadoModelMapper: CatalogoEstadoModelMapperService,
    private catalogoTipoVotoModelMapperService: CatalogoTipoVotoModelMapperService,
    private proyectoModelMapperService: ProyectoModelMapperService,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: VotoProyectoModel): VotoProyectoEntity {
    if (model) {
      const entity: VotoProyectoEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapper.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.proyecto) {
        entity.proyecto = this.proyectoModelMapperService.transform(model.proyecto)
      }

      if (model.perfil) {
        entity.perfil = this.perfilModelMapperService.transform(model.perfil)
      }

      entity.traducciones = []
      if (model.descripcion) {
        entity.traducciones.push({
          descripcion: model.descripcion
        })
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoVotoModelMapperService.transform(model.tipo)
      }

      if (model.numeroVoto) {
        entity.numeroVoto = model.numeroVoto
      }

      if (model.configuracion) {
        // configuracion?: ConfiguracionEventoEntity
      }
      return entity;
    }
    return null;
  }
}
