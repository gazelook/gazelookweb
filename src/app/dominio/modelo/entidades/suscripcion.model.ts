import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { TransaccionModel } from "dominio/modelo/entidades/transaccion.model";
import { CatalogoSuscripcionModel } from "dominio/modelo/catalogos/catalogo-suscripcion.model";
export interface SuscripcionModel {
    id?: string,
    fechaCreacion?: Date,
    fechaRenovacion?: Date,
    estado?: CatalogoEstadoModel,
    tipo?: CatalogoSuscripcionModel,
    transacion?: TransaccionModel
}
