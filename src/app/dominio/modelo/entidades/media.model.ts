import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { MediaEntity } from 'dominio/entidades/media.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { CatalogoMediaModel, CatalogoMediaModelMapperService } from 'dominio/modelo/catalogos/catalogo-media.model';
import { ArchivoModel, ArchivoModelMapperService } from 'dominio/modelo/entidades/archivo.model';
export interface MediaModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  catalogoMedia?: CatalogoMediaModel,
  principal?: ArchivoModel,
  miniatura?: ArchivoModel,
  enlace?: string,
  descripcion?: string,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
}
@Injectable({ providedIn: 'root' })
export class MediaModelMapperService extends MapedorService<MediaModel, MediaEntity> {
  constructor(
    private estadoModelMapper: CatalogoEstadoModelMapperService,
    private archivoModelMapper: ArchivoModelMapperService,
    private catalogoMediaModelMapper: CatalogoMediaModelMapperService,
  ) {
    super()
  }

  protected map(model: MediaModel): MediaEntity {
    if (model) {
      const mediaEntity: MediaEntity = {}

      if (model.id) {
        mediaEntity._id = model.id
      }

      if (model.estado) {
        mediaEntity.estado = this.estadoModelMapper.transform(model.estado)
      }

      if (model.catalogoMedia) {
        mediaEntity.catalogoMedia = this.catalogoMediaModelMapper.transform(model.catalogoMedia)
      }

      if (model.principal) {
        mediaEntity.principal = this.archivoModelMapper.transform(model.principal)
      }

      if (model.miniatura) {
        mediaEntity.miniatura = this.archivoModelMapper.transform(model?.miniatura)
      }

      if (model.enlace) {
        mediaEntity.enlace = model.enlace
      }

      if (model.descripcion) {
        mediaEntity.traducciones = [
          {
            descripcion: model.descripcion
          }
        ]
      }
      return mediaEntity
    }
    return null;
  }
}

