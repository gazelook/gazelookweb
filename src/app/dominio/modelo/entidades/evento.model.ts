import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { ConfiguracionEventoModel } from "dominio/modelo/catalogos/configuracion-evento.model";
import { ProyectoModel } from "dominio/modelo/entidades/proyecto.model";
export interface EventoModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  fechaInicio?: Date,
  fechaFin?: Date,
  configuracionEvento?: ConfiguracionEventoModel,
  proyectos?: Array<ProyectoModel>
}
