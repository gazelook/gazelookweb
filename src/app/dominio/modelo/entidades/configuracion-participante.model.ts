import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { ParticipanteAsociacionModel } from "dominio/modelo/entidades/participante-asociacion.model";
import { ConfiguracionEstiloModel } from "dominio/modelo/entidades/configuracion-estilo.model";
export interface ConfiguracionEstiloParticipanteModel{
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    propietario?:ParticipanteAsociacionModel
    asignado?:ParticipanteAsociacionModel
    configuracionEstilo?:ConfiguracionEstiloModel
}
