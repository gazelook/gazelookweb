import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { DireccionEntity, TraduccionDireccionEntity } from 'dominio/entidades/direccion.entity';
import { CatalogoLocalidadMapperService, CatalogoLocalidadModel } from 'dominio/modelo/catalogos/catalogo-localidad.model';
import { CatalogoPaisModel, CatalogoPaisModelMapperService } from 'dominio/modelo/catalogos/catalogo-pais.model';
export interface DireccionModel {
  id?: string,
  estado?: CatalogoEstadoEntity,
  latitud?: number,
  longitud?: number,
  localidad?: CatalogoLocalidadModel,
  pais?: CatalogoPaisModel,
  descripcion?: string,
  descripcionOriginal?: string,
}
@Injectable({ providedIn: 'root' })
export class DireccionModelMapperService extends MapedorService<DireccionModel, DireccionEntity> {
  constructor
    (
      private traduccionDireccionMapper: TraduccionDireccionModelMapperService,
      private localidadMapper: CatalogoLocalidadMapperService,
      private catalogoPaisModelMapperService: CatalogoPaisModelMapperService

    ) {
    super();
  }

  protected map(model: DireccionModel): DireccionEntity {
    if (model) {
      const entity: DireccionEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.descripcion) {
        entity.traducciones = []
        entity.traducciones.push({
          descripcion: model.descripcion
        })
      }

      if (!model.descripcion) {
        entity.traducciones = []
        entity.traducciones.push({
            descripcion: ''
        })
    }

      if (model.localidad) {
        entity.localidad = this.localidadMapper.transform(model.localidad)
      }

      if (model.latitud) {
        entity.latitud = model.latitud
      }

      if (model.longitud) {
        entity.longitud = model.longitud
      }

      if (model.pais) {
        entity.pais = this.catalogoPaisModelMapperService.transform(model.pais)
      }
      return entity
    }
    return null
  }

}
@Injectable({ providedIn: 'root' })
export class TraduccionDireccionModelMapperService extends MapedorService<DireccionModel, TraduccionDireccionEntity> {
  constructor( ) { super(); }

  protected map(model: DireccionModel): TraduccionDireccionEntity {
    if (model) {
      return {
        descripcion: model.descripcion
      };
    }
    return null;
  }
}
