import { Injectable } from '@angular/core';
import { MapedorService } from "@core/base/mapeador.interface";
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { ParticipanteIntercambioEntity } from 'dominio/entidades/participante-intercambio.entity';
import { ComentarioModel } from "dominio/modelo/entidades/comentario.model";
import { ConfiguracionEstiloModel } from "dominio/modelo/entidades/configuracion-estilo.model";
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { PerfilModel, PerfilModelMapperService } from "dominio/modelo/entidades/perfil.model";
import { RolEntidadModel } from "dominio/modelo/entidades/rol-entidad.model";

export interface ParticipanteIntercambioModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  roles?: Array<RolEntidadModel>,
  configuraciones?: Array<ConfiguracionEstiloModel>,
  comentarios?: Array<ComentarioModel>,
  intercambio?: IntercambioModel,
  coautor?: PerfilModel,
  totalComentarios?: number
}

@Injectable({ providedIn: 'root' })
export class ParticipanteIntercambioModelMapperService extends MapedorService<ParticipanteIntercambioModel, ParticipanteIntercambioEntity> {

  constructor(
    private catalogoEstadoMapperService: CatalogoEstadoModelMapperService,
    private perfilModelMapperService: PerfilModelMapperService
  ) {
    super()
  }

  protected map(model: ParticipanteIntercambioModel): ParticipanteIntercambioEntity {

    const entity: ParticipanteIntercambioEntity = {}

    if (model.id) {
      entity._id = model.id
    }

    if (model.estado) {
      entity.estado = this.catalogoEstadoMapperService.transform(model.estado)
    }

    if (model.fechaCreacion) {
      entity.fechaCreacion = model.fechaCreacion
    }

    if (model.fechaActualizacion) {
      entity.fechaActualizacion = model.fechaActualizacion
    }

    if (model.roles) {
      // roles?: Array<RolEntidadModel>,

    }

    if (model.configuraciones) {
      // configuraciones?: Array<ConfiguracionEstiloModel>,
    }

    if (model.comentarios) {
      // comentarios?: Array<ComentarioModel>,
    }

    if (model.intercambio) {
    }

    if (model.coautor) {
      entity.coautor = this.perfilModelMapperService.transform(model.coautor)
    }

    if (model.totalComentarios) {
      // totalComentarios?: number
    }

    return entity
  }
}
