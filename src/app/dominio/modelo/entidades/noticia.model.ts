import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { VotoNoticiaModel } from 'dominio/modelo/entidades/voto-noticia.model';
import { ItemResultadoBusqueda } from 'dominio/modelo/item-resultado-busqueda';
import {
  NoticiaEntity,
  TraduccionNoticiaEntity,
} from 'dominio/entidades/noticia.entity';
import { CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import {
  AlbumModel,
  AlbumModelMapperService,
} from 'dominio/modelo/entidades/album.model';
import {
  DireccionModel,
  DireccionModelMapperService,
} from 'dominio/modelo/entidades/direccion.model';
import {
  MediaModel,
  MediaModelMapperService,
} from 'dominio/modelo/entidades/media.model';
import { PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';

export interface NoticiaModel {
  id?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  direccion?: DireccionModel;
  autor?: string;
  adjuntos?: Array<AlbumModel>;
  perfil?: PerfilModel;
  votos?: Array<VotoNoticiaModel>;
  totalVotos?: number;
  tituloCorto?: string;
  titulo?: string;
  descripcion?: string;
  tags?: Array<string>;
  medias?: Array<MediaModel>;
  voto?: boolean;
  actualizado?: boolean;
  tituloOriginal?: string;
  tituloCortoOriginal?: string;
  descripcionOriginal?: string;
}
@Injectable({ providedIn: 'root' })
export class NoticiaModelMapperResultadoBusqueda extends MapedorService<
  NoticiaModel,
  ItemResultadoBusqueda
> {
  protected map(entity: NoticiaModel): ItemResultadoBusqueda {
    return {
      titulo: entity.tituloCorto,
      subtitulo: entity.titulo,
      tipo: CodigosCatalogoEntidad.PROYECTO,
    };
  }
}
@Injectable({ providedIn: 'root' })
export class NoticiaModelMapperService extends MapedorService<
  NoticiaModel,
  NoticiaEntity
> {
  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private direccionModelMapperService: DireccionModelMapperService,
    private albumModelMapperService: AlbumModelMapperService,
    private perfilModelMapperService: PerfilModelMapperService,
    private mediaModelMapperService: MediaModelMapperService
  ) {
    super();
  }

  protected map(model: NoticiaModel): NoticiaEntity {
    if (model) {
      const entity: NoticiaEntity = {};

      if (model.id) {
        entity._id = model.id;
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(
          model.estado
        );
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion;
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion;
      }

      if (model.direccion) {
        entity.direccion = this.direccionModelMapperService.transform(
          model.direccion
        );
      }

      if (model.autor) {
        entity.autor = model.autor;
      }

      if (model.adjuntos) {
        entity.adjuntos = this.albumModelMapperService.transform(
          model.adjuntos
        );
      }

      if (model.perfil) {
        entity.perfil = this.perfilModelMapperService.transform(model.perfil);
      }

      if (model.votos) {
      }

      if (model.totalVotos) {
        entity.totalVotos = model.totalVotos;
      }

      entity.traducciones = [];

      if (model.tituloCorto || model.titulo || model.descripcion) {
        const traduccion: TraduccionNoticiaEntity = {};

        if (model.tituloCorto) {
          traduccion.tituloCorto = model.tituloCorto;
        }

        if (model.titulo) {
          traduccion.titulo = model.titulo;
        }

        if (model.descripcion) {
          traduccion.descripcion = model.descripcion;
        }

        entity.traducciones.push(traduccion);
      }

      if (model.tags) {
      }

      if (model.medias) {
        entity.medias = this.mediaModelMapperService.transform(model.medias);
      }

      entity.voto = model.voto;
      entity.actualizado = model.actualizado;
      return entity;
    }
    return null;
  }
}
