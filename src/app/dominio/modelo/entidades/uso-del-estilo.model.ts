import { UsoDelEstiloEntity } from 'dominio/entidades/uso-del-estilo.entity';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
export interface UsoDelEstiloModel {
    id?: string,
    estado?: CatalogoEstadoModel,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    codigo?: string,
    nombre?: string,
    descripcion?: string
}
@Injectable({ providedIn: 'root' })
export class UsoDelEstiloModelMapperService extends MapedorService<UsoDelEstiloModel, UsoDelEstiloEntity> {

    constructor(
        private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    ) {
        super()
    }

    protected map(model: UsoDelEstiloModel): UsoDelEstiloEntity {

        if (model) {
            const entity: UsoDelEstiloEntity = {}

            if (model.id) {
                entity._id = model.id
            }

            if (model.estado) {
                entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.nombre) {
                entity.nombre = model.nombre
            }

            if (model.descripcion) {
                entity.descripcion = model.descripcion
            }
            return entity
        }
        return null
    }

}
