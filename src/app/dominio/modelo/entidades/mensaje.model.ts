import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';
import {
  CatalogoMensajeModel,
  CatalogoMensajeModelMapperService,
} from 'dominio/modelo/catalogos/catalogo-tipo-mensaje.model';
import { LocalStorage } from '@core/servicios/locales/local-storage.service';
import {
  MensajeEntity,
  TraduccionMensajeEntity,
} from 'dominio/entidades/mensaje.entity';
import {
  CatalogoEntidadModel,
  CatalogoEntidadModelMapperService,
} from 'dominio/modelo/catalogos/catalogo-entidad.model';
import {
  ConversacionModel,
  ConversacionModelMapperService,
  EntrarConversacionModelMapperService,
} from './conversacion.model';
import {
  MediaModel,
  MediaModelMapperService,
} from 'dominio/modelo/entidades/media.model';
import {
  ParticipanteAsociacionModel,
  ParticipanteModelMapperResultadoBusqueda,
} from 'dominio/modelo/entidades/participante-asociacion.model';
export interface MensajeModel {
  id?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  tipo?: CatalogoMensajeModel;
  importante?: boolean;
  adjuntos?: Array<MediaModel>;
  conversacion?: ConversacionModel;
  propietario?: ParticipanteAsociacionModel;
  contenido?: string;
  entregadoA?: Array<ParticipanteAsociacionModel>;
  eliminadoPor?: Array<ParticipanteAsociacionModel>;
  leidoPor?: Array<ParticipanteAsociacionModel>;
  listaIdConversacion?: Array<ConversacionModel>;
  identificadorTemporal?: number;
  estatus?: CatalogoMensajeModel;
  entidadCompartida?: CatalogoEntidadModel;
  referenciaEntidadCompartida?: any;
  idConversacionAgora?: string;
  infoOtroParticipanteLlamada?: any;
}
export interface MensajeModelFirebase {
  id?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: any;
  fechaActualizacion?: any;
  tipo?: CatalogoMensajeModel;
  importante?: boolean;
  adjuntos?: Array<MediaModel>;
  conversacion?: ConversacionModel;
  propietario?: ParticipanteAsociacionModel;
  contenido?: string;
  entregadoA?: Array<ParticipanteAsociacionModel>;
  eliminadoPor?: Array<ParticipanteAsociacionModel>;
  leidoPor?: Array<ParticipanteAsociacionModel>;
  listaIdConversacion?: Array<ConversacionModel>;
  identificadorTemporal?: number;
  estatus?: CatalogoMensajeModel;
  entidadCompartida?: CatalogoEntidadModel;
  referenciaEntidadCompartida?: any;
  idConversacionAgora?: string;
  infoOtroParticipanteLlamada?: any;
  esLocal?: boolean;
}
@Injectable({ providedIn: 'root' })
export class MensajeModelMapperService extends MapedorService<
  MensajeModel,
  MensajeEntity
> {
  constructor(
    private localStorage: LocalStorage,
    private mediaModelMapper: MediaModelMapperService,
    private conversacionModelMapperService: ConversacionModelMapperService,
    private participanteModelMapperResultadoBusqueda: ParticipanteModelMapperResultadoBusqueda,
    private entrarConversacionModelMapperService: EntrarConversacionModelMapperService,
    private catalogoMensajeModelMapperService: CatalogoMensajeModelMapperService,
    private catalogoEntidadModelMapperService: CatalogoEntidadModelMapperService
  ) {
    super();
  }

  protected map(model: MensajeModel): MensajeEntity {
    if (model) {
      const entity: MensajeEntity = {};

      if (model.listaIdConversacion) {
        entity.listaIdConversacion =
          this.conversacionModelMapperService.transform(
            model.listaIdConversacion
          );
      }

      if (model.propietario) {
        entity.propietario =
          this.participanteModelMapperResultadoBusqueda.transform(
            model.propietario
          );
      }

      const traduccion: TraduccionMensajeEntity = {};
      if (model.contenido) {
        entity.traducciones = [];
        traduccion.contenido = model.contenido;
      }

      if (model.contenido) {
        traduccion.idioma = {
          codNombre: this.localStorage.obtenerIdiomaLocal().codNombre,
        };
      }

      if (model.contenido) {
        entity.traducciones.push(traduccion);
      }

      if (model.adjuntos) {
        entity.adjuntos = this.mediaModelMapper.transform(model.adjuntos);
      }

      if (model.identificadorTemporal) {
        entity.identificadorTemporal = model.identificadorTemporal;
      }

      if (model.id) {
        entity._id = model.id;
      }

      if (model.tipo) {
        entity.tipo = this.catalogoMensajeModelMapperService.transform(
          model.tipo
        );
      }

      if (model.conversacion) {
        entity.conversacion =
          this.entrarConversacionModelMapperService.transform(
            model.conversacion
          );
      }

      if (model.entidadCompartida) {
        entity.entidadCompartida =
          this.catalogoEntidadModelMapperService.transform(
            model.entidadCompartida
          );
      }

      if (model.referenciaEntidadCompartida) {
        entity.referenciaEntidadCompartida = model.referenciaEntidadCompartida;
      }

      if (model.estatus) {
        entity.estatus = this.catalogoMensajeModelMapperService.transform(
          model.estatus
        );
      }

      if (model.infoOtroParticipanteLlamada) {
        entity.infoOtroParticipanteLlamada = model.infoOtroParticipanteLlamada;
      }
      return entity;
    }
    return null;
  }
}
