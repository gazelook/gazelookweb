import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { ConfiguracionEstiloModel } from "dominio/modelo/entidades/configuracion-estilo.model";
import { PerfilModel, PerfilModelMapperService } from "dominio/modelo/entidades/perfil.model";
import { ComentarioModel } from "dominio/modelo/entidades/comentario.model";
import { RolEntidadModel } from "dominio/modelo/entidades/rol-entidad.model";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ParticipanteProyectoEntity } from 'dominio/entidades/participante-proyecto.entity';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';

export interface ParticipanteProyectoModel {
    id?: string,
    estado?: CatalogoEstadoModel,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    roles?: Array<RolEntidadModel>,
    configuraciones?: Array<ConfiguracionEstiloModel>,
    comentarios?: Array<ComentarioModel>,
    proyecto?: ProyectoModel,
    coautor?: PerfilModel,
    totalComentarios?: number
}

@Injectable({ providedIn: 'root' })
export class ParticipanteProyectoModelMapperService extends MapedorService<ParticipanteProyectoModel, ParticipanteProyectoEntity> {

    constructor(
        private catalogoEstadoMapperService: CatalogoEstadoModelMapperService,
        private perfilModelMapperService: PerfilModelMapperService
    ) {
        super()
    }

    protected map(model: ParticipanteProyectoModel): ParticipanteProyectoEntity {

        const entity: ParticipanteProyectoEntity = {}

        if (model.id) {
            entity._id = model.id
        }

        if (model.estado) {
            entity.estado = this.catalogoEstadoMapperService.transform(model.estado)
        }

        if (model.fechaCreacion) {
            entity.fechaCreacion = model.fechaCreacion
        }

        if (model.fechaActualizacion) {
            entity.fechaActualizacion = model.fechaActualizacion
        }

        if (model.roles) {
            // roles?: Array<RolEntidadModel>,

        }

        if (model.configuraciones) {
            // configuraciones?: Array<ConfiguracionEstiloModel>,
        }

        if (model.comentarios) {
            // comentarios?: Array<ComentarioModel>,
        }

        if (model.proyecto) {
            // proyecto?: ProyectoModel,
        }

        if (model.coautor) {
            entity.coautor = this.perfilModelMapperService.transform(model.coautor)
        }

        if (model.totalComentarios) {
            // totalComentarios?: number
        }

        return entity
    }
}
