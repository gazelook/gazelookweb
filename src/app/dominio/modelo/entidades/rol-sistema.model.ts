import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { RolEntidadModel } from "dominio/modelo/entidades/rol-entidad.model";
import { CatalogoRolModel } from "dominio/modelo/catalogos/catalogo-rol.model";
export interface RolSistemaModel{
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    rolesEspecificos?:RolEntidadModel
    rol?:CatalogoRolModel
    nombre?:string
}
