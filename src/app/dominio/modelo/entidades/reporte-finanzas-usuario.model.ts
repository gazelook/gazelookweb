import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ReporteFinanzasUsuarioEntity } from 'dominio/entidades/reporte-finanzas-usuario.entity';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';

export interface valores {
  monto?: number;
  fechaCreacion?: Date;
  moneda?: string;
  montoUSD?: number;
  montoEUR?: number;
}

export interface ReporteFinanzasUsuarioModel {
  aportacionesSuscripcion?: number;
  aportacionesValorExtra?: Array<valores>;
  aportacionesDonacionProyectos?: Array<valores>;
  moneda?: CatalogoTipoMonedaModel;
  aportaciones?: number;
}

@Injectable({ providedIn: 'root' })
export class ReporteFinanzasUsuarioMapperService extends MapedorService<
  ReporteFinanzasUsuarioModel,
  ReporteFinanzasUsuarioEntity
> {
  constructor() {
    super();
  }

  protected map(
    model: ReporteFinanzasUsuarioModel
  ): ReporteFinanzasUsuarioEntity {
    if (model) {
      const entity: ReporteFinanzasUsuarioEntity = {};
      return entity;
    }
    return null;
  }
}
