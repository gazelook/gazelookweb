import { Injectable } from "@angular/core";
import { MapedorService } from "@core/base/mapeador.interface";
import { NotificacionEntity } from "dominio/entidades/notificacion.entity";
import { CatalogoEventoNotificacionModel, CatalogoEventoNotificacionModelMapperService } from 'dominio/modelo/catalogos/catalogo-evento-notificacion';
import { CatalogoEntidadModel, CatalogoEntidadModelMapperService } from 'dominio/modelo/catalogos/catalogo-entidad.model';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { PerfilModel, PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';

export interface NotificacionModel {
  fechaCreacion?: Date
  fechaActualizacion?: Date
  id?: string
  entidad?: CatalogoEntidadModel
  evento?: CatalogoEventoNotificacionModel
  data?: any
  listaPerfiles?: Array<PerfilModel>
  estado?: CatalogoEstadoModel,
  accion?: CatalogoEventoNotificacionModel
}

@Injectable({ providedIn: 'root' })
export class NotificacionModelMapperService extends MapedorService<NotificacionModel, NotificacionEntity> {
  constructor
    (
      private estadoMapper: CatalogoEstadoModelMapperService,
      private catalogoEntidadModelMapperService: CatalogoEntidadModelMapperService,
      private catalogoEventoNotificacionModelMapperService: CatalogoEventoNotificacionModelMapperService,
      private perfilModelMapperService: PerfilModelMapperService

    ) { super(); }

  protected map(model: NotificacionModel): NotificacionEntity {

    if (model) {
      const entity: NotificacionEntity = {}
      if (model.id) {
        entity.id = model.id
      }

      if (model.entidad) {
        entity.entidad = this.catalogoEntidadModelMapperService.transform(model.entidad)
      }

      if (model.evento) {
        entity.evento = this.catalogoEventoNotificacionModelMapperService.transform(model.evento)
      }

      if (model.data) {
        entity.data = model.data
      }

      if (model.listaPerfiles) {
        entity.listaPerfiles = this.perfilModelMapperService.transform(model.listaPerfiles)
      }

      if (model.estado) {
        entity.estado = this.estadoMapper.transform(model.estado)
      }
      return entity
    }
    return null;
  }
}
