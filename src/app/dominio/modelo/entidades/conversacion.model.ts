import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ConversacionEntity } from 'dominio/entidades/conversacion.entity';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { AsociacionModel } from "dominio/modelo/entidades/asociacion.model";
import { MensajeModel } from "dominio/modelo/entidades/mensaje.model";
export interface ConversacionModel {
  id?: string
  estado?: CatalogoEstadoModel //CatalogoEstado
  fechaCreacion?: Date
  fechaActualizacion?: Date
  asociacion?: AsociacionModel //Asociacion
  ultimoMensaje?: MensajeModel //Mensaje
  mensajes?: Array<MensajeModel> //Mensaje
  listaUltimoMensaje?: MensajeModel
}
@Injectable({ providedIn: 'root' })
export class ConversacionModelMapperService extends MapedorService<ConversacionModel, ConversacionEntity> {
  constructor ( ) { super(); }

  protected map(model: ConversacionModel): ConversacionEntity {
    if (model) {
      const entity: ConversacionEntity = {}

      if (model.id) {
        entity._id = model.id
      }
      return entity
    }
    return null;
  }
}

@Injectable({ providedIn: 'root' })
export class EntrarConversacionModelMapperService extends MapedorService<ConversacionModel, ConversacionEntity> {
  constructor( ) { super();}

  protected map(model: ConversacionModel): ConversacionEntity {
    if (model) {
      const entity: ConversacionEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.asociacion) {
        entity.asociacion = {
          _id: model.asociacion.id
        }
      }
      return entity
    }
    return null;
  }
}
