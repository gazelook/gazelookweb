import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-anuncio-sistema.entity';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';

export interface CatalogoTipoAnuncioSistemaModel {
  id?: string;
  codigo?: string;
  nombre?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoAnuncioSistemaModelMapperService extends MapedorService<
  CatalogoTipoAnuncioSistemaModel,
  CatalogoTipoAnuncioSistemaEntity
> {
  protected map(
    model: CatalogoTipoAnuncioSistemaModel
  ): CatalogoTipoAnuncioSistemaEntity {
    if (model) {
      let entity: CatalogoTipoAnuncioSistemaEntity = {};

      if (model.id) entity._id = model.id;
      if (model.codigo) entity.codigo = model.codigo;
      if (model.nombre) entity.nombre = model.nombre;

      return entity;
    }
    return null;
  }
}
