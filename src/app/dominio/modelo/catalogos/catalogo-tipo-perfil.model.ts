import { Injectable } from '@angular/core';
import { MapedorService } from 'src/app/nucleo/base/mapeador.interface';
import { CatalogoTipoPerfilEntity } from 'dominio/entidades/catalogos/catalogo-tipo-perfil.entity';
import { PerfilResumenModel } from 'dominio/modelo/entidades/perfil-resumen.model';
import { PerfilModel } from "dominio/modelo/entidades/perfil.model";

export interface CatalogoTipoPerfilModel {
  codigo?: string,
  nombre?: string,
  descripcion?: string
  mostrarDescripcion?: boolean
  perfil?: PerfilResumenModel | PerfilModel
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoPerfilModelMapperService extends MapedorService<CatalogoTipoPerfilModel, CatalogoTipoPerfilEntity> {

  constructor(
    //private perfilMaper: PerfilModelMapperService
  ) {
    super()
  }

  protected map(model: CatalogoTipoPerfilModel,): CatalogoTipoPerfilEntity {
    return {
      codigo: model.codigo,
      // perfil: this.perfilMaper.transform(model.perfil)
    };
  }
}
