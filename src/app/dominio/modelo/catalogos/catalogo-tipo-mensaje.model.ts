import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoMensajeEntity } from 'dominio/entidades/catalogos/catalogo-mensaje.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoMensajeModel {
  id?: string
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoMensajeModelMapperService extends MapedorService<CatalogoMensajeModel, CatalogoMensajeEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService
  ) {
    super()
  }

  protected map(model: CatalogoMensajeModel): CatalogoMensajeEntity {
    if (model) {
      const entity: CatalogoMensajeEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.codigo) {
        entity.codigo = model.codigo
      }
      if (model.nombre) {
        entity.nombre = model.nombre
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }
      return entity
    }
    return null
  }
}
