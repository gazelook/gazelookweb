import { Injectable } from '@angular/core';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { MapedorService } from "@core/base/mapeador.interface";
import { CatalogoTipoIntercambioEntity, TraduccionCatalogoTipoIntercambioEntity } from "../../entidades/catalogos/catalogo-tipo-intercambio.entity";
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "./catalogo-estado.model";
export interface CatalogoTipoIntercambioModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  nombre?: string,
  descripcion?: string
}
export interface TraduccionCatalogoTipoIntercambioModel {
  _id?: string,
  nombre?: string,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean,
  descripcion?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoIntercambioModelMapperService extends MapedorService<CatalogoTipoIntercambioModel, CatalogoTipoIntercambioEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService
  ) {
    super()
  }

  protected map(model: CatalogoTipoIntercambioModel): CatalogoTipoIntercambioEntity {
    const entity: CatalogoTipoIntercambioEntity = {}

    if (model.id) {
      entity._id = model.id
    }

    if (model.codigo) {
      entity.codigo = model.codigo
    }

    if (model.estado) {
      entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
    }

    if (model.fechaCreacion) {
      entity.fechaCreacion = model.fechaCreacion
    }

    if (model.fechaActualizacion) {
      entity.fechaActualizacion = model.fechaActualizacion
    }

    entity.traducciones = []
    const traduccion: TraduccionCatalogoTipoIntercambioEntity = {}
    if (model.nombre) {
      traduccion.nombre = model.nombre
    }
    if (model.descripcion) {
      traduccion.descripcion = model.descripcion
    }
    if (traduccion.nombre || traduccion.descripcion) {
      entity.traducciones.push(traduccion)
    }
    return entity
  }
}
