import { CatalogoEstiloAnuncioSistemaModel, CatalogoEstiloAnuncioSistemaModelMapperService } from 'dominio/modelo/catalogos/catalogo-estilo-anuncio-sistema.model';
import { CatalogoEstiloAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-estilo-anuncio-sistema';
import { CatalogoConfiguracionAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-configuracion-anuncio-sistema.entity';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';

export interface CatalogoConfiguracionAnuncioSistemaModel {
    estilos?: Array<CatalogoEstiloAnuncioSistemaModel>
}

@Injectable({ providedIn: 'root' })
export class CatalogoConfiguracionAnuncioSistemaModelMapperService extends MapedorService<CatalogoConfiguracionAnuncioSistemaModel, CatalogoConfiguracionAnuncioSistemaEntity> {

    constructor(
        private catalogoEstiloAnuncioSistemaModelMapperService: CatalogoEstiloAnuncioSistemaModelMapperService
    ) {
        super()
    }

    protected map(model: CatalogoConfiguracionAnuncioSistemaModel): CatalogoConfiguracionAnuncioSistemaEntity {
        if (model) {
            let entity: CatalogoConfiguracionAnuncioSistemaEntity = {}

            if (model.estilos) {
                let lista:Array<CatalogoEstiloAnuncioSistemaEntity> = [];
                for (let estilo of model.estilos) {
                    lista.push(this.catalogoEstiloAnuncioSistemaModelMapperService.transform(estilo));
                }

                model.estilos = lista;
            }

            return entity
        }
        return null
    }
}