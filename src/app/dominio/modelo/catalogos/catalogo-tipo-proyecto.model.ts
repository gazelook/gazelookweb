import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoProyectoEntity, TraduccionCatalogoTipoProyectoEntity } from 'dominio/entidades/catalogos/catalogo-tipo-proyecto.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoTipoProyectoModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  nombre?: string,
  descripcion?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoProyectoModelMapperService extends MapedorService<CatalogoTipoProyectoModel, CatalogoTipoProyectoEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService
  ) {
    super()
  }

  protected map(model: CatalogoTipoProyectoModel): CatalogoTipoProyectoEntity {
    const entity: CatalogoTipoProyectoEntity = {}

    if (model.id) {
      entity._id = model.id
    }

    if (model.codigo) {
      entity.codigo = model.codigo
    }

    if (model.estado) {
      entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
    }

    if (model.fechaCreacion) {
      entity.fechaCreacion = model.fechaCreacion
    }

    if (model.fechaActualizacion) {
      entity.fechaActualizacion = model.fechaActualizacion
    }

    entity.traducciones = []
    const traduccion: TraduccionCatalogoTipoProyectoEntity = {}

    if (model.nombre) {
      traduccion.nombre = model.nombre
    }

    if (model.descripcion) {
      traduccion.descripcion = model.descripcion
    }

    if (traduccion.nombre || traduccion.descripcion) {
      entity.traducciones.push(traduccion)
    }
    return entity
  }
}
