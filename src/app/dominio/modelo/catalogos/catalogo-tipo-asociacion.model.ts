import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoTipoAsociacionModel {
  id?: string
  estado?: CatalogoEstadoModel //CatalogoEstado
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  nombre?: string
}
