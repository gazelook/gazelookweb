import { CatalogoEstadoModel } from "dominio/modelo/catalogos";
import { CatalogoTipoColorModel } from "dominio/modelo/catalogos";
export interface CatalogoColorModel {
    id?: string,
    codigo?: string,
    estado?: CatalogoEstadoModel,
    tipo?: CatalogoTipoColorModel,
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}
