import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoConfiguracionModel{
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?:string
    nombre?:string
}
