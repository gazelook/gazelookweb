import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoMediaEntity } from 'dominio/entidades/catalogos/catalogo-media.entity';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoMediaModel {
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoMediaModelMapperService extends MapedorService<CatalogoMediaModel, CatalogoMediaEntity> {
    protected map(model: CatalogoMediaModel): CatalogoMediaEntity {
        if (model) {
            const entity: CatalogoMediaEntity = {}
            if (model.id) {
                entity.id = model.id
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }
            return entity
        }
        return null;
    }
}
