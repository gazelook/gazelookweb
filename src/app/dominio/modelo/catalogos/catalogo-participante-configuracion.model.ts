import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoPaticipanteConfiguracionEntity } from 'dominio/entidades/catalogos/catalogo-participante-configuracion.entity';
import { Injectable } from '@angular/core';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "./catalogo-estado.model";
export interface CatalogoPaticipanteConfiguracionModel {
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
    nombre?: string
}
@Injectable({ providedIn: 'root' })
export class CatalogoPaticipanteConfiguracionModelMapperService extends MapedorService<CatalogoPaticipanteConfiguracionModel, CatalogoPaticipanteConfiguracionEntity> {

    constructor(
        private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    ) { super() }

    protected map(model: CatalogoPaticipanteConfiguracionModel): CatalogoPaticipanteConfiguracionEntity {

        if (model) {

            const entity: CatalogoPaticipanteConfiguracionModel = {}

            if (model.id) {
                entity.id = entity.id
            }

            if (model.estado) {
                entity.estado = this.catalogoEstadoModelMapperService.transform(entity.estado)
            }

            if (model.codigo) {
                entity.codigo = entity.codigo
            }

            if (model.nombre) {
                entity.nombre = entity.nombre
            }
            return entity
        }
        return null
    }
  }
