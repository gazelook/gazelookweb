import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstiloAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-estilo-anuncio-sistema';
import { CatalogoTipoAnuncioSistemaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-anuncio-sistema.model';
import { CatalogoTipoEstiloAnuncioSistemaModel } from 'dominio/modelo/catalogos/catalogo-tipo-estilo-anuncio-sistema.model';

export interface CatalogoEstiloAnuncioSistemaModel {
    _id?: string,
    color?: string,
    tamanioLetra?: string,
    tipo?: CatalogoTipoEstiloAnuncioSistemaModel
}

@Injectable({ providedIn: 'root' })
export class CatalogoEstiloAnuncioSistemaModelMapperService extends MapedorService<CatalogoEstiloAnuncioSistemaModel, CatalogoEstiloAnuncioSistemaEntity> {

    constructor(
        private catalogoTipoAnuncioSistemaModelMapperService: CatalogoTipoAnuncioSistemaModelMapperService
    ) {
        super()
    }

    protected map(model: CatalogoEstiloAnuncioSistemaModel): CatalogoEstiloAnuncioSistemaEntity {
        if (model) {
            let entity: CatalogoEstiloAnuncioSistemaEntity = {}

            if (model._id) entity._id = model._id
            if (model.color) entity.color = model.color
            if (model.tamanioLetra) entity.tamanioLetra = model.tamanioLetra
            if (model.tipo) entity.tipo = this.catalogoTipoAnuncioSistemaModelMapperService.transform(model.tipo)

            return entity
        }
        return null
    }
}