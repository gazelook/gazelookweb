import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoEventoModel } from "dominio/modelo/catalogos/catalogo-evento.model";
export interface FormulaEventoModel{
    id?:string,
    codigo?:string,
    estado?:CatalogoEstadoModel
    fechaCreacion?:Date,
    fechaActualizacion?:Date,
    formula?:string,
    descripcion?:string,
    catalogoEvento?:CatalogoEventoModel
    prioridad?:number
}
