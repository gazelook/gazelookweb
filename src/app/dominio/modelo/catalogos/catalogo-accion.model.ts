import { CatalogoEstadoModel } from "dominio/modelo/catalogos";
export interface CatalogoAccionModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel, // CatalogoEstado
  nombre?: string,
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}
