import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoComentarioEntity } from 'dominio/entidades/catalogos/catalogo-tipo-comentario.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoTipoComentarioModel {
  id?: string,
  codigo?: string,
  nombre?: string,
  estado?: CatalogoEstadoModel //Catalogo estados
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}
@Injectable({ providedIn: 'root' })
export class CatalogoTipoComentarioModelMapperService extends MapedorService<CatalogoTipoComentarioModel, CatalogoTipoComentarioEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
  ) {
    super()
  }

  protected map(model: CatalogoTipoComentarioModel): CatalogoTipoComentarioEntity {
    if (model) {

      const entity: CatalogoTipoComentarioEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.codigo) {
        entity.codigo = model.codigo
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }
      return entity
    }
    return null
  }
}
