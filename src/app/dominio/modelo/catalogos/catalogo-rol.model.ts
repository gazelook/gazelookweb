import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoRolModel } from "dominio/modelo/catalogos/catalogo-tipo-rol.model";
export interface CatalogoRolModel {
    id?:string,
    codigo?:string,
    estado?:CatalogoEstadoModel
    nombre?:string,
    fechaCreacion?:Date,
    tipo?:CatalogoTipoRolModel,
    descripcion?:string
}
