import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { CatalogoAccionModel } from 'dominio/modelo/catalogos/catalogo-accion.model';
import { CatalogoEntidadModel } from 'dominio/modelo/catalogos/catalogo-entidad.model';
export interface CatalogoEventoNotificacionModel {
  id?: String;
  codigo?: string;
  nombre?: string;
  entidad?: CatalogoEntidadModel;
  accion?: CatalogoAccionModel;
  descripcion?: string;
  fechaCreacion?: Date;
}
@Injectable({ providedIn: 'root' })
export class CatalogoEventoNotificacionModelMapperService extends MapedorService<
  CatalogoEventoNotificacionModel,
  CatalogoEstadoEntity
> {
  protected map(model: CatalogoEventoNotificacionModel): CatalogoEstadoEntity {
    if (model) {
      const entity: CatalogoEstadoEntity = {};

      if (model.id) {
        entity._id = model.id;
      }

      if (model.codigo) {
        entity.codigo = model.codigo;
      }

      if (model.nombre) {
        entity.nombre = model.nombre;
      }

      if (model.entidad) {
      }

      if (model.accion) {
      }

      if (model.descripcion) {
        entity.descripcion = model.descripcion;
      }
      return entity;
    }
    return null;
  }
}
