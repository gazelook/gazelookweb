import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoOrigenEntity } from 'dominio/entidades/catalogos/catalogo-origen.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
export interface CatalogoOrigenModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  nombre?: string,
  descripcion?: string,
  ingreso?: boolean
}
@Injectable({ providedIn: 'root' })
export class CatalogoOrigenModelMapperService extends MapedorService<CatalogoOrigenModel, CatalogoOrigenEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
  ) {
    super()
  }

  protected map(model: CatalogoOrigenModel): CatalogoOrigenEntity {

    if (model) {
      const entity: CatalogoOrigenEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.codigo) {
        entity.codigo = model.codigo
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.nombre) {
        entity.nombre = model.nombre
      }

      if (model.descripcion) {
        entity.descripcion = model.descripcion
      }

      entity.ingreso = (model.ingreso) ? true : false
      return entity
    }
    return null
  }
}
