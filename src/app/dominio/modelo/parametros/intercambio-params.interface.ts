import { CodigosCatalogoTipoIntercambio } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { AccionEntidad } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
export interface IntercambioParams {
    estado?: boolean,
    id?: string, // Id del proyecto
    codigoTipoIntercambio?: CodigosCatalogoTipoIntercambio,
    accionEntidad?: AccionEntidad
}
