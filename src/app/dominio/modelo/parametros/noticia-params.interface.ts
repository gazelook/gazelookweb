import { AccionEntidad } from '../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
export interface NoticiaParams {
  estado?: boolean,
  id?: string, // Id de la noticia
  accionEntidad?: AccionEntidad
}
