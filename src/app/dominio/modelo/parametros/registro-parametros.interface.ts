import { CatalogoTipoPerfilModel } from '../catalogos/catalogo-tipo-perfil.model';
import { AccionEntidad } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
export interface RegistroParams {
    estado?: boolean,
    id?: string,
    tipoPerfil?: CatalogoTipoPerfilModel,
    codigoTipoPerfil?: string,
    accionEntidad?: AccionEntidad
}
