import { AccionEntidad } from './../../../nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoProyecto } from './../../../nucleo/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum'

export interface ProyectoParams {
    estado?: boolean,
    id?: string, // Id del proyecto
    codigoTipoProyecto?: CodigosCatalogoTipoProyecto,
    accionEntidad?: AccionEntidad,
    menosVotado?: string
}