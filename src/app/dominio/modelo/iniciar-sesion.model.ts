import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { TokenModel } from 'dominio/modelo/token.model';
export interface IniciarSesionModel extends TokenModel {
  usuario: UsuarioModel
  //perfiles?: PerfilModel[],
}
