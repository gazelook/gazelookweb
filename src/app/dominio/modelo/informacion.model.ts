export interface InformacionModel {
    codigo: string,
    nombre: string,
    descripcion?: string[]
    mostrarDescripcion?: boolean
}
