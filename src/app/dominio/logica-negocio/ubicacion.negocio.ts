import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { UbicacionRepository } from 'dominio/repositorio/ubicacion.repository';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
@Injectable({ providedIn: 'root' })
export class UbicacionNegocio {

  constructor(
    private ubicacionRepository: UbicacionRepository,
  ) { }

  // Paises para selector - Remoto
  obtenerCatalogoPaisesParaSelector(): Observable<ItemSelector[]> {
    const data: ItemSelector[] = this.ubicacionRepository.obtenerPaisesDelLocalStorageParaItemSelector()
    if (data && data.length > 0) {
      return of(data)
    } else {
      return this.ubicacionRepository.obtenerCatalogoPaisesParaSelector()
    }
  }

  // Localidades por nombre y pais para selector - Remoto
  obtenerCatalogoLocalidadesPorNombrePorPaisParaSelector(pais: string, query: string): Observable<ItemSelector[]> {
    return this.ubicacionRepository.obtenerCatalogoLocalidadesPorNombrePorPaisParaSelector(pais, query)
  }

  // - Local
  guardarPaisesDelSelectorEnLocalStorage(paises: ItemSelector[]) {
    this.ubicacionRepository.guardarPaisesEnElLocalStorageDesdeElItemSelector(paises)
  }
  // - Local
  obtenerPaisesParaSelectorDelLocalStorage(): ItemSelector[] {
    return this.ubicacionRepository.obtenerPaisesDelLocalStorageParaItemSelector()
  }

  buscarLocalidadesPorNombrePaisConPaginacion(
    limite: number,
    pagina: number,
    codigoPais: string,
    busqueda: string
  ): Observable<PaginacionModel<ItemSelector>> {
    return this.ubicacionRepository.buscarLocalidadesPorNombrePaisConPaginacion(
      limite,
      pagina,
      codigoPais,
      busqueda
    )
  }
}
