import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-moneda.enum';
import { TipoMonedaRepository } from 'dominio/repositorio/moneda.repository';
@Injectable({ providedIn: 'root' })
export class TipoMonedaNegocio {

  constructor(
    private tipoMonedaRepository: TipoMonedaRepository
  ) { }

  guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda: ItemSelector[]) {
    this.tipoMonedaRepository.guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda)
  }

  obtenerCatalogoTipoMonedaDelLocalStorage(): ItemSelector[] {
    return this.tipoMonedaRepository.obtenerCatalogoTipoMonedaDelLocalStorage()
  }

  eliminarCatalogoTipoMonedaDelLocalStorage() {
    this.tipoMonedaRepository.eliminarCatalogoTipoMonedaDelLocalStorage()
  }

  validarcatalogoMonedaEnLocalStorage(catalogoTipoMoneda: ItemSelector[]) {
    const data = this.obtenerCatalogoTipoMonedaDelLocalStorage()
    if (!data) {
      this.guardarCatalogoTipoMonedaEnLocalStorage(catalogoTipoMoneda)
    }
  }

  obtenerCatalogoTipoMonedaParaElegibles(): Observable<ItemSelector[]> {
    const catalogo = this.obtenerCatalogoTipoMonedaDelLocalStorage()
    if (catalogo) {
      return of(catalogo)
    } else {
      return this.tipoMonedaRepository.obtenerCatalogoTipoMoneda()
    }
  }

  convertirMontoEntreMonedas(
    monto: number,
    convertirDe: CodigosCatalogoTipoMoneda,
    convertirA: CodigosCatalogoTipoMoneda,
  ): Observable<{ monto: string }> {
    return this.tipoMonedaRepository.convertirMontoEntreMonedas(monto, convertirDe, convertirA)
  }

  convertirMontoEntreMonedasProyectos(
    monto: number,
    convertirDe: string,
    convertirA: string,
  ): Observable<{ monto: string }> {
    return this.tipoMonedaRepository.convertirMontoEntreMonedasProyectos(monto, convertirDe, convertirA)
  }

}
