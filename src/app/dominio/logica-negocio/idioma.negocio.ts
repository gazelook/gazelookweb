import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { Injectable } from "@angular/core";
import { IdiomaRepository } from "dominio/repositorio/idioma.repository";
import { Observable, throwError, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity'
import { PerfilRepository } from 'dominio/repositorio/perfil.repository';
import { UbicacionRepository } from 'dominio/repositorio/ubicacion.repository';
import { PagoRepository } from 'dominio/repositorio/pago.repository';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';
import { TipoMonedaNegocio } from 'dominio/logica-negocio/moneda.negocio';
@Injectable({
	providedIn: 'root'
})
export class IdiomaNegocio {
	constructor(private idiomaRepository: IdiomaRepository,
		private perfilRepository: PerfilRepository,
		private ubicacionRepository: UbicacionRepository,
		private pagoRepository: PagoRepository,
		private tipoMonedaNegocio: TipoMonedaNegocio
	) { }

	guardarIdiomaSeleccionado(idioma: CatalogoIdiomaEntity) {
		this.idiomaRepository.guardarIdiomaLocal(idioma)
	}

	obtenerIdiomaSeleccionado(): CatalogoIdiomaEntity {
		return this.idiomaRepository.obtenerIdiomaLocal()
	}

	eliminarVarablesStorage() {
		this.perfilRepository.eliminarVariableStorage(LlavesLocalStorage.TIPO_PERFILES)
		this.pagoRepository.eliminarVariableStorage(LlavesLocalStorage.METODOS_PAGO)
		this.ubicacionRepository.eliminarVariableStorage(LlavesLocalStorage.PAISES)
		this.tipoMonedaNegocio.eliminarCatalogoTipoMonedaDelLocalStorage()
	}

	obtenerCatalogoIdiomas(): Observable<Array<CatalogoIdiomaEntity>> {
		let idiomas: Array<CatalogoIdiomaEntity> = this.idiomaRepository.obtenerIdiomas()


		if (idiomas) {
			return of(this.ordenarIdioma(idiomas))
		} else {
			return this.idiomaRepository.obtenerCatalogoIdiomas()
				.pipe(
					map((data: Array<CatalogoIdiomaEntity>) => {
						//Para no guardar en el local storage el catalogo de idiomas vacio
						if (data && data.length > 0) {
							this.idiomaRepository.guardarIdiomas(data)
							return this.ordenarIdioma(data)
						}
						return null
					}),
					catchError(err => {
						return throwError(err)
					})
				)
		}
	}
	ordenarIdioma(data: Array<CatalogoIdiomaEntity>): any {
		return data.sort()
	}

	async obtenerIdiomaSegunCodigo(codigo: CodigosCatalogoIdioma): Promise<CatalogoIdiomaEntity> {
		try {
			const idiomas = await this.obtenerCatalogoIdiomas().toPromise()
			let idioma: CatalogoIdiomaEntity
			idiomas.forEach(item => {
				if (item && item.codNombre === codigo) {
					idioma = item
				}
			})

			return idioma
		} catch (error) {
			return {
				codigo: CodigosCatalogoIdioma.INGLES,
				codNombre: 'EN',
				idiomaSistema: true
			}
		}
	}
}
