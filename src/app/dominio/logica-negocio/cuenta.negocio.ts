import {Location} from '@angular/common';
import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {CodigosCatalogoOrigenTransaccion, CodigosCatalogoTipoMoneda} from '@core/servicios/remotos/codigos-catalogos';
import {CodigosCatalogosEstadoPerfiles} from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-perfiles.enun';
import {Codigos2CatalogoIdioma} from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import {CodigosCatalogoTipoPerfil} from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import {MetodosLocalStorageService} from '@core/util/metodos-local-storage.service';
import {MetodosSessionStorageService} from '@core/util/metodos-session-storage.service';
import {UsuarioEntityMapperService} from 'dominio/entidades';
import {CatalogoIdiomaEntity} from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import {PagoFacturacionEntity} from 'dominio/entidades/catalogos/catalogo-metodo-pago.entity';
import {CoinPaymentesResponse, CoinPaymentsRatesEntity} from 'dominio/entidades/catalogos/coinpayments-rates.entity';
import {PerfilModel} from 'dominio/modelo/entidades/perfil.model';
import {PagoModel} from 'dominio/modelo/pago.model';
import {TokenModel} from 'dominio/modelo/token.model';
import {CuentaRepository} from 'dominio/repositorio/cuenta.repository';
import {IdiomaRepository} from 'dominio/repositorio/idioma.repository';
import {PerfilRepository} from 'dominio/repositorio/perfil.repository';
import {CorreoContacto} from 'presentacion/contacto/contacto.component';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {RutasLocales} from 'src/app/rutas-locales.enum';
import {FormularioContactanos} from '../modelo/entidades/fomulario-contactanos.model';
import {UsuarioModel, UsuarioModelMapperService} from '../modelo/entidades/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class CuentaNegocio {
  constructor(
    private cuentaRepository: CuentaRepository,
    private perfilRepository: PerfilRepository,
    private idiomaRepository: IdiomaRepository,
    private usuarioModelMapper: UsuarioModelMapperService,
    private metodosSessionStorageService: MetodosSessionStorageService,
    private usuarioEntityMapperService: UsuarioEntityMapperService,
    private metodosLocalStorageService: MetodosLocalStorageService,
    private auth: AngularFireAuth,
    private router: Router,
    private _location: Location,
  ) {
  }

  noAutorizado() {
    // this.metodosLocalStorageService.eliminarLocalStorage()
    // this.metodosSessionStorageService.eliminarSessionStorage()
    this.cerrarSesionEnFirebase();
    this._location.replaceState('');
    this.router.navigateByUrl('');
  }

  async cerrarSesionEnFirebase() {
    try {
      await this.auth.signOut();
    } catch (error) {
    }
  }

  iniciarSesion(email: string, contrasena: string): Observable<UsuarioModel> {
    let data = {email: email, contrasena: contrasena};
    return this.cuentaRepository.iniciarSesion(data)
      .pipe(
        map(data => {
          if (data) {
            this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
            this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
            this.cuentaRepository.guardarUsuarioEnLocalStorage(data.usuario);
            return data.usuario;
          }
          return null;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  crearCuenta(metodoPago: string, pago?: PagoFacturacionEntity): Observable<PagoModel> {
    const idioma: CatalogoIdiomaEntity = this.idiomaRepository.obtenerIdiomaLocal();
    let usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();

    usuario.direccion = usuario.perfiles[0].direcciones[0];

    usuario.idioma = {
      codigo: (idioma) ? idioma.codigo : Codigos2CatalogoIdioma.INGLES
    };

    if (pago) {
      usuario.datosFacturacion = {
        direccion: pago.direccion,
        nombres: pago.nombres,
        telefono: pago.telefono,
        email: pago.email
      };
    }

    usuario.metodoPago = {
      codigo: metodoPago
    };

    let usuarioCrear = this.usuarioModelMapper.transform(usuario);

    return this.cuentaRepository.crearCuenta(usuarioCrear)
      .pipe(
        map(data => {
          return data;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  activarCuenta(
    idTransaccion: string,
    pagoNoConfirmado: boolean = false
  ): Observable<UsuarioModel> {
    let data = {
      'idTransaccion': idTransaccion,
      'pagoNoConfirmado': pagoNoConfirmado
    };
    return this.cuentaRepository.activarCuenta(data)
      .pipe(
        map(data => {
          this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
          this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
          this.cuentaRepository.guardarUsuarioEnLocalStorage(data.usuario);
          return data.usuario;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  crearCuentaPaymentez(): Observable<PagoModel> {
    const idioma: CatalogoIdiomaEntity = this.idiomaRepository.obtenerIdiomaLocal();
    const usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    usuario.direccion = usuario.perfiles[0].direcciones[0];
    usuario.idioma = {codigo: (idioma) ? idioma.codigo : Codigos2CatalogoIdioma.INGLES};
    const usuarioCrear = this.usuarioModelMapper.transform(usuario);
    const {transacciones, ...usuarioSave} = usuarioCrear;
    return this.cuentaRepository.crearCuentaPaymentez(usuarioSave)
      .pipe(
        map(data => data),
        catchError(err => throwError(err))
      );
  }

  activarCuentaPaymentez(
    codigo: string,
    monto: number,
    idPago: string,
    pagoNoConfirmado: boolean,
    codNombre: string,
    cuotaMinima: number,
    autorizacionCodePaymentez?: string
  ): Observable<any> {
    const usuarioLocalStorage: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    const user: UsuarioModel = {};
    user.autorizacionCodePaymentez = autorizacionCodePaymentez;
    user.metodoPago = {codigo};
    user.monedaRegistro = {codNombre};
    user.email = usuarioLocalStorage.email;
    user.pagoNoConfirmado = pagoNoConfirmado;
    user.datosFacturacion = {
      nombres: usuarioLocalStorage.perfiles[0].nombre,
      email: usuarioLocalStorage.email,
      telefono: usuarioLocalStorage.perfiles[0].telefonos[0]?.numero || '09xxxxxxxxxxxx',
      idPago,
      direccion: usuarioLocalStorage.perfiles[0].direcciones[0].pais.nombre,
    };
    user.transacciones = [];
    user.transacciones.push({
      moneda: {
        codNombre: CodigosCatalogoTipoMoneda.USD
      },
      monto: cuotaMinima,
      origen: {
        codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
      },
      destino: {
        codigo: CodigosCatalogoOrigenTransaccion.SUSCRIPCION_DEL_USUARIO
      }
    });
    if (
      Number(monto) > cuotaMinima &&
      // tslint:disable-next-line: no-string-literal
      Math['round10'](Number(monto) - cuotaMinima, -2) >= 0.1
    ) {
      user.transacciones.push({
        moneda: {
          codNombre: CodigosCatalogoTipoMoneda.USD
        },
        // tslint:disable-next-line: no-string-literal
        monto: Math['round10'](Number(monto) - cuotaMinima, -2),
        origen: {
          codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
        },
        destino: {
          codigo: CodigosCatalogoOrigenTransaccion.CUOTA_EXTRA
        }
      });
    }
    return this.cuentaRepository.activarCuentaPaymentez(user)
      .pipe(
        map(data => {
          this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
          this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
          this.cuentaRepository.guardarUsuarioEnLocalStorage(data.usuario);
          return data.usuario;
        }),
        catchError(err => throwError(err))
      );
  }

  crearCuentaCoinPayments(metodoPago: string, pago: PagoFacturacionEntity, currency2: string): Observable<CoinPaymentesResponse> {
    const idioma: CatalogoIdiomaEntity = this.idiomaRepository.obtenerIdiomaLocal();
    const usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    usuario.direccion = usuario.perfiles[0].direcciones[0];
    usuario.idioma = {codigo: (idioma) ? idioma.codigo : Codigos2CatalogoIdioma.INGLES};
    usuario.metodoPago = {codigo: metodoPago};
    const usuarioTemp = this.usuarioModelMapper.transform(usuario);
    const usuarioCrear = {...usuarioTemp, currency2, datosFacturacion: pago};

    return this.cuentaRepository.crearCuentaCoinPayments(usuarioCrear)
      .pipe(map(data => data), catchError(err => throwError(err)));
  }

  activarCuentaCoinPayments(data: CoinPaymentesResponse): UsuarioModel {
    const usuario = this.usuarioEntityMapperService.transform(data.usuario);
    this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
    this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
    this.cuentaRepository.guardarUsuarioEnLocalStorage(usuario);
    this.eliminarSessionStorage();
    return usuario;
  }


  crearCuentaCripto(metodoPago: string): Observable<CoinPaymentesResponse> {
    const idioma: CatalogoIdiomaEntity = this.idiomaRepository.obtenerIdiomaLocal();
    const usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    usuario.direccion = usuario.perfiles[0].direcciones[0];
    usuario.idioma = {codigo: (idioma) ? idioma.codigo : Codigos2CatalogoIdioma.INGLES};
    usuario.metodoPago = {codigo: metodoPago};
    usuario.datosFacturacion = {
      nombres: usuario.perfiles[0].nombre,
      email: usuario.email,
      telefono: usuario.perfiles[0].telefonos[0]?.numero || '09xxxxxxxxxxxx',
      direccion: usuario.perfiles[0].direcciones[0].pais.nombre,
    };
    const usuarioTemp = this.usuarioModelMapper.transform(usuario);
    const pagoCripto = usuario.pagoCripto;
    console.log(usuarioTemp);
    console.log('pagoCripto', usuario);
    const usuarioCrear = {...usuarioTemp, pagoCripto};
    console.log('usuarioCrear', usuarioCrear);


    return this.cuentaRepository.crearCuentaCoinPayments(usuarioCrear)
      .pipe(map(data => data), catchError(err => throwError(err)));
  }

  activarCuentaCripto(data: CoinPaymentesResponse): UsuarioModel {
    const usuario = this.usuarioEntityMapperService.transform(data.usuario);
    this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
    this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
    this.cuentaRepository.guardarUsuarioEnLocalStorage(usuario);
    this.eliminarSessionStorage();
    return usuario;
  }

  obtenerCoinPaymentsRates(): Observable<CoinPaymentsRatesEntity[]> {
    return this.cuentaRepository.obtenerCoinPaymentsRates()
      .pipe(map(data => data), catchError(err => throwError(err)));
  }

  obtenerTokenAutenticacion(): Observable<string> {
    const tokenActual = this.cuentaRepository.obtenerTokenAutenticacion();
    if (tokenActual) {
      const helper = new JwtHelperService();
      const isExpired = helper.isTokenExpired(tokenActual);

      if (isExpired) {
        const tokenRefrescar = this.cuentaRepository.obtenerTokenRefresh();
        return this.cuentaRepository.refrescarToken(tokenRefrescar)
          .pipe(
            map((data: TokenModel) => {
              if (data === undefined) {
                const sesionIniciada = this.sesionIniciada();

                this.cerrarSesion();
                return;
              }
              this.cuentaRepository.guardarTokenAutenticacion(data.tokenAccess);
              this.cuentaRepository.guardarTokenRefresh(data.tokenRefresh);
              return data.tokenAccess;
            }),
            catchError(err => {
              this.cerrarSesion();
              return throwError(err);
            })
          );
      } else {
        return of(tokenActual);
      }
    } else {
      return of(tokenActual);
    }
  }

  async cerrarSesion() {
    const usuario = this.obtenerUsuarioDelLocalStorage();
    const respuesta = await this.cerrarSessionEnElApi(usuario).toPromise();

    if (respuesta !== 200 && respuesta !== 201) {
      throw new Error('');
    }

    await this.auth.signOut();

    this.cerrarSession();
    this._location.replaceState('/');
    this.router.navigateByUrl(RutasLocales.BASE, {replaceUrl: true});
  }

  // Guardar usuario en el local storage
  guardarUsuarioEnLocalStorage(usuario: UsuarioModel): void {
    this.cuentaRepository.guardarUsuarioEnLocalStorage(usuario);
  }

  // Obtener usuario del local storage
  obtenerUsuarioDelLocalStorage(): UsuarioModel {
    return this.cuentaRepository.obtenerUsuarioDelLocalStorage();
  }

  obtenerIdDispositivoDelUsuario(): string {
    const usuario = this.obtenerUsuarioDelLocalStorage();
    if (
      usuario &&
      usuario.dispositivos &&
      usuario.dispositivos.length > 0
    ) {
      return usuario.dispositivos[0].id;
    }

    return '';
  }

  removerUsuarioDelLocalStorage() {
    return this.cuentaRepository.removerUsuarioDelLocalStorage();
  }

  // Guardar usuario en el session storage
  guardarUsuarioEnSessionStorage(usuario: UsuarioModel) {
    this.cuentaRepository.guardarUsuarioEnSessionStorage(usuario);
  }

  // Obtener usuario del session storage
  obtenerUsuarioDelSessionStorage(): UsuarioModel {
    return this.cuentaRepository.obtenerUsuarioDelSessionStorage();
  }

  removerUsuarioDelSessionStorage() {
    return this.cuentaRepository.removerUsuarioDelSessionStorage();
  }

  eliminarSessionStorage() {
    this.cuentaRepository.eliminarSessionStorage();
  }

  // Valida si existe el usuario, caso contrario lo crea
  validarUsuarioDelSesionStorage(codigoPerfil: string): UsuarioModel {
    let usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    if (!usuario) {
      usuario = {
        id: '',
        email: '',
        contrasena: '',
        perfiles: [],
        perfilGrupo: (codigoPerfil === CodigosCatalogoTipoPerfil.GROUP),
        aceptoTerminosCondiciones: false,
        emailResponsable: '',
        menorEdad: false,
        fechaNacimiento: new Date(),
        nombreResponsable: '',
      };
      this.guardarUsuarioEnSessionStorage(usuario);
    } else {
      usuario.perfilGrupo = (codigoPerfil === CodigosCatalogoTipoPerfil.GROUP);
      this.guardarUsuarioEnSessionStorage(usuario);
      usuario = this.obtenerUsuarioDelSessionStorage();
    }
    return usuario;
  }

  // Eliminar perfil de usuario
  eliminarPerfilDelUsuario(codigoPerfil: string) {
    let usuario: UsuarioModel = this.obtenerUsuarioDelSessionStorage();
    let pos = -1;
    usuario.perfiles.forEach((perfil, i) => {
      if (perfil.tipoPerfil.codigo === codigoPerfil) {
        pos = i;
      }
    });
    if (pos >= 0) {
      usuario.perfiles.splice(pos, 1);
      // Borrar email y contrasena
      if (usuario.perfiles.length === 0) {
        usuario.email = '';
        usuario.contrasena = '';
      }
      this.guardarUsuarioEnSessionStorage(usuario);
    }
  }

  // Valida el estado del perfil cuando el usuario abandona el formulario de registro
  validarEstadoPerfilParaDestruir(codigoPerfil: string, codigoEstado: CodigosCatalogosEstadoPerfiles) {
    if (codigoEstado === CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR) {
      this.eliminarPerfilDelUsuario(codigoPerfil);
    }
  }

  // Obtener Email y Contrasena del usuario
  obtenerEmailConContrasenaDelUsuario(session: boolean = true) {
    const usuario: UsuarioModel = (session)
      ? this.obtenerUsuarioDelSessionStorage()
      : this.obtenerUsuarioDelLocalStorage();
    return {
      email: usuario.email,
      contrasena: (session) ? usuario.contrasena : '**********',
      fechaNacimiento: (usuario.fechaNacimiento) ? usuario.fechaNacimiento : null,
      direccionDomiciliaria: usuario.direccionDomiciliaria,
      documentoIdentidad: usuario.documentoIdentidad
    };
  }

  guardarAceptacionMenorEdad(correoResponsable: string, nombreResponsable: string, fechaNacimiento: Date) {
    let cuenta: UsuarioModel = {
      id: '',
      email: '',
      contrasena: '',
      perfiles: [],
      aceptoTerminosCondiciones: true,
      emailResponsable: correoResponsable,
      menorEdad: true,
      fechaNacimiento: fechaNacimiento,
      nombreResponsable: nombreResponsable,
    };
    this.cuentaRepository.guardarUsuarioEnSessionStorage(cuenta);
  }

  eliminarAceptacionTerminosCondiciones() {
    this.cuentaRepository.guardarUsuarioEnSessionStorage(null);
  }

  aceptoTerminosCondiciones() {
    let cuenta: UsuarioModel = {
      id: '',
      email: '',
      contrasena: '',
      perfiles: [],
      aceptoTerminosCondiciones: true,
      menorEdad: false,
    };
    this.cuentaRepository.guardarUsuarioEnSessionStorage(cuenta);
  }

  sesionIniciada(): boolean {
    return this.cuentaRepository.obtenerTokenAutenticacion() != null;
  }

  limpiarTerminosCondiciones() {
    const user = this.cuentaRepository.obtenerUsuarioDelSessionStorage();
    if (user) {
      this.cuentaRepository.guardarUsuarioEnSessionStorage(null);
    }
  }

  verificarAceptacionTerminosCondiciones() {
    let cuenta = this.obtenerUsuarioDelSessionStorage();
    if (cuenta) {
      return !cuenta.aceptoTerminosCondiciones;
    }
    return true;
  }

  guardarFirebaseUIDEnLocalStorage(uid: string) {
    this.cuentaRepository.guardarFirebaseUIDEnLocalStorage(uid);
  }

  obtenerFirebaseUIDEnLocalStorage(): string {
    return this.cuentaRepository.obtenerFirebaseUIDEnLocalStorage();
  }

  cerrarSession() {
    this.cuentaRepository.guardarTokenAutenticacion(null);
    this.cuentaRepository.guardarTokenRefresh(null);
    this.cuentaRepository.guardarUsuarioEnLocalStorage(null);
    this.perfilRepository.almacenarPerfilSeleccionado(null);
    this.cuentaRepository.guardarFirebaseUIDEnLocalStorage(null);
    this.cuentaRepository.eliminarSessionStorage();
  }

  cerrarSessionEnElApi(
    usuario: UsuarioModel
  ): Observable<number> {
    return this.cuentaRepository.cerrarSessionEnElApi(usuario);
  }

  validarEmailUnico(email: string): Observable<string> {
    return this.cuentaRepository.validarEmailUnico(email);
  }

  asignarTransaccionesAlUsuario() {
  }

  obtenerPerfilActivoDelUsuario(
    usuario: UsuarioModel,
  ): PerfilModel {
    let perfilRes: PerfilModel;
    if (usuario && usuario.perfiles) {
      usuario.perfiles.forEach(perfil => {
        if (perfil && perfil.estado && perfil.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO) {
          perfilRes = perfil;
        }
      });
    }
    return perfilRes;
  }

  recuperarContrasena(email: string): Observable<string> {
    return this.cuentaRepository.recuperarContrasena(email);
  }

  enviarEmailDeContacto(correo: CorreoContacto): Observable<string> {
    return this.cuentaRepository.enviarEmailDeContacto(correo);
  }

  solicitarInformacionUsuario(idUsuario: string): Observable<any> {
    return this.cuentaRepository.solicitarInformacionUsuario(idUsuario);
  }

  eliminarDatosUsuario(idUsuario: string): Observable<any> {
    return this.cuentaRepository.eliminarDatosUsuario(idUsuario);
  }

  reenviarCorreoVerificacion(idUsuario: string): Observable<any> {
    return this.cuentaRepository.reenviarCorreoVerificacion(idUsuario);
  }

  enviarFormularioContactanos(formulario: FormularioContactanos): Observable<number> {
    return this.cuentaRepository.enviarFormularioContactanos(formulario);
  }
}
