import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { Injectable } from '@angular/core';
import { ParamsRepository } from 'dominio/repositorio/params.repository';

@Injectable({ providedIn: 'root' })
export class ParamsNegocio {

    constructor(
        private paramsRepository: ParamsRepository
    ) { }

    guardarParamsEnSessionStorage(params: any) {
        this.paramsRepository.guardarParamsEnSessionStorage(params)
    }

    obtenerParamsDelSessionStorage() : any {
        this.paramsRepository.obtenerParamsDelSessionStorage()
    }

    obtenerParamsParaProyecto() : ProyectoParams {
        return this.obtenerParamsDelSessionStorage() as ProyectoParams
    }
}
