import { UbicacionDelComponente } from './../../presentacion/enrutador/enrutador.component';
import { Injectable } from "@angular/core";
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { IntercambioParams } from 'dominio/modelo/parametros/intercambio-params.interface';
import { IntercambioRepository } from 'dominio/repositorio/intercambio.repository';
import { IntercambioActivoEnrutador } from "../../presentacion/enrutador/enrutador.component";
@Injectable({ providedIn: 'root' })
export class IntercambioNegocio {

  constructor(
    private intercambioRepository: IntercambioRepository
  ) { }


  guardarIntercambioActivoTerciosEnSessionStorage(intercambio: Array<IntercambioActivoEnrutador>) {
    this.intercambioRepository.guardarIntercambioActivoTerciosEnSessionStorage(intercambio)
  }

  obtenerIntercambioActivoTerciosDelSessionStorage(): Array<IntercambioActivoEnrutador> {
    return this.intercambioRepository.obtenerIntercambioActivoTerciosDelSessionStorage()
  }

  // guardarintercambioActivoEnSessionStorage(intercambio: IntercambioModel) {
  //   this.intercambioRepository.guardarIntercambioActivoEnSessionStorage(intercambio)
  // }

  // obtenerIntercambioActivoDelSessionStorage(): IntercambioModel {
  //   return this.intercambioRepository.obtenerIntercambioActivoDelSessionStorage()
  // }

  removerIntercambioActivoDelSessionStorage() {
    this.intercambioRepository.removerintercambioActivoDelSessionStorage()
  }

  crearIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {
    return this.intercambioRepository.crearIntercambio(intercambio)
  }

  obtenerInformacionDelIntercambio(idIntercambio: string, idPerfil: string): Observable<IntercambioModel> {
    return this.intercambioRepository.obtenerInformacionDelIntercambio(idIntercambio, idPerfil)
  }

  actualizarIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {
    const intercambioSinAlbums: IntercambioModel = this.excluirAlbumsYaRegistradosDelIntercambio(intercambio)
    return this.intercambioRepository.actualizarIntercambio(intercambioSinAlbums)
  }

  eliminarIntercambio(idIntercambio: string, idPerfil: string): Observable<string> {
    return this.intercambioRepository.eliminarIntercambio(idIntercambio, idPerfil)
  }

  cambiarStatusIntercambio(intercambio: IntercambioModel): Observable<string> {
    return this.intercambioRepository.cambiarStatusIntercambio(intercambio)
  }

  tiposIntercambio(): Observable<Array<CatalogoTipoIntercambioModel>> {
    return this.intercambioRepository.tiposIntercambio()
  }

  excluirAlbumsYaRegistradosDelIntercambio(intercambio: IntercambioModel) {
    const adjuntosParaEnviar: Array<AlbumModel> = []
    intercambio.adjuntos.forEach((item, pos) => {
      if (!(item && item._id)) {
        adjuntosParaEnviar.push(item)
      }
    })

    intercambio.adjuntos = adjuntosParaEnviar
    return intercambio
  }

  validarIntercambioActivoSegunAccionCrear(
    codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ): IntercambioModel {
    let intercambios = this.obtenerIntercambioActivoTerciosDelSessionStorage()
    const intercambioNuevo = this.crearObjetoVacioDeintercambioParaRetornar(
      codigoTipoIntercambio,
      perfil,
    )


    const index2 = intercambios?.findIndex(e => e.intercambio.perfil._id === perfil._id)
  

    if (!intercambios || intercambios === null || index2 < 0) {
      intercambios = []
    }

    const index = intercambios.findIndex(e => e.ubicacion === ubicacion)


    if (index < 0) {
      const item: IntercambioActivoEnrutador = {
        ubicacion: ubicacion,
        intercambio: intercambioNuevo
      }

      intercambios.push(item)
      this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
      return item.intercambio
    }

    const item = intercambios[index]

    if (
      item.intercambio &&
      item.intercambio.tipo &&
      item.intercambio.tipo.codigo &&
      item.intercambio.tipo.codigo === codigoTipoIntercambio
    ) {
      return item.intercambio
    }

    item.intercambio = intercambioNuevo
    intercambios[index] = item

    this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)

    return item.intercambio
  }

  crearObjetoVacioDeintercambioParaRetornar(
    codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
    perfil: PerfilModel,
  ) {
    const intercambio: IntercambioModel = {
      id: '',
      tituloCorto: '',
      titulo: '',
      direccion: {
        descripcion: '',
        pais: {
        },
        localidad: {
        }
      },
      adjuntos: [],
      email: '',
      tipo: {
        codigo: codigoTipoIntercambio
      },
      tipoIntercambiar: {
        codigo: CodigosCatalogoTipoIntercambio.INTERCAMBIO_CUALQUIERA
      },
      perfil: perfil,
      descripcion: '',
      medias: [],
      fechaCreacion: new Date(),
    }
    return intercambio
  }



  asignarValoresDeLosCamposAlIntercambio(
    params: IntercambioParams,
    intercambio: IntercambioModel,
    pais: ItemSelector,
    localidad: ItemSelector,
    intercambioForm: FormGroup,
    eliminarData: boolean,
    codigoAIntercambiar: string,
    ubicacion: UbicacionDelComponente
  ): IntercambioModel {

    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:

        intercambio.tituloCorto = intercambioForm.value.tituloCorto
        intercambio.titulo = intercambioForm.value.titulo
        intercambio.direccion.descripcion = intercambioForm.value.ubicacion
        intercambio.direccion.pais.codigo = pais.codigo
        intercambio.direccion.pais.nombre = pais.nombre
        intercambio.direccion.localidad.codigo = localidad.codigo
        intercambio.direccion.localidad.nombre = localidad.nombre
        intercambio.email = intercambioForm.value.email
        intercambio.tipoIntercambiar.codigo = codigoAIntercambiar
        this.guardarProyectoEnProyectosActivosTercio(ubicacion, intercambio)
        break
      case AccionEntidad.ACTUALIZAR:
        intercambio.tituloCorto = intercambioForm.value.tituloCorto
        intercambio.titulo = intercambioForm.value.titulo
        intercambio.direccion.descripcion = intercambioForm.value.ubicacion
        intercambio.direccion.pais.codigo = pais.codigo
        intercambio.direccion.pais.nombre = pais.nombre
        intercambio.direccion.localidad.codigo = localidad.codigo
        intercambio.direccion.localidad.nombre = localidad.nombre
        intercambio.email = intercambioForm.value.email
        intercambio.tipoIntercambiar.codigo = codigoAIntercambiar
        break
      default: break;
    }

    if (eliminarData) {
      this.removerIntercambioActivoDelSessionStorage()
    }
    return intercambio
  }

  asignarValoresDeLosCamposAlintercambioParaValidarCambios(
    intercambio: IntercambioModel,
    pais: ItemSelector,
    localidad: ItemSelector,
    intercambioForm: FormGroup
  ): IntercambioModel {
    intercambio.tituloCorto = intercambioForm.value.tituloCorto
    intercambio.titulo = intercambioForm.value.titulo
    intercambio.email = intercambioForm.value.email
    intercambio.direccion.descripcion = intercambioForm.value.ubicacion

    if (!intercambio.direccion.pais) {
      intercambio.direccion.pais = {
        codigo: '',
        nombre: ''
      }
    }

    if (!intercambio.direccion.localidad) {
      intercambio.direccion.localidad = {
        codigo: '',
        nombre: ''
      }
    }

    intercambio.direccion.pais.codigo = pais.codigo
    intercambio.direccion.pais.nombre = pais.nombre

    intercambio.direccion.localidad.codigo = localidad.codigo
    intercambio.direccion.localidad.nombre = localidad.nombre

    return intercambio
  }

  validarSiExistenCambiosEnElIntercambio(
    intercambioAValidar: IntercambioModel,
    accionEntidad: AccionEntidad,
    ubicacion: UbicacionDelComponente
  ): boolean {
    try {

      const intercambioOriginal = (accionEntidad === AccionEntidad.CREAR) ?
        this.crearObjetoVacioDeintercambioParaRetornar(
          intercambioAValidar.tipo.codigo as CodigosCatalogoTipoIntercambio,
          intercambioAValidar.perfil
        ) :
        this.obtenerIntercambioDeIntercamboActivoTercio(ubicacion)


      if (!intercambioOriginal || !intercambioAValidar) {
        return false
      }

      if (intercambioAValidar.tituloCorto.trim() !== intercambioOriginal.tituloCorto.trim()) {
        return true
      }

      if (intercambioAValidar.titulo.trim() !== intercambioOriginal.titulo.trim()) {
        return true
      }

      const direccionOriginal = intercambioOriginal.direccion
      const direccionModificada = intercambioAValidar.direccion

      if (
        !direccionOriginal.pais ||
        !direccionModificada.pais
      ) {
        return false
      }

      if (direccionOriginal.pais.codigo.trim() !== direccionModificada.pais.codigo.trim()) {
        return true
      }

      if (direccionOriginal.descripcion.trim() !== direccionModificada.descripcion.trim()) {
        return true
      }

      if (intercambioAValidar.descripcion !== intercambioOriginal.descripcion) {
        return true
      }

      if (intercambioAValidar.medias.length !== intercambioOriginal.medias.length) {
        return true
      }

      const mediaAValidar = intercambioAValidar.medias[0]
      const mediaOriginal = intercambioOriginal.medias[0]

      if (
        mediaAValidar &&
        mediaOriginal &&
        mediaAValidar.id !== mediaOriginal.id
      ) {
        return true
      }

      return false
    } catch (error) {
      return false
    }
  }

  validarIntercambioActivoSegunAccionActualizarVisitar(
    intercambio: IntercambioModel,
    ubicacion: UbicacionDelComponente
  ) {

    let intercambios = this.obtenerIntercambioActivoTerciosDelSessionStorage()

    if (!intercambios || intercambios === null) {
      intercambios = []
    }

    const index = intercambios.findIndex(e => e.ubicacion === ubicacion)

    if (index < 0) {
      intercambios.push({
        ubicacion: ubicacion,
        intercambio: intercambio
      })
 
      this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
      return
    }

    intercambios[index].intercambio = intercambio

    this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
  }

  crearObjetoVacioDeintercambio(
    codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ) {
    const intercambio: IntercambioModel = this.crearObjetoVacioDeintercambioParaRetornar(codigoTipoIntercambio, perfil)


    this.guardarProyectoEnProyectosActivosTercio(ubicacion, intercambio)
  }



  buscarMisIntercambiosTipo(
    tipoIntercambio: CodigosCatalogoTipoIntercambio,
    perfil: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<IntercambioModel>> {
    return this.intercambioRepository.buscarMisIntercambiosTipo(
      tipoIntercambio,
      perfil,
      limite,
      pagina
    )
  }
  buscarIntercambioPorFiltro(
    limite: number,
    pagina: number,
    tipo: CodigosCatalogoTipoIntercambio,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
    pais: string,
    titulo: string,
    localidad: string,
    codigoAIntercambiarA: string,
    codigoAIntercambiarB: string,
  ): Observable<PaginacionModel<IntercambioModel>> {

    return this.intercambioRepository.buscarintercambiosPorFiltro(
      limite,
      pagina,
      tipo,
      perfil,
      fechaInicial,
      fechaFinal,
      pais,
      titulo,
      localidad,
      codigoAIntercambiarA,
      codigoAIntercambiarB,
    )
  }

  guardarProyectoEnProyectosActivosTercio(
    ubicacion: UbicacionDelComponente,
    intercambio: IntercambioModel
  ) {
    let intercambios = this.obtenerIntercambioActivoTerciosDelSessionStorage()

    if (!intercambios || intercambios === null) {
      intercambios = []
    }

    const index = intercambios.findIndex(e => e.ubicacion === ubicacion)

    if (index < 0) {
      intercambios.push({
        intercambio: intercambio,
        ubicacion: ubicacion
      })
  
      this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
      return
    }

    intercambios[index].intercambio = intercambio
    this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
  }

  removerIntercambioDeIntercambioActivoTercios(
    ubicacion: UbicacionDelComponente
  ) {
    let intercambios = this.obtenerIntercambioActivoTerciosDelSessionStorage()

    if (!intercambios || intercambios === null) {
      intercambios = []
    }

    const index = intercambios.findIndex(e => e.ubicacion === ubicacion)

    if (index >= 0) {
      intercambios.splice(index, 1)

      this.guardarIntercambioActivoTerciosEnSessionStorage(intercambios)
    }
  }
  obtenerIntercambioDeIntercamboActivoTercio(
    ubicacion: UbicacionDelComponente
  ): IntercambioModel {
    let intercambios = this.obtenerIntercambioActivoTerciosDelSessionStorage()

    if (!intercambios || intercambios === null) {
      intercambios = []
    }

    const index = intercambios.findIndex(e => e.ubicacion === ubicacion)

    if (index < 0) {
      return undefined
    }
    return intercambios[index].intercambio
  }

}
