import { MensajeModel } from 'dominio/modelo/entidades/mensaje.model';
import { MensajesAsociacionRepository } from 'dominio/repositorio/mensajes-asociacion.repository';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { Injectable } from "@angular/core"
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators'
import { ConversacionModel } from 'dominio/modelo/entidades/conversacion.model';
@Injectable({ providedIn: 'root' })
export class MensajesAsociacionNegocio {
    constructor(
        private mensajesAsociacionRepository: MensajesAsociacionRepository,
    ) { }

    obtenerConversacion(idPerfil: string,idAsociacion:string, limite: number, pagina: number, filtro: string): Observable<PaginacionModel<MensajeModel>> {
        return this.mensajesAsociacionRepository.obtenerConversacion(idPerfil,idAsociacion, limite, pagina, filtro)
            .pipe(
                map((data: PaginacionModel<MensajeModel>) => {
                    return data
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }

    obtenerConversacionFecha(idPerfil: string,idAsociacion:string, limite: number, pagina: number, fecha: string): Observable<PaginacionModel<MensajeModel>> {
        return this.mensajesAsociacionRepository.obtenerConversacionFecha(idPerfil,idAsociacion, limite, pagina, fecha)
            .pipe(
                map((data: PaginacionModel<MensajeModel>) => {
                    return data
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }

    obtenerMsjNoLeidos(idPerfil: string, limite: number, pagina: number): Observable<PaginacionModel<ConversacionModel>> {
        return this.mensajesAsociacionRepository.obtenerMsjNoLeidos(idPerfil, limite, pagina)
            .pipe(
                map((data: PaginacionModel<ConversacionModel>) => {
                    return data
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }
}
