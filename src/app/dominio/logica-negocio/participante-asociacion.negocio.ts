import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ParticipanteAsociacionEntity } from 'dominio/entidades/participante-asociacion.entity';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ParticipanteAsociacionRepository } from 'dominio/repositorio/participante-asociacion.repository';
@Injectable({ providedIn: 'root' })
export class ParticipanteAsociacionNegocio {
  constructor(
    private participanteAsociocionRepository: ParticipanteAsociacionRepository
  ) {}

  obtenerParticipanteAsoTipo(
    idPerfil: string,
    limite: number,
    pagina: number,
    filtro: string,
    filtroOrden?: string
  ): Observable<PaginacionModel<ParticipanteAsociacionModel>> {
    return this.participanteAsociocionRepository
      .obtenerParticipanteAsoTipo(idPerfil, limite, pagina, filtro, filtroOrden)
      .pipe(
        map((data: PaginacionModel<ParticipanteAsociacionModel>) => {
          return data;
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  cambiarEstadoAsociacion(
    idAsociacion: string,
    estado: string,
    idPerfil: string
  ): Observable<string> {
    let participanteAsoEntity: ParticipanteAsociacionEntity = {
      estado: {
        codigo: estado,
      },
      asociacion: {
        _id: idAsociacion,
      },
      perfil: {
        _id: idPerfil,
      },
    };
    return this.participanteAsociocionRepository
      .cambiarEstadoAsociacion(participanteAsoEntity)
      .pipe(
        map((data: string) => {
          return data;
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  obtenerParticipantesAsociacion(
    idAsociacion: string,
    idPerfil: string
  ): Observable<AsociacionModel> {
    return this.participanteAsociocionRepository.obtenerParticipantesAsociacion(
      idAsociacion,
      idPerfil
    );
  }

  crearAsociacion(
    nombre: string,
    tipo: string,
    participantes: any[]
  ): Observable<string> {
    let participanteAsoEntity = {
      tipo: {
        codigo: tipo,
      },
      nombre: nombre,
      participantes: participantes,
    };
    return this.participanteAsociocionRepository
      .crearAsociacion(participanteAsoEntity)
      .pipe(
        map((data: string) => {
          return data;
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }
}
