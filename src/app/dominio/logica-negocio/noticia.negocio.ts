import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import {
  NoticiaActivaEnrutador,
  UbicacionDelComponente,
} from 'presentacion/enrutador/enrutador.component';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { NoticiaParams } from 'dominio/modelo/parametros/noticia-params.interface';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstadoNoticia } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-estado-noticia.enum';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { VotoNoticiaModel } from 'dominio/modelo/entidades/voto-noticia.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { NoticiaRepository } from 'dominio/repositorio/noticia.repository';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';

@Injectable({
  providedIn: 'root',
})
export class NoticiaNegocio {
  constructor(
    private noticiaRepository: NoticiaRepository,
    private cuentaNegocio: CuentaNegocio
  ) {}

  obtenerNoticiasPerfil(
    idPerfil: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<NoticiaEntity>> {
    return this.noticiaRepository.obtenerNoticiasPerfil(
      idPerfil,
      limite,
      pagina
    );
  }

  obtenerNoticiasMapeadasDelPerfil(
    idPerfil: string,
    limite: number,
    pagina: number,
    traducir: boolean
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.obtenerNoticiasMapeadasDelPerfil(
      idPerfil,
      limite,
      pagina,
      traducir
    );
  }

  guardarNoticiaActivaEnSessionStorage(noticia: NoticiaModel) {
    this.noticiaRepository.guardarNoticiaActivaEnSessionStorage(noticia);
  }

  obtenerNoticiaActiviaDelSessionStorage(): NoticiaModel {
    return this.noticiaRepository.obtenerNoticiaActiviaDelSessionStorage();
  }

  removerNoticiaActivaDelSessionStorage() {
    this.noticiaRepository.removerNoticiaActivaDelSessionStorage();
  }

  guardarNoticiaActivaTerciosEnSessionStorage(
    noticias: Array<NoticiaActivaEnrutador>
  ) {
    this.noticiaRepository.guardarNoticiaActivaTerciosEnSessionStorage(
      noticias
    );
  }

  obtenerNoticiaActiviaTerciosDelSessionStorage(): Array<NoticiaActivaEnrutador> {
    return this.noticiaRepository.obtenerNoticiaActiviaTerciosDelSessionStorage();
  }

  removerNoticiaActivaTerciosDelSessionStorage() {
    this.noticiaRepository.removerNoticiaActivaTerciosDelSessionStorage();
  }

  obtenerNoticiaParaDemo(): NoticiaModel {
    return {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR,
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: '',
      },
      autor: '',
      adjuntos: [],
      perfil: {
        nombre: 'm4v15texto9',
        nombreContacto: 'm4v15texto9',
        tipoPerfil: {
          codigo: CodigosCatalogoTipoPerfil.CLASSIC,
        },
      },
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: [],
    };
  }

  validarNoticiaActivaSegunAccionCrear(perfil: PerfilModel): NoticiaModel {
    let noticia: NoticiaModel = this.obtenerNoticiaActiviaDelSessionStorage();

    if (!noticia) {
      noticia = {
        id: '',
        estado: {
          codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR,
        },
        fechaCreacion: new Date(),
        direccion: {
          descripcion: '',
        },
        autor: '',
        adjuntos: [],
        perfil: perfil,
        votos: [],
        totalVotos: 0,
        tituloCorto: '',
        titulo: '',
        descripcion: '',
        medias: [],
      };
      this.guardarNoticiaActivaEnSessionStorage(noticia);
    }
    return noticia;
  }

  validarNoticiaActivoSegunAccionCrear(
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ): NoticiaModel {
    let noticias = this.obtenerNoticiaActiviaTerciosDelSessionStorage();
    const noticiaNueva = this.crearObjetoDeNoticiaVacioParaRetornar(perfil);

    if (!noticias || noticias === null) {
      noticias = [];
    }
    const index2 = noticias?.findIndex(
      (e) => e.noticia.perfil._id === perfil._id
    );

    if (!noticias || noticias === null || index2 < 0) {
      noticias = [];
    }
    // const index = noticias.findIndex(e => e.ubicacion === ubicacion)
    let index: number = -1;

    for (let posNoticias = 0; posNoticias < noticias.length; posNoticias++) {
      if (noticias[posNoticias].ubicacion === ubicacion) {
        index = posNoticias;
      }
    }

    if (index <= 0) {
      const item: NoticiaActivaEnrutador = {
        ubicacion: ubicacion,
        noticia: noticiaNueva,
      };
      let ultimaPosTercio: number = -1;
      if (noticias) {
        for (
          let posArrayProyectos = 0;
          posArrayProyectos < noticias.length;
          posArrayProyectos++
        ) {
          if (
            noticias[posArrayProyectos].ubicacion === ubicacion &&
            noticias[posArrayProyectos].noticia.id === ''
          ) {
            ultimaPosTercio = posArrayProyectos;
          }
        }
        if (ultimaPosTercio === -1) {
          noticias.push(item);
          this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
          return item.noticia;
        }
      } else {
        this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
        return item.noticia;
      }
    }

    const item = noticias[index];
    return item.noticia;
  }

  validarNoticiaActivaSegunAccionActualizarVisitar(
    noticia: NoticiaModel,
    ubicacion: UbicacionDelComponente
  ) {
    let noticias = this.obtenerNoticiaActiviaTerciosDelSessionStorage();

    if (!noticias || noticias === null) {
      noticias = [];
    }

    const index = noticias.findIndex((e) => e.ubicacion === ubicacion);

    if (index < 0) {
      noticias.push({
        ubicacion: ubicacion,
        noticia: noticia,
      });

      this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
      return;
    }

    noticias[index].noticia = noticia;
    this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
  }

  crearObjetoDeNoticiaVacio(
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ) {
    const noticia: NoticiaModel = {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR,
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: '',
      },
      autor: '',
      adjuntos: [],
      perfil: perfil,
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: [],
    };

    this.guardarNoticiaEnNoticiaActivaTercios(ubicacion, noticia);
  }

  crearObjetoDeNoticiaVacioParaRetornar(perfil: PerfilModel): NoticiaModel {
    return {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR,
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: '',
      },
      autor: '',
      adjuntos: [],
      perfil: perfil,
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: [],
    };
  }

  asignarValoresDeLosCamposALaNoticia(
    params: NoticiaParams,
    noticia: NoticiaModel,
    noticiaForm: FormGroup,
    eliminarData: boolean,
    ubicacion: UbicacionDelComponente
  ): NoticiaModel {
    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:
        noticia.tituloCorto = noticiaForm.value.tituloCorto;
        noticia.titulo = noticiaForm.value.titulo;
        noticia.autor = noticiaForm.value.autor;
        noticia.direccion.descripcion = noticiaForm.value.ubicacion;

        this.guardarNoticiaEnNoticiaActivaTercios(ubicacion, noticia);
        break;
      case AccionEntidad.ACTUALIZAR:
        noticia.tituloCorto = noticiaForm.value.tituloCorto;
        noticia.titulo = noticiaForm.value.titulo;
        noticia.autor = noticiaForm.value.autor;
        noticia.direccion.descripcion = noticiaForm.value.ubicacion;
        break;
      default:
        break;
    }

    if (eliminarData) {
      this.removerNoticiaDeNoticiaActivaTercios(ubicacion);
    }

    return noticia;
  }

  crearNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
    return this.noticiaRepository.crearNoticia(noticia);
  }

  obtenerInformacionDeLaNoticia(
    idNoticia: string,
    idPerfil: string
  ): Observable<NoticiaModel> {
    return this.noticiaRepository.obtenerInformacionDeLaNoticia(
      idNoticia,
      idPerfil
    );
  }

  actualizarNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
    const noticiaSinAlbums: NoticiaModel =
      this.excluirAlbumsYaRegistradosDeLaNoticia(noticia);
    // noticia.adjuntos = []
    return this.noticiaRepository.actualizarNoticia(noticiaSinAlbums);
  }

  excluirAlbumsYaRegistradosDeLaNoticia(noticia: NoticiaModel) {
    const adjuntosParaEnviar: Array<AlbumModel> = [];
    noticia.adjuntos.forEach((item, pos) => {
      if (!(item && item._id)) {
        adjuntosParaEnviar.push(item);
      }
    });

    noticia.adjuntos = adjuntosParaEnviar;

    return noticia;
  }

  eliminarNoticia(idPerfil: string, idNoticia: string): Observable<string> {
    return this.noticiaRepository.eliminarNoticia(idPerfil, idNoticia);
  }

  apoyarNoticia(voto: VotoNoticiaModel): Observable<string> {
    return this.noticiaRepository.apoyarNoticia(voto);
  }

  buscarNoticiasSinfiltroFechas(
    limite: number,
    pagina: number,
    perfil: string
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasSinfiltroFechas(
      limite,
      pagina,
      perfil
    );
  }

  buscarNoticiasPorFechas(
    limite: number,
    pagina: number,
    fechaInicial: string,
    fechaFinal: string,
    filtro: string,
    perfil: string
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasPorFechas(
      limite,
      pagina,
      fechaInicial,
      fechaFinal,
      filtro,
      perfil
    );
  }

  buscarNoticiasPorTitulo(
    titulo: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasPorTitulo(
      titulo,
      limite,
      pagina
    );
  }

  asignarValoresDeLosCamposALaNoticiaParaValidarCambios(
    noticia: NoticiaModel,
    noticiaForm: FormGroup
  ): NoticiaModel {
    noticia.tituloCorto = noticiaForm.value.tituloCorto;
    noticia.titulo = noticiaForm.value.titulo;
    noticia.autor = noticiaForm.value.autor;
    noticia.direccion.descripcion = noticiaForm.value.ubicacion;

    return noticia;
  }

  validarSiExistenCambiosEnLaNoticia(
    noticiaAValidar: NoticiaModel,
    accionEntidad: AccionEntidad,
    ubicacion: UbicacionDelComponente
  ): boolean {
    try {
      const noticiaOriginal =
        accionEntidad === AccionEntidad.CREAR
          ? this.crearObjetoDeNoticiaVacioParaRetornar(noticiaAValidar.perfil)
          : this.obtenerNoticiaDeNoticiaActivaTercio(ubicacion);

      if (!noticiaOriginal || !noticiaAValidar) {
        return false;
      }

      if (
        noticiaAValidar.tituloCorto.trim() !==
        noticiaOriginal.tituloCorto.trim()
      ) {
        return true;
      }

      if (noticiaAValidar.titulo.trim() !== noticiaOriginal.titulo.trim()) {
        return true;
      }

      const direccionOriginal = noticiaOriginal.direccion;
      const direccionModificada = noticiaAValidar.direccion;

      if (
        direccionOriginal.descripcion.trim() !==
        direccionModificada.descripcion.trim()
      ) {
        return true;
      }

      if (noticiaAValidar.descripcion !== noticiaOriginal.descripcion) {
        return true;
      }

      if (noticiaAValidar.autor.trim() !== noticiaOriginal.autor.trim()) {
        return true;
      }

      if (noticiaAValidar.medias.length !== noticiaOriginal.medias.length) {
        return true;
      }

      const mediaAValidar = noticiaAValidar.medias[0];
      const mediaOriginal = noticiaOriginal.medias[0];

      if (
        mediaAValidar &&
        mediaOriginal &&
        mediaAValidar.id !== mediaOriginal.id
      ) {
        return true;
      }
      return false;
    } catch (error) {
      return false;
    }
  }

  guardarNoticiaEnNoticiaActivaTercios(
    ubicacion: UbicacionDelComponente,
    noticia: NoticiaModel
  ) {
    let noticias = this.obtenerNoticiaActiviaTerciosDelSessionStorage();

    if (!noticias || noticias === null) {
      noticias = [];
    }

    const index = noticias.findIndex((e) => e.ubicacion === ubicacion);

    if (index < 0) {
      noticias.push({
        noticia: noticia,
        ubicacion: ubicacion,
      });

      this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
      return;
    }

    noticias[index].noticia = noticia;
    this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
  }

  removerNoticiaDeNoticiaActivaTercios(ubicacion: UbicacionDelComponente) {
    let noticias = this.obtenerNoticiaActiviaTerciosDelSessionStorage();

    if (!noticias || noticias === null) {
      noticias = [];
    }

    const index = noticias.findIndex((e) => e.ubicacion === ubicacion);

    if (index >= 0) {
      noticias.splice(index, 1);
      this.guardarNoticiaActivaTerciosEnSessionStorage(noticias);
    }
  }

  obtenerNoticiaDeNoticiaActivaTercio(
    ubicacion: UbicacionDelComponente
  ): NoticiaModel {
    let noticias = this.obtenerNoticiaActiviaTerciosDelSessionStorage();

    if (!noticias || noticias === null) {
      noticias = [];
    }

    // const index = noticias.findIndex(e => e.ubicacion === ubicacion)

    let index: number = -1;

    for (let posNoticias = 0; posNoticias < noticias.length; posNoticias++) {
      if (noticias[posNoticias].ubicacion === ubicacion) {
        index = posNoticias;
      }
    }

    if (index < 0) {
      return undefined;
    }
    return noticias[index].noticia;
  }
}
