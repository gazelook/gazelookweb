import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { ValorBase } from '@shared/diseno/modelos/moneda-picker.interface';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import {
  ProyectoActivoEnrutador,
  UbicacionDelComponente,
} from 'presentacion/enrutador/enrutador.component';
import { ProyectoEntity } from 'dominio/entidades/proyecto.entity';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import {
  DonacionProyectosModel,
  ProyectoModel,
} from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { ProyectoRepository } from 'dominio/repositorio/proyecto.repository';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { CodigosCatatalogosEstadoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-estado-proyecto.enum';
import { FiltroBusquedaProyectos } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { VotoProyectoModel } from 'dominio/modelo/entidades/voto-proyecto.model';
import { PagoEntity } from 'dominio/entidades';

@Injectable({ providedIn: 'root' })
export class ProyectoNegocio {
  constructor(private proyectoRepository: ProyectoRepository) {}

  guardarProyectoActivoEnSessionStorage(proyecto: ProyectoModel): void {
    this.proyectoRepository.guardarProyectoActivoEnSessionStorage(proyecto);
  }

  obtenerProyectoActivoDelSessionStorage(): ProyectoModel {
    return this.proyectoRepository.obtenerProyectoActivoDelSessionStorage();
  }

  guardarProyectoActivoTerciosEnSessionStorage(
    proyectos: Array<ProyectoActivoEnrutador>
  ): void {
    this.proyectoRepository.guardarProyectoActivoTerciosEnSessionStorage(
      proyectos
    );
  }

  obtenerProyectoActivoTerciosDelSessionStorage(): Array<ProyectoActivoEnrutador> {
    return this.proyectoRepository.obtenerProyectoActivoTerciosDelSessionStorage();
  }

  obtenerProyectoParaDemo(): ProyectoModel {
    return {
      id: '',
      tituloCorto: '',
      titulo: '',
      direccion: {
        descripcion: '',
        localidad: {
          codigo: '',
          codigoPostal: '',
          nombre: '',
          pais: {
            codigo: '',
            nombre: '',
          },
        },
      },
      adjuntos: [],
      valorEstimado: 0,
      tipo: {
        codigo: CodigosCatalogoTipoProyecto.PROYECTO_PAIS.toString(),
      },
      perfil: {
        nombreContacto: '',
        nombre: '',
        tipoPerfil: {
          codigo: CodigosCatalogoTipoPerfil.CLASSIC.toString(),
        },
      },
      descripcion: '',
      medias: [],
      moneda: {
        codigo: '',
      },
      estado: {
        codigo: CodigosCatatalogosEstadoProyecto.PROYECTO_SIN_CREAR,
      },
      fechaCreacion: new Date(),
    };
  }

  validarProyectoActivoSegunAccionCrear(
    codigoTipoProyecto: CodigosCatalogoTipoProyecto,
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ): ProyectoModel {
    let proyectos = this.obtenerProyectoActivoTerciosDelSessionStorage();
    const proyectoNuevo = this.crearObjetoVacioDeProyectoParaRetornar(
      codigoTipoProyecto,
      perfil
    );
    const index2 = proyectos?.findIndex(
      (e) => e.proyecto.perfil._id === perfil._id
    );

    if (!proyectos || index2 < 0) {
      proyectos = [];
    }

    // const index = proyectos.findIndex(e => e.ubicacion === ubicacion)

    let index = -1;

    for (
      let posProyectos = 0;
      posProyectos < proyectos.length;
      posProyectos++
    ) {
      if (proyectos[posProyectos].ubicacion === ubicacion) {
        index = posProyectos;
      }
    }

    if (index <= 0) {
      const proyectoActivoEnrutador: ProyectoActivoEnrutador = {
        ubicacion,
        proyecto: proyectoNuevo,
      };
      let ultimaPosTercio = -1;
      if (proyectos) {
        for (
          let posArrayProyectos = 0;
          posArrayProyectos < proyectos.length;
          posArrayProyectos++
        ) {
          if (
            proyectos[posArrayProyectos].ubicacion === ubicacion &&
            proyectos[posArrayProyectos].proyecto.id === ''
          ) {
            ultimaPosTercio = posArrayProyectos;
          }
        }
        if (ultimaPosTercio === -1) {
          proyectos.push(proyectoActivoEnrutador);
          this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
          return proyectoActivoEnrutador.proyecto;
        }
      } else {
        this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
        return proyectoActivoEnrutador.proyecto;
      }
    }

    const item = proyectos[index];

    if (
      item.proyecto &&
      item.proyecto.tipo &&
      item.proyecto.tipo.codigo &&
      item.proyecto.tipo.codigo === codigoTipoProyecto
    ) {
      return item.proyecto;
    }
    item.proyecto = proyectoNuevo;
    proyectos[index] = item;
    this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
    return item.proyecto;
  }

  validarProyectoActivoSegunAccionActualizarVisitar(
    proyecto: ProyectoModel,
    ubicacion: UbicacionDelComponente
  ): void {
    let proyectos = this.obtenerProyectoActivoTerciosDelSessionStorage();

    if (!proyectos) {
      proyectos = [];
    }

    const index = proyectos.findIndex((e) => e.ubicacion === ubicacion);

    if (index < 0) {
      proyectos.push({
        ubicacion,
        proyecto,
      });

      this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
      return;
    }

    proyectos[index].proyecto = proyecto;

    this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
  }

  crearObjetoVacioDeProyecto(
    codigoTipoProyecto: CodigosCatalogoTipoProyecto,
    perfil: PerfilModel,
    ubicacion: UbicacionDelComponente
  ): void {
    const proyecto: ProyectoModel = this.crearObjetoVacioDeProyectoParaRetornar(
      codigoTipoProyecto,
      perfil
    );

    this.guardarProyectoEnProyectosActivosTercio(ubicacion, proyecto);
  }

  crearObjetoVacioDeProyectoParaRetornar(
    codigoTipoProyecto: CodigosCatalogoTipoProyecto,
    perfil: PerfilModel
  ): ProyectoModel {
    const proyecto: ProyectoModel = {
      id: '',
      tituloCorto: '',
      titulo: '',
      direccion: {
        descripcion: '',
      },
      adjuntos: [],
      valorEstimado: 0,
      tipo: {
        codigo: codigoTipoProyecto,
      },
      perfil,
      descripcion: '',
      medias: [],
      moneda: {
        codigo: '',
      },
      estado: {
        codigo: CodigosCatatalogosEstadoProyecto.PROYECTO_SIN_CREAR,
      },
      fechaCreacion: new Date(),
    };

    if (
      codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
      codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_PAIS
    ) {
      proyecto.direccion.pais = {
        codigo: '',
        nombre: '',
      };
    }

    return proyecto;
  }

  asignarValoresDeLosCamposAlProyecto(
    params: ProyectoParams,
    proyecto: ProyectoModel,
    pais: ItemSelector,
    valorDeLaMoneda: ValorBase,
    monedaSeleccionada: ItemSelector,
    proyectoForm: FormGroup,
    eliminarData: boolean,
    ubicacion: UbicacionDelComponente
  ): ProyectoModel {
    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:
        proyecto.tituloCorto = proyectoForm.value.tituloCorto;
        proyecto.titulo = proyectoForm.value.titulo;
        proyecto.direccion.descripcion = proyectoForm.value.ubicacion;

        if (
          proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
          proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_PAIS
        ) {
          proyecto.direccion.pais.codigo = pais.codigo;
          proyecto.direccion.pais.nombre = pais.nombre;
        }

        proyecto.valorEstimado = parseInt(valorDeLaMoneda.valorNeto, 10);
        proyecto.moneda.codigo = monedaSeleccionada.codigo;
        proyecto.moneda.codNombre = monedaSeleccionada.auxiliar;

        this.guardarProyectoEnProyectosActivosTercio(ubicacion, proyecto);
        break;
      case AccionEntidad.ACTUALIZAR:
        proyecto.tituloCorto = proyectoForm.value.tituloCorto;
        proyecto.titulo = proyectoForm.value.titulo;
        proyecto.direccion.descripcion = proyectoForm.value.ubicacion;

        if (
          proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
          proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_PAIS
        ) {
          if (!proyecto.direccion.pais) {
            proyecto.direccion.pais = {
              codigo: '',
              nombre: '',
            };
          }

          proyecto.direccion.pais.codigo = pais.codigo;
          proyecto.direccion.pais.nombre = pais.nombre;
        }

        proyecto.valorEstimado = parseInt(valorDeLaMoneda.valorNeto, 10);
        proyecto.moneda.codigo = monedaSeleccionada.codigo;
        proyecto.moneda.codNombre = monedaSeleccionada.auxiliar;
        break;
      default:
        break;
    }

    if (eliminarData) {
      this.removerProyectoDeProyectoActivoTercios(ubicacion);
    }

    return proyecto;
  }

  crearProyecto(proyecto: ProyectoModel): Observable<ProyectoModel> {
    return this.proyectoRepository.crearProyecto(proyecto);
  }

  donacionesProyectos(
    donacionProyecto: DonacionProyectosModel
  ): Observable<PagoEntity> {
    return this.proyectoRepository.donacionesProyectos(donacionProyecto);
  }

  validarDonacionProyectoStripe(idTransaccion: string): Observable<boolean> {
    return this.proyectoRepository.validarDonacionProyectoStripe(idTransaccion);
  }

  removerProyectoActivoDelSessionStorage(): void {
    this.proyectoRepository.removerProyectoActivoDelSessionStorage();
  }

  obtenerInformacionDelProyecto(
    idProyecto: string,
    idPerfil: string,
    estado?: string
  ): Observable<ProyectoModel> {
    return this.proyectoRepository.obtenerInformacionDelProyecto(
      idProyecto,
      idPerfil,
      estado
    );
  }

  excluirAlbumsYaRegistradosDelProyecto(
    proyecto: ProyectoModel
  ): ProyectoModel {
    const adjuntosParaEnviar: Array<AlbumModel> = [];
    proyecto.adjuntos.forEach((item, pos) => {
      if (!(item && item._id)) {
        adjuntosParaEnviar.push(item);
      }
    });

    proyecto.adjuntos = adjuntosParaEnviar;

    return proyecto;
  }

  actualizarProyecto(proyecto: ProyectoModel): Observable<ProyectoModel> {
    const proyectoSinAlbums: ProyectoModel =
      this.excluirAlbumsYaRegistradosDelProyecto(proyecto);
    return this.proyectoRepository.actualizarProyecto(proyectoSinAlbums);
  }

  obtenerProyectosPerfil(
    idPerfil: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<ProyectoEntity>> {
    return this.proyectoRepository.obtenerProyectosPerfil(
      idPerfil,
      limite,
      pagina
    );
  }

  obtenerProyectosMapeadosDelPerfil(
    idPerfil: string,
    limite: number,
    pagina: number,
    traducir: boolean
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosMapeadosDelPerfil(
      idPerfil,
      limite,
      pagina,
      traducir
    );
  }

  apoyarProyecto(voto: VotoProyectoModel): Observable<string> {
    return this.proyectoRepository.apoyarProyecto(voto);
  }

  obtenerFechaMaximaParaActualizarValorEstimado(): Observable<{ fecha: Date }> {
    return this.proyectoRepository.obtenerFechaMaximaParaActualizarValorEstimado();
  }

  eliminarProyecto(idProyecto: string, idPerfil: string): Observable<string> {
    return this.proyectoRepository.eliminarProyecto(idProyecto, idPerfil);
  }

  obtenerFechaForo(inicio: boolean): Observable<any> {
    return this.proyectoRepository.obtenerFechaForo(inicio);
  }

  obtenerProyectosSinFriltroFechas(
    limite: number,
    pagina: number,
    perfil: string,
    tipo: CodigosCatalogoTipoProyecto
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosSinFriltroFechas(
      limite,
      pagina,
      perfil,
      tipo
    );
  }

  obtenerProyectosRecomendados(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosRecomendados(
      idPerfil,
      limite,
      pagina,
      tipo
    );
  }

  obtenerProyectosForos(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosForos(
      idPerfil,
      limite,
      pagina,
      tipo
    );
  }

  obtenerProyectosEsperaFinanciamiento(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosEsperaFinanciamiento(
      idPerfil,
      limite,
      pagina,
      tipo
    );
  }

  obtenerProyectosSeleccionados(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string,
    fecha?: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.obtenerProyectosSeleccionados(
      idPerfil,
      limite,
      pagina,
      tipo,
      fecha
    );
  }

  buscarProyectosPorFiltro(
    limite: number,
    pagina: number,
    filtro: FiltroBusquedaProyectos,
    tipo: CodigosCatalogoTipoProyecto,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.buscarProyectosPorFiltro(
      limite,
      pagina,
      filtro,
      tipo,
      perfil,
      fechaInicial,
      fechaFinal
    );
  }

  buscarProyectosPorTitulo(
    titulo: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoRepository.buscarProyectosPorTitulo(
      titulo,
      limite,
      pagina
    );
  }

  confirmarTransferenciaDeProyecto(
    idProyecto: string,
    idPerfilNuevo: string,
    idPerfilPropietario: string
  ): Observable<number> {
    return this.proyectoRepository.confirmarTransferenciaDeProyecto(
      idProyecto,
      idPerfilNuevo,
      idPerfilPropietario
    );
  }

  asignarValoresDeLosCamposAlProyectoParaValidarCambios(
    proyecto: ProyectoModel,
    pais: ItemSelector,
    valorDeLaMoneda: ValorBase,
    monedaSeleccionada: ItemSelector,
    proyectoForm: FormGroup
  ): ProyectoModel {
    proyecto.tituloCorto = proyectoForm.value.tituloCorto;
    proyecto.titulo = proyectoForm.value.titulo;
    proyecto.direccion.descripcion = proyectoForm.value.ubicacion;

    if (
      proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
      proyecto.tipo.codigo === CodigosCatalogoTipoProyecto.PROYECTO_PAIS
    ) {
      if (!proyecto.direccion.pais) {
        proyecto.direccion.pais = {
          codigo: '',
          nombre: '',
        };
      }

      proyecto.direccion.pais.codigo = pais.codigo;
      proyecto.direccion.pais.nombre = pais.nombre;
    }

    proyecto.valorEstimado = parseInt(valorDeLaMoneda.valorNeto, 10);
    proyecto.moneda.codigo = monedaSeleccionada.codigo;
    proyecto.moneda.codNombre = monedaSeleccionada.auxiliar;

    return proyecto;
  }

  validarSiExistenCambiosEnElProyecto(
    proyectoAValidar: ProyectoModel,
    accionEntidad: AccionEntidad,
    ubicacion: UbicacionDelComponente
  ): boolean {
    try {
      const proyectoOriginal =
        accionEntidad === AccionEntidad.CREAR
          ? this.crearObjetoVacioDeProyectoParaRetornar(
              proyectoAValidar.tipo.codigo as CodigosCatalogoTipoProyecto,
              proyectoAValidar.perfil
            )
          : this.obtenerProyectoDeProyectoActivoTercio(ubicacion);

      if (!proyectoOriginal || !proyectoAValidar) {
        return false;
      }

      if (
        proyectoAValidar.tituloCorto.trim() !==
        proyectoOriginal.tituloCorto.trim()
      ) {
        return true;
      }

      if (proyectoAValidar.titulo.trim() !== proyectoOriginal.titulo.trim()) {
        return true;
      }

      const direccionOriginal = proyectoOriginal.direccion;
      const direccionModificada = proyectoAValidar.direccion;

      if (
        proyectoAValidar.tipo.codigo ===
          CodigosCatalogoTipoProyecto.PROYECTO_LOCAL ||
        proyectoAValidar.tipo.codigo ===
          CodigosCatalogoTipoProyecto.PROYECTO_PAIS
      ) {
        if (!direccionOriginal.pais || !direccionModificada.pais) {
          return false;
        }

        if (
          direccionOriginal.pais.codigo.trim() !==
          direccionModificada.pais.codigo.trim()
        ) {
          return true;
        }
      }

      if (
        proyectoAValidar.tipo.codigo ===
          CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL ||
        proyectoAValidar.tipo.codigo ===
          CodigosCatalogoTipoProyecto.PROYECTO_LOCAL
      ) {
        if (direccionModificada.descripcion !== '') {
          if (
            direccionOriginal.descripcion &&
            direccionModificada.descripcion !== ''
          ) {
            if (
              direccionOriginal.descripcion.trim() !==
              direccionModificada.descripcion.trim()
            ) {
              return true;
            }
          }

          if (
            !direccionOriginal.descripcion &&
            direccionModificada.descripcion !== ''
          ) {
            return true;
          }
        }

        if (direccionModificada.descripcion === '') {
          if (direccionOriginal.descripcion) {
            return true;
          }
        }
      }

      if (proyectoAValidar.valorEstimado !== proyectoOriginal.valorEstimado) {
        return true;
      }

      if (
        proyectoAValidar.moneda.codNombre.length > 0 &&
        proyectoAValidar.moneda.codNombre !== proyectoOriginal.moneda.codNombre
      ) {
        return true;
      }

      if (proyectoAValidar.descripcion !== proyectoOriginal.descripcion) {
        return true;
      }

      if (proyectoAValidar.medias.length !== proyectoOriginal.medias.length) {
        return true;
      }

      const mediaAValidar = proyectoAValidar.medias[0];
      const mediaOriginal = proyectoOriginal.medias[0];

      return (
        mediaAValidar && mediaOriginal && mediaAValidar.id !== mediaOriginal.id
      );
    } catch (error) {
      return false;
    }
  }

  guardarProyectoEnProyectosActivosTercio(
    ubicacion: UbicacionDelComponente,
    proyecto: ProyectoModel
  ): void {
    let proyectos = this.obtenerProyectoActivoTerciosDelSessionStorage();

    if (!proyectos) {
      proyectos = [];
    }

    // const index = proyectos.findIndex(e => e.ubicacion === ubicacion)
    let index = -1;

    for (
      let posProyectos = 0;
      posProyectos < proyectos.length;
      posProyectos++
    ) {
      if (proyectos[posProyectos].ubicacion === ubicacion) {
        index = posProyectos;
      }
    }
    if (index < 0) {
      proyectos.push({
        proyecto,
        ubicacion,
      });

      this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
      return;
    }
    proyectos[index].proyecto = proyecto;
    this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
  }

  removerProyectoDeProyectoActivoTercios(
    ubicacion: UbicacionDelComponente
  ): void {
    let proyectos = this.obtenerProyectoActivoTerciosDelSessionStorage();

    if (!proyectos) {
      proyectos = [];
    }

    const index = proyectos.findIndex((e) => e.ubicacion === ubicacion);

    if (index >= 0) {
      proyectos.splice(index, 1);

      this.guardarProyectoActivoTerciosEnSessionStorage(proyectos);
    }
  }

  obtenerProyectoDeProyectoActivoTercio(
    ubicacion: UbicacionDelComponente
  ): ProyectoModel {
    let proyectos = this.obtenerProyectoActivoTerciosDelSessionStorage();

    if (!proyectos) {
      proyectos = [];
    }

    const index = proyectos.findIndex((e) => e.ubicacion === ubicacion);
    // let index: number = -1

    // for (let posProyectos = 0; posProyectos < proyectos.length; posProyectos++) {

    //     if (proyectos[posProyectos].ubicacion === ubicacion) {
    //         index === posProyectos
    //     }

    // }

    if (index < 0) {
      return undefined;
    }
    return proyectos[index].proyecto;
  }
}
