import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { ComentarioRepository } from 'dominio/repositorio/comentario.repository';
@Injectable({ providedIn: 'root' })
export class ComentarioNegocio {
  constructor(
    private comentarioRepository: ComentarioRepository
  ) { }

  crearComentario(comentario: ComentarioModel): Observable<ComentarioModel> {
    return this.comentarioRepository.crearComentario(comentario)
  }

  obtenerComentariosDelProyecto() { }

  obtenerComentarios(
    idProyecto: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<ComentarioModel>> {
    return this.comentarioRepository.obtenerComentarios(idProyecto, limite, pagina)
      .pipe(
        map((data: PaginacionModel<ComentarioModel>) => {
          return data
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  eliminarComentario(
    idProyecto: string,
    coautor?: string,
    idComentario?: string
  ): Observable<string> {
    return this.comentarioRepository.eliminarComentario(idProyecto, coautor, idComentario)
  }

  eliminarMiComentario(
    idComentario?: string,
    idCoautor?: string,
  ): Observable<string> {
    return this.comentarioRepository.eliminarMiComentario(idComentario, idCoautor)
  }

  obtenerContenidoComentario(
    comentarios: Array<ComentarioModel>
  ): Observable<ComentarioModel[]> {
    return this.comentarioRepository.obtenerContenidoComentario(comentarios)
  }
}
