// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://192.168.16.5:4000/api/',
  apiSocket: 'http://192.168.16.5:4000',
  apiKey: 'd2e621a6646a4211768cd68e26f21228a81',
  apiMantenimiento: 'https://support.gazelook.com/api/',
  agora: {
    appId: '33a4343cd8fc4ccab62d5549555188c2'
  },
  firebase: {
    apiKey: 'AIzaSyDvN6Ml6tvQT2mC-ZQTrcpbWLmzyiH_gXg',
    authDomain: 'gazelook-563ed.firebaseapp.com',
    projectId: 'gazelook-563ed',
    storageBucket: 'gazelook-563ed.appspot.com',
    messagingSenderId: '221376992422',
    appId: '1:221376992422:web:afaa2a082ee40987a21067',
    measurementId: 'G-M4L57SKYY8'
  },
  mapBoxToken: 'pk.eyJ1IjoiZ2F6ZWxvb2siLCJhIjoiY2tvdTdqeWpoMGN1bDJ4bzNqMnVnaWF1aSJ9.EeoWuUTzEGcu0XwGShytTA',
  pkStripe: 'pk_live_51IqgS8BwtZzPGjbYFMNVxWBs9Y35HpEb1qXz6XniTUcVAXurLHaYbMV5Gqi0SfMKfNYLsbFukMiwqFgrqiKUsYeT00r3MaYFve'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
