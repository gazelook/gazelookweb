// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://apiqa.gazelook.com/api/',
  // apiSocket: "https://api.gazelook.com/",
  apiMantenimiento: 'https://supportqa.gazelook.com/api/',
  apiKey: 'd2e621a6646a4211768cd68e26f21228a81',
  agora: {
    appId: '5db4afad7da0472999cfb765cbb7952b'
  },
  firebase: {
    apiKey: 'AIzaSyDvN6Ml6tvQT2mC-ZQTrcpbWLmzyiH_gXg',
    authDomain: 'gazelook-563ed.firebaseapp.com',
    databaseURL: 'https://gazelook-563ed-default-rtdb.firebaseio.com',
    projectId: 'gazelook-563ed',
    storageBucket: 'gazelook-563ed.appspot.com',
    messagingSenderId: '221376992422',
    appId: '1:221376992422:web:afaa2a082ee40987a21067',
    measurementId: 'G-M4L57SKYY8'
  },
  mapBoxToken: 'pk.eyJ1IjoiZ2F6ZWxvb2siLCJhIjoiY2tvdTdqeWpoMGN1bDJ4bzNqMnVnaWF1aSJ9.EeoWuUTzEGcu0XwGShytTA',
  clientIdPaypal: 'AfoZpR_cdNqCpFhZ9i2SdDd8by6G35z0CZGLZcGXrbRB_RfgVYgEUXy3ouWrl4drS-fd9hOv2DM2l8aq',
  pkStripe: 'pk_test_51IqgS8BwtZzPGjbYedBedJ5SLS8m3UBTBgi5OohH58uvtrB3HRP0fgt8i0WzckdfuY4KI1yg9GDCCVZpm87zCyiD00UlXHzeej',
  clientAppCodePaymentez: 'GAZELOOKCOM-EC-CLIENT',
  clientAppKeyPaymentez: 'MXzMNGWBvBqxmxCBHRymj7lXDnML8H',
  serverAppCodePaymentez: 'GAZELOOKCOM-EC-SERVER',
  serverAppKeyPaymentez: 'yA0qvxk3LK1H1ltDLfJSQWMNmzulkp',
  envModePaymentez: 'prod',
  apiPaymentez: 'https://ccapi.paymentez.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
