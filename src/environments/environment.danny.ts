// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "http://192.168.100.17:4000/api/",
  apiSocket: "http://192.168.100.17:4000",
  apiKey: "d2e621a6646a4211768cd68e26f21228a81",
  apiMantenimiento: "http://3.16.80.145:5000/api/",
  agora: {
    appId: '33a4343cd8fc4ccab62d5549555188c2'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
 